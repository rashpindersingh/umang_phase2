//
//  DownloadedDocumentsViewController.m
//  Umang
//
//  Created by Lokesh Jain on 17/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigiDownloadViewController.h"
#import "FAQWebVC.h"
#import "Base64.h"

#define kNoRecordImageHeight 85
@interface DigiDownloadViewController ()
{
    NSMutableArray *downloadDocArray;
    SharedManager *singleton;

}

@end

@implementation DigiDownloadViewController

- (void)viewDidLoad
{
    singleton = [SharedManager sharedSingleton];

    [super viewDidLoad];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/DigiFolder"];
    
    NSArray* dirs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath
                                                                        error:NULL];
    
    
    downloadDocArray = [NSMutableArray new];
    downloadDocArray = [dirs mutableCopy];
    
    self.noRecordsLabel.text  = NSLocalizedString(@"no_result_found", nil);
    
    if (downloadDocArray.count == 0)
    {
        self.noRecordsView.hidden = NO;
        [self.view bringSubviewToFront:self.noRecordsView];
        
        //self.downloadDocTableView.hidden = YES;
        
    }
    else
    {
        self.noRecordsView.hidden = YES;
        //self.downloadDocTableView.hidden = NO;
    }
    
    self.downloadDocTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self addNavigationView];
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    _backBtn.hidden = true;
    self.screenTitle.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backButtonAction:btnBack];
    };
    nvView.lblTitle.text = self.screenTitle.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect frame = self.downloadDocTableView.frame;
        frame.origin.y = kiPhoneXNaviHeight;
        frame.size.height = fDeviceHeight - kiPhoneXNaviHeight;
        self.downloadDocTableView.frame = frame;
        self.noRecordsView.frame = frame;
        [self.view layoutIfNeeded];
    }
    self.noFileImageView.frame = CGRectMake(self.noRecordsView.frame.size.width/2 - kNoRecordImageHeight/2, self.noRecordsView.frame.size.height/2 - (kNoRecordImageHeight+kNoRecordImageHeight/2), kNoRecordImageHeight, kNoRecordImageHeight);
    self.noRecordsLabel.frame = CGRectMake(0, self.noFileImageView.frame.origin.y + self.noFileImageView.frame.size.height+8, self.view.frame.size.width, 30);
    
}
-(void)setViewFont{
    [self.backBtn.titleLabel setFont:[AppFont regularFont:17.0]];
    self.screenTitle.font = [AppFont semiBoldFont:17.0];
    _noRecordsLabel.font = [AppFont regularFont:16.0];
    
}
- (IBAction)backButtonAction:(UIButton *)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"DigiLocker Download View" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
}

# pragma mark - UITableView Delegate and Datasource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return downloadDocArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DownloadDocCell *cell = (DownloadDocCell *)[tableView dequeueReusableCellWithIdentifier:@"DownloadDocCellIdentifier"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.docNameLabel.text = [downloadDocArray objectAtIndex:indexPath.row];
    
    cell.docDateLabel.text = @"";
    
    //cell.docDateLabel.text = [NSString stringWithFormat:@"%@",[[[[self.issuedDocArray objectAtIndex:indexPath.row] valueForKey:@"date"] componentsSeparatedByString:@"T"] objectAtIndex:0]];
    
    
    if ([[downloadDocArray objectAtIndex:indexPath.row] containsString:@"jpeg"])
    {
        cell.docImageView.image = [UIImage imageNamed:@"adhr_icon_jpg.png"];
    }
    else if ([[downloadDocArray objectAtIndex:indexPath.row] containsString:@"png"])
    {
        cell.docImageView.image = [UIImage imageNamed:@"adhr_icon_png.png"];
    }
    else if ([[downloadDocArray objectAtIndex:indexPath.row] containsString:@"pdf"])
    {
        cell.docImageView.image = [UIImage imageNamed:@"adhr_icon_pdf.png"];
    }
    cell.docNameLabel.font = [AppFont regularFont:16.0];
    cell.docDateLabel.font = [AppFont regularFont:14.0];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/DigiFolder"];
    
    NSString *fileName = [NSString stringWithFormat:@"/%@",[downloadDocArray objectAtIndex:indexPath.row]];
    
    NSString *pathToFile = [dataPath stringByAppendingPathComponent:fileName];
    
    
    
    NSData *documentData = [NSData dataWithContentsOfFile:pathToFile];
    
    NSString *mimeType = [self mimeTypeForData:documentData]; // how would I implement getMimeType
    
    
    NSString *base64String = [documentData base64Encoding]; //my base64Encoding function
    
    NSData *base64data = [[NSData alloc] initWithData:[NSData dataWithBase64EncodedString:base64String]];
    
    [self openPDFfile:base64data withfilename:fileName withType:mimeType];
}


-(void)openPDFfile:(NSData*)pdfDocumentData withfilename:(NSString*)name withType:(NSString*)type

{
    [singleton traceEvents:@"Open FAQWeb" withAction:@"Clicked" withLabel:@"DigiLocker Download View" andValue:0];

    //NSLog(@"OpenWebViewFeedback");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.pdfData=pdfDocumentData;
    vc.titleOpen=@"PDF View";
    vc.urltoOpen=@"";
    vc.isfrom=@"WEBVIEWHANDLEPDF";
    vc.file_name=name;
    vc.file_type=type;
    vc.file_shareType=@"ShareYes";
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    // [self performSelector:@selector(openPDFfile:) withObject:pdfDocumentData afterDelay:1];
    
    
}

-(NSString *)mimeTypeForData:(NSData *)data
{
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
            break;
        case 0x89:
            return @"image/png";
            break;
        case 0x47:
            return @"image/gif";
            break;
        case 0x49:
        case 0x4D:
            return @"image/tiff";
            break;
        case 0x25:
            return @"application/pdf";
            break;
        case 0xD0:
            return @"application/vnd";
            break;
        case 0x46:
            return @"text/plain";
            break;
        default:
            return @"application/octet-stream";
    }
    return nil;
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
        
        NSLog(@"topController name = %@",topController);
        
    }
    
    return topController;
}

#pragma mark -
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end


#pragma mark - Download Doc Cell Interface and Implementation

@interface DownloadDocCell()
@end

@implementation DownloadDocCell

@end
