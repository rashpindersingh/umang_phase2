//
//  BLVideo.h
//  BlendPhotoAndVideo
//
//  Created by Hoang Tran on 12/26/14.
//  Copyright (c) 2014 ILandApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLMediaItem.h"

@interface BLVideo : BLMediaItem

@property (nonatomic, copy) NSURL *videoURL;
@property (nonatomic, assign) CGPoint visiblePoint;
@property (nonatomic, assign) CGSize scaledSize;

@end
