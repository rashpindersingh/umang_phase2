//
//  SocialHelpVC.m
//  Umang
//
//  Created by admin on 27/02/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "SocialHelpVC.h"
#import "SocialHelpIconCell.h"
#import "HelpViewController.h"
#import "FAQWebVC.h"
#import "SubmitQueryVC.h"


@interface SocialHelpVC () <UITableViewDelegate,UITableViewDataSource>
{
    
    NSMutableArray *arrTblData;
    NSMutableArray *arrImage;
    __weak IBOutlet UIView *vwSocialMedia;
      SharedManager *singleton;
    __weak IBOutlet UIButton *btnBack;
}


@property (weak, nonatomic) IBOutlet UILabel *lblSocialMedia;
@property (weak, nonatomic) IBOutlet UILabel *lblAbtSocialMedia;
@property (weak, nonatomic) IBOutlet UITableView *tblHelpSocial;


@end

@implementation SocialHelpVC



- (void)viewDidLoad
{
    [super viewDidLoad];
     singleton=[SharedManager sharedSingleton];
    _tblHelpSocial.delegate = self;
    _tblHelpSocial.dataSource = self;
    
    vwSocialMedia.layer.borderWidth = 0.5;
    vwSocialMedia.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    arrTblData = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"help_faq", nil),NSLocalizedString(@"user_manual", nil), nil];
    
    arrImage = [[NSMutableArray alloc]initWithObjects:@"more_help_support",@"faq_Updated",@"_SocialHelp", nil];
    
    _lblSocialMedia.text = NSLocalizedString(@"social_media_accounts", nil);
 
    CGRect lblAadharTitleFrame =  _lblSocialMedia.frame;
    lblAadharTitleFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(_tblHelpSocial.frame) -lblAadharTitleFrame.size.width +50: 14;
    _lblAbtSocialMedia.adjustsFontSizeToFitWidth = YES;
    _lblAbtSocialMedia.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    _lblSocialMedia.frame =  lblAadharTitleFrame;
    
   
    _lblAbtSocialMedia.text = NSLocalizedString(@"link_social_account_help_txt", nil);
    _lblAbtSocialMedia.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    
     CGRect imgSocialFrame =  _imgSocial.frame;
      imgSocialFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(_tblHelpSocial.frame) -50: 15;
    _imgSocial.frame =  imgSocialFrame;
    [self addNavigationView];

    
    // Do any additional setup after loading the view.
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btnBack.hidden = true;
  //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btnBackClicked:btnBack];
    };
    nvView.lblTitle.text = NSLocalizedString(@"social_media_accounts", "");
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tblFrame = vwSocialMedia.frame;
        tblFrame.origin.y = 20.0 + kiPhoneXNaviHeight;
        vwSocialMedia.frame = tblFrame;
        CGRect tbFrame = _tblHelpSocial.frame;
        tbFrame.origin.y = 20.0 + CGRectGetHeight(tblFrame) + kiPhoneXNaviHeight ;
        _tblHelpSocial.frame = tbFrame;
        [self.view layoutIfNeeded];
    }
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _lblSocialMedia.font = [AppFont regularFont:16.0];
    _lblAbtSocialMedia.font = [AppFont regularFont:16.0];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section

{
    return  44;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    //check header height is valid
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,50)];
    headerView.backgroundColor=[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(15,15,300,25);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:0.298 green:0.337 blue:0.424 alpha:1.0];
    CGRect labelFrame =  label.frame;
    labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
    label.frame = labelFrame;

    
    
    label.font = [UIFont systemFontOfSize:14.0];
    
    label.text = NSLocalizedString(@"more_help_lbl", nil);
    
    
    [headerView addSubview:label];
    
    return headerView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"HelpCell";
    
    SocialHelpIconCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    cell.lblSocialHelp.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    
    // update frame
    
    CGRect imgvwFrame = cell.img_SocialHelp.frame;
    imgvwFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 80 : 15;
    cell.img_SocialHelp.frame = imgvwFrame;
    
    
    
    
    CGRect lblFrame = cell.lblSocialHelp.frame;
    lblFrame.origin.x = singleton.isArabicSelected ? 10 : 60;
    cell.lblSocialHelp.frame = lblFrame;
    
    if (cell == nil)
    {
        cell = [[SocialHelpIconCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [arrTblData objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[arrImage objectAtIndex:indexPath.row]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [AppFont regularFont:17.0];

    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section]==0)
    {

    if (indexPath.row==0)
    {
        
        [self myHelp_Action];
        
    }
    
    if (indexPath.row==1)
    {
        
        
        [self openFAQWebVC];
        
    }
    
        if (indexPath.row==2)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
            if ([singleton.arr_initResponse  count]>0) {
                vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"usrman"];
                
            }
            
            vc.titleOpen= NSLocalizedString(@"user_manual", nil);
            //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
}

-(void)myHelp_Action
{
    NSLog(@"My Help Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)openFAQWebVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    if ([singleton.arr_initResponse  count]>0) {
        vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"faq"];
        
    }
    vc.titleOpen= NSLocalizedString(@"help_faq", nil);
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
