//
//  ProfilePickerVC.m
//  Umang
//
//  Created by admin on 18/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ProfilePickerVC.h"
#import "CustomPickerCell.h"

@interface ProfilePickerVC ()
@property(nonatomic,retain)NSMutableArray *table_data;

@end

@implementation ProfilePickerVC
@synthesize table_data;
@synthesize get_arr_element,get_title_pass,get_TAG;


- (void)viewDidLoad
{
    singleton = [SharedManager sharedSingleton];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    vw_line.frame=CGRectMake(0, 74, fDeviceWidth, 0.5);
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    table_State.contentInset = UIEdgeInsetsMake(-20,0,0,0);
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


-(void)viewWillAppear:(BOOL)animated
{
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(btn_backClicked:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.navigationController.view addGestureRecognizer:gestureRecognizer];
    
    
    lbl_header.text=get_title_pass;
    
    table_data=[[NSMutableArray alloc]init];
    
    table_data=[get_arr_element mutableCopy];
    
    //[self setHeightOfTableView];
    
    // [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:0.1];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
}

-(void)setHeightOfTableView
{
    table_State.contentInset = UIEdgeInsetsMake(-20,0,0,0);
    
    
    /**** set frame size of tableview according to number of cells ****/
    float height=45.0f*[table_data count]+42;
    
    if (height>400) {
        
    }
    else
    {
        [table_State setFrame:CGRectMake(table_State.frame.origin.x, table_State.frame.origin.y, table_State.frame.size.width,height-3)];
        
    }
    table_State.scrollEnabled = YES;
    
    //table_helpView.layer.backgroundColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
    table_State.layer.borderWidth=1;
    table_State.layer.borderColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
    
    //[table_State reloadData];
    
}


//- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0.001f;
//}



//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    if (scrollView.contentOffset.y<=0) {
//        scrollView.contentOffset = CGPointZero;
//    }
//}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Here you must return the number of sectiosn you want
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  40;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    //check header height is valid
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
    headerView.backgroundColor=[UIColor clearColor];
    
    
    return headerView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [table_data  count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       [self valueSelectedAction:indexPath];
                       [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
                       [table_State reloadData];
                   });
    
    [self btn_backClicked:self];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomPickerCell";
    
    CustomPickerCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CustomPickerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.lbl_title.text=[table_data objectAtIndex:indexPath.row];
    
    
    
    if ([get_TAG isEqualToString:TAG_STATE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.stateSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[UIFont boldSystemFontOfSize:16]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    
    if ([get_TAG isEqualToString:TAG_STATE_PROFILE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.profilestateSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[UIFont boldSystemFontOfSize:16]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    if ([get_TAG isEqualToString:TAG_GENDER]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypeGenderSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[UIFont boldSystemFontOfSize:16]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    if ([get_TAG isEqualToString:TAG_CITY]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypeCitySelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[UIFont boldSystemFontOfSize:16]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    
    
    if ([get_TAG isEqualToString:TAG_DISTRICT]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypDistricteSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[UIFont boldSystemFontOfSize:16]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    
    if ([get_TAG isEqualToString:TAG_OCCUPATION_PROFILE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.user_Occupation]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[UIFont boldSystemFontOfSize:16]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    if ([get_TAG isEqualToString:TAG_QUALIFI_PROFILE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.user_Qualification]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[UIFont boldSystemFontOfSize:16]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    
    // cell.accessoryView.backgroundColor = [UIColor clearColor]; // Not working
    
    
    
    return cell;
}


- (void)valueSelectedAction:(NSIndexPath *)index
{
    
    // Add image to button for normal state
    
    int indexrow=(int)index.row;
    
    NSLog(@"Selected State=%@",[table_data objectAtIndex:indexrow]);
    
    
    
    if ([get_TAG isEqualToString:TAG_STATE]) {
        singleton.stateSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    
    if ([get_TAG isEqualToString:TAG_GENDER]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.notiTypeGenderSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    
    
    if ([get_TAG isEqualToString:TAG_DISTRICT]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.notiTypDistricteSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    
    if ([get_TAG isEqualToString:TAG_STATE_PROFILE]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.profilestateSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    
    if ([get_TAG isEqualToString:TAG_CITY]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.notiTypeCitySelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    
    
    if ([get_TAG isEqualToString:TAG_OCCUPATION_PROFILE]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.user_Occupation=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        
    }
    
    
    
    if ([get_TAG isEqualToString:TAG_QUALIFI_PROFILE]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.user_Qualification=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        
        
    }
    
    
    
    
    
    
}





- (IBAction)btn_backClicked:(id)sender
{
    // [self
    //    CATransition *transition = [CATransition animation];
    //    transition.duration = 0.001;
    //    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionFade;
    //    transition.subtype = kCATransitionFromLeft;
    // [self.view.window.layer addAnimation:transition forKey:nil];
    //  [self dismissViewControllerAnimated:NO completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
    // [self dismissViewControllerAnimated:YES completion:nil];
    
    
    if ([self.TAG_FROM isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    if ([self.TAG_FROM isEqualToString:@"ISFROMREGISTRATION"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

@end
