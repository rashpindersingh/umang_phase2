//
//  BLMediaItem.m
//  BlendPhotoAndVideo
//
//  Created by Hoang Tran on 12/26/14.
//  Copyright (c) 2014 ILandApp. All rights reserved.
//

#import "BLMediaItem.h"
#import "Common.h"

@implementation BLMediaItem

-(instancetype)init
{
    self = [super init];
    
    if (self) {
        self.opacity = 1.0f;
    }
    
    return self;
}

-(BOOL)isEqual:(BLMediaItem*)object
{
    
    if ([[self.asset valueForProperty:ALAssetPropertyAssetURL] isEqual:[object.asset valueForProperty:ALAssetPropertyAssetURL]]) {
        return YES;
    }
    return NO;
}

-(UIImage*)thumbnail
{
    assert("NOT IMPLEMENTED IN SUB CLASS");
    return nil;
}

-(UIImage*)fullSizeImage
{
    assert("NOT IMPLEMENTED IN SUB CLASS");
    return nil;
}

-(BOOL)isVideo
{
    return NO;
}

-(BOOL)isPhoto
{
    return NO;
}

-(CGSize)getThumbnailSizeFromSize:(CGSize)size
{
    float scale = getPreviewSize().width/size.width;
    return CGSizeMake(roundf(size.width * scale), roundf(size.height * scale));
}

@end
