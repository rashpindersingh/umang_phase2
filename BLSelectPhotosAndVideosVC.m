 //
//  BLSelectPhotosAndVideosVC.m
//  BlendPhotoAndVideo
//
//  Created by Hoang Tran on 12/24/14.
//  Copyright (c) 2014 ILandApp. All rights reserved.
//


#include <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>
#import "BLSelectPhotosAndVideosVC.h"
#import "BLPhotoVideoCLVC.h"
#import "BLPhoto.h"
#import "BLVideo.h"
#import "AppConstants.h"
//#import "BLCameraVC.h"
#import "BLSelectAlbumCell.h"


#import "UIImage+FX.h"


@interface BLSelectPhotosAndVideosVC () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) IBOutlet UICollectionView *photoCollectionView;
@property (strong, nonatomic) IBOutlet UITableView *tbAlbums;

@property (strong, nonatomic) IBOutlet UIButton *btChooseAlbum;
@property (strong, nonatomic) IBOutlet UIButton *btDone;
@property (strong, nonatomic) IBOutlet UILabel *lbPhotosSelected;

@property (strong, nonatomic) IBOutlet UIView *viewImageChoosed;

@property (strong, nonatomic) IBOutlet UIView *viewChoosedFirst;
@property (strong, nonatomic) IBOutlet UIView *viewChoosedSecond;
@property (strong, nonatomic) IBOutlet UIView *viewChoosedThird;
@property (strong, nonatomic) IBOutlet UIView *viewChoosedFourth;
@property (strong, nonatomic) IBOutlet UIView *viewChoosedFifth;
@property (strong, nonatomic) IBOutlet UIView *middleView;

@property (strong, nonatomic) IBOutlet UIView *viewAdAndSelectedItems;

@property (strong, nonatomic) IBOutlet UIImageView *imgChoosedFirst;
@property (strong, nonatomic) IBOutlet UIImageView *imgChoosedSecond;
@property (strong, nonatomic) IBOutlet UIImageView *imgChoosedThird;
@property (strong, nonatomic) IBOutlet UIImageView *imgChoosedFourth;
@property (strong, nonatomic) IBOutlet UIImageView *imgChoosedFifth;

@property (nonatomic, assign) IBOutlet UIImageView *img1VideoSign;
@property (nonatomic, assign) IBOutlet UIImageView *img2VideoSign;
@property (nonatomic, assign) IBOutlet UIImageView *img3VideoSign;
@property (nonatomic, assign) IBOutlet UIImageView *img4VideoSign;
@property (nonatomic, assign) IBOutlet UIImageView *img5VideoSign;

@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property (nonatomic, strong) NSArray *allItems;
@property (nonatomic, strong) NSArray *arrayGroup;
@property (nonatomic, strong) NSArray *arrayNumberOfAb;
@property (nonatomic, strong) NSArray *arrayAlbumAvatar;

@property BOOL isCaptured;
@property BOOL isChoosedAlbum;
@property BOOL isChoosedCameraRoll;
@property BOOL isCapturedVideo;
@property (nonatomic, copy) NSString *choosedAlbumName;


@property (nonatomic ,strong)NSString * ShowPhotosForSelectedID;
@property (nonatomic) BOOL shouldPreventDisplayCellAnimation;

@end


@implementation BLSelectPhotosAndVideosVC
//@synthesize selectedItems = _selectedItems;
@synthesize assetsLibrary = _assetsLibrary;

@synthesize chooseMediaType;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [appDelegate.selectedItems removeAllObjects];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.ShowPhotosForSelectedID =@"Camera roll selected";
    
    [self initializeObjects];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Initializer

-(void)initializeObjects{
    
    self.btDone.contentMode  = UIViewContentModeScaleAspectFit;
    
    [self.tbAlbums setHidden:YES];
    [self customImageChoosed];
    
    switch ([ALAssetsLibrary authorizationStatus]) {
        case ALAuthorizationStatusAuthorized:
        {
            [self getPhotoOfAlbumsWithAlbumNameChoosed:@"Camera Roll"];
        }
            break;
            
        case ALAuthorizationStatusDenied:
            NSLog(@"Denied access to photo library");
            break;
            
        case ALAuthorizationStatusNotDetermined:
            
            if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)
            {
                
                [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                    switch (status) {
                        case PHAuthorizationStatusAuthorized:
                        {
                            [self getPhotoOfAlbumsWithAlbumNameChoosed:@"Camera Roll"];
                        }
                            break;
                        case PHAuthorizationStatusRestricted:
                            NSLog(@"Restricted access to photo library");
                            
                            break;
                        case PHAuthorizationStatusDenied:
                            NSLog(@"Denied access to photo library");
                            
                            break;
                        default:
                            break;
                    }
                }];
            }
            else
            {
                [self getPhotoOfAlbumsWithAlbumNameChoosed:@"Camera Roll"];
            }
            
            break;
            
        case ALAuthorizationStatusRestricted:
            NSLog(@"Restricted access to photo library");
            break;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLibrary:) name:UIApplicationWillEnterForegroundNotification object:nil];
   
    [self showPhotoChoosed];
}

-(void)deallocObjects{
    
}

/*-(void)setSelectedItems:(NSMutableArray *)selectedItems{
    selectedItems = _selectedItems;
}
-(NSMutableArray *)selectedItems{
    if (_selectedItems == nil) {
        _selectedItems = [[NSMutableArray alloc]init];
    }
    return _selectedItems;
}*/

-(ALAssetsLibrary*)assetsLibrary{
    if (_assetsLibrary == nil) {
        _assetsLibrary = [[ALAssetsLibrary alloc] init];
    }
    return _assetsLibrary;
}

#pragma mark - UICollectionView delegate & datasource
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    /*UIView *cellContentView  = [cell contentView];
    CGFloat rotationAngleDegrees = -30;
    CGFloat rotationAngleRadians = rotationAngleDegrees * (M_PI/180);
    CGPoint offsetPositioning = CGPointMake(500, -20.0);
    CATransform3D transform = CATransform3DIdentity;
    transform = CATransform3DRotate(transform, rotationAngleRadians, -50.0, 0.0, 1.0);
    transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, -50.0);
    cellContentView.layer.transform = transform;
    cellContentView.layer.opacity = 0.8;
    
    [UIView animateWithDuration:.65 delay:0.0 usingSpringWithDamping:0.85 initialSpringVelocity:.8 options:0 animations:^{
        cellContentView.layer.transform = CATransform3DIdentity;
        cellContentView.layer.opacity = 1;
    } completion:^(BOOL finished) {}];*/
    
    
    /*CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0/ -600;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    
    //3. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];*/
    
    if (!self.shouldPreventDisplayCellAnimation) {
        
        UIView* view = [cell contentView];
        view.layer.transform = [self initialTransformation];
        view.layer.opacity = 0.3;
        
        [UIView animateWithDuration:1.0 animations:^{
            view.layer.transform = CATransform3DIdentity;
            view.layer.opacity = 1;
        }];
    }
    
    
    
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.shouldPreventDisplayCellAnimation=NO;
}

-(CATransform3D)initialTransformation
{
    CATransform3D transform = CATransform3DIdentity;
    
    transform = CATransform3DMakeScale(0.2, 0.2, 0.1);
//    transform = CATransform3DScale(transform, -0.5, -0.5, 0.0);
    
    
    
    return transform;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger index;
    ALAsset *asset;
    

    BLPhotoVideoCLVC *cell                                  = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoVideoCLVIdentifier" forIndexPath:indexPath];
    
    cell.viewBorder.layer.cornerRadius                      = 0;
    cell.viewBorder.layer.borderWidth                       = 8.0;
    cell.viewBorder.layer.borderColor=[UIColor colorWithRed:226.0/255.0 green:226.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    cell.viewBorder.layer.masksToBounds                     = YES;
    [cell.viewBorder setBackgroundColor:[UIColor clearColor]];
    [cell.viewBorder setHidden:YES];
    
    [cell.imgChecked setHidden:YES];
    
    
    if (self.isChoosedCameraRoll && self.isCaptured && indexPath.row == 1 && (!self.isChoosedVideo || !self.isCapturedVideo)) {
        [cell.imgChecked setHidden:NO];
        [cell.viewBorder setHidden:NO];
    } else {
        [cell.imgChecked setHidden:YES];
        [cell.viewBorder setHidden:YES];
    }

    
    if (self.isChoosedCameraRoll == YES)
    {
        
        index = indexPath.row ;
//        if (indexPath.row == 0)
//        {
//            [cell.imgView setImage:[UIImage imageNamed:@"Icon_Camera"]];
//        }
//        else
        
        
        if (self.allItems.count)
        {
            
            asset = ((BLMediaItem *)[self.allItems objectAtIndex:(self.allItems.count - index -1)]).asset;
            
            
            if ([kSystemVersion floatValue] >= 9.0) {
                
                [cell.imgView setImage:[UIImage imageWithCGImage:[asset aspectRatioThumbnail]]];
                [cell.imgView setContentMode:UIViewContentModeScaleAspectFill];
                
            }
            else
            {
                [cell.imgView setImage:[UIImage imageWithCGImage:[asset thumbnail]]];
            }
            
            for (BLMediaItem *item in AppDel.selectedItems) {
                if ([item isEqual:[self.allItems objectAtIndex:(self.allItems.count - index -1)]]) {
                    [cell.viewBorder setHidden:NO];
                    [cell.imgChecked setHidden:NO];
                }
            }
            
        }
    }
    else
    {
        
        index = indexPath.row;
        asset = ((BLMediaItem*)[self.allItems objectAtIndex:(self.allItems.count - index -1)]).asset;
        
        [cell.imgView setImage:[UIImage imageWithCGImage:[asset thumbnail]]];
        
        for (BLMediaItem *item in AppDel.selectedItems) {
            if ([item isEqual:[self.allItems objectAtIndex:(self.allItems.count - index -1)]]) {
                [cell.viewBorder setHidden:NO];
                [cell.imgChecked setHidden:NO];
            }
        }
        
        
    }
    
    if ([[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo])
    {
        [cell.imgViewVideoPlaySign setHidden:NO];
    }
    else
    {
        [cell.imgViewVideoPlaySign setHidden:YES];
    }
    
    
    

    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger max = 3;

    NSInteger index;
    
    if (self.isChoosedCameraRoll) {
        index                                                   = indexPath.row;
    } else {
        index                                                   = indexPath.row;
    }
    
    BOOL selected                                           = NO;
    //Remove if this item in selected
    for (BLMediaItem *item in AppDel.selectedItems)
    {
        if ([item isEqual:[self.allItems objectAtIndex:(self.allItems.count - index-1)]])
        {
            //Remove
            [AppDel.selectedItems removeObject:item];
            [self showPhotoChoosed];
            selected = YES;
            
            break;
        }
    }
    
    if (!selected) {
        
        if (AppDel.selectedItems.count >= max)
        {
            
        }
        else
        {
            ALAsset *asset = ((BLMediaItem *)[self.allItems objectAtIndex:(self.allItems.count - index-1)]).asset;
            if (self.isChoosedVideo && [[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]) {
                NSLog(@"You can choose only one video");
            } else if ([[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]) {
                [AppDel.selectedItems insertObject:[self.allItems objectAtIndex:(self.allItems.count - index)] atIndex:0];
                [self showPhotoChoosed];
            } else {
                [AppDel.selectedItems addObject:[self.allItems objectAtIndex:(self.allItems.count - index-1)]];
                [self showPhotoChoosed];
            }
        }
        
    }
    
    self.shouldPreventDisplayCellAnimation=YES;
    [self.photoCollectionView reloadData];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.isChoosedCameraRoll) {
        return self.allItems.count;
    } else {
        return self.allItems.count;
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(collectionView.frame.size.width/3 - 2, collectionView.frame.size.width /3 - 2);
}

#pragma mark - Get all photos from library

-(void)getPhotoAfterCaptureWithAlbumName:(NSString *)albumChoosed
{
    
    if ([albumChoosed isEqualToString:@"Camera Roll"]) {
        [self.btChooseAlbum setTitle:@"CAMERA ROLL ▼" forState:UIControlStateNormal];
        self.isChoosedCameraRoll                                = YES;
    }
    
    NSMutableArray *arrayGroup = [NSMutableArray array];
    NSMutableArray *arrayAlbumAvatar = [NSMutableArray array];
    NSMutableArray *arrayNumberOfAb  = [NSMutableArray array];
    NSMutableArray *allItems = [NSMutableArray array];
    
    __weak typeof (&*self) weakSelfRef = self;
    
    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (result != nil) {
            NSString *type = [result valueForProperty:ALAssetPropertyType];
            
           /* if ([type isEqualToString:ALAssetTypePhoto]) {
                BLMediaItem *photo = [[BLPhoto alloc] init];
                photo.asset = result;
                [allItems addObject:photo];
            } else if ([type isEqualToString:ALAssetTypeVideo]) {
                BLMediaItem *video = [[BLVideo alloc] init];
                video.asset = result;
                [allItems addObject:video];
            }*/
            if ([type isEqualToString:ALAssetTypePhoto])
            {
                if ([chooseMediaType isEqualToString:@"TypePhoto"])
                {
                    BLMediaItem *photo = [[BLPhoto alloc] init];
                    photo.asset = result;
                    [allItems addObject:photo];
                }
            }
            
            else if ([type isEqualToString:ALAssetTypeVideo])
            {
                if ([chooseMediaType isEqualToString:@"TypeVideo"])
                {
                    
                    BLMediaItem *video = [[BLVideo alloc] init];
                    video.asset = result;
                    [allItems addObject:video];
                    
                }
            }

        } else {
            
            if (weakSelfRef.isCaptured) {
                weakSelfRef.isCaptured                                  = NO;
                if (!weakSelfRef.isChoosedVideo || !weakSelfRef.isCapturedVideo) {
                    int max                                                 = NumberOfItemsCanChooseWithoutPurchase;
                    if (XGET_BOOL(PurchaseAppProductIdentifier)) {
                        max                                                     = NumberOfItemsCanChooseWithPurchase;
                    }
                    if (AppDel.selectedItems.count < max) {
                        BLMediaItem *item= [allItems lastObject];
                        if ([item isVideo]) {
                            [AppDel.selectedItems insertObject:item atIndex:0];
                        } else {
                            [AppDel.selectedItems addObject:item];
                        }
                    }
                }
                
                weakSelfRef.isCapturedVideo                             = NO;
            }
            
            weakSelfRef.allItems                                    = allItems;
            [weakSelfRef allPhotosCollected];
            [weakSelfRef showPhotoChoosed];
        }
    };
    
    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil && [group numberOfAssets] > 0) {
            NSString *albumName                                     = [group valueForProperty:ALAssetsGroupPropertyName];
            if ([group numberOfAssets] > 0) {
                [arrayGroup addObject:albumName];
                [arrayAlbumAvatar addObject:[UIImage imageWithCGImage:[group posterImage]]];
                [arrayNumberOfAb addObject:[NSString stringWithFormat:@"%ld",(long)[group numberOfAssets]]];
            }
            
            if ([albumName compare:albumChoosed] == NSOrderedSame) {
                [group enumerateAssetsUsingBlock:assetEnumerator];
            }
            
        } else {
            weakSelfRef.arrayGroup                                  = arrayGroup;
            weakSelfRef.arrayAlbumAvatar                            = arrayAlbumAvatar;
            weakSelfRef.arrayNumberOfAb                             = arrayNumberOfAb;
        }
    };
    
    [self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll
                                      usingBlock:assetGroupEnumerator
                                    failureBlock:^(NSError *error) {
                                        if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                                            NSLog(@"user denied access, code: %ld",(long)error.code);
                                        }else{
                                            NSLog(@"Other error code: %ld",(long)error.code);
                                        }
                                        
                                    }];
    
    
}

-(void)allPhotosCollected
{
    self.shouldPreventDisplayCellAnimation=YES;
    [self.photoCollectionView reloadData];
}

-(void)getPhotoOfAlbumsWithAlbumNameChoosed:(NSString *)albumChoosed
{
    
    [self.btChooseAlbum setTitle:[NSString stringWithFormat:@"%@ ▼",[albumChoosed uppercaseString]] forState:UIControlStateNormal];
    
    self.isChoosedCameraRoll = [albumChoosed isEqualToString:@"Camera Roll"];
    
    NSMutableArray *arrayGroup = [[NSMutableArray alloc] init];
    NSMutableArray *arrayAlbumAvatar = [[NSMutableArray alloc] init];
    NSMutableArray *arrayNumberOfAb = [[NSMutableArray alloc] init];
    NSMutableArray *allItems = [[NSMutableArray alloc] init];
    /*
     PHFetchOptions *options                                 = [[PHFetchOptions alloc] init];
     options.sortDescriptors                                 = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
     PHFetchResult *assetsFetchResults                       = [PHAsset fetchAssetsWithOptions:options] ;
     */
    //PHAsset *asset = assetsFetchResults[indexPath.item];
    /*
     void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
     
     if (result != nil) {
     NSString *type                                          = [result valueForProperty:ALAssetPropertyType];
     
     if ([type isEqualToString:ALAssetTypePhoto]) {
     BLMediaItem *photo                                      = [[BLPhoto alloc] init];
     photo.asset                                             = result;
     NSLog(@"result%@",result);
     [allItems addObject:photo];
     } else if ([type isEqualToString:ALAssetTypeVideo]) {
     BLMediaItem *video                                      = [[BLVideo alloc] init];
     video.asset                                             = result;
     [allItems addObject:video];
     }
     } else {
     weakSelfRef.allItems                                    = allItems;
     [weakSelfRef allPhotosCollected];
     weakSelfRef.choosedAlbumName                            = albumChoosed;
     }
     };
     */
    /*
     void (^assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
     if(group != nil && [group numberOfAssets] > 0) {
     NSString *albumName                                     = [group valueForProperty:ALAssetsGroupPropertyName];
     if ([group numberOfAssets] > 0) {
     [arrayGroup addObject:albumName];
     [arrayAlbumAvatar addObject:[UIImage imageWithCGImage:[group posterImage]]];
     [arrayNumberOfAb addObject:[NSString stringWithFormat:@"%ld", (long)[group numberOfAssets]]];
     }
     
     if ([albumName isEqualToString:albumChoosed]) {
     [group enumerateAssetsUsingBlock:assetEnumerator];
     }
     
     } else {
     weakSelfRef.arrayNumberOfAb                             = arrayNumberOfAb;
     weakSelfRef.arrayGroup                                  = arrayGroup;
     weakSelfRef.arrayAlbumAvatar                            = arrayAlbumAvatar;
     }
     };
     */
    
    __weak typeof (&*self) weakSelfRef                      = self;

    
    dispatch_group_t loadingGroup                           = dispatch_group_create();
    //NSMutableArray * assets = [[NSMutableArray array] init];
    NSMutableArray * albums = [[NSMutableArray array] init];
    
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *)  = ^(ALAsset *asset, NSUInteger index, BOOL *stop)
    {
        NSLog(@"iteration started with Index :%lu",index);
        
        if(index != NSNotFound) {
            NSLog(@"get into If condition");
            
            NSString *type= [asset valueForProperty:ALAssetPropertyType];
            
            /*if ([type isEqualToString:ALAssetTypePhoto])
            {
                
                BLMediaItem *photo = [[BLPhoto alloc] init];
                photo.asset= asset;
                [allItems addObject:photo];
            }
            else if ([type isEqualToString:ALAssetTypeVideo])
            {
                
                BLMediaItem *video= [[BLVideo alloc] init];
                
                video.asset= asset;
                [allItems addObject:video];
            }
            */
            
           
                if ([type isEqualToString:ALAssetTypePhoto])
                {
                    if ([chooseMediaType isEqualToString:@"TypePhoto"])
                    {
                    BLMediaItem *photo = [[BLPhoto alloc] init];
                    photo.asset = asset;
                    [allItems addObject:photo];
                    }
                }
                
             else if ([type isEqualToString:ALAssetTypeVideo])
                  {
                    if ([chooseMediaType isEqualToString:@"TypeVideo"])
                      {
                          
                        BLMediaItem *video = [[BLVideo alloc] init];
                        video.asset = asset;
                        [allItems addObject:video];
                          
                    }
                }

                
            dispatch_async(dispatch_get_main_queue(), ^{ });
        }
        
        else {
            NSLog(@"get into else condition");
            
            weakSelfRef.allItems= allItems;
            [weakSelfRef allPhotosCollected];
            
            weakSelfRef.choosedAlbumName= albumChoosed;
            
            dispatch_group_leave(loadingGroup);
        }
    };
    
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *)   = ^(ALAssetsGroup *group, BOOL *stop) {
        
        if(group != nil)
        {
            if ([group numberOfAssets] > 0)
            {
                [arrayGroup addObject:@{@"name":[group valueForProperty:ALAssetsGroupPropertyName],@"Persistentid":[group valueForProperty:ALAssetsGroupPropertyPersistentID]}];

                [arrayAlbumAvatar addObject:[UIImage imageWithCGImage:[group posterImage]]];
                [arrayNumberOfAb addObject:[NSString stringWithFormat:@"%ld", (long)[group numberOfAssets]]];
            }
            
            if (self.isChoosedCameraRoll && [self.ShowPhotosForSelectedID isEqualToString:@"Camera roll selected"]) {
                
                if ([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString:@"Camera Roll"]) {
                    self.ShowPhotosForSelectedID = [group valueForProperty:ALAssetsGroupPropertyPersistentID] ;
                }else{
                    return ;
                }
            }else{
                if (![self.ShowPhotosForSelectedID isEqualToString:[group valueForProperty:ALAssetsGroupPropertyPersistentID]] ) {
                    return ;
                }
            }
            [albums addObject: group];
            
            
            
            
            
        }
        else {
            
            for (ALAssetsGroup * album in albums) {
                dispatch_group_enter(loadingGroup);
                [album enumerateAssetsUsingBlock: assetEnumerator];
            }
            
            dispatch_group_notify(loadingGroup, dispatch_get_main_queue(), ^{
                NSLog(@"DONE: ALAsset array contains %lu elements", (unsigned long)[arrayGroup count]);
                
                
                weakSelfRef.arrayNumberOfAb= arrayNumberOfAb;
                weakSelfRef.arrayGroup= arrayGroup;
                weakSelfRef.arrayAlbumAvatar= arrayAlbumAvatar;
                
            });
        }
    };
    
    
    //    if (!self.assetsLibrary) {
    //        self.assetsLibrary = [[ALAssetsLibrary alloc] init];
    //
    //    }
    
    [self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos | ALAssetsGroupAlbum
                                      usingBlock:assetGroupEnumerator
                                    failureBlock: ^(NSError *error) {
                                        NSLog(@"library enumeration Failed.");
                                    }];
    
    
}
/*
 initial sinnepet

 {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [self.btChooseAlbum setTitle:[NSString stringWithFormat:@"%@ ▼",[albumChoosed uppercaseString]] forState:UIControlStateNormal];

    self.isChoosedCameraRoll                                = [albumChoosed isEqualToString:@"Camera Roll"];

    NSMutableArray *arrayGroup                              = [NSMutableArray array];
    NSMutableArray *arrayAlbumAvatar                        = [NSMutableArray array];
    NSMutableArray *arrayNumberOfAb                         = [NSMutableArray array];
    NSMutableArray *allItems                                = [NSMutableArray array];

    __weak typeof (&*self) weakSelfRef                      = self;

    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (result != nil) {
    NSString *type                                          = [result valueForProperty:ALAssetPropertyType];

            if ([type isEqualToString:ALAssetTypePhoto]) {
    BLMediaItem *photo                                      = [[BLPhoto alloc] init];
    photo.asset                                             = result;
                [allItems addObject:photo];
            } else if ([type isEqualToString:ALAssetTypeVideo]) {
    BLMediaItem *video                                      = [[BLVideo alloc] init];
    video.asset                                             = result;
                [allItems addObject:video];
            }
        } else {
    weakSelfRef.allItems                                    = allItems;
            [weakSelfRef allPhotosCollected];
    weakSelfRef.choosedAlbumName                            = albumChoosed;
        }
    };

    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil && [group numberOfAssets] > 0) {
    NSString *albumName                                     = [group valueForProperty:ALAssetsGroupPropertyName];
            if ([group numberOfAssets] > 0) {
                [arrayGroup addObject:albumName];
                [arrayAlbumAvatar addObject:[UIImage imageWithCGImage:[group posterImage]]];
                [arrayNumberOfAb addObject:[NSString stringWithFormat:@"%ld", (long)[group numberOfAssets]]];
            }

            if ([albumName isEqualToString:albumChoosed]) {
                [group enumerateAssetsUsingBlock:assetEnumerator];
            }

        } else {
    weakSelfRef.arrayNumberOfAb                             = arrayNumberOfAb;
    weakSelfRef.arrayGroup                                  = arrayGroup;
    weakSelfRef.arrayAlbumAvatar                            = arrayAlbumAvatar;
        }
    };

    [self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll
                                      usingBlock:assetGroupEnumerator
                                    failureBlock:^(NSError *error) {
                                        MyLog(@"Error: %@", error.localizedDescription);
                                    }];


}*/



#pragma mark - UITableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self.arrayGroup objectAtIndex:indexPath.row] valueForKey:@"name"]) {
        if ([self haveMoreThanOneAlbumAndChoosedAlbumName:[[self.arrayGroup objectAtIndex:indexPath.row] valueForKey:@"name"]]) {
            return 0;
        } else {
            if ((indexPath.row != 0) && (indexPath.row != self.arrayGroup.count -1)) {
                return 72;
            } else {
                return 78;
            }
        }
        
    }else{
        return 0;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayGroup.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *selectAlbumCell                        = @"BLSelectAlbumCell";
    BLSelectAlbumCell *cell                                 = [tableView dequeueReusableCellWithIdentifier:selectAlbumCell];
    if (!cell) {
        NSArray *nib                                            = [[NSBundle mainBundle] loadNibNamed:@"BLSelectAlbumCell" owner:self options:nil];
        cell                                                    = [nib objectAtIndex:0];
    }
    
    if (indexPath.row == 0) {
        [cell.imgAlbum setFrame:CGRectMake(14, 12, 60, 60)];
        [cell.lbAlbumName setFrame:CGRectMake(91, 26, 177, 21)];
        [cell.lbNumberOfAlbum setFrame:CGRectMake(91, 45, 177, 21)];
    }
    
    cell.lbAlbumName.text                                   = [[[self.arrayGroup objectAtIndex:indexPath.row]valueForKey:@"name" ] uppercaseString];
    cell.lbNumberOfAlbum.text                               = [NSString stringWithFormat:@"%@ Photos", [self.arrayNumberOfAb objectAtIndex:indexPath.row]];
    
    [cell.imgAlbum setImage:[self.arrayAlbumAvatar objectAtIndex:indexPath.row]];
    cell.imgAlbum.layer.cornerRadius                        = 7.0;
    cell.imgAlbum.layer.masksToBounds                       = YES;
    
    [self.tbAlbums setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if ([self haveMoreThanOneAlbumAndChoosedAlbumName:[[self.arrayGroup objectAtIndex:indexPath.row]valueForKey:@"name" ]]) {
        cell.hidden                                             = YES;
        cell.userInteractionEnabled                             = NO;
    } else {
        cell.hidden                                             = NO;
        cell.userInteractionEnabled                             = YES;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *albumNameChoosed = [[self.arrayGroup objectAtIndex:indexPath.row]valueForKey:@"name" ];
    
    self.ShowPhotosForSelectedID =  [[self.arrayGroup objectAtIndex:indexPath.row]valueForKey:@"Persistentid" ];

    [self.middleView setHidden:YES];
    self.isChoosedAlbum                                     = NO;
    [self.tbAlbums setHidden:YES];

    [self getPhotoOfAlbumsWithAlbumNameChoosed:albumNameChoosed];
    
    
    
    
}


#pragma mark - BLCameraDelegate

-(void)isCapturedVideo:(BOOL)isCapturedVideo
{
    _isCapturedVideo = isCapturedVideo;
    _isCaptured = YES;
}


#pragma mark - Image Choosed

- (void)customImageChoosed
{
    self.middleView.layer.opacity                           = 0.5f;
    [self.middleView setHidden:YES];
    [self.middleView setBackgroundColor:[UIColor blackColor]];
    [self.viewImageChoosed setBackgroundColor:[UIColor colorWithRed:54.0/255 green:54.0/255 blue:54.0/255 alpha:1.0]];

    [self.viewChoosedFirst  setHidden:YES];
    [self.viewChoosedSecond setHidden:YES];
    [self.viewChoosedThird  setHidden:YES];
    [self.viewChoosedFourth setHidden:YES];
    [self.viewChoosedFifth  setHidden:YES];

    [self.img1VideoSign setHidden:YES];
    [self.img2VideoSign setHidden:YES];
    [self.img3VideoSign setHidden:YES];
    [self.img4VideoSign setHidden:YES];
    [self.img5VideoSign setHidden:YES];

    self.imgChoosedFirst.layer.cornerRadius                 = 4.0f;
    self.imgChoosedSecond.layer.cornerRadius                = 4.0f;
    self.imgChoosedThird.layer.cornerRadius                 = 4.0f;
    self.imgChoosedFourth.layer.cornerRadius                = 4.0f;
    self.imgChoosedFifth.layer.cornerRadius                 = 4.0f;

    self.imgChoosedFirst.layer.masksToBounds                = YES;
    self.imgChoosedSecond.layer.masksToBounds               = YES;
    self.imgChoosedThird.layer.masksToBounds                = YES;
    self.imgChoosedFourth.layer.masksToBounds               = YES;
    self.imgChoosedFifth.layer.masksToBounds                = YES;

    [self.btDone setHidden:YES];
}

-(void)showPhotoChoosed
{
    NSInteger numberOfItems                                 = AppDel.selectedItems.count;

    if (self.viewImageChoosed.hidden && numberOfItems) {
    self.viewImageChoosed.hidden                            = NO;
        [UIView animateWithDuration:0.3f animations:^{
    CGRect frame                                            = self.viewAdAndSelectedItems.frame;
    frame.origin.y                                          -= self.viewImageChoosed.frame.size.height;
            [self.viewAdAndSelectedItems setFrame:frame];
        } completion:^(BOOL finished) {
    CGRect frame                                            = self.photoCollectionView.frame;
    frame.size.height                                       -= self.viewImageChoosed.frame.size.height;
            [self.photoCollectionView setFrame:frame];
        }];
    }

    if (numberOfItems == 0) {
        [self.viewChoosedFirst  setHidden:YES];
        [self.viewChoosedSecond setHidden:YES];
        [self.viewChoosedThird  setHidden:YES];
        [self.viewChoosedFourth setHidden:YES];
        [self.viewChoosedFifth  setHidden:YES];

    } else if (numberOfItems == 1){
    BLMediaItem *item1                                      = (BLMediaItem *)[AppDel.selectedItems objectAtIndex:0];
        [self.imgChoosedFirst setImage:item1.thumbnail];
        if ([item1 isVideo]) {
            [self.img1VideoSign setHidden:NO];
        } else {
            [self.img1VideoSign setHidden:YES];
        }

        [self.viewChoosedFirst setHidden:NO];
        [self.viewChoosedSecond setHidden:YES];
        [self.viewChoosedThird setHidden:YES];
        [self.viewChoosedFourth setHidden:YES];
        [self.viewChoosedFifth setHidden:YES];
    } else if (numberOfItems == 2) {
    BLMediaItem *item1                                      = (BLMediaItem *)[AppDel.selectedItems objectAtIndex:0];
        [self.imgChoosedFirst setImage:item1.thumbnail];

    BLMediaItem *item2                                      = [AppDel.selectedItems objectAtIndex:1];
        [self.imgChoosedSecond setImage:item2.thumbnail];

        if ([item1 isVideo]) {
            [self.img1VideoSign setHidden:NO];
        } else {
            [self.img1VideoSign setHidden:YES];
        }
        if ([item2 isVideo]) {
            [self.img2VideoSign setHidden:NO];
        } else {
            [self.img2VideoSign setHidden:YES];
        }

        [self.viewChoosedFirst setHidden:NO];
        [self.viewChoosedSecond setHidden:NO];
        [self.viewChoosedThird setHidden:YES];
        [self.viewChoosedFourth setHidden:YES];
        [self.viewChoosedFifth setHidden:YES];
    } else if (numberOfItems == 3) {
    BLMediaItem *item1                                      = (BLMediaItem *)[AppDel.selectedItems objectAtIndex:0];
        [self.imgChoosedFirst setImage:item1.thumbnail];

    BLMediaItem *item2                                      = [AppDel.selectedItems objectAtIndex:1];
        [self.imgChoosedSecond setImage:item2.thumbnail];

    BLMediaItem *item3                                      = [AppDel.selectedItems objectAtIndex:2];
        [self.imgChoosedThird setImage:item3.thumbnail];

        if ([item1 isVideo]) {
            [self.img1VideoSign setHidden:NO];
        } else {
            [self.img1VideoSign setHidden:YES];
        }
        if ([item2 isVideo]) {
            [self.img2VideoSign setHidden:NO];
        } else {
            [self.img2VideoSign setHidden:YES];
        }
        if ([item3 isVideo]) {
            [self.img3VideoSign setHidden:NO];
        } else {
            [self.img3VideoSign setHidden:YES];
        }

        [self.viewChoosedFirst setHidden:NO];
        [self.viewChoosedSecond setHidden:NO];
        [self.viewChoosedThird setHidden:NO];
        [self.viewChoosedFourth setHidden:YES];
        [self.viewChoosedFifth setHidden:YES];
    } else if (numberOfItems == 4) {
    BLMediaItem *item1                                      = (BLMediaItem *)[AppDel.selectedItems objectAtIndex:0];
        [self.imgChoosedFirst setImage:item1.thumbnail];

    BLMediaItem *item2                                      = [AppDel.selectedItems objectAtIndex:1];
        [self.imgChoosedSecond setImage:item2.thumbnail];

    BLMediaItem *item3                                      = [AppDel.selectedItems objectAtIndex:2];
        [self.imgChoosedThird setImage:item3.thumbnail];

    BLMediaItem *item4                                      = [AppDel.selectedItems objectAtIndex:3];
        [self.imgChoosedFourth setImage:item4.thumbnail];

        if ([item1 isVideo]) {
            [self.img1VideoSign setHidden:NO];
        } else {
            [self.img1VideoSign setHidden:YES];
        }
        if ([item2 isVideo]) {
            [self.img2VideoSign setHidden:NO];
        } else {
            [self.img2VideoSign setHidden:YES];
        }
        if ([item3 isVideo]) {
            [self.img3VideoSign setHidden:NO];
        } else {
            [self.img3VideoSign setHidden:YES];
        }
        if ([item4 isVideo]) {
            [self.img4VideoSign setHidden:NO];
        } else {
            [self.img4VideoSign setHidden:YES];
        }

        [self.viewChoosedFirst setHidden:NO];
        [self.viewChoosedSecond setHidden:NO];
        [self.viewChoosedThird setHidden:NO];
        [self.viewChoosedFourth setHidden:NO];
        [self.viewChoosedFifth setHidden:YES];
    } else {
    BLMediaItem *item1                                      = (BLMediaItem *)[AppDel.selectedItems objectAtIndex:0];
        [self.imgChoosedFirst setImage:item1.thumbnail];

    BLMediaItem *item2                                      = [AppDel.selectedItems objectAtIndex:1];
        [self.imgChoosedSecond setImage:item2.thumbnail];

    BLMediaItem *item3                                      = [AppDel.selectedItems objectAtIndex:2];
        [self.imgChoosedThird setImage:item3.thumbnail];

    BLMediaItem *item4                                      = [AppDel.selectedItems objectAtIndex:3];
        [self.imgChoosedFourth setImage:item4.thumbnail];

    BLMediaItem *item5                                      = [AppDel.selectedItems objectAtIndex:4];
        [self.imgChoosedFifth setImage:item5.thumbnail];

        if ([item1 isVideo]) {
            [self.img1VideoSign setHidden:NO];
        } else {
            [self.img1VideoSign setHidden:YES];
        }
        if ([item2 isVideo]) {
            [self.img2VideoSign setHidden:NO];
        } else {
            [self.img2VideoSign setHidden:YES];
        }
        if ([item3 isVideo]) {
            [self.img3VideoSign setHidden:NO];
        } else {
            [self.img3VideoSign setHidden:YES];
        }
        if ([item4 isVideo]) {
            [self.img4VideoSign setHidden:NO];
        } else {
            [self.img4VideoSign setHidden:YES];
        }
        if ([item5 isVideo]) {
            [self.img5VideoSign setHidden:NO];
        } else {
            [self.img5VideoSign setHidden:YES];
        }


        [self.viewChoosedFirst setHidden:NO];
        [self.viewChoosedSecond setHidden:NO];
        [self.viewChoosedThird setHidden:NO];
        [self.viewChoosedFourth setHidden:NO];
        [self.viewChoosedFifth setHidden:NO];
    }

    if (AppDel.selectedItems.count > 1) {
    self.lbPhotosSelected.text                              = [NSString stringWithFormat:@"%ld PHOTOS SELECTED", (unsigned long)AppDel.selectedItems.count];
    } else {
    self.lbPhotosSelected.text                              = [NSString stringWithFormat:@"%ld PHOTO SELECTED", (unsigned long)AppDel.selectedItems.count];
    }

    if (AppDel.selectedItems.count < 1)
    {
        [self.btDone setHidden:YES];
    } else {
        [self.btDone setHidden:NO];
    }
}

-(BOOL)haveMoreThanOneAlbumAndChoosedAlbumName:(NSString*)name
{
    return self.arrayGroup.count > 1 && [self.choosedAlbumName.lowercaseString isEqualToString:name.lowercaseString];
}


#pragma mark - IBAction

- (IBAction)btChooseAlbumPress:(id)sender
{
    self.isChoosedAlbum                                     = !self.isChoosedAlbum;
    static float oldMiddleViewOpacity                       = 0;
    if (oldMiddleViewOpacity == 0) {
    oldMiddleViewOpacity                                    = self.middleView.alpha;
    }

    if (self.isChoosedAlbum) { //Show
    self.tbAlbums.transform                                 = CGAffineTransformScale(self.tbAlbums.transform, 0.1, 0.1);
        [self.tbAlbums setHidden:NO];
        [self.middleView setHidden:NO];
        [self.tbAlbums reloadData];
    self.middleView.alpha                                   = 0;
        [UIView animateWithDuration:0.3f animations:^{
    self.tbAlbums.transform                                 = CGAffineTransformIdentity;
    self.middleView.alpha                                   = oldMiddleViewOpacity;
        }];
    } else { //Hide
        [UIView animateWithDuration:0.3f animations:^{
    self.middleView.alpha                                   = 0;
    self.tbAlbums.transform                                 = CGAffineTransformScale(self.tbAlbums.transform, 0.1, 0.1);
        } completion:^(BOOL finished) {
            [self.tbAlbums setHidden:YES];
            [self.middleView setHidden:YES];
    self.middleView.alpha                                   = oldMiddleViewOpacity;
    self.tbAlbums.transform                                 = CGAffineTransformIdentity;
        }];
    }
}

- (IBAction)btDeleteAllPress:(id)sender
{
    [AppDel.selectedItems removeAllObjects];
    [self showPhotoChoosed];
    self.lbPhotosSelected.text    = @"0 PHOTO SELECTED";
    self.shouldPreventDisplayCellAnimation=YES;
    [self.photoCollectionView reloadData];
}

- (IBAction)btDeleteImgFirstPress:(id)sender
{
    [AppDel.selectedItems removeObjectAtIndex:0];
    [self showPhotoChoosed];
    self.shouldPreventDisplayCellAnimation=YES;
    [self.photoCollectionView reloadData];
}

- (IBAction)btDeleteImgSecondPress:(id)sender
{
    [AppDel.selectedItems removeObjectAtIndex:1];
    [self showPhotoChoosed];
    self.shouldPreventDisplayCellAnimation=YES;
    [self.photoCollectionView reloadData];
}

- (IBAction)btDeleteImgThirdPress:(id)sender
{
    [AppDel.selectedItems removeObjectAtIndex:2];
    [self showPhotoChoosed];
    self.shouldPreventDisplayCellAnimation=YES;
    [self.photoCollectionView reloadData];
}

- (IBAction)btDeleteImgFourthPress:(id)sender
{
    [AppDel.selectedItems removeObjectAtIndex:3];
    [self showPhotoChoosed];
    self.shouldPreventDisplayCellAnimation=YES;
    [self.photoCollectionView reloadData];
}

- (IBAction)btDeleteImgFifthPress:(id)sender
{
    [AppDel.selectedItems removeObjectAtIndex:4];
    [self showPhotoChoosed];
    self.shouldPreventDisplayCellAnimation=YES;
    [self.photoCollectionView reloadData];
}

-(void)updateLibrary:(NSNotification*)noti
{
    if (self.choosedAlbumName) {
        [self getPhotoOfAlbumsWithAlbumNameChoosed:self.choosedAlbumName];
    }
}



- (IBAction)btDoneClicked:(id)sender {

    if (AppDel.selectedItems.count > 1)
    {

        

        //Clear selections
      //  [AppDel.selectedItems removeAllObjects];
        [self showPhotoChoosed];
        self.shouldPreventDisplayCellAnimation=YES;
        [self.photoCollectionView reloadData];
    }
    else {
        
        NSLog(@"Please select more than 2 items");
    }
    
    
    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:chooseMediaType,@"ChooseMediaType",AppDel.selectedItems,@"ArrayImgVid", nil];

    [self selectimageVideoReturnToDept:dic_temp];

}



- (void)selectimageVideoReturnToDept:(NSDictionary*)parameters;
{
    NSLog(@"parameters=%@",parameters);
    
    if ([self.delegate respondsToSelector:@selector(selectimageVideoReturnToDept:)]) {
        [self.delegate selectimageVideoReturnToDept:parameters];
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (void)selectimageVideoFailCaseToDept:(NSDictionary*)parameters
{
    if ([self.delegate respondsToSelector:@selector(selectimageVideoFailCaseToDept:)]) {
        [self.delegate selectimageVideoFailCaseToDept:parameters];
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

-(IBAction)backBtnAction:(id)sender
{
    
    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:chooseMediaType,@"ChooseMediaType",@"FAIL",@"ArrayImgVid", nil];
    
    [self selectimageVideoFailCaseToDept:dic_temp];
    
   // [self dismissViewControllerAnimated:NO completion:nil];
}
-(IBAction)cancelBtnAction:(id)sender
{
    //[self dismissViewControllerAnimated:NO completion:nil];
    
    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:chooseMediaType,@"ChooseMediaType",@"FAIL",@"ArrayImgVid", nil];
    
    [self selectimageVideoFailCaseToDept:dic_temp];
}



-(BOOL)isChoosedVideo
{
    for (BLMediaItem *item in AppDel.selectedItems) {
        if ([item isVideo]) {
            return YES;
        }
    }
    return NO;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
