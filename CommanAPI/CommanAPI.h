//
//  CommanAPI.h
//  UMANG
//
//  Created by admin on 08/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommanAPI : NSObject
{
    
}
+ (CommanAPI*)sharedSingleton;
-(void)hitwebService_infoScreen;
-(void)callStateAPI;
-(void)hitInitAPI;

@end
