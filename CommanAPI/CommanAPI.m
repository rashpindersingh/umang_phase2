//
//  CommanAPI.m
//  UMANG
//
//  Created by admin on 08/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "CommanAPI.h"
#import "SharedManager.h"

#import "UMAPIManager.h"
#import <Crashlytics/Crashlytics.h>
#import "StateList.h"
#import "TabBarVC.h"

static CommanAPI *sharedCommonApi = nil;

@interface CommanAPI()
{
    SharedManager * singleton;
    StateList *obj;

}

@end



@implementation CommanAPI
+ (CommanAPI*)sharedSingleton
{
    @synchronized(self) {
        if (sharedCommonApi == nil)
        {
            sharedCommonApi = [[super allocWithZone:NULL] init];
           
        }
        return sharedCommonApi;
    }
}

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitwebService_infoScreen
{
    NSString *lastFlagScreenTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFlagScreenTime"];
   
        if ([lastFlagScreenTime length]==0||lastFlagScreenTime==nil||[lastFlagScreenTime isEqualToString:@""])
        {
            lastFlagScreenTime=@"";
        }
   
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:lastFlagScreenTime forKey:@"ldate"];
    [dictBody setObject:@"" forKey:@"st"];
    [dictBody setObject:@"" forKey:@"mno"];
    [dictBody setObject:@"" forKey:@"tkn"]; //tkn number blank as new user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    NSInteger size;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        size =  fDeviceWidth * 2;
    }else {
        size =  fDeviceWidth <380 ? fDeviceWidth * 2: fDeviceWidth * 3;
    }
    
    [dictBody setObject:[NSString stringWithFormat:@"%ld", (long)size] forKey:@"size"];
    
   // NSLog(@"TOKEN :::: %@",singleton.user_tkn);
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INFOSCREEN withBody:dictBody andTag:TAG_REQUEST_INFO_SCREEN  completionHandler:^(id response, NSError *error,REQUEST_TAG tag) {
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            NSString *rd=[response valueForKey:@"rd"];

            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ value of rd=%@",rc,rs,tkn,rd);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
                dispatch_async(serialQueue, ^{
                    [self insertToDBTask:response]; //insert into DB
                });
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"CommanInfoFlagAPI" forKey:@"CommanAPI"];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"LOADINFOFLAGDATA" object:nil userInfo:userInfo];
            
        
            
            
        }
        
    }];
    
    
    
}



-(void)insertToDBTask:(NSDictionary*)response
{
  
        NSLog(@"response =%@",response);
        singleton = [SharedManager sharedSingleton];

        //------- Code to insert data in database with service list-------
        NSMutableArray *addServiceList=[[NSMutableArray alloc]init];
        addServiceList=[[[response valueForKey:@"pd"] valueForKey:@"addServiceList"] mutableCopy];
        
        
        //----------- Update service list--------
        
        NSMutableArray *updateServiceList=[[NSMutableArray alloc]init];
        updateServiceList=[[[response valueForKey:@"pd"] valueForKey:@"updateServiceList"] mutableCopy];
        
        //----------- Delete service list--------

        NSMutableArray *deleteServiceList=[[NSMutableArray alloc]init];
        deleteServiceList=[[[response valueForKey:@"pd"] valueForKey:@"deleteServiceList"] mutableCopy];
        
        
  
        //------- listHeroSpace insert it into database-------
         NSMutableArray * listHeroSpace=[[NSMutableArray alloc]init];
         listHeroSpace=[[[response valueForKey:@"pd"] valueForKey:@"listHeroSpace"] mutableCopy];
        
         if ([listHeroSpace count]>0)
         {
          [singleton.dbManager deleteFlagBannerData]; //delete old database
         }
  
    
        //------- lastFetchDate-------

        NSString* lastFetchDate=[NSString stringWithFormat:@"%@",[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"]];
        
        //------- shareText-------
        NSString* shareText=[[response valueForKey:@"pd"] valueForKey:@"shareText"];
        singleton.shareText=[NSString stringWithFormat:@"%@",shareText];
        NSLog(@"shareText=%@",shareText);
        //------- emailSupport-------
        NSString*  emailSupport=[[response valueForKey:@"pd"] valueForKey:@"emailSupport"];
        singleton.emailSupport=[NSString stringWithFormat:@"%@",emailSupport];

        NSLog(@"emailSupport=%@",emailSupport);

        //------- phoneSupport-------
        NSString*  phoneSupport=[[response valueForKey:@"pd"] valueForKey:@"phoneSupport"];
         singleton.phoneSupport=[NSString stringWithFormat:@"%@",phoneSupport];

        NSLog(@"phoneSupport=%@",phoneSupport);

        //------- dirsershow-------

        BOOL serviceDirectory = [[[response valueForKey:@"pd"] valueForKey:@"dirsershow"] boolValue];
        [[NSUserDefaults standardUserDefaults] setBool:serviceDirectory forKey:@"Enable_ServiceDir"];
        
        
        
        //------------------------- Encrypt Value to save Last Flag Screen Time ------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:lastFetchDate withKey:@"lastFlagScreenTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //------------------------- Encrypt Value---------------------------------------------------------
     
        dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
    
 
    dispatch_async(serialQueue, ^{
        if(listHeroSpace && [listHeroSpace count]>=1)
        {
            [self addlistHeroSpaceToDBTask:listHeroSpace];

            
        }
        
        dispatch_async(serialQueue, ^{
            if(addServiceList && [addServiceList count]>=1)
            {
                NSLog(@"addServiceToDBTask 1");
                [self addServiceToDBTask:addServiceList]; //insert
                
            }
            dispatch_async(serialQueue, ^{
                if(updateServiceList && [updateServiceList count]>=1)
                {
                    NSLog(@"updateServiceToDBTask 2");
                    [self updateServiceToDBTask:updateServiceList]; //update
                    
                }                dispatch_async(serialQueue, ^{
                    if(deleteServiceList && [deleteServiceList count]>=1)
                    {
                        NSLog(@"deleteServiceToDBTask 3");
                        [self deleteServiceToDBTask:deleteServiceList];
                        
                    }
      dispatch_async(dispatch_get_main_queue(), ^{
                                        //code to be executed on the main thread when background task is finished
                                        NSLog(@"fetchDatafromDB 9");
                                        //add Fire notifier here
          
          NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"CommanInfoFlagAPI" forKey:@"CommanAPI"];
          [[NSNotificationCenter defaultCenter] postNotificationName: @"LOADINFOFLAGDATA" object:nil userInfo:userInfo];
          
                                    });
                });
            });
        });
       });
        
    }
    
    
   
    



-(void)addServiceToDBTask:(NSMutableArray*)addServiceList
{
 
    for (int i=0; i<[addServiceList count]; i++)
    {

        @try {
            NSString* serviceId =[[addServiceList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* serviceName =[[addServiceList objectAtIndex:i]valueForKey:@"serviceName"];
            NSString* description =[[addServiceList objectAtIndex:i]valueForKey:@"description"];
            NSString* image=[[addServiceList objectAtIndex:i]valueForKey:@"image"];
            NSString* categoryName =[[addServiceList objectAtIndex:i]valueForKey:@"categoryName"];
            NSString* subCategoryName =[[addServiceList objectAtIndex:i]valueForKey:@"subCategoryName"];
            NSString* rating=[[addServiceList objectAtIndex:i]valueForKey:@"rating"];
            NSString* url =[[addServiceList objectAtIndex:i]valueForKey:@"url"];
            NSString* state=[[addServiceList objectAtIndex:i]valueForKey:@"state"];
            NSString* lat =[[addServiceList objectAtIndex:i]valueForKey:@"lat"];
            NSString* log=[[addServiceList objectAtIndex:i]valueForKey:@"log"];
            NSString* serviceIsFav =@"false";
            NSString* serviceIsHidden =@"false";
            NSString* contact =[[addServiceList objectAtIndex:i]valueForKey:@"contact"];
            NSString* serviceisNotifEnabled =@"true";
            NSString* website =[[addServiceList objectAtIndex:i]valueForKey:@"website"];
            
            //------- NEW fields added to be shown--------------
            NSString* deptAddress =[[addServiceList objectAtIndex:i]valueForKey:@"deptAddress"];
            NSString* workingHours =[[addServiceList objectAtIndex:i]valueForKey:@"workingHours"];
            NSString* deptDescription =[[addServiceList objectAtIndex:i]valueForKey:@"deptDescription"];
            NSString* lang =[[addServiceList objectAtIndex:i]valueForKey:@"lang"];
            NSString* email =[[addServiceList objectAtIndex:i]valueForKey:@"email"];
            NSString* popularity =[[addServiceList objectAtIndex:i]valueForKey:@"popularity"];
            NSString* servicecategoryId =[[addServiceList objectAtIndex:i]valueForKey:@"categoryId"];
            
            // otherState = "20|28|31";
            
            NSString* serviceOtherState =[[addServiceList objectAtIndex:i]valueForKey:@"otherState"];
            
            if ([serviceOtherState length]!=0)
            {
                serviceOtherState=[NSString stringWithFormat:@"|%@|",serviceOtherState];
            }
            
            NSLog(@"Insert Other State=%@",serviceOtherState);
            
            if (serviceOtherState == (NSString *)[NSNull null]||[serviceOtherState length]==0)
            {
                serviceOtherState=@"";
            }
            
            
            if (deptAddress == (NSString *)[NSNull null]||[deptAddress length]==0) {
                deptAddress=@"";
            }
            if (workingHours == (NSString *)[NSNull null]||[workingHours length]==0) {
                workingHours=@"";
            }
            if (deptDescription == (NSString *)[NSNull null]||[deptDescription length]==0) {
                deptDescription=@"";
            }
            if (lang == (NSString *)[NSNull null]||[lang length]==0) {
                lang=@"";
            }
            if (email == (NSString *)[NSNull null]||[email length]==0) {
                email=@"";
            }
            
            //-------  fields added to be shown--------------
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId=@"";
            }
            
            if (serviceName == (NSString *)[NSNull null]||[serviceName length]==0) {
                serviceName=@"";
            }
            if (description == (NSString *)[NSNull null]||[description length]==0) {
                description=@"";
            }
            if (image == (NSString *)[NSNull null]||[image length]==0) {
                image=@"";
            }
            if (categoryName == (NSString *)[NSNull null]||[categoryName length]==0) {
                categoryName=@"";
            }
            if (subCategoryName == (NSString *)[NSNull null]||[subCategoryName length]==0) {
                subCategoryName=@"";
            }
            if (rating == (NSString *)[NSNull null]||[rating length]==0) {
                rating=@"";
            }
            if (url == (NSString *)[NSNull null]||[url length]==0) {
                url=@"";
            }
            if (state == (NSString *)[NSNull null]||[state length]==0) {
                state=@"";
            }
            if (lat == (NSString *)[NSNull null]||[lat length]==0) {
                lat=@"";
            }
            if (log == (NSString *)[NSNull null]||[log length]==0) {
                log=@"";
            }
            if (serviceIsFav == (NSString *)[NSNull null]||[serviceIsFav length]==0) {
                serviceIsFav=@"";
            }
            if (serviceIsHidden == (NSString *)[NSNull null]||[serviceIsHidden length]==0) {
                serviceIsHidden=@"";
            }
            if (contact == (NSString *)[NSNull null]||[contact length]==0) {
                contact=@"";
            }
            if (serviceisNotifEnabled == (NSString *)[NSNull null]||[serviceisNotifEnabled length]==0) {
                serviceisNotifEnabled=@"";
            }
            
            if (website == (NSString *)[NSNull null]||[website length]==0) {
                website=@"";
            }
            
            
            
            
            if (servicecategoryId == (NSString *)[NSNull null]||[servicecategoryId length]==0) {
                servicecategoryId=@"";
            }
            
            
            
            NSString* otherwebsite =[[addServiceList objectAtIndex:i]valueForKey:@"otherwebsite"];
            
            
            if (otherwebsite == (NSString *)[NSNull null]||[otherwebsite length]==0) {
                otherwebsite=@"";
            }
            
            NSLog(@"otherwebsite=%@",otherwebsite);
            
            
            NSString* depttype =[[addServiceList objectAtIndex:i]valueForKey:@"depttype"];
            if (depttype == (NSString *)[NSNull null]||[depttype length]==0) {
                depttype=@"";
            }
            NSLog(@"depttype=%@",depttype); // I
            
            
            NSString* multicatid =[[addServiceList objectAtIndex:i]valueForKey:@"multicatid"];
            if (multicatid == (NSString *)[NSNull null]||[multicatid length]==0) {
                multicatid=@"";
            }
            NSLog(@"multicatid=%@",multicatid); // I
            
            NSString* multicatname =[[addServiceList objectAtIndex:i]valueForKey:@"multicatname"];
            if (multicatname == (NSString *)[NSNull null]||[multicatname length]==0) {
                multicatname=@"";
            }
            NSLog(@"multicatname=%@",multicatname); // I
            
       
            [singleton.dbManager insertFlagServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:lang servicedeptAddress:deptAddress
                                    serviceworkingHours:workingHours servicedeptDescription:deptDescription  serviceemail:email popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState otherwebsite:otherwebsite depttype:depttype multicatid:multicatid multicatname:multicatname];
            
            
            
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
        
    }
    
}

//---- [singleton.dbManager open];
-(void)updateServiceToDBTask:(NSMutableArray*)updateServiceList
{
    
    for (int i=0; i<[updateServiceList count]; i++)
    {
        @try {
            
            
            NSString* serviceId =[[updateServiceList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* serviceName =[[updateServiceList objectAtIndex:i]valueForKey:@"serviceName"];
            NSString* description =[[updateServiceList objectAtIndex:i]valueForKey:@"description"];
            NSString* image=[[updateServiceList objectAtIndex:i]valueForKey:@"image"];
            NSString* categoryName =[[updateServiceList objectAtIndex:i]valueForKey:@"categoryName"];
            NSString* subCategoryName =[[updateServiceList objectAtIndex:i]valueForKey:@"subCategoryName"];
            NSString* rating=[[updateServiceList objectAtIndex:i]valueForKey:@"rating"];
            NSString* url =[[updateServiceList objectAtIndex:i]valueForKey:@"url"];
            NSString* state=[[updateServiceList objectAtIndex:i]valueForKey:@"state"];
            NSString* lat =[[updateServiceList objectAtIndex:i]valueForKey:@"lat"];
            NSString* log=[[updateServiceList objectAtIndex:i]valueForKey:@"log"];
            NSString* serviceIsFav =@"false";
            NSString* serviceIsHidden =@"false";
            NSString* contact =[[updateServiceList objectAtIndex:i]valueForKey:@"contact"];
            NSString* serviceisNotifEnabled =@"true";
            NSString* website =[[updateServiceList objectAtIndex:i]valueForKey:@"website"];
            
            //------- NEW fields added to be shown--------------
            NSString* servicedeptAddress =[[updateServiceList objectAtIndex:i]valueForKey:@"deptAddress"];
            NSString* serviceworkingHours =[[updateServiceList objectAtIndex:i]valueForKey:@"workingHours"];
            NSString* servicedeptDescription =[[updateServiceList objectAtIndex:i]valueForKey:@"deptDescription"];
            NSString* servicelang =[[updateServiceList objectAtIndex:i]valueForKey:@"lang"];
            NSString* serviceemail =[[updateServiceList objectAtIndex:i]valueForKey:@"email"];
            NSString* popularity =[[updateServiceList objectAtIndex:i]valueForKey:@"popularity"];
            NSString* servicecategoryId =[[updateServiceList objectAtIndex:i]valueForKey:@"categoryId"];
            
            
            NSString* serviceOtherState =[[updateServiceList objectAtIndex:i]valueForKey:@"otherState"];
            
            if ([serviceOtherState length]!=0)
            {
                serviceOtherState=[NSString stringWithFormat:@"|%@|",serviceOtherState];
            }
            
            NSLog(@"Update Other State=%@",serviceOtherState);
            
            if (serviceOtherState == (NSString *)[NSNull null]||[serviceOtherState length]==0) {
                serviceOtherState=@"";
            }
            
            
            
            
            
            //NSLog(@"insertServicesData");
            //code to be executed in the background
            
            if (servicedeptAddress == (NSString *)[NSNull null]||[servicedeptAddress length]==0) {
                
                servicedeptAddress=@"";
            }
            if (serviceworkingHours == (NSString *)[NSNull null]||[serviceworkingHours length]==0) {
                serviceworkingHours=@"";
            }
            if (servicedeptDescription == (NSString *)[NSNull null]||[servicedeptDescription length]==0) {
                servicedeptDescription=@"";
            }
            if (servicelang == (NSString *)[NSNull null]||[servicelang length]==0) {
                servicelang=@"";
            }
            if (serviceemail == (NSString *)[NSNull null]||[serviceemail length]==0) {
                serviceemail=@"";
            }
            
            //-------  fields added to be shown--------------
            
            
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId=@"";
            }
            
            if (serviceName == (NSString *)[NSNull null]||[serviceName length]==0) {
                serviceName=@"";
            }
            if (description == (NSString *)[NSNull null]||[description length]==0) {
                description=@"";
            }
            if (image == (NSString *)[NSNull null]||[image length]==0) {
                image=@"";
            }
            if (categoryName == (NSString *)[NSNull null]||[categoryName length]==0) {
                categoryName=@"";
            }
            if (subCategoryName == (NSString *)[NSNull null]||[subCategoryName length]==0) {
                subCategoryName=@"";
            }
            if (rating == (NSString *)[NSNull null]||[rating length]==0) {
                rating=@"";
            }
            if (url == (NSString *)[NSNull null]||[url length]==0) {
                url=@"";
            }
            if (state == (NSString *)[NSNull null]||[state length]==0) {
                state=@"";
            }
            if (lat == (NSString *)[NSNull null]||[lat length]==0) {
                lat=@"";
            }
            if (log == (NSString *)[NSNull null]||[log length]==0) {
                log=@"";
            }
            if (serviceIsFav == (NSString *)[NSNull null]||[serviceIsFav length]==0) {
                serviceIsFav=@"";
            }
            if (serviceIsHidden == (NSString *)[NSNull null]||[serviceIsHidden length]==0) {
                serviceIsHidden=@"";
            }
            if (contact == (NSString *)[NSNull null]||[contact length]==0) {
                contact=@"";
            }
            if (serviceisNotifEnabled == (NSString *)[NSNull null]||[serviceisNotifEnabled length]==0) {
                serviceisNotifEnabled=@"";
            }
            
            if (website == (NSString *)[NSNull null]||[website length]==0) {
                website=@"";
            }
            
            if (servicecategoryId == (NSString *)[NSNull null]||[servicecategoryId length]==0) {
                servicecategoryId = @"";
            }
            
            NSString* otherwebsite =[[updateServiceList objectAtIndex:i]valueForKey:@"otherwebsite"];
            
            if (otherwebsite == (NSString *)[NSNull null]||[otherwebsite length]==0) {
                otherwebsite = @"";
            }
            
            
            NSLog(@"otherwebsite ---------> %@",otherwebsite);
            
        
            NSString* depttype =[[updateServiceList objectAtIndex:i]valueForKey:@"depttype"];
            
            
            if (depttype == (NSString *)[NSNull null]||[depttype length]==0) {
                depttype=@"";
            }
            
            NSLog(@"depttype=%@",depttype); // I
            
            
            
            NSString* multicatid =[[updateServiceList objectAtIndex:i]valueForKey:@"multicatid"];
            if (multicatid == (NSString *)[NSNull null]||[multicatid length]==0) {
                multicatid=@"";
            }
            NSLog(@"multicatid=%@",multicatid); // I
            
            NSString* multicatname =[[updateServiceList objectAtIndex:i]valueForKey:@"multicatname"];
            if (multicatname == (NSString *)[NSNull null]||[multicatname length]==0) {
                multicatname=@"";
            }
            NSLog(@"multicatname=%@",multicatname); // I
            
            [singleton.dbManager updateFlagServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:servicelang servicedeptAddress:servicedeptAddress serviceworkingHours:serviceworkingHours servicedeptDescription:servicedeptDescription serviceemail:serviceemail popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState otherwebsite:otherwebsite depttype:depttype multicatid:multicatid multicatname:multicatname ];
            
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
}

-(void)deleteServiceToDBTask:(NSMutableArray*)deleteServiceList
{
    
    
    for (int i=0; i<[deleteServiceList count]; i++)
    {
        NSString* serviceId =[[deleteServiceList objectAtIndex:i]valueForKey:@"serviceId"];
        
        
        //NSLog(@"deleteServiceData");
        
        //code to be executed in the background
        [singleton.dbManager deleteFlagServiceData:serviceId];
    }
}



-(void)addlistHeroSpaceToDBTask:(NSMutableArray*)listHeroSpaceArr
{
    for (int i=0; i<[listHeroSpaceArr count]; i++)
    {
        @try {
            NSString* actionType =[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"actionType"];
            NSString* actionURL =[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"actionURL"];
            NSString* desc =[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"desc"];
            NSString* imageUrl=[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"imageUrl"];
            
            
            NSString* depttype =[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"depttype"];
            
            
          
            
            //NSLog(@"insertBannerHomeData");
            
            NSString* bannerid=[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"bannerid"];
            
            if([bannerid length]==0 || bannerid ==nil|| [bannerid isEqualToString:@"(null)"])
            {
                bannerid=@"";
            }
            if([actionType length]==0 || actionType ==nil|| [actionType isEqualToString:@"(null)"])
            {
                actionType=@"";
            }
            if([actionURL length]==0 || actionURL ==nil|| [actionURL isEqualToString:@"(null)"])
            {
                actionURL=@"";
            }
            if([desc length]==0 || desc ==nil|| [desc isEqualToString:@"(null)"])
            {
                desc=@"";
            }
            if([imageUrl length]==0 || imageUrl ==nil|| [imageUrl isEqualToString:@"(null)"])
            {
                imageUrl=@"";
            }
            
            if (depttype == (NSString *)[NSNull null]||[depttype length]==0) {
                depttype=@"";
            }
            
            NSLog(@"depttype=%@",depttype); // I
            
            
            [singleton.dbManager insertFlagBannerHomeData:imageUrl bannerActionType:actionType bannerActionUrl:actionURL bannerDesc:desc bannerid:bannerid depttype:depttype];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
    
    
    
  //  [self fetchHeroSpaceDatafromDB];
    // add observer here
    
    
    
}


-(void)callStateAPI
{
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    
}


-(void)hitInitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    singleton = [SharedManager sharedSingleton];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            singleton.arr_initResponse=[[NSMutableDictionary alloc]init];
            singleton.arr_initResponse=[response valueForKey:@"pd"];
            NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
            
            [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
            
            NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
            NSString*  infoTab=[singleton.arr_initResponse valueForKey:@"infotab"];

            NSString*  emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
            NSLog(@"value of abbr=%@",abbr);
            
            if ([abbr length]==0)
            {
                abbr=@"";
            }
            
            emblemString = emblemString.length == 0? @"": emblemString;
            
            [[NSUserDefaults standardUserDefaults] setObject:[infoTab capitalizedString] forKey:@"infotab"];

            [[NSUserDefaults standardUserDefaults] setObject:[abbr capitalizedString] forKey:@"ABBR_KEY"];
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //---clear data and last fetch date so Home API can load data
            
            //---clear data and last fetch date so Home API can load data
            
            //------------------------- Encrypt Value------------------------
            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
            // Encrypt
            [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
            
            NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSHTTPCookie *cookie;
            for (cookie in [storage cookies]) {
                
                [storage deleteCookie:cookie];
                
            }
            NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
            [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            //------------------------- Encrypt Value------------------------
            
          /*  if (singleton.user_tkn.length != 0) {
                [self openNextView];
            }*/
            
            
         /*   dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                // get the data here
                [singleton.dbManager deleteBannerHomeData];
                [singleton.dbManager  deleteAllServices];
                [singleton.dbManager  deleteSectionData];
                
                
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   //-------------check condition temp-----
                                   //                                   [[NSUserDefaults standardUserDefaults] setObject:singleton.tabSelected  forKey:@"SELECTED_TAB"];
                                   
                                   [[NSUserDefaults standardUserDefaults]synchronize];
                                   
                                   singleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
                                   
                          
                                   
                               });
            });
            */
            
      
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
           
          
        }
        
    }];
    
}



-(void)openNextView
{
    // Default value for Keep Me Login is True
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSInteger currentIndexOfTab=[singleton getSelectedTabIndex];
    TabBarVC *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    tbc.selectedIndex=currentIndexOfTab;
    tbc.comingFrom = @"login";
    //[self presentViewController:tbc animated:NO completion:nil];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    [delegate.window setRootViewController:tbc];
    [UIView transitionWithView:delegate.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
    
}




@end
