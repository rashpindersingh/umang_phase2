//
//  ChatImagePreviewVC.m
//  Umang
//
//  Created by admin on 17/10/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ChatImagePreviewVC.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"

@interface ChatImagePreviewVC ()

@end

@implementation ChatImagePreviewVC
@synthesize imgKindOf,imagePath;


-(IBAction)backbtnAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.view removeFromSuperview];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if ([imgKindOf isEqualToString:@"URL"])
    {
        
        NSURL *url=[NSURL URLWithString:imagePath];
        [vw_imgMsg sd_setImageWithURL:url
                                 placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];

        
    }
    else
    {
        vw_imgMsg.image=[UIImage imageWithData:self.sharedImageData];
    }
    
    vw_imgMsg.contentMode=UIViewContentModeScaleToFill;

    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
