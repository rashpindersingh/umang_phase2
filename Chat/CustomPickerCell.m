//
//  CustomPickerCell.m
//  Umang
//
//  Created by spice on 19/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "CustomPickerCell.h"

@implementation CustomPickerCell

@synthesize lbl_title,btn_radio;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
/*- (void)setAccessoryType:(UITableViewCellAccessoryType)accessoryType
{
    // Check for the checkmark
    if (accessoryType == UITableViewCellAccessoryCheckmark)
    {
        // Add the image
        self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]];
    }
    // We don't have to modify the accessory
    else
    {
        self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"untick"]];

    }
    self.accessoryView.backgroundColor = [UIColor clearColor]; // Not working

}
*/

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
