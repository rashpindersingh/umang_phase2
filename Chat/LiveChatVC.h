//
//  LiveChatVC.h
//  Umang
//
//  Created by spice_digital on 27/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveChatVC  : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,NSFetchedResultsControllerDelegate,NSURLSessionDelegate,ChatDelegate,UITextFieldDelegate,UITabBarControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    IBOutlet UITextField *txt_chat;
    
    
    SharedManager *singleton;
    
    
    IBOutlet UIButton *btn_back;
    IBOutlet UIButton *btn_endchat;
    IBOutlet UILabel *chattitle;
    IBOutlet NSLayoutConstraint *navHeightConstraint;
    
    
}

@property (nonatomic,strong) NSFetchedResultsController *fetchedResultController;
@property (strong, nonatomic) XMPPJID *userJID;
@property (nonatomic,copy) NSString *comingFromString;

@property (weak, nonatomic) IBOutlet UILabel *typingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatTitleTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typingLabelHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *backButtonTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *endButtonTopConstraint;


@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *attachButton;

-(IBAction)backBtnAction:(id)sender;


@end

