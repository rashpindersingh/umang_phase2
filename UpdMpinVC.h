//
//  UpdMpinVC.h
//  Umang
//
//  Created by deepak singh rawat on 28/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface UpdMpinVC : UIViewController
@property(nonatomic,retain)NSMutableDictionary *dic_info;
@property(nonatomic,strong)NSData *user_img;
@property(nonatomic,retain)NSString *socialLinkType;
@property(nonatomic,retain)NSString *TAG_FROM;

@property (nonatomic,strong)NSMutableArray *quesAnswArray;

@property (strong, nonatomic) IBOutlet UILabel *enterMpinLabel;
@property (strong, nonatomic) IBOutlet UILabel *enterMpinDescLabel;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property(copy)  void(^ _Nullable didShowHomeVC)(void);




-(IBAction)doneClicked:(id)sender;
-(IBAction)keyPress:(id)sender;

@end

