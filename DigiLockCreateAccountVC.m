//
//  DigiLockCreateAccountVC.m
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigiLockCreateAccountVC.h"
#import "DigiLockOtpVerifyVC.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "DigiLockOtpVerifyVC.h"
@interface DigiLockCreateAccountVC ()<MyTextFieldDelegate>
{
    NSString *strAadharNumber;
    NSString *strMobileNumber;
    NSString *strEmailID;
    
    MBProgressHUD *hud;
    SharedManager *singleton;
    
}
@end

@implementation DigiLockCreateAccountVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    singleton = [SharedManager sharedSingleton];
    self.txtFldOne.myDelegate = self;
    self.txtFldThree.myDelegate = self;
    self.txtFldTwo.myDelegate = self;
    
    
    [self.btnGoNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btnGoNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnGoNext.layer.cornerRadius = 3.0f;
    self.btnGoNext.clipsToBounds = YES;
    
    [self.txtFldOne addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.txtFldTwo addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.txtFldThree addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    strAadharNumber = @"";
    [self enabledGoNextButton:NO];
    
    
    
    [self.backButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    self.createAccountLabel.text = NSLocalizedString(@"create_digilocker_account", nil);
    self.enterAdhaarLabel.text = NSLocalizedString(@"enter_your_aadhaar_number_below", nil);
    self.sendOTPLabel.text = NSLocalizedString(@"register_intro_sub_heading", nil);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.consentLabel.numberOfLines = 0;
        self.consentLabel.frame = CGRectMake(self.consentLabel.frame.origin.x, self.consentLabel.frame.origin.y - 30, self.consentLabel.frame.size.width, self.consentLabel.frame.size.height);
        //[self.consentLabel sizeToFit];
    }
    else
    {
        
        if (fDeviceHeight == 480)
        {
            self.consentLabel.frame = CGRectMake(self.consentLabel.frame.origin.x, self.consentLabel.frame.origin.y+4, self.consentLabel.frame.size.width, self.consentLabel.frame.size.height);
        }
        else if (fDeviceHeight == 568)
        {
            self.consentLabel.frame = CGRectMake(self.consentLabel.frame.origin.x, self.consentLabel.frame.origin.y, self.consentLabel.frame.size.width, self.consentLabel.frame.size.height);
        }
        else if (fDeviceHeight == 667)
        {
            self.consentLabel.frame = CGRectMake(self.consentLabel.frame.origin.x, self.consentLabel.frame.origin.y-10, self.consentLabel.frame.size.width, self.consentLabel.frame.size.height);
        }
        else
        {
            self.consentLabel.frame = CGRectMake(self.consentLabel.frame.origin.x, self.consentLabel.frame.origin.y-20, self.consentLabel.frame.size.width, self.consentLabel.frame.size.height);
        }
        
    }
    self.consentLabel.text = NSLocalizedString(@"aadhaar_digi_consent_txt", nil);
    [self addNavigationView];
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    _backButton.hidden = true;
    //self.consentLabel.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btnBackTapped:btnBack];
    };
    nvView.lblTitle.text = NSLocalizedString(@"digi_locker", "");//;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
   
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [_backButton.titleLabel setFont:[AppFont regularFont:17.0]];
    _createAccountLabel.font = [AppFont semiBoldFont:21.0];
    _enterAdhaarLabel.font = [AppFont mediumFont:15];
    _sendOTPLabel.font = [AppFont regularFont:13.0];
    _txtFldOne.font = [AppFont regularFont:21.0];
    _txtFldTwo.font = [AppFont regularFont:21.0];
    _txtFldThree.font = [AppFont regularFont:21.0];
    _consentLabel.font = [AppFont regularFont:12];
    [_btnGoNext.titleLabel setFont:[AppFont mediumFont:19.0]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)enabledGoNextButton:(BOOL)isEnabled{
    if (isEnabled)
    {
        self.btnGoNext.selected = YES;
        self.btnGoNext.enabled = YES;
        
        [self.btnGoNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else{
        self.btnGoNext.selected = NO;
        self.btnGoNext.enabled = NO;
        
        [self.btnGoNext setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
}

-(void)validationForTextField{
    strAadharNumber=[NSString stringWithFormat:@"%@%@%@",self.txtFldOne.text, self.txtFldTwo.text, self.txtFldThree.text];
    
    if (strAadharNumber.length == 12 && _btnCheckboxConsent.selected)
    {
        //Enable button and jump to next view
        [self enabledGoNextButton:YES];
    }
    else
    {
        [self enabledGoNextButton:NO];
    }
}

- (void)textFieldDidDelete:(UITextField *)textField
{
    
    if (textField.text.length == 0)
    {
        
        if ([textField isEqual:self.txtFldThree])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [self.txtFldTwo becomeFirstResponder];
        }
        else if ([textField isEqual:self.txtFldTwo])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [self.txtFldOne becomeFirstResponder];
        }
        else if ([textField isEqual:self.txtFldOne])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [textField resignFirstResponder];
        }
        
    }
    
}

//---Code for hangle jump from one textfield to another while filling values
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.text.length >= 4)
    {
        
        if ([textField isEqual:self.txtFldOne])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [self.txtFldTwo becomeFirstResponder];
        }
        else if ([textField isEqual:self.txtFldTwo])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [self.txtFldThree becomeFirstResponder];
        }
        else if ([textField isEqual:self.txtFldThree])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [textField resignFirstResponder];
        }
        
        [self validationForTextField];
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan)
    {
        [self.view endEditing:YES];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)btnGoToNextTapped:(id)sender {
    
    [singleton traceEvents:@"Next Button" withAction:@"Clicked" withLabel:@"DigiLocker Create Account" andValue:0];

    [self hitAPI];
    //    DigiLockOtpVerifyVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DigiLockOtpVerifyVC"];
    //    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)btnConsentCheckboxTapped:(UIButton*)sender {
    _btnCheckboxConsent.selected = !_btnCheckboxConsent.selected;
    
    [self validationForTextField];
}


- (IBAction)btnBackTapped:(id)sender {
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"DigiLocker Create Account" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
}


-(void)hitAPI
{
    [self.view endEditing:YES];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:strAadharNumber forKey:@"uid"];
    [dictBody setObject:strAadharNumber forKey:@"aadhr"];
    
    [dictBody setObject:@"Y" forKey:@"consent"];
    [dictBody setObject:@"APP" forKey:@"mod"];
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:selectedLanguage forKey:@"lang"];
    
    if (singleton.user_tkn)
    {
        [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    }
    else
    {
        [dictBody setObject:singleton.apiMode.RefreshTokenBearer forKey:@"tkn"];
    }
    
    
    
    [objRequest hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_DIGILOCKER_AADHAR_VERIFICATION withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_AADHAR_VERIFY completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         
         if (error == nil)
         {
             NSLog(@"Server Response = %@",response);
             //------ Sharding Logic parsing---------------
             NSString *node=[response valueForKey:@"node"];
             if([node length]>0)
             {
                 [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
             }
             
             NSInteger timeOut =[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
             NSString *retryCount=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
             NSString *emailID=[[response valueForKey:@"pd"] valueForKey:@"email"];
             NSString *mobileNumber=[[response valueForKey:@"pd"] valueForKey:@"mobile"];
             
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
             {
                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

                 DigiLockOtpVerifyVC  *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockOtpVerifyVC"];
                 [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                 vc.aadharNumber = strAadharNumber;
                 vc.maskedEmailNumber = emailID;
                 vc.maskedMobileNumber =mobileNumber;
                 vc.timeOut = timeOut;
                 vc.retryCount=retryCount;
                 [self presentViewController:vc animated:YES completion:nil];
             }
             else if ([[response objectForKey:@"rs"] isEqualToString:@"F"] && [[response valueForKey:@"rd"] isEqualToString:@"Aadhaar already registered. You can sign in using it."])
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:[NSString stringWithFormat:@"%@",[response valueForKey:@"rd"]] delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
                 [alert show];
             }
             else if ([[response objectForKey:@"rs"] isEqualToString:@"F"] && [[response valueForKey:@"rd"] isEqualToString:@"Linked mobile number is not verified at Aadhaar."])
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:[NSString stringWithFormat:@"%@",[response valueForKey:@"rd"]] delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
                 [alert show];
             }
             
         }
         else
         {
             NSLog(@"Error Occured = %@",error.localizedDescription);
             self.txtFldOne.text = @"";
             self.txtFldTwo.text = @"";
             self.txtFldThree.text = @"";
             [self btnConsentCheckboxTapped:_btnCheckboxConsent];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                             message:error.localizedDescription
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                   otherButtonTitles:nil];
             [alert show];
         }
     }];
}


@end
