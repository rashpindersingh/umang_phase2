//
//  EnterMobileOTPVC.m
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import "EnterMobileOTPVC.h"

#define kOFFSET_FOR_KEYBOARD 80.0

#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

#import "MyTextField.h"
#import "UMAPIManager.h"
#import "SetMPINForMobileVC.h"
#import "UpdateQuestionsViewController.h"
#import "ResetPinVC.h"


#define MAX_LENGTH 6

@interface EnterMobileOTPVC () <UITextFieldDelegate>
{
    SharedManager *singleton;
    MBProgressHUD *hud ;
    IBOutlet UIButton *btn_resend;
    IBOutlet UIButton *btn_callme;
    NSString *retryResend;
    NSString *retryCall;
    NSString *retryAdhar;
    NSUInteger newLength;
    __weak IBOutlet UILabel *lblwaitingForSMS;
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblDidntReceive;
    __weak IBOutlet UILabel *lblSubHeaderDescritopn;
    __weak IBOutlet UILabel *lblHeaderDescriptn;
    __weak IBOutlet UILabel *lblHeaderText;
    __weak IBOutlet UILabel *lblEnter6Digit;
    int count;
      NSMutableArray *questionsArray;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lb_rtryResend;
@property (weak, nonatomic) IBOutlet UILabel *lb_rtryCall;
@property(nonatomic,retain)NSString *mno_resend;
@property(nonatomic,retain)NSString *chnl_resend;
@property(nonatomic,retain)NSString *tkn_resend;
@property(nonatomic,retain)NSString *ort_resend;
@property(nonatomic,retain)NSString *type;
@property(nonatomic,retain)NSString *stype;

//--- code for otp and resend handling---------


@property (nonatomic, strong) IBOutlet UIProgressView *progressView;
@property (nonatomic, strong) NSTimer *myTimer;
@property (weak, nonatomic) IBOutlet UILabel *lb_timer;

@property (weak, nonatomic) IBOutlet UIView *vw_line;
@property (weak, nonatomic) IBOutlet UITextField *txt1;

@property (weak, nonatomic) IBOutlet UILabel *vw_line1;




@end




@implementation EnterMobileOTPVC

@synthesize strAadharNumber;
@synthesize maskedMobileNumber;
//-------- Code for resend otp and IVR---------
@synthesize altmobile;
@synthesize TAGFROM;
@synthesize rtry;
@synthesize tout;
@synthesize scrollView;


@synthesize mno_resend;
@synthesize chnl_resend;
@synthesize tkn_resend;
@synthesize ort_resend;
@synthesize type,stype;

@synthesize TYPE_LOGIN_CHOOSEN;

- (id)initWithNibName:(NSString* )nibNameOrNil bundle:(NSBundle* )nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        self =[super initWithNibName:@"EnterMobileOTPVC_iPad" bundle:nil];
    }
    return self;
}

-(IBAction)btn_resendAction:(id)sender
{
    [[SharedManager sharedSingleton] traceEvents:@"Resend OTP Button" withAction:@"Clicked" withLabel:@"Mobile OTP Screen" andValue:0];
    _txt1.text=@"";
    
    resendOTPview.hidden=TRUE;
    
    chnl_resend=@"sms";
    
    [self parameterForApiCall:chnl_resend];
}

//- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
//    [[NSOperationQueue mainQueue] addOperationWithBlock:
//     ^{
//        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
//    }];
//    return [super canPerformAction:action withSender:sender];
//}
//-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
//{
//    if (action == @selector(copy:) || action == @selector(paste:)) {
//        return NO;
//    }
//    return [super canPerformAction:action withSender:sender];
//}  // this is also not working still showing the paste option
//

-(IBAction)btn_callmeAction:(id)sender
{
     [[SharedManager sharedSingleton] traceEvents:@"Call Me Button" withAction:@"Clicked" withLabel:@"Mobile OTP Screen" andValue:0];
    resendOTPview.hidden=TRUE;
    
    chnl_resend=@"ivr";
    [self parameterForApiCall:chnl_resend];
    
}



- (void)textFieldDidChange:(UITextField *)textField
{
    
    if (textField.text.length >= MAX_LENGTH)
    {
        textField.text = [textField.text substringToIndex:MAX_LENGTH];
        [textField resignFirstResponder];
        [self enableBtnNext:YES];
        
        //[self checkValidation];
    }
    else
    {
        [self enableBtnNext:NO];

    }
        
}

- (void)updateUI:(NSTimer *)timer
{
    
    
    NSLog(@"count=%d",count);
    
    
    if (count <= 0)
    {
        [self.myTimer invalidate];
        self.myTimer = nil;
        resendOTPview.hidden=FALSE;
        
        
        
        
        int rtryOtp=[retryResend intValue];
        int rtryCall=[retryCall intValue];
        int rtryAdhar=[retryAdhar intValue];
        
        if (rtryAdhar>0)
        {
            resendOTPview.hidden=FALSE;
            
            
            if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
            {
                btn_callme.hidden=TRUE;
                self.lb_rtryCall.hidden=TRUE;
                
            }
            else
            {
                btn_callme.hidden = YES;
                self.lb_rtryCall.hidden=YES;
                vwLineUnderResend.hidden = YES;
            }
            
        }
        else
        {
            if (rtryOtp <=0)
            {
                btn_resend.hidden=TRUE;
                self.lb_rtryResend.hidden=TRUE;
                
            }
            if (rtryCall<=0)
            {
                
                btn_callme.hidden=TRUE;
                self.lb_rtryCall.hidden=TRUE;
                
                
            }
            if (rtryOtp <=0 && rtryCall<=0)
            {
                resendOTPview.hidden=TRUE;
            }
            
        }
        
        
    }
    else
    {
        
        count --;
        
        self.progressView.progress = (float)count/120.0f;
        
        self.lb_timer.text=[self timeFormatted:count];
        
    }
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, resendOTPview.isHidden ? scrollView.frame.size.height:scrollView.frame.size.height+150)];
    
    
}

-(void)showRetryOption
{
    count = -1;
    [self updateUI:nil];
}



-(void)parameterForApiCall:(NSString*)chnlresend
{
    if (TYPE_LOGIN_CHOOSEN == ISFROMLOGINWITHOTP)
    {
        
        btn_callme.hidden=FALSE;
        self.lb_rtryCall.hidden=FALSE;
        [self hitAPIResendOTPwithIVR:chnlresend];
        
        
    }
    if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
    {
        
        btn_callme.hidden=FALSE;
        self.lb_rtryCall.hidden=FALSE;
        [self hitAPIResendOTPwithIVR:chnlresend];
        
        
    }
    else if (TYPE_LOGIN_CHOOSEN == IS_FROM_MOBILE_NUMBER_REGISTRATION)
    {
        btn_callme.hidden=FALSE;
        self.lb_rtryCall.hidden=FALSE;
        [self hitAPIResendOTPwithIVR:chnlresend];
        
    }
    else if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
    {
        btn_callme.hidden=TRUE;
        self.lb_rtryCall.hidden=TRUE;
        
        [self hitresendAadharAPI];
    }
}



//----- hitAPI for IVR OTP call Type registration ------
-(void)hitresendAadharAPI
{
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    //aadhaar card
    [dictBody setObject:@"aadhar" forKey:@"type"];
    [dictBody setObject:@"" forKey:@"stype"];
    [dictBody setObject:strAadharNumber forKey:@"aadhr"];
    [dictBody setObject:@"loginadhr" forKey:@"ort"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_GENERATE_AADHAR_OTP withBody:dictBody andTag:TAG_REQUEST_GENERATE_AADHAR_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        NSLog(@"dictBody is = %@",dictBody);
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            
            
            
            
            
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                
                //do here
                tout=[[[[response valueForKey:@"pd"]valueForKey:@"pd"] valueForKey:@"tout"] intValue];
                rtry=[[[response valueForKey:@"pd"] valueForKey:@"pd"]valueForKey:@"rtry"];
                
                
                
                NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
                
                retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
                retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
                
                // retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                @try {
                    retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
                
                
                if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
                {
                    self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryAdhar,strRetryValue];
                    
                    
                }
                else
                {
                    self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:0],strRetryValue];
                    self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:1],strRetryValue];
                    
                    
                }
                
                
                
                /*   int rtryOtp=[retryResend intValue];
                 int rtryCall=[retryCall intValue];
                 int rtryAdhar=[retryAdhar intValue];
                 
                 if (rtryAdhar<=0)
                 {
                 resendOTPview.hidden=TRUE;
                 
                 }
                 else
                 {
                 if (rtryOtp <=0)
                 {
                 btn_resend.hidden=TRUE;
                 self.lb_rtryResend.hidden=TRUE;
                 
                 }
                 if (rtryCall<=0)
                 {
                 
                 btn_callme.hidden=TRUE;
                 self.lb_rtryCall.hidden=TRUE;
                 
                 
                 }
                 if (rtryOtp <=0 && rtryCall<=0)
                 {
                 resendOTPview.hidden=TRUE;
                 }
                 
                 
                 if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
                 {
                 btn_callme.hidden=TRUE;
                 self.lb_rtryCall.hidden=TRUE;
                 
                 }
                 
                 
                 }*/
                count =tout;// count++;
                
                // wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
                
                if ( [self.myTimer isValid]){
                    [self.myTimer invalidate], self.myTimer=nil;
                }
                
                self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
                [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];
                
                
            }
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
            
        }
        
    }];
    
}


#pragma mark - Menu controller



-(void)hitAPIResendOTPwithIVR:(NSString*)channelType
{
    NSLog(@"value of mno=%@ \n value of channel=%@ \n value of token=%@ \n value of ort=%@ \n ",mno_resend,chnl_resend,tkn_resend,ort_resend);
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Wait...", @"Wait");
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    
    if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
    {
        [dictBody setObject:@"aadhar" forKey:@"type"];
        [dictBody setObject:@"" forKey:@"stype"];
        [dictBody setObject:strAadharNumber forKey:@"aadhr"];
        [dictBody setObject:@"loginadhr" forKey:@"ort"];
        [dictBody setObject:chnl_resend forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
        
    }
    if (TYPE_LOGIN_CHOOSEN == ISFROMLOGINWITHOTP)
    {
        [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
        [dictBody setObject:chnl_resend forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
        [dictBody setObject:@"loginmob" forKey:@"ort"];
    }
    if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
    {
        [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
        [dictBody setObject:chnl_resend forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
        [dictBody setObject:@"frgtmpn" forKey:@"ort"];
    }
    else if (TYPE_LOGIN_CHOOSEN == IS_FROM_MOBILE_NUMBER_REGISTRATION)
    {
        [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
        [dictBody setObject:chnl_resend forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
        [dictBody setObject:@"rgtmob" forKey:@"ort"];
    }
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_IVR_OTP withBody:dictBody andTag:TAG_REQUEST_IVR_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //[self openNextView];
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rd=[response valueForKey:@"rd"];
            //  NSString *rs=[response valueForKey:@"rs"];
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            
            
            
            //------ Sharding Logic parsing---------------
            
            
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                //[self alertwithMsg:rd];
                // [self hitUpdateAlterMob];
                // man=[[response valueForKey:@"pd"] valueForKey:@"man"];
                rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
                tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
                NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
                
                retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
                retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
                
                @try {
                    //  retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                    retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                btn_resend.enabled=TRUE;
                btn_callme.enabled=TRUE;
                
                NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
                
                // self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryAdhar,strRetryValue];
                
                self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryResend,strRetryValue];
                self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",retryCall,strRetryValue];
                
                
                int rtryOtp=[retryResend intValue];
                int rtryCall=[retryCall intValue];
                int rtryAdhar=[retryAdhar intValue];
                
                if (rtryAdhar>0)
                {
                    resendOTPview.hidden=FALSE;
                    
                }
                else
                {
                    if (rtryOtp <=0)
                    {
                        btn_resend.hidden=TRUE;
                        self.lb_rtryResend.hidden=TRUE;
                        
                    }
                    if (rtryCall<=0)
                    {
                        
                        btn_callme.hidden=TRUE;
                        self.lb_rtryCall.hidden=TRUE;
                        
                        
                    }
                    if (rtryOtp <=0 && rtryCall<=0)
                    {
                        resendOTPview.hidden=TRUE;
                    }
                    
                    
                    if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
                    {
                        btn_callme.hidden=TRUE;
                        self.lb_rtryCall.hidden=TRUE;
                        
                    }
                    
                    
                }
                count =tout;// count++;
                
                // wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
                
                
                if ( [self.myTimer isValid]){
                    [self.myTimer invalidate], self.myTimer=nil;
                }
                
                self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
                [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];
                
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            

        }
        
    }];
    
}


- (void)viewDidLoad
{
    

    lblwaitingForSMS.text = NSLocalizedString(@"waiting_for_sms", nil);
      questionsArray = [NSMutableArray new];
    [self.btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnNext.layer.cornerRadius = 3.0f;
    self.btnNext.clipsToBounds = YES;
    [self enableBtnNext:NO];
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:MOBILE_REGISTRATION_OTP_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    tkn_resend=@"";
    btn_resend.alpha = 1.0;
    btn_callme.alpha = 1.0;
    if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
    {
        lblMaskedMNumber.text  = maskedMobileNumber;
    }
    else
    {
        lblMaskedMNumber.text  = @"";
    }
    //-------- Code for resend otp and IVR---------
    
    [_txt1 becomeFirstResponder];
    
    lblDidntReceive.text = NSLocalizedString(@"didnt_receive_otp", nil);
    lblSubHeaderDescritopn.text =NSLocalizedString(@"register_verify_otp_sub_heading", nil);
    lblHeaderDescriptn.text = NSLocalizedString(@"register_verify_otp_heading", nil);
    lblHeaderText.text =NSLocalizedString(@"verify_otp_label", nil);
    lblEnter6Digit.text =NSLocalizedString(@"enter_6_digit_otp", nil);
    
    [btn_resend setTitle:NSLocalizedString(@"resend_otp", nil) forState:UIControlStateNormal];
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    [btn_callme setTitle:NSLocalizedString(@"call_me", nil) forState:UIControlStateNormal];
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    
    singleton = [SharedManager sharedSingleton];
    singleton.user_aadhar_number = self.strAadharNumber;
    
    
    //----- Setting delegate for Custom textfield so back space operation work smooth
    _txt1.delegate = self;
    [_txt1 addTarget:self
              action:@selector(textFieldDidChange:)
    forControlEvents:UIControlEventEditingChanged];
    
    
    
    
    //self.vw_line.frame=CGRectMake(53, 492, 268, 0.5);
    self.view.userInteractionEnabled = YES;
    
    
    
    self.vw_line1.backgroundColor=[UIColor lightGrayColor];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapGesture];
    
    
    //UMGIOSINT-1193 FIX
   // _txt1.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    _txt1.keyboardType = UIKeyboardTypeNumberPad;
  
    [self addNavigationView];
    
}
-(void)hideKeyboard
{
    
    [self.view endEditing:YES];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btnBack.hidden = true;
    //.hidden = true;
    //btnHelp.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btnBackClicked:btnBack];
    };
    //nvView.lblTitle.text = lblHeadingAccountSeeting.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX())
    {
        CGRect tbFrame = scrollView.frame;
        tbFrame.origin.y = kiPhoneXNaviHeight
        tbFrame.size.height = fDeviceHeight - kiPhoneXNaviHeight;
        scrollView.frame = tbFrame;
        [self.view layoutIfNeeded];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    
    //----- code for handling ----
    
    resendOTPview.hidden=TRUE;
    NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
    
    
    
    // retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
    @try {
        
        retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
        retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
        retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
    
    
    if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
    {
        self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryAdhar,strRetryValue];
        
        
    }
    else
    {
        self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:0],strRetryValue];
        self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:1],strRetryValue];
        
        
    }
    
    
    
    
    count =tout;// count++;
    if ( [self.myTimer isValid]){
        [self.myTimer invalidate], self.myTimer=nil;
    }
    
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];
    
    //--------- Code for handling -------------------
    [super viewWillAppear:NO];
    
    
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeaderText.font = [AppFont semiBoldFont:22.0];
    _lblScreenTitleName.font = [AppFont semiBoldFont:22.0];
    lblHeaderDescriptn.font = [AppFont semiBoldFont:17.0];
    lblMaskedMNumber.font = [AppFont regularFont:18.0];
    lblSubHeaderDescritopn.font = [AppFont mediumFont:15.0];
    _txt1.font = [AppFont regularFont:21.0];
    lblEnter6Digit.font = [AppFont mediumFont:14.0];
    _lb_timer.font = [AppFont mediumFont:14];
    lblwaitingForSMS.font = [AppFont mediumFont:14];
    
    lblDidntReceive.font = [AppFont regularFont:16.0];
    btn_resend.titleLabel.font = [AppFont mediumFont:15.0];
    btn_callme.titleLabel.font = [AppFont mediumFont:15.0];
    _lb_rtryResend.font = [AppFont regularFont:13.0];
    _lb_rtryCall.font = [AppFont regularFont:13.0];
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}


-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

- (IBAction)btnBackClicked:(id)sender {
    [self.myTimer invalidate];
    self.myTimer = nil;
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [self.myTimer invalidate];
    self.myTimer = nil;
    
    [super viewDidDisappear:NO];
}




- (NSString *)timeFormatted:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    // int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark UITextFieldDelegate methods
-(void)enableBtnNext:(BOOL)status
{
    self.btnNext.userInteractionEnabled = status;

    if (status ==YES)
    {
        [self hideKeyboard];
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

- (IBAction)didTapNextBtnAction:(UIButton *)sender {
     [[SharedManager sharedSingleton] traceEvents:@"Next Button" withAction:@"Clicked" withLabel:@"Mobile OTP Screen" andValue:0];
    [self checkValidation];
    
}

/*-(BOOL)isValidOTP:(NSString*)otpString
{
    
    NSString *validRegEx =@"[0123456789][0-9]{9}";

    NSString *validRegEx =@"^[0-9]$"; //change this regular expression as your requirement
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", validRegEx];
    return [test evaluateWithObject:otpString];
    
    
}*/


-(BOOL) isValidOTP:(NSString*) str
{
    NSString *strMatchstring=@"\\b([0-9%_.+\\-]+)\\b";
    NSPredicate *textpredicate=[NSPredicate predicateWithFormat:@"SELF MATCHES %@", strMatchstring];
    
    if(![textpredicate evaluateWithObject:str])
    {
        return FALSE;
    }
    return TRUE;
}


-(void)checkValidation
{
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"register_verify_otp_heading", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
    }
    else if ([self isValidOTP:_txt1.text]!=TRUE)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"register_verify_otp_heading", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
            [alert show];
            
        }
    else
    {
        //[_txt1 resignFirstResponder];
        [self hideKeyboard];
        
        if (TYPE_LOGIN_CHOOSEN == ISFROMLOGINWITHOTP)
        {
            [self hitloginAPI];
        }
        if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
        {
            [self hitAPI];
        }
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_MOBILE_NUMBER_REGISTRATION)
        {
            //[self hitAPI];
            [self hitRegisterAPI];
        }
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
        {
            [self hitLoginAPIForAadharAfterValidationCheck];
            
        }
        
        
        
    }
    
}

-(void)hitLoginAPIForAadharAfterValidationCheck
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    NSString *otpString= _txt1.text;
    
    [dictBody setObject:@"aadharo" forKey:@"type"];
    [dictBody setObject:otpString forKey:@"lid"];
    [dictBody setObject:self.strAadharNumber forKey:@"aadhr"];
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LOGIN withBody:dictBody andTag:TAG_REQUEST_LOGIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         NSLog(@"dictBody is = %@",dictBody);
         
         [hud hideAnimated:YES];
         // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
         
         
         if (error == nil) {
             NSLog(@"Server Response = %@",response);
             
             
             //             NSString *rc=[response valueForKey:@"rc"];
             NSString *rd=[response valueForKey:@"rd"];
             
             NSString *rs=[response valueForKey:@"rs"];
             
             NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
             
             if ([rs isEqualToString:@"SU"]||[rs isEqualToString:@"S"])
             {
                 //singleton.mobileNumber= txtAadhar.text;
                 
                 
                 //for login in pd sending General pd
                 
                 //-------- Add later For handling mpinflag / mpinmand ----------
                 singleton.shared_mpinflag =[[response valueForKey:@"pd"]  valueForKey:@"mpinflag"];
                 singleton.shared_mpinmand =[[response valueForKey:@"pd"]  valueForKey:@"mpinmand"];
                 
                 NSLog(@"mpinflag =%@",singleton.shared_mpinflag);
                 NSLog(@"mpinmand =%@",singleton.shared_mpinmand);
                 if (singleton.shared_mpinflag == (NSString *)[NSNull null]||[singleton.shared_mpinflag length]==0) {
                     singleton.shared_mpinflag=@"TRUE";
                 }
                 
                 if (singleton.shared_mpinmand == (NSString *)[NSNull null]||[singleton.shared_mpinmand length]==0) {
                     singleton.shared_mpinmand=@"TRUE";
                 }
                 NSString *mpindial =[[response valueForKey:@"pd"]  valueForKey:@"mpindial"];
                 NSLog(@"mpindial =%@",mpindial);
                 [[NSUserDefaults standardUserDefaults] setValue:mpindial forKey:@"mpindial"];
                 
                 // recflag save start
                 NSString *recflag  = [[response valueForKey:@"pd"]  valueForKey:@"recflag"];
                 [[NSUserDefaults standardUserDefaults] setValue:recflag forKey:@"recflag"];


                 [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinflag forKey:@"mpinflag"];
                 [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinmand forKey:@"mpinmand"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 //-------- Add later For handling mpinflag / mpinmand ----------

                 
                 singleton.objUserProfile = nil;
                 singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                 
                 //                 NSMutableArray *generalpdList=[[NSMutableArray alloc]init];
                 //
                 //                 generalpdList=[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];
                 
                 singleton.user_tkn=tkn;
                 //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                 //[[NSUserDefaults standardUserDefaults]synchronize];
                 
                 
                 NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                 
                 if ([abbreviation length]==0)
                 {
                     abbreviation=@"";
                 }
                 
                 NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                 emblemString = emblemString.length == 0 ? @"":emblemString;
                 [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                 
                 [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                 [singleton setStateId:singleton.user_StateId];
                 
                 
                 //------------------------- Encrypt Value------------------------
                 
                 [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                 // Encrypt
                 [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 //------------------------- Encrypt Value------------------------
                 // ========= check to open show mpin alert box=====
                 dispatch_async(dispatch_get_main_queue(),^{
                     AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                     //[delegate showHideSetMpinAlertBox ];
                     if(([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"T"])||([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"TRUE"]))
                     {
                         if(!([[recflag uppercaseString] isEqualToString:@"T"]||[[recflag uppercaseString] isEqualToString:@"TRUE"]))
                         {
                             [delegate checkRecoveryOptionBOXShownStatus:@""];
                         }
                     }
                     else
                     {
                         
                         [delegate showHideSetMpinAlertBox ];
                     }
                     [delegate checkRecoveryOptionBOXShownStatus:@""];

                 });
                 
                 
                 [self alertwithMsg:rd];
                 
             }
             
         }
         
         else{
             NSLog(@"Error Occured = %@",error.localizedDescription);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                             message:error.localizedDescription
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                   otherButtonTitles:nil];
             [alert show];
             
         }
         
         
     }];
    return;
}



//----- hitAPI for IVR OTP call Type registration ------
-(void)hitloginAPI
{
    
    NSString *otpString=_txt1.text;
    
    
    NSString *encryptedInputID = @"";
    NSString *loginType = @"";
    NSString *sType = @"";
    NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",otpString ,strSaltMPIN];
    encryptedInputID=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    
    NSLog(@"encry=%@",encryptedInputID);
    loginType = @"mobo";
    sType = @"";
    
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:loginType forKey:@"type"];
    
    
    //for Mobile with Mpin
    //[dictBody setObject:@"OTP" forKey:@"lid"];  //for Mobile with OTP
    //[dictBody setObject:@"Social ID" forKey:@"lid"]; //for Mobile with Social ID
    [dictBody setObject:otpString forKey:@"lid"];
    [dictBody setObject:sType forKey:@"stype"];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LOGIN withBody:dictBody andTag:TAG_REQUEST_LOGIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
            
            NSString *rs=[response valueForKey:@"rs"];
            
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            if ([rs isEqualToString:@"SU"]||[rs isEqualToString:@"S"]) {
                //  rd = "OTP Match & session is created successfully";
                
                //---fix lint----
                //  NSMutableArray *generalpdList=[[NSMutableArray alloc]init];
                //  generalpdList=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                //singleton.user_id=[[response valueForKey:@"pd"]valueForKey:@"uid"];
                
                
                
                NSMutableArray *generalpdList = (NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                
                singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                
                
                NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                
                if ([abbreviation length]==0)
                {
                    abbreviation=@"";
                }
                
                //-------- Add later For handling mpinflag / mpinmand ----------
                singleton.shared_mpinflag =[[response valueForKey:@"pd"]  valueForKey:@"mpinflag"];
                singleton.shared_mpinmand =[[response valueForKey:@"pd"]  valueForKey:@"mpinmand"];
                
                NSLog(@"mpinflag =%@",singleton.shared_mpinflag);
                NSLog(@"mpinmand =%@",singleton.shared_mpinmand);
                if (singleton.shared_mpinflag == (NSString *)[NSNull null]||[singleton.shared_mpinflag length]==0) {
                    singleton.shared_mpinflag=@"TRUE";
                }
                
                if (singleton.shared_mpinmand == (NSString *)[NSNull null]||[singleton.shared_mpinmand length]==0) {
                    singleton.shared_mpinmand=@"TRUE";
                }
                NSString *mpindial =[[response valueForKey:@"pd"]  valueForKey:@"mpindial"];
                NSLog(@"mpindial =%@",mpindial);
                [[NSUserDefaults standardUserDefaults] setValue:mpindial forKey:@"mpindial"];
                
                // recflag save start
                NSString *recflag  = [[response valueForKey:@"pd"]  valueForKey:@"recflag"];
                [[NSUserDefaults standardUserDefaults] setValue:recflag forKey:@"recflag"];


                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinflag forKey:@"mpinflag"];
                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinmand forKey:@"mpinmand"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //-------- Add later For handling mpinflag / mpinmand ----------

                
                NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                emblemString = emblemString.length == 0 ? @"":emblemString;
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
                
                
                //-------- Add later----------
                singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                //-------- Add later----------
                
                NSString* str_name=[generalpdList valueForKey:@"nam"];
                NSString* str_mno =[generalpdList valueForKey:@"mno"];
                NSString* str_amno=[generalpdList valueForKey:@"amno"];
                NSString* str_city=[generalpdList valueForKey:@"cty"];
                NSString* str_state=[generalpdList valueForKey:@"st"];
                NSString* str_district=[generalpdList valueForKey:@"dist"];
                NSString* str_dob=[generalpdList valueForKey:@"dob"];
                NSString* str_gender=[generalpdList valueForKey:@"gndr"];
                NSString* str_pic=[generalpdList valueForKey:@"pic"];
                
                
                NSString* str_occup=[generalpdList valueForKey:@"occup"];
                NSString* str_qual=[generalpdList valueForKey:@"qual"];
                NSString* str_email=[generalpdList valueForKey:@"email"];
                
                
                
                singleton.notiTypeGenderSelected=@"";
                singleton.profileNameSelected =@"";
                singleton.profilestateSelected=@"";
                singleton.notiTypeCitySelected=@"";
                singleton.notiTypDistricteSelected=@"";
                singleton.profileDOBSelected=@"";
                singleton.altermobileNumber=@"";
                singleton.user_Qualification=@"";
                singleton.user_Occupation=@"";
                singleton.user_profile_URL=@"";
                singleton.profileEmailSelected=@"";
                
                
                
                if ([str_occup length]!=0) {
                    singleton.user_Occupation=str_occup;
                }
                
                if ([str_qual length]!=0) {
                    singleton.user_Qualification=str_qual;
                }
                if ([str_email length]!=0) {
                    singleton.profileEmailSelected=str_email;
                }
                
                
                
                
                if ([str_mno length]!=0) {
                    
                    singleton.mobileNumber=str_mno;
                    
                }
                
                
                if ([str_amno length]!=0) {
                    
                    singleton.altermobileNumber=str_amno;
                    
                }
                
                
                
                if ([str_pic length]!=0) {
                    
                    singleton.user_profile_URL=str_pic;
                    //------------------------- Encrypt Value------------------------
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //------------------------- Encrypt Value------------------------
                    
                    
                }
                
                
                
                
                if ([str_name length]!=0) {
                    str_name = [str_name stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[str_name substringToIndex:1] uppercaseString]];
                    
                    
                }
                if ([singleton.profileNameSelected length]==0) {
                    if ([str_name length]!=0) {
                        singleton.profileNameSelected=str_name;
                        
                    }
                }
                
                
                
                
                if ([singleton.notiTypeGenderSelected length]==0) {
                    if ([str_gender length]!=0) {
                        str_gender=[str_gender uppercaseString];
                        
                        if ([str_gender isEqualToString:@"M"]||[str_gender isEqualToString:@"MALE"]) {
                            singleton.notiTypeGenderSelected=@"Male";
                            
                        }
                        if ([str_gender isEqualToString:@"F"] || [str_gender isEqualToString:@"FEMALE"] ){
                            singleton.notiTypeGenderSelected=@"Female";
                            
                        }
                        if ([str_gender isEqualToString:@"T"]) {
                            singleton.notiTypeGenderSelected=@"Other";
                            
                        }
                        
                        
                        
                    }
                }
                
                if ([singleton.profilestateSelected length]==0) {
                    if ([str_state length]!=0) {
                        singleton.profilestateSelected=str_state;
                        //state_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypeCitySelected length]==0) {
                    if ([str_city length]!=0) {
                        singleton.notiTypeCitySelected=str_city;
                        // city_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypDistricteSelected length]==0) {
                    if ([str_district length]!=0) {
                        singleton.notiTypDistricteSelected=str_district;
                        //district_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                
                if ([singleton.profileDOBSelected length]==0) {
                    if ([str_dob length]!=0) {
                        singleton.profileDOBSelected=str_dob;
                        
                    }
                }
                
                
                
                singleton.user_tkn=tkn;
                
                // [[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                // [[NSUserDefaults standardUserDefaults]synchronize];
                
                
                //------------------------- Encrypt Value------------------------
                
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setValue:[NSDate new] forKey:RECOVERY_BOX_DATE];
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:RECOVERY_BOX_COUNT];
                // ========= check to open show mpin alert box=====
                dispatch_async(dispatch_get_main_queue(),^{
                    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    //[delegate showHideSetMpinAlertBox ];
                    if(([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"T"])||([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"TRUE"]))
                    {
                        if(!([[recflag uppercaseString] isEqualToString:@"T"]||[[recflag uppercaseString] isEqualToString:@"TRUE"]))
                        {
                            [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];
                        }
                    }
                    else
                    {
                        
                        [delegate showHideSetMpinAlertBox ];
                    }
                    [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];

                });
                
                
                [self alertwithMsg:rd];
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}

//NEW CHANGE
//HIT NEW API FOR REGISTERATION

-(void)hitRegisterAPI
{
    NSString *otpString= _txt1.text;
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    
    [dictBody setObject:otpString forKey:@"otp"];
    
    
    [dictBody setObject:@"sms" forKey:@"chnl"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:@"rgtmob" forKey:@"ort"];
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VALIDATE_OTP2 withBody:dictBody andTag:TAG_REQUEST_VALIDATE_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
    {
        
        [hud hideAnimated:YES];
        
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            NSString *tkn=[[response valueForKey:@"pd"] valueForKey:@"tkn"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                singleton.user_tkn=tkn;
            
                
                //-------- Add later For handling mpinflag / mpinmand ----------
                singleton.shared_mpinflag =[[response valueForKey:@"pd"]  valueForKey:@"mpinflag"];
                singleton.shared_mpinmand =[[response valueForKey:@"pd"]  valueForKey:@"mpinmand"];
                
                NSLog(@"mpinflag =%@",singleton.shared_mpinflag);
                NSLog(@"mpinmand =%@",singleton.shared_mpinmand);
                if (singleton.shared_mpinflag == (NSString *)[NSNull null]||[singleton.shared_mpinflag length]==0) {
                    singleton.shared_mpinflag=@"TRUE";
                }
                
                if (singleton.shared_mpinmand == (NSString *)[NSNull null]||[singleton.shared_mpinmand length]==0) {
                    singleton.shared_mpinmand=@"TRUE";
                }
                NSString *mpindial =[[response valueForKey:@"pd"]  valueForKey:@"mpindial"];
                NSLog(@"mpindial =%@",mpindial);
                [[NSUserDefaults standardUserDefaults] setValue:mpindial forKey:@"mpindial"];
                
                // recflag save start
                NSString *recflag  = [[response valueForKey:@"pd"]  valueForKey:@"recflag"];
                [[NSUserDefaults standardUserDefaults] setValue:recflag forKey:@"recflag"];


                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinflag forKey:@"mpinflag"];
                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinmand forKey:@"mpinmand"];
                //[[NSUserDefaults standardUserDefaults] synchronize];
                //-------- Add later For handling mpinflag / mpinmand ----------

                
                // check here for login with registration here
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isLoginWithRegistration"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                
                // check here for login with registration here
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"showLoginOTP_Hint"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
                singleton.lastFetchDate=@"NR";
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.lastFetchDate withKey:@"lastFetchDate"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setAESKey:@"UMANGIOSAPP"];
                [defaults encryptValue:@"" withKey:@"CITY_KEY"];
                [defaults encryptValue:@"" withKey:@"ALTERMB_KEY"];
                [defaults encryptValue:@"" withKey:@"QUALI_KEY"];
                [defaults encryptValue:@"" withKey:@"OCCUP_KEY"];
                [defaults encryptValue:@"" withKey:@"EMAIL_KEY"];
                [defaults encryptValue:@"" withKey:@"URLPROFILE_KEY"];
                [defaults encryptValue:@"" withKey:@"MOBILE_KEY"];
                [defaults encryptValue:@"" withKey:@"NAME_KEY"];
                [defaults encryptValue:@"" withKey:@"GENDER_KEY"];
                [defaults encryptValue:@"" withKey:@"STATE_KEY"];
                [defaults encryptValue:@"" withKey:@"DISTRICT_KEY"];
                [defaults encryptValue:@"" withKey:@"DOB_KEY"];
                [defaults synchronize];
                
                singleton.profilestateSelected = @"";
                
                singleton.profileNameSelected=@"";
                singleton.notiTypeGenderSelected=@"";
                singleton.profileDOBSelected = @"";
                
                singleton.notiTypDistricteSelected = @"";
                singleton.profileUserAddress = @"";
                singleton.altermobileNumber = @"";
                singleton.profileEmailSelected=@"";
                
                
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                
                
                
                
             
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                tbc.selectedIndex = 0;
                [self presentViewController:tbc animated:NO completion:nil];
                
                //questionsArray = [[[response valueForKey:@"quesid"] componentsSeparatedByString:@","] mutableCopy];
                
                // ========= check to open show mpin alert box=====
                dispatch_async(dispatch_get_main_queue(),^{
                    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    //[delegate showHideSetMpinAlertBox ];
                    if(([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"T"])||([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"TRUE"]))
                    {
                        if(!([[recflag uppercaseString] isEqualToString:@"T"]||[[recflag uppercaseString] isEqualToString:@"TRUE"]))
                        {
                            [delegate checkRecoveryOptionBOXShownStatus:@""]; // no use
                        }
                    }
                    else
                    {
                        
                        [delegate showHideSetMpinAlertBox ];
                    }
                    [delegate checkRecoveryOptionBOXShownStatus:@""];

                });
                
                //[self alertwithMsg:rd];
            }
            else
            {
                _txt1.text=@"";
                
                [self showRetryOption];
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            _txt1.text=@"";
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            [self enableBtnNext:NO];
            
        }
        
    }];
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    
    NSString *otpString= _txt1.text;
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    
    [dictBody setObject:otpString forKey:@"otp"];//Enter OTP number by user
    
    
    [dictBody setObject:@"sms" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile contact //not supported iphone
    
    
    if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
        
    {
        [dictBody setObject:@"frgtmpn" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
        
    }
    
    
    
    
    
    else if (TYPE_LOGIN_CHOOSEN == IS_FROM_MOBILE_NUMBER_REGISTRATION)
    {
        [dictBody setObject:@"rgtmob" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
        
    }
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VALIDATE_OTP withBody:dictBody andTag:TAG_REQUEST_VALIDATE_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            // NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
            //   NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                questionsArray = [[[response valueForKey:@"quesid"] componentsSeparatedByString:@","] mutableCopy];
                
                [self alertwithMsg:rd];
            }else{
                _txt1.text=@"";
                
                [self showRetryOption];
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            _txt1.text=@"";
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            [self enableBtnNext:NO];

        }
        
    }];
    
    
}




-(void)alertwithMsg:(NSString*)msg
{
    if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        tbc.selectedIndex=[singleton getSelectedTabIndex];
        [self presentViewController:tbc animated:NO completion:nil];
        
    }
    else if (TYPE_LOGIN_CHOOSEN == ISFROMLOGINWITHOTP)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        tbc.selectedIndex=[singleton getSelectedTabIndex];
        [self presentViewController:tbc animated:NO completion:nil];
    }
    
   /* else if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
    {
        [self openResetPinView];
        
    }*/
    else if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
    {
        // Open security Question View
        
        NSString *otpString= _txt1.text;
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DetailService" bundle:nil];

        
        UpdateQuestionsViewController *updateQuesView = [storyboard instantiateViewControllerWithIdentifier:@"UpdateQuestionController"];
        
        updateQuesView.tagComingFrom = @"ForgotMPIN";
        updateQuesView.responseQuestionArray = questionsArray;
        updateQuesView.otpString = otpString;
        //rpvc.otpstr=otpString;
        [updateQuesView setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:updateQuesView animated:NO completion:nil];
        
        
        //[self openResetPinView];
        
    }

    else{
        [self openNextView];
    }
    
    
}



-(void)openResetPinView
{
    NSString *otpString= _txt1.text;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    ResetPinVC *rpvc = [storyboard instantiateViewControllerWithIdentifier:@"ResetPinVC"];
    rpvc.otpstr=otpString;
    [rpvc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:rpvc animated:NO completion:nil];

    
}

// below method is closed due to disable copy paste option
/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(string.length==0)
    {
        return YES;
    }
    else
    {
        NSString *validRegEx =@"^[0-9]$"; //change this regular expression as your requirement
        NSPredicate *regExPredicate =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", validRegEx];
        BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:string];
        if (myStringMatchesRegEx)
            return YES;
        else
            return NO;
    }
}
*/
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+150)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];
    
    [self setFontforView:self.view andSubViews:YES];
}





- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [self.txt1 resignFirstResponder];
            
            
        }
    }
}

-(void)openNextView
{
    SetMPINForMobileVC *vc;
    if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        vc = [[SetMPINForMobileVC alloc] initWithNibName:@"SetMPINForMobileVC_iPad" bundle:nil];
        //loadNibNamed:@"MobileRegistrationVC_ipad" owner:self options:nil];
        // cell = (MobileRegistrationVC_ipad *)[nib objectAtIndex:0];
        [self presentViewController:vc animated:YES completion:nil];
        
        
    }
    
    else
        
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        vc = [storyboard instantiateViewControllerWithIdentifier:@"SetMPINForMobileVC"];
        //   RS3vc.isFromAadharRegistration = YES;
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        //[self.navigationController pushViewController:vc animated:YES];
        [self presentViewController:vc animated:NO completion:nil];
    }
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/




@end

