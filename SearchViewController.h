//
//  SearchViewController.h
//  Umang
//
//  Created by spice_digital on 06/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    IBOutlet UITableView *tbl_search;
    IBOutlet UITextField *txt_search;
    IBOutlet UIView *vw_line;
    NSArray *searchResults;
    
    NSMutableArray *pastServices;
    NSMutableArray *autocompleteService;
    
    IBOutlet UIButton *btn_closeSearch;
    IBOutlet UIButton *btn_filter;

}
-(IBAction)btn_filter:(id)sender;

-(IBAction)btnSearchClose:(id)sender;
-(IBAction)cancelBtnAction:(id)sender;
@end

