//
//  AadharRegModuleVC.m
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import "AadharRegModuleVC.h"
#import "MyTextField.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "UserEditVC.h"
#define kOFFSET_FOR_KEYBOARD 80.0

#import "AadharModuleOTPVerifyVC.h"




@interface AadharRegModuleVC ()<MyTextFieldDelegate,UIScrollViewDelegate>

{
    BOOL flag_Accept;
    MBProgressHUD *hud;
    NSString *aadharStr;
    NSString *maskedMobilenum;
    
    __weak IBOutlet UILabel *lblTips;
    __weak IBOutlet UIButton *btnBack;
    SharedManager *singleton;
    IBOutlet UIScrollView *scrollview;
    int tagAPI;
    
    
    __weak IBOutlet UILabel *lblHeader;
    __weak IBOutlet UILabel *lblRegisterUsingAadhar;
    
    __weak IBOutlet UILabel *lblSendOTP;
    
    __weak IBOutlet UILabel *lblAgreeTerms;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet MyTextField *txt_aadhar1;
@property (weak, nonatomic) IBOutlet MyTextField *txt_aadhar2;
@property (weak, nonatomic) IBOutlet MyTextField *txt_aadhar3;

@end



@implementation AadharRegModuleVC

@synthesize txt_aadhar1,txt_aadhar2,txt_aadhar3;
@synthesize tout,rtry;

- (id)initWithNibName:(NSString* )nibNameOrNil bundle:(NSBundle* )nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        self =[super initWithNibName:@"AadharRegModuleVC_iPad" bundle:nil];
    }
    return self;
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeader.font = [AppFont semiBoldFont:22.0];
    lblSendOTP.font = [AppFont mediumFont:14];
    lblRegisterUsingAadhar.font = [AppFont semiBoldFont:16.0];
    txt_aadhar1.font = [AppFont regularFont:21.0];
    txt_aadhar2.font = [AppFont regularFont:21.0];
    txt_aadhar3.font = [AppFont regularFont:21.0];
    lblAgreeTerms.font = [AppFont mediumFont:13];
    lblTips.font = [AppFont lightFont:13];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (void)viewDidLoad
{

    [super viewDidLoad];
    NSString *str = NSLocalizedString(@"tip", nil);
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;

    
    lblTips.text = [NSString stringWithFormat:@"%@%@",str,NSLocalizedString(@"aadhaar_mob_tip", nil)];
    

    [super viewDidLoad];
     [txt_aadhar1 becomeFirstResponder];
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:AADHAR_REGISTRATION_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    // Do any additional setup after loading the view.
    flag_Accept=TRUE;
    singleton=[SharedManager sharedSingleton];
    //----- Setting delegate for Custom textfield so back space operation work smooth
    
    
    
    lblHeader.text = NSLocalizedString(@"registration_label", nil);
    lblRegisterUsingAadhar.text = NSLocalizedString(@"aadhar_reg_intro", nil);
    
    
    lblAgreeTerms.text = NSLocalizedString(@"aadhaar_consent_txt", nil);
     lblAgreeTerms.adjustsFontSizeToFitWidth = YES;
    lblSendOTP.text = NSLocalizedString(@"aadhar_sub_intro", nil);
    

    
    txt_aadhar1.myDelegate = self;
    txt_aadhar2.myDelegate = self;
    txt_aadhar3.myDelegate = self;
    
    [txt_aadhar1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [txt_aadhar2 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [txt_aadhar3 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _btnSkip.hidden = YES;
    
    self.btn_next.enabled=NO;
    self.view.userInteractionEnabled = YES;
    
    
    [self.btn_acceptTerm setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
    [self.btn_acceptTerm setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateSelected];
    
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    
    //--------- Code for handling -------------------
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollView.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height+100;
                       scrollView.contentSize = contentRect.size;
                   });
    
    
    
}
- (IBAction)btnSkipClicked:(id)sender
{
    
    
}


- (IBAction)btnAccept_termsClicked:(UIButton*)sender
{
    sender.selected = !(sender.selected);
    
    
    if (((sender.selected == YES))&&(aadharStr.length ==  12))
    {
        [self enableBtnNext:YES];
        self.btn_next.enabled=YES;
        
    }
    else
    {
        [self enableBtnNext:NO];
        self.btn_next.enabled=NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





-(void)hideKeyboard
{
    //[self.view endEditing:YES];
    [scrollView setContentOffset:
     CGPointMake(0, -scrollView.contentInset.top) animated:YES];
    
    [txt_aadhar1 resignFirstResponder];
    [txt_aadhar2 resignFirstResponder];
    [txt_aadhar3 resignFirstResponder];
    
    
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void)textFieldDidDelete:(UITextField *)textField
{
    
    if (textField.text.length == 0)
    {
        
        if ([textField isEqual:txt_aadhar3])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [txt_aadhar2 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar2])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [txt_aadhar1 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar1])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [textField resignFirstResponder];
        }
        
    }
    
}


//---Code for hangle jump from one textfield to another while filling values
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.text.length >= 4)
    {
        
        if ([textField isEqual:txt_aadhar1])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [txt_aadhar2 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar2])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [txt_aadhar3 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar3])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [textField resignFirstResponder];
        }
        
        
        [self checkValidation];
        
        // NSLog(@"got it");
    }
    else
    {
        flag_Accept=FALSE;
        [self btnAcceptTerm:self];
        
    }
    
    
}


-(void)checkValidation
{
    aadharStr=[NSString stringWithFormat:@"%@%@%@",txt_aadhar1.text, txt_aadhar2.text, txt_aadhar3.text];
    
    if (aadharStr.length <12)
    {
        //Enable button and jump to next view
        
        [self enableBtnNext:NO];
        self.btn_next.enabled=NO;
        
        
    }
    else
    {
        
        
    }
    
    
}


-(void)enableBtnNext:(BOOL)status
{
    if (status == YES)
    {
        [self hideKeyboard];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];

    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+150)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];
    [self setFontforView:self.view andSubViews:YES];
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.txt_aadhar1 resignFirstResponder];
        [self.txt_aadhar2 resignFirstResponder];
        [self.txt_aadhar3 resignFirstResponder];
        
    }
}

- (IBAction)btnAcceptTerm:(id)sender
{
    
    if (flag_Accept==TRUE) {
        self.btn_acceptTerm.selected=TRUE;
        flag_Accept=FALSE;
        
        
    }
    else
    {
        self.btn_acceptTerm.selected=FALSE;
        flag_Accept=TRUE;
        
        
        
    }
    [self checkValidation];
}


- (IBAction)btnNextClicked:(id)sender
{
    
    [self hitAPI];
    
    
    
    
}



-(void)hitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
   hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    
    [dictBody setObject:aadharStr forKey:@"aadhr"];
    [dictBody setObject:@"aadhar" forKey:@"type"];

    //  singleton.mobileNumber=txtMobileNumber.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_REGISTRATION withBody:dictBody andTag:TAG_REQUEST_INIT_REG completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         
         if (error == nil) {
             NSLog(@"Server Response = %@",response);
             //------ Sharding Logic parsing---------------
             NSString *node=[response valueForKey:@"node"];
             if([node length]>0)
             {
                 [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
             }
             

             //------ Sharding Logic parsing---------------
             

             //----- below value need to be forword to next view according to requirement after checking Android apk-----
           //  NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
           //  NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
             NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"maskmno"];
             
         //    NSString *rc=[response valueForKey:@"rc"];
         //    NSString *rd=[response valueForKey:@"rd"];
         //    NSString *rs=[response valueForKey:@"rs"];
             tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
             rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
             
             
        
             //----- End value need to be forword to next view according to requirement after checking Android apk-----
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
             {
                 
                 //    singleton.mobileNumber=txtMobileNumber.text;
                 
                 AadharModuleOTPVerifyVC *vc;
                 //869441
                 if ([[UIScreen mainScreen]bounds].size.height == 1024)
                 {
                     vc = [[AadharModuleOTPVerifyVC alloc] initWithNibName:@"AadharModuleOTPVerifyVC_iPad" bundle:nil];
                     vc.strAadharNumber = aadharStr;
                     vc.rtry=rtry;
                     vc.maskedMobileNumber = wmsg;
                     vc.tout=tout;
                     
                     vc.lblScreenTitleName.text = @"Mobile Number Verification";
                     [self presentViewController:vc animated:YES completion:nil];
                     
                     
                 }
                 
                 else
                 {
                     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

                 
                     vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharModuleOTPVerifyVC"];
                     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                     vc.strAadharNumber = aadharStr;
                     vc.rtry=rtry;
                     vc.maskedMobileNumber = wmsg;
                     vc.tout=tout;
                     
                     vc.lblScreenTitleName.text = @"Mobile Number Verification";
                     
                     [self presentViewController:vc animated:YES completion:nil];
             }
             //             682647029382
         }
         }
         else{
             
             

             NSLog(@"Error Occured = %@",error.localizedDescription);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                             message:error.localizedDescription
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                   otherButtonTitles:nil];
             [alert show];
             txt_aadhar1.text=@"";
             txt_aadhar2.text=@"";
             txt_aadhar3.text=@"";
             
             [self enableBtnNext:NO];
             self.btn_next.enabled=NO;

             self.btn_acceptTerm.selected=FALSE;
             flag_Accept=FALSE;
             [self checkValidation];
             

         }
         
     }];
    
}


- (IBAction)btnBackClicked:(id)sender {
    
    
   // [self.navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}



#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
//#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
//- (NSUInteger)supportedInterfaceOrientations
//#else
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations
//#endif
//{
//    // Return a bitmask of supported orientations. If you need more,
//    // use bitwise or (see the commented return).
//    return UIInterfaceOrientationMaskPortrait;
//    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
//}
//
//- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
//    // Return the orientation you'd prefer - this is what it launches to. The
//    // user can still rotate. You don't have to implement this method, in which
//    // case it launches in the current orientation
//    return UIInterfaceOrientationPortrait;
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
