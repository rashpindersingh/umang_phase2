//
//  DepartmentListingViewController.h
//  Umang
//
//  Created by Lokesh Jain on 11/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DepartmentListingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISearchBar *deptSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *deptTableView;


@property (nonatomic,copy) NSString *viewComingFrom;

@end

#pragma mark - Department Cell Interface
@interface DepartmentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *departmentNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *deptImage;

@end
