//
//  HelpAccountSetVC.m
//  Umang
//
//  Created by admin on 15/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "HelpAccountSetVC.h"
#import "HelpScreenCell.h"
#import "SocialHelpIconCell.h"
#import "FAQWebVC.h"
#import "SubmitQueryVC.h"
#import "MBProgressHUD.h"
#import "UMAPIManager.h"
#import "HelpViewController.h"
//#import "ServiceHeaderCell.h"

@interface HelpAccountSetVC ()<UITableViewDelegate,UITableViewDataSource>
{
    
    __weak IBOutlet UILabel *lblHelpTitle;
    
    __weak IBOutlet UIButton *btnBack;
    SharedManager *singleton;
    NSMutableArray *arrAadharData;
    NSMutableArray *arrAadharHelpName;
    NSMutableArray *arrAadharHelpImage;
    //  HelpScreenCell *helpCell;
    
}


@end

@implementation HelpAccountSetVC
@synthesize tbl_helpSetting;
- (void)viewDidLoad {
    singleton = [SharedManager sharedSingleton];
    
    [super viewDidLoad];
    
    lblHelpTitle.text = NSLocalizedString(@"help", nil);
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    // Do any additional setup after loading the view.
    self.view.backgroundColor  = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    tbl_helpSetting.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    arrAadharHelpName = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"help_faq", nil),NSLocalizedString(@"user_manual", nil), nil];
    
    arrAadharHelpImage = [[NSMutableArray alloc]initWithObjects:@"more_help_support",@"faq_Updated",@"_SocialHelp", nil];
    [self addNavigationView];
}

-(IBAction)backbtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    
    // [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}


/*- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
 {
 
 if (section==0) {
 return 50.00;
 
 }
 if (section==1) {
 return 35.00;
 
 }
 if (section==2) {
 return 35.00;
 
 }
 if (section==3) {
 return 35.00;
 
 }
 if (section==4) {
 return 50.00;
 
 }
 
 else
 
 return 50.00;
 
 
 
 
 }*/
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btnBack.hidden = true;
    lblHelpTitle.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backbtnAction:btnBack];
    };
    nvView.lblTitle.text = lblHelpTitle.text;
   
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tbFrame = self.tbl_helpSetting.frame;
        tbFrame.origin.y = kiPhoneXNaviHeight
        tbFrame.size.height = fDeviceHeight - kiPhoneXNaviHeight;
        self.tbl_helpSetting.frame = tbFrame;
        [self.view layoutIfNeeded];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    [tbl_helpSetting reloadData];
    [self setViewFont];
    [super viewWillAppear:YES];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHelpTitle.font = [AppFont semiBoldFont:17.0];
    
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
/*
 - (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
 {
 
 
 
 if (section == 0)
 {
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text = NSLocalizedString(@"keep_me_logged_in_help_txt", nil);
 
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 
 
 
 
 }
 
 else if (section == 1)
 {
 
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text = NSLocalizedString(@"changemob_small_label_desc", nil);
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 
 
 }
 else if (section == 2)
 
 
 {
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text = NSLocalizedString(@"change_mpin_help_txt", nil);
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 
 }
 else if (section == 3)
 
 
 {
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text =@"Reset your MPIN in case you have forgotten it";
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 
 }
 
 else
 
 {
 
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text = NSLocalizedString(@"delete_profile_help_txt", nil);
 
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 }
 
 
 
 
 return nil;
 }
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section

{
    
    if (section == 0) {
        return 0;
    }
    else
    {
        return  44;
    }
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.section == 0) {
        return 90;
    }
    else
    {
        return 50;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 6;
    }
    else
    {
        return 3;
    }
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    static NSString *CellIdentifierType = @"HelpScreenCell";
    HelpScreenCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
    
    
    
    
    /*
     if (singleton.isArabicSelected==TRUE)
     {
     cell.transform = CGAffineTransformMakeRotation(180*0.0174532925);
     
     cell.lblServiceTitle.transform = CGAffineTransformMakeRotation(-M_PI);
     cell.lblServiceDescription.transform = CGAffineTransformMakeRotation(-M_PI);
     cell.imgService.transform= CGAffineTransformMakeRotation(-M_PI);
     cell.lblServiceTitle.textAlignment=NSTextAlignmentRight;
     cell.btnSwitch.transform=  CGAffineTransformMakeRotation(-M_PI);
     cell.lblServiceTitle.textAlignment=NSTextAlignmentRight;
     cell.lblServiceDescription.textAlignment=NSTextAlignmentRight;
     
     }
     else
     {
     cell.lblServiceTitle.textAlignment=NSTextAlignmentLeft;
     cell.lblServiceDescription.textAlignment=NSTextAlignmentLeft;
     
     }
     */
    if (singleton.isArabicSelected==TRUE)
    {
        cell.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        cell.lblServiceTitle.transform= CGAffineTransformMakeScale(-1.0, 1.0);
        cell.lblServiceDescription.transform= CGAffineTransformMakeScale(-1.0, 1.0);
        cell.lblServiceTitle.textAlignment=NSTextAlignmentRight;
        cell.lblServiceDescription.textAlignment=NSTextAlignmentRight;
    }
    
    else
    {
        cell.lblServiceTitle.textAlignment=NSTextAlignmentLeft;
        cell.lblServiceDescription.textAlignment=NSTextAlignmentLeft;
    }
    
    
    
    if (indexPath.section == 0)
    {
        
//        if (indexPath.row == 0)
//        {
//            cell.lblServiceTitle.text = NSLocalizedString(@"keep_me_logged_in", nil);
//            cell.imgService.image = [UIImage imageNamed:@"logged_in_SS.png"];
//            cell.btnSwitch.hidden=TRUE;
//            cell.lblServiceDescription.text = NSLocalizedString(@"logged_in_help_txt", nil);
//            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
//            
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }
        
        if (indexPath.row == 0)
        {
            ///1
            cell.lblServiceTitle.text = NSLocalizedString(@"changereg_mob_label", nil);
            cell.imgService.image = [UIImage imageNamed:@"regstrd_mobile_number_SS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text = NSLocalizedString(@"changemob_small_label_desc", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            cell.accessoryType = UITableViewCellAccessoryNone;
       
        }
        
        else if (indexPath.row == 1)
        {
            
            
            cell.lblServiceTitle.text = NSLocalizedString(@"changempin_small_label", nil);
            cell.imgService.image = [UIImage imageNamed:@"change_MPIN_SS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text = NSLocalizedString(@"changempin_small_label_desc", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        else if (indexPath.row == 2)
            
            
        {
            cell.lblServiceTitle.text = NSLocalizedString(@"forgot_mpin", nil);
            cell.imgService.image = [UIImage imageNamed:@"forgot_mpin_SS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text = NSLocalizedString(@"forgot_mpin_desc", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
           
            
        }
        else if (indexPath.row == 3)
            
        {
            cell.lblServiceTitle.text = NSLocalizedString(@"account_recovery_options", nil).capitalizedString;//NSLocalizedString(@"update_security_ques", nil);
            cell.imgService.image = [UIImage imageNamed:@"icon_security_que.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text = NSLocalizedString(@"manage_recovery_options", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            
        }
        
        else if (indexPath.row == 4)
        {
            
            cell.lblServiceTitle.text = NSLocalizedString(@"logged_in_sessions", nil);
            cell.imgService.image = [UIImage imageNamed:@"icon_logged_in"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text = NSLocalizedString(@"logout_session_desc", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            
            cell.accessoryType = UITableViewCellAccessoryNone;

        }
        else if (indexPath.row == 5)
        {
            cell.lblServiceTitle.text = NSLocalizedString(@"deleteprofile_label", nil);
            cell.imgService.image = [UIImage imageNamed:@"delete_profile_SS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text = NSLocalizedString(@"deleteprofile_small_label_desc", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lblServiceDescription.font=[AppFont regularFont:15];
        cell.lblServiceTitle.font=[AppFont regularFont:17];
        return cell;
        
    }
    else
    {
        
        static NSString *simpleTableIdentifier = @"HelpCell";
        
        SocialHelpIconCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (cell == nil)
        {
            cell = [[SocialHelpIconCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.textLabel.text = [arrAadharHelpName objectAtIndex:indexPath.row];
       
        cell.textLabel.font = [AppFont regularFont:17.0];
        cell.imageView.image = [UIImage imageNamed:[arrAadharHelpImage objectAtIndex:indexPath.row]];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.backgroundColor = [UIColor whiteColor];
        
        
        if (singleton.isArabicSelected==TRUE)
        {
            cell.transform = CGAffineTransformMakeRotation(180*0.0174532925);
            
            cell.textLabel.transform = CGAffineTransformMakeRotation(-M_PI);
            cell.imageView.transform = CGAffineTransformMakeRotation(-M_PI);
            cell.textLabel.textAlignment=NSTextAlignmentRight;
            
            
            
        }
        else
        {
            cell.textLabel.textAlignment=NSTextAlignmentLeft;
            
        }
        
        
        
        return cell;
    }
    
    
}




- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    //check header height is valid
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,50)];
    headerView.backgroundColor=[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(15,15,300,20);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:0.298 green:0.337 blue:0.424 alpha:1.0];
    /*
     
     CGRect labelFrame =  label.frame;
     labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
     label.frame = labelFrame;
     */
    
    if (singleton.isArabicSelected==TRUE)
    {
        label.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        label.textAlignment=NSTextAlignmentLeft;
        
    }
    
    
    label.font = [AppFont boldFont:14.0];

    label.text = NSLocalizedString(@"more_help_lbl", nil);
    
    
    [headerView addSubview:label];
    
    return headerView;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0)
    {
    }
    
    else
    {
        
        if (indexPath.row==0)//tab picker
        {
            
            [self myHelp_Action];
            
        }
        
        if (indexPath.row==1)//language picker
        {
            
            
            [self openFAQWebVC];
            
        }
        
        if (indexPath.row==2)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

            FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
            if ([singleton.arr_initResponse  count]>0) {
                vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"usrman"];
                
            }
            
            vc.titleOpen= NSLocalizedString(@"user_manual", nil);
            // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
    
}

-(void)myHelp_Action
{
    NSLog(@"My Help Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)openFAQWebVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.isfrom = @"Settings";
    
    if ([singleton.arr_initResponse  count]>0) {
        vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"faq"];
        
    }
    vc.titleOpen= NSLocalizedString(@"help_faq", nil);
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

@end
