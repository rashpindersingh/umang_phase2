//
//  AadharOTPVerifyAfterMobVC.h
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AadharOTPVerifyAfterMobVC : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIView *resendOTPview;
    IBOutlet UIScrollView *scrollView;
    
}

@property(nonatomic,retain)NSString *altmobile;
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
@property(nonatomic,retain)NSString *TAGFROM;
@property (weak, nonatomic) IBOutlet UILabel *maskedMobileLabel;
@property (nonatomic,copy)NSString *mobileNumber;

//--- for handling all resend and call option------


@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property(nonatomic,retain)IBOutlet UIScrollView *scrollView;

@property(strong,nonatomic) NSString *strAadharNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblScreenTitleName;


@end

