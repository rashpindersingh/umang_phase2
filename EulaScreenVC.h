//
//  EulaScreenVC.h
//  Umang
//
//  Created by admin on 28/12/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol EulaChangeDelegate <NSObject>
- (void)EulaAcceptChanged:(BOOL)Acceptstatus;
@end




@interface EulaScreenVC : UIViewController
{
    IBOutlet UIImageView *img_logo;
    IBOutlet UITextView *txt_eula;
    IBOutlet UIButton *btnAccept;
    IBOutlet UILabel *lbl_eulaHeading;
    IBOutlet UIView *vw_bottom;
    BOOL flagAccept;
    IBOutlet UIButton *btn_cancel;
    
}
@property (nonatomic, weak) id<EulaChangeDelegate> EulaDelegate;
@property (nonatomic, assign) BOOL Acceptstatus;

-(IBAction)btnAccept:(id)sender;
-(IBAction)btncancel:(id)sender;


@end


