//
//  ServiceNameCell.h
//  Umang
//
//  Created by admin on 28/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceNameCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameService;
@property (weak, nonatomic) IBOutlet UIImageView *imgService;

@end
