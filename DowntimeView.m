//
//  DowntimeView.m
//  UMANG
//
//  Created by Deepak Rawat on 03/01/19.
//  Copyright © 2019 SpiceDigital. All rights reserved.
//

#import "DowntimeView.h"
#import "AppConstants.h"

@implementation DowntimeView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.userInteractionEnabled = YES;

    }
    return self;
}


-(IBAction)btn_clickRetry:(id)sender;
{
    NSLog(@"retryAction");
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;

    //test case for downtime
    /*
    delegate.testCase=101;
    */
    [delegate callInitAPIWit:TAG_REQUEST_RETRY];
    
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
