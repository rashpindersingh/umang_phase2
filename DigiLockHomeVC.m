//
//  DigiLockHomeVC.m
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigiLockHomeVC.h"
#import "DigiLockIssuedVC.h"
#import "DigiLockUploadVC.h"
#import "MBProgressHUD.h"
#import "SharedManager.h"
#import "UMAPIManager.h"

#import "DigiDownloadViewController.h"
#import "Base64.h"
#import "MoreTabVC.h"


@interface DigiLockHomeVC ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    __weak IBOutlet UIButton *btnMore;
    __weak IBOutlet UILabel *lblUploadDoc;
    __weak IBOutlet UILabel *lblShareDoc;
    __weak IBOutlet UILabel *lbllinkDigilocker;
    SharedManager *singleton;
    __weak IBOutlet UILabel *lblHeaderDigilocker;
    
    UIImageView   *imglinkDigi;
    UIImageView   *imglinkArrow;
    
    UIImageView   *imgDocument;
    UIImageView *imgArrowDocument;
    
    UIImageView   *imgSharing;
    UIImageView *imgArrow;
    
    MBProgressHUD *hud;
}
@end

@implementation DigiLockHomeVC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    singleton = [SharedManager sharedSingleton];
    
    [btnMore setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnMore.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnMore.frame.origin.x, btnMore.frame.origin.y, btnMore.frame.size.width, btnMore.frame.size.height);
        
        
        [btnMore setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnMore.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:DIGILOCKER_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    [self addNavigationView];
}


-(void)loadbuttons
{
    
    
    lblHeaderDigilocker.text = NSLocalizedString(@"digi_locker", nil);
    btn_shareDocument.layer.cornerRadius = 5.0;
    btn_uploadDocument.layer.cornerRadius = 5.0;
    btn_linkdigilocker.layer.cornerRadius=5.0;
    
    imgSharing = [[UIImageView alloc]initWithFrame:CGRectMake(10,( btn_shareDocument.frame.size.height/2)-16, 32, 32)];
    imgSharing.image = [UIImage imageNamed:@"issued_documents_icon"];
    [btn_shareDocument setTitle: NSLocalizedString(@"issued_documents", nil) forState:UIControlStateNormal];
    btn_shareDocument.titleLabel.font = [UIFont systemFontOfSize:19.0];
    [btn_shareDocument addSubview:imgSharing];
    
    
    imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(btn_shareDocument.frame.size.width-30,(btn_shareDocument.frame.size.height/2)-12, 20, 25)];
    [btn_shareDocument addSubview:imgArrow];
    imgArrow.image = [UIImage imageNamed:@"arrow_digi.png"];
    
    
    
    imgDocument = [[UIImageView alloc]initWithFrame:CGRectMake(10,(btn_uploadDocument.frame.size.height/2)-16, 32, 32)];
    [btn_uploadDocument addSubview:imgDocument];
    [btn_uploadDocument setTitle: NSLocalizedString(@"uploaded_documents", nil) forState:UIControlStateNormal];
    btn_uploadDocument.titleLabel.font = [UIFont systemFontOfSize:19.0];
    imgDocument.image = [UIImage imageNamed:@"upload_documents.png"];
    
    imgArrowDocument = [[UIImageView alloc]initWithFrame:CGRectMake(btn_uploadDocument.frame.size.width-30,(btn_uploadDocument.frame.size.height/2)-12, 20, 25)];
    [btn_uploadDocument addSubview:imgArrowDocument];
    imgArrowDocument.image = [UIImage imageNamed:@"arrow_digi.png"];
    
    
    lblShareDoc.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    lblUploadDoc.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    lblShareDoc.text =  NSLocalizedString(@"issued_document_desc", nil);
    
    lblUploadDoc.text = NSLocalizedString(@"upload_documents_txt", nil);
    
    
    
    NSString *checkLinkStatus=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"LINKDIGILOCKERSTATUS"];
    
    
    
    if (checkLinkStatus == nil|| [checkLinkStatus isEqualToString:@"NO"])
    {
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //------------------------- Encrypt Value------------------------
        
        
        
        
        
        lbllinkDigilocker.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        lbllinkDigilocker.text = NSLocalizedString(@"digilocker_document_sub", nil);
        
        imglinkDigi = [[UIImageView alloc]initWithFrame:CGRectMake(10,( btn_linkdigilocker.frame.size.height/2)-16, 32, 32)];
        imglinkDigi.image = [UIImage imageNamed:@"download_cellicon"];
        [btn_linkdigilocker setTitle: NSLocalizedString(@"digilocker_document", nil) forState:UIControlStateNormal];
        [btn_linkdigilocker setTitle: NSLocalizedString(@"digilocker_document", nil) forState:UIControlStateSelected];
        btn_linkdigilocker.titleLabel.font = [UIFont systemFontOfSize:19.0];
        [btn_linkdigilocker addSubview:imglinkDigi];
        imglinkArrow = [[UIImageView alloc]initWithFrame:CGRectMake(btn_linkdigilocker.frame.size.width-30,(btn_linkdigilocker.frame.size.height/2)-12, 20, 25)];
        [btn_linkdigilocker addSubview:imgArrow];
        imglinkArrow.image = [UIImage imageNamed:@"arrow_digi.png"];
        
        
        lbllinkDigilocker.hidden=FALSE;
        btn_linkdigilocker.hidden=FALSE;
        imglinkDigi.hidden=FALSE;
        
    }
    else  if ([checkLinkStatus isEqualToString:@"YES"])
    {
        lbllinkDigilocker.hidden=TRUE;
        btn_linkdigilocker.hidden=TRUE;
        imglinkDigi.hidden=TRUE;
    }
    
}


-(void)updateFrame

{
    imglinkDigi.frame = CGRectMake(10,( btn_linkdigilocker.frame.size.height/2)-16, 32, 32);
    imglinkArrow.frame =CGRectMake(btn_linkdigilocker.frame.size.width-30,(btn_linkdigilocker.frame.size.height/2)-12, 20, 25);
    
    
    imgDocument.frame = CGRectMake(10,(btn_uploadDocument.frame.size.height/2)-16, 32, 32);
    imgArrowDocument.frame = CGRectMake(btn_uploadDocument.frame.size.width-30,(btn_uploadDocument.frame.size.height/2)-12, 20, 25);
    
    imgSharing.frame = CGRectMake(10,( btn_shareDocument.frame.size.height/2)-16, 32, 32);
    imgArrow.frame = CGRectMake(btn_shareDocument.frame.size.width-30,(btn_shareDocument.frame.size.height/2)-12, 20, 25);
    
    
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    
    [self updateFrame];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btnMore.hidden = true;
    lblHeaderDigilocker.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] initSecondBarItemsView];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backbtnAction:btnBack];
    };
    
    nvView.didTapRightBarButton  = ^(id btnBack) {
        [weakSelf btn_LogoutDigilocker:btnBack];
    };
    [nvView.rightRightBarButton setImage:[UIImage imageNamed:@"logout_digi"] forState:UIControlStateNormal];
    [nvView.secondRightBarButtobn setImage:[UIImage imageNamed:@"upload_digi"] forState:UIControlStateNormal];
    nvView.didTapSecondRightBarButton  = ^(id btnBack) {
        [weakSelf btn_DocumentUpload:btnBack];
    };
    //[nvView.rightRightBarButton setHidden:false];
    nvView.lblTitle.text = lblHeaderDigilocker.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
//    if (iPhoneX()) {
////        CGRect tbFrame = _webView.frame;
////        tbFrame.origin.y = kiPhoneXNaviHeight
////        tbFrame.size.height = fDeviceHeight -kiPhoneXNaviHeight;
////        _webView.frame = tbFrame;
////        [self.view layoutIfNeeded];
//    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [self updateFrame];
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    
    
    
    
    
    
    [self loadbuttons];
    // [self setNeedsStatusBarAppearanceUpdate];
    //  [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:.1];
    /*UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backbtnAction:)];
     [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
     [self.navigationController.view addGestureRecognizer:gestureRecognizer];
     */
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
}



-(IBAction)backbtnAction:(id)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"DigiLocker Home" andValue:0];

    [self.navigationController popToRootViewControllerAnimated:YES];
    
}


-(IBAction)btn_shareDocument:(id)sender
{
    [singleton traceEvents:@"Issue Document Button" withAction:@"Clicked" withLabel:@"DigiLocker Home" andValue:0];

    NSLog(@"Issued Documents");
    
    //[[NSUserDefaults standardUserDefaults] setValue:accessToken forKey:@"AccessTokenDigi"];
    //[[NSUserDefaults standardUserDefaults] setValue:refreshToken forKey:@"RefreshTokenDigi"];
    
    //gil
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DigiLockIssuedVC *issuedDocVC = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockIssuedVC"];
    [self.navigationController pushViewController:issuedDocVC animated:YES];
    
}




-(IBAction)btn_uploadDocument:(id)sender
{
    [singleton traceEvents:@"Upload Document Button" withAction:@"Clicked" withLabel:@"DigiLocker Home" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DigiLockUploadVC *uploadVC = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockUploadVC"];
    [self.navigationController pushViewController:uploadVC animated:YES];
    
}

-(IBAction)btn_LogoutDigilocker:(id)sender
{

    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"logout_msg_txt", nil) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [singleton traceEvents:@"Logout Digilocker Button" withAction:@"Clicked" withLabel:@"DigiLocker Home" andValue:0];

                                       [self hitAPIToLogoutDigiLocker];
                                       
                                   }];
    
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
    {
        [singleton traceEvents:@"Logout Digilocker Cancel Button" withAction:@"Clicked" withLabel:@"DigiLocker Home" andValue:0];

    }];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(IBAction)btn_DocumentUpload:(id)sender
{

    [self openChooseFromSheet];
    
}

-(IBAction)btn_linkdigilocker:(id)sender
{
    NSLog(@"Downloaded Documents");

    [singleton traceEvents:@"Open Digilocker Document View" withAction:@"Clicked" withLabel:@"DigiLocker Home" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DigiDownloadViewController *downloadVC = [storyboard instantiateViewControllerWithIdentifier:@"DownloadDocVC"];
    [self.navigationController pushViewController:downloadVC animated:YES];
    
    
}

#pragma mark - OPEN ACTION SHEET

-(void)openChooseFromSheet
{
    //NSLog(@"openChooseFrom");
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Upload Document"
                                                             delegate:self
                                                    cancelButtonTitle:[NSLocalizedString(@"cancel", nil) capitalizedString]
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:[NSLocalizedString(@"camera", nil) capitalizedString],[NSLocalizedString(@"gallery", nil) capitalizedString], nil];
    
    actionSheet.delegate=self;
    [actionSheet showInView:self.view];
    
    
    
    
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    switch (buttonIndex) {
        case 0:
            [self openCamera];
            break;
        case 1:
            [self openGallary];
            break;
            
        default:
            break;
    }
}

-(void)openCamera
{
    [singleton traceEvents:@"Open Camera" withAction:@"Clicked" withLabel:@"DigiLocker Home" andValue:0];

    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            
            
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
                
                [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
                
                
                
                
                
            } else {
                
                [self presentViewController:picker animated:NO completion:nil];
                
            }
            
        });
        
    }
}

-(void)openGallary
{
    [singleton traceEvents:@"Open Gallery" withAction:@"Clicked" withLabel:@"DigiLocker Home" andValue:0];

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            
            
        }
        else
        {
            [self presentViewController:picker animated:NO completion:nil];
        }
        
    });
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/DigiUploadFolder"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString *fileName = [NSString stringWithFormat:@"%@/Image-%@.jpeg",dataPath,dateString];
    

    NSData *imageData = UIImageJPEGRepresentation(beforeCrop, 1.0);
    
    
    NSString *base64String = [imageData base64Encoding]; //my base64Encoding function
    
    __block NSData *base64data = [[NSData alloc] initWithData:[NSData dataWithBase64EncodedString:base64String]];
    
    
    [base64data writeToFile:fileName atomically:YES];
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        [self hitAPIToUploadDocumentWithData:imageData andFilePath:fileName];
    }];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UPLOAD DOCUMENT API

-(void)hitAPIToUploadDocumentWithData:(NSData *)baseData andFilePath:(NSString *)filePath
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    NSString *accessTokenString = [[NSUserDefaults standardUserDefaults]valueForKey:@"AccessTokenDigi"];
    
    
    
    NSString *fileName = [[filePath componentsSeparatedByString:@"/"] lastObject];
    NSString *path = [NSString stringWithFormat:@"/%@",fileName];
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:accessTokenString forKey:@"utkn"];
    [dictBody setObject:[NSString stringWithFormat:@"%lu",baseData.length] forKey:@"clength"];
    [dictBody setObject:path forKey:@"path"];
    [dictBody setObject:@"image/jpeg" forKey:@"ctype"];
    [dictBody setObject:baseData forKey:@"file"];
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    
    [dictBody setObject:selectedLanguage forKey:@"lang"];
    
    [dictBody setObject:@"app" forKey:@"mod"];
    
    [dictBody setObject:[singleton appUniqueID] forKey:@"did"];
    
    
    NSLog(@"Dictionary is %@",dictBody);
    
    
    [objRequest hitAPIForDigiLockerUploadFileWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_UPLOAD_DOC withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_UPLOADDOC andFilePath:filePath completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        [hud hideAnimated:YES];
        if (error == nil)
        {
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"uploaded_successfully", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                               {
                                                   
                                                   
                                                   
                                               }];
                [alert addAction:cancelAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else
        {
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                        {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }];
            
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        
    }];
}


#pragma mark - LOGOUT API

-(void)hitAPIToLogoutDigiLocker
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    
    
    NSString *accessTokenString = [[NSUserDefaults standardUserDefaults]valueForKey:@"AccessTokenDigi"];
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:accessTokenString forKey:@"utkn"];
    [dictBody setObject:@"access_token" forKey:@"tokenType"];
    
    NSLog(@"Dictionary is %@",dictBody);
    
    //
    
    
    
    
    
    [objRequest hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_LOGOUT_DIGI withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_GETTOKEN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil)
        {
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                NSLog(@"Logout Successful");
                
                
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"AccessTokenDigi"];
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"RefreshTokenDigi"];
                
                NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                
                for (UIViewController *aViewController in allViewControllers) {
                    if ([aViewController isKindOfClass:[MoreTabVC class]]) {
                        [self.navigationController popToViewController:aViewController animated:YES];
                    }
                }
                
            }
            
        }
        else
        {
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                        {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }];
            
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        
    }];
    
}


#pragma mark --
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


@end
