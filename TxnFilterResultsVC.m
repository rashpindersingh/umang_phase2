//
//  TxnFilterResultsVC.m
//  Umang
//
//  Created by admin on 22/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "TxnFilterResultsVC.h"

#import "TransactionCell.h"
#import "UMAPIManager.h"
#import "UIImageView+WebCache.h"
#import "TxnDetailVC.h"
#import "AppConstants.h"
#import "SDCapsuleButton.h"

#import "TransactionalHistoryVC.h"

@interface TxnFilterResultsVC()  <UITableViewDelegate,UITableViewDataSource>
{
    
    __weak IBOutlet UILabel *lblNoTransaction;
    
    SharedManager *singleton;
    
    NavigationFilterScollResultView *navFilterScrollView;

}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end



@implementation TxnFilterResultsVC
@synthesize arrTransactionHistory;
@synthesize dictFilterParams;





- (void)viewDidLoad {
    
    if (iPhoneX())
    {
        
        navFilterScrollView = [[NavigationFilterScollResultView alloc] init];
        NSLog(@"An iPhone X Load UI");
    }
    singleton = [SharedManager sharedSingleton];

    [super viewDidLoad];
    
    
    lblNoTransaction.text = NSLocalizedString(@"no_transaction_found", nil);
    _tblTransactionHistory.hidden=FALSE;
    vw_noresults.hidden=TRUE;
    
    [self addFilterOptionsAtNavigationBar];
    
    if ([arrTransactionHistory count]<=0) {
        _tblTransactionHistory.hidden=TRUE;
        vw_noresults.hidden=FALSE;
    }
    else
      //  lbl_noresult.font = [AppFont regularFont:14.0];

    {
        _tblTransactionHistory.hidden=FALSE;
        vw_noresults.hidden=TRUE;
        
    }
    
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    // Do any additional setup after loading the view.
    
    
    [self addNavigationFilterResultScrollView];
    
}


#pragma mark- add Navigation View to View

-(void)addNavigationFilterResultScrollView
{
    
    if (iPhoneX())
    {
        
        // nvSearchView = [[NavigationSearchView alloc] init];
        // NavigationSearchView *nvSearchView = [[NavigationSearchView alloc] init];
        
        __weak typeof(self) weakSelf = self;
        
        navFilterScrollView.didTapRightFilterBarButton = ^(id btnfilter)
        {
            
            [weakSelf btnSettingAgainClicked:btnfilter];
            
        };
        
        
        navFilterScrollView.didTapBackButton = ^(id btnBack)
        {
            
            [weakSelf btnFilterClicked:btnBack];
            
        };
        navFilterScrollView.scroll.delegate=self;
        
        
        [self.view addSubview:navFilterScrollView];
        
        CGRect table = _tblTransactionHistory.frame;
        table.origin.y = kiPhoneXNaviHeight;
        table.size.height = fDeviceHeight - kiPhoneXNaviHeight;
        _tblTransactionHistory.frame = table;
        [self.view layoutIfNeeded];
        
        
    }
    else
    {
        NSLog(@"Not an iPhone X use default UI");
    }
    
}


-(void)addFilterOptionsAtNavigationBar
{
    
    [([UIScreen mainScreen].bounds.size.height == 812.0 ?[navFilterScrollView.scroll subviews] : [self.scrollView subviews]) makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    // [[self.scrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // First fetch State Name
    
    CGFloat xCord = 10;
    CGFloat yCord = 0;
    CGFloat padding = 5;
    CGFloat itemHeight = 30.0;
    
    
    // Now fetch Categories
    
    
    NSMutableArray *arrCategories = [self.dictFilterParams objectForKey:@"category_type"];
    for(int i = 0   ;i <arrCategories.count ;i++){
        
        NSString *serviceName = arrCategories[i];
        
        CGRect frame = [self rectForText:serviceName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:serviceName];
        
        btn.tag = 2300;
        btn.btnCross.hidden = YES;
        btn.userInteractionEnabled = NO;
        
        
        [([UIScreen mainScreen].bounds.size.height == 812.0 ?navFilterScrollView.scroll : self.scrollView)  addSubview:btn];
        
        //[self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
        
        
    }
    
    NSMutableArray *arrServices = [self.dictFilterParams objectForKey:@"service_type"];
    
    for(int i = 0   ;i <arrServices.count ;i++){
        
        NSString *serviceName = arrServices[i];
        
        CGRect frame = [self rectForText:serviceName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        
        [btn setBtnTitle:serviceName];
        
        btn.btnCross.hidden = YES;
        btn.userInteractionEnabled = NO;
        
        
        [([UIScreen mainScreen].bounds.size.height == 812.0 ?navFilterScrollView.scroll : self.scrollView)  addSubview:btn];
        
        //[self.scrollView addSubview:btn];
        
        xCord+=frame.size.width + padding;
    }
    
    
    
    CGFloat height =([UIScreen mainScreen].bounds.size.height == 812.0 ?navFilterScrollView.scroll.frame.size.height : self.scrollView.frame.size.height);
    
    [([UIScreen mainScreen].bounds.size.height == 812.0 ?navFilterScrollView.scroll : self.scrollView) setContentSize:CGSizeMake(xCord, height)];
    
    
    //  [self.scrollView setContentSize:CGSizeMake(xCord, self.scrollView.frame.size.height)];
    
    NSString *sort_byType = [self.dictFilterParams objectForKey:@"sort_by"];
    
    if ([sort_byType isEqualToString:@"date_time"])
    {
        [self sortArrayBasedOnDate];
    }
    else
    {
        
    }
    
    //alpha
    
    
}


-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)sortArrayBasedOnDate
{
    
    NSArray *arrSortedItems = nil;
    arrSortedItems = [arrTransactionHistory sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        NSString *ldate1 = [obj1 valueForKey:@"ldate"];
        if ([ldate1 length]>10) {
            ldate1 = [ldate1 substringToIndex:16];
        }
        
        NSString *ldate2 = [obj2 valueForKey:@"ldate"];
        if ([ldate2 length]>10) {
            ldate2 = [ldate2 substringToIndex:16];
        }
        
        NSLog(@"Date 1= %@ and Date2 = %@",ldate1,ldate2);
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        
        [df setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *d1 = [df dateFromString:ldate1];
        NSDate *d2 = [df dateFromString:ldate2];
        
        df = nil;
        
        return [d2 compare: d1];
    }];
    
    
    
    
    [arrTransactionHistory removeAllObjects];
    [arrTransactionHistory addObjectsFromArray:arrSortedItems];
    [_tblTransactionHistory reloadData];
}



- (IBAction)btnFilterClicked:(id)sender

{
    [singleton traceEvents:@"Filter Button" withAction:@"Clicked" withLabel:@"Transactional Filter Result" andValue:0];


    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    for (UIViewController *aViewController in allViewControllers)
    {
        if ([aViewController isKindOfClass:[TransactionalHistoryVC class]])
        {
            // [aViewController removeFromParentViewController];
            [self.navigationController popToViewController:aViewController
                                                  animated:YES];
            
        }
        
        
    }
    
    
    
}

- (IBAction)btnSettingAgainClicked:(id)sender
{
    [singleton traceEvents:@"Setting Button" withAction:@"Clicked" withLabel:@"Transactional Filter Result" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
}



- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section

{
    
    return 0.001f;
    
}






- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section

{
    
    
    
    return 0.001;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //Here, for each section, you must return the number of rows it will contain
    
    
    
    return arrTransactionHistory.count;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:NO];
    
}
#pragma mark- Font Set to View
-(void)setViewFont{
    
    lblNoTransaction.font= [AppFont regularFont:14.0];
    lbl_noresult.font = [AppFont regularFont:14.0];
    
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return 94;
    
    
    
}







- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *simpleTableIdentifier = @"TransactionCell";
    
    
    
    TransactionCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    
    if (cell == nil) {
        
        cell = [[TransactionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    /*
     
     "dept_resp" = "Appointment Booked Successfully";
     deptid = 402;
     deptname = Health;
     event = "Appointment Booked";
     image = "https://static.umang.gov.in/app/ico/service/ors.png";
     ldate = "2017-01-10 20:47:21.299271";
     sdltid = 17011008000100439;
     servicename = ORS;
     srid = 11;
     status = S;
     tdatezone = "2017-01-10 20:47:21.299271";
     uid = 5796;
     },
     {
     "dept_resp" = "Candidate Added Successfully";
     deptid = 407;
     deptname = Skill;
     event = "Candidate Added";
     image = "https://static.umang.gov.in/app/ico/service/pmkvy.png";
     ldate = "2017-01-12 19:58 :15.080129";
     sdltid = 1269;
     servicename = PMKVY;
     srid = 16;
     status = 1;
     tdatezone = "2017-01-12 19:58:15.080129";
     uid = 5796;
     
     */
    
    
    
    cell.headerTransaction.text = [[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"servicename"];
    cell.descriptnTransaction.text =  [[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"dept_resp"];
    cell.lblCategory.text= [[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"event"];
    cell.lblCategory.font =[AppFont regularFont:15.0];

    if (IS_IPAD || IS_IPAD_2 || IS_IPAD_3)
    {
        CGRect lblFrame=cell.lblCategory.frame;
        cell.lblCategory.frame=CGRectMake(lblFrame.origin.x, lblFrame.origin.y, lblFrame.size.width+100, lblFrame.size.height);
    }
    else
    {
        if (self.view.frame.size.width == 414)
        {
            CGRect lblFrame=cell.lblCategory.frame;
            cell.lblCategory.frame=CGRectMake(lblFrame.origin.x, lblFrame.origin.y, lblFrame.size.width+10, lblFrame.size.height);
        }
        
        cell.lblCategory.adjustsFontSizeToFitWidth = NO;
    }
    NSString *timedate=[[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"ldate"];
    
    timedate = [timedate substringToIndex:16];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *d1 = [df dateFromString:timedate];
    NSString *newDate = [df stringFromDate:d1];
    NSLog(@"New Date = %@",newDate);
    
    
    
    NSDate *date = [df dateFromString: newDate]; // here you can fetch date from string with define format
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *updDate = [df stringFromDate:date];
    NSLog(@"New Date = %@",updDate);
    
    
    cell.lblDateNtime.text=updDate;
    NSLog(@"New Date = %@",newDate);
    
    
    cell.lblDateNtime.font = [UIFont systemFontOfSize:13.0];
    df = nil;
    NSURL *url=[NSURL URLWithString:[[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"image"]];
    [cell.imgTransaction sd_setImageWithURL:url
                           placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];    /*cell.headerTransaction.text=[[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"];
                                                                                             
                                                                                             cell.descriptnTransaction.text=[[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"SERVICE_DESC"];
                                                                                             NSString *imageName= [[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"SERVICE_IMAGE"];
                                                                                             cell.lblCategory.text=[[arrTransactionHistory  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_CATEGORY"];
                                                                                             NSURL *url=[NSURL URLWithString:imageName];*/
    //---------- Set business Unit color here---------------------------------
    cell.headerTransaction.font = [AppFont regularFont:16];
    cell.descriptnTransaction.font = [AppFont lightFont:14.0];
    cell.lblCategory.font = [AppFont regularFont:14.0];
    cell.lblDateNtime.font = [AppFont mediumFont:12.0];
    return cell;
}



- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    [_tblTransactionHistory reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath

{
    
    NSDictionary *celldata=[arrTransactionHistory objectAtIndex:indexPath.row];
    
    if (celldata)
    {
        [singleton traceEvents:@"Open Transactional Detail" withAction:@"Clicked" withLabel:@"Transactional Filter Result" andValue:0];

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        TxnDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"TxnDetailVC"];
        vc.srid=[celldata valueForKey:@"srid"];
        vc.sdltid=[celldata valueForKey:@"sdltid"];
        vc.dic_txnInfo=celldata;
        //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}





- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */


@end

/*
 
 #pragma mark - Navigation
 
 
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
 // Get the new view controller using [segue destinationViewController].
 
 // Pass the selected object to the new view controller.
 
 }
 
 */
