//
//  newAadharDetailTableViewCell.m
//  Umang
//
//  Created by admin on 06/04/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "newAadharDetailTableViewCell.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"


@implementation newAadharDetailTableViewCell
@synthesize TYPE_AADHAR_CHOOSEN,lbllangLocal,lbllangDefault;

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    
    singleton = [SharedManager sharedSingleton];
    
    
    
    UIColor *onColor=[UIColor colorWithRed:70/255.0 green:205/255.0 blue:84/255.0 alpha:1.0];
    UIColor *offColor=[UIColor colorWithRed:70/255.0 green:205/255.0 blue:84/255.0 alpha:1.0];
    
    
    
    self.btn_switch.onTintColor = onColor;
    
    self.btn_switch.tintColor = offColor;
    self.btn_switch.layer.cornerRadius = 16;
    self.btn_switch.backgroundColor = offColor;
    
    
    
    
}



-(void)bindDataForHeaderView
{
    
    CGRect fullFrame = self.frame;
    CGFloat yPos = _imgAadhar.frame.origin.y +_imgAadhar.frame.size.height + 10;
    
    if(TYPE_AADHAR_CHOOSEN == AADHARWITHOUTPIC1)
    {
        _lblAadharInformation.hidden = YES;
        _imgUserProfilePic.hidden = YES;
        _lblAadharNumber.frame = CGRectMake(30, yPos, self.frame.size.width - 60, 30);
        _lblaadharDigit.frame = CGRectMake(30, _lblAadharNumber.frame.origin.y +  50 ,self.frame.size.width - 60, 30);
        fullFrame.size.height = self.frame.size.height - 80;
        
        _lblAadharNumber.text = NSLocalizedString(@"your_aadhaar_number", nil);
        
        
        self.frame = fullFrame;
        
    }
    
    else if (TYPE_AADHAR_CHOOSEN == NOTLINKED1)
    {
        _lblAadharInformation.hidden = YES;
        _imgUserProfilePic.hidden = YES;
        _lblaadharDigit.hidden = YES;
        _lblAadharNumber.frame = CGRectMake(30, yPos, self.frame.size.width - 60, 30);
        
        _lblAadharNumber.text = NSLocalizedString(@"aadhaar_num_not_linked", nil);
        // _lblAadharNumber.font=[UIFont systemFontOfSize:14.0];
        
        _lblAadharNumber.font =[UIFont systemFontOfSize:20 weight:UIFontWeightRegular];
        
        //        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(_lblAadharNumber.frame.origin.x+30, yPos+4, 25, 25)];
        //        [self addSubview:image];
        //        image.image = [UIImage imageNamed:@"icon_not_linked_id"];
        
        fullFrame.size.height = self.frame.size.height - 110;
        // fullFrame.size.height = 268;
        
        self.frame = fullFrame;
        
    }
    
    else if (TYPE_AADHAR_CHOOSEN == AADHARWITHPIC1)
    {
        _lblAadharInformation.text= NSLocalizedString(@"aadhaar_info", nil);
        _lblAadharNumber.text = NSLocalizedString(@"your_aadhaar_number", nil);
        
        _btn_switch.selected=NO;
        
        // _lblAadharInformation.textAlignment =  singleton.isArabicSelected ? NSt : NSTextAlignmentLeft;
        
                NSString *aadharString=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.aadhar_number];
        
        _lblaadharDigit.text =aadharString;

        if ([aadharString length]>0) {
            
            @try {
                NSString*  tempAadharString=[self resetCardNumberAsVisa:aadharString];
                NSString *newAdharString = [tempAadharString substringToIndex:[tempAadharString length]-1];
                _lblaadharDigit.text=newAdharString;
            } @catch (NSException *exception) {
                
            } @finally {
                 
            }
        
        }
        
        UIImage *tempImg;
        //F
        //M
        NSString *gender=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.gender];
        
        if ([gender isEqualToString:@"Male"]||[gender isEqualToString:@"M"])
        {
            tempImg=[UIImage imageNamed:@"male_avatar"];
            
        }
        else if ([gender isEqualToString:@"Female"]||[gender isEqualToString:@"F"])
        {
            tempImg=[UIImage imageNamed:@"female_avatar"];
            
        }
        else
        {
            tempImg=[UIImage imageNamed:@"user_placeholder"];
            
            
        }
        
        _imgUserProfilePic.image=tempImg;
        
        
        
        if (singleton.objUserProfile.objAadhar.aadhar_image_url)
        {
            // donwload image url
            NSURL *url=[NSURL URLWithString:singleton.objUserProfile.objAadhar.aadhar_image_url];
            // [_imgUserProfilePic sd_setImageWithURL:url];
            
            @try {
                
                
                /*[_imgUserProfilePic sd_setImageWithURL:url placeholderImage:tempImg options:SDWebImageRefreshCached];
                self.imgUserProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
                self.imgUserProfilePic.layer.borderWidth = 2.0;*/
                
                [_imgUserProfilePic sd_setImageWithURL:url placeholderImage:tempImg completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                    self.imgUserProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
                    self.imgUserProfilePic.layer.borderWidth = 2.0;
                    
                }];
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            
            
            /*
             dispatch_queue_t queue = dispatch_queue_create("com.spice.umang", NULL);
             dispatch_async(queue, ^{
             
             
             //code to be executed in the background
             NSData *imageData = [NSData dataWithContentsOfURL:url];
             UIImage *imageUser = [UIImage imageWithData:imageData];
             dispatch_async(dispatch_get_main_queue(), ^{
             //code to be executed on the main thread when background task is finished
             //update UI with new database inserted means reload uicollectionview
             _imgUserProfilePic.image = imageUser;
             
             });
             
             });*/
            
        }
        
    }
    //_lblAadharInformation.text= NSLocalizedString(@"aadhaar_info", nil);
    
    
    
}

- (NSString *)resetCardNumberAsVisa:(NSString*)originalString {
    NSMutableString *resultString = [NSMutableString string];
    
    for(int i = 0; i<[originalString length]/4; i++)
    {
        NSUInteger fromIndex = i * 4;
        NSUInteger len = [originalString length] - fromIndex;
        if (len > 4) {
            len = 4;
        }
        
        [resultString appendFormat:@"%@-",[originalString substringWithRange:NSMakeRange(fromIndex, len)]];
    }
    return resultString;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
