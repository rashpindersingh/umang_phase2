//
//  LoginSingupView.h
//  UMANG
//
//  Created by Rashpinder on 08/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginSingupView : UIView
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnArrowUpDown;
@property (weak, nonatomic) IBOutlet UILabel *lblHeading;
@property (weak, nonatomic) IBOutlet UILabel *lblSubHeading;
@property (strong, nonatomic) UIViewController *superVC;
@property (strong, nonatomic)NSString *isShown;

-(id)initWithZeroFrame;
-(void)didShowCompleteView:(BOOL)show;
@property(copy) void(^didTapCrossButton)(UIButton *btnBack);
@end
