//
//  LoginSingupView.m
//  UMANG
//
//  Created by Rashpinder on 08/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "LoginSingupView.h"
#import "LoginAppVC.h"
#import "MobileRegistrationVC.h"
#import "UMANG-Swift.h"
@implementation LoginSingupView

-(id)initWithZeroFrame {
    CGRect frame = [self getRect];
    self = [self initWithFrame:frame];
    [self.btnLogin setTitle:NSLocalizedString(@"login_caps", nil) forState:UIControlStateNormal];
    [self.btnRegister setTitle:NSLocalizedString(@"register_caps", nil) forState:UIControlStateNormal];
    self.lblHeading.text = NSLocalizedString(@"login_to_umang", nil);//@"Login to UMANG";
    self.lblSubHeading.text = NSLocalizedString(@"login_feature_text", nil);
    self.btnArrowUpDown.tag = 200;
    
    return self;
}
- (id)init{
    
    self = [self initWithFrame:[self getRect]];
    [self.btnLogin setTitle:NSLocalizedString(@"login", nil) forState:UIControlStateNormal];
    [self.btnRegister setTitle:NSLocalizedString(@"register", nil) forState:UIControlStateNormal];
    self.lblHeading.text = @"Login to Umang";
    self.lblSubHeading.text = @"Login to UmangLogin to Umang Login to Umang Login to Umang Login to Umang Login to Umang Login to Umang";
    self.btnArrowUpDown.tag = 200;
    
    return self;
    
}
-(void)layouBorderWithCornerRadius:(UIView*)VW borderWidth:(CGFloat)width {
    VW.layer.cornerRadius = 18;
    VW.layer.borderColor = self.btnLogin.backgroundColor.CGColor;
    VW.layer.borderWidth = width;
    VW.layer.masksToBounds = true ;
}
-(CGRect)getRect {
    return  CGRectMake(0, [UIScreen mainScreen].bounds.size.height + 10 ,[UIScreen mainScreen].bounds.size.width , 0.0);
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"LoginSignupXib" owner:self options:nil];
    self = (LoginSingupView *)[objects objectAtIndex:0];
    self.frame = frame;
     self.btnLogin.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:89.0/255.0 blue:157.0/255.0 alpha:1];
    [self layouBorderWithCornerRadius:self.btnLogin borderWidth:0];
    [self layouBorderWithCornerRadius:self.btnRegister borderWidth:1.2];
    [self setViewFonts];
    self.lblSubHeading.textColor = [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1];
   
    [self.btnRegister setTitleColor:self.btnLogin.backgroundColor forState:UIControlStateNormal];
    
    
    
    return self;
}
-(void)setViewFonts {
    self.lblHeading.font = [AppFont regularFont:26];
    self.lblSubHeading.font = [AppFont regularFont:16];
    self.btnLogin.titleLabel.font = [AppFont mediumFont:14];
    self.btnRegister.titleLabel.font = [AppFont mediumFont:14];
}
-(void)didShowCompleteView:(BOOL)show {
    [self setViewFonts];
   // [UIScreen mainScreen].bounds.size.height - (64.0 + 200.0)
    self.hidden = false;
    CGFloat y = [UIScreen mainScreen].bounds.size.height + 10;
    CGFloat height;
     NSString *existingLanguageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    
    if ([UIScreen mainScreen].bounds.size.height == 736)
    {
        height = 150;
        if (![existingLanguageSaved containsString:@"en"]) {
            height = 165;
        }
    }
    else
    {
        height = 170;
        if (![existingLanguageSaved containsString:@"en"]) {
            height = 175;
        }
    }
    self.isShown = @"no";
    if (show) {
        self.isShown = @"yes";
        y = [UIScreen mainScreen].bounds.size.height == 812 ? [UIScreen mainScreen].bounds.size.height - (height + 80) : [UIScreen mainScreen].bounds.size.height - (height + 46);
       }
    SharedManager.sharedSingleton.isLoginViewShow = self.isShown;
    if ([[SharedManager sharedSingleton].isLargeFont isEqualToString:@"yes"]) {
        if ([UIScreen mainScreen].bounds.size.height == 736)
        {
            height = 180;
        }
        else
        {
            height = 200;
        }
        if (show) {
            y = [UIScreen mainScreen].bounds.size.height == 812 ? [UIScreen mainScreen].bounds.size.height - (height + 78) : [UIScreen mainScreen].bounds.size.height - (height + 44);
        }
    }
    
     __block typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.50 animations:^{
        [UIView animateWithDuration:0.30 animations:^{
            CGRect frameShow = weakSelf.frame;
            frameShow.origin.y = y;
            frameShow.size.height = height;
            weakSelf.frame = frameShow;
            self.hidden = false;
        }];
     }completion:^(BOOL finished) {
        
    }];
}

- (IBAction)didTapLoginBtnAction:(UIButton *)sender {
    
    LoginAppVC *vc;
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    vc.hidesBottomBarWhenPushed = YES;
    if (_superVC != nil ) {
        [_superVC.navigationController pushViewController:vc animated:YES];
    }
}
- (IBAction)didTapRegisterBtnAction:(id)sender {
    
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
    
    MobileRegistrationVC *mobileReg;
    
    if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        mobileReg = [[MobileRegistrationVC alloc] initWithNibName:@"MobileRegistrationVC_iPad" bundle:nil];
    }
    else
    {
        mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegistrationVC"];
    }
    mobileReg.hidesBottomBarWhenPushed = YES;
    mobileReg.commingFrom=@"PreLogin";*/
    
    //[self.navigationController pushViewController:mobileReg animated:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Flagship" bundle:nil];
    MobileRegister_FlagVC *mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegister_FlagVC"];
    
    mobileReg.hidesBottomBarWhenPushed = YES;
    mobileReg.commingFrom=@"PreLogin";
    
    
    if (_superVC != nil ) {
        [_superVC.navigationController pushViewController:mobileReg animated:YES];
    }
}
- (IBAction)didTapArrowUpDownBtnAction:(UIButton *)sender {
    [self didShowCompleteView:false];
    if (self.didTapCrossButton != nil) {
        self.didTapCrossButton(sender);
    }
}

@end
