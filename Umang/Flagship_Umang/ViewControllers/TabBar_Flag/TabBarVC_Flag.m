//
//  TabBarVC_Flag.m
//  UMANG
//
//  Created by Rashpinder on 07/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "TabBarVC_Flag.h"
#import "HomeDataVC_Flag.h"
#import "AllServiceVC_Flag.h"
#import "MoreTabVC_Flag.h"
#import "LoginAppVC.h"
#import "LoginSingupView.h"
@interface TabBarVC_Flag ()<UITabBarControllerDelegate>
{
    MBProgressHUD *hud ;
    SharedManager*singleton;
    ///AllServiceVC_Flag *allServiceVC;

    LoginSingupView *loginSingupView ;

}
@property(nonatomic,retain)NSMutableArray *arrTabVC ;
@property(nonatomic,retain)NSString *tabOrders;
@property(nonatomic,retain)HomeDataVC_Flag *homeVC;
@property(nonatomic,retain)AllServiceVC_Flag *allServiceVC;

@end

@implementation TabBarVC_Flag

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self ;
     [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"languageScreenShown"];
    if (self.isLangChange == nil ) {
        self.isLangChange = @"";
    }
    singleton = [SharedManager sharedSingleton];
    singleton.loadServiceStatus=@"FALSE";
    

    [self getInitResponseWithTabBarOrder];
    //close it
   // [[NSNotificationCenter defaultCenter] addObserver:self
                                             //selector:@selector(reloadEverything)
                                             //    name:@"ReloadLanguageChanges_FLAG" object:nil];
    self.isLoginShown = @"NO";

    if (self.isLangChange != nil && [self.isLangChange isEqualToString:@"YES"]) {
        self.isLoginShown = @"YES";
        [self reloadEverything];
    }
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSUserDefaults standardUserDefaults] setInteger:kFlagShipScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.navigationController.navigationBarHidden = true;
    self.navigationController.navigationBar.hidden = true;
    __block typeof(self) weakSelf = self;
    if (self.homeVC != nil ) {
        self.homeVC.didTapLoginView = ^(UIButton *btnBack) {
            [weakSelf showLoginSingupView:weakSelf.homeVC];
            [weakSelf.allServiceVC checkLoginSingupView];
        };
        self.homeVC.didTapLoginViewForBanner = ^(UIButton *btnBack) {
            [weakSelf showLoginSingupViewForBanner:weakSelf.homeVC];
            [weakSelf.allServiceVC checkLoginSingupView];
        };
    }
    if (self.allServiceVC != nil ) {
        self.allServiceVC.didTapLoginView = ^(UIButton *btnBack) {
            [weakSelf showLoginSingupView:weakSelf.allServiceVC];
            [weakSelf.homeVC checkLoginSingupView];

        };
    }
    if (self.homeVC != nil ) {
        self.homeVC.didTapHelp = ^(UIButton *btnBack) {
            if (weakSelf->loginSingupView != nil ) {
                weakSelf->loginSingupView.hidden = true;
            }
        };
    }
    if (self.allServiceVC != nil ) {
        self.allServiceVC.didTapHelp  = ^(UIButton *btnBack) {
            if (weakSelf->loginSingupView != nil ) {
                weakSelf->loginSingupView.hidden = true;
            }
        };
    }
    if (self.selectedIndex != 2) {
        if ([SharedManager.sharedSingleton.isLoginViewShow isEqualToString:@"yes"]) {
            [self showLoginSingupView:self];
            //[self.homeVC checkLoginSingupView];
            //[self.allServiceVC checkLoginSingupView];
        }
    }else {
        if (loginSingupView != nil ) {
            loginSingupView.hidden = true;
        }
    }
    
    
}
-(void)reloadEverything {
    //[self getInitResponseWithTabBarOrder];
    __block typeof(self) weakSelf = self;
    
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [weakSelf  loginBtnAction];
        //[weakSelf performSelector:@selector(loginBtnAction) withObject:nil afterDelay:1.0];
        
    }];
    

}
#pragma mark- add Navigation View to View
-(void)checkLoginSingupView {
    if ([SharedManager.sharedSingleton.isLoginViewShow isEqualToString:@"yes"]) {
        loginSingupView.hidden = false;
    }
}
-(void)showLoginSingupViewForBanner:(UIViewController*)SenderVC{
     if (loginSingupView != nil) {
         return;
     }
    [self showLoginSingupView:SenderVC];
    
}
-(void)showLoginSingupView:(UIViewController*)SenderVC{
    if (loginSingupView != nil) {
        [loginSingupView didShowCompleteView:false];
        [self.homeVC checkLoginSingupView];
        [self.allServiceVC checkLoginSingupView];
        //[loginSingupView removeFromSuperview];
        loginSingupView = nil ;
        return;
    }
    NSLog(@"An iPhone X Load UI");
    loginSingupView = [[LoginSingupView alloc] initWithZeroFrame];
    __weak typeof(self) weakSelf = self;
    loginSingupView.superVC = self;
    loginSingupView.tag = 786;
    loginSingupView.didTapCrossButton = ^(UIButton* btnfilter)
    {
        [weakSelf.homeVC checkLoginSingupView];
        [weakSelf.allServiceVC checkLoginSingupView];
        [weakSelf makeNIlLoginSingupView];
       
    };
    [self.view addSubview:loginSingupView];
   // [self.view bringSubviewToFront:loginSingupView];
    loginSingupView.hidden = true;
    [loginSingupView didShowCompleteView:true];
    //[self changeCollectionHeight:true];
    [self.homeVC checkLoginSingupView];
    [self.allServiceVC checkLoginSingupView];
    
}
#pragma mark- add Navigation View to View


-(void)makeNIlLoginSingupView {
     loginSingupView = nil ;
}
#pragma mark - Button Action Methods
- (void)loginBtnAction
{
    singleton.isLoginViewShow = @"NO";
    LoginAppVC *vc;
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    vc.hidesBottomBarWhenPushed = YES;
    //[self pushViewController:vc animated:false];
    //UIViewController *vcSelected = self.viewControllers[self.selectedIndex];
    [self.homeVC.navigationController pushViewController:vc animated:false];
    self.isLangChange = @"";
    
}
-(void)getInitResponseWithTabBarOrder {
   
   // [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"TabKey"];
   // [[NSUserDefaults standardUserDefaults] synchronize];
    self.tabOrders= @"FLAGSHIP SERVICES|flagship|,ALL SERVICES|allservices";

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    singleton.infoTab = [userDefaults objectForKey:@"infotab"];
    self.tabOrders = singleton.infoTab;
    if ( [singleton.arr_initResponse  count]!=0)
    {
        self.tabOrders=[singleton.arr_initResponse  valueForKey:@"infotab"];
        if(self.tabOrders != nil && [self.tabOrders length] == 0)
        {
            self.tabOrders= @"FLAGSHIP SERVICES|flagship|,ALL SERVICES|allservices";
        }
        
    }
    singleton.infoTab=self.tabOrders;
    BOOL isLocal = false;
    if (self.tabOrders == nil || self.tabOrders.length == 0) {
        self.tabOrders= @"FLAGSHIP SERVICES|flagship|,ALL SERVICES|allservices";
        isLocal = true;
    }
    NSMutableArray *tabOrderArray=[[self.tabOrders componentsSeparatedByString:@","] mutableCopy];
    [tabOrderArray addObject:@"MORE|more"];
    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
    NSMutableArray *webtabViewControllers = [[NSMutableArray alloc] init];
    for (int i=0; i<[tabOrderArray count]; i++)
    {
        NSArray *arr_tab=[[tabOrderArray objectAtIndex:i] componentsSeparatedByString:@"|"];
        NSString *tabAction=[[arr_tab objectAtIndex:1] lowercaseString];
        
        if ([tabAction isEqualToString:@"flagship"])
        {
            [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
        }
        else if ([tabAction isEqualToString:@"allservices"])
        {
            [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
        }
        else if ([tabAction isEqualToString:@"more"])
        {
            [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
        }
        else
        {
            [webtabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
        }
     }
    NSMutableArray *tabitemsControllers = [[NSMutableArray alloc] init];
    
    
    for (int i=0; i<[tabViewControllers count]; i++)
    {
        NSArray *arr_tab=[[tabViewControllers objectAtIndex:i] componentsSeparatedByString:@"|"];
        NSString *tabName=[arr_tab objectAtIndex:0]; //get array name from pipe serate
       // NSString *tabAction=[arr_tab objectAtIndex:1];
        NSString *tabAction=[[arr_tab objectAtIndex:1] lowercaseString];
        
        NSLog(@"tabNames=%@",tabName);
        
        if ([tabAction isEqualToString:@"flagship"])
        {

            self.homeVC =(HomeDataVC_Flag*)[self getViewControler:@"HomeDataVC_Flag"];
            
            //homeVC.tabBarItem.image = [UIImage imageNamed:@"flagship_schemes_grey"];
            //homeVC.tabBarItem.selectedImage = [UIImage imageNamed:@"flagship_schemes"];

            self.homeVC.tabBarItem.image = [UIImage imageNamed:@"flagship_schemes_new"];
            self.homeVC.tabBarItem.selectedImage = [UIImage imageNamed:@"flagship_schemes_select"];
            
            if ([tabName length]==0 || isLocal)
            {
                self.homeVC.title = [NSLocalizedString(@"flagship", @"") capitalizedString];
            }
            else
            {
                self.homeVC.title =tabName.capitalizedString;
            }
            //homeVC.tabBarItem.title = [NSLocalizedString(@"flagship_scheme", nil) capitalizedString];
            UINavigationController *favtabNavVC = [[UINavigationController alloc] initWithRootViewController: self.homeVC];
            [tabitemsControllers addObject:favtabNavVC];
        }
        else if ([tabAction isEqualToString:@"allservices"])
        {
             self.allServiceVC =(AllServiceVC_Flag*)[self getViewControler:@"AllServiceVC_Flag"];
            //allVC.tabBarItem.image = [UIImage imageNamed:@"icon_all_services32"];
            
            self.allServiceVC.tabBarItem.image = [UIImage imageNamed:@"all_services"];
            
            if ([tabName length]==0 || isLocal)
            {
                self.allServiceVC.title = [NSLocalizedString(@"all_services_small", @"")  capitalizedString];
            }
            else
            {
                self.allServiceVC.title =tabName.capitalizedString;
            }
           // allVC.tabBarItem.title =  [NSLocalizedString(@"all_services_small", nil) capitalizedString];//[@"all_services_small" capitalizedString];
            UINavigationController *favtabNavVC = [[UINavigationController alloc] initWithRootViewController: self.allServiceVC];
            [tabitemsControllers addObject:favtabNavVC];
        }
        else if ([tabAction isEqualToString:@"more"])
        {
            
            MoreTabVC_Flag *allVC =(MoreTabVC_Flag*)[self getViewControler:@"MoreTabVC_Flag"];
            //allVC.tabBarItem.image = [UIImage imageNamed:@"icon_more2_32"];
            allVC.tabBarItem.image = [UIImage imageNamed:@"more"];
            
            allVC.tabBarItem.title = [NSLocalizedString(@"more_Home", nil) capitalizedString];//[@"more_Home" capitalizedString];
            UINavigationController *favtabNavVC = [[UINavigationController alloc] initWithRootViewController: allVC];
            [tabitemsControllers addObject:favtabNavVC];
        }
    }
    
    [self setViewControllers:tabitemsControllers];
    self.selectedIndex = 0;
}


-(UIViewController*)getViewControler:(NSString*)class  {
    
    UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:kFlagShip_Storyboard bundle:nil];
    return  [storyboard instantiateViewControllerWithIdentifier:class];
    
 //   return  [[UINavigationController alloc] initWithRootViewController:vc];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    NSLog(@"VC Sleected -- %@",viewController);
    if (self.selectedIndex == 2) {
        loginSingupView.hidden = true;
    }else {
        [self checkLoginSingupView];
        loginSingupView.superVC = viewController;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
