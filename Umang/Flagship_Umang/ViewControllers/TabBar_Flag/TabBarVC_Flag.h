//
//  TabBarVC_Flag.h
//  UMANG
//
//  Created by Rashpinder on 07/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarVC_Flag : UITabBarController

@property(strong,nonatomic)NSString *isLangChange;
@property(strong,nonatomic)NSString *isLoginShown;

@property(copy) void(^didShowLoginView)(BOOL show);
-(void)showLoginSingupView:(UIViewController*)SenderVC;

@end
