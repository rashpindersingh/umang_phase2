//
//  MoreTabVC_Flag.m
//  UMANG
//
//  Created by Rashpinder on 07/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "MoreTabVC_Flag.h"
#import "SharedManager.h"
#import "SettingsViewController.h"
#import "AboutInfoVC.h"
#import "HelpViewController.h"
#import "LoginAppVC.h"
#import "MobileRegistrationVC.h"
#import "ServiceDirectoryVC.h"
#import "UMANG-Swift.h"

@interface MoreTabVC_Flag ()
{
    IBOutlet UITableView *moreTableView;
    IBOutlet UIButton *loginBtn;
    IBOutlet UIButton *registerBtn;
    
    SharedManager *singleton;
    IBOutlet UILabel *loginAccessLabel;
    
    BOOL includeServiceDir;
    
    IBOutlet NSLayoutConstraint *logoTopConstraint;
    
    IBOutlet NSLayoutConstraint *viewHeightConstraint;
}

@end

@implementation MoreTabVC_Flag

#pragma mark - UIView LifeCycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    singleton = [SharedManager sharedSingleton];
    
    [loginBtn setTitle:NSLocalizedString(@"login", nil) forState:UIControlStateNormal];
    [registerBtn setTitle:NSLocalizedString(@"register", nil) forState:UIControlStateNormal];
    
    moreTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    moreArray = [NSArray new];
    
    moreTableView.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    
    loginAccessLabel.text = NSLocalizedString(@"login_access_profile", nil);
    
    if (iPhoneX())
    {
       // logoTopConstraint.constant = 45.0f ;
        viewHeightConstraint.constant = 190.0f;
    }
   
    loginBtn.layer.cornerRadius = 18.0;
    
    registerBtn.layer.cornerRadius = 18.0;
    registerBtn.layer.borderWidth = 1.0;
    registerBtn.layer.borderColor = loginBtn.backgroundColor.CGColor;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.navigationBarHidden = true ;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    //------------- Network View Handle------------
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    //------------- Tab bar Handle------------
    includeServiceDir = [[[NSUserDefaults standardUserDefaults] valueForKey:@"Enable_ServiceDir"] boolValue];
    
    //includeServiceDir = NO;
    
    if (includeServiceDir)
    {
        moreArray = @[@{@"name":NSLocalizedString(@"service_directory", nil),@"image":@"icon_service_directory"},@{@"name":NSLocalizedString(@"settings", nil),@"image" : @"more_settings"},@{@"name" : NSLocalizedString(@"tell_friend", nil),@"image":@"more_share"},@{@"name":NSLocalizedString(@"help_and_support", nil),@"image":@"more_help_support"},@{@"name":NSLocalizedString(@"about", nil),@"image":@"more_about"}];
    }
    else
    {
        moreArray = @[@{@"name":NSLocalizedString(@"settings", nil),@"image" : @"more_settings"},@{@"name" : NSLocalizedString(@"tell_friend", nil),@"image":@"more_share"},@{@"name":NSLocalizedString(@"help_and_support", nil),@"image":@"more_help_support"},@{@"name":NSLocalizedString(@"about", nil),@"image":@"more_about"}];
    }
    
    [self setViewFonts];
    [moreTableView reloadData];
}

-(void)setViewFonts {
    loginAccessLabel.font = [AppFont regularFont:17];
    loginBtn.titleLabel.font = [AppFont regularFont:16];
    registerBtn.titleLabel.font = [AppFont regularFont:16];
}
#pragma mark - Button Action Methods
- (IBAction)loginBtnAction:(UIButton *)sender
{
    LoginAppVC *vc;
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    vc.hidesBottomBarWhenPushed = YES;
    [self.tabBarController.navigationController pushViewController:vc animated:YES];
}

- (IBAction)registerBtnAction:(UIButton *)sender
{
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
    
    MobileRegistrationVC *mobileReg;
    
    if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        mobileReg = [[MobileRegistrationVC alloc] initWithNibName:@"MobileRegistrationVC_iPad" bundle:nil];
    }
    else
    {
        mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegistrationVC"];
    }
    mobileReg.commingFrom=@"PreLogin";

    mobileReg.hidesBottomBarWhenPushed = YES;*/
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Flagship" bundle:nil];
    MobileRegister_FlagVC *mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegister_FlagVC"];
    
    mobileReg.hidesBottomBarWhenPushed = YES;
    mobileReg.commingFrom=@"PreLogin";
    
    [self.tabBarController.navigationController pushViewController:mobileReg animated:YES];
}

- (IBAction)didTapHelpSupportBtnAction:(UIButton *)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    vc.comingFrom = @"accountRecovery";

    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - UITableView DataSource and Delegate Methods

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 60;
    }
    return 50;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return moreArray.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"moreTabCellIdentifier";
    
    MoreTabCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[MoreTabCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    cell.moreCellLabel.text = [[moreArray objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.moreCellImageView.image = [UIImage imageNamed:[[moreArray objectAtIndex:indexPath.row] valueForKey:@"image"]] ;
    
    cell.moreCellLabel.font = [AppFont regularFont:17.0];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    if (includeServiceDir)
    {
        switch (indexPath.row)
        {
            case 0:
            {
                [self openServiceDirectoryVC];
            }
                break;
                
            case 1:
            {
                [self openSettingsController];
            }
                break;
                
            case 2:
            {
                [self shareContent];
            }
                break;
                
            case 3:
            {
                [self openHelpController];
            }
                break;
            case 4:
            {
                [self openAboutScreen];
            }
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
            {
                [self openSettingsController];
            }
                break;
                
            case 1:
            {
                [self shareContent];
            }
                break;
                
            case 2:
            {
                [self openHelpController];
            }
                break;
            case 3:
            {
                [self openAboutScreen];
            }
                break;
                
            default:
                break;
        }
    }
    
    
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)openServiceDirectoryVC
{
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"DetailService" bundle:nil];
    ServiceDirectoryVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ServiceDirectoryVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)openAboutScreen
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    AboutInfoVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AboutInfoVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)shareContent
{
    NSString *textToShare =singleton.shareText;
    
    NSArray *objectsToShare = @[textToShare];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
}

-(void)openSettingsController
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)openHelpController
{
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    vc.comingFrom = @"accountRecovery";

    [self.navigationController pushViewController:vc animated:YES];

}

#pragma mark -
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end

#pragma mark - MoreTab Cell Interface and Implementation

@interface MoreTabCell()
@end

@implementation MoreTabCell

@end
