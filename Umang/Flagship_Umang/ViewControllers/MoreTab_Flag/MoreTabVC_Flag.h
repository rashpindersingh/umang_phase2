//
//  MoreTabVC_Flag.h
//  UMANG
//
//  Created by Rashpinder on 07/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface MoreTabVC_Flag : UIViewController
{
    NSArray *moreArray;
}
@end

#pragma mark - MoreTab Cell Interface
@interface MoreTabCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *moreCellImageView;
@property (strong, nonatomic) IBOutlet UILabel *moreCellLabel;


@end
