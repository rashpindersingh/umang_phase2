//
//  MPIN_AlertBox.h
//  UMANG
//
//  Created by Deepak Rawat on 24/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MPIN_AlertBox : UIView<UITextFieldDelegate>

{
    NSMutableArray *itemsTitle;
    NSMutableArray *itemsImages;

}
@property (strong,nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titlempin;
@property (weak, nonatomic) IBOutlet UILabel *lbl_entermpin;
@property (weak, nonatomic) IBOutlet UILabel *lbl_confirmMpin;
@property (weak, nonatomic) IBOutlet UIButton *btn_cross;

@property (weak, nonatomic) IBOutlet UIButton *btn_Next;
@property (weak, nonatomic) IBOutlet UIControl *vw_SetMPIN;

@property (weak, nonatomic) IBOutlet UILabel *lbl_subtitle1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subtitle2;
@property (weak, nonatomic) IBOutlet UIImageView *img_setmpin;


@property (weak, nonatomic) IBOutlet UIView *vw_RecoveryOptions;
@property (weak, nonatomic) IBOutlet UIButton *btn_openRecoverySettings;

@property (weak, nonatomic) IBOutlet UIImageView *img_check;

@property (weak, nonatomic) IBOutlet UILabel *lbl_MpinVerifiedMsg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_recoveryTitle;
@property (weak, nonatomic) IBOutlet UITableView *tbl_recovery;
@property (weak, nonatomic) IBOutlet UIButton *btn_RecoveryClose;
@property (strong, nonatomic) IBOutlet UILabel *lbl_secureAccount;

-(id)initCustom;
-(void)showHideClose:(NSString*)status;
-(id)initOnlyRecoveryOption;
@property(strong,nonatomic)NSString *isServiceDepartment;
@property(copy)void(^didMPINSet)(BOOL isSet,NSString *message);

@property(copy) void(^didTapNextButton)(id btnNext);
@property(copy) void(^didTapCrossButton)(id btnCross);
@property(copy) void(^didTapRecoveryCloseButton)(id btnClose);
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *leadingConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightSecureConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSecureConstraint;

-(IBAction)didTapBackGround:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt_enterMPIN;
@property (weak, nonatomic) IBOutlet UITextField *txt_confirmMPIN;
@end

NS_ASSUME_NONNULL_END
