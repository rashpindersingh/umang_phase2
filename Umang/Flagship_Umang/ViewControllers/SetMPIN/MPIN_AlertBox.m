//
//  MPIN_AlertBox.m
//  UMANG
//
//  Created by Deepak Rawat on 24/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "MPIN_AlertBox.h"
#import "UMANG-Swift.h"
#define kOFFSET_FOR_KEYBOARD 80.0

@interface MPIN_AlertBox()<UITableViewDelegate, UITableViewDataSource>
{
    
}
@end
//img_check

@implementation MPIN_AlertBox



-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
   
    return self;
    
    
}
-(id)initOnlyRecoveryOption{
    self = [self initCustom];
    self.vw_SetMPIN.hidden=TRUE;
    self.vw_RecoveryOptions.hidden=FALSE;
    [self endEditing:YES];
    [self loadTableView];
    self.lbl_secureAccount.text = @"";
    self.topSecureConstraint.constant = 10.0;
    self.heightSecureConstraint.constant = 0.0;
    self.lbl_secureAccount.hidden = true;
   // self.lbl_secureAccount.hidden=TRUE;
    self.img_check.image = [UIImage imageNamed:@"lock"];
    self.lbl_MpinVerifiedMsg.text = NSLocalizedString(@"secure_account", nil);;
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate new] forKey:RECOVERY_BOX_DATE];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:RECOVERY_BOX_COUNT];
    [self.btn_RecoveryClose setTitle:NSLocalizedString(@"ask_me_later", nil) forState:UIControlStateNormal];
    return self;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"MPIN_AlertBoxXIB" owner:self options:nil];
    self = (MPIN_AlertBox *)[objects objectAtIndex:0];
    if (self.tag ==303) {
        [self removeFromSuperview];
    }
    self.tag = 303;
   
    self.frame = frame;
    
    
    return self;
}


-(id)initCustom
{
    self = [self initWithFrame:CGRectMake(0, 0,fDeviceWidth , fDeviceHeight)];
    self.txt_enterMPIN.delegate=self;
    self.txt_confirmMPIN.delegate=self;
    
    self.txt_enterMPIN.tag=101;
    self.txt_confirmMPIN.tag=102;
    
    self.vw_SetMPIN.hidden=false;
    self.vw_RecoveryOptions.hidden=TRUE;
    /*
      ;
    btn_Next
    btn_openRecoverySettings
         */
    self.lbl_titlempin.text =NSLocalizedString(@"set_umang_mpin", nil);
    self.lbl_subtitle1.text =NSLocalizedString(@"mpin_mandatory", nil);
    self.lbl_subtitle2.text =NSLocalizedString(@"mpin_protects", nil);

    self.lbl_entermpin.text = NSLocalizedString(@"enter_mpin_verify", nil);
    self.lbl_confirmMpin.text=NSLocalizedString(@"confirm_mpin", nil);

    self.lbl_MpinVerifiedMsg.text=NSLocalizedString(@"mpin_is_set", nil);
    self.lbl_recoveryTitle.text=NSLocalizedString(@"secure_recovery", nil);
    [self.btn_Next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_RecoveryClose setTitle:NSLocalizedString(@"close", nil) forState:UIControlStateNormal];
    
    self.lbl_secureAccount.text=NSLocalizedString(@"secure_recovery_options", nil);

    [self.btn_openRecoverySettings setTitle:NSLocalizedString(@"go_account_settings", nil) forState:UIControlStateNormal];
   SharedManager *singleton = [ SharedManager sharedSingleton];
   [self showHideClose:singleton.shared_mpinmand];
    
    itemsTitle = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"alt_mobile_num", nil), NSLocalizedString(@"email", nil),NSLocalizedString(@"security_ques_text", nil), nil];
    
    itemsImages=[[NSMutableArray alloc]initWithObjects:@"mobile_mpin",@"email_mpin",@"security_grey_mpin", nil];
    
    [self.vw_RecoveryOptions layoutCornerRadiusWithRadius:10];
    [self.vw_SetMPIN layoutCornerRadiusWithRadius:10];
    [self setViewFont];
    if (fDeviceWidth <= 320) {
        for (NSLayoutConstraint* constraint in _leadingConstant) {
            constraint.constant = 10.0;
        }
    }
    _isServiceDepartment = @"no";
     [self.btn_RecoveryClose setTitle:NSLocalizedString(@"ask_me_later", nil) forState:UIControlStateNormal];
    return  self;
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [_btn_Next.titleLabel setFont:[AppFont mediumFont:20.0]];
    [_btn_RecoveryClose.titleLabel setFont:[AppFont mediumFont:20]];

    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _lbl_entermpin.font = [AppFont regularFont:16.0];
    _lbl_confirmMpin.font = [AppFont regularFont:16.0];

    _lbl_subtitle1.font = [AppFont regularFont:17.0];
     _lbl_subtitle1.textColor = [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1];
    
    _lbl_subtitle2.textColor = [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1];
    _lbl_subtitle2.font = [AppFont mediumFont:14.0];
    
    _lbl_titlempin.font = [AppFont mediumFont:20.0];
    
    _lbl_recoveryTitle.font = [AppFont regularFont:16.0];
    _lbl_recoveryTitle.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    
    _lbl_MpinVerifiedMsg.font = [AppFont regularFont:20.0];
    
    
    _txt_enterMPIN.font = [AppFont mediumFont:25.0];
    _txt_confirmMPIN.font = [AppFont regularFont:25.0];
    
    // Open Account Settinhs
       // Open -- 36 regular -- 102 color
       // Account Seriings -- Deafult Blue
    
    /*NSMutableAttributedString *open =  [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"open_text", nil) attributes:@{NSFontAttributeName : [AppFont regularFont:16.0],NSForegroundColorAttributeName: [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1]}];
    
    NSMutableAttributedString *blank =  [[NSMutableAttributedString alloc] initWithString:@" " attributes:@{NSFontAttributeName : [AppFont regularFont:16.0],NSForegroundColorAttributeName: [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1]}];

    
    NSAttributedString *accountSettings =  [[NSAttributedString alloc] initWithString:NSLocalizedString(@"account", nil) attributes:@{NSFontAttributeName :  [AppFont regularFont:18.0],NSForegroundColorAttributeName: DEFAULT_BLUE}];
    [open appendAttributedString:blank];
    [open appendAttributedString:accountSettings];

    
    [_btn_openRecoverySettings setAttributedTitle:open forState:UIControlStateNormal];*/

    [_btn_openRecoverySettings setTitle:@"GO TO ACCOUNT SETTINGS" forState:UIControlStateNormal];
    
   /* _btn_openRecoverySettings.titleLabel.font = [AppFont regularFont:12.0];
    _btn_openRecoverySettings.layer.cornerRadius = 25.0;
    _btn_openRecoverySettings.layer.borderWidth = 0.5;
    _btn_openRecoverySettings.layer.borderColor = [UIColor colorWithRed:226.0/255.0f green:226.0/255.0f blue:226.0/255.0f alpha:1.0].CGColor;
    _btn_openRecoverySettings.clipsToBounds = YES;
    
    */
    _lbl_secureAccount.text = @"Secure Your Account";
    _lbl_secureAccount.font = [AppFont mediumFont:16];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (IBAction)didTapNextButton:(UIButton *)sender {
    
    [self endEditing:YES];
    [self checkValidation];
    if (self.didTapNextButton != nil) {
        self.didTapNextButton(sender);
    }
}

//===============================================================
//  #pragma method for check enter value is valid Numeric only
//===============================================================

-(BOOL) isValidPIN:(NSString*) str
{
    NSString *strMatchstring=@"\\b([0-9%_.+\\-]+)\\b";
    NSPredicate *textpredicate=[NSPredicate predicateWithFormat:@"SELF MATCHES %@", strMatchstring];
    
    if(![textpredicate evaluateWithObject:str])
    {
        return FALSE;
    }
    return TRUE;
}

//===============================================================
//  #pragma method for Validation
//===============================================================

-(void)checkValidation
{
   
    
    NSLog(@"value of txt Enter MPIN = %@",self.txt_enterMPIN.text);
    NSLog(@"value of txt Enter Confirm MPIN = %@",self.txt_confirmMPIN.text);

   
    if (self.txt_enterMPIN.text.length < 4)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_mpin_to_verify", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
    }
    
    else  if (self.txt_confirmMPIN.text.length < 4)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"confirm_mpin", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
    }
    else if ([self isValidPIN:self.txt_enterMPIN.text]!=TRUE)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_mpin_to_verify", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
    }
    
    else if ([self isValidPIN:self.txt_confirmMPIN.text]!=TRUE)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"confirm_mpin", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
    }
    else if (![self.txt_enterMPIN.text isEqualToString:self.txt_confirmMPIN.text])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"mpin_donot_match_txt", nil)  delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // After Hit API Success ful response 

        [self hitSetMpinAPI];
        // test case below need to close and above line need to open
        /*self.vw_SetMPIN.hidden=TRUE;
        self.vw_RecoveryOptions.hidden=FALSE;
        [self endEditing:YES];
        [self loadTableView];
        */
      
    }
    
   
    
    
    
    
}




- (IBAction)didTapCrossButton:(UIButton *)sender {
    if (self.didTapCrossButton != nil) {
        self.didTapCrossButton(sender);
       // [self clearMPIN:self];
    }
    if (_didMPINSet != nil && [_isServiceDepartment isEqualToString:@"true"])
    {
       // _didMPINSet(false,@"error");
        self.didMPINSet(true, @"error : user_cancel");

    }
}

-(void)clearMPIN:(UIView *)view
{
   /* for (UIView *subView in view.subviews) {    // UIView.subviews
        if (subView.tag == 303) {
            [subView removeFromSuperview];
        }
    }*/
    
    [view removeFromSuperview];

}

-(IBAction)didTapBackGround:(id)sender
{
  //  [self.txt_enterMPIN resignFirstResponder];
   // [self.txt_confirmMPIN resignFirstResponder];
    [self endEditing:YES];

}






-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:.3];
//    [UIView setAnimationBeginsFromCurrentState:TRUE];
//    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.frame.size.width, self.frame.size.height);
//
//    [UIView commitAnimations];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:.3];
//    [UIView setAnimationBeginsFromCurrentState:TRUE];
//    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.frame.size.width, self.frame.size.height);
//
//    [UIView commitAnimations];
    
    //[self checkValidation];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textField.text];
//    [attributedString addAttribute:NSKernAttributeName
//                             value:@(10.4)
//                             range:NSMakeRange(0, textField.text.length)];
//    textField.attributedText = attributedString;
    NSString * currentTxtStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    
    
    NSLog(@"currentTxtStr txt =%@ ",currentTxtStr);
    currentTxtStr = [currentTxtStr stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
    //mobile
    //if (_txt_enterId.text.length >= 10 && range.length == 0)
   /* if (currentTxtStr.length >= 10 && range.length == 0)
        
    {
        
        _txt_enterId.text=[currentTxtStr substringToIndex:10];;
        
        */
   
    if (textField==self.txt_enterMPIN)
    {
        
        if (currentTxtStr.length >= 4 && range.length == 0)
        {
            self.txt_enterMPIN.text=[currentTxtStr substringToIndex:4];;
            [textField resignFirstResponder];
            [self.txt_confirmMPIN becomeFirstResponder];
            return NO; // return NO to not change text
        }
        
    }
    if (textField==self.txt_confirmMPIN)
    {
        
        if (self.txt_confirmMPIN.text.length >= 4 && range.length == 0)
        {
            [textField resignFirstResponder];

            return NO; // return NO to not change text
        }
        
    }
    
    return YES;
    
    
}




-(void)loadTableView
{
   
    
    self.tbl_recovery.delegate=self;
    self.tbl_recovery.dataSource=self;
    self.tbl_recovery.scrollEnabled = false;
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemsTitle count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  35;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    [[cell viewWithTag:1234] removeFromSuperview];
    [[cell viewWithTag:12345] removeFromSuperview];

    UIImageView *imageVW = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 35)];
    imageVW.contentMode = UIViewContentModeScaleAspectFit;
    imageVW.image = [UIImage imageNamed:[itemsImages objectAtIndex:indexPath.row]];
    imageVW.tag = 1234;
    [cell addSubview:imageVW];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(22, 0, fDeviceWidth - 100, 35)];
    lblTitle.textAlignment = NSTextAlignmentLeft;
    lblTitle.text = [itemsTitle objectAtIndex:indexPath.row];
    
    lblTitle.font = [AppFont mediumFont:16];
    lblTitle.textColor =  [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    imageVW.tag = 12345;

    [cell addSubview:lblTitle];
    
   // cell.accessoryType = UITableViewCellAccessoryNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    [self endEditing:YES];
    
    if (self.didTapRecoveryCloseButton != nil) {
        self.didTapRecoveryCloseButton([UIButton new]);
    }
    
    
    [self removeFromSuperview];
    //[self openRecoveryView];
    NSString *type = @"non";
    
    /*
     
     
     itemsTitle = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"alt_mobile_num", nil), NSLocalizedString(@"email", nil),NSLocalizedString(@"security_ques_text", nil), nil];
     */
    if (indexPath.row == 2)
    {
        type = @"security";
    }
   else if (indexPath.row == 0)
    {
      type = @"mobile";
    }
    else if (indexPath.row == 1)
    {
        type = @"email";
    }
   
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate pushSetSecurityVC :type];
}


-(void)openRecoveryView : (NSString *)type
{
    // ========= check to open show mpin alert box=====
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate pushSetSecurityVC :type];


}
 
- (IBAction)didTapRecoveryCloseButton:(UIButton *)sender
{
  
   // [self removeFromSuperview];
    [self endEditing:YES];
    if (self.didTapRecoveryCloseButton != nil) {
        self.didTapRecoveryCloseButton(sender);
    }
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate new] forKey:RECOVERY_BOX_DATE];
}

-(void)showHideClose:(NSString*)status
{
       if ([[status lowercaseString] isEqualToString:@"true"])
    {
        self.btn_cross.hidden=TRUE;
    }
    else
    {
        self.btn_cross.hidden=FALSE;

    }
    //self.btn_cross.hidden=FALSE;

}






//----- hitAPI for IVR OTP call Type registration ------

-(void)hitSetMpinAPI
{
    

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",self.txt_enterMPIN.text,SaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    double CurrentTime = CACurrentMediaTime();
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    SharedManager *singleton = [SharedManager sharedSingleton];
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:@"" forKey:@"lang"];
    [dictBody setObject:timeInMS forKey:@"trkr"];
    
   MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    //Type for which OTP to be intiate eg register,login,forgot mpin
    NSLog(@"Dict body is :%@",dictBody);
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_SET_MPINU2 withBody:dictBody andTag:TAG_REQUEST_SET_MPINU2 completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        NSString *rd=[response valueForKey:@"rd"];
        NSString *rc=[response valueForKey:@"rc"];

        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rs=[response valueForKey:@"rs"];
           // NSString *rd=[response valueForKey:@"rd"];

            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
           // NSString *rd=[response valueForKey:@"rd"];
            //If rc= MAS
            
           // json.put("status","true");
            //json.put("message","MPIN_ALREADY_SET");
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
              //  singleton.user_tkn=tkn;
                //-------- Add later For handling mpinflag / mpinmand ----------
                singleton.shared_mpinflag =[[response valueForKey:@"pd"]  valueForKey:@"mpinflag"];
                singleton.shared_mpinmand =[[response valueForKey:@"pd"]  valueForKey:@"mpinmand"];
                
                NSLog(@"mpinflag =%@",singleton.shared_mpinflag);
                NSLog(@"mpinmand =%@",singleton.shared_mpinmand);
                if (singleton.shared_mpinflag == (NSString *)[NSNull null]||[singleton.shared_mpinflag length]==0) {
                    singleton.shared_mpinflag=@"TRUE";
                }
                
                if (singleton.shared_mpinmand == (NSString *)[NSNull null]||[singleton.shared_mpinmand length]==0) {
                    singleton.shared_mpinmand=@"TRUE";
                }
                
                // New mpindial parameter parsing added
              /*  NSString *mpindial =[[response valueForKey:@"pd"]  valueForKey:@"mpindial"];
                if (mpindial == (NSString *)[NSNull null]||[mpindial length]==0) {
                    mpindial=@"FALSE";
                }
                NSLog(@"mpindial =%@",mpindial);
                [[NSUserDefaults standardUserDefaults] setValue:mpindial forKey:@"mpindial"];
             
               // recflag save start
               NSString *recflag  = [[response valueForKey:@"pd"]  valueForKey:@"recflag"];
               [[NSUserDefaults standardUserDefaults] setValue:recflag forKey:@"recflag"];

              */
                // check Remove ShowLogin OTP
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"showLoginOTP_Hint"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinflag forKey:@"mpinflag"];
                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinmand forKey:@"mpinmand"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                if (_didMPINSet != nil && [_isServiceDepartment isEqualToString:@"true"])
                {
                   
                        self.didMPINSet(true, @"MPIN_SET");
                }
                else {
                    self.vw_SetMPIN.hidden=TRUE;
                    self.vw_RecoveryOptions.hidden=FALSE;
                    [self endEditing:YES];
                    [self loadTableView];
                }
               
                
               
                
                
                
            }
            
        }
        else{
            if (_didMPINSet != nil && [_isServiceDepartment isEqualToString:@"true"])
            {
                //_didMPINSet(false);
                if ([[rc uppercaseString]isEqualToString:@"MAS"])
                {
                    if (_didMPINSet != nil && [_isServiceDepartment isEqualToString:@"true"])
                    {
                        self.didMPINSet(true, @"MPIN_ALREADY_SET");
                    }
                }
                else {
                    
                NSString *error_rd = [NSString stringWithFormat:@"error: %@",rd];
                self.didMPINSet(true,error_rd);
                }
            }
            else
            {
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            }
        }
        
    }];
    
}



- (IBAction)btn_OpenRecoveryAction:(id)sender {
    [self endEditing:YES];
    [self removeFromSuperview];
    [self openRecoveryView:@"non"];
}

@end
