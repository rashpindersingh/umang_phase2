//
//  HomeWithFav_TabVC.h
//  UMANG
//
//  Created by Rashpinder on 14/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeWithFav_TabVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITabBarDelegate,UITextFieldDelegate>
{
    
    AdvertisingColumn *_headerView;
    
    IBOutlet UITableView *tableView_home;
    SharedManager *singleton;
    IBOutlet UITextField *txt_searchField;
    
    BOOL flag_complete;
    
    UIRefreshControl *refreshController;
    
    IBOutlet UIView *vw_line;
    
    
    IBOutlet UIView *vw_RetryOption;
    IBOutlet UIButton *btn_retryclick;
    IBOutlet UILabel *lbl_errorloading;
    
    IBOutlet UIImageView *searchIconImage;
    
    __weak IBOutlet UILabel *toAddFavouriteLabel;
    
    IBOutlet UILabel *noFavouriteFoundLabel;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingSearchConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topNoServiceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomTableConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthSearchConstraint;

@property (strong, nonatomic) IBOutlet UIView *vw_noServiceFound;

@property(nonatomic,strong)NSString *comingFrom;

@property (strong, nonatomic) IBOutlet UIView *viewSegmentHeader;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

-(IBAction)retryAction:(id)sender;
-(IBAction)btn_filterAction:(id)sender;//for filterview show
-(IBAction)btn_noticationAction:(id)sender;//for notification view show
-(void)fetchDatafromDB;
-(void)hitHomeAPI;

@end
