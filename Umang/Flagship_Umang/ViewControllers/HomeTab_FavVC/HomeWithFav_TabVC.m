
//
//  HomeWithFav_TabVC.m
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "HomeTabVC.h"
#import "ServiceCollectionViewCell.h"
#import "HomeContainerCell.h"
#import "HomeContainerCellView.h"
//#import "NotificationViewController.h"
#import "AddFilterHomeVC.h"
#import "ScrollNotificationVC.h"
#import "AdvanceSearchVC.h"
#import "HCSStarRatingView.h"
#import "FavouriteCell.h"
#import <Crashlytics/Crashlytics.h>


#define fDeviceWidth ([UIScreen mainScreen].bounds.size.width)
#define fDeviceHeight ([UIScreen mainScreen].bounds.size.height)
#define DEFAULT_BLUE_NEW [UIColor colorWithRed:0.0/255.0 green:89.0/255.0 blue:157.0/255.0 alpha:1.0]

#import "THProgressView.h"

#import "HomeDetailVC.h"

#import "HintViewController.h"

#import  "ShowMoreServiceVC.h"
#import "UserProfileVC.h"
#import "ShowUserProfileVC.h"
#import "UMAPIManager.h"

#import "UIImageView+WebCache.h"

//static const CGSize progressViewSize = { 95.0f, 25.0f };

#import "MBProgressHUD.h"


#import "NetworkView.h"

#import "AdvanceSearchVC.h"

#import "IntroView.h"
#import "StateList.h"

#import "WSCoachMarksView.h"
#import "Constant.h"
//#import "GAITracker.h"

#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "UIView+MGBadgeView.h"

#import "CustomBadge.h"
#import "BadgeStyle.h"
#import "RunOnMainThread.h"
#import "UMANG-Swift.h"
#import "HomeDialogBox.h"
#import "HomeWithFav_TabVC.h"
static const CGSize progressViewSize = { 95.0f, 25.0f };
static float NV_height= 50;




@interface HomeWithFav_TabVC ()<UITabBarDelegate,UITabBarDelegate,HomeCallback,MaterialShowcaseDelegate,HomeBannerProtocol>
{
    UIImageView *imgProfile;
    UILabel *lbl_profileComplete;
    UILabel *lbl_whiteLine;
    UILabel *lbl_percentage;
    UIButton *btn_close;
    UIButton *btn_clickUpdate;
    UIView *NotifycontainerView;
    
    HintViewController *vwContHint;
    MBProgressHUD *hud ;
    
    __weak IBOutlet UIView *vwTextBg;
    
    IBOutlet UIButton *btn_filter;
    IBOutlet UIButton *btn_notification;
    IBOutlet UIButton *btn_more;
    
    IntroView *introView;
    StateList *obj;
    
    
    
    
    NSMutableArray *listHeroSpace;
    WSCoachMarksView *coachMarksView;
    BOOL flagrotation;
    
    
    BOOL flagStatusBar;
    
    UIActivityIndicatorView *activityIndicator ;
    NSMutableDictionary * homeDialogueDict;
    
    
    HomeDialogBox *homeDialog;
    BOOL hintShown;

    
    NavigationSearchView *nvSearchView ;
    
}
@property (nonatomic, strong) HCSStarRatingView *starRatingView;

@property (nonatomic, strong) NSString *profileComplete;
@property(nonatomic,retain)NSMutableArray *tableData ;
@property(nonatomic,retain)NSDictionary *cellDataOfmore;



//@property (retain, nonatomic) UITableView *tableView;



@property (strong, nonatomic) NSMutableArray *sampleData;
@property (nonatomic) CGFloat progress;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSArray *progressViews;


@property (strong, nonatomic) IBOutlet HomeDataBannersView *headerBanner;

@end

@implementation HomeWithFav_TabVC
@synthesize profileComplete;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = NSLocalizedString(@"Master", @"Master");
    }
    return self;
}



- (void)reloadTable:(NSNotification *)notif {
    [self reloadTableOnMainQueue];
    //[tableView_home reloadData];
}



//-----------
-(void)hitTrendingAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    
    
    [dictBody setObject:@"" forKey:@"sno"];
    [dictBody setObject:@"" forKey:@"category"];
    [dictBody setObject:@"" forKey:@"alises"];
    [dictBody setObject:@"" forKey:@"inlang"];
    [dictBody setObject:@"fetch" forKey:@"type"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"inlang"];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    [dictBody setObject:@"" forKey:@"peml"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FTAL withBody:dictBody andTag:TAG_REQUEST_FTAL completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        if (error == nil) {
            //NSLog(@"Server Response = %@",response);
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                
                singleton.arr_trend_Searchservice=[[response objectForKey:@"pd"]objectForKey:@"alisesList"];
                
                //NSLog(@"singleton.arr_trend_Searchservice = %@", singleton.arr_trend_Searchservice);
                
                
            }
            
        }
        else{
            
            
        }
        
    }];
    
}

- (void)statusBarchange
{
    [self addNotify];
    
}

//test

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
  
    tableView_home.delegate=self;
    tableView_home.dataSource=self;
    tableView_home.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView_home.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if (iPhoneX())
    {
        
       // nvSearchView = [[NavigationSearchView alloc] init];
        NSLog(@"An iPhone X Load UI");
    }
    ///headerView = [[HomeBannersView alloc] initWithFrame:CGRectMake(0, 0, 0, [self getHeaderHeight])];
    self.headerBanner.didSelect = self;
    self.viewSegmentHeader.backgroundColor = [UIColor clearColor];
    obj = [[StateList alloc] init];
    NSArray *arry_state = [obj getStateList];
    
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    homeDialogueDict = [NSMutableDictionary new];
    
    flagStatusBar=FALSE;
    //--later add
    //---later add
    
    /*  [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(statusBarchange)
     name:@"STATUSBARCHANGE" object:nil];
     
     */
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self adjustSearchBarView];
    }
    
    
    
    listHeroSpace=[[NSMutableArray alloc]init];
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    [self.view addSubview: activityIndicator];
    
    
    
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:HOME];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    vw_RetryOption.hidden=TRUE;
    
    
    //---- Remove state tabbar---------
    /*
     NSMutableArray *tabbarViewControllers = [NSMutableArray arrayWithArray: [self.tabBarController viewControllers]];
     [tabbarViewControllers removeObjectAtIndex: 1];
     [self.tabBarController setViewControllers: tabbarViewControllers ];*/
    //---- Remove state tabbar---------
    
    
    
    vwTextBg.layer.cornerRadius = 5.0;
    [txt_searchField setValue:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    txt_searchField.placeholder = NSLocalizedString(@"search", @"");
    
    txt_searchField.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    //txt_searchField.backgroundColor=[UIColor greenColor];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"RELOADCOLLECTIONVIEW"
                                               object:nil];
    
    
    singleton = [SharedManager sharedSingleton];
    
    txt_searchField.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"LOGIN_KEY"];
    
    
    
    //NSLog(@"singleton.user_tkn=%@",singleton.user_tkn);
    ////NSLog(@"singleton.user_tkn=%@",[defaults stringForKey:@"USER_ID"]);
    
    
    if ([singleton.user_tkn length]<=0)
    {
        // NSString *value = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"country"];
        singleton.user_id=[defaults decryptedValueForKey:@"USER_ID"];
        
        
        
        singleton.user_tkn=[defaults decryptedValueForKey:@"TOKEN_KEY"];
        singleton.mobileNumber=[defaults decryptedValueForKey:@"MOBILE_KEY"];
        singleton.profileNameSelected=[defaults decryptedValueForKey:@"NAME_KEY"];
        singleton.notiTypeGenderSelected=[defaults decryptedValueForKey:@"GENDER_KEY"];
        singleton.profilestateSelected=[defaults decryptedValueForKey:@"STATE_KEY"];
        singleton.notiTypDistricteSelected=[defaults decryptedValueForKey:@"DISTRICT_KEY"];
        singleton.profileDOBSelected=[defaults decryptedValueForKey:@"DOB_KEY"];
        singleton.objUserProfile.objAadhar.aadhar_number=[defaults decryptedValueForKey:@"ADHAR_ADHARNUM_KEY"];
        singleton.objUserProfile.objAadhar.aadhar_image_url=[defaults decryptedValueForKey:@"ADHAR_IMAGE_URL_KEY"];
        singleton.objUserProfile.objAadhar.pincode=[defaults decryptedValueForKey:@"ADHAR_PINCODE_KEY"];
        singleton.objUserProfile.objAadhar.dob=[defaults decryptedValueForKey:@"ADHAR_DOB_KEY"];
        singleton.objUserProfile.objAadhar.email=[defaults decryptedValueForKey:@"ADHAR_EMAIL_KEY"];
        singleton.objUserProfile.objAadhar.gender=[defaults decryptedValueForKey:@"ADHAR_GENDER_KEY"];
        singleton.objUserProfile.objAadhar.mobile_number=[defaults decryptedValueForKey:@"ADHAR_MOB_KEY"];
        singleton.objUserProfile.objAadhar.name=[defaults decryptedValueForKey:@"ADHAR_NAME_KEY"];
        singleton.objUserProfile.objAadhar.district=[defaults decryptedValueForKey:@"ADHAR_DISTRICT_KEY"];
        singleton.objUserProfile.objAadhar.father_name=[defaults decryptedValueForKey:@"ADHAR_FATHERNAME_KEY"];
        singleton.objUserProfile.objAadhar.state=[defaults decryptedValueForKey:@"ADHAR_STATE_KEY"];
        singleton.objUserProfile.objAadhar.street=[defaults decryptedValueForKey:@"ADHAR_STREET_KEY"];
        singleton.objUserProfile.objAadhar.district=[defaults decryptedValueForKey:@"ADHAR_SUBDISTRICT_KEY"];
        singleton.objUserProfile.objAadhar.vtc=[defaults decryptedValueForKey:@"ADHAR_VTC_KEY"];
        singleton.objUserProfile.objAadhar.vtc_code=[defaults decryptedValueForKey:@"ADHAR_VTCCODE_KEY"];
    }
    //========================================
    //========================================
    else
    {
        
        NSString *aadhar_number=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.aadhar_number];
        
        if([aadhar_number length]==0 || aadhar_number ==nil|| [aadhar_number isEqualToString:@"(null)"])
        {
            aadhar_number=@"";
        }
        
        
        NSString *aadhar_image_url=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.aadhar_image_url];
        
        if([aadhar_image_url length]==0|| aadhar_image_url ==nil|| [aadhar_image_url isEqualToString:@"(null)"])
        {
            aadhar_image_url=@"";
        }
        
        NSString *pincode=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.pincode];
        
        if([pincode length]==0 || pincode ==nil|| [pincode isEqualToString:@"(null)"])
        {
            pincode=@"";
        }
        
        
        NSString *dob=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.dob];
        
        if([dob length]==0 || dob ==nil|| [dob isEqualToString:@"(null)"])
        {
            dob=@"";
        }
        
        
        
        NSString *email=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.email];
        if([email length]==0 || email ==nil|| [email isEqualToString:@"(null)"])
        {
            email=@"";
        }
        
        
        NSString *gender=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.gender];
        
        if([gender length]==0 || gender ==nil|| [gender isEqualToString:@"(null)"])
        {
            gender=@"";
        }
        
        NSString *mobile_number=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.mobile_number];
        
        if([mobile_number length]==0 || mobile_number ==nil|| [mobile_number isEqualToString:@"(null)"])
        {
            mobile_number=@"";
        }
        
        
        NSString *name=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.name];
        
        if([name length]==0 || name ==nil|| [name isEqualToString:@"(null)"])
        {
            name=@"";
        }
        
        
        NSString *district=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.district];
        
        if([district length]==0 || district ==nil|| [district isEqualToString:@"(null)"])
        {
            district=@"";
        }
        
        
        
        NSString *father_name=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.father_name];
        
        if([father_name length]==0 || father_name ==nil|| [father_name isEqualToString:@"(null)"])
        {
            father_name=@"";
        }
        
        
        
        
        NSString *state=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.state];
        
        if([state length]==0 || state ==nil|| [state isEqualToString:@"(null)"])
        {
            state=@"";
        }
        
        NSString *street=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.street];
        
        if([street length]==0 || street ==nil|| [street isEqualToString:@"(null)"])
        {
            street=@"";
        }
        
        
        NSString *subdistrict=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.subdistrict];
        
        
        if([subdistrict length]==0 || subdistrict ==nil|| [subdistrict isEqualToString:@"(null)"])
        {
            subdistrict=@"";
        }
        
        
        
        NSString *vtc=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.vtc];
        
        if([vtc length]==0 || vtc ==nil|| [vtc isEqualToString:@"(null)"])
        {
            vtc=@"";
        }
        
        
        
        NSString *vtc_code=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.vtc_code];
        
        if([vtc_code length]==0 || vtc_code ==nil|| [vtc_code isEqualToString:@"(null)"])
        {
            vtc_code=@"";
        }
        
        NSString *aadhaar_address=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.address];
        
        if([aadhaar_address length]==0 || aadhaar_address ==nil|| [aadhaar_address isEqualToString:@"(null)"])
        {
            aadhaar_address=@"";
        }
        
        
        
        NSString *notiTypeCitySelected=[NSString stringWithFormat:@"%@",singleton.notiTypeCitySelected];
        
        if([notiTypeCitySelected length]==0 || notiTypeCitySelected ==nil|| [notiTypeCitySelected isEqualToString:@"(null)"])
        {
            notiTypeCitySelected=@"";
        }
        
        
        NSString *altermobileNumber=[NSString stringWithFormat:@"%@",singleton.altermobileNumber];
        if([altermobileNumber length]==0 || altermobileNumber ==nil|| [altermobileNumber isEqualToString:@"(null)"])
        {
            altermobileNumber=@"";
        }
        
        
        NSString *user_Qualification=[NSString stringWithFormat:@"%@",singleton.user_Qualification];
        
        if([user_Qualification length]==0 || user_Qualification ==nil|| [user_Qualification isEqualToString:@"(null)"])
        {
            user_Qualification=@"";
        }
        
        
        NSString *user_Occupation=[NSString stringWithFormat:@"%@",singleton.user_Occupation];
        if([user_Occupation length]==0 || user_Occupation ==nil|| [user_Occupation isEqualToString:@"(null)"])
        {
            user_Occupation=@"";
        }
        
        NSString *profileEmailSelected=[NSString stringWithFormat:@"%@",singleton.profileEmailSelected];
        
        if([profileEmailSelected length]==0 || profileEmailSelected ==nil|| [profileEmailSelected isEqualToString:@"(null)"])
        {
            profileEmailSelected=@"";
        }
        
        NSString *user_profile_URL=[NSString stringWithFormat:@"%@",singleton.user_profile_URL];
        
        if([user_profile_URL length]==0 || user_profile_URL ==nil|| [user_profile_URL isEqualToString:@"(null)"])
        {
            user_profile_URL=@"";
        }
        NSString *user_tkn=[NSString stringWithFormat:@"%@",singleton.user_tkn];
        if([user_tkn length]==0 || user_tkn ==nil|| [user_tkn isEqualToString:@"(null)"])
        {
            user_tkn=@"";
        }
        NSString *mobileNumber=[NSString stringWithFormat:@"%@",singleton.mobileNumber];
        
        if([mobileNumber length]==0 || mobileNumber ==nil|| [mobileNumber isEqualToString:@"(null)"])
        {
            mobileNumber=@"";
        }
        
        NSString *profileNameSelected=[NSString stringWithFormat:@"%@",singleton.profileNameSelected];
        
        if([profileNameSelected length]==0 || profileNameSelected ==nil|| [profileNameSelected isEqualToString:@"(null)"])
        {
            profileNameSelected=@"";
        }
        
        
        NSString *notiTypeGenderSelected=[NSString stringWithFormat:@"%@",singleton.notiTypeGenderSelected];
        if([notiTypeGenderSelected length]==0 || notiTypeGenderSelected ==nil|| [notiTypeGenderSelected isEqualToString:@"(null)"])
        {
            notiTypeGenderSelected=@"";
        }
        
        
        
        NSString *profilestateSelected=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
        
        if([profilestateSelected length]==0 || profilestateSelected ==nil|| [profilestateSelected isEqualToString:@"(null)"])
        {
            profilestateSelected=@"";
        }
        
        
        
        NSString *notiTypDistricteSelected=[NSString stringWithFormat:@"%@",singleton.notiTypDistricteSelected];
        
        if([notiTypDistricteSelected length]==0 || notiTypDistricteSelected ==nil|| [notiTypDistricteSelected isEqualToString:@"(null)"])
        {
            notiTypDistricteSelected=@"";
        }
        
        
        
        NSString *profileDOBSelected=[NSString stringWithFormat:@"%@",singleton.profileDOBSelected];
        
        if([profileDOBSelected length]==0 || profileDOBSelected ==nil|| [profileDOBSelected isEqualToString:@"(null)"])
        {
            profileDOBSelected=@"";
        }
        
        
        
        NSString *user_id=[NSString stringWithFormat:@"%@",singleton.user_id];
        
        [defaults setAESKey:@"UMANGIOSAPP"];
        
        if ([user_id length]>0) {
            // [defaults setObject:user_id forKey:@"USER_ID"];
            [defaults encryptValue:user_id withKey:@"USER_ID"];
            
        }
        
        
        
        [defaults encryptValue:aadhar_number withKey:@"ADHAR_ADHARNUM_KEY"];
        [defaults encryptValue:aadhar_image_url withKey:@"ADHAR_IMAGE_URL_KEY"];
        [defaults encryptValue:pincode withKey:@"ADHAR_PINCODE_KEY"];
        [defaults encryptValue:dob withKey:@"ADHAR_DOB_KEY"];
        [defaults encryptValue:email withKey:@"ADHAR_EMAIL_KEY"];
        [defaults encryptValue:gender withKey:@"ADHAR_GENDER_KEY"];
        [defaults encryptValue:mobile_number withKey:@"ADHAR_MOB_KEY"];
        [defaults encryptValue:name withKey:@"ADHAR_NAME_KEY"];
        [defaults encryptValue:district withKey:@"ADHAR_DISTRICT_KEY"];
        [defaults encryptValue:father_name withKey:@"ADHAR_FATHERNAME_KEY"];
        [defaults encryptValue:state withKey:@"ADHAR_STATE_KEY"];
        [defaults encryptValue:street withKey:@"ADHAR_STREET_KEY"];
        [defaults encryptValue:subdistrict withKey:@"ADHAR_SUBDISTRICT_KEY"];
        [defaults encryptValue:vtc withKey:@"ADHAR_VTC_KEY"];
        [defaults encryptValue:vtc_code withKey:@"ADHAR_VTCCODE_KEY"];
        [defaults encryptValue:aadhaar_address withKey:@"ADHAR_ADDRESS_KEY"];
        
        
        ///---------- Mobile Credential----------------------------------
        [defaults encryptValue:notiTypeCitySelected withKey:@"CITY_KEY"];
        [defaults encryptValue:altermobileNumber withKey:@"ALTERMB_KEY"];
        [defaults encryptValue:user_Qualification withKey:@"QUALI_KEY"];
        [defaults encryptValue:user_Occupation withKey:@"OCCUP_KEY"];
        [defaults encryptValue:profileEmailSelected withKey:@"EMAIL_KEY"];
        [defaults encryptValue:user_profile_URL withKey:@"URLPROFILE_KEY"];
        [defaults encryptValue:user_tkn withKey:@"TOKEN_KEY"];
        [defaults encryptValue:mobileNumber withKey:@"MOBILE_KEY"];
        [defaults encryptValue:profileNameSelected withKey:@"NAME_KEY"];
        [defaults encryptValue:notiTypeGenderSelected withKey:@"GENDER_KEY"];
        [defaults encryptValue:profilestateSelected withKey:@"STATE_KEY"];
        [defaults encryptValue:notiTypDistricteSelected withKey:@"DISTRICT_KEY"];
        [defaults encryptValue:profileDOBSelected withKey:@"DOB_KEY"];
        
        
        
        [defaults synchronize];
        //------------------------- Encrypt Value------------------------
        
    }
    singleton.user_profile_URL=[defaults decryptedValueForKey:@"USER_PIC"];
    NSLog(@"user_profile_URL=%@",singleton.user_profile_URL);
    
    
    [defaults synchronize];
    
    
    //========================================
    //========================================
    
    [self updateNofificationStatusAPI];//now
    
    
    
    /*
     
     dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", DISPATCH_QUEUE_SERIAL);
     dispatch_async(queue, ^{
     [self hitFetchHerospaceAPI];
     
     dispatch_async(dispatch_get_main_queue(), ^{
     [self hitHomeAPI];
     
     });
     });
     */
    [self performSelector:@selector(hitGCMAPINotifier) withObject:nil afterDelay:5.0];
    [self performSelector:@selector(hitTrendingAPI) withObject:nil afterDelay:0.1];
    
    
    
    
    
    self.navigationController.navigationBar.hidden = YES;
    
    // NotifycontainerView
    
    // tableView_home.frame=CGRectMake(0,69,377,548-NV_height);
    
    //tableView_home.frame=CGRectMake(0,69,377,fDeviceHeight-69-NV_height);
    
    
    
    
    
    
   
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
  //  tableView_home.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight);
//
//    if (iPhoneX())
//    {
//        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
//
//        float yX= kiPhoneXNaviHeight4Search;
//        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
//
//        float heightX = heightVar-tabBarHeight ;
//        tableView_home.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
//
//    }
    
    // UIEdgeInsets inset = UIEdgeInsetsMake(-20, 0,0, 0);
    // tableView_home.contentInset = inset;
    
    
    //tableView_home = [[UITableView alloc] initwithFrame:frame style:UITableViewStyleGrouped];
    
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
    
    _headerView = [[AdvertisingColumn alloc]init];
    _headerView.backgroundColor = [UIColor clearColor];
    _headerView.comingFrom = @"home";
    
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.0/255.0 green:89.0/255.0 blue:157.0/255.0 alpha:1.0]];
    
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1.0]];
    
    
    // NSArray *imgArray1 = [NSArray arrayWithObjects:@"banner1.png",@"banner2.png",@"banner3.png",@"banner4.png", nil];
    
    
    // [_headerView setArray:imgArray1];
    
    
    
    
    // Register the table cell
    [tableView_home registerClass:[HomeContainerCell class] forCellReuseIdentifier:@"HomeContainerCell"];
    
    // Add observer that will allow the nested collection cell to trigger the view controller select row at index path
    // self.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:248/255.0 blue:248/255.0 alpha:1.0];
    
    
    
    
    [tableView_home setShowsHorizontalScrollIndicator:NO];
    [tableView_home setShowsVerticalScrollIndicator:NO];
    
    
    
    
    
    
    vw_line.frame=CGRectMake(0, 74, fDeviceWidth, 0.5);
    
    refreshController = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 20, fDeviceWidth, 20)];
    // refreshController.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refreshController addTarget:self action:@selector(getEvents:) forControlEvents:UIControlEventValueChanged];
    // self.refreshControl = refresh;
    [tableView_home addSubview:refreshController];
    
    
    
    
    
    [self getEvents:refreshController];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSelector:@selector(callStateAPI) withObject:nil afterDelay:1];
    
    tableView_home.backgroundColor = [UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bannerStatusChanged)
                                                 name:Banner_State_Changed
                                               object:nil];
    
    btn_filter.accessibilityLabel = @"Apply Filter";
    
    
    //[self addNavigationSearchTabView];
    NSString *localString=[NSString stringWithFormat:@"%@",NSLocalizedString(@"tap_fav_icon_help_text", nil)];
    localString = [localString stringByReplacingOccurrencesOfString:@"u2665"
                                                         withString:@"♥"];
    NSLog(@"str=%@",localString);
    
    toAddFavouriteLabel.text = localString;
    
    noFavouriteFoundLabel.text = NSLocalizedString(@"no_favourites", nil);
    [self.segmentControl setTitle:NSLocalizedString(@"home_small", @"") forSegmentAtIndex:0];
    [self.segmentControl setTitle:NSLocalizedString(@"favourites_small", @"") forSegmentAtIndex:1];
    [tableView_home setContentInset:UIEdgeInsetsMake(-35, 0, 0, 0)];
//    if ([self.comingFrom isEqualToString:@"login"]) {
//        [tableView_home setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//        tableView_home.sectionHeaderHeight  = 0.0;
//      }
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    __block typeof(self) weakSelf = self;
    delegate.didCloseMPIN = ^{
        [weakSelf checkHintScreen];
    };
    
    
}

-(void)checkHintScreen {
    BOOL isHintShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"HintShown"];
    
    // NSLog(isHintShown ? @"Yes" : @"No");
    // [self showHint];
    
    if (!isHintShown)
    {
        
        hintShown = YES;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HintShown"];
        
        [self showHint];
        
    }
    else
    {
        hintShown = NO;
    }
    
}
#pragma mark- add Navigation View to View

-(void)addNavigationSearchTabView{
    
    if (iPhoneX())
    {
        
        // nvSearchView = [[NavigationSearchView alloc] init];
        // NavigationSearchView *nvSearchView = [[NavigationSearchView alloc] init];
        
        __weak typeof(self) weakSelf = self;
        vw_line.hidden=TRUE;
        
        nvSearchView.didTapFilterBarButton= ^(id btnfilter)
        {
            
            [weakSelf btn_filterAction:btnfilter];
            
        };
        
        
        nvSearchView.didTapNotificationBarButton= ^(id btnNotification)
        {
            
            [weakSelf btn_noticationAction:btnNotification];
            
        };
        
        nvSearchView.didTapSearchTextfield= ^(id txtSearchView)
        {
            
            [weakSelf openSearch];
            
            
        };
        
        [self.view addSubview:nvSearchView];
        
//        CGRect table = tableView_home.frame;
//        table.origin.y = kiPhoneXNaviHeight4Search;
//        table.size.height = fDeviceHeight - kiPhoneXNaviHeight4Search;
//        tableView_home.frame = table;
        [self.view layoutIfNeeded];
        
        
    }
    else
    {
        NSLog(@"Not an iPhone X use default UI");
    }
    
}


-(void)bannerStatusChanged
{
    
    BOOL selectedBanner = [[[NSUserDefaults standardUserDefaults]objectForKey:@"SELECTED_BANNER"] boolValue];
    
    NSLog(@"%@",NSLocalizedString(@"no", nil));
    
    if (selectedBanner == YES)
    {
        
        if ([listHeroSpace count]>0)
        {
            singleton.bannerStatus = YES;
            
        }
        else
        {
            // singleton.bannerStatus=NSLocalizedString(@"no", nil);//need to close here
            
        }
        
    }
    else if (selectedBanner == NO)
    {
        singleton.bannerStatus =  NO;
        
    }
    else
    {
        //singleton.bannerStatus=NSLocalizedString(@"yes", nil);
        if ([listHeroSpace count]>0)
        {
            singleton.bannerStatus = YES;
            
        }
        else
        {
            // singleton.bannerStatus=NSLocalizedString(@"no", nil); //need here to add changes
            
        }
    }
    [self reloadTableOnMainQueue];
    [self reloadTableOnMainQueue];

//    dispatch_async(dispatch_get_main_queue(), ^{
//        [tableView_home reloadData];
//    });
    
}
-(void)adjustSearchBarView
{
    
    vwTextBg.translatesAutoresizingMaskIntoConstraints = true;
    
    vwTextBg.frame = CGRectMake(self.view.frame.size.width/2 - 150, vwTextBg.frame.origin.y, 300, vwTextBg.frame.size.height);
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
//    searchIconImage.frame = CGRectMake(vwTextBg.frame.origin.x + 7, searchIconImage.frame.origin.y, searchIconImage.frame.size.width, searchIconImage.frame.size.height);
//
//    txt_searchField.frame = CGRectMake(searchIconImage.frame.origin.x + searchIconImage.frame.size.width + 5, txt_searchField.frame.origin.y, 265, txt_searchField.frame.size.height);
    
}

-(void)callStateAPI
{
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
}


- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}



- (void)getEvents:(UIRefreshControl *)refresh
{
    
    static BOOL refreshInProgress = NO;
    
    if (!refreshInProgress)
    {
        refreshInProgress = YES;
        
        //   refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing"]; // let the user know refresh is in progress
        
        //  [self hitFetchHerospaceAPI];
        
        [self.timer invalidate];
        
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
      //
       // tableView_home.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight);
        if (iPhoneX())
        {
            CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
            
            float yX= kiPhoneXNaviHeight4Search;
            float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
            
            float heightX = heightVar-tabBarHeight ;
            ///tableView_home.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
            
        }
        NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
        
        @try {
            //NSLog(@"lastTime=%@ %lu",lastTime,(unsigned long)[lastTime length]);
            if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]) {
                lastTime=@"";
                vw_RetryOption.hidden=TRUE;
            }
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        //[self hitRetryMethod];
        
        //[self hitHomeAPI];
        // [self hitRetryMethod];
        if (![self connected]) {
            // Not connected
            [self hitRetryMethod];
            
        } else
        {
            // Connected. Do some Internet stuff
            [self hitHomeAPI];
            
        }
        
        /*
         NSURL *scriptUrl = [NSURL URLWithString:@"http://www.google.com/"];
         NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
         if (data)
         {
         //NSLog(@"Device is connected to the Internet");
         [self hitHomeAPI];
         
         }
         else
         {
         //NSLog(@"Device is not connected to the Internet");
         [self hitRetryMethod];
         }
         */
        
        [refresh endRefreshing];
        refreshInProgress = NO;
        
        
        /*
         NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
         NSBlockOperation *deleteOperation = [[NSBlockOperation alloc] init];
         __weak NSBlockOperation *weakDeleteOperation = deleteOperation;
         [weakDeleteOperation addExecutionBlock:^{
         // Download your stuff
         // Finally put it on the right place:
         
         
         }];
         NSBlockOperation *saveToDataBaseOperation = [[NSBlockOperation alloc] init];
         __weak NSBlockOperation *weakSaveToDataBaseOperation = saveToDataBaseOperation;
         [weakSaveToDataBaseOperation addExecutionBlock:^{
         // Work with your NSData instance
         // Save your stuff
         
         }];
         [saveToDataBaseOperation addDependency:weakDeleteOperation];
         [myQueue addOperation:saveToDataBaseOperation];
         [myQueue addOperation:weakDeleteOperation];
         */
        
        
    }
}


-(void)hitGCMAPINotifier
{
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:singleton.user_tkn forKey:@"USER_TOKEN"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"HITGCMAPI" object:nil userInfo:userInfo];
    
    
    // [self crashButtonTapped:self];
}

- (IBAction)crashButtonTapped:(id)sender {
    /* CLSLog(@"crash occured=%@",CrashlyticsKit.description);
     [[Crashlytics sharedInstance] crash];
     
     [[Crashlytics sharedInstance] recordCustomExceptionName:@"HandledException" reason:@"Some reason" frameArray:@[]];*/
    
    [[Crashlytics sharedInstance] crash];
    
    
}
-(MaterialShowcase*)showcaseWithtext:(NSString*)text withView:(UIView*)view {
    
    MaterialShowcase *showcase = [[MaterialShowcase alloc] init];
    showcase.backgroundPromptColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:1];
    
    [showcase setTargetViewWithView:view];
    showcase.primaryText =@"";
    [showcase setDelegate:self];
    showcase.secondaryText = text;
    return  showcase;
    
}
//showCaseDidDismiss(showcase: MaterialShowcase)
-(void)showCaseWillDismissWithShowcase:(MaterialShowcase*)showcase {
    NSLog(@"showcase%@", showcase);
}
-(void)showCaseDidDismissWithShowcase:(MaterialShowcase*)showcase {
    NSLog(@"showcase%@", showcase);
    MaterialShowcase *show = nil ;
    UITextField *lbl = nil ;
    
    
    
    switch (showcase.tag) {
        case 6660:
        {
            
            show = [self showcaseWithtext:NSLocalizedString(@"hint_notif", nil) withView:btn_notification];
            [singleton traceEvents:@"Hint Screen Notification" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
            show.tag = 6661;
        }
            break;
        case 6661:
        {
            show = [self showcaseWithtext:NSLocalizedString(@"hint_sort", nil) withView:btn_filter];
            [singleton traceEvents:@"Hint Screen Sort" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
            show.tag = 6662;
        }
            break;
        case 6662:
        {
            
            lbl = [[UITextField alloc]init];
            if (iPhoneX())
            {
                lbl.frame=CGRectMake(CGRectGetMinX(txt_searchField.frame), 50, 70, CGRectGetHeight(txt_searchField.frame));
                lbl.text = txt_searchField.placeholder;
            }
            else
            {
                lbl.frame=CGRectMake(CGRectGetMinX(vwTextBg.frame), CGRectGetMaxY(txt_searchField.frame), 70, CGRectGetHeight(txt_searchField.frame));
                lbl.text = txt_searchField.placeholder;
            }

            
            lbl.textAlignment = NSTextAlignmentLeft;
            lbl.textColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:0.70];
            show = [self showcaseWithtext:NSLocalizedString(@"hint_search", nil) withView:lbl];
            show.tag = 6663;
            [singleton traceEvents:@"Hint Screen Search" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
        }
            break;
        case 6663:
        {
            [self showHomeDialogueBox];
        }
        default:
            break;
    }
    if (show != nil ) {
        [show showWithCompletion:nil];
    }
    
}





-(void)showHint
{
    
    //    CGRect tabBarRect = self.tabBarController.tabBar.frame;
    //    NSInteger buttonCount = self.tabBarController.tabBar.items.count;
    //    CGFloat containingWidth = tabBarRect.size.width/buttonCount;
    //    CGFloat originX = containingWidth * (buttonCount-1) ;
    //    CGRect containingRect = CGRectMake( (originX+containingWidth/2) -self.tabBarController.tabBar.frame.size.height/2, fDeviceHeight-self.tabBarController.tabBar.frame.size.height, self.tabBarController.tabBar.frame.size.height , self.tabBarController.tabBar.frame.size.height );
    
    MaterialShowcase *showcase = [[MaterialShowcase alloc] init];
    showcase.backgroundPromptColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:1];
    showcase.tag = 6660;
    //showcase.backgroundPromptColorAlpha = 0.96
    
    
    
    NSArray *array =[NSArray arrayWithArray:singleton.AppTabOrders];
    NSMutableArray *tempTabOrder =[NSMutableArray new];
    NSString *home = [NSLocalizedString(@"home_small", @"") capitalizedString];
    NSString *flagship = [NSLocalizedString(@"flagship", @"") capitalizedString];
    NSString *fav = [NSLocalizedString(@"favourites_small", @"") capitalizedString];
    NSString *allservices = [NSLocalizedString(@"all_services_small", @"") capitalizedString];
    NSString *state = NSLocalizedString(@"state_txt", @"") ;
    
    NSString *more  =[NSLocalizedString(@"more_Home", @"") capitalizedString];// tabName; //TabTitle
    NSString *livechat=NSLocalizedString(@"help_live_chat", @"");//tabName;
    
    
    
    for (int i =0; i<[array count]; i++)
    {
        NSString *tempItem = [array objectAtIndex:i];
        if ([tempItem containsString:@"home"])
        {
            NSLog(@"string contains home!");
            [tempTabOrder addObject: home];
        }
        else if ([tempItem containsString:@"HomeWithFav"]) {
            NSLog(@"string contains flagship!");
            [tempTabOrder addObject: home];
        }
        else if ([tempItem containsString:@"flagship"]) {
            NSLog(@"string contains flagship!");
            [tempTabOrder addObject: flagship];
        }
        else if ([tempItem containsString:@"fav"]) {
            NSLog(@"string contains fav!");
            [tempTabOrder addObject: fav];
        }
        else if ([tempItem containsString:@"allservices"]) {
            NSLog(@"string contains allservices!");
            [tempTabOrder addObject: allservices];
        }
        else if ([tempItem containsString:@"state"]) {
            NSLog(@"string contains state!");
            [tempTabOrder addObject:state];
        }
        else if ([tempItem containsString:@"livechat"]) {
            NSLog(@"string contains state!");
            [tempTabOrder addObject: livechat];
        }
        else if ([tempItem containsString:@"more"]) {
            NSLog(@"string contains state!");
            [tempTabOrder addObject: more];
        }
    }
    
    
    
    NSLog(@" tempTabOrder=%@",tempTabOrder);
    if ([tempTabOrder count]>0)
    {
        NSUInteger moreIndex = [tempTabOrder indexOfObject: more];
        
        
        [showcase setTargetViewWithTabBar:self.tabBarController.tabBar itemIndex:moreIndex];
        
        
        
    }
    
    showcase.primaryText =@"";
    [showcase setDelegate:self];
    showcase.secondaryText = NSLocalizedString(@"hint_menu", nil);
    [showcase showWithCompletion:nil];    // 0 59 187
    
    
    /* // Background
     
     // Target
     showcase.targetTintColor = UIColor.blue
     showcase.targetHolderRadius = 44
     showcase.targetHolderColor = UIColor.white
     // Text
     showcase.primaryTextColor = UIColor.white
     showcase.secondaryTextColor = UIColor.white
     showcase.primaryTextSize = 20
     showcase.secondaryTextSize = 15
     // Animation
     showcase.aniComeInDuration = 0.5 // unit: second
     showcase.aniGoOutDuration = 0.5 // unit: second
     showcase.aniRippleScale = 1.5
     showcase.aniRippleColor = UIColor.white
     showcase.aniRippleAlpha = 0.2*/
    //  CGRect tabRect=tabitemview.frame;
    
    //CGRect tabRect=CGRectMake(fDeviceWidth-65, fDeviceHeight-44, 35, 35);
    //    NSArray *coachMarks = @[
    //                            @{
    //                                @"rect": [NSValue valueWithCGRect:containingRect],
    //                                @"caption": NSLocalizedString(@"hint_menu", nil),
    //                                @"shape": @"circle"
    //                                },
    //                            @{
    //                                @"rect": [NSValue valueWithCGRect:btn_notification.frame],
    //                                @"caption": NSLocalizedString(@"hint_notif", nil),
    //                                @"shape": @"circle"
    //                                },
    //                            @{
    //                                @"rect": [NSValue valueWithCGRect:btn_filter.frame],
    //                                @"caption": NSLocalizedString(@"hint_sort", nil),
    //                                @"shape": @"circle"
    //                                },
    //                            @{
    //                                @"rect": [NSValue valueWithCGRect:vwTextBg.frame],
    //                                @"caption": NSLocalizedString(@"hint_search", nil),
    //                                },
    //                            ];
    //    coachMarksView = [[WSCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    //
    //    coachMarksView.homeDelegate = self;
    //
    //    UIViewController *vc=[self topMostController];
    //    [vc.view addSubview:coachMarksView];
    //
    //    // coachMarksView.animationDuration = 0.5f;
    //    //coachMarksView.enableContinueLabel = YES;
    //    coachMarksView.tag=900;
    //    [coachMarksView start];
    
    
}
-(UIView*)viewForTabBarItemAtIndex:(NSInteger)index {
    
    CGRect tabBarRect = self.tabBarController.tabBar.frame;
    NSInteger buttonCount = self.tabBarController.tabBar.items.count;
    CGFloat containingWidth = tabBarRect.size.width/buttonCount;
    CGFloat originX = containingWidth * index ;
    CGRect containingRect = CGRectMake( originX, fDeviceHeight, containingWidth, self.tabBarController.tabBar.frame.size.height );
    CGPoint center = CGPointMake( CGRectGetMidX(containingRect), CGRectGetMidY(containingRect));
    return [ self.tabBarController.tabBar hitTest:center withEvent:nil ];
}

//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}



//------ Method to get Your tab bar button view CGRect------------

-(UIView*)viewForTabBarItemAtIndex:(NSInteger)index withUITabBarController:(UITabBarController*)tabBarController

{
    
    CGRect tabBarRect = tabBarController.tabBar.frame;
    NSInteger buttonCount = tabBarController.tabBar.items.count;
    CGFloat containingWidth = tabBarRect.size.width/buttonCount;
    CGFloat originX = containingWidth * index ;
    CGRect containingRect = CGRectMake( originX, 0, containingWidth,tabBarController.tabBar.frame.size.height );
    CGPoint center = CGPointMake( CGRectGetMidX(containingRect), CGRectGetMidY(containingRect));
    return [tabBarController.tabBar hitTest:center withEvent:nil ];
}
//------ Method END to get Your tab bar button view CGRect------------


//------------------------------------------

-(HintViewController*)getHintLayerController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if (vwContHint == nil) {
        vwContHint = [storyboard instantiateViewControllerWithIdentifier:@"HintViewController"];
    }
    return vwContHint;
}


- (void) displayContentController: (UIViewController*) content;
{
    // [self addChildViewController:content];
    content.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:content.view];
    
    [self showViewControllerMiddle];
    
}

-(void)showViewControllerMiddle{
    
    if (vwContHint == nil)
    {
        vwContHint = [self getHintLayerController];
    }
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        vwContHint.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    if (vwContHint == nil) {
        vwContHint = [self getHintLayerController];
    }
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        vwContHint.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}


-(void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    //NSLog(@"Selected INDEX OF home TAB-BAR ==> %lu", (unsigned long)tabBarController.selectedIndex);
    
    int indexoftag=(int)tabBarController.selectedIndex;
    
    //NSLog(@"indexoftag=%d",indexoftag);
    self.tabBarController.selectedIndex = indexoftag;
    
    [singleton traceEvents:@"Tab Selected" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
    
    
}

-(void)fetchOnlyHomeDataFromDB {
    
    
    NSArray *arrServiceSection=[singleton.dbManager loadDataServiceSection];
    
    //NSLog(@"arrServiceSection=%@",arrServiceSection);
    
    
    NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
    
    if (arrServiceSection.count == 0 || arrServiceData.count == 0)
    {
        NSLog(@"fetchDatafromDB arrServiceSection EMPTY");
        [self hitHomeAPI];
        return;
    }
    
    NSMutableArray *arr_serviceWithSection=[[NSMutableArray alloc]init];
    
    
    [arrServiceSection enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
     {
         //NSArray *itemsService = [[[arrServiceSection objectAtIndex:idx]valueForKey:@"SECTION_SERVICES"] componentsSeparatedByString:@","];
         
         NSString *serviceString=[NSString stringWithFormat:@"%@",[[arrServiceSection objectAtIndex:idx]valueForKey:@"SECTION_SERVICES"]];
         NSArray *itemsTempService = [serviceString componentsSeparatedByString:@","];
         NSArray *itemsService=[NSArray arrayWithArray:itemsTempService];
         
         
         NSMutableArray *section_service=[[NSMutableArray alloc]init];
         
         [itemsService enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger itemIndex, BOOL * _Nonnull stop)
          {
              NSString *serviceCard=[NSString stringWithFormat:@"%@",[[arrServiceSection objectAtIndex:idx]valueForKey:@"SERVICE_CARD"]];
              
              //NSArray *sectionserviceitem=[singleton.dbManager getServiceData:[itemsService objectAtIndex:itemIndex]];
              
              NSArray *sectionserviceitem=[singleton.dbManager getServiceHomeData:[itemsService objectAtIndex:itemIndex] withServiceCard:serviceCard];
              
              ////NSLog(@"sectionserviceitem=%@",sectionserviceitem);
              if ([sectionserviceitem count]!=0)
              {
                  [section_service addObject:[sectionserviceitem objectAtIndex:0]];
              }
          }];
         
         
         if([section_service count]!=0)
         {
             //NSLog(@"arrServiceSection before kill");
             NSString *SECTION_IMAGE=  [[arrServiceSection objectAtIndex:idx]valueForKey:@"SECTION_IMAGE"];
             NSString  *SECTION_NAME= [[arrServiceSection objectAtIndex:idx]valueForKey:@"SECTION_NAME"];
             NSString  *ID =[[arrServiceSection objectAtIndex:idx]valueForKey:@"ID"];
             //NSLog(@"arrServiceSection after kill");
             NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:section_service,@"SECTION_SERVICES",SECTION_IMAGE,@"SECTION_IMAGE",SECTION_NAME,@"SECTION_NAME",ID,@"ID", nil];
             [arr_serviceWithSection addObject:dic_temp];
         }
         else
         {
             //its empty service list
         }
         
         
     }];
    
    
    self.sampleData=[[NSMutableArray alloc]init];
    [self.sampleData addObject:@"banner"];
    [self.sampleData addObject:@"segment"];
    for (int i=0; i<[arr_serviceWithSection count]; i++)
    {
        [self.sampleData addObject:[arr_serviceWithSection objectAtIndex:i]];
    }
    NSLog(@"============>self.sampleData=%@",self.sampleData);
}


-(void)fetchDatafromDB
{
    
    if (self.segmentControl.selectedSegmentIndex == 0) {
       [self fetchOnlyHomeDataFromDB];
    }else {
        [self fetchFavouriteServices];
    }
   
    [self reloadTableOnMainQueue];
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"AddDataInSearch" forKey:@"AddDataInSpotlight"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"SPOTLIGHTSEARCH" object:nil userInfo:userInfo];
}
-(void)fetchFavouriteServices
{
    singleton = [SharedManager sharedSingleton];
    
    self.tableData=[[NSMutableArray alloc]init];
    
    
    //NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
    NSArray *arrServiceData=[singleton.dbManager loadAllDataServiceDataIncludeFlag];
    
    NSLog(@"arrServiceData=%@",arrServiceData);
    
    
    for (int i=0; i<[arrServiceData count]; i++) {
        
        NSDictionary *oldDict = (NSDictionary *)[arrServiceData objectAtIndex:i];
        
        //[temp replaceObjectAtIndex:0 withObject:newDict];
        
        NSString *favStatus = [oldDict objectForKey:@"SERVICE_IS_FAV"]; //test the user exist
        if([favStatus isEqualToString:@"true"])                   //checking user exist or not
        {
            
            NSLog(@"Not Selected!");
            @try {
                if ([self.tableData containsObject:[arrServiceData objectAtIndex:i]]) // YES
                {
                    NSLog(@"contain element");
                }
                else
                {
                    [self.tableData addObject:[arrServiceData objectAtIndex:i]];
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
        }
        else
        {
            
            
            NSLog(@"Selected!");
        }
        
        
    }
    
    
    @try {
        
        NSSortDescriptor *aphabeticDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
        self.tableData = [[self.tableData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    [self checkNoFavouriteListView];
    
//   // [self checkServiceStatus];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [tableView_home reloadData];
//        [self addNotify];//add later
//    });
}
-(void)writetoaFile:(NSArray*)arr
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"file.txt"];
    
    //NSLog(@"the path is %@, the array is %@", path, array2);
    
    /////write to file/////
    [arr writeToFile:filePath atomically:YES];
    
    NSMutableArray *test = [[NSMutableArray alloc ]initWithContentsOfFile:filePath];
    
    NSLog(@"the test is %@", test);
}


-(void)refreshSettingData
{
    
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    NSLog(@"selectedLanguage=%@",selectedLanguage);
    
    //----- Condition for font change------
    NSString *selectedFont = [[NSUserDefaults standardUserDefaults] objectForKey:@"SELECTED_FONTSIZE"];
    
    if ([selectedFont isEqualToString:NSLocalizedString(@"small", nil)])
    {
        
        singleton.fontSizeSelected=NSLocalizedString(@"small", nil);
        
    }
    else if ([selectedFont isEqualToString:NSLocalizedString(@"normal", nil)])
    {
        singleton.fontSizeSelected=NSLocalizedString(@"normal", nil);
        
    }
    else if ([selectedFont isEqualToString:NSLocalizedString(@"large", nil)])
    {
        singleton.fontSizeSelected=NSLocalizedString(@"large", nil);
        
    }
    else
    {
        singleton.fontSizeSelected=NSLocalizedString(@"normal", nil);
    }
    
    
    //------------Setting for notifications ----------
    NSString *selectedNotification = [[NSUserDefaults standardUserDefaults] objectForKey:@"SELECTED_NOTIFICATION_STATUS"];
    if([selectedNotification isEqualToString:NSLocalizedString(@"enabled", nil)])
    {
        singleton.notificationSelected = NSLocalizedString(@"enabled", nil);
    }
    else
    {
        singleton.notificationSelected=NSLocalizedString(@"disabled", nil);
        
        
    }
    //------------Setting for Banner ----------
    
    
    
    [self bannerStatusChanged];
    
}



-(void)updateNofificationStatusAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    singleton = [SharedManager sharedSingleton];
    
    // Set the label text.
    if ([singleton.mobileNumber length]==0)
    {
        singleton.mobileNumber=@"";
    }
    
    
    if ([singleton.user_tkn length]==0)
    {
        singleton.user_tkn=@"";
    }
    if ([singleton.shared_ntfp length]==0)
    {
        singleton.shared_ntfp=@"";
    }
    
    if ([singleton.shared_ntft length]==0)
    {
        singleton.shared_ntft=@"";
    }
    
    if([singleton.shared_ntft isEqualToString:@"1"]&&[singleton.shared_ntfp isEqualToString:@"1"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IS_NOTIFICATION_ENABLED"];
        
    }
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"app" forKey:@"mod"];
    [dictBody setObject:@"" forKey:@"type"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:singleton.shared_ntft forKey:@"ntft"];
    [dictBody setObject:singleton.shared_ntfp forKey:@"ntfp"];
    
    
    [dictBody setObject:@"" forKey:@"imsi"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UPDATE_NOTIFICATION_SETTINGS withBody:dictBody andTag:TAG_REQUEST_NOTIFICATION_SETTINGS completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1]){
                // Update the value
                
                //singleton.shared_ntft = [dictBody objectForKey:@"ntft"];
                //singleton.shared_ntfp = [dictBody objectForKey:@"ntfp"];
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear: animated];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    //------------- Network View Handle------------
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    //------------- Tab bar Handle------------
    
    //AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    //appDelegate.shouldRotate = YES;
    //[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];
    //------------- Tab bar Handle------------
    
    
    [self.tabBarController.tabBar setHidden:NO];
    
    flagrotation=TRUE;
    
    [self refreshSettingData];
    [self resetOrientationView];
    self.hidesBottomBarWhenPushed=NO;
    //Do something
    //Do More Stuff
    
    self.tabBarController.delegate=self;
  
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectItemFromCollectionView:) name:@"didSelectItemFromCollectionView" object:nil];
    
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [singleton.reach startNotifier];
    

  
    
    //-------- Added later-------------
    [self badgeHandling];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(badgeHandling) name:@"NotificationRecievedComplete" object:nil];
    
//    BOOL selectedBanner = [[[NSUserDefaults standardUserDefaults]objectForKey:@"SELECTED_BANNER"] boolValue];
//
//    if ([[UIScreen mainScreen]bounds].size.height == 667)
//    {
//
//        if (selectedBanner == NO)
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(0, 0,0, 0);
//            tableView_home.contentInset = inset;
//
//        }
//        else
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(0, 0,0, 0);
//            tableView_home.contentInset = inset;
//        }
//
//
//    }
//
//    if ([[UIScreen mainScreen]bounds].size.height == 480)
//        // if ([[UIScreen mainScreen]bounds].size.height == 667)
//    {
//
//        //return 150;
//
//        if (selectedBanner == NO)
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(5, 0,0, 0);
//            tableView_home.contentInset = inset;
//
//        }
//        else
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(-15, 0,0, 0);
//            tableView_home.contentInset = inset;
//        }
//
//    }
//    if ([[UIScreen mainScreen]bounds].size.height == 568)
//        // if ([[UIScreen mainScreen]bounds].size.height == 667)
//    {
//
//        if (selectedBanner == NO)
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(6, 0,0, 0);
//            tableView_home.contentInset = inset;
//
//        }
//        else
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(-16, 0,0, 0);
//            tableView_home.contentInset = inset;
//        }
//
//
//    }
//
//    if ([[UIScreen mainScreen]bounds].size.height == 736)
//    {
//
//        if (selectedBanner == NO)
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(7, 0,0, 0);
//            tableView_home.contentInset = inset;
//
//        }
//        else
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(-9, 0,0, 0);
//            tableView_home.contentInset = inset;
//        }
//
//
//    }
//
//    if ([[UIScreen mainScreen]bounds].size.height == 812)
//    {
//
//        if (selectedBanner == NO)
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(7, 0,0, 0);
//            tableView_home.contentInset = inset;
//
//        }
//        else
//        {
//            UIEdgeInsets inset = UIEdgeInsetsMake(-9, 0,0, 0);
//            tableView_home.contentInset = inset;
//        }
//    }
    
    self.tabBarController.tabBar.itemPositioning = UITabBarItemPositioningCentered;
    [self fetchDatafromDB];
    [self setViewFont];
   // tableView_home.sectionHeaderHeight = 0.0;
    //tableView_home.tableHeaderView = self.headerBanner;
    
//NSString *test =  [singleton.dbManager getFlagServiceMultiCategoryID:@"Public Grievance"];

}
//-(void)viewWillLayoutSubviews {
//    [super viewWillLayoutSubviews];
//    tableView_home.sectionHeaderHeight = 0.0;
//    tableView_home.estimatedSectionHeaderHeight = 0.0;
//
//}

#pragma mark- Font Set to View
-(void)setViewFont{
    [btn_retryclick.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lbl_errorloading.font = [AppFont regularFont:17.0];
    txt_searchField.font = [AppFont mediumFont:13.0];
    txt_searchField.font = [AppFont mediumFont:13.0];
    noFavouriteFoundLabel.font = [AppFont mediumFont:18.0];
    toAddFavouriteLabel.font = [AppFont regularFont:15.0];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}

-(void)badgeHandling
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //open for testing purpose
    // NSString *badge=[NSString stringWithFormat:@"%d",1];
    //[[NSUserDefaults standardUserDefaults] setObject:badge forKey:@"BadgeValue"];
    
    app.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"appDelegate.badgeCount=%d",app.badgeCount);
    NSString*badgeValue=[NSString stringWithFormat:@"%d",app.badgeCount];
    
    if (app.badgeCount>0)
    {
        
        if (app.badgeCount>=10)      // 3 and 2 digit digit case
        {
            CustomBadge *badge = [CustomBadge customBadgeWithString:badgeValue withScale:.9];
            CGSize size = CGSizeMake(badge.frame.size.width, badge.frame.size.height);
            
            //CGPoint point = CGPointMake(btn_notification.frame.size.width+5-badge.frame.size.width, -14);
            CGPoint  point =  CGPointMake(btn_notification.frame.size.width+5-badge.frame.size.width, -14);
            
            CGRect rect = CGRectMake(point.x, point.y, size.width, size.height+8);
            [badge setFrame:rect];
            badge.userInteractionEnabled=NO;
            badge.tag=786;
            
            //  [btn_notification addSubview:badge];
            
            
            [ btn_notification addSubview:badge];
            
        }
        else
        {
            CustomBadge * badge = [CustomBadge customBadgeWithString:badgeValue];
            //CGPoint point = CGPointMake(btn_notification.frame.size.width/2+8-badge.frame.size.width/2, -8);
            
            CGPoint  point =  CGPointMake(btn_notification.frame.size.width/2+8-badge.frame.size.width/2, -8);
            
            CGRect rect = CGRectMake(point.x, point.y, 21, 21);
            [badge setFrame:rect];
            badge.userInteractionEnabled=NO;
            badge.tag=786;
            // [btn_notification addSubview:badge];
            [ btn_notification addSubview:badge];
            
            
        }
        
        
        
    }
    else
    {
        // for (UIView *subview in [btn_notification subviews])
        for (UIView *subview in [ btn_notification subviews])
            
        {
            if (subview.tag ==786) {
                [subview removeFromSuperview];
            }
            
        }
        
        
    }
    
    
}




/*
 -(void)removedataforchecking
 {
 [self.sampleData removeAllObjects];
 
 [tableView_home reloadData];
 }*/

-(IBAction)retryAction:(id)sender
{
    
    vw_RetryOption.hidden=TRUE;
    // tableView_home.hidden=false;
    [self.timer invalidate];
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
   // tableView_home.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight);
    if (iPhoneX())
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        float yX= kiPhoneXNaviHeight4Search;
        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
        
        float heightX = heightVar-tabBarHeight ;
        //tableView_home.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
        
    }
    if (![self connected]) {
        // Not connected
        [self hitRetryMethod];
        
    } else {
        // Connected. Do some Internet stuff
        [self hitHomeAPI];
        
    }
    
    //[tableView_home reloadData];
    //[self reloadTableOnMainQueue];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [tableView_home reloadData];
//    });
    
}
- (void)didReceiveMemoryWarning
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didSelectItemFromCollectionView" object:nil];
}
#pragma mark- Segment Control Changes...

- (IBAction)segmentControlDidChangeValueAction:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 1) {
        [self fetchFavouriteServices];
    }else {
        self.vw_noServiceFound.hidden = true;
        [self fetchOnlyHomeDataFromDB];
    }
    [self reloadTableOnMainQueue];
}

-(void)reloadTableOnMainQueue {
    __block typeof(tableView_home) tableHome = tableView_home;
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [tableHome reloadData];
        //[tableHome setContentInset:UIEdgeInsetsMake(-35, 0, 0, 0)];
       
     }];
}

-(CGFloat)getHeaderHeight {
    if (singleton.bannerStatus == YES && [listHeroSpace count]>0)
    {
        if (iPhoneX())
        {
            //return 150;
            return 125;
        }
        
        else if ([[UIScreen mainScreen]bounds].size.height == 812)
        {
            //return 150;
            return 135;
            
        }
        
        else if ([[UIScreen mainScreen]bounds].size.height >= 815)
        {
           //eturn 150;
            return 215;
            
        }
        else if ([[UIScreen mainScreen]bounds].size.height == 736)
        {
            
            //return 150;
            return 135;
            
        }
        else if ([[UIScreen mainScreen]bounds].size.height == 414)
        {
            //return 150;
            return 135;
        }
        else if ([[UIScreen mainScreen]bounds].size.height == 480)
        {
            return 105;
        }
        else if ([[UIScreen mainScreen]bounds].size.height == 568)
        {
            return 105;
        }
        else
        {
            return 120;
        }
    }
    else return 0;
}
#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    if ([self.sampleData count]==1)
    {
        NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
        //NSLog(@"lastTime=%@ %lu",lastTime,(unsigned long)[lastTime length]);
        if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]) {
            lastTime=@"";
            vw_RetryOption.hidden=TRUE;
        }
        else
        {
            @try {
                NSString *valueOfbanner=[NSString stringWithFormat:@"%@",[self.sampleData objectAtIndex:0]];
                if ([valueOfbanner isEqualToString:@"banner"])
                {
                    vw_RetryOption.hidden=TRUE;
                }
                else
                {
                    vw_RetryOption.hidden=false;
                }
                
            } @catch (NSException *exception) {
            } @finally
            {
            }
        }
    }
    else
    {
        vw_RetryOption.hidden=true;
        
    }
    if (self.segmentControl.selectedSegmentIndex == 1) {
        return 3;
    }
    return [self.sampleData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0 || section == 1) {
        return 0;
    }
    if (self.segmentControl.selectedSegmentIndex == 1) {
        return self.tableData.count;
    }
    return 1;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    //    [self addNotify];
    //
    //    NSLog(@"[[UIScreen mainScreen]bounds].size.height =%f",[[UIScreen mainScreen]bounds].size.height);
    //    if ([[UIScreen mainScreen]bounds].size.height == 667)
    //    {
    //
    //        //return 150;
    //        UIEdgeInsets inset = UIEdgeInsetsMake(-3, 0,0, 0);
    //        tableView_home.contentInset = inset;
    //
    //    }
    //
    //    if ([[UIScreen mainScreen]bounds].size.height == 480||[[UIScreen mainScreen]bounds].size.height == 568)
    //    {
    //
    //        //return 150;
    //        UIEdgeInsets inset = UIEdgeInsetsMake(-10, 0,0, 0);
    //        tableView_home.contentInset = inset;
    //
    //    }
    //
    //
    //    [self resetOrientationView];
}


/*-(void)resetOrientationView
 {
 UIViewController *vc=[self topMostController];
 
 for (UIView *subview in [vc.view subviews])
 {
 if ([subview isKindOfClass:[coachMarksView class]]) {
 [coachMarksView removeFromSuperview];
 // BOOL isHintShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"HintShown"];
 [self showHint];
 
 }
 }
 [self addNotify];
 // _headerView.frame = CGRectMake(0, 5, fDeviceWidth, AD_height);
 
 
 
 _headerView.frame = CGRectMake(0, 5, fDeviceWidth, AD_height+15);
 
 if ([[UIScreen mainScreen]bounds].size.height == 736)
 {
 
 //return 150;
 _headerView.frame = CGRectMake(0, 5, fDeviceWidth, AD_height+25);
 
 }
 
 [_headerView layoutSubviews];
 [_headerView updateFrameOfAllComponents];
 NSArray *imagearray=[singleton.dbManager getBannerHomeData];
 
 [_headerView setArray:imagearray];
 
 
 
 
 NSLog(@"tableView_home = %f height=%f",tableView_home.frame.size.width,tableView_home.frame.size.height);
 dispatch_async(dispatch_get_main_queue(), ^{
 // tableView_home.contentSize = CGSizeMake(tableView_home.frame.size.width, tableView_home.contentSize.height);
 [tableView_home reloadData];
 });
 
 [tableView_home beginUpdates];
 [tableView_home endUpdates];
 
 NSLog(@"==================== %@==================",singleton.bannerStatus);
 
 }*/

-(void)resetOrientationView
{
    
    [self addNotify];
    
    NSLog(@"tableView_home = %f height=%f",tableView_home.frame.size.width,tableView_home.frame.size.height);
    dispatch_async(dispatch_get_main_queue(), ^{
        // tableView_home.contentSize = CGSizeMake(tableView_home.frame.size.width, tableView_home.contentSize.height);
        [tableView_home reloadData];
    });
    
//    [tableView_home beginUpdates];
//    [tableView_home endUpdates];
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (self.segmentControl.selectedSegmentIndex == 0) {
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"HomeContainerCell"];
        
        HomeContainerCell *cell=[tableView_home cellForRowAtIndexPath:indexPath];
        
        if (cell == nil)
        {
            cell = [[HomeContainerCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        
        //NSLog(@"[indexPath section]=%ld",(long)[indexPath section]);
        //NSLog(@"indexPath.row=%ld",(long)indexPath.row);
        
        //    if ([indexPath section]==0 )
        //    {
        //        //if ([singleton.bannerStatus isEqualToString:NSLocalizedString(@"yes", nil)])
        //        if (singleton.bannerStatus == YES && [listHeroSpace count]>0)
        //        {
        //            [cell.contentView addSubview:_headerView];
        //        }
        //        else
        //        {
        //            //NSLog(@"Inside remvoe banner code");
        //
        //            [_headerView removeFromSuperview];
        //        }
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //        //  _headerView.contentMode = UIViewContentModeScaleAspectFit;
        //        //_headerView.backgroundColor=[UIColor greenColor];
        //
        //    }
        //
        //
        //    else
        //    {
        
        if ([self.sampleData count]>1) {
            
            
            //NSLog(@"self.sampleData=%@",self.sampleData);
            
            //cause kill
            
            NSDictionary *cellData = (NSDictionary*)[self.sampleData objectAtIndex:[indexPath section]];
            //NSLog(@"cellData=%@",cellData);
            
            NSArray *AllServicesData = [cellData objectForKey:@"SECTION_SERVICES"];
            //  //NSLog(@"AadharCardData=%@",AadharCardData);
            
            [cell setCollectionData:AllServicesData];
        }
        else
            
        {
            //do nothing
        }
        return cell;

    }
    else {
        // }
        static NSString *simpleTableIdentifier = @"FavouriteCell";
        
        FavouriteCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[FavouriteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        
        // dispatch_async(dispatch_get_main_queue(), ^{
        
        
        cell.lbl_serviceName.text=[[self.tableData objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"];
        cell.lbl_serviceDesc.text=[[self.tableData objectAtIndex:indexPath.row] valueForKey:@"SERVICE_DEPTDESCRIPTION"];
        
        NSString *imageName= [[self.tableData objectAtIndex:indexPath.row] valueForKey:@"SERVICE_IMAGE"];
        
        
        NSURL *url=[NSURL URLWithString:imageName];
        
        [cell.img_serviceCateg sd_setImageWithURL:url
                                 placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
        
        
        
        
        cell.lbl_nouse.text = @"";
        
        
        //---------- Set business Unit color here---------------------------------
        // cell.lbl_serviceCateg.backgroundColor = [UIColor clearColor];
        //NSLog(@"get_BUcolorcode=%@",get_BUcolorcode);
        // cell.lbl_serviceCateg.textColor=[UIColor whiteColor];
        
        
      //  NSString *category = [[self.tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_CATEGORY"];
        NSString *multi_Category = [[self.tableData  objectAtIndex:indexPath.row] valueForKey:@"MULTI_CATEGORY_NAME"];
        
        
       // MULTI_CATEGORY_NAME
        NSMutableArray *arrCat = [[NSMutableArray alloc] init];
        /*NSArray *arr = [category componentsSeparatedByString:@","];
         for (NSString *cat in arr) {
         [arrCat addObject:cat];
         }*/
        //[arrCat addObject:category];
        if ([multi_Category containsString:@","]) {
            NSArray *arrMulti = [multi_Category componentsSeparatedByString:@","];
            [arrCat addObjectsFromArray:arrMulti];
        }else {
              [arrCat addObject:multi_Category];
        }
        
        NSString *stateId = [[self.tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_STATE"];
        NSString *stateName = [obj getStateName:stateId];
        if (stateId.length == 0 || [stateId isEqualToString:@"99"]) {
            stateName =  NSLocalizedString(@"central", @"");
        }
        NSMutableArray *otherStatesNames = [[NSMutableArray alloc] init] ;
        if (stateId.length != 0 && ![stateId isEqualToString:@"99"])
        {
            NSString *otherStateId = [[self.tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_OTHER_STATE"];
            if (otherStateId.length != 0) {
                NSString *newString = [otherStateId substringToIndex:[otherStateId length]-1];
                NSString *finalString = [newString substringFromIndex:1];
                if (finalString.length != 0)
                {
                    NSArray *otherStates = [finalString componentsSeparatedByString:@"|"];
                    NSLog(@"%@",otherStates);
                    for (NSString *stateIDS in otherStates) {
                        [otherStatesNames addObject: [obj getStateName:stateIDS]];
                    }
                }
            }
            
        }
        if ([otherStatesNames containsObject:stateName]) {
            [otherStatesNames removeObject:stateName];
        }
        NSMutableArray *arrState = [[NSMutableArray alloc] initWithObjects:stateName, nil];
        if (otherStatesNames.count != 0) {
            [arrState addObjectsFromArray:otherStatesNames];
        }
        /*NSString *otherStateID = [[tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_OTHER_STATE"];
         if (otherStateID.length != 0 && ![otherStateID isEqualToString:@"99"]) {
         NSArray *arr = [otherStateID componentsSeparatedByString:@","];
         for (NSString *stateID in arr) {
         [arrState addObject:[obj getStateName:stateID]];
         }
         }*/
        
        //  NSString *stateName = [obj getStateName:stateId];
        
        //cell.lbl_serviceCateg.text= category;
        
        CGFloat fontSize = 12.0;
        
        [cell addCategoryStateItemsToScrollView:arrCat state:arrState withFont:fontSize cell:cell andTagIndex:indexPath];
        
        //        CGRect frame = [self rectForText:category usingFont:[UIFont systemFontOfSize:fontSize] boundedBySize:CGSizeMake(self.view.frame.size.width, cell.lbl_serviceCateg.frame.size.height)];
        
        
        
        //        cell.lbl_serviceCateg.frame=CGRectMake(cell.lbl_serviceCateg.frame.origin.x, cell.lbl_serviceCateg.frame.origin.y, frame.size.width+20,cell.lbl_serviceCateg.frame.size.height);
        //        cell.lbl_serviceCateg.layer.cornerRadius = 10.0;
        //        cell.lbl_serviceCateg.layer.backgroundColor=[UIColor colorWithRed:235/255.0 green:86/255.0 blue:2/255.0 alpha:1.0].CGColor;
        
        
        [cell.btn_serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btn_serviceFav.tag=indexPath.row ;
        
        cell.btn_moreInfo.tag=indexPath.row;
        
        
        [cell.btn_moreInfo  addTarget:self action:@selector(btnMoreInfoClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSString *rating=[[self.tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_RATING"];
        //[cellData objectForKey:@"SERVICE_RATING"];
        cell.lbl_rating.text=rating;
        cell.lbl_serviceName.font = [AppFont semiBoldFont:16.0];
        cell.lbl_serviceDesc.font = [AppFont lightFont:14.0];
        cell.lbl_serviceDesc.numberOfLines = 2;
        cell.lbl_serviceDesc.lineBreakMode = NSLineBreakByWordWrapping;
        cell.lbl_rating.font = [AppFont regularFont:13.0];
        //});
        [cell viewWithTag:787878].hidden = true;

        if (indexPath.row == 0) {
            [cell viewWithTag:787878].hidden = false;
        }
        
        return cell;
    }
  
    
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    if ([cell isKindOfClass:[FavouriteCell class]])
    {
        FavouriteCell *favCell = (FavouriteCell*)cell;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //            favCell.lblName.font = [UIFont systemFontOfSize:16.0];
            //            favCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            //            [favCell setNeedsDisplay];
            //        });
            
            //  }
            
            
            CGRect buttonImageAdvanceFrame =  favCell.img_serviceCateg.frame;
            buttonImageAdvanceFrame.origin.x = singleton.isArabicSelected ?CGRectGetWidth(tableView.frame) - 62 : 12;
            favCell.img_serviceCateg.frame = buttonImageAdvanceFrame;
            
            
            CGRect lblServiceTitle =  favCell.lbl_serviceName.frame;
            lblServiceTitle.origin.x = singleton.isArabicSelected ? 10  : 80;
            lblServiceTitle.size.width = favCell.frame.size.width - 100;
            favCell.lbl_serviceName.frame = lblServiceTitle;
            favCell.lbl_serviceName.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            
            CGRect lbl_servicedescFrame =  favCell.lbl_serviceDesc.frame;
            lbl_servicedescFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
            lbl_servicedescFrame.size.width = favCell.frame.size.width - 100;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                lbl_servicedescFrame.origin.y = CGRectGetMaxY(lblServiceTitle);
                lbl_servicedescFrame.size.height = 45;
            }
            
            favCell.lbl_serviceDesc.frame = lbl_servicedescFrame;
            favCell.lbl_serviceDesc.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            CGRect lblRatingFrame =  favCell.lbl_rating.frame;
            lblRatingFrame.origin.x = singleton.isArabicSelected ? favCell.frame.size.width - 95  : 80;
            favCell.lbl_rating.frame = lblRatingFrame;
            //        cell.lbl_rating.backgroundColor = [UIColor redColor];
            
            CGRect starImageFrame =  favCell.img_star.frame;
            starImageFrame.origin.x = singleton.isArabicSelected ? lblRatingFrame.origin.x -20  : lblRatingFrame.origin.x +lblRatingFrame.size.width -8;
            favCell.img_star.frame = starImageFrame;
            
            
            
            
            CGRect categoryLabelFrame =  favCell.lbl_serviceCateg.frame;
            categoryLabelFrame.origin.x = singleton.isArabicSelected ? starImageFrame.origin.x - 90  : starImageFrame.origin.x + 30 ;
            favCell.lbl_serviceCateg.frame = categoryLabelFrame;
            
            
            CGRect btnFavFrame =  favCell.btn_serviceFav.frame;
            btnFavFrame.origin.x = singleton.isArabicSelected ? 10  : favCell.frame.size.width - 30 ;
            favCell.btn_serviceFav.frame = btnFavFrame;
            
            CGRect btnMoreFrame =  favCell.btn_moreInfo.frame;
            btnMoreFrame.origin.x = singleton.isArabicSelected ? 5  : favCell.frame.size.width - 36 ;
            favCell.btn_moreInfo.frame = btnMoreFrame;
            
        });
    }
}

/*- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
 if ([indexPath section ] == 0){
 return CGSizeMake(320, 100);
 }else
 return CGSizeMake(320, 180);
 
 }*/


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // This code is commented out in order to allow users to click on the collection view cells.
    /*   if (!self.detailViewController) {
     self.detailViewController = [[ORGDetailViewController alloc] initWithNibName:@"ORGDetailViewController" bundle:nil];
     }
     NSDate *object = _objects[indexPath.row];
     self.detailViewController.detailItem = object;
     [self.navigationController pushViewController:self.detailViewController animated:YES];*/
    if (self.segmentControl.selectedSegmentIndex == 0) {
        return;
    }
    NSDictionary *cellData = [self.tableData objectAtIndex:indexPath.row];
    if (cellData)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        vc.dic_serviceInfo=cellData;
        vc.tagComeFrom=@"OTHERS";
        
        [singleton traceEvents:@"Department Page" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
        
        vc.sourceTab     = @"favourites";
        vc.sourceState   = @"";
        vc.sourceSection = @"";
        vc.sourceBanner  = @"";
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    
    
}


#pragma mark UITableViewDelegate methods

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSDictionary *sectionData = [self.sampleData objectAtIndex:section];
//    NSString *header = [sectionData objectForKey:@"category"];
//    return header;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0 ){
        return 0.001;
    }
    else
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            return 20;
        }
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0){
       // return  UITableViewAutomaticDimension;
        if (singleton.bannerStatus == YES && [listHeroSpace count]>0)
        {
            if (iPhoneX())
            {
                
                //return 150;
                return 125;
                
            }
            
            else if ([[UIScreen mainScreen]bounds].size.height == 812)
            {
                
                //return 150;
                return 135;
                
            }
            
            else if ([[UIScreen mainScreen]bounds].size.height >= 815)
            {
                
                //return 150;
                return 215;
                
            }
            else if ([[UIScreen mainScreen]bounds].size.height == 736)
            {
                
                //return 150;
                return 135;
                
            }
            else if ([[UIScreen mainScreen]bounds].size.height == 414)
            {
                //return 150;
                return 135;
            }
            else if ([[UIScreen mainScreen]bounds].size.height == 480)
            {
                return 105;
            }
            else if ([[UIScreen mainScreen]bounds].size.height == 568)
            {
                return 105;
            }
            else
            {
                return 120;
            }
        }
        else
            return 0;
     }
    else if (section == 1 ) {
        if (self.segmentControl.selectedSegmentIndex == 0) {
            return 40.0;
        }
        return 55.0;
    }
    else {
        if (self.segmentControl.selectedSegmentIndex == 0) {
            return 40.0;
        }
        return 0.0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"===========>BAHUBALI<========= %f",[[UIScreen mainScreen]bounds].size.height );
    //return 180.0;
    if([indexPath section]==0)
    {
        
    }
    else {
        if (self.segmentControl.selectedSegmentIndex == 0) {
            if ([[UIScreen mainScreen]bounds].size.height >= 1024)
            {
                return 230;
            }
            return 155;
        }else {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                return 120;
            }
            return 115;
        }
     }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section

{
    
    if (section == 0 || section == 1){
        //do all the stuff here for CellA
        return nil;
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(2.0, 0.0, fDeviceWidth, 25.0)];
        
        
        
        //---------- Top View of header line-----------
        UIView *headerTopView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,0.5)];
        // headerTopView.backgroundColor = [UIColor colorWithRed:163/255.0 green:163/255.0 blue:163/255.0 alpha:1.0];
        [myView addSubview:headerTopView];
        
        
        //myView.backgroundColor=[UIColor whiteColor];
        
        
        //  headerTopView.backgroundColor=[UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
        
        // myView.backgroundColor=[UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
        
        
        
        
        headerTopView.backgroundColor= [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
        
        myView.backgroundColor= [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
        
        // myView.backgroundColor=[UIColor orangeColor];
        return myView;
        
        
        
    }else{
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad && self.segmentControl.selectedSegmentIndex == 0){
            UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, fDeviceWidth, 20)];
            myView.backgroundColor= [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
            return myView;
        }
    }
    return nil;
    
}







#pragma mark UITableViewDelegate methods
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    //Headerview
    
    
    //NSLog(@"viewForHeaderInSection =%lu",(unsigned long)[self.sampleData count]);
    if (section == 0)
    {
        if (singleton.bannerStatus == YES && [listHeroSpace count]>0)
        {
            self.headerBanner.frame = CGRectMake(0, 0, 0, [self getHeaderHeight]);
            
            return  self.headerBanner;
        }
        else
        {
            //NSLog(@"Inside remvoe banner code");
            return nil;
        }
    }
    //else if (section == 1)
    if (section == 1)
    {
        return  self.viewSegmentHeader;
    }
    else if (self.segmentControl.selectedSegmentIndex == 0){
        
        if ([self.sampleData count]>1)
        {
            
            //NSLog(@"viewForHeaderInSection inside =%lu",(unsigned long)[self.sampleData count]);
            
            
            UIView *myView;
            if ([[UIScreen mainScreen]bounds].size.height >= 740) {
                myView = [[UIView alloc] initWithFrame:CGRectMake(2.0, 0.0, fDeviceWidth, 40.0)];
            }
            else
            {
                myView = [[UIView alloc] initWithFrame:CGRectMake(2.0, 0.0, 315.0, 40.0)];
            }
            
            //--------- Adding UIButton------------------------------------------
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            
            [button setFrame:CGRectMake(fDeviceWidth-80, 10, 70.0, 30.0)];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            //button.tag = section;
            [button setTitle:[NSLocalizedString(@"more_Home", nil) capitalizedString] forState:UIControlStateNormal];
            [button setTitle:[NSLocalizedString(@"more_Home", nil) capitalizedString] forState:UIControlStateSelected];
            button.titleLabel.font = [UIFont systemFontOfSize:15];
            button.tag=section;
            
            [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            
            [button setTitleColor:[[UIColor colorWithRed:6.0/255.0 green:84.0/255.0 blue:141.0/255.0 alpha:1.0] colorWithAlphaComponent:1.0] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(morePressed:) forControlEvents:UIControlEventTouchDown];
            [myView addSubview:button];
            
            //--------- More Arrow Icon------------------------------------------
            /*  UIImageView *icon_more= [[UIImageView alloc] initWithFrame:CGRectMake(button.frame.origin.x+43, 25, 12, 12)];
             icon_more.image=[UIImage imageNamed:@"icon_more_arrow"];
             
             [myView addSubview:icon_more];
             
             */
            
            
            //---------- Footer View of header line-----------
            UIView *headerFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, myView.frame.size.height-1,fDeviceWidth,0.5)];
            // headerFooterView.backgroundColor =[UIColor colorWithRed:183/255.0 green:182/255.0 blue:189/255.0 alpha:1.0];
            [myView addSubview:headerFooterView];
            
            
            
            
            
            
            NSDictionary *sectionData = (NSDictionary*)[self.sampleData objectAtIndex:section];
            
            //--------- Adding Category Icon------------------------------------------
            UIImageView *icon_category = [[UIImageView alloc] initWithFrame:CGRectMake(10, 12, 25, 25)];
            
            NSString* category_icon=[sectionData objectForKey:@"SECTION_IMAGE"];
            
            
            NSURL *url=[NSURL URLWithString:category_icon];
            
            [icon_category sd_setImageWithURL:url
                             placeholderImage:[UIImage imageNamed:@"icon_recent.png"]];
            
            
            //NSLog(@"SECTION");
            
            [myView addSubview:icon_category];
            
            //--------- Adding Category Label------------------------------------------
            
            UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(40, 8, 180, 30)];
            NSString *header = [sectionData objectForKey:@"SECTION_NAME"];
            if(header.length>0)
            {
                NSLog(@"1befor header");
                
                header = [NSString stringWithFormat:@"%@%@",[[header substringToIndex:1] uppercaseString],[header substringFromIndex:1] ];
                NSLog(@"after header");
                
            }
            labelHeader.text = header;
            labelHeader.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
            
            labelHeader.textColor = [UIColor blackColor];
            
            // myView.backgroundColor=[UIColor whiteColor];
            [myView addSubview:labelHeader];
            
            
            
            myView.backgroundColor= [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
            
            headerFooterView.backgroundColor=  [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
            
            
            // Update frames for Arabic Language
            
            if (singleton.isArabicSelected) {
                [button setFrame:CGRectMake(20, 10, 80.0, 30.0)];
                labelHeader.frame  = CGRectMake(fDeviceWidth - 220, 8, 180, 30);
                labelHeader.textAlignment = NSTextAlignmentRight;
                icon_category.frame =  CGRectMake(fDeviceWidth - 35, 12, 25, 25);
                button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                
            }
            else{
                
                [button setFrame:CGRectMake(fDeviceWidth-85, 10, 80.0, 30.0)];
                labelHeader.frame  = CGRectMake(40, 8, 180, 30);
                labelHeader.textAlignment = NSTextAlignmentLeft;
                icon_category.frame =  CGRectMake(10, 12, 25, 25);
                button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                
                
            }
            // myView.backgroundColor=[UIColor purpleColor];
            
            
            @try {
                NSDictionary *tempcellData = (NSDictionary*)[self.sampleData objectAtIndex:section];
              //  NSLog(@"tempcellData===>%@",tempcellData);
               // NSLog(@"self.sampleData===>%@",self.sampleData);
                
                NSArray *sectionServicesData = [tempcellData objectForKey:@"SECTION_SERVICES"];
            //    NSLog(@"sectionServicesData=%@ and count =%lu",sectionServicesData,(unsigned long)[sectionServicesData count]);
                if([sectionServicesData count]<=5)
                {
                    button.hidden=TRUE;
                }
                else
                {
                    button.hidden=FALSE;
                    
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            if (section == 1)
            {
                //do all the stuff here for Cell banner
                if ([[UIScreen mainScreen]bounds].size.height >= 740)
                {
                    myView.frame=CGRectMake(2.0, 0.0, 315.0, 32.0);
                }
                else
                {
                    myView.frame=CGRectMake(2.0, 0.0, 315.0, 32.0);
                }
                
                if (singleton.isArabicSelected) {
                    [button setFrame:CGRectMake(20, 4, 80.0, 30.0)];
                    labelHeader.frame  = CGRectMake(fDeviceWidth - 220, 4, 180, 30);
                    labelHeader.textAlignment = NSTextAlignmentRight;
                    icon_category.frame =  CGRectMake(fDeviceWidth - 35, 6, 25, 25);
                    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    
                }
                else{
                    
                    [button setFrame:CGRectMake(fDeviceWidth-85, 4, 80.0, 30.0)];
                    labelHeader.frame  = CGRectMake(40, 4, 180, 30);
                    labelHeader.textAlignment = NSTextAlignmentLeft;
                    icon_category.frame =  CGRectMake(10, 6 , 25, 25);
                    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                    
                    
                }
                headerFooterView.frame=CGRectMake(0, myView.frame.size.height-1,fDeviceWidth,0.5);
           
            }
            return myView;
            
        }
        else
        {
            
            [self fetchDatafromDB];// show here your retry options
        }
        
    }
    return nil;
}
#pragma mark - Banner Selection
- (void)didTapBannerWithBanner:(NSDictionary *)banner {
    NSLog(@"banner dic tap --%@", banner);
    AdvertisingColumn *colomn = [[AdvertisingColumn alloc] init];
    [colomn selectedIndexNotify:banner];
    //[self :banner];
}

-(void)morePressed:(UIButton*)sender
{
    
    
    int index=(int)[sender tag];
    
    //NSLog(@"tag=%d",index);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    NSDictionary *cellData = (NSDictionary*)[self.sampleData objectAtIndex:index];
    NSArray *arr_category = [cellData objectForKey:@"SECTION_SERVICES"];
    ShowMoreServiceVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowMoreServiceVC"];
    vc.arr_service=arr_category;
    vc.categ_title= [cellData  objectForKey:@"SECTION_NAME"];
    vc.btn_backTitle=NSLocalizedString(@"back", nil);
    
    [vc.btn_back setTitle:vc.btn_backTitle forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:vc.btn_back.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(vc.btn_back.frame.origin.x, vc.btn_back.frame.origin.y, vc.btn_back.frame.size.width, vc.btn_back.frame.size.height);
        
        [vc.btn_back setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        vc.btn_back.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    // for force handling
    if(index==0)
    {
        index=1;
    }
    vc.indexUsed=index-1;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
    
    [singleton traceEvents:@"More Button" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
    
    
}
#pragma mark - NSNotification to select table cell

- (void) didSelectItemFromCollectionView:(NSNotification *)notification
{
    NSDictionary *cellData = (NSDictionary*)[notification object];
    if (cellData)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didSelectItemFromCollectionView" object:nil];
        
        // [[NSNotificationCenter defaultCenter] removeObserver:observerObjectHere];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        
        HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        vc.dic_serviceInfo=cellData;
        vc.tagComeFrom=@"OTHERS";
        
        vc.sourceTab     = @"home";
        vc.sourceState   = @"";
        vc.sourceSection = [NSString stringWithFormat:@"%@",[cellData valueForKey:@"serviceCard"]];
        vc.sourceBanner  = @"";
        
        [self presentViewController:vc animated:NO completion:nil];
        
        [singleton traceEvents:@"Department Selected" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
    }
}



//-----------for filterview show----------
-(IBAction)btn_filterAction:(id)sender
{
    if (self.segmentControl.selectedSegmentIndex == 0) {
        [singleton traceEvents:@"Filter Button" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
        AddFilterHomeVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddFilterHomeVC"];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [singleton traceEvents:@"Filter Button" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
        AddFavFilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddFavFilterVC"];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        

    }
    
  
    
    
}





//-----------for notification view show---
-(IBAction)btn_noticationAction:(id)sender
{
    if (self.segmentControl.selectedSegmentIndex == 0)
    {
        [singleton traceEvents:@"Notification Button" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
    }
    else
    {
        [singleton traceEvents:@"Notification button" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
        
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
    
    ScrollNotificationVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ScrollNotificationVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:vc animated:NO completion:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
    
}


//----- Add notify view for profile completeness----------

/*
 - (IBAction)mapsDirections:(id)sender {
 NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",self.mapView.userLocation.coordinate.latitude, self.mapView.userLocation.coordinate.longitude, mapPoint.coordinate.latitude, mapPoint.coordinate.longitude];
 [[UIApplication sharedApplication] openURL: [NSURL URLWithString: directionsURL]];
 }*/



/*- (void)viewWillLayoutSubviews {
 // Your adjustments accd to
 // viewController.bounds
 
 
 CGRect rect;
 
 rect = [[UIApplication sharedApplication] statusBarFrame]; // Get status bar frame dimensions
 //NSLog(@"Statusbar frame: %1.0f, %1.0f, %1.0f, %1.0f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
 
 if (rect.size.height>=40) {
 
 
 //self.view.frame=CGRectMake(0, 0, fDeviceWidth, fDeviceHeight);
 self.view.autoresizesSubviews=UIViewAutoresizingNone;
 self.view.autoresizingMask=UIViewAutoresizingNone;
 
 }
 else
 {
 
 }
 
 
 // [self addNotify];
 
 [super viewWillLayoutSubviews];
 }
 
 */




- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    [tableView_home beginUpdates];
    [tableView_home endUpdates];
}



/*- (void)layoutSubviews
 {
 [super layoutSubviews];
 
 CGSize contentSize = self.contentSize;
 contentSize.width  = self.bounds.size.width;
 self.contentSize   = contentSize;
 }
 */



//------- Start delegate methods of update profile------------
/*
 -(void)addNotify
 {
 CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
 
 tableView_home.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-NV_height);
 // Allocate a reachability object
 for (UIView *subview in [self.view subviews])
 {
 if ([subview isKindOfClass:[profileBarVC class]])
 {
 [subview removeFromSuperview];
 }
 }
 
 
 
 viewFromNib = [[NSBundle mainBundle] loadNibNamed:@"profileBarVC" owner:nil options:nil].firstObject;
 
 
 viewFromNib.delegate=self;
 viewFromNib.frame=CGRectMake(0, fDeviceHeight-100, fDeviceWidth, 50);
 // code here
 //UIViewController *vc=[weakSelf topMostController];
 [self.view addSubview:viewFromNib];
 
 
 }
 
 
 -(void)closePressed
 {
 NSLog(@"home tabremove");
 CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
 
 tableView_home.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight);
 [viewFromNib removeFromSuperview];
 
 }
 
 
 -(void)updateProfile
 {
 NSLog(@"home clickUpdateAction");
 
 UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
 vc.hidesBottomBarWhenPushed = YES;
 
 [self.navigationController pushViewController:vc animated:YES];
 
 
 }
 */

//------- End of delegate methods of update profile------------








-(void)changeTableViewHeight {
    NSString* showProfilestatus =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"SHOW_PROFILEBAR"];
    self.bottomTableConstraint.constant = 0;

    if ([showProfilestatus isEqualToString:@"YES"])
    {
        self.bottomTableConstraint.constant = NV_height;
    }
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
    
}
-(void)addNotify
{
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    //tableView_home.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight);
    if (iPhoneX())
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        float yX= kiPhoneXNaviHeight4Search;
        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
        
        float heightX = heightVar-tabBarHeight ;
      //  tableView_home.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
        
    }
    NSString* showProfilestatus =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"SHOW_PROFILEBAR"];//[NSString stringWithFormat:@"%@", profileComplete];
    if (iPhoneX())
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        float yX= kiPhoneXNaviHeight4Search;
        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
        
        float heightX = heightVar-tabBarHeight-NV_height ;
        //   tableView_home.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
        
    }
    if ([showProfilestatus isEqualToString:@"YES"])
    {
        
        
        
        // tableView_home.frame=CGRectMake(0,69,377,fDeviceHeight-69-NV_height);
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        //tableView_home.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-NV_height);
        
        
        
        
        NSLog(@"tableView_home =%f height=%f",tableView_home.frame.size.width,tableView_home.frame.size.height);
        for (UIView *subview in [self.view subviews])
        {
            if (subview.tag == 7) {
                [subview removeFromSuperview];
            }
        }
        
        
        
        // UIEdgeInsets inset = UIEdgeInsetsMake(-20, 0,0, 0);
        // tableView_home.contentInset = inset;
        
        
        //----------- Add later can remove it----
        //CASE of iPHONE X
        if ([[UIScreen mainScreen]bounds].size.height == 812)
        {
            NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-tabBarHeight-NV_height,fDeviceWidth, NV_height)];
            
            if (flagStatusBar==TRUE)
            {
                NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
                
            }
        }
        else
        {
            NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-1.95*NV_height,fDeviceWidth, NV_height)];
            
            if (flagStatusBar==TRUE)
            {
                NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
                
            }
        }
        
        
        NotifycontainerView.backgroundColor = [UIColor clearColor];
        NotifycontainerView.tag=7;
        [self.view addSubview:NotifycontainerView];
        
        
        //-------- background image view--------------
        UIImageView *bg_img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,fDeviceWidth, NV_height)];
        bg_img.image=[UIImage imageNamed:@"img_popup_notify.png"];
        //bg_img.tag=7;
        [NotifycontainerView addSubview:bg_img];
        
        
        
        //  dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", NULL);
        //  dispatch_async(queue, ^{
        //code to be executed in the background
        
        
        //------- Profile image view-------
        UIImageView *user_img=[[UIImageView alloc]initWithFrame:CGRectMake(10,5,35,35)];
        
        UIImage *tempImg;
        if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_male", nil)]) {
            tempImg=[UIImage imageNamed:@"male_avatar"];
            
        }
        else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_female", nil)]) {
            tempImg=[UIImage imageNamed:@"female_avatar"];
            
        }
        else
        {
            tempImg=[UIImage imageNamed:@"user_placeholder"];
            
            
        }
        
        
        
        user_img.image=tempImg;
        
        //dispatch_async(dispatch_queue_create("com.getImage", NULL), ^(void) {
        //NSLog(@"singleton.imageLocalpath=%@",singleton.imageLocalpath);
        
        UIImage *tempImg1 ;
        
        if([[NSFileManager defaultManager] fileExistsAtPath:singleton.imageLocalpath])
        {
            // ur code here
            NSLog(@"file present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            
            tempImg1 = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
            if (tempImg1==nil) {
                
                tempImg1=tempImg;
                
            }
            user_img.image=tempImg1;
        } else {
            // ur code here**
            NSLog(@"Not present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            tempImg1=tempImg;
            
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        singleton.user_profile_URL=[defaults decryptedValueForKey:@"USER_PIC"];
        NSLog(@"user_profile_URL=%@",singleton.user_profile_URL);
        
        
        [defaults synchronize];
        
        NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
        
        if(![[url absoluteString] isEqualToString:@""])
        {
            
            
            NSURLRequest* request = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse * response,
                                                       NSData * data,
                                                       NSError * error) {
                                       if (!error){
                                           if(data == nil)
                                           {
                                               user_img.image = tempImg1;
                                               
                                           }
                                           else
                                           {
                                               UIImage* image = [[UIImage alloc] initWithData:data];
                                               //added later to resolve if server image path is invalid image
                                               if(image == nil)
                                               {
                                                   image = tempImg1;
                                                   
                                               }
                                               //== end of  image nil
                                               
                                               user_img.image = image;
                                           }
                                           // do whatever you want with image
                                       }
                                       else
                                       {
                                           user_img.image = tempImg1;
                                           
                                       }
                                       
                                   }];
            
        }
        else
        {
            user_img.image = tempImg1;
            
        }
        
        // });
        
        user_img.layer.cornerRadius = user_img.frame.size.height /2;
        user_img.layer.masksToBounds = YES;
        user_img.layer.borderWidth = 0.1;
        //user_img.tag=7;
        [NotifycontainerView addSubview:user_img];
        
        // img_blurProfile.image=[self blurredImageWithImage:img_profileView.image];//comment this code
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //code to be executed on the main thread when background task is finished
            //update UI with new database inserted means reload uicollectionview
            [tableView_home reloadData];
            
        });
        //  });
        
        
        
        
        
        
        //-------Label Complete text----------
        
        CGSize stringcompletesize = [NSLocalizedString(@"profile_completion", nil) sizeWithFont:[UIFont systemFontOfSize:12]];
        //or whatever font you're using
        
        
        
        
        
        
        
        
        lbl_profileComplete=[[UILabel alloc]init];
        
        [lbl_profileComplete setFrame:CGRectMake(51,6,stringcompletesize.width, stringcompletesize.height)];
        
        lbl_profileComplete.text= NSLocalizedString(@"profile_completion", nil);
        
        lbl_profileComplete.font=[UIFont systemFontOfSize:12];
        lbl_profileComplete.textColor=[UIColor whiteColor];
        lbl_profileComplete.adjustsFontSizeToFitWidth = YES;
        //lbl_profileComplete.tag=7;
        [NotifycontainerView addSubview:lbl_profileComplete];
        
        
        
        
        
        
        
        //-------Button Click Update ----------
        
        
        btn_clickUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
        
        // btn_clickUpdate.frame = CGRectMake(52,20,290,22);
        
        
        @try {
            btn_clickUpdate.titleLabel.adjustsFontSizeToFitWidth = YES;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        CGSize stringsize = [NSLocalizedString(@"update_profile_txt", nil) sizeWithFont:[UIFont systemFontOfSize:12]];
        //or whatever font you're using
        
        
        
        
        [btn_clickUpdate setFrame:CGRectMake(52,25,stringsize.width, stringsize.height)];
        
        
        [btn_clickUpdate addTarget:self
                            action:@selector(clickUpdateAction:)
                  forControlEvents:UIControlEventTouchUpInside];
        [btn_clickUpdate setTitle:NSLocalizedString(@"update_profile_txt", nil) forState:UIControlStateNormal];
        
        [btn_clickUpdate setTitle:NSLocalizedString(@"update_profile_txt", nil) forState:UIControlStateSelected];
        btn_clickUpdate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        btn_clickUpdate.titleLabel.font=[UIFont systemFontOfSize:12];
        // btn_clickUpdate.tag=7;
        
        [NotifycontainerView addSubview:btn_clickUpdate];
        
        
        //---- Show Custom Progress Profile View--------
        
        
        
        UIProgressView *progressView;
        progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        progressView.progressTintColor =[UIColor colorWithRed:12.0/255.0 green:118.0/255.0 blue:157.0/255.0 alpha:1.0];
        [[progressView layer]setFrame:CGRectMake(lbl_profileComplete.frame.origin.x+lbl_profileComplete.frame.size.width+5,9,fDeviceWidth-188-50,10)];
        progressView.trackTintColor = [UIColor whiteColor];
        [[progressView layer]setCornerRadius:2];
        progressView.layer.masksToBounds = TRUE;
        progressView.clipsToBounds = TRUE;
        
        
        NSString* str =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"PROFILE_COMPELTE_KEY"];//[NSString stringWithFormat:@"%@", profileComplete];
        //str=@"90";
        if (str == nil)
        {
            str=@"0";
        }
        if ([str length]==0)
        {
            str=@"0";
            
        }
        
        CGFloat value = [str floatValue]/100;
        lbl_percentage.text=  [NSString stringWithFormat:@"%@%%",str];
        // self.progress=value;
        [progressView setProgress:value];  ///15
        
        
        
        [NotifycontainerView addSubview:progressView];
        
        
        
        
        
        //-------Label Percentage text----------
        lbl_percentage=[[UILabel alloc]initWithFrame:CGRectMake(progressView.frame.origin.x+progressView.frame.size.width+5,2,30,22)];
        
        
        
        if ([str intValue]>=70)
            
        {
            
            [self closeBtnAction:self];
        }
        
        
        
        lbl_percentage.text=[NSString stringWithFormat:@"%@%%",str];
        lbl_percentage.font=[UIFont boldSystemFontOfSize:11];
        lbl_percentage.textColor=[UIColor whiteColor];
        @try {
            lbl_percentage.adjustsFontSizeToFitWidth = YES;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        //lbl_percentage.tag=7;
        [NotifycontainerView addSubview:lbl_percentage];
        
        
        //-------Button Close ----------
        
        btn_close = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn_close.frame = CGRectMake(fDeviceWidth-30,NV_height/2-8, 22, 22);
        
        [btn_close addTarget:self
                      action:@selector(closeBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        UIImage *buttonImagePressed = [UIImage imageNamed:@"btn_close_notify.png"];
        [btn_close setImage:buttonImagePressed forState:UIControlStateNormal];
        [btn_close setImage:buttonImagePressed forState:UIControlStateSelected];
        
        btn_close.tag=7;
        
        [NotifycontainerView addSubview:btn_close];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [tableView_home reloadData];
        });
        // [tableView_home reloadData];
    }
    else
    {
        [self closeBtnAction:self];
    }
    [self changeTableViewHeight];
    [self checkAppUpdate];
}
-(void)checkAppUpdate {
     NSString *newVersion = [NSString stringWithFormat:@"%@",[[SharedManager sharedSingleton].arr_initResponse valueForKey:@"nver"]];
    NSString *showVersion = [[NSUserDefaults standardUserDefaults] valueForKey:APPUPDATE_SHOWN_VERSION];
   // BOOL isRemoved = [[NSUserDefaults standardUserDefaults] boolForKey:APPUPDATE_IS_REMOVED];
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    for (UIView *subview in [self.view subviews])
    {
        if (subview.tag == 7878) {
            [subview removeFromSuperview];
        }
    }
    BOOL isShown = false;
    if ([newVersion integerValue] > [showVersion integerValue] ) {
        AppUpdateView *updateView = nil ;
        if ([[UIScreen mainScreen]bounds].size.height == 812)
        {
            updateView  = [[AppUpdateView alloc] initWithFrame:CGRectMake(0, fDeviceHeight-tabBarHeight-NV_height,fDeviceWidth, NV_height)];
        }
        else
        {
            updateView  = [[AppUpdateView alloc] initWithFrame:CGRectMake(0, fDeviceHeight-1.95*NV_height,fDeviceWidth, NV_height)];
            
        }
        if (flagStatusBar==TRUE)
        {
            updateView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
        }
        updateView.tag = 7878;
        __block typeof(self) weakSelf = self;
        updateView.didRemoveApp = ^{
            [weakSelf changeTableViewHeight];
        };
        [self.view addSubview:updateView];
        [self.view bringSubviewToFront:updateView];
        isShown = true;
    }
    
    NSString* showProfilestatus =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"SHOW_PROFILEBAR"];
    self.bottomTableConstraint.constant = 0;
    
    if ([showProfilestatus isEqualToString:@"YES"] || isShown)
    {
        self.bottomTableConstraint.constant = NV_height;
    }
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
}
- (void)closeBtnAction:(id)sender
{
    
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
   // tableView_home.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight);
    if (iPhoneX())
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        float yX= kiPhoneXNaviHeight4Search;
        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search
        
        float heightX = heightVar-tabBarHeight ;
       // tableView_home.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
        
    }
    
    //[tableView_home reloadData];
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"SHOW_PROFILEBAR"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self changeTableViewHeight];
    [NotifycontainerView removeFromSuperview];
    
    [singleton traceEvents:@"Hide Profile Bar" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
}





- (void)updateProgress
{
    // NSString* str = [NSString stringWithFormat:@"%@", profileComplete];
    NSString* str =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"PROFILE_COMPELTE_KEY"];//[NSString stringWithFormat:@"%@", profileComplete];
    
    if (str == nil)
    {
        str=@"0";
    }
    if ([str length]==0)
    {
        str=@"0";
        
    }
    
    CGFloat value = [str floatValue]/100;
    lbl_percentage.text=  [NSString stringWithFormat:@"%@%%",str];
    self.progress=value;
    
    
    
    [self.progressViews enumerateObjectsUsingBlock:^(THProgressView *progressView, NSUInteger idx, BOOL *stop) {
        [progressView setProgress:self.progress animated:YES];
        
    }];
    
}



-(void)clickUpdateAction:(id)sender
{
   
    // Reset here for login with registration here
    
    
   [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLoginWithRegistration"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    
    SharedManager *singleton  = [SharedManager sharedSingleton];
    singleton.shared_mpinflag = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinflag"];
    singleton.shared_mpinmand = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinmand"];
   
    if ([[singleton.shared_mpinflag lowercaseString] isEqualToString:@"false"])
    {
        
        NSLog(@"MPIN SHOWING STATUS IS FALSE, so  show it");
         AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        dispatch_async(dispatch_get_main_queue(),^{
           
            [delegate showMPIN_AlertBox];
            [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
            delegate.vw_MPIN_AlertBox.lbl_subtitle1.text = NSLocalizedString(@"setting_mpin_mandatory_this_screen", nil);
        });
       
    }
    else
    {
        // do nothing
        NSLog(@"MPIN SHOWING STATUS IS TRUE, so dont show it");
        
        [singleton traceEvents:@"Profile Bar Profile Update" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
        
        ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
        vc.hidesBottomBarWhenPushed = YES;
        
        
        [self.navigationController pushViewController:vc animated:YES];
    }

   
    
   

}

//----- End----------

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
 - (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
 [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
 }];
 return [super canPerformAction:action withSender:sender];
 }
 */

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == txt_searchField)
    {
        [self openSearch];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)openSearch
{
    
    
    // SearchViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    //  [self presentViewController:vc animated:YES completion:nil];
    
    [singleton traceEvents:@"Search Bar Clicked" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    AdvanceSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AdvanceSearchVC"];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:vc animated:NO completion:nil];
    
    
    
}


-(void)heroSpaceAPIAfterDelay
{
    [self hitFetchHerospaceAPI];
}


-(void)showHomeDialogueBox
{
    
    NSString *dialogueIDStr = [NSString stringWithFormat:@"%@",[homeDialogueDict valueForKey:@"id"]];
    
    if (![dialogueIDStr isEqualToString:@"(null)"])
    {
        
        
        if (![dialogueIDStr isEqualToString:@""])
        {
            if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"dialogueID"] isEqualToString:dialogueIDStr])
            {
                
                if (homeDialog != nil)
                {
                    [homeDialog removeFromParentViewController];
                    homeDialog = nil;
                }
                
                homeDialog = [[HomeDialogBox alloc] initWithNibName:@"HomeDialogBox" bundle:nil];
                [homeDialog removeFromParentViewController];
                
                homeDialog.dialogueDict = [NSMutableDictionary new];
                
                homeDialog.dialogueDict = homeDialogueDict;
                
                [homeDialog.view setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
                
                [self addChildViewController:homeDialog];
                [self.view addSubview:homeDialog.view];
                //[homeDialog didMoveToParentViewController:self];
                
                
                [self.view bringSubviewToFront:homeDialog.view];
                
                [[NSUserDefaults standardUserDefaults] setValue:dialogueIDStr forKey:@"dialogueID"];;
            }
        }
        
        
    }
    
    
    
    
    
}

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitHomeAPI
{
    
    NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
    
    [activityIndicator startAnimating];
    
    
    
    @try
    {
        if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""])
        {
            lastTime=@"";
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
    
    
    NSString *lastV1Home = [[NSUserDefaults standardUserDefaults] valueForKey:@"lastFetchV1"];
    
    if ([lastV1Home length]==0||lastV1Home==nil||[lastV1Home isEqualToString:@""])
    {
        lastTime=@"";
    }
    else
    {
        lastTime = lastV1Home;
    }
    
    
    
    singleton.lastFetchDate=lastTime;
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.lastFetchDate forKey:@"ldate"]; //blank first time then it will return a date save it in Shared manager and hit using it//singleton.lastFetchDate
    [dictBody setObject:@"" forKey:@"st"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    NSLog(@"TOKEN :::: %@",singleton.user_tkn);
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_HOME_SCREEN_DATA withBody:dictBody andTag:TAG_REQUEST_HOME_SCREEN_DATA completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         // [hud hideAnimated:YES];
         
         [activityIndicator stopAnimating];
         
         [RunOnMainThread runBlockInMainQueueIfNecessary:^{
             
             [self performSelector:@selector(heroSpaceAPIAfterDelay) withObject:nil afterDelay:0.5];
             
         }];
         
         if ([lastV1Home length]==0||lastV1Home==nil||[lastV1Home isEqualToString:@""])
         {
             if (error == nil)
             {
                 
                 [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                     
                     @try
                     {
                         if ([response isKindOfClass:[NSNull class]] )
                         {
                             //do something
                             
                             //NSLog(@"Null response");
                             [self fetchDatafromDB];
                         }
                         
                         NSString *str_response=[NSString stringWithFormat:@"%@",response];
                         
                         //NSLog(@"str_response=%@",str_response);
                         if (str_response==nil)
                         {
                             
                             [self fetchDatafromDB];
                             
                         }
                         //NSLog(@"Null response");
                         
                     } @catch (NSException *exception) {
                         
                     } @finally {
                         
                     }
                     
                 }];
                 
                 
                 if ([[response valueForKey:@"pd"] valueForKey:@"objDialog"])
                 {
                     NSLog(@"Show Dialog");
                     
                     homeDialogueDict = [[response valueForKey:@"pd"] valueForKey:@"objDialog"];
                     BOOL isHintShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"HintShown"];
                     
                     // NSLog(isHintShown ? @"Yes" : @"No");
                     
                     
                     if (isHintShown)
                     {
                         if (!hintShown)
                         {
                             [self showHomeDialogueBox];
                         }
                     }
                     
                 }
                 
                 BOOL serviceDirectory = [[[response valueForKey:@"pd"] valueForKey:@"dirsershow"] boolValue];
                 
                 [[NSUserDefaults standardUserDefaults] setBool:serviceDirectory forKey:@"Enable_ServiceDir"];
                 
                 if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                 {
                     
                     
                     [singleton.dbManager deleteAllServices];
                     
                     //[[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"] forKey:@"lastFetchV1"];
                     
                     
                     dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
                     dispatch_async(serialQueue, ^{
                         [self insertToDBTask:response]; //insert into DB
                     });
                 }
                 
                 
             }
             else
             {
                 [self hitRetryMethod];
             }
             
         }
         else
         {
             
             if (error == nil)
             {
                 
                 
                 [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                     
                     @try
                     {
                         if ([response isKindOfClass:[NSNull class]] )
                         {
                             //do something
                             
                             //NSLog(@"Null response");
                             [self fetchDatafromDB];
                         }
                         
                         NSString *str_response=[NSString stringWithFormat:@"%@",response];
                         
                         //NSLog(@"str_response=%@",str_response);
                         if (str_response==nil)
                         {
                             
                             [self fetchDatafromDB];
                             
                         }
                         //NSLog(@"Null response");
                         
                     } @catch (NSException *exception) {
                         
                     } @finally {
                         
                     }
                     
                 }];
                 
                 
                 if ([[response valueForKey:@"pd"] valueForKey:@"objDialog"])
                 {
                     NSLog(@"Show Dialog");
                     
                     homeDialogueDict = [[response valueForKey:@"pd"] valueForKey:@"objDialog"];
                     BOOL isHintShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"HintShown"];
                     
                     // NSLog(isHintShown ? @"Yes" : @"No");
                     
                     
                     if (isHintShown)
                     {
                         if (!hintShown)
                         {
                             [self showHomeDialogueBox];
                         }
                     }
                     
                 }
                 
                 BOOL serviceDirectory = [[[response valueForKey:@"pd"] valueForKey:@"dirsershow"] boolValue];
                 
                 [[NSUserDefaults standardUserDefaults] setBool:serviceDirectory forKey:@"Enable_ServiceDir"];
                 
                 NSString *forceUpdate=[[response valueForKey:@"pd"] valueForKey:@"forceUpdate"];
                 //NSLog(@"forceUpdate=%@",forceUpdate);
                 
                 
                 
                 if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                 {
                     //[[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"] forKey:@"lastFetchV1"];
                     
                     if ([[forceUpdate uppercaseString] isEqualToString:@"Y"])
                     {
                         
                         [singleton.dbManager deleteAllServices]; //delete all service data
                     }
                     
                     
                     dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
                     dispatch_async(serialQueue, ^{
                         [self insertToDBTask:response]; //insert into DB
                     });
                     
                     //  [self insertToDBTask:response]; //insert into DB
                     
                 }
                 
             }
             else
             {
                 /* //NSLog(@"Error Occured = %@",error.localizedDescription);
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                  message:error.localizedDescription
                  delegate:self
                  cancelButtonTitle:@"OK"
                  otherButtonTitles:nil];
                  [alert show];*/
                 [self hitRetryMethod];
             }
         }
         
         
         //[hud hideAnimated:YES];
         
         
     }];
    
}

-(void)hitRetryMethod
{
    vw_RetryOption.hidden=TRUE;
    
    [self fetchDatafromDB]; //in case of fail api
    
    [self fetchHeroSpaceDatafromDB];
    
}

//----------------------------------------------
//
// Code to update popularity of service
//----------------------------------------------

-(void)UpdateMostPopularToDBTask:(NSMutableArray*)mostPopularList
{
    
    @try {
        
        
        for (int i=0; i<[mostPopularList count]; i++)
        {
            NSString* serviceId =[[mostPopularList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* viewcnt =[[mostPopularList objectAtIndex:i]valueForKey:@"viewcnt"];
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId=@"";
            }
            //code to be executed in the background
            [singleton.dbManager updateServiceMostPopular:serviceId withRating:viewcnt];
        }
        
    }
    
    
    @catch (NSException *exception)
    {
        [[Crashlytics sharedInstance] recordCustomExceptionName:@"HandledException" reason:@"Some reason" frameArray:@[exception]];
        
    }
    @finally
    {
        
    }
}

-(void)insertToDBTask:(NSDictionary*)response
{
    //singleton.dbManager  = [[DBManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    
    @try {
        //  [singleton.dbManager open];
        NSMutableArray *mostPopularList=[[NSMutableArray alloc]init];
        mostPopularList=[[[response valueForKey:@"pd"] valueForKey:@"mostPopularList"] mutableCopy];
        
        
        
        
        //------- Code to insert data in database with service list-------
        NSMutableArray *addServiceList=[[NSMutableArray alloc]init];
        addServiceList=[[[response valueForKey:@"pd"] valueForKey:@"addServiceList"] mutableCopy];
        
        NSLog(@"response =%@",response);
        
        if([addServiceList count]<13)
        {
            NSLog(@"break down Response of incomplete Array =%@",response);
        }
        
        //  [singleton.dbManager close];
        
        //----------- Update service list--------
        
        NSMutableArray *updateServiceList=[[NSMutableArray alloc]init];
        updateServiceList=[[[response valueForKey:@"pd"] valueForKey:@"updateServiceList"] mutableCopy];
        
        //  [singleton.dbManager open];
        //  [singleton.dbManager close];
        
        //----------- deleteServiceList--------
        
        NSMutableArray *deleteServiceList=[[NSMutableArray alloc]init];
        deleteServiceList=[[[response valueForKey:@"pd"] valueForKey:@"deleteServiceList"] mutableCopy];
        //  [singleton.dbManager open];
        //  [singleton.dbManager close];
        //----------- favouriteServiceList--------
        
        
        NSMutableArray *favouriteServiceList=[[NSMutableArray alloc]init];
        favouriteServiceList=[[[response valueForKey:@"pd"] valueForKey:@"favouriteServiceList"] mutableCopy];
        
        //NSLog(@"favouriteServiceList===>%@",favouriteServiceList);
        //[singleton.dbManager open];
        //[singleton.dbManager close];
        //[singleton.dbManager open];
        // ----------- serviceCardList--------
        NSMutableArray *serviceCardList=[[NSMutableArray alloc]init];
        serviceCardList=[[[response valueForKey:@"pd"] valueForKey:@"serviceCardList"] mutableCopy];
        
        //NSLog(@"deleteSectionData");
        
        //[singleton.dbManager close];
        
        
        singleton.shareText=[[response valueForKey:@"pd"] valueForKey:@"shareText"];
        //NSLog(@"shareText=%@",singleton.shareText);
        
        
        singleton.phoneSupport=[[response valueForKey:@"pd"] valueForKey:@"phoneSupport"];
        //NSLog(@"shareText=%@",singleton.phoneSupport);
        
        
        singleton.emailSupport=[[response valueForKey:@"pd"] valueForKey:@"emailSupport"];
        //NSLog(@"emailSupport=%@",singleton.emailSupport);
        
       // recflag save start
        NSString *recflag  = [[response valueForKey:@"pd"]  valueForKey:@"recflag"];
        [[NSUserDefaults standardUserDefaults] setValue:recflag forKey:@"recflag"];
        
        // recflag save end

        NSString *MpinDial  = [[response valueForKey:@"pd"]  valueForKey:@"mpindial"];
        NSString * mpinflag = [[response valueForKey:@"pd"]  valueForKey:@"mpinflag"];
        NSString * mpinmand = [[response valueForKey:@"pd"]  valueForKey:@"mpinmand"];
        
        // over ride preferece value of mpinflag
        singleton.shared_mpinflag =mpinflag;
        [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinflag forKey:@"mpinflag"];

        
        
        SharedManager *singleton  = [SharedManager sharedSingleton];
        singleton.shared_mpinflag = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinflag"];
        singleton.shared_mpinmand = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinmand"];
        
       NSString * mpindialpref = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpindial"];

        if ([[singleton.shared_mpinflag lowercaseString] isEqualToString:@"false"])
        
       // if ([[mpinflag lowercaseString] isEqualToString:@"false"])
        {
            // do nothing
            if ([[singleton.shared_mpinmand lowercaseString] isEqualToString:@"true"] &&  [[mpindialpref lowercaseString] isEqualToString:@"true"])
           // if ([[MpinDial lowercaseString] isEqualToString:@"true"] &&  [[mpinmand lowercaseString] isEqualToString:@"true"])
            {
                // dont override preference value
            }
            else
            {
                // over ride preference value
                singleton.shared_mpinmand =mpinmand;
                NSString *mpindial =MpinDial;
                [[NSUserDefaults standardUserDefaults] setValue:mpindial forKey:@"mpindial"];
                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinmand forKey:@"mpinmand"];
            }

        }
        else
        {
           
         // do nothing
            
        }
        //-------- Add later For handling mpinflag / mpinmand ----------
       
        
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
        
     

        
        //-------- Add later For handling mpinflag / mpinmand ----------

        
        
        
        
        
        
        
        
        /*
         forceUpdate = N;
         lastFetchDate = "2016-11-23 14:26:12";
         phoneSupport = 9915333365;
         serviceCardList
         cardName = trending;
         des = "most rated";
         ldate = "2016-11-22 10:40:35.899963";
         link = "";
         priority = 0;
         serviceCard = 1;
         url = cdjschsd;
         */
        
        singleton.lastFetchDate=[NSString stringWithFormat:@"%@",[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"]];
        
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.lastFetchDate withKey:@"lastFetchDate"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //------------------------- Encrypt Value------------------------
        
        //---------------------------------------------------------------------------
        //---------------------------------------------------------------------------
        //---------------------------------------------------------------------------
        
        
        // Save your stuff
        /*     NSOperationQueue *queue = [[NSOperationQueue alloc] init];
         queue.maxConcurrentOperationCount = 1;
         
         [queue addOperationWithBlock:^{
         NSLog(@"===>block 1");
         
         if(addServiceList && [addServiceList count]>=1)
         {
         [self addServiceToDBTask:addServiceList]; //insert
         
         }
         
         
         }];
         
         [queue addOperationWithBlock:^{
         
         NSLog(@"===>block 2");
         if(updateServiceList && [updateServiceList count]>=1)
         {
         [self updateServiceToDBTask:updateServiceList]; //update
         
         }
         
         }];
         
         [queue addOperationWithBlock:^{
         NSLog(@"===>block 3");
         if(deleteServiceList && [deleteServiceList count]>=1)
         {
         [self deleteServiceToDBTask:deleteServiceList];
         
         }
         
         
         
         
         
         }];
         [queue addOperationWithBlock:^{
         NSLog(@"===>block 4");
         
         
         if(favouriteServiceList && [favouriteServiceList count]>=1)
         {
         [self favServiceToDBTask:favouriteServiceList];
         
         }
         
         
         
         }];
         
         [queue addOperationWithBlock:^{
         NSLog(@"===>block 5");
         
         [singleton.dbManager deleteSectionData];// check
         
         
         
         }];
         
         [queue addOperationWithBlock:^{
         NSLog(@"===>block 6");
         
         
         if(serviceCardList && [serviceCardList count]>=1)
         {
         [self ServiceCardToDBTask:serviceCardList];
         
         }
         
         
         
         }];
         
         [queue addOperationWithBlock:^{
         //NSLog(@"log->3");
         NSLog(@"===>block 7");
         
         
         [self fetchDatafromDB];
         
         
         }];
         
         
         
         */
        
        
        
        
        
        // ========= check to open show mpin alert box=====
     
        // ========= check to open show mpin alert box=====
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        __block typeof(self) weakSelf = self;
        delegate.didCloseMPIN = ^{
            [weakSelf checkHintScreen];
        };
        dispatch_async(dispatch_get_main_queue(),^{
            //[delegate showHideSetMpinAlertBox ];
            if(([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"T"])||([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"TRUE"]))
            {
                if(!([[recflag uppercaseString] isEqualToString:@"T"]||[[recflag uppercaseString] isEqualToString:@"TRUE"]))
                {
                    [delegate checkRecoveryOptionBOXShownStatus:@""];
                }
            }
            else
            {
                
                [delegate showHideSetMpinAlertBox ];
            }
            // check every time case App is opening
            [delegate checkRecoveryOptionBOXShownStatus:@""];
        });
        

        
        
        dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
        dispatch_async(serialQueue, ^{
            if(addServiceList && [addServiceList count]>=1)
            {
                NSLog(@"addServiceToDBTask 1");
                [self addServiceToDBTask:addServiceList]; //insert
                
            }
            
            dispatch_async(serialQueue, ^{
                if(updateServiceList && [updateServiceList count]>=1)
                {
                    NSLog(@"updateServiceToDBTask 2");
                    [self updateServiceToDBTask:updateServiceList]; //update
                    
                }                dispatch_async(serialQueue, ^{
                    if(deleteServiceList && [deleteServiceList count]>=1)
                    {
                        NSLog(@"deleteServiceToDBTask 3");
                        [self deleteServiceToDBTask:deleteServiceList];
                        
                    }
                    
                    dispatch_async(serialQueue, ^{
                        if(favouriteServiceList && [favouriteServiceList count]>=1)
                        {
                            NSLog(@"favServiceToDBTask 4");
                            [self favServiceToDBTask:favouriteServiceList];
                            
                        }
                        dispatch_async(serialQueue, ^{
                            if(serviceCardList && [serviceCardList count]>=1)
                            {
                                NSLog(@"deleteSectionData 5");
                                NSLog(@"==============>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>==================Delete section if serviceCardlist is not empty=====>");
                                [singleton.dbManager deleteSectionData];// check
                            }
                            dispatch_async(serialQueue, ^{
                                if(serviceCardList && [serviceCardList count]>=1)
                                {
                                    NSLog(@"ServiceCardToDBTask 6");
                                    [self ServiceCardToDBTask:serviceCardList];
                                    
                                }
                                
                                dispatch_async(serialQueue, ^{
                                    
                                    if(mostPopularList && [mostPopularList count]>=1)
                                    {
                                        NSLog(@"UpdateMostPopularToDBTask 7");
                                        [self UpdateMostPopularToDBTask:mostPopularList];
                                        
                                    }
                                    
                                    
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        //code to be executed on the main thread when background task is finished
                                        // Check and delete in case of dublicate entry found
                                        [singleton.dbManager deleteDublicateValueFromServiceData];
                                        
                                        NSLog(@"fetchDatafromDB 8");
                                        
                                        [self fetchDatafromDB];// used to get data from database
                                        
                                        [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"] forKey:@"lastFetchV1"];
                                        
                                        // [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCHHOMEDATA" object:nil];
                                        //[[NSNotificationCenter defaultCenter]  postNotificationName:@"FETCHHOMEDATA" object:nil];
                                        
                                        
                                    });
                                    
                                });
                                
                            });
                        });
                    });
                });
            });
        });
        
        
        
        
        
    }
    
    
    @catch (NSException *exception)
    {
        [[Crashlytics sharedInstance] recordCustomExceptionName:@"HandledException" reason:@"Some reason" frameArray:@[exception]];
        
    }
    @finally
    {
        
    }
    
}

-(void)addServiceToDBTask:(NSMutableArray*)addServiceList
{
    
    /*  NSLog(@"addServiceList=%@",addServiceList);
     
     NSString *totalServiceInsert=[NSString stringWithFormat:@"%lu",(unsigned long)[addServiceList count]];
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
     message:totalServiceInsert
     delegate:self
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];
     
     
     */
    for (int i=0; i<[addServiceList count]; i++)
    {
        
        
        
        
        @try {
            NSString* serviceId =[[addServiceList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* serviceName =[[addServiceList objectAtIndex:i]valueForKey:@"serviceName"];
            NSString* description =[[addServiceList objectAtIndex:i]valueForKey:@"description"];
            NSString* image=[[addServiceList objectAtIndex:i]valueForKey:@"image"];
            NSString* categoryName =[[addServiceList objectAtIndex:i]valueForKey:@"categoryName"];
            NSString* subCategoryName =[[addServiceList objectAtIndex:i]valueForKey:@"subCategoryName"];
            NSString* rating=[[addServiceList objectAtIndex:i]valueForKey:@"rating"];
            NSString* url =[[addServiceList objectAtIndex:i]valueForKey:@"url"];
            NSString* state=[[addServiceList objectAtIndex:i]valueForKey:@"state"];
            NSString* lat =[[addServiceList objectAtIndex:i]valueForKey:@"lat"];
            NSString* log=[[addServiceList objectAtIndex:i]valueForKey:@"log"];
            NSString* serviceIsFav =@"false";
            NSString* serviceIsHidden =@"false";
            NSString* contact =[[addServiceList objectAtIndex:i]valueForKey:@"contact"];
            NSString* serviceisNotifEnabled =@"true";
            NSString* website =[[addServiceList objectAtIndex:i]valueForKey:@"website"];
            
            //------- NEW fields added to be shown--------------
            NSString* deptAddress =[[addServiceList objectAtIndex:i]valueForKey:@"deptAddress"];
            NSString* workingHours =[[addServiceList objectAtIndex:i]valueForKey:@"workingHours"];
            NSString* deptDescription =[[addServiceList objectAtIndex:i]valueForKey:@"deptDescription"];
            NSString* lang =[[addServiceList objectAtIndex:i]valueForKey:@"lang"];
            NSString* email =[[addServiceList objectAtIndex:i]valueForKey:@"email"];
            NSString* popularity =[[addServiceList objectAtIndex:i]valueForKey:@"popularity"];
            NSString* servicecategoryId =[[addServiceList objectAtIndex:i]valueForKey:@"categoryId"];
            
            // otherState = "20|28|31";
            
            NSString* serviceOtherState =[[addServiceList objectAtIndex:i]valueForKey:@"otherState"];
            
            if ([serviceOtherState length]!=0)
            {
                serviceOtherState=[NSString stringWithFormat:@"|%@|",serviceOtherState];
            }
            
            NSLog(@"Insert Other State=%@",serviceOtherState);
            
            if (serviceOtherState == (NSString *)[NSNull null]||[serviceOtherState length]==0)
            {
                serviceOtherState=@"";
            }
            
            
            
            //NSLog(@"insertServicesData");
            //code to be executed in the background
            
            
            
            
            if (deptAddress == (NSString *)[NSNull null]||[deptAddress length]==0) {
                deptAddress=@"";
            }
            if (workingHours == (NSString *)[NSNull null]||[workingHours length]==0) {
                workingHours=@"";
            }
            if (deptDescription == (NSString *)[NSNull null]||[deptDescription length]==0) {
                deptDescription=@"";
            }
            if (lang == (NSString *)[NSNull null]||[lang length]==0) {
                lang=@"";
            }
            if (email == (NSString *)[NSNull null]||[email length]==0) {
                email=@"";
            }
            
            //-------  fields added to be shown--------------
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId=@"";
            }
            
            if (serviceName == (NSString *)[NSNull null]||[serviceName length]==0) {
                serviceName=@"";
            }
            if (description == (NSString *)[NSNull null]||[description length]==0) {
                description=@"";
            }
            if (image == (NSString *)[NSNull null]||[image length]==0) {
                image=@"";
            }
            if (categoryName == (NSString *)[NSNull null]||[categoryName length]==0) {
                categoryName=@"";
            }
            if (subCategoryName == (NSString *)[NSNull null]||[subCategoryName length]==0) {
                subCategoryName=@"";
            }
            if (rating == (NSString *)[NSNull null]||[rating length]==0) {
                rating=@"";
            }
            if (url == (NSString *)[NSNull null]||[url length]==0) {
                url=@"";
            }
            if (state == (NSString *)[NSNull null]||[state length]==0) {
                state=@"";
            }
            if (lat == (NSString *)[NSNull null]||[lat length]==0) {
                lat=@"";
            }
            if (log == (NSString *)[NSNull null]||[log length]==0) {
                log=@"";
            }
            if (serviceIsFav == (NSString *)[NSNull null]||[serviceIsFav length]==0) {
                serviceIsFav=@"";
            }
            if (serviceIsHidden == (NSString *)[NSNull null]||[serviceIsHidden length]==0) {
                serviceIsHidden=@"";
            }
            if (contact == (NSString *)[NSNull null]||[contact length]==0) {
                contact=@"";
            }
            if (serviceisNotifEnabled == (NSString *)[NSNull null]||[serviceisNotifEnabled length]==0) {
                serviceisNotifEnabled=@"";
            }
            
            if (website == (NSString *)[NSNull null]||[website length]==0) {
                website=@"";
            }
            
            
            
            
            if (servicecategoryId == (NSString *)[NSNull null]||[servicecategoryId length]==0) {
                servicecategoryId=@"";
            }
            
            
            
            NSString* otherwebsite =[[addServiceList objectAtIndex:i]valueForKey:@"otherwebsite"];
            
            
            if (otherwebsite == (NSString *)[NSNull null]||[otherwebsite length]==0) {
                otherwebsite=@"";
            }
            
            NSLog(@"otherwebsite=%@",otherwebsite);
            
            /*
             [singleton.dbManager insertServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:lang servicedeptAddress:deptAddress
             serviceworkingHours:workingHours servicedeptDescription:deptDescription  serviceemail:email popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState];
             
             */
            /*    [singleton.dbManager insertServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:lang servicedeptAddress:deptAddress
             serviceworkingHours:workingHours servicedeptDescription:deptDescription  serviceemail:email popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState otherwebsite:otherwebsite];
             
             */
            
            NSString* depttype =[[addServiceList objectAtIndex:i]valueForKey:@"depttype"];
            
            
            if (depttype == (NSString *)[NSNull null]||[depttype length]==0) {
                depttype=@"";
            }
            
            NSString* disname =[[addServiceList objectAtIndex:i]valueForKey:@"disname"];
            
            
            if (disname == (NSString *)[NSNull null]||[disname length]==0) {
                disname=@"";
            }
            
            
            NSString* multicatid =[[addServiceList objectAtIndex:i]valueForKey:@"multicatid"];
            if (multicatid == (NSString *)[NSNull null]||[multicatid length]==0) {
                multicatid=@"";
            }
            
            NSString* multicatname =[[addServiceList objectAtIndex:i]valueForKey:@"multicatname"];
            if (multicatname == (NSString *)[NSNull null]||[multicatname length]==0) {
                multicatname=@"";
            }
            
            
            [singleton.dbManager insertServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:lang servicedeptAddress:deptAddress
                                serviceworkingHours:workingHours servicedeptDescription:deptDescription  serviceemail:email popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState otherwebsite:otherwebsite depttype:depttype disname:disname multicatid:multicatid multicatname:multicatname];
            
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        //Delete from TABLE_SERVICES_DATA  where id in ( select max(id) from TABLE_SERVICES_DATA  group by SERVICE_ID   having count(*) > 1 );

        
        
    }
    
}

//---- [singleton.dbManager open];
-(void)updateServiceToDBTask:(NSMutableArray*)updateServiceList
{
    
    for (int i=0; i<[updateServiceList count]; i++)
    {
        @try {
            
            
            NSString* serviceId =[[updateServiceList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* serviceName =[[updateServiceList objectAtIndex:i]valueForKey:@"serviceName"];
            NSString* description =[[updateServiceList objectAtIndex:i]valueForKey:@"description"];
            NSString* image=[[updateServiceList objectAtIndex:i]valueForKey:@"image"];
            NSString* categoryName =[[updateServiceList objectAtIndex:i]valueForKey:@"categoryName"];
            NSString* subCategoryName =[[updateServiceList objectAtIndex:i]valueForKey:@"subCategoryName"];
            NSString* rating=[[updateServiceList objectAtIndex:i]valueForKey:@"rating"];
            NSString* url =[[updateServiceList objectAtIndex:i]valueForKey:@"url"];
            NSString* state=[[updateServiceList objectAtIndex:i]valueForKey:@"state"];
            NSString* lat =[[updateServiceList objectAtIndex:i]valueForKey:@"lat"];
            NSString* log=[[updateServiceList objectAtIndex:i]valueForKey:@"log"];
            NSString* serviceIsFav =@"false";
            NSString* serviceIsHidden =@"false";
            NSString* contact =[[updateServiceList objectAtIndex:i]valueForKey:@"contact"];
            NSString* serviceisNotifEnabled =@"true";
            NSString* website =[[updateServiceList objectAtIndex:i]valueForKey:@"website"];
            
            //------- NEW fields added to be shown--------------
            NSString* servicedeptAddress =[[updateServiceList objectAtIndex:i]valueForKey:@"deptAddress"];
            NSString* serviceworkingHours =[[updateServiceList objectAtIndex:i]valueForKey:@"workingHours"];
            NSString* servicedeptDescription =[[updateServiceList objectAtIndex:i]valueForKey:@"deptDescription"];
            NSString* servicelang =[[updateServiceList objectAtIndex:i]valueForKey:@"lang"];
            NSString* serviceemail =[[updateServiceList objectAtIndex:i]valueForKey:@"email"];
            NSString* popularity =[[updateServiceList objectAtIndex:i]valueForKey:@"popularity"];
            NSString* servicecategoryId =[[updateServiceList objectAtIndex:i]valueForKey:@"categoryId"];
            
            
            NSString* serviceOtherState =[[updateServiceList objectAtIndex:i]valueForKey:@"otherState"];
            
            if ([serviceOtherState length]!=0)
            {
                serviceOtherState=[NSString stringWithFormat:@"|%@|",serviceOtherState];
            }
            
            NSLog(@"Update Other State=%@",serviceOtherState);
            
            if (serviceOtherState == (NSString *)[NSNull null]||[serviceOtherState length]==0) {
                serviceOtherState=@"";
            }
            
            
            
            
            
            //NSLog(@"insertServicesData");
            //code to be executed in the background
            
            if (servicedeptAddress == (NSString *)[NSNull null]||[servicedeptAddress length]==0) {
                
                servicedeptAddress=@"";
            }
            if (serviceworkingHours == (NSString *)[NSNull null]||[serviceworkingHours length]==0) {
                serviceworkingHours=@"";
            }
            if (servicedeptDescription == (NSString *)[NSNull null]||[servicedeptDescription length]==0) {
                servicedeptDescription=@"";
            }
            if (servicelang == (NSString *)[NSNull null]||[servicelang length]==0) {
                servicelang=@"";
            }
            if (serviceemail == (NSString *)[NSNull null]||[serviceemail length]==0) {
                serviceemail=@"";
            }
            
            //-------  fields added to be shown--------------
            
            
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId=@"";
            }
            
            if (serviceName == (NSString *)[NSNull null]||[serviceName length]==0) {
                serviceName=@"";
            }
            if (description == (NSString *)[NSNull null]||[description length]==0) {
                description=@"";
            }
            if (image == (NSString *)[NSNull null]||[image length]==0) {
                image=@"";
            }
            if (categoryName == (NSString *)[NSNull null]||[categoryName length]==0) {
                categoryName=@"";
            }
            if (subCategoryName == (NSString *)[NSNull null]||[subCategoryName length]==0) {
                subCategoryName=@"";
            }
            if (rating == (NSString *)[NSNull null]||[rating length]==0) {
                rating=@"";
            }
            if (url == (NSString *)[NSNull null]||[url length]==0) {
                url=@"";
            }
            if (state == (NSString *)[NSNull null]||[state length]==0) {
                state=@"";
            }
            if (lat == (NSString *)[NSNull null]||[lat length]==0) {
                lat=@"";
            }
            if (log == (NSString *)[NSNull null]||[log length]==0) {
                log=@"";
            }
            if (serviceIsFav == (NSString *)[NSNull null]||[serviceIsFav length]==0) {
                serviceIsFav=@"";
            }
            if (serviceIsHidden == (NSString *)[NSNull null]||[serviceIsHidden length]==0) {
                serviceIsHidden=@"";
            }
            if (contact == (NSString *)[NSNull null]||[contact length]==0) {
                contact=@"";
            }
            if (serviceisNotifEnabled == (NSString *)[NSNull null]||[serviceisNotifEnabled length]==0) {
                serviceisNotifEnabled=@"";
            }
            
            if (website == (NSString *)[NSNull null]||[website length]==0) {
                website=@"";
            }
            
            if (servicecategoryId == (NSString *)[NSNull null]||[servicecategoryId length]==0) {
                servicecategoryId = @"";
            }
            
            NSString* otherwebsite =[[updateServiceList objectAtIndex:i]valueForKey:@"otherwebsite"];
            
            if (otherwebsite == (NSString *)[NSNull null]||[otherwebsite length]==0) {
                otherwebsite = @"";
            }
            
            
            NSLog(@"otherwebsite ---------> %@",otherwebsite);
            
            //code to be executed in the background
            /*  [singleton.dbManager updateServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:servicelang servicedeptAddress:servicedeptAddress serviceworkingHours:serviceworkingHours servicedeptDescription:servicedeptDescription serviceemail:serviceemail popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState];*/
            
            /*  [singleton.dbManager updateServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:servicelang servicedeptAddress:servicedeptAddress serviceworkingHours:serviceworkingHours servicedeptDescription:servicedeptDescription serviceemail:serviceemail popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState otherwebsite:otherwebsite];
             
             */
            NSString* depttype =[[updateServiceList objectAtIndex:i]valueForKey:@"depttype"];
            if (depttype == (NSString *)[NSNull null]||[depttype length]==0) {
                depttype=@"";
            }
            NSString* disname =[[updateServiceList objectAtIndex:i]valueForKey:@"disname"];
            if (disname == (NSString *)[NSNull null]||[disname length]==0) {
                disname=@"";
            }
            
            
            NSString* multicatid =[[updateServiceList objectAtIndex:i]valueForKey:@"multicatid"];
            if (multicatid == (NSString *)[NSNull null]||[multicatid length]==0) {
                multicatid=@"";
            }
            
            NSString* multicatname =[[updateServiceList objectAtIndex:i]valueForKey:@"multicatname"];
            if (multicatname == (NSString *)[NSNull null]||[multicatname length]==0) {
                multicatname=@"";
            }
            
            

            
            [singleton.dbManager updateServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:servicelang servicedeptAddress:servicedeptAddress serviceworkingHours:serviceworkingHours servicedeptDescription:servicedeptDescription serviceemail:serviceemail popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState otherwebsite:otherwebsite depttype:depttype disname:disname multicatid:multicatid multicatname:multicatname];
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
}

-(void)deleteServiceToDBTask:(NSMutableArray*)deleteServiceList
{
    
    
    for (int i=0; i<[deleteServiceList count]; i++)
    {
        NSString* serviceId =[[deleteServiceList objectAtIndex:i]valueForKey:@"serviceId"];
        
        
        //NSLog(@"deleteServiceData");
        
        //code to be executed in the background
        [singleton.dbManager deleteServiceData:serviceId];
    }
}
-(void)favServiceToDBTask:(NSMutableArray*)favouriteServiceList
{
    for (int i=0; i<[favouriteServiceList count]; i++)
    {
        NSString* serviceId =[[favouriteServiceList objectAtIndex:i]valueForKey:@"serviceId"];
        NSString* serviceIsFav =@"true";
        //NSLog(@"updateServiceIsFav");
        
        if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
            serviceId=@"";
        }
        
        
        //code to be executed in the background
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:serviceIsFav hitAPI:@"No"];
    }
}
-(void)ServiceCardToDBTask:(NSMutableArray*)serviceCardList
{
    for (int i=0; i<[serviceCardList count]; i++)
    {
        @try {
            
            /*
             NSMutableArray *serviceArr = [[serviceCardList objectAtIndex:i]valueForKey:@"ServiceId"];
             NSString* temp = @"";
             for (int j = 0; j < [serviceArr count]; j++)
             {
             temp =[NSString stringWithFormat:@"%@%@%@", temp , [serviceArr objectAtIndex:j] ,@","];
             
             }
             NSRange match=[temp rangeOfString:@"," options:NSBackwardsSearch];
             temp= [temp substringWithRange:NSMakeRange(0,match.location)];
             NSLog(@"temp=%@",temp);
             */
            
            //------- compress code ---------
            NSMutableArray *serviceArr = [[serviceCardList objectAtIndex:i]valueForKey:@"ServiceId"];
            NSString *temp = [serviceArr componentsJoinedByString:@","];
            NSLog(@"temp=%@",temp);
            //-------compress code end---------
            NSString* cardName =[[serviceCardList objectAtIndex:i]valueForKey:@"cardName"];
            NSString* url =[[serviceCardList objectAtIndex:i]valueForKey:@"url"];
            NSString* serviceCard =[[serviceCardList objectAtIndex:i]valueForKey:@"serviceCard"];
            
            // serviceCard
            if ([url length]==0) {
                url=@"";
            }
            if ([cardName length]==0) {
                cardName=@"";
                
            }
            if ([serviceCard length]==0) {
                serviceCard=@"";
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //code to be executed on the main thread when background task is finished
                // [singleton.dbManager insertServiceSections:cardName sectionImg:url  sectionServices:temp];
                [singleton.dbManager insertServiceSections:cardName sectionImg:url  sectionServices:temp serviceCard:serviceCard];
                
            });
            
        }
        
        @catch (NSException *exception)
        {
            
        }
        @finally
        {
            
        }
        
    }
    
    
}


//----- hitAPI for hitFetchHerospaceAPI ------
-(void)hitFetchHerospaceAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"st"]; //(M in case of fetch data related to state)like punjab name
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    // =-----Rashpinder Changes For Banner Size -----
    // UM_API_HERO_SPACE
    NSInteger size;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        size =  fDeviceWidth * 2;
    }else {
        size =  fDeviceWidth <380 ? fDeviceWidth * 2: fDeviceWidth * 3;
    }
    
    [dictBody setObject:[NSString stringWithFormat:@"%ld", (long)size] forKey:@"size"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_HERO_SPACE withBody:dictBody andTag:TAG_REQUEST_HERO_SPACE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        //[hud hideAnimated:YES];
        
        if (error == nil) {
            //NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //            NSString *rc=[response valueForKey:@"rc"];
            //            NSString *rs=[response valueForKey:@"rs"];
            //  NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", DISPATCH_QUEUE_SERIAL);
                dispatch_async(queue, ^{
                    //NSLog(@"deleteBannerHomeData");
                    
                    //code to be executed in the background
                    //code is added by me
                    singleton.objUserProfile = nil;
                    singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                    
                    // code is added by me
                    //------- listHeroSpace insert it into database-------
                    listHeroSpace=[[NSMutableArray alloc]init];
                    listHeroSpace=[[[response valueForKey:@"pd"] valueForKey:@"listHeroSpace"] mutableCopy];
                    
                    if ([listHeroSpace count]>0)
                    {
                        [singleton.dbManager deleteBannerHomeData]; //delete old database
                        
                    }
                    else
                    {
                        // singleton.bannerStatus=NSLocalizedString(@"no", nil);
                        
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //code to be executed on the main thread when background task is finished
                        [self insertHeroSpaceToDBTask:response];
                        
                    });
                });
                
            }
            
        }
        else{
            /* //NSLog(@"Error Occured = %@",error.localizedDescription);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
            
            
            // [self closeBtnAction:self]; // if error occur hide the notify view
        }
        
    }];
    
}



-(void)insertHeroSpaceToDBTask:(NSDictionary*)response
{
    //singleton.dbManager  = [[DBManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    //NSLog(@"response= %@",response);
    
    
    
    @try {
        
        
        //   NSString *  amno=[NSString stringWithFormat:@"%@",[response valueForKey:@"amno"]];
        //  NSString *  email=[NSString stringWithFormat:@"%@",[response valueForKey:@"email"]];
        //  NSString *  mno=[NSString stringWithFormat:@"%@",[response valueForKey:@"mno"]];
        //   NSString *  nam=[NSString stringWithFormat:@"%@",[response valueForKey:@"nam"]];
        profileComplete=[NSString stringWithFormat:@"%@",[[response valueForKey:@"pd"] valueForKey:@"profileComplete"]];
        
        
        NSString *str_pic=[NSString stringWithFormat:@"%@",[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"pic"]];
        singleton.user_profile_URL=str_pic;
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //------------------------- Encrypt Value------------------------
        
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:profileComplete withKey:@"PROFILE_COMPELTE_KEY"];
        // [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //------------------------- Encrypt Value------------------------
        //NSLog(@"amno= %@",amno);
        //NSLog(@"email= %@",email);
        //NSLog(@"mno= %@",mno);
        //NSLog(@"nam= %@",nam);
        //NSLog(@"profileComplete= %@",profileComplete);
        /*
         "profileComplete":"30","generalpd":{"sid":"577","nam":"deepak","addr":"","st":"Uttarakhand","cty":"Haridwar","dob":"11-04-1987","gndr":"M","qual":"","email":"","amno":"","lang":"en","aadhr":"","status":"","ntfp":"1","ntft":"1","pic":"","psprt":"","occup":"","dist":"Haridwar","amnos":"0","emails":"0","ivrlang":"","gcmid":"15"},"socialpd":{"goid":"","goname":"","goimg":"","fbid":"","fbname":"","fbimg":"","twitid":"","twitname":"","twitimg":"","status":"","cdate":"","mdate":""}},"uid":nu
         
         */
        
        //------- generalpdList-------
        NSMutableArray *generalpdList=[[NSMutableArray alloc]init];
        generalpdList=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] mutableCopy];
        //NSLog(@"generalpdList= %@",generalpdList);
        
        
        //------- listHeroSpace insert it into database-------
        NSMutableArray *listHeroSpace1=[[NSMutableArray alloc]init];
        listHeroSpace1=[[[response valueForKey:@"pd"] valueForKey:@"listHeroSpace"] mutableCopy];
        
        
        //------- listMessageBoard-------
        NSMutableArray *listMessageBoard=[[NSMutableArray alloc]init];
        listMessageBoard=[[[response valueForKey:@"pd"] valueForKey:@"listMessageBoard"] mutableCopy];
        //NSLog(@"listMessageBoard= %@",listMessageBoard);
        
        
        
        
        if(listHeroSpace1 && [listHeroSpace1 count]>=1)
        {
            [self addlistHeroSpaceToDBTask:listHeroSpace1];
        }
        else
        {
            [self fetchHeroSpaceDatafromDB];// used to get data from database
            
        }
        
        
        
        
    }
    
    
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
        
    }
    
}


-(void)addlistHeroSpaceToDBTask:(NSMutableArray*)listHeroSpaceArr
{
    
    // Save your stuff
    /* NSOperationQueue *queueBanner = [[NSOperationQueue alloc] init];
     queueBanner.maxConcurrentOperationCount = 1;
     
     
     
     [queueBanner addOperationWithBlock:^{
     
     //NSLog(@"banner Log 1");
     
     for (int i=0; i<[listHeroSpace count]; i++)
     {
     @try {
     
     
     NSString* actionType =[[listHeroSpace objectAtIndex:i]valueForKey:@"actionType"];
     NSString* actionURL =[[listHeroSpace objectAtIndex:i]valueForKey:@"actionURL"];
     NSString* desc =[[listHeroSpace objectAtIndex:i]valueForKey:@"desc"];
     NSString* imageUrl=[[listHeroSpace objectAtIndex:i]valueForKey:@"imageUrl"];
     //NSLog(@"insertBannerHomeData");
     
     
     
     
     if ([actionType length]==0) {
     actionType=@"";
     }
     if ([actionURL length]==0) {
     actionURL=@"";
     }
     if ([desc length]==0) {
     desc=@"";
     }
     if ([imageUrl length]==0) {
     imageUrl=@"";
     }
     
     
     [singleton.dbManager insertBannerHomeData:imageUrl bannerActionType:actionType bannerActionUrl:actionURL bannerDesc:desc];
     } @catch (NSException *exception) {
     
     } @finally {
     
     }
     }
     
     
     
     }];
     
     [queueBanner addOperationWithBlock:^{
     //NSLog(@"banner Log 2");
     
     
     
     [self fetchHeroSpaceDatafromDB];
     
     
     }];
     */
    
    for (int i=0; i<[listHeroSpaceArr count]; i++)
    {
        @try {
            
            
            NSString* actionType =[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"actionType"];
            NSString* actionURL =[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"actionURL"];
            NSString* desc =[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"desc"];
            NSString* imageUrl=[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"imageUrl"];
            //NSLog(@"insertBannerHomeData");
            
            NSString* bannerid=[[listHeroSpaceArr objectAtIndex:i]valueForKey:@"bannerid"];
            
            if([bannerid length]==0 || bannerid ==nil|| [bannerid isEqualToString:@"(null)"])
            {
                bannerid=@"";
            }
            if([actionType length]==0 || actionType ==nil|| [actionType isEqualToString:@"(null)"])
            {
                actionType=@"";
            }
            if([actionURL length]==0 || actionURL ==nil|| [actionURL isEqualToString:@"(null)"])
            {
                actionURL=@"";
            }
            if([desc length]==0 || desc ==nil|| [desc isEqualToString:@"(null)"])
            {
                desc=@"";
            }
            if([imageUrl length]==0 || imageUrl ==nil|| [imageUrl isEqualToString:@"(null)"])
            {
                imageUrl=@"";
            }
            
            
            [singleton.dbManager insertBannerHomeData:imageUrl bannerActionType:actionType bannerActionUrl:actionURL bannerDesc:desc bannerid:bannerid];
            
            //[singleton.dbManager insertBannerHomeData:imageUrl bannerActionType:actionType bannerActionUrl:actionURL bannerDesc:desc];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
    
    
    
    [self fetchHeroSpaceDatafromDB];
    
    
    
    //[self fetchHeroSpaceDatafromDB];// used to get data from database
    
}


-(void)fetchHeroSpaceDatafromDB
{
    
    NSArray *imagearray=[singleton.dbManager getBannerHomeData];
    [self.headerBanner setDataSourceWithArrBaners:imagearray];
    //NSLog(@"imagearray=%@",imagearray);
    //_headerView = [[AdvertisingColumn alloc]initWithFrame:CGRectMake(0, 5, fDeviceWidth, AD_height)];
//    _headerView = [[AdvertisingColumn alloc]initWithFrame:CGRectMake(0, 5, fDeviceWidth, AD_height+15)];
//
//    if ([[UIScreen mainScreen]bounds].size.height == 736)
//    {
//
//        //return 150;
//        _headerView = [[AdvertisingColumn alloc]initWithFrame:CGRectMake(0, 5, fDeviceWidth, AD_height+25)];
//
//    }
//    if ([[UIScreen mainScreen]bounds].size.height >= 815)
//    {
//
//        //return 150;
//        _headerView = [[AdvertisingColumn alloc]initWithFrame:CGRectMake(0, 0, fDeviceWidth, AD_height+105)];
//
//    }
//
//    if (iPhoneX())
//    {
//
//        //return 150;
//        _headerView = [[AdvertisingColumn alloc]initWithFrame:CGRectMake(0, 0, fDeviceWidth, AD_height+25)];
//
//    }
//    _headerView.backgroundColor = [UIColor clearColor];
//    // _headerView.backgroundColor = [UIColor yellowColor];
//
//    [_headerView setArray:imagearray];
    
    
//    if (![self connected])
//    {
//        // Not connected
//        [listHeroSpace removeAllObjects];
//        listHeroSpace=[imagearray mutableCopy];
//    } else {
//        // Connected. Do some Internet stuff
//
//    }
    
    
    
    if ([listHeroSpace count]>0)
    {
        
        if ([imagearray count]!=0)
        {
            // [self reloadRow0Section0];//reload banner view
            [self refreshSettingData];
            [self reloadTableOnMainQueue];
            //[tableView_home reloadData];
        }
        else
        {
            
            [self hitFetchHerospaceAPI];
        }
        
        
    }
    else
    {
        // singleton.bannerStatus=NSLocalizedString(@"no", nil);
        [self reloadTableOnMainQueue];
        
    }
    [self addNotify];
    
    
    
    
}



#pragma mark- Fav Cell Action Methods


- (IBAction)fav_action:(MyFavButton *)sender
{
    
    [singleton traceEvents:@"Favourite button" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
    
    MyFavButton *button = (MyFavButton *)sender;
    // Add image to button for normal state
    NSLog(@"sender=%ld",(long)[sender tag]);
    
    int indexOfTheRow=(int)button.tag;
    
    NSLog(@"indexOfTheRow=%d",indexOfTheRow);
    
    
    // NSString *text = [NSString stringWithFormat:@"%@",indexOfTheRow];
    
    NSString *serviceFav=[NSString stringWithFormat:@"%@",[[self.tableData  objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_IS_FAV"]];
    
    NSString *serviceId=[[self.tableData objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_ID"];
    
    
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
        button.selected=FALSE;
        [self.tableData removeObjectAtIndex:indexOfTheRow];
    }
    else
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
        button.selected=true;
    }
    singleton.loadServiceStatus =@"FALSE";//if added load service status
    //[self checkServiceStatus];
    [self checkNoFavouriteListView];
    [self reloadTableOnMainQueue];
    
}
-(void)checkNoFavouriteListView {
    self.vw_noServiceFound.hidden = true;
    if (self.tableData.count == 0 && self.segmentControl.selectedSegmentIndex == 1) {
        self.vw_noServiceFound.hidden = false;
        [self.view bringSubviewToFront:self.vw_noServiceFound];
        CGFloat topHeight = 50 + 150;
        if ([[UIScreen mainScreen]bounds].size.height >= 815)
        {
            topHeight = 50 + 215;
        }
        self.topNoServiceConstraint.constant = topHeight;
        [self.view layoutIfNeeded];
        [self.view updateConstraintsIfNeeded];
    }
}
- (IBAction)btnMoreInfoClicked:(UIButton *)sender
{
    
    
    self.cellDataOfmore = (NSMutableDictionary*) (NSDictionary*)[self.tableData objectAtIndex:[sender tag]];
    [singleton traceEvents:@"More Button" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
    
    [self btnOpenSheet];
}
- (void)btnOpenSheet

{
    NSString *information=NSLocalizedString(@"information", nil);
    NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [singleton traceEvents:@"Department Information" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
            
            [self callDetailServiceVC];
        }
            break;
        case 1:
        {
            NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
            
            [singleton traceEvents:@"View On Map" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
            
            NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
            
            NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
            
            NSString *deptName=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_NAME"]];
            deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]])
                
            {
                NSLog(@"Map App Found");
                
                
                NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
                
                // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
                
                NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSLog(@"mapURL= %@",mapURL);
                
                [[UIApplication sharedApplication] openURL:mapURL];
                
            } else
            {
                NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
                
            }
            
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}
-(void)callDetailServiceVC
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    vc.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [topvc presentViewController:vc animated:NO completion:nil];
    
}
@end

