//
//  AppUpdateView.m
//  UMANG
//
//  Created by Rashpinder on 06/10/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "AppUpdateView.h"

@implementation AppUpdateView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



-(CGRect)getRect {
    // return [UIScreen mainScreen].bounds.size.height == 812.0 ? CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 84.0) : CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 64.0);
    return [UIScreen mainScreen].bounds.size.height == 812.0 ? CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 94.0) : CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 64.0);
    
}

- (id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"AppUpdateViewXIB" owner:self options:nil];
    self = (AppUpdateView *)[objects objectAtIndex:0];
    self.frame = frame;
   // self.lblUpdateMSG.text = NSLocalizedString(@"app_version_available", nil);//@"New app version available";
    
    self.lblUpdateMSG.text = NSLocalizedString(@"please_update_your_app", nil);
    
    [self.btnUpdate setTitle:NSLocalizedString(@"update_caps", nil) forState:UIControlStateNormal];
    [self.btnUpdate setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    self.btnUpdate.alpha = 1.0;
    UISwipeGestureRecognizer *swipeGestureDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didRemoveAppUpdateView:)];
    swipeGestureDown.direction = UISwipeGestureRecognizerDirectionDown;
     UISwipeGestureRecognizer *swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didRemoveAppUpdateView:)];
    swipeGestureLeft.direction = UISwipeGestureRecognizerDirectionLeft;

     UISwipeGestureRecognizer *swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didRemoveAppUpdateView:)];
    swipeGestureRight.direction = UISwipeGestureRecognizerDirectionRight;

    
    [self addGestureRecognizer:swipeGestureDown];
    [self addGestureRecognizer:swipeGestureLeft];

    [self addGestureRecognizer:swipeGestureRight];

    
    return self;
}
- (IBAction)didTapUpdateButtonAction:(UIButton *)sender {
    
    NSURL *url = [NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1236448857"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        if (self.didRemoveApp != nil) {
            self.didRemoveApp();
        }
        NSString *newVersion = [NSString stringWithFormat:@"%@",[[SharedManager sharedSingleton].arr_initResponse valueForKey:@"nver"]];
        [[NSUserDefaults standardUserDefaults] setValue:newVersion forKey:APPUPDATE_SHOWN_VERSION];
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)didRemoveAppUpdateView:(UISwipeGestureRecognizer *)sender{
    
    if (self.didRemoveApp != nil) {
        self.didRemoveApp();
    }
   // [[NSUserDefaults standardUserDefaults] setBool:true forKey:APPUPDATE_IS_REMOVED];
    NSString *newVersion = [NSString stringWithFormat:@"%@",[[SharedManager sharedSingleton].arr_initResponse valueForKey:@"nver"]];
     [[NSUserDefaults standardUserDefaults] setValue:newVersion forKey:APPUPDATE_SHOWN_VERSION];
    [self removeFromSuperview];
}
@end
