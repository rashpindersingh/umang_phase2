//
//  AppUpdateView.h
//  UMANG
//
//  Created by Rashpinder on 06/10/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppUpdateView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblUpdateMSG;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property(copy) void(^didRemoveApp)(void);

@end
