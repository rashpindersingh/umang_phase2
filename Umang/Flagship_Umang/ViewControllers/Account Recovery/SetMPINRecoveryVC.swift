//
//  SetMPINRecoveryVC.swift
//  UMANG
//
//  Created by Rashpinder on 26/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

class SetMPINRecoveryVC: UIViewController ,UITextFieldDelegate
{
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var btn_help: UIButton!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var lbl_mpinbelow: UILabel!
    @IBOutlet weak var lbl_EnterMPIN: UILabel!
    @IBOutlet weak var lbl_ConfirmMPIN: UILabel!
    @IBOutlet weak var txt_mpin: UITextField!
    @IBOutlet weak var txt_Cmpin: UITextField!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var vw_line1: UIView!
    @IBOutlet weak var vw_line2: UIView!
    var mpinType : SetMinType! = .register
    var isQtsn = "no"
    var mobileVerify = ""
    var hud = MBProgressHUD()
    var arrayQstn = NSMutableArray()

  //  "rectype":"altmno"  //types can be null(registered no with otp), altmno(for otp on alternate mobile no), secques(for secutiry questions), emailotp(for otp on email)

    enum SetMinType :String {
        case alterMobile = "altmno"
        case securityQtsn = "secques"
        case emailOtp = "emailotp"
        case register = "null"
    }
    
    @IBAction func submitAction(_ sender: Any)
    {
     
        
        let maxLength = 4
        let currentMpinString: NSString = self.txt_mpin.text! as NSString
        let currentCmpinString: NSString = self.txt_Cmpin.text! as NSString

        if currentMpinString.length < maxLength
        {
            
            let alert = UIAlertView(title: "error".localized, message: "enter_mpin_to_verify".localized, delegate: nil, cancelButtonTitle:"ok".localized)
            alert.show()
        }
        else if currentCmpinString.length < maxLength
        {
            
            let alert = UIAlertView(title: "error".localized, message: "confirm_mpin".localized, delegate: nil, cancelButtonTitle:"ok".localized)
            alert.show()
        }
            
        else if self.txt_mpin.text != self.txt_Cmpin.text
        {
            
            
            let alert = UIAlertView(title: "error".localized, message: "mpin_donot_match_txt".localized, delegate: nil, cancelButtonTitle:"ok".localized)
            alert.show()
            
            
            
        }
        else
        {
 
            print("mpin \(self.txt_mpin.text ?? "")")
            print("Cmpin \(String(describing: self.txt_Cmpin.text))")

            // Next to hit api Here
            self.callApiVerifyMobile()

        }

    }
    
    @IBAction func helpButtonAction(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let helpVC = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController  
        helpVC.comingFrom = "accountRecovery"

        self.navigationController?.pushViewController(helpVC, animated: true)
        
        
    }
    
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        for vc  in self.navigationController!.viewControllers {
            if vc.isKind(of: AccountRecoveryVC.self) {
                self.navigationController?.popToViewController(vc, animated: true)
            }
        }

    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isQtsn == "yes" {
            mpinType = .securityQtsn
        }
        self.txt_mpin.delegate = self
        self.txt_Cmpin.delegate = self
        
         self.setFontSize()

        self.layoutCornerRadius(view: self.btn_submit, radius: 5)

        UIApplication.shared.statusBarStyle = .default
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(didTapView))
        
        self.view.addGestureRecognizer(tapGesture)

        // Do any additional setup after loading the view.
    }
    @objc func didTapView()
    {
        self.view.endEditing(true)
    }
    func setFontSize()  {
        txt_mpin.font = AppFont.regularFont(20)
        txt_Cmpin.font = AppFont.regularFont(20)
        self.lbl_EnterMPIN.font = AppFont.regularFont(17)

        self.lbl_ConfirmMPIN.font = AppFont.regularFont(17)

        lbl_title.font = AppFont.semiBoldFont(17)
        self.lbl_mpinbelow.font = AppFont.regularFont(14)
      
        self.btn_submit.titleLabel?.font = AppFont.mediumFont(19)
        self.btnback.titleLabel?.font = AppFont.regularFont(17.0)
     
        btnback.setTitle("back".localized, for: .normal)
        
        self.lbl_EnterMPIN.text = "enter_mpin_verify".localized
        self.lbl_ConfirmMPIN.text = "confirm_mpin".localized
        self.lbl_mpinbelow.text = "set_mpin_heading".localized
        
        self.btn_submit.setTitle("submit".localized, for: .normal)
        self.btn_help.setTitle("help".localized, for: .normal)
        self.btnback.setTitle("back".localized, for: .normal)
        
        self.lbl_title.text = "setmpin_label".localized
        
        
    }
    
    struct EncryptSaltKey {
        static let WSO2_Port = "8580"
        static let SaltAES = "@#digitalspice*&"
        static let SaltAESDigilocker = "@#localedigitalumang*&"
        
        static let SaltRequestControl = "$f%GY#JX^9H@"
        static let SaltRequestVaue = "R%d&Wst676#(Na"
        static let SaltRequestVaueDigiLocker = "D@GUM4NG$#4PP"
        
        static let SaltMPIN = "56$f@D8H2x^"
        static let KEYCHAIN_ACCOUNT_KEY = "UmangUnique"
        static let SaltCHAT = "this"
        
    }
    
    func callApiVerifyMobile()  {
        
        
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        // Set the label text.
        hud.label.text = NSLocalizedString("loading", comment: "")
        let dictBody = NSMutableDictionary()
        if mobileVerify.count == 0 {
            mobileVerify = SharedManager.sharedSingleton().mobileNumber
        }
        dictBody["mno"] = mobileVerify //Enter mobile number of user
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = ""
        dictBody["tkn"] = SharedManager.sharedSingleton()?.user_tkn
        
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        dictBody["trkr"] = timeInMS
        
        let saltMpin :String = "|\(self.txt_mpin.text!)|\(EncryptSaltKey.SaltMPIN)|"
        let encryptMpin = saltMpin.sha256Hash(for: saltMpin)
        dictBody["mpin"] = encryptMpin
        if mpinType != .register {
            dictBody["rectype"] = mpinType.rawValue

        }
        if mpinType == .securityQtsn {
            dictBody["quesAnsList"] = arrayQstn

        }

        //"rectype":"altmno"  //types can be null(registered no with otp), altmno(for otp on alternate mobile no), secques(for secutiry questions), emailotp(for otp on email)

        
        
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: "/umang/coreapi/ws2/umpinu2", withBody: dictBody, andTag: TAG_REQUEST_CHAT, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            self?.hud.hide(animated: true)
            if error == nil && response != nil  {
                if let jsonResponse = response as? [String:Any] {
                    let node = jsonResponse["node"] as? String ?? ""
                    if  node.count > 0 {
                        UserDefaults.standard.setValue(node, forKey: "NODE_KEY")
                        UserDefaults.standard.synchronize()
                    }
                    let rd = jsonResponse["rd"]
                    //  let rtry = pdData["rtry"]
                    if (jsonResponse.string(key: "rs") == API_SUCCESS_CASE) || (jsonResponse.string(key: "rs") == API_SUCCESS_CASE1)
                    {
                        self?.showAlertUpdatedMsg(msg: "\(rd!)")
                    }
                }
            }
            else {
                self?.showAlert(msg: error!.localizedDescription)
            }
        })
        
    }
    
    
    
     func showAlertUpdatedMsg(msg:String)  {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title:"ok".localized, style: UIAlertActionStyle.default, handler: {[weak self](action:UIAlertAction!) in
            print("you have pressed the ok button")
            self?.pushToFlagshipVC()
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func pushToFlagshipVC()  {
         let objRequest =  UMAPIManager();
        objRequest.clearValueOnlogout();
        let api = CommanAPI()
        api.hitInitAPI()
        app_del.setFlagshipTabBarVC("NO")

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == txt_mpin && string.isEmpty == false {
            if textField.text!.count >= 4 {
                txt_mpin.resignFirstResponder()
                txt_Cmpin.becomeFirstResponder()
            }
        }else if textField == txt_Cmpin && string.isEmpty == false {
            if textField.text!.count >= 4 {
                txt_Cmpin.resignFirstResponder()
            }
        }
        return newString.length <= maxLength
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        
    }
    
    

}
