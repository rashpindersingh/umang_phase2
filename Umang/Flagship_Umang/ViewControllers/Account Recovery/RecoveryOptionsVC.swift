//
//  RecoveryOptionsVC.swift
//  UMANG
//
//  Created by Rashpinder on 24/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

class CellSecurityOptions: UITableViewCell {
    @IBOutlet weak var imageLeft:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblSubTitle:UILabel!
    @IBOutlet weak var btnSetOption:UIButton!
    @IBOutlet var btnCellOptions: UIButton!
    
    @IBOutlet var btnEmailVerify: UIButton!
    @IBOutlet var btnMoreOption: UIButton!
    
    @IBOutlet var dividerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var btnSetOptionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var lblSubtitleLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var descriptionBottomConstraint: NSLayoutConstraint!
    @IBOutlet var arrowHeightConstraint: NSLayoutConstraint!
}
enum RecoveryOptions :String{
    case securityQstn = "security_question"
    case email = "profile_email_hint"
    case alternateMobile = "profile_alt_mob_num"
}
enum  RecoveryType:String{
    case get = "Get"
    case set = "set"
    case non = "none"
}

class RecoveryOptionsVC: UIViewController {
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblViewTitle: UILabel!
    @IBOutlet weak var btnHelp: UIButton!
    @IBOutlet weak var tblRecovery:UITableView!
    @IBOutlet weak var headerView:UIView!
    var typeOfRecovery :RecoveryType = .non
    var arrRecovery :[RecoveryOptions] = [.alternateMobile,.email,.securityQstn]
    @IBOutlet weak var headerTitle: UILabel!
    var email = ""
    var alterMobile = ""
    var securityQtsn = ""
    
    var mobileVerify = ""
    var otpStr = ""
    var isFromLogin = "no"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.arrRecovery.removeAll()
        
        if !alterMobile.isEmpty {
            arrRecovery.append(.alternateMobile)
        }
        if !email.isEmpty {
            arrRecovery.append(.email)
        }
        if !securityQtsn.isEmpty {
            arrRecovery.append(.securityQstn)
        }
        
        
        self.tblRecovery.reloadData()
        self.setLocalized()
        
        tblRecovery.tableFooterView = UIView.init(frame: .zero)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setLocalized()
    {
       self.headerTitle.text = "following_recovery_options".localized
        self.headerTitle.numberOfLines = 10
        self.headerTitle.lineBreakMode = .byWordWrapping
        self.headerTitle.font = AppFont.regularFont(14)
        lblViewTitle.font = AppFont.semiBoldFont(17)
        lblViewTitle.text = "recovery_options_text".localized.capitalized
        btnBack.setTitle("back".localized, for: .normal)
        self.btnBack.titleLabel?.font = AppFont.regularFont(17.0)
        
    }
    
    @IBAction func didTapBtnHelpAction(_ sender: UIButton) {
        
        
    }
    @objc func didTapBtnSetOrChangeAction(_ sender: UIButton) {
        
    }
    
    
    @IBAction func didTapBackBtnAction(_ sender: UIButton) {
        
        
        for vc  in self.navigationController!.viewControllers {
                if vc.isKind(of: AccountRecoveryVC) {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
           }
       }
    
    func updateSecurityQtsn()  {
        
        SharedManager.sharedSingleton().mobileNumber = self.mobileVerify
        let array = (self.securityQtsn as NSString).components(separatedBy: ",") as NSArray
        app_del.pushtoUpdateSecurityVC(array as? [Any], sender: self, otp: self.otpStr , withTag: "RecoveryTAG")
       
    }
}
extension RecoveryOptionsVC :UITableViewDataSource , UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRecovery.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblRecovery.dequeueReusableCell(withIdentifier: "CellSecurityOptions", for: indexPath) as! CellSecurityOptions
        let option = arrRecovery[indexPath.row]
        
        cell.lblTitle.text = option.rawValue.localized
        //cell.lblSubTitle.text = "Dumy dumy dumt dumt dumt  e wer we Dumy dumy dumt dumt dumt  e wer we Dumy dumy dumt dumt dumt  e wer we "
        cell.lblSubTitle.numberOfLines = 3
        cell.lblSubTitle.lineBreakMode = .byWordWrapping
        
        cell.viewWithTag(44555)?.isHidden = true
        if indexPath.row == arrRecovery.count - 1 {
            cell.viewWithTag(44555)?.isHidden = false
        }
        
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        
        cell.lblTitle.font = AppFont.mediumFont(17)
        cell.lblSubTitle.font = AppFont.regularFont(15)
        cell.lblSubTitle.textColor = UIColor(red: 103.0/255.0, green: 103.0/255.0, blue: 103.0/255.0, alpha: 1)
        switch option {
        case .email:
            cell.lblSubTitle.text = String(format:"verify_emailaddress".localized, email)
            cell.imageLeft.image = UIImage(named: "email_recovery")
        case .alternateMobile:
            cell.lblSubTitle.text = String(format:"verify_altmobile".localized, alterMobile)
            cell.imageLeft.image = UIImage(named: "mobile_recovery")
        case .securityQstn:
            cell.lblSubTitle.text = "answer_securityques".localized
            cell.imageLeft.image = UIImage(named: "security_que_recovery")
            
        }
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let option = arrRecovery[indexPath.row]
        switch option {
        case .email:
            self.pushToVerifyEmailVC()
        case .alternateMobile:
            self.pushToVerifyAlterNateMobilVC()
        case .securityQstn:
            self.updateSecurityQtsn()
        
        }
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
   
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return self.headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 70
    }
}
extension RecoveryOptionsVC {
    
    
    func pushToVerifyAlterNateMobilVC()  {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
        vc.alterMobile = self.alterMobile
        vc.verifyType = .alterMobile
        vc.mobileVerify = mobileVerify
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func pushToVerifyEmailVC()  {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
        vc.email = self.email
        vc.verifyType = .email
        vc.mobileVerify = mobileVerify
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
