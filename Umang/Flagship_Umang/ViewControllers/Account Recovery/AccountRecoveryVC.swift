//
//  AccountRecoveryVC.swift
//  UMANG
//
//  Created by Rashpinder on 26/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

let blueColor =  UIColor(red:0.0/255.0, green: 89.0/255.0, blue:  157.0/255.0, alpha: 1)
class AccountRecoveryVC: UIViewController {

    @IBOutlet weak var btnHelp: UIButton!
    @IBOutlet weak var lblViewTitle: UILabel!
    @IBOutlet weak var lblEnterMobileMSG: UILabel!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDntAccountAcess: UILabel!
    @IBOutlet weak var btnContactCustomer: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    var hud:MBProgressHUD!
     let lbl91 = UILabel(frame: CGRect(x: 35, y: 0, width:60, height: 48))
    
    
    var isFromLogin = "no"
    var mobileVerify = ""

    override func viewDidLoad() {
        super.viewDidLoad()
       let leftImage = UIImageView(frame: CGRect(x: 0, y: self.txtMobile.frame.size.height / 2 - 12, width: 25, height: 25))
        leftImage.image = UIImage(named: "profile_alt_mob_num")
        leftImage.contentMode = .scaleAspectFit
     
        lbl91.text = "+91"
        lbl91.textColor = .lightGray
        lbl91.font = AppFont.regularFont(22)
        lbl91.textColor = UIColor(red: 153.0/255.0, green:  153.0/255.0, blue:  153.0/255.0, alpha: 1)
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height:50))
          leftView.addSubview(leftImage)
        leftView.addSubview(lbl91)

        self.txtMobile.keyboardType = .numberPad
        self.txtMobile.leftView = leftView
        self.txtMobile.leftViewMode = .always
        let layer =  UIView(frame: CGRect(x: 2, y: self.txtMobile.frame.size.height-1, width: Screen_Size.width - 54, height: 1))
        layer.backgroundColor = blueColor
        self.txtMobile.addSubview(layer)
        
        self.btnNext.backgroundColor = UIColor(red:53.0/255.0, green:  184.0/255.0, blue:  104.0/255.0, alpha: 1)
        self.btnContactCustomer.setTitleColor(UIColor(red:53.0/255.0, green:  184.0/255.0, blue:  104.0/255.0, alpha: 1), for: .normal)
        
        self.layoutCornerRadius(view: self.btnNext, radius: 5)
        if let vwBottom = self.view.viewWithTag(8899) {
            self.layoutBorderWithCornerRadius(view: vwBottom, radius: 0, borderColor: .lightGray, borderWidth: 1.0)
        }
        self.setFontSize()
        let tap = UITapGestureRecognizer(target: self, action: #selector(didEndEditing))
        self.view.addGestureRecognizer(tap)
        self.txtMobile.isUserInteractionEnabled = true
        if mobileVerify.count != 0 {
            self.txtMobile.text = mobileVerify
            self.txtMobile.isUserInteractionEnabled = false
        }
        
        if let txt = txtMobile.text {
            lbl91.textColor = txt.isEmpty ?  UIColor(red: 153.0/255.0, green:  153.0/255.0, blue:  153.0/255.0, alpha: 1) : UIColor.black
        }
        // Do any additional setup after loading the view.
    }

    @objc func didEndEditing(){
    self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setFontSize()  {
        txtMobile.font = AppFont.regularFont(22)
        lblViewTitle.font = AppFont.semiBoldFont(17)
        self.lblTitle.font = AppFont.regularFont(18)
        self.lblEnterMobileMSG.font = AppFont.regularFont(14)
        self.lblEnterMobileMSG.textColor = UIColor(red: 120.0/255.0, green:  120.0/255.0, blue:  120.0/255.0, alpha: 1)
        self.lblDntAccountAcess.font = AppFont.regularFont(12)
        self.btnContactCustomer.titleLabel?.font = AppFont.mediumFont(15)
        self.btnContactCustomer.setTitleColor(blueColor, for: .normal)
        self.btnContactCustomer.titleLabel?.textColor = blueColor
  
        self.btnNext.setTitle("next".localized, for: .normal)
        self.btnNext.titleLabel?.font = AppFont.mediumFont(19)
        self.btnBack.titleLabel?.font = AppFont.regularFont(17.0)
        
        lblTitle.text = "recover_account".localized
        lblEnterMobileMSG.text = "enter_registered_mobile".localized
        lblViewTitle.text = "account_recovery".localized
        btnBack.setTitle("back".localized, for: .normal)
        
        
        self.lblDntAccountAcess.text = "noaccess_umang_mobile".localized
        self.btnContactCustomer.setTitle("contact_customer".localized, for: .normal)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func didTapBtnHelpAction(_ sender: UIButton) {
        
    }
    
    @IBAction func didTapBackBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didTapNextButtonAction(_ sender: UIButton) {
        
//        self.pushToVerifyOTPVC(tout: "20", rtry: "29|20")
//        return
        if txtMobile.text == nil || txtMobile.text?.count == 0 || !self.validatePhone(txtMobile.text!) {
            let alert = UIAlertView(title: "error".localized, message: "enter_correct_phone_number".localized, delegate: nil, cancelButtonTitle:"ok".localized) //UIAlertView(title:"error".localized, message: "enter_correct_phone_number".localized, delegate: nil, cancelButtonTitle: "ok".localized, otherButtonTitles: "")
            alert.show()
            return
        }
        self.callApiVerifyMobile()
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecoveryOptionsVC") as! RecoveryOptionsVC
//        vc.typeOfRecovery = .get
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapContactCustomerCareAction(_ sender: UIButton)
    {
    
        /*if let shared = SharedManager.sharedSingleton().phoneSupport {
            var mobileNumber = "telprompt://\(shared)"
            if let aNumber = URL(string: mobileNumber) {
                if UIApplication.shared.canOpenURL(aNumber){
                    UIApplication.shared.openURL(aNumber)
                }
            }
        }*/
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let helpVC = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        helpVC.comingFrom = "accountRecovery"
        self.navigationController?.pushViewController(helpVC, animated: true)
      
    }
    override func showAlert(msg:String)  {
        let alert = UIAlertView(title: "error".localized, message: msg, delegate: nil, cancelButtonTitle:"ok".localized) //UIAlertView(title:"error".localized, message: "enter_correct_phone_number".localized, delegate: nil, cancelButtonTitle: "ok".localized, otherButtonTitles: "")
        alert.show()
    }
//  The converted code is limite d to 2 KB.
//  Upgrade your plan to remove this limitation.
//
    func callApiVerifyMobile()  {
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        // Set the label text.
        hud.label.text = NSLocalizedString("loading", comment: "")
        let dictBody = NSMutableDictionary()
        dictBody["mno"] = txtMobile.text! //Enter mobile number of user
        dictBody["chnl"] = "sms" //chnl : type sms for OTP and IVR for call
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = "frgtmpn"
        SharedManager.sharedSingleton()!.mobileNumber = txtMobile.text!
        
        // [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FORGOT_MPIN withBody:dictBody andTag:TAG_REQUEST_INIT_REG completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_FORGOT_MPIN, withBody: dictBody, andTag: TAG_REQUEST_DIGILOCKER_GETTOKEN, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            self?.hud.hide(animated: true)
            if error == nil && response != nil  {
                if let jsonResponse = response as? [String:Any] {
                    
                    let node = jsonResponse["node"] as? String ?? ""
                    if  node.count > 0 {
                        UserDefaults.standard.setValue(node, forKey: "NODE_KEY")
                        UserDefaults.standard.synchronize()
                        print("node in Accounr Recovery --\(node)")

                    }
                    if let pdData = jsonResponse["pd"] as? [String:Any] {
                        let tout = pdData["tout"]
                        let rtry = pdData["rtry"]
                        if (jsonResponse.string(key: "rs") == API_SUCCESS_CASE) || (jsonResponse.string(key: "rs") == API_SUCCESS_CASE1) {
                            self?.pushToVerifyOTPVC(tout: tout, rtry: rtry)
                        }
                    }
                }
              }
            else {
                self?.showAlert(msg: error!.localizedDescription)
            }
        })
   
}
    func pushToVerifyOTPVC(tout:Any?,rtry:Any?)  {
        var vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
        vc.tout = "\(tout!)".toFloat()
        vc.rtry = "\(rtry!)"
        vc.mobileVerify = self.txtMobile.text!
        vc.isFromLogin = isFromLogin
         SharedManager.sharedSingleton()!.mobileNumber = txtMobile.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    
}
extension AccountRecoveryVC :UITextFieldDelegate
{
    
func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     
    var currentTxtStr = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
    currentTxtStr = currentTxtStr?.trimmingCharacters(in: CharacterSet.whitespaces)
    if (currentTxtStr?.count ?? 0) >= 10 && Int(range.length) == 0 {
        textField.text = (currentTxtStr as NSString?)?.substring(to: 10)
        textField.resignFirstResponder()
        return false // return NO to not change text
    }
    return true
   }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        lbl91.textColor =  UIColor.black
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let txt = txtMobile.text {
            lbl91.textColor = txt.isEmpty ?  UIColor(red: 153.0/255.0, green:  153.0/255.0, blue:  153.0/255.0, alpha: 1) : UIColor.black
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true 
    }
}

