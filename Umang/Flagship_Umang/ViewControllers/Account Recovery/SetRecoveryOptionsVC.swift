//
//  SetRecoveryOptionsVC.swift
//  UMANG
//
//  Created by Rashpinder on 27/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit



class SetRecoveryOptionsVC: UIViewController
{
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblViewTitle: UILabel!
    @IBOutlet weak var btnHelp: UIButton!
    @IBOutlet weak var tblRecovery:UITableView!
    @IBOutlet weak var headerView:UIView!
    enum MPINBoxType :String {
        case email = "email"
        case alternateMobile = "mobile"
        case securityQstn = "security"
        case non = "non"
    }
    var typeOfRecovery :RecoveryType = .non
    var arrRecovery :[RecoveryOptions] = [ .alternateMobile, .email,.securityQstn]
    @IBOutlet weak var headerTitle: UILabel!
    
    var hud = MBProgressHUD()
    var email            = ""
    var alternateMobile  = ""
    var securityQuestion = ""
    var recoveryMPINBox = ""
    var tagComingFrom = ""
    var emailVerified = Bool()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //self.tblRecovery.reloadData()
        
        self.hitAPIToGetRecoveryOptions()
        self.setLocalized()
        
        tblRecovery.tableFooterView = UIView.init(frame: .zero)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func showAlert(msg:String)
    {
        let alert = UIAlertView(title: "error".localized, message: msg, delegate: nil, cancelButtonTitle:"ok".localized) //UIAlertView(title:"error".localized, message: "enter_correct_phone_number".localized, delegate: nil, cancelButtonTitle: "ok".localized, otherButtonTitles: "")
        alert.show()
    }
    
    func checkRecoveryType()  {
        let type : MPINBoxType = MPINBoxType.init(rawValue: self.recoveryMPINBox) ?? .non
        switch type {
        case .email:
            self.pushToEmailVC()
            break
        case .alternateMobile:
            break
        case .securityQstn:
            break
        default:
            break
        }
    }
    func hitAPIToGetRecoveryOptions()
    {
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        
        hud.label.text = NSLocalizedString("loading", comment: "")
        let dictBody = NSMutableDictionary()
        
        dictBody["tkn"]  = SharedManager.sharedSingleton()?.user_tkn
        dictBody["mno"]  = ""
        dictBody["peml"] = ""
        
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_GET_RECOVERY, withBody: dictBody, andTag: TAG_REQUEST_GET_RECOVERY, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            self?.hud.hide(animated: true)
            
            if error == nil && response != nil
            {
                if let jsonResponse = response as? [String:Any]
                {
                    if (jsonResponse.string(key: "rs") == API_SUCCESS_CASE) || (jsonResponse.string(key: "rs") == API_SUCCESS_CASE1)
                    {
                        self?.email  = jsonResponse["email"] as! String
                        self?.alternateMobile   = jsonResponse["amno"]  as! String
                        self?.securityQuestion = jsonResponse["quesid"] as! String
                        let emailVer = jsonResponse["emailflag"] as! String
                        
                        self?.emailVerified = emailVer.toBool() ?? false
                        
                        self?.tblRecovery.reloadData()
                    }
                    
                }
            }
            else {
                self?.showAlert(msg: error!.localizedDescription)
            }
        })
    }
    
    func setLocalized()
    {
        self.headerTitle.text = "option_help_text".localized
        headerTitle.font = AppFont.regularFont(14)
        
        lblViewTitle.text = "account_recovery_options".localized.capitalized
        lblViewTitle.font = AppFont.semiBoldFont(17)
    }
    
    
    @IBAction func didTapBtnHelpAction(_ sender: UIButton)
    {
        
        
    }
    @objc func didTapBtnSetOrChangeAction(_ sender: UIButton)
    {
        if sender.tag == 2
        {
            app_del.pushToUpdateSecurity(fromSetRecovery: self)
            
        }
        else if sender.tag == 0
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var vc = storyboard.instantiateViewController(withIdentifier: "AlternateMobileVC") as! AlternateMobileVC
            
            if UIScreen.main.bounds.size.height == 1024
            {
                vc = AlternateMobileVC.init(nibName: "AlternateMobile_iPad", bundle: nil)
            }
            
            
            if alternateMobile.isEmpty
            {
                vc.titletopass = "add_alt_mob_num".localized
            }
            else
            {
                vc.titletopass = "update_alt_mob_num".localized
            }
            

            if tagComingFrom == "UPDATEFROMSECURITY_MPINBOX"
            {
                vc.tagtopass   = "UPDATEFROMSECURITY_MPINBOX"

            }
            else
            {
                vc.tagtopass   = "ISFROMPROFILEUPDATE"
            }
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if sender.tag == 1
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let addEmail = storyboard.instantiateViewController(withIdentifier: "AddEmailVC") as! AddEmailVC
            
            if email.isEmpty
            {
                addEmail.titletopass = "add_email_address".localized
            }
            else
            {
                addEmail.titletopass = "update_email_address".localized
            }
            
            
            if tagComingFrom == "UPDATEFROMSECURITY_MPINBOX"
            {
                addEmail.tagtopass   = "UPDATEFROMSECURITY_MPINBOX"
                
            }
            else
            {
                addEmail.tagtopass   = "UPDATEEMAILFROMSECURITY"
            }
          //  addEmail.tagtopass   = "UPDATEEMAILFROMSECURITY"
            
            self.navigationController?.pushViewController(addEmail, animated: true)
        }
    }
    
    func pushToEmailVC()  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let addEmail = storyboard.instantiateViewController(withIdentifier: "AddEmailVC") as! AddEmailVC
        
        if email.isEmpty
        {
            addEmail.titletopass = "add_email_address".localized
        }
        else
        {
            addEmail.titletopass = "update_email_address".localized
        }
        
        addEmail.tagtopass   = "UPDATEEMAILFROMSECURITY"
        
        self.navigationController?.pushViewController(addEmail, animated: true)
    }
    @IBAction func didTapBackBtnAction(_ sender: UIButton)
    {
        if tagComingFrom == "UPDATEFROMSECURITY_MPINBOX" //  if tagComingFrom == "dialogBox"
        {
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @objc func didTapBtnMoreOptionsAction(_ sender: UIButton)
    {
        if sender.tag == 2
        {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "update".localized, style: .default , handler:{ (UIAlertAction)in
                
                app_del.pushToUpdateSecurity(fromSetRecovery: self)
            }))
            
            alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel , handler:nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        else if sender.tag == 0
        {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "update".localized, style: .default , handler:{ (UIAlertAction)in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                var vc = storyboard.instantiateViewController(withIdentifier: "AlternateMobileVC") as! AlternateMobileVC
                
                if UIScreen.main.bounds.size.height == 1024
                {
                    vc = AlternateMobileVC.init(nibName: "AlternateMobile_iPad", bundle: nil)
                }
                
                vc.titletopass = "update_alt_mob_num".localized
                
                if self.tagComingFrom == "UPDATEFROMSECURITY_MPINBOX"
                {
                    vc.tagtopass   = "UPDATEFROMSECURITY_MPINBOX"
                    
                }
                else
                {
                    vc.tagtopass   = "ISFROMPROFILEUPDATE"
                }
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }))
            
            alert.addAction(UIAlertAction(title: "remove".localized, style: .default , handler:{ (UIAlertAction)in
                
                
                let confirmAlert = UIAlertController(title: "sure_altmobile".localized, message: "altmobile_help_recover".localized, preferredStyle: .alert)
                
                confirmAlert.addAction(UIKit.UIAlertAction(title: "remove".localized, style: .default , handler:{ (UIAlertAction)in
                    
                    let user_dic = NSMutableDictionary()
                    user_dic["amno"] = ""
                    user_dic["Tag"]   = "UPDATEFROMSECURITY"
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let updMPinVC = storyboard.instantiateViewController(withIdentifier: "UpdMpinVC") as! UpdMpinVC
                    
                    updMPinVC.dic_info=user_dic
                    
                    self.navigationController?.pushViewController(updMPinVC, animated: true)
                }))
                
                confirmAlert.addAction(UIKit.UIAlertAction(title: "cancel".localized, style: .cancel , handler:nil))
                
                self.present(confirmAlert, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel , handler:{ (UIAlertAction)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else if sender.tag == 1
        {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "update".localized, style: .default , handler:{ (UIAlertAction)in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let addEmail = storyboard.instantiateViewController(withIdentifier: "AddEmailVC") as! AddEmailVC
                
                addEmail.titletopass = "update_email_address".localized
                
                
                if self.tagComingFrom == "UPDATEFROMSECURITY_MPINBOX"
                {
                    addEmail.tagtopass   = "UPDATEFROMSECURITY_MPINBOX"
                    
                }
                else
                {
                    addEmail.tagtopass   = "UPDATEEMAILFROMSECURITY"
                }
                
                self.navigationController?.pushViewController(addEmail, animated: true)
            }))
            
            alert.addAction(UIAlertAction(title: "remove".localized, style: .default , handler:{ (UIAlertAction)in
                
                
                let confirmAlert = UIAlertController(title: "sure_email".localized, message: "email_help_recover".localized, preferredStyle: .alert)
                
                confirmAlert.addAction(UIKit.UIAlertAction(title: "remove".localized, style: .default , handler:{ (UIAlertAction)in
                    
                    let user_dic = NSMutableDictionary()
                    user_dic["email"] = ""
                    user_dic["Tag"]   = "UPDATEEMAILFROMSECURITY"
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let updMPinVC = storyboard.instantiateViewController(withIdentifier: "UpdMpinVC") as! UpdMpinVC
                    
                    updMPinVC.dic_info=user_dic
                    
                    self.navigationController?.pushViewController(updMPinVC, animated: true)
                }))
                
                confirmAlert.addAction(UIKit.UIAlertAction(title: "cancel".localized, style: .cancel , handler:nil))
                
                self.present(confirmAlert, animated: true, completion: nil)
                
                
            }))
            
            alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel , handler:{ (UIAlertAction)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func emailVerifyOption()
    {
        
        let alert = UIAlertController(title: "Your email address is not verified", message: "resend_email_heading_desp".localized, preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: "yes".localized.capitalized, style: .default , handler:{ (UIAlertAction)in
            
            self.hitEmailResendAPI()
            
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel , handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func hitEmailResendAPI()
    {
        
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        
        hud.label.text = NSLocalizedString("loading", comment: "")
        let dictBody = NSMutableDictionary()
        
        dictBody["tkn"]  = SharedManager.sharedSingleton()?.user_tkn
        dictBody["mno"]  = SharedManager.sharedSingleton()?.mobileNumber
        dictBody["peml"] = ""
        dictBody["email"] = email
        
        
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_RESENDEMAILVERIFY, withBody: dictBody, andTag: TAG_REQUEST_RESENDEMAILVERIFY, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            self?.hud.hide(animated: true)
            
            if error == nil && response != nil
            {
                if let jsonResponse = response as? [String:Any]
                {
                    if (jsonResponse.string(key: "rs") == API_SUCCESS_CASE) || (jsonResponse.string(key: "rs") == API_SUCCESS_CASE1)
                    {
                        
                        let rd = jsonResponse.string(key: "rd")
                        
                        let alert = UIAlertController(title: "success".localized.capitalized, message: rd, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "ok".localized, style: .cancel , handler:{ (UIAlertAction)in
                            
                            
                        }))
                        
                        self?.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
            else {
                self?.showAlert(msg: error!.localizedDescription)
            }
        })
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
extension SetRecoveryOptionsVC :UITableViewDataSource , UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrRecovery.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tblRecovery.dequeueReusableCell(withIdentifier: "CellSecurityOptions", for: indexPath) as! CellSecurityOptions
        let option = arrRecovery[indexPath.row]
        cell.backgroundColor = .clear
        cell.btnCellOptions.tag = indexPath.row
        cell.btnCellOptions.addTarget(self, action: #selector(didTapBtnSetOrChangeAction(_:)), for: .touchUpInside)
        cell.viewWithTag(44555)?.isHidden = true
        if indexPath.row == arrRecovery.count - 1 {
            cell.viewWithTag(44555)?.isHidden = false
        }
        
        cell.btnMoreOption.isHidden = true
        cell.btnCellOptions.isHidden = false;
        cell.arrowHeightConstraint.constant = 17
        cell.btnSetOptionHeightConstraint.constant = 35
        cell.dividerHeightConstraint.constant = 1
        cell.descriptionBottomConstraint.constant = 12
        cell.lblSubtitleLeadingConstraint.constant = 19
        
        cell.btnMoreOption.tag = indexPath.row
        cell.btnMoreOption.addTarget(self, action: #selector(didTapBtnMoreOptionsAction(_:)), for: .touchUpInside)
        
        cell.btnEmailVerify.isHidden = true
        cell.btnEmailVerify.addTarget(self, action: #selector(emailVerifyOption), for: .touchUpInside)
        
        switch option
        {
        case .email:
            
            cell.lblTitle.text    = "email_addrss".localized
            cell.imageLeft.image = UIImage(named: "email_recovery")
            
            if email.isEmpty
            {
                cell.lblSubTitle.text = "add_email_recover".localized
                cell.btnCellOptions.setTitle("set_email".localized.capitalized, for: .normal)
                
            }
            else
            {
                cell.lblSubTitle.text = email
                cell.btnCellOptions.setTitle("change_email".localized.capitalized, for: .normal)
                
                cell.btnMoreOption.isHidden = false
                cell.arrowHeightConstraint.constant = 0
                cell.btnSetOptionHeightConstraint.constant = 0
                cell.dividerHeightConstraint.constant = 0
                cell.btnCellOptions.isHidden = true;
                cell.descriptionBottomConstraint.constant = 0
                
                if !self.emailVerified
                {
                    cell.btnEmailVerify.isHidden = false
                    cell.lblSubtitleLeadingConstraint.constant = 42
                }
                else
                {
                    cell.btnEmailVerify.isHidden = true
                    cell.lblSubtitleLeadingConstraint.constant = 19
                }
            }
            
        case .alternateMobile:
            
            cell.lblTitle.text    = "alt_mob_num".localized
            cell.imageLeft.image = UIImage(named: "mobile_recovery")
            
            if alternateMobile.isEmpty
            {
                cell.lblSubTitle.text = "add_altmobile_recover".localized
                cell.btnCellOptions.setTitle("set_altmobile".localized.capitalized, for: .normal)
                
            }
            else
            {
                cell.lblSubTitle.text = alternateMobile
                cell.btnCellOptions.setTitle("change_altmobile".localized.capitalized, for: .normal)
                
                cell.btnMoreOption.isHidden = false
                cell.arrowHeightConstraint.constant = 0
                cell.btnSetOptionHeightConstraint.constant = 0
                cell.dividerHeightConstraint.constant = 0
                cell.btnCellOptions.isHidden = true;
                cell.descriptionBottomConstraint.constant = 0
            }
            
        case .securityQstn:
            
            cell.lblTitle.text    = "security_ques_text".localized
            cell.imageLeft.image = UIImage(named: "security_que_recovery")
            
            if securityQuestion.isEmpty
            {
                cell.lblSubTitle.text = "set_security_recover".localized
                cell.btnCellOptions.setTitle("set_security".localized.capitalized, for: .normal)
            }
            else
            {
                
                cell.lblSubTitle.text = "change_security_quest_text".localized
                cell.btnCellOptions.setTitle("change_security_question".localized.capitalized, for: .normal)
                
                cell.btnMoreOption.isHidden = false
                cell.arrowHeightConstraint.constant = 0
                cell.btnSetOptionHeightConstraint.constant = 0
                cell.dividerHeightConstraint.constant = 0
                cell.btnCellOptions.isHidden = true;
                cell.descriptionBottomConstraint.constant = 0
            }
            
        default: break
            
        }
        
        cell.lblTitle.font       = AppFont.mediumFont(17)
        cell.lblSubTitle.font    = AppFont.regularFont(15)
        cell.btnCellOptions.titleLabel?.font = AppFont.regularFont(16)
        cell.btnCellOptions.contentHorizontalAlignment = .left
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        self.headerView.backgroundColor = tableView.backgroundColor
        return self.headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 70
    }
}

