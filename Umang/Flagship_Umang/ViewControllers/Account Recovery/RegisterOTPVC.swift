//
//  RegisterOTPVC.swift
//  UMANG
//
//  Created by Rashpinder on 25/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit


enum OtpType {
    case otp
    case call
}
class RegisterOTPVC: UIViewController
{
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var headerDescriptionLabel: UILabel!
    @IBOutlet var otpTextfield: UITextField!
    @IBOutlet var waitingOTPLabel: UILabel!
    
    @IBOutlet var timerLabel: UILabel!
    
    @IBOutlet weak var topNextBtnConstraint: NSLayoutConstraint!
    @IBOutlet var resendOTPView: UIView!
    @IBOutlet var resendOTPHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingWaitingSmsConstraint: NSLayoutConstraint!

    @IBOutlet var resendOTPButton: UIButton!
    
    @IBOutlet var callMeButton: UIButton!
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet weak var btnChangeMobile: UIButton!
    var otpTimer = Timer()
    
    @IBOutlet weak var lblMobileNumber: UILabel!
    var verifyType :OptVerifyType = .recoverMPIN
    @IBOutlet weak var loaderSpinner: JTMaterialSpinner!

    var count :Float = 0
    var retryOTPString  = String()
    var retryCallString = String()
    var tout :Float = 0
    var rtry  = "29|30"
    
    var mobileVerify = ""
    var email = ""
    var qtsnIDS = ""
    var alterMobile = ""
    @IBOutlet weak var lblEnterOptMSg: UILabel!
    
    var isFromLogin = "no"
    var hud = MBProgressHUD()
    
    @IBOutlet weak var progressBar: UIProgressView!
    
    // MARK:- View Methods
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        count = tout
        
        if otpTimer.isValid
        {
            otpTimer.invalidate()
        }
        let profress = count / tout
        
        self.progressBar.progress = profress
        otpTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(updateUI(_:)), userInfo: nil, repeats: true)
        RunLoop.main.add(otpTimer, forMode: .commonModes)
        
        self.setHTMLStringForOTP()
        
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.otpTextfield.keyboardType = .numberPad
        waitingOTPLabel.text = "waiting_for_otp".localized
        nextButton.setTitle("next".localized, for: .normal)
        nextButton.setTitleColor(UIColor.white, for: .normal)
        nextButton.layer.cornerRadius = 3.0;
        nextButton.layer.masksToBounds = true
        backButton.setTitle("back".localized, for: .normal)
        resendOTPButton.setTitle("resend_otp".localized, for: .normal)
        callMeButton.setTitle("call_me".localized, for: .normal)
        callMeButton.titleLabel?.numberOfLines = 5;
        callMeButton.titleLabel?.lineBreakMode = .byWordWrapping
        
        resendOTPButton.titleLabel?.numberOfLines = 5;
        resendOTPButton.titleLabel?.lineBreakMode = .byWordWrapping
        lblEnterOptMSg.text = "enter_6_digit_otp".localized
        
        let stringArray = self.rtry.components(separatedBy:"|")
            
        self.retryOTPString  = stringArray[0]
        self.retryCallString = stringArray[1]
        otpTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.layoutCornerRadius(view: self.nextButton, radius: 5)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(didTapView))
        
        self.view.addGestureRecognizer(tapGesture)
        
        self.showRetyView(hide: true)
        self.setFontSize()
        self.enableBtnNext(status: false)
        loaderSpinner.circleLayer.lineWidth = 4.0
        loaderSpinner.circleLayer.strokeColor = blueColor.cgColor
        self.hideLoader(hide: false)
        
        otpTextfield.becomeFirstResponder()
    }
    
    func setHTMLStringForOTP()
    {
        
        var htmlString = String(format: NSLocalizedString("OTP_sent_via_SMS", comment: ""), mobileVerify)
        
        
        defer {
        }
        
           htmlString =   htmlString + (String(format: "<style>body{font-family: '%@'; font-size:%fpx;}</style>", headerDescriptionLabel.font.fontName , headerDescriptionLabel.font.pointSize))
       
        
        
        
        
        var attrStr: NSAttributedString? = nil
        if let anEncoding = htmlString.data(using: String.Encoding(rawValue: String.Encoding.unicode.rawValue)) {
            attrStr = try? NSAttributedString(data: anEncoding, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        headerDescriptionLabel.attributedText = attrStr
        headerDescriptionLabel.isUserInteractionEnabled = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.btnChangeNumber(sender:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        headerDescriptionLabel.addGestureRecognizer(tapGestureRecognizer)
        headerDescriptionLabel.lineBreakMode = .byWordWrapping
        
    }
   @objc func btnChangeNumber(sender :UITapGestureRecognizer)  {
    self.dismiss(animated: true, completion: nil)

    }
    func setFontSize()  {
        otpTextfield.font = AppFont.regularFont(20)
        titleLabel.font = AppFont.semiBoldFont(17)
        self.headerDescriptionLabel.font = AppFont.mediumFont(17)
        self.lblMobileNumber.font = AppFont.semiBoldFont(17)
        self.btnChangeMobile.titleLabel?.font = AppFont.mediumFont(17)
        self.waitingOTPLabel.font = AppFont.regularFont(17)
        self.timerLabel.textColor = blueColor
        self.timerLabel.font = AppFont.regularFont(17)
        self.btnChangeMobile.setTitle("change".localized.uppercased(), for: .normal)
        //change
        self.nextButton.setTitle("next".localized, for: .normal)
        self.nextButton.titleLabel?.font = AppFont.mediumFont(19)
        self.backButton.titleLabel?.font = AppFont.regularFont(17.0)
        titleLabel.text = "verify_otp_text".localized
        self.backButton.setTitle("back".localized, for: .normal)
        headerDescriptionLabel.text = String(format: "otp_sent_via_sms".localized, "")

//        switch verifyType {
//        case .alterMobile:
//            headerDescriptionLabel.text = String(format: "otp_sent_verification".localized, alterMobile)
//        case .email:
//            headerDescriptionLabel.text = String(format: "otp_sent_verification".localized, email)
//        default:
//
//        }
       // lblMobileNumber.text = mobileVerify
        
    }
    @IBAction func changeMobileNumberTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func didTapView()
    {
        self.view.endEditing(true)
    }
    
    @objc func updateUI(_ sender: Timer?)
    {
        if count <= 0
        {
            otpTimer.invalidate()
            self.showRetyView(hide: false)
            if loaderSpinner.isAnimating {
                self.hideLoader(hide: true)
            }
        }
        else
        {
            count = count - 1
            let profress = count / tout
            
            self.progressBar.progress = profress
            
            timerLabel.text = self.formatCounterTime(totalSeconds: NSInteger(count))
            if !loaderSpinner.isAnimating {
                self.hideLoader(hide: false)
            }
            
        }
    }
    func hideLoader(hide:Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.leadingWaitingSmsConstraint.constant = hide ? 20.0 : 55.0;
            if hide {
                self?.loaderSpinner.endRefreshing()
                self?.loaderSpinner.isHidden = true
            }else {
                self?.loaderSpinner.beginRefreshing()
                self?.loaderSpinner.isHidden = false
            }
        }
        
    }
    func showRetyView(hide:Bool)  {
        self.showRtryOptions()
        self.resendOTPView.isHidden = hide
        resendOTPHeightConstraint.constant = hide ? 0.0: 55.0
        topNextBtnConstraint.constant = hide ? 40: 100.0
        
        callMeButton.isHidden = hide
        resendOTPButton.isHidden = hide
        let resendOTPInt  = NSInteger(retryOTPString)
        let resendCallInt = NSInteger(retryCallString)
        if resendOTPInt ?? 0 <= 0
        {
            resendOTPButton.isHidden = true
        }
        if resendCallInt ?? 0 <= 0
        {
            callMeButton.isHidden = true
        }
        if resendOTPInt ?? 0 <= 0 && resendCallInt ?? 0 <= 0
        {
            resendOTPHeightConstraint.constant = 0
            self.resendOTPView.isHidden = true
        }
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
    }
    
    func formatCounterTime(totalSeconds : NSInteger) -> String
    {
        let seconds = totalSeconds % 60
        let minutes = (totalSeconds / 60) % 60
        
        return String(format: "%02d:%02d", minutes,seconds)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resendOTPButtonAction(_ sender: UIButton)
    {
        otpTextfield.text = ""
        ///  resendOTPHeightConstraint.constant = 55
        self.showRetyView(hide: true)
        self.hideLoader(hide: false)

        // self.hitAPIResendOTPwithIVR(channelType: "sms")
        switch self.verifyType {
        case .recoverMPIN:
            self.hitAPIResendOTPwithIVR(channelType: "sms")
        case .alterMobile:
            self.hitSendOTP_AlternateMobile_EmailAPI(chanel: "sms")
        case .email:
            self.hitSendOTP_AlternateMobile_EmailAPI(chanel: "sms")
        default:
            break
        }
    }
    
    
    
    @IBAction func callMeButtonAction(_ sender: UIButton)
    {
        otpTextfield.text = ""
        ///  resendOTPHeightConstraint.constant = 55
        self.showRetyView(hide: true)
        self.hideLoader(hide: false)

        // self.hitAPIResendOTPwithIVR(channelType: "sms")
        switch self.verifyType {
        case .recoverMPIN:
            self.hitAPIResendOTPwithIVR(channelType: "ivr")
        case .alterMobile:
            self.hitSendOTP_AlternateMobile_EmailAPI(chanel: "ivr")
        case .email:
            self.hitSendOTP_AlternateMobile_EmailAPI(chanel: "ivr")
        default:
            break
        }
        
    }
    
    func enableBtnNext(status : Bool)
    {
        self.nextButton.isUserInteractionEnabled = status
        if status
        {
            self.view.endEditing(true)
            self.nextButton.backgroundColor = UIColor(red: 84.0/255.0, green: 185.0/255.0, blue: 105.0/255.0, alpha: 1.0)
        }
        else
        {
            self.nextButton.backgroundColor = UIColor(red: 176.0/255.0, green: 176.0/255.0, blue: 176.0/255.0, alpha: 1.0)
        }
    }
    
    func isValidOTP(otpString : String) -> Bool
    {
        let strMatchstring = "\\b([0-9%_.+\\-]+)\\b"
        let predicate = NSPredicate(format: "SELF MATCHES %@", strMatchstring)
        
        if !predicate.evaluate(with: otpString)
        {
            return false
        }
        
        return true
    }
    
    func checkValidation()
    {
        if (otpTextfield.text?.count)! < 6
        {
            let alert = UIAlertController(title: "error".localized, message: "register_verify_otp_heading".localized, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if !self.isValidOTP(otpString: otpTextfield.text!)
        {
            let alert = UIAlertController(title: "error".localized, message: "register_verify_otp_heading".localized, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    
    @objc func textFieldDidChange(_ sender : UITextField)
    {
        if (sender.text?.count)! >= 6
        {
            let mySubstring = sender.text!.prefix(6)
            otpTextfield.text = (mySubstring as NSString).substring(to: 6)
            
            sender.resignFirstResponder()
            self.enableBtnNext(status: true)
            
        }
        else
        {
            self.enableBtnNext(status: false)
        }
    }
    func showRtryOptions()  {
        let attemptsLeft = "attempts_left".localized
        let btnOtpResnd = getAttributedString(first: "resend_otp".localized, second: "(\(self.retryOTPString) \(attemptsLeft))")
        self.resendOTPButton.setAttributedTitle(btnOtpResnd, for: .normal)
        let btnCallResnd = getAttributedString(first: "call_me".localized, second: "(\(self.retryCallString) \(attemptsLeft))")
        self.callMeButton.setAttributedTitle(btnCallResnd, for: .normal)
    }
    func getAttributedString(first:String,second:String) -> NSMutableAttributedString {
        let myAttribute = [NSForegroundColorAttributeName: UIColor.black,  NSFontAttributeName:AppFont.regularFont(15)] as [String : Any]
        let firstAttributed = NSMutableAttributedString(string: first, attributes: myAttribute)//NSAttributedString(string: first, attributes:myAttribute )
        
        let myAttribute2 = [NSForegroundColorAttributeName: UIColor(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1),  NSFontAttributeName:AppFont.lightFont(11)] as [NSAttributedStringKey : Any]
        let secondAttributed = NSAttributedString(string: "\n \(second)", attributes: myAttribute2 as [String : Any])
        firstAttributed.append(secondAttributed)
        return firstAttributed
    }
    
    
    // MARK:- IVR API Call
    
    func hitAPIResendOTPwithIVR(channelType : String)
    {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Wait..."
        
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        dictBody["mno"] = mobileVerify //Enter mobile number of user
        dictBody["chnl"] = channelType //chnl : type sms for OTP and IVR for call
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = "rgtmob"
        
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_IVR_OTP, withBody: dictBody, andTag: TAG_REQUEST_IVR_OTP) { (response, error, tag) in
            
            self.hud.hide(animated: true)
            
            if error == nil && response != nil
            {
                if let jsonResponse = response as? [String:Any]
                {
                    let node = jsonResponse["node"] as? String ?? ""
                    if  node.count > 0
                    {
                        UserDefaults.standard.setValue(node, forKey: "NODE_KEY")
                        UserDefaults.standard.synchronize()
                    }
                    if let pdData = jsonResponse["pd"] as? [String:Any]
                    {
                        
                        if (jsonResponse.string(key: "rs") == API_SUCCESS_CASE) || (jsonResponse.string(key: "rs") == API_SUCCESS_CASE1)
                        {
                            self.tout = Float(pdData["tout"] as! String) ?? 0
                            self.rtry = pdData["rtry"] as! String
                            
                            let stringArray = self.rtry.components(separatedBy:"|")
                            self.retryOTPString  = stringArray[0]
                            self.retryCallString = stringArray[1]
                            self.showRetyView(hide: true)
                            self.count = self.tout
                            if self.otpTimer.isValid
                            {
                                self.otpTimer.invalidate()
                            }
                            
                            self.otpTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.updateUI(_:)), userInfo: nil, repeats: true)
                            
                            RunLoop.main.add(self.otpTimer, forMode: .commonModes)
                        }
                    }
                }
            }
            else
            {
                self.showAlert(msg: error!.localizedDescription)
            }
            
            
        }
    }
    
    // MARK:- Verify OTP API Call
    
    func hitOTPVerifyAPI()
    {
        let otpString = otpTextfield.text
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "loading".localized
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        SharedManager.sharedSingleton()!.mobileNumber  = mobileVerify
        dictBody["mno"] = mobileVerify //Enter mobile number of user
        dictBody["chnl"] = "sms" //chnl : type sms for OTP and IVR for call
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = "rgtmob"
        dictBody["otp"] = otpString
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_VALIDATE_OTP2, withBody: dictBody, andTag: TAG_REQUEST_VALIDATE_OTP, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            
            self?.hud.hide(animated: true)
            
            if error == nil && response != nil
            {
                if let data = response as? NSDictionary
                {
                    let rs = data["rs"] as! String
                    
                    if rs == API_SUCCESS_CASE || rs == API_SUCCESS_CASE1
                    {
                        //OPEN NEXT VIEW
                        self?.processVerifyResponse(response:data )
                    }
                    else
                    {
                        self?.otpTextfield.text = ""
                    }
                }
                
            }
            else {
                if let localError = error?.localizedDescription {
                    
                    let alert = UIAlertController(title: "error".localized, message: localError, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
               
                
                self?.enableBtnNext(status: false)
                
                
            }
            
            
            
        })
    }
    
    func processVerifyResponse(response:NSDictionary) {
        
        guard  var pd =  response.value(forKey: "pd") as? [String:Any] else {
            self.showAlert(msg: "error".localized)
            self.enableBtnNext(status: false)
            return
        }
        pd = pd.formatDictionaryForNullValues(pd) ?? ["":""]
        SharedManager.sharedSingleton().user_tkn = pd.string(key: "tkn")
        //-------- Add later For handling mpinflag / mpinmand ----------
         SharedManager.sharedSingleton().shared_mpinflag = pd.string(key: "mpinflag")
         SharedManager.sharedSingleton().shared_mpinmand = pd.string(key: "mpinmand")

        let mpindial = pd.string(key: "mpindial")
        let mpinFlag = pd.string(key: "mpinflag")

         var recFlag = ""
        if let genPD = pd.pdJSON(key: "generalpd")  {
            recFlag = genPD.string(key: "recflag")
            UserDefaults.standard.setValue(recFlag, forKey: "recflag")
        }

        UserDefaults.standard.setValue(mpindial, forKey: "mpindial")

        if SharedManager.sharedSingleton().shared_mpinflag.count == 0 {
            SharedManager.sharedSingleton().shared_mpinflag = "TRUE"
        }
        if  SharedManager.sharedSingleton().shared_mpinmand.count == 0 {
            SharedManager.sharedSingleton().shared_mpinmand = "TRUE"
        }
    UserDefaults.standard.setValue(SharedManager.sharedSingleton().shared_mpinflag, forKey: "mpinflag")
    UserDefaults.standard.setValue(SharedManager.sharedSingleton().shared_mpinmand, forKey: "mpinmand")
        //[[NSUserDefaults standardUserDefaults] synchronize];
        //-------- Add later For handling mpinflag / mpinmand ----------
        // check here for login with registration here
        UserDefaults.standard.setValue("YES", forKey: "isLoginWithRegistration")
        UserDefaults.standard.synchronize()
        // check here for login with registration here
        UserDefaults.standard.setValue("YES", forKey: "showLoginOTP_Hint")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.setAESKey("UMANGIOSAPP")
        // Encrypt
        //  The converted code is limited to 2 KB.
        //  Upgrade your plan to remove this limitation.
        //
        UserDefaults.standard.encryptValue(SharedManager.sharedSingleton().user_tkn, withKey: "TOKEN_KEY")
        SharedManager.sharedSingleton().lastFetchDate = "NR"
        UserDefaults.standard.encryptValue(SharedManager.sharedSingleton().lastFetchDate, withKey: "lastFetchDate")
        UserDefaults.standard.synchronize()
        SharedManager.sharedSingleton().objUserProfile = nil
        SharedManager.sharedSingleton().objUserProfile = ProfileDataBO(response: response["pd"])
        var defaults = UserDefaults.standard
        defaults.setAESKey("UMANGIOSAPP")
        defaults.encryptValue("", withKey: "CITY_KEY")
        defaults.encryptValue("", withKey: "ALTERMB_KEY")
        defaults.encryptValue("", withKey: "QUALI_KEY")
        defaults.encryptValue("", withKey: "OCCUP_KEY")
        defaults.encryptValue("", withKey: "EMAIL_KEY")
        defaults.encryptValue("", withKey: "URLPROFILE_KEY")
        defaults.encryptValue("", withKey: "MOBILE_KEY")
        defaults.encryptValue("", withKey: "NAME_KEY")
        defaults.encryptValue("", withKey: "GENDER_KEY")
        defaults.encryptValue("", withKey: "STATE_KEY")
        defaults.encryptValue("", withKey: "DISTRICT_KEY")
        defaults.encryptValue("", withKey: "DOB_KEY")
        defaults.synchronize()
        SharedManager.sharedSingleton().profilestateSelected = ""
        SharedManager.sharedSingleton().profileNameSelected = ""
        SharedManager.sharedSingleton().notiTypeGenderSelected = ""
        SharedManager.sharedSingleton().profileDOBSelected = ""
        SharedManager.sharedSingleton().notiTypDistricteSelected = ""
        SharedManager.sharedSingleton().profileUserAddress = ""
        SharedManager.sharedSingleton().altermobileNumber = ""
        SharedManager.sharedSingleton().profileEmailSelected = ""
        UserDefaults.standard.setAESKey("UMANGIOSAPP")
        UserDefaults.standard.encryptValue("YES", withKey: "SHOW_PROFILEBAR")
        UserDefaults.standard.synchronize()

        let value = UserDefaults.standard.decryptedValue(forKey: "SHOW_PROFILEBAR")
        
        self.pushToTabBar()
        UserDefaults.standard.setValue(NSDate(), forKey: RECOVERY_BOX_DATE)
        UserDefaults.standard.set(0, forKey: RECOVERY_BOX_COUNT)
       /* weak var delegate = app_del
        DispatchQueue.main.async {[weak self] in
            delegate?.showHideSetMpinAlertBox()
        }*/
        
        weak var delegate = app_del
        if mpinFlag.uppercased() == "T" || mpinFlag.uppercased() == "TRUE" {
            if (recFlag.uppercased() != "T" || recFlag.uppercased() != "TRUE" )
            {
                DispatchQueue.main.async {
                    delegate?.checkRecoveryOptionBOXShownStatus("loginwithOTPRegister")
                }
            }
        }
        else {
            DispatchQueue.main.async
                {
                    delegate?.showHideSetMpinAlertBox()
            }
            delegate?.checkRecoveryOptionBOXShownStatus("loginwithOTPRegister")

        }
        
       /*
        
         dispatch_async(dispatch_get_main_queue(),^{
         AppDelegate delegate = (AppDelegate)[UIApplication sharedApplication].delegate;
         //[delegate showHideSetMpinAlertBox ];
         if(([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"T"])||([[singleton.shared_mpinflag uppercaseString] isEqualToString:@"TRUE"]))
         {
         if(!([[recflag uppercaseString] isEqualToString:@"T"]||[[recflag uppercaseString] isEqualToString:@"TRUE"]))
         {
         [delegate checkRecoveryOptionBOXShownStatus];
         }
         }
         else
         {
         
         [delegate showHideSetMpinAlertBox ];
         }
         });
 
 
 */
        
        
        
        
        
        
        
    }
    func pushToTabBar()  {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var tbc = storyboard.instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController
        tbc?.selectedIndex = 0
        if let aTbc = tbc {
           self.present(aTbc, animated: false, completion: nil)
        }
    }
    @IBAction func didTapNextButtonAction(_ sender: UIButton) {
        
        //        self.pushToSetMPINVC(dicJson: nil)
        //        return
        self.hitOTPVerifyAPI()
       
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    func openRecoveryOption(dicJson:NSDictionary) {
        let email = dicJson.value(forKey: "email") as? String ?? ""
        let alterMobile = dicJson.value(forKey: "amno") as? String ?? ""
        let qtsnIDs = dicJson.value(forKey: "quesid") as? String ?? ""
        if "\(email)".isEmpty && "\(alterMobile)".isEmpty && "\(qtsnIDs)".isEmpty {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetMPINRecoveryVC") as! SetMPINRecoveryVC
            vc.mpinType = .register
            vc.mobileVerify = mobileVerify
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let recoverVC = self.storyboard?.instantiateViewController(withIdentifier: "RecoveryOptionsVC") as! RecoveryOptionsVC
            recoverVC.email = "\(email)"
            recoverVC.alterMobile = "\(alterMobile)"
            recoverVC.securityQtsn = "\(qtsnIDs)"
            recoverVC.mobileVerify = self.mobileVerify
            recoverVC.otpStr = self.otpTextfield.text!
            recoverVC.isFromLogin = isFromLogin
            self.navigationController?.pushViewController(recoverVC, animated: true)
        }
    }
    
    
}
extension RegisterOTPVC : UITextFieldDelegate
{
    // MARK:- Verify OTP API Call
    
    func hitSendOTP_AlternateMobile_EmailAPI(chanel:String)
    {
        let otpString = otpTextfield.text
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "loading".localized
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        dictBody["mno"] = mobileVerify //Enter mobile number of user
        dictBody["chnl"] = chanel //chnl : type sms for OTP and IVR for call
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = "frgtmpnamem"
        if verifyType == .alterMobile {
            dictBody["amno"] = self.alterMobile
            dictBody["amemflag"] = "M"
        }
        if verifyType == .email {
            dictBody["email"] = self.email
            dictBody["amemflag"] = "E"
        }
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_NEW_OTP, withBody: dictBody, andTag: TAG_REQUEST_VALIDATE_OTP, completionHandler: { (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            self.hud.hide(animated: true)
            if error == nil && response != nil
            {
                if let jsonResponse = response as? [String:Any]
                {
                    let node = jsonResponse["node"] as? String ?? ""
                    if  node.count > 0
                    {
                        UserDefaults.standard.setValue(node, forKey: "NODE_KEY")
                        UserDefaults.standard.synchronize()
                    }
                    if let pdData = jsonResponse["pd"] as? [String:Any]
                    {
                        
                        if (jsonResponse.string(key: "rs") == API_SUCCESS_CASE) || (jsonResponse.string(key: "rs") == API_SUCCESS_CASE1)
                        {
                            self.tout = Float(pdData["tout"] as! String) ?? 0
                            self.rtry = pdData["rtry"] as! String
                            
                            let stringArray = self.rtry.components(separatedBy:"|")
                            self.retryOTPString  = stringArray[0]
                            self.retryCallString = stringArray[1]
                            /// UPDATE BUTTON TEXTS
                            self.showRetyView(hide: true)
                            self.count = self.tout
                            let profress = self.count / self.tout
                            
                            self.progressBar.progress = profress
                            if self.otpTimer.isValid
                            {
                                self.otpTimer.invalidate()
                            }
                            
                            self.otpTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.updateUI(_:)), userInfo: nil, repeats: true)
                            
                            RunLoop.main.add(self.otpTimer, forMode: .commonModes)
                        }
                    }
                }
            }
            else
            {
                self.showAlert(msg: error!.localizedDescription)
                
            }
        })
    }
    func hitVerifyAlternate_Email_OTP_API(chanel:String)
    {
        let otpString = otpTextfield.text
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "loading".localized
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        dictBody["mno"] = mobileVerify //Enter mobile number of user
        dictBody["chnl"] = chanel //chnl : type sms for OTP and IVR for call
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = "frgtmpnamem"
        dictBody["otp"] = otpString
        if verifyType == .alterMobile {
            dictBody["amno"] = self.alterMobile
            dictBody["amemflag"] = "M"
        }
        if verifyType == .email {
            dictBody["email"] = self.email
            dictBody["amemflag"] = "E"
        }
        dictBody["otp"] = otpString
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_NEW_VALIDATEOTP, withBody: dictBody, andTag: TAG_REQUEST_VALIDATE_OTP, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            
            self?.hud.hide(animated: true)
            if error == nil && response != nil
            {
                if let data = response as? NSDictionary
                {
                    let rs = data["rs"] as! String
                    
                    if rs == API_SUCCESS_CASE || rs == API_SUCCESS_CASE1
                    {
                        //OPEN NEXT VIEW
                        self?.pushToSetMPINVC(dicJson: data)
                    }
                    else
                    {
                        self?.otpTextfield.text = ""
                        //self?.count = -1;
                        // self?.updateUI(nil)
                    }
                }
            }
            else
            {
                let alert = UIAlertController(title: "error".localized, message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
                self?.present(alert, animated: true, completion: nil)
                
                self?.enableBtnNext(status: false)
            }
        })
    }
    func pushToSetMPINVC(dicJson:NSDictionary?)  {
        var vc = self.storyboard?.instantiateViewController(withIdentifier: "SetMPINRecoveryVC") as! SetMPINRecoveryVC
        if verifyType == .alterMobile {
            vc.mpinType = .alterMobile
        }
        if verifyType == .email {
            vc.mpinType = .emailOtp
        }
        vc.mobileVerify = mobileVerify
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
