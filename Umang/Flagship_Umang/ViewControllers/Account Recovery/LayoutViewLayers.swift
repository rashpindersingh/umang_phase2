//
//  LayoutViewLayers.swift
//  UmangSwift
//
//  Created by admin on 08/08/17.
//  Copyright © 2017 Umang. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func layoutCornerRadius(radius:CGFloat)  {
        self.layer.cornerRadius = radius;
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    func layoutBorderWithCornerRadius( radius:CGFloat, borderColor:UIColor, borderWidth: CGFloat)  {
        self.layer.cornerRadius = radius;
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    func animate(_ animation: @autoclosure @escaping () -> Void,
                 duration: TimeInterval = 0.25) {
        UIView.animate(withDuration: duration, animations: animation)
    }
}

extension Optional {
    func unwrapOrThrow(_ errorExpression: @autoclosure () -> Error) throws -> Wrapped {
        guard let value = self else {
            throw errorExpression()
        }
        
        return value
    }
}
// MARK: - UIViewController Extention Methods 
extension UIViewController {
    
    func layoutCornerRadius(view:UIView, radius:CGFloat)  {
        view.layer.cornerRadius = radius;
        view.layer.masksToBounds = true
        view.clipsToBounds = true
    }
    func layoutBorderWithCornerRadius(view:UIView, radius:CGFloat, borderColor:UIColor, borderWidth: CGFloat)  {
        view.layer.cornerRadius = radius;
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.masksToBounds = true
        view.clipsToBounds = true
    }
    func addShadowToView(vwItem:UIView,color:UIColor)  {
        vwItem.layer.shadowOffset = CGSize(width:0.0,height: 1.0);
        vwItem.layer.shadowColor = color.cgColor;
        vwItem.layer.shadowRadius = 3;
        vwItem.layer.shadowOpacity = 0.5;
        vwItem.layer.cornerRadius = 3.0;
    }
    // MARK:- === PUSH POP Custom Methods =====
    
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.40
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromLeft
        navigationController?.view.layer.add(transition, forKey: kCATransition)
    navigationController?.pushViewController(viewControllerToPresent , animated: false)
    }
    
    func dismissDetail() {
         let transition = CATransition()
         transition.duration = 0.40
         transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
         transition.type = kCATransitionMoveIn
         transition.subtype = kCATransitionFromRight
         navigationController?.view.layer.add(transition, forKey: kCATransition)
         navigationController?.popViewController(animated: true)
    }
    // MARK:- ==== Check Bar Navigation RTL Support
    
    func validatePhone(_ phoneNumber: String) -> Bool {
        //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
        let phoneRegex = "[6789][0-9]{9}"
        let test = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return test.evaluate(with: phoneNumber)
    }
    
    //MARK:- Navigation Bar Hidden && Show Methods
    func hideNavigation()  {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func showNavigation()  {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    func showAlert(msg:String)  {
        let alert = UIAlertView(title: "error".localized, message: msg, delegate: nil, cancelButtonTitle:"ok".localized) //UIAlertView(title:"error".localized, message: "enter_correct_phone_number".localized, delegate: nil, cancelButtonTitle: "ok".localized, otherButtonTitles: "")
        alert.show()
    }
}



