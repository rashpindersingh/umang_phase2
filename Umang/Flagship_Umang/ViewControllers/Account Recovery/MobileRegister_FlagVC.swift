//
//  MobileRegister_FlagVC.swift
//  UMANG
//
//  Created by Rashpinder on 29/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

class MobileRegister_FlagVC: UIViewController,EulaChangeDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var viewEulaAgree: UIView!
    @IBOutlet weak var lblRemeberMsg: UILabel!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var lblEulaAgreement: UILabel!
    @IBOutlet weak var btnEula: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var viewTitleLabel: UILabel!
    
    var hud = MBProgressHUD()
    var flagAccept = Bool()
    
    var tout = Float()
    var rtry = String()
    var commingFrom = String()
    
    var mobileNumber = String()

    let lbl91 = UILabel(frame: CGRect(x: 35, y: 0, width:60, height: 48))
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        flagAccept = false;
        
        btnEula.setImage(UIImage(named: "checkbox_outline"), for: .normal)
        
        var htmlString = "I_agree_to_the_term_EULA".localized
        
        htmlString = htmlString.appending(String(format: "<style>body{font-family: '%@'; font-size:%fpx;}</style>", lblEulaAgreement.font.fontName,lblEulaAgreement.font.pointSize))

        
        do {
            
            var attrStr: NSAttributedString? = nil
            let myAttribute = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,  NSCharacterEncodingDocumentAttribute:String.Encoding.utf8.rawValue] as [String : Any]
            
            
            
            attrStr = try NSMutableAttributedString(data: htmlString.data(using: String.Encoding.unicode)!, options: myAttribute, documentAttributes: nil)
            
            lblEulaAgreement.attributedText = attrStr
            lblEulaAgreement.isUserInteractionEnabled = true
            lblEulaAgreement.lineBreakMode = .byWordWrapping
            
            let eulaGesture = UITapGestureRecognizer.init(target: self, action: #selector(btnOpenEula(_:)))
            eulaGesture.numberOfTapsRequired = 1
            lblEulaAgreement.addGestureRecognizer(eulaGesture)
            
        }catch {
            print("error \(error)")
        }
        
        
        txtMobile.becomeFirstResponder()
        
        self.layoutCornerRadius(view: btnNext, radius: 3.0)
        btnNext.setTitleColor(UIColor.white, for: .normal)
        
        txtMobile.clearButtonMode = .whileEditing
        
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(didTapView))
        self.view.addGestureRecognizer(tapGesture)
        
        
        let leftImage = UIImageView(frame: CGRect(x: 0, y: self.txtMobile.frame.size.height / 2 - 12, width: 25, height: 25))
        leftImage.image = UIImage(named: "profile_alt_mob_num")
        leftImage.contentMode = .scaleAspectFit
        
        lbl91.text = "+91"
        lbl91.textColor = .black
        lbl91.font = AppFont.regularFont(22)
        //lbl91.textColor = UIColor(red: 153.0/255.0, green:  153.0/255.0, blue:  153.0/255.0, alpha: 1)
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height:50))
        leftView.addSubview(leftImage)
        leftView.addSubview(lbl91)
        
        self.txtMobile.keyboardType = .numberPad
        self.txtMobile.leftView = leftView
        self.txtMobile.leftViewMode = .always
        let layer =  UIView(frame: CGRect(x: 2, y: self.txtMobile.frame.size.height-1, width: Screen_Size.width - 54, height: 1.5))
        layer.backgroundColor = blueColor
        self.txtMobile.addSubview(layer)
        
        btnNext.isEnabled = false
        self.enableBtnNext(status: false)
        
        if !mobileNumber.isEmpty
        {
            txtMobile.text = mobileNumber
            txtMobile.resignFirstResponder()
            self.view.endEditing(true)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .default
        
        self.setViewFonts()
        self.setLocalisation()
       
        self.hitInitAPI()
    }
    
    @objc func didTapView()
    {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        txtMobile.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    @objc func btnOpenEula(_ sender : UIGestureRecognizer)
    {
        let storyboard = UIStoryboard(name: "EulaScreen", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EulaScreenVC") as! EulaScreenVC
        vc.eulaDelegate = self
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    
    @objc func textFieldDidChange(_ sender : UITextField)
    {
        if (sender.text?.count)! >= 10
        {
            let mySubstring = sender.text!.prefix(10)
            sender.text = (mySubstring as NSString).substring(to: 10)
            
            sender.resignFirstResponder()
        }
        self.checkValidation()
    }
    
    func enableBtnNext(status: Bool)
    {
        if status
        {
            txtMobile.resignFirstResponder()
            btnNext.backgroundColor = UIColor(red: 84.0/255.0, green: 185.0/255.0, blue: 105.0/255.0, alpha: 1.0)
        }
        else
        {
            btnNext.backgroundColor = UIColor(red: 176.0/255.0, green: 176.0/255.0, blue: 176.0/255.0, alpha: 1.0)
        }
    }
    
    func validatePhone(phoneNumber:String) -> Bool
    {
        let phoneRegex = "[6789][0-9]{9}"
        
        let test = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        
        return test.evaluate(with:phoneNumber)
    }
    
    func eulaAcceptChanged(_ Acceptstatus: Bool)
    {
        if Acceptstatus
        {
            btnEula.setImage(UIImage(named: "checkbox_Tick"), for: .normal)
            flagAccept = true
        }
        else
        {
             btnEula.setImage(UIImage(named: "checkbox_outline"), for: .normal)
            flagAccept = false
        }
        
        self.checkValidation()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton)
    {
        if SharedManager.sharedSingleton()!.user_tkn.count == 0
        {
            if commingFrom == "PreLogin"
            {
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.dismiss(animated: false, completion: nil)
            }
        }
        else
        {
            self.dismiss(animated: false, completion: nil)
        }
        
        
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton)
    {
        if self.validatePhone(phoneNumber: txtMobile.text!) == false
        {
            let alert = UIAlertController(title: "error".localized, message: "enter_correct_phone_number".localized, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            txtMobile.text = ""
        }
        else
        {
            self.hitAPI()
        }
    }
    
    func hitInitAPI()
    {
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        // Set the label text.
        hud.label.text = NSLocalizedString("loading".localized, comment: "")
        let dictBody = NSMutableDictionary()
        
        
        let userTkn = SharedManager.sharedSingleton()?.user_tkn.count == 0 ? "" : SharedManager.sharedSingleton()?.user_tkn
        
        
        dictBody["lang"] = ""
        dictBody["tkn"] = userTkn
        
        
        
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_INIT, withBody: dictBody, andTag: TAG_REQUEST_CHAT, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            self?.hud.hide(animated: true)
            if error == nil && response != nil
            {
                if var jsonResponse = response as? [String:Any]
                {
                    jsonResponse = jsonResponse.formatDictionaryForNullValues(jsonResponse)!
                    
                    if let dicPD = jsonResponse["pd"] as? [String:Any]
                    {
                        SharedManager.sharedSingleton()!.arr_initResponse = NSMutableDictionary(dictionary: dicPD, copyItems: true)
                        
                        //SharedManager.sharedSingleton()!.arr_initResponse = dicPD as? NSMutableDictionary
                   
                        print(SharedManager.sharedSingleton()!.arr_initResponse)
                    UserDefaults.standard.setValue(SharedManager.sharedSingleton()!.arr_initResponse, forKey: "InitAPIResponse")
                        
                         let abbr = dicPD.string(key: "abbr")
                         UserDefaults.standard.setValue(abbr.capitalized, forKey: "ABBR_KEY")
                        
                         let emblemString = dicPD.string(key: "stemblem")
                        UserDefaults.standard.setValue(emblemString, forKey: "EMB_STR")
                         let infotab = dicPD.string(key: "infotab")
                        UserDefaults.standard.setValue(infotab.capitalized, forKey: "infotab")
                        
                    }
                   
                    UserDefaults.standard.setAESKey("UMANGIOSAPP")
                    UserDefaults.standard.encryptValue("", withKey: "lastFetchDate")
                    
                    UserDefaults.standard.setValue("", forKey: "lastFetchV1")
                    
                    let storage = HTTPCookieStorage.shared
                    
                    if let arrayCookies = storage.cookies
                    {
                        for cookie in arrayCookies
                        {
                            storage.deleteCookie(cookie)
                        }
                    }
                   
                    
                    
                    let cookieArray = NSMutableArray()
                    UserDefaults.standard.setValue(cookieArray, forKey: "cookieArray")
                    
                    UserDefaults.standard.synchronize()
                    
                    let serialQueue = DispatchQueue(label: "com.Umang.Gov.In")
                    
                    do
                    {
                        serialQueue.async
                            {
                                SharedManager.sharedSingleton()!.dbManager.deleteBannerHomeData()
                                SharedManager.sharedSingleton()!.dbManager.deleteAllServices()
                                SharedManager.sharedSingleton()!.dbManager.deleteSectionData()
                            }
                        
                        DispatchQueue.main.async {
                             SharedManager.sharedSingleton()!.tabSelectedIndex = UserDefaults.standard.integer(forKey: "SELECTED_TAB_INDEX")
                        }
                        
                    }
                }
                
            }
            else
            {
                self?.showAlert(msg: error!.localizedDescription)
            }
        })
    }
    
    func hitAPI()
    {
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        // Set the label text.
        hud.label.text = NSLocalizedString("loading".localized, comment: "")
        let dictBody = NSMutableDictionary()
        
        dictBody["mno"] = txtMobile.text
        dictBody["peml"] = ""
        dictBody["ort"] = "rgtmob"
        dictBody["tkn"] = SharedManager.sharedSingleton()?.user_tkn
        

        
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_REGISTRATION, withBody: dictBody, andTag: TAG_REQUEST_CHAT, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            self?.hud.hide(animated: true)
            if error == nil && response != nil
            {
                if let jsonResponse = response as? [String:Any] {
                    let node = jsonResponse["node"] as? String ?? ""
                    
                    if  node.count > 0
                    {
                        UserDefaults.standard.setValue(node, forKey: "NODE_KEY")
                        UserDefaults.standard.synchronize()
                    }
                    if let pdData = jsonResponse["pd"] as? [String:Any]
                    {
                        let toutString = pdData["tout"] as! String
                        self?.tout = toutString.toFloat()
                        self?.rtry = pdData["rtry"] as! String
                        
                        if (jsonResponse.string(key: "rs") == API_SUCCESS_CASE) || (jsonResponse.string(key: "rs") == API_SUCCESS_CASE1)
                        {
                            self?.openNextView()
                        }
                    }
                }
            }
            else
            {
                
                let alert = UIAlertController(title: "error".localized, message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: { (action) in
                    
                    self?.openLoginView()
                }))
                
                self?.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    func openLoginView()
    {
        if SharedManager.sharedSingleton()?.user_tkn.count == 0
        {
            if commingFrom == "PreLogin"
            {
                let storyboard = UIStoryboard(name: "DetailService", bundle: nil)
                var vc = storyboard.instantiateViewController(withIdentifier: "LoginAppVC") as! LoginAppVC
                
                if UIScreen.main.bounds.size.height == 1024
                {
                    vc = LoginAppVC.init(nibName: "LoginAppVC_iPad", bundle: nil)
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                self.dismiss(animated: false, completion: nil)
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "DetailService", bundle: nil)
            var vc = storyboard.instantiateViewController(withIdentifier: "LoginAppVC") as! LoginAppVC
            
            if UIScreen.main.bounds.size.height == 1024
            {
                vc = LoginAppVC.init(nibName: "LoginAppVC_iPad", bundle: nil)
            }
            
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    func openNextView()
    {
    
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterOTPVC") as! RegisterOTPVC
        vc.tout = tout
        vc.rtry = rtry
        vc.mobileVerify = txtMobile.text!
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func eulaButtonAction(_ sender: UIButton)
    {
        if flagAccept
        {
            btnEula.setImage(UIImage(named: "checkbox_outline"), for: .normal)
            flagAccept = false
        }
        else
        {
            btnEula.setImage(UIImage(named: "checkbox_Tick"), for: .normal)
            flagAccept = true
        }
        
        self.checkValidation()
    }
    
    func checkValidation()
    {
        if self.validatePhone(phoneNumber: txtMobile.text!) == true && flagAccept == true
        {
            btnNext.isEnabled = true
            self.enableBtnNext(status: true)
        }
        else
        {
            btnNext.isEnabled = false
            self.enableBtnNext(status: false)
        }
    }
    
    func setLocalisation()
    {
        btnNext.setTitle("next".localized, for: .normal)
        btnBack.setTitle("back".localized, for: .normal)
        
        viewTitleLabel.text = "registration_label".localized
        lblTitle.text = "register_intro_heading".localized
        lblSubtitle.text = "otp_on_this_number".localized
        lblRemeberMsg.text = "register_intro_sub_heading_hint".localized

    }
    
    func setViewFonts()
    {
        
        btnBack.titleLabel?.font = AppFont.regularFont(17)
        viewTitleLabel.font = AppFont.semiBoldFont(17)
        
        self.lblTitle.font = AppFont.mediumFont(17)
        self.lblSubtitle.font = AppFont.mediumFont(13)
        
        self.lblRemeberMsg.font = AppFont.lightFont(12)
        
        //self.lblEulaAgreement.font = AppFont.mediumFont(18)
        
        self.txtMobile.font = AppFont.regularFont(22)
        
        self.btnNext.titleLabel?.font = AppFont.mediumFont(19)
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
