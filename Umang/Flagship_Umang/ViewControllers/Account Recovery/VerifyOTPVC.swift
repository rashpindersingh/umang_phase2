//
//  VerifyOTPVC.swift
//  UMANG
//
//  Created by Rashpinder on 26/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import Foundation


enum OptVerifyType :String {
    case recoverMPIN = "Recover MPIN"
    case alterMobile = "Alternate MObile"
    case email = "Email"
}
class VerifyOTPVC: UIViewController
{

    @IBOutlet var backButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var headerDescriptionLabel: UILabel!
    @IBOutlet var otpTextfield: UITextField!
    @IBOutlet var waitingOTPLabel: UILabel!
    
    @IBOutlet var timerLabel: UILabel!
    
    @IBOutlet weak var topNextBtnConstraint: NSLayoutConstraint!
    @IBOutlet var resendOTPView: UIView!
    @IBOutlet var resendOTPHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var resendOTPButton: UIButton!
    
    @IBOutlet var callMeButton: UIButton!
    
    @IBOutlet var nextButton: UIButton!
    
    var otpTimer = Timer()
    
    var verifyType :OptVerifyType = .recoverMPIN
    
    var count :Float = 0
    var retryOTPString  = String()
    var retryCallString = String()
    var tout :Float = 0
    var rtry  = "29|30"
    
    @IBOutlet var resendTrailingConstraintLow: NSLayoutConstraint!
    @IBOutlet var dividerLabel: UILabel!
    
    @IBOutlet var resendCallMeTrailingConstraint: NSLayoutConstraint!
    
    
    var mobileVerify = ""
    var email = ""
    var qtsnIDS = ""
    var alterMobile = ""
    @IBOutlet weak var lblEnterOptMSg: UILabel!
    var isFromLogin = "no"
    var hud = MBProgressHUD()
    
    @IBOutlet weak var leadingWaitingSmsConstraint: NSLayoutConstraint!
    @IBOutlet weak var loaderSpinner: JTMaterialSpinner!
    @IBOutlet weak var progressBar: UIProgressView!
    
    // MARK:- View Methods
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        
        
        if verifyType == .alterMobile || verifyType == .email {
        }else {
            count = tout
            
            if otpTimer.isValid
            {
                otpTimer.invalidate()
            }
            let profress = count / tout
            
            self.progressBar.progress = profress
            otpTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(updateUI(_:)), userInfo: nil, repeats: true)
            RunLoop.main.add(otpTimer, forMode: .commonModes)
        }
        
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.otpTextfield.keyboardType = .numberPad
        waitingOTPLabel.text = "waiting_for_otp".localized
        nextButton.setTitle("next".localized, for: .normal)
        nextButton.setTitleColor(UIColor.white, for: .normal)
        nextButton.layer.cornerRadius = 3.0;
        nextButton.layer.masksToBounds = true
        backButton.setTitle("back".localized, for: .normal)
        resendOTPButton.setTitle("resend_otp".localized, for: .normal)
        callMeButton.setTitle("call_me".localized, for: .normal)
        resendOTPButton.titleLabel?.numberOfLines = 5;
        resendOTPButton.titleLabel?.lineBreakMode = .byWordWrapping
        
        
        callMeButton.titleLabel?.numberOfLines = 5;
        callMeButton.titleLabel?.lineBreakMode = .byWordWrapping
        
        lblEnterOptMSg.text = "enter_6_digit_otp".localized
        
        if verifyType == .alterMobile || verifyType == .email {
            self.hitSendOTP_AlternateMobile_EmailAPI(chanel: "sms")
        }else {
            let stringArray = self.rtry.components(separatedBy:"|")
            
            self.retryOTPString  = stringArray[0]
            self.retryCallString = stringArray[1]
        }
        otpTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.layoutCornerRadius(view: self.nextButton, radius: 5)

        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(didTapView))
        
        self.view.addGestureRecognizer(tapGesture)
      
        loaderSpinner.circleLayer.lineWidth = 4.0
        loaderSpinner.circleLayer.strokeColor = blueColor.cgColor
        self.showRetyView(hide: true)
         self.setFontSize()
        self.enableBtnNext(status: false)
        self.hideLoader(hide: false)
        
        otpTextfield.becomeFirstResponder()
    }

    func setFontSize()  {
        otpTextfield.font = AppFont.regularFont(20)
        titleLabel.font = AppFont.semiBoldFont(17)
        
        self.headerDescriptionLabel.font = AppFont.mediumFont(17)
        self.waitingOTPLabel.font = AppFont.regularFont(18)
        self.timerLabel.textColor = blueColor
        self.timerLabel.font = AppFont.regularFont(18)
        
        self.nextButton.setTitle("next".localized, for: .normal)
        self.nextButton.titleLabel?.font = AppFont.mediumFont(19)
        self.backButton.titleLabel?.font = AppFont.regularFont(17.0)
        
        titleLabel.text = "verify_otp_text".localized
        self.backButton.setTitle("back".localized, for: .normal)
        switch verifyType {
        case .alterMobile:
            headerDescriptionLabel.text = String(format: "otp_sent_verification".localized, alterMobile)
        case .email:
            headerDescriptionLabel.text = String(format: "otp_sent_verification".localized, email)
        default:
           headerDescriptionLabel.text = String(format: "otp_sent_via_sms".localized, SharedManager.sharedSingleton()!.mobileNumber)
        }
       
    }
    @objc func didTapView()
    {
        self.view.endEditing(true)
    }
    
    @objc func updateUI(_ sender: Timer?)
    {
        
        if count <= 0
        {
            otpTimer.invalidate()
            self.showRetyView(hide: false)
            if loaderSpinner.isAnimating {
                self.hideLoader(hide: true)
            }
        }
        else
        {
            count = count - 1
            let profress = count / tout

            self.progressBar.progress = profress
            
            timerLabel.text = self.formatCounterTime(totalSeconds: NSInteger(count))
            if !loaderSpinner.isAnimating {
                self.hideLoader(hide: false)
            }
            
        }
    }
    func hideLoader(hide:Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.leadingWaitingSmsConstraint.constant = hide ? 20.0 : 55.0;
            if hide {
                self?.loaderSpinner.endRefreshing()
                self?.loaderSpinner.isHidden = true
            }else {
                self?.loaderSpinner.beginRefreshing()
                self?.loaderSpinner.isHidden = false
            }
        }
        
    }
    func showRetyView(hide:Bool)  {
        self.showRtryOptions()
        self.resendOTPView.isHidden = hide
        resendOTPHeightConstraint.constant = hide ? 0.0: 55.0
        topNextBtnConstraint.constant = hide ? 40: 100.0

        callMeButton.isHidden = hide
        resendOTPButton.isHidden = hide
        
        if verifyType == .email
        {
            callMeButton.isHidden = true
            resendTrailingConstraintLow.priority = 1000
            resendCallMeTrailingConstraint.priority = 250
            dividerLabel.isHidden = true
        }
        else
        {
            resendTrailingConstraintLow.priority = 250
            resendCallMeTrailingConstraint.priority = 1000
            dividerLabel.isHidden = false
        }
        
        let resendOTPInt  = NSInteger(retryOTPString)
        let resendCallInt = NSInteger(retryCallString)
        if resendOTPInt ?? 0 <= 0
        {
            resendOTPButton.isHidden = true
        }
        if resendCallInt ?? 0 <= 0
        {
            callMeButton.isHidden = true
        }
        if resendOTPInt ?? 0 <= 0 && resendCallInt ?? 0 <= 0
        {
            resendOTPHeightConstraint.constant = 0
            self.resendOTPView.isHidden = true
        }
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
    }
    
    func formatCounterTime(totalSeconds : NSInteger) -> String
    {
        let seconds = totalSeconds % 60
        let minutes = (totalSeconds / 60) % 60
        
        return String(format: "%02d:%02d", minutes,seconds)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton)
    {
        if  verifyType == .alterMobile || verifyType == .email {
            for vc  in self.navigationController!.viewControllers {
                if vc.isKind(of: AccountRecoveryVC.self) {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func resendOTPButtonAction(_ sender: UIButton)
    {
        otpTextfield.text = ""
      ///  resendOTPHeightConstraint.constant = 55
        self.showRetyView(hide: true)
        
        self.hideLoader(hide: false)

       // self.hitAPIResendOTPwithIVR(channelType: "sms")
        switch self.verifyType {
        case .recoverMPIN:
            self.hitAPIResendOTPwithIVR(channelType: "sms")
        case .alterMobile:
            self.hitSendOTP_AlternateMobile_EmailAPI(chanel: "sms")
        case .email:
            self.hitSendOTP_AlternateMobile_EmailAPI(chanel: "sms")
        default:
            break
        }
    }
    
    
    
    @IBAction func callMeButtonAction(_ sender: UIButton)
    {
        otpTextfield.text = ""
        ///  resendOTPHeightConstraint.constant = 55
        self.showRetyView(hide: true)
        self.hideLoader(hide: false)

        // self.hitAPIResendOTPwithIVR(channelType: "sms")
        switch self.verifyType {
        case .recoverMPIN:
            self.hitAPIResendOTPwithIVR(channelType: "ivr")
        case .alterMobile:
            self.hitSendOTP_AlternateMobile_EmailAPI(chanel: "ivr")
        case .email:
            self.hitSendOTP_AlternateMobile_EmailAPI(chanel: "ivr")
        default:
            break
        }
        
    }
    
    func enableBtnNext(status : Bool)
    {
        self.nextButton.isUserInteractionEnabled = status
        if status
        {
            self.view.endEditing(true)
            self.nextButton.backgroundColor = UIColor(red: 84.0/255.0, green: 185.0/255.0, blue: 105.0/255.0, alpha: 1.0)
//            if loaderSpinner.isAnimating {
//                self.hideLoader(hide: true)
//            }
        }
        else
        {
            self.nextButton.backgroundColor = UIColor(red: 176.0/255.0, green: 176.0/255.0, blue: 176.0/255.0, alpha: 1.0)
        }
    }
    
    func isValidOTP(otpString : String) -> Bool
    {
        let strMatchstring = "\\b([0-9%_.+\\-]+)\\b"
        let predicate = NSPredicate(format: "SELF MATCHES %@", strMatchstring)
        
        if !predicate.evaluate(with: otpString)
        {
            return false
        }
        
        return true
    }
    
    func checkValidation()
    {
        if (otpTextfield.text?.count)! < 6
        {
            let alert = UIAlertController(title: "error".localized, message: "register_verify_otp_heading".localized, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if !self.isValidOTP(otpString: otpTextfield.text!)
        {
            let alert = UIAlertController(title: "error".localized, message: "register_verify_otp_heading".localized, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    
    @objc func textFieldDidChange(_ sender : UITextField)
    {
        if (sender.text?.count)! >= 6
        {
            let mySubstring = sender.text!.prefix(6)
            otpTextfield.text = (mySubstring as NSString).substring(to: 6)
            
            sender.resignFirstResponder()
            self.enableBtnNext(status: true)
            
        }
        else
        {
            self.enableBtnNext(status: false)
        }
    }
    func showRtryOptions()  {
        let attemptsLeft = "attempts_left".localized
        let btnOtpResnd = getAttributedString(first: "resend_otp".localized, second: "(\(self.retryOTPString) \(attemptsLeft))")
        self.resendOTPButton.setAttributedTitle(btnOtpResnd, for: .normal)
        let btnCallResnd = getAttributedString(first: "call_me".localized, second: "(\(self.retryCallString) \(attemptsLeft))")
        self.callMeButton.setAttributedTitle(btnCallResnd, for: .normal)
    }
    func getAttributedString(first:String,second:String) -> NSMutableAttributedString {
        let myAttribute = [NSForegroundColorAttributeName: UIColor.black,  NSFontAttributeName:AppFont.regularFont(15)] as [String : Any]
       let firstAttributed = NSMutableAttributedString(string: first, attributes: myAttribute)//NSAttributedString(string: first, attributes:myAttribute )
        
        let myAttribute2 = [NSForegroundColorAttributeName: UIColor(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1),  NSFontAttributeName:AppFont.lightFont(11)] as [NSAttributedStringKey : Any]
        let secondAttributed = NSAttributedString(string: "\n \(second)", attributes: myAttribute2 as [String : Any])
          firstAttributed.append(secondAttributed)
        return firstAttributed
    }
    
    
    // MARK:- IVR API Call
    
    func hitAPIResendOTPwithIVR(channelType : String)
    {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Wait..."
        
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        dictBody["mno"] = mobileVerify //Enter mobile number of user
        dictBody["chnl"] = channelType //chnl : type sms for OTP and IVR for call
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = "frgtmpn"
        
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_IVR_OTP, withBody: dictBody, andTag: TAG_REQUEST_IVR_OTP) { (response, error, tag) in
            
            self.hud.hide(animated: true)
            
            if error == nil && response != nil
            {
                if let jsonResponse = response as? [String:Any]
                {
                    let node = jsonResponse["node"] as? String ?? ""
                    if  node.count > 0
                    {
                        UserDefaults.standard.setValue(node, forKey: "NODE_KEY")
                        UserDefaults.standard.synchronize()
                    }
                    if let pdData = jsonResponse["pd"] as? [String:Any]
                    {
                        
                        if (jsonResponse.string(key: "rs") == API_SUCCESS_CASE) || (jsonResponse.string(key: "rs") == API_SUCCESS_CASE1)
                        {
                            self.tout = Float(pdData["tout"] as! String) ?? 0
                            self.rtry = pdData["rtry"] as! String
                            
                            let stringArray = self.rtry.components(separatedBy:"|")
                            self.retryOTPString  = stringArray[0]
                            self.retryCallString = stringArray[1]
                            self.showRetyView(hide: true)
                            self.count = self.tout
                            if self.otpTimer.isValid
                            {
                                self.otpTimer.invalidate()
                            }
                            
                            self.otpTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.updateUI(_:)), userInfo: nil, repeats: true)
                            
                            RunLoop.main.add(self.otpTimer, forMode: .commonModes)
                        }
                    }
                    
                }
                
            }
            else
            {
                if let localError = error?.localizedDescription {
                    let alert = UIAlertController(title: "error".localized, message: localError, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
            
            
        }
    }
    
    // MARK:- Verify OTP API Call
    
    func hitOTPVerifyAPI()
    {
        let otpString = otpTextfield.text
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "loading".localized
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        //SharedManager.sharedSingleton()!.mobileNumber  = "9780379992"
        dictBody["mno"] = SharedManager.sharedSingleton()!.mobileNumber //Enter mobile number of user
        dictBody["chnl"] = "sms" //chnl : type sms for OTP and IVR for call
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = "frgtmpn"
        dictBody["otp"] = otpString
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_VALIDATE_OTP2, withBody: dictBody, andTag: TAG_REQUEST_VALIDATE_OTP, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            
            self?.hud.hide(animated: true)
            
            if error == nil && response != nil
            {
                if let data = response as? NSDictionary
                {
                    let rs = data["rs"] as! String
                    
                    if rs == API_SUCCESS_CASE || rs == API_SUCCESS_CASE1
                    {
                        //OPEN NEXT VIEW
                        self?.openRecoveryOption(dicJson: data)
                    }
                    else
                    {
                        self?.otpTextfield.text = ""
                        //self?.count = -1;
                        //self?.updateUI(nil)
                    }
                }
                
            }
            else {
                
                if let localError = error?.localizedDescription {
                    let alert = UIAlertController(title: "error".localized, message: localError, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
               
                
                self?.enableBtnNext(status: false)
                
                
            }

            
           
        })
    }
    @IBAction func didTapNextButtonAction(_ sender: UIButton) {
        
//        self.pushToSetMPINVC(dicJson: nil)
//        return
        switch self.verifyType {
        case .recoverMPIN:
            self.hitOTPVerifyAPI()
        case .alterMobile:
            self.hitVerifyAlternate_Email_OTP_API(chanel: "sms")
        case .email:
            self.hitVerifyAlternate_Email_OTP_API(chanel: "sms")
        default:
            break
        }
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    func openRecoveryOption(dicJson:NSDictionary) {
        let email = dicJson.value(forKey: "email") as? String ?? ""
        let alterMobile = dicJson.value(forKey: "amno") as? String ?? ""
        let qtsnIDs = dicJson.value(forKey: "quesid") as? String ?? ""
        if "\(email)".isEmpty && "\(alterMobile)".isEmpty && "\(qtsnIDs)".isEmpty {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetMPINRecoveryVC") as! SetMPINRecoveryVC
            vc.mpinType = .register
            vc.mobileVerify = mobileVerify
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let recoverVC = self.storyboard?.instantiateViewController(withIdentifier: "RecoveryOptionsVC") as! RecoveryOptionsVC
            recoverVC.email = "\(email)"
            recoverVC.alterMobile = "\(alterMobile)"
            recoverVC.securityQtsn = "\(qtsnIDs)"
            recoverVC.mobileVerify = self.mobileVerify
            recoverVC.otpStr = self.otpTextfield.text!
            recoverVC.isFromLogin = isFromLogin
            self.navigationController?.pushViewController(recoverVC, animated: true)
        }
    }
    

}

extension VerifyOTPVC : UITextFieldDelegate
{
    // MARK:- Verify OTP API Call
    
    func hitSendOTP_AlternateMobile_EmailAPI(chanel:String)
    {
        let otpString = otpTextfield.text
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "loading".localized
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        dictBody["mno"] = mobileVerify //Enter mobile number of user
        dictBody["chnl"] = chanel //chnl : type sms for OTP and IVR for call
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = "frgtmpnamem"
        if verifyType == .alterMobile {
            dictBody["amno"] = self.alterMobile
            dictBody["amemflag"] = "M"
        }
        if verifyType == .email {
            dictBody["email"] = self.email
            dictBody["amemflag"] = "E"
        }
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_NEW_OTP, withBody: dictBody, andTag: TAG_REQUEST_VALIDATE_OTP, completionHandler: { (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            self.hud.hide(animated: true)
            if error == nil && response != nil
            {
            if let jsonResponse = response as? [String:Any]
            {
                let node = jsonResponse["node"] as? String ?? ""
                if  node.count > 0
                {
                    UserDefaults.standard.setValue(node, forKey: "NODE_KEY")
                    UserDefaults.standard.synchronize()
                }
                if let pdData = jsonResponse["pd"] as? [String:Any]
                {
                    
                    if (jsonResponse.string(key: "rs") == API_SUCCESS_CASE) || (jsonResponse.string(key: "rs") == API_SUCCESS_CASE1)
                    {
                        self.tout = Float(pdData["tout"] as! String) ?? 0
                        self.rtry = pdData["rtry"] as! String
                        
                        let stringArray = self.rtry.components(separatedBy:"|")
                        self.retryOTPString  = stringArray[0]
                        self.retryCallString = stringArray[1]
                        /// UPDATE BUTTON TEXTS
                        self.showRetyView(hide: true)
                        self.count = self.tout
                        let profress = self.count / self.tout
                        
                        self.progressBar.progress = profress
                        if self.otpTimer.isValid
                        {
                            self.otpTimer.invalidate()
                        }
                        
                        self.otpTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.updateUI(_:)), userInfo: nil, repeats: true)
                        
                        RunLoop.main.add(self.otpTimer, forMode: .commonModes)
                    }
                }
            }
            }
            else
            {
                if let localError = error?.localizedDescription {
                    let alert = UIAlertController(title: "error".localized, message: localError, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }

            }
        })
    }
    func hitVerifyAlternate_Email_OTP_API(chanel:String)
    {
        let otpString = otpTextfield.text
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "loading".localized
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        dictBody["mno"] = mobileVerify //Enter mobile number of user
        dictBody["chnl"] = chanel //chnl : type sms for OTP and IVR for call
        dictBody["peml"] = "" //get from mobile contact //not supported iphone
        dictBody["ort"] = "frgtmpnamem"
        dictBody["otp"] = otpString
        if verifyType == .alterMobile {
            dictBody["amno"] = self.alterMobile
            dictBody["amemflag"] = "M"
        }
        if verifyType == .email {
            dictBody["email"] = self.email
            dictBody["amemflag"] = "E"
        }
         dictBody["otp"] = otpString
        objRequest.hitWebServiceAPI(withPostMethod: true, isAccessTokenRequired: false, webServiceURL: UM_API_NEW_VALIDATEOTP, withBody: dictBody, andTag: TAG_REQUEST_VALIDATE_OTP, completionHandler: {[weak self] (_ response:Any?,_ error:Error?,_ tag:REQUEST_TAG) -> Void in
            
            self?.hud.hide(animated: true)
            if error == nil && response != nil
            {
            if let data = response as? NSDictionary
            {
                let rs = data["rs"] as! String
                
                if rs == API_SUCCESS_CASE || rs == API_SUCCESS_CASE1
                {
                    //OPEN NEXT VIEW
                    self?.pushToSetMPINVC(dicJson: data)
                }
                else
                {
                    self?.otpTextfield.text = ""
                    //self?.count = -1;
                   // self?.updateUI(nil)
                }
            }
            }
            else
            {
                if let localError = error?.localizedDescription {
                    let alert = UIAlertController(title: "error".localized, message: localError, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
                self?.enableBtnNext(status: false)
            }
        })
    }
    func pushToSetMPINVC(dicJson:NSDictionary?)  {
        var vc = self.storyboard?.instantiateViewController(withIdentifier: "SetMPINRecoveryVC") as! SetMPINRecoveryVC
        if verifyType == .alterMobile {
            vc.mpinType = .alterMobile
        }
        if verifyType == .email {
            vc.mpinType = .emailOtp
        }
        vc.mobileVerify = mobileVerify
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
