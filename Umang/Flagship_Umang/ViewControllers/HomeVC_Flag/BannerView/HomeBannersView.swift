//
//  HomeBannersView.swift
//  UmangSwift
//
//  Created by Rashpinder on 24/05/18.
//  Copyright © 2018 Umang. All rights reserved.
//

import UIKit


@objc protocol HomeBannerProtocol
{
    func didTapBanner(banner: NSDictionary)
}

 @objc class HomeBannersView: UICollectionReusableView {
    @IBOutlet var contentView: UIView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var collBanners: UICollectionView!
    var arrBanners = [BannerData]()
    
     var didSelect:HomeBannerProtocol!
    var isToucBegin = false
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        self.commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed("HomeBannersView", owner: self, options: nil)
        contentView.frame = self.bounds
        contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        addSubview(contentView)
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.collBanners.delegate = self
        self.collBanners.dataSource = self
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.collBanners.collectionViewLayout = flowLayout
         self.collBanners.register(BannersDataCell.cellNib, forCellWithReuseIdentifier:BannersDataCell.id)
        self.pageControl.pageIndicatorTintColor = .lightGray
        self.pageControl.currentPageIndicatorTintColor = .gray
        pageControl.transform = CGAffineTransform(scaleX: 0.7, y: 0.7); // Looks better!

    }
    public func setDataSource(arrBaners:[NSDictionary]) {
         var arrBanr = [BannerData]()

         for dic in arrBaners {
            if let type = dic["BANNER_ACTION_TYPE"] as? String , let actionUrl = dic["BANNER_ACTION_URL"] as? String, let desc = dic["BANNER_DESC"] as? String , let image = dic["BANNER_IMAGE_URL"] as? String, let id = dic["BANNER_ID"] as? String  {
                let baner = BannerData.init(actionType:type , actionURL: actionUrl, desc: desc, imageUrl: image,id:id)
                 arrBanr.append(baner)
            }
            
        }
        self.pageControl.numberOfPages = arrBaners.count
        self.arrBanners = arrBanr
        self.collBanners.reloadData()
    }
    @IBAction func pageControlDidChangeValue(_ sender: UIPageControl) {
        //self.scrollToNextCell()
        let indexPath = IndexPath(row: sender.currentPage, section: 0)
        self.collBanners.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.pageControl.currentPage = sender.currentPage
    }
}
extension HomeBannersView : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrBanners.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannersDataCell.id, for: indexPath) as! BannersDataCell
        let banner = arrBanners[indexPath.row]
        cell.setDataSource(banner: banner.imageUrl)
        return cell
    }
}
extension HomeBannersView : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
        // top, left, bottom, right
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let size = self.collBanners.frame.size
        return size //CGSize(width:  Screen_Size.width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if  self.didSelect != nil  {
            let baner = arrBanners[indexPath.row]
            let dic :NSDictionary = ["BANNER_ACTION_TYPE" :baner.actionType,"BANNER_ACTION_URL" :baner.actionURL,"BANNER_DESC" :baner.desc,"BANNER_IMAGE_URL" :baner.imageUrl,"BANNER_ID" :baner.banerID ]
            self.didSelect.didTapBanner(banner: dic)
        }
    }
}
// MARK: - ======  Scroll View Handling with Page Control change Value =====

extension HomeBannersView {
    //MARK: Scroll View Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        pageControl.currentPage = Int((scrollView.contentOffset.x / Screen_Size.width))
        self.isToucBegin = true
    }
    /*
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func scrollToNextCell(){
        //get cell size
        var  cellSize :CGSize!
        let frameColl = self.collBanners.frame.size;
        cellSize = CGSize(width: Screen_Size.width, height: frameColl.height);
        //get current content Offset of the Collection view
        let contentOffset = self.collBanners.contentOffset;
        //scroll to next cell
        if isToucBegin == false {
            if self.collBanners.contentSize.width <= self.collBanners.contentOffset.x + cellSize.width
            {
                self.collBanners.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
                pageControl.currentPage = 0
            } else {
                self.collBanners.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
                pageControl.currentPage = Int((collBanners.contentOffset.x / Screen_Size.width)) + 1
            }
        }else {
            self.isToucBegin = false
        }
    }
}
@objc class HomeDataBannersView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var collBanners: UICollectionView!
    var arrBanners = [BannerData]()
    
    var didSelect:HomeBannerProtocol!
    var isToucBegin = false
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        self.commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed("HomeDataBannersView", owner: self, options: nil)
        contentView.frame = self.bounds
        contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        addSubview(contentView)
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.collBanners.delegate = self
        self.collBanners.dataSource = self
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.collBanners.collectionViewLayout = flowLayout
        self.collBanners.register(BannersDataCell.cellNib, forCellWithReuseIdentifier:BannersDataCell.id)
        self.pageControl.pageIndicatorTintColor = .lightGray
        self.pageControl.currentPageIndicatorTintColor = .gray
        pageControl.transform = CGAffineTransform(scaleX: 0.7, y: 0.7); // Looks better!
        
    }
    public func setDataSource(arrBaners:[NSDictionary]) {
        var arrBanr = [BannerData]()
        
        for dic in arrBaners {
            if let type = dic["BANNER_ACTION_TYPE"] as? String , let actionUrl = dic["BANNER_ACTION_URL"] as? String, let desc = dic["BANNER_DESC"] as? String , let image = dic["BANNER_IMAGE_URL"] as? String, let id = dic["BANNER_ID"] as? String  {
                let baner = BannerData.init(actionType:type , actionURL: actionUrl, desc: desc, imageUrl: image,id:id)
                arrBanr.append(baner)
            }
            
        }
        self.pageControl.numberOfPages = arrBaners.count
        self.arrBanners = arrBanr
        self.collBanners.reloadData()
    }
    @IBAction func pageControlDidChangeValue(_ sender: UIPageControl) {
        //self.scrollToNextCell()
        let indexPath = IndexPath(row: sender.currentPage, section: 0)
        self.collBanners.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.pageControl.currentPage = sender.currentPage
    }
}
extension HomeDataBannersView : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrBanners.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannersDataCell.id, for: indexPath) as! BannersDataCell
        let banner = arrBanners[indexPath.row]
        cell.setDataSource(banner: banner.imageUrl)
        return cell
    }
}
extension HomeDataBannersView : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
        // top, left, bottom, right
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = self.collBanners.frame.size
        return size //CGSize(width:  Screen_Size.width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if  self.didSelect != nil  {
            let baner = arrBanners[indexPath.row]
            let dic :NSDictionary = ["BANNER_ACTION_TYPE" :baner.actionType,"BANNER_ACTION_URL" :baner.actionURL,"BANNER_DESC" :baner.desc,"BANNER_IMAGE_URL" :baner.imageUrl,"BANNER_ID" :baner.banerID ]
            self.didSelect.didTapBanner(banner: dic)
        }
    }
}
// MARK: - ======  Scroll View Handling with Page Control change Value =====

extension HomeDataBannersView {
    //MARK: Scroll View Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        pageControl.currentPage = Int((scrollView.contentOffset.x / Screen_Size.width))
        self.isToucBegin = true
    }
    /*
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func scrollToNextCell(){
        //get cell size
        var  cellSize :CGSize!
        let frameColl = self.collBanners.frame.size;
        cellSize = CGSize(width: Screen_Size.width, height: frameColl.height);
        //get current content Offset of the Collection view
        let contentOffset = self.collBanners.contentOffset;
        //scroll to next cell
        if isToucBegin == false {
            if self.collBanners.contentSize.width <= self.collBanners.contentOffset.x + cellSize.width
            {
                self.collBanners.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
                pageControl.currentPage = 0
            } else {
                self.collBanners.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
                pageControl.currentPage = Int((collBanners.contentOffset.x / Screen_Size.width)) + 1
            }
        }else {
            self.isToucBegin = false
        }
    }
}

