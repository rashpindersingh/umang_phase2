//
//  BannersDataCell.swift
//  UmangSwift
//
//  Created by Rashpinder on 24/05/18.
//  Copyright © 2018 Umang. All rights reserved.
//

import UIKit

let app_del : AppDelegate = UIApplication.shared.delegate as! AppDelegate
let Screen_Size = UIScreen.main.bounds.size
var iPad = false
class BannerData :NSObject {
    
    var actionType: String = ""
    var actionURL: String = ""
    var desc: String = ""
    var imageUrl: String = ""
    var banerID = ""
    init(actionType:String, actionURL:String, desc:String,imageUrl:String,id:String) {
        self.actionURL = actionURL
        self.actionType = actionType
        self.desc = desc
        self.imageUrl = imageUrl
        self.banerID = id 
    }
}
class BannersDataCell: UICollectionViewCell, CellInterface {
    
    @IBOutlet var bannerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapBannerImage(tap:)))
        
        self.bannerImage.contentMode = .scaleAspectFit;
    }
    func addLayerAndShadow() {
        layer.shadowOffset = CGSize(width: 0.2, height: 1.5)
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
        if  let borderView = self.viewWithTag(4444) {
            borderView.backgroundColor = UIColor.white
            borderView.layer.cornerRadius = 5.0
            borderView.clipsToBounds = true
            // borderView.layer.borderWidth = 0.05;
            // borderView.layer.borderColor = UIColor.gray.cgColor
            borderView.layer.masksToBounds = true
            sendSubview(toBack: borderView)
        }
    }
    @objc func didTapBannerImage(tap:UITapGestureRecognizer){
        
    }
    func setDataSource(banner:String)  {
        let placeHolder = UIImage(named: "img_loadertime.png")
        if let strUrl = URL(string: banner) {
            //self.bannerImage.sd_setImage(with: strUrl, placeholderImage: placeHolder)
             
            DispatchQueue.main.async {[weak self] in
                self?.bannerImage.sd_setImage(with: strUrl, placeholderImage: placeHolder, options: .highPriority)
            }
             return
        }
        self.bannerImage.image = placeHolder
    }
}
