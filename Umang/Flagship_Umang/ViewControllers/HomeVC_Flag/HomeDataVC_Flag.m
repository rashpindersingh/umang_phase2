//
//  HomeDataVC_Flag.m
//  UMANG
//
//  Created by Rashpinder on 07/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "HomeDataVC_Flag.h"
#import "UMANG-Swift.h"
#import "HelpViewController.h"
#import "FAQWebVC.h"
#import "MoreTabVC_Flag.h"
#import "SettingsViewController.h"
static const CGSize progressViewSize = { 95.0f, 25.0f };
static float NV_height= 50;

@interface HomeDataVC_Flag () <HomeBannerProtocol>
{
    itemMoreInfoVC *itemMoreVC;
    
    __weak IBOutlet UIView *vwSearchBG;
    BOOL flagrotation;
    
    BOOL flagStatusBar;
    IBOutlet UIButton *btn_notification;
    
    StateList *obj;
    NSString *state_id;
    CustomPickerVC *customVC;
    
    ServiceNotAvailableView *noServiceView;
    MBProgressHUD *hud;
    HomeBannersView *headerView;
    NSIndexPath *lastSelectedIndex;
    UIActivityIndicatorView *spinner;
    LoginSingupView *loginSingupView ;
    
}

@property (nonatomic, strong) NSString *profileComplete;



@property (nonatomic, strong) HCSStarRatingView *starRatingView;
@property(nonatomic,retain)NSArray *sampleData ;
@property(nonatomic,retain)NSMutableArray *tableData ;
@property(nonatomic,retain)NSDictionary *cellData;
@property(nonatomic,retain)NSMutableDictionary * cellDataOfmore;

@property(nonatomic,retain)NSMutableArray *arrBannerData ;




@property (nonatomic) CGFloat progress;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSArray *progressViews;



@property(nonatomic,retain)UIImageView *imgProfile;
@property(nonatomic,retain)UILabel *lbl_profileComplete;
@property(nonatomic,retain)UILabel *lbl_whiteLine;
@property(nonatomic,retain)UILabel *lbl_percentage;
@property(nonatomic,retain)UIButton *btn_close;
@property(nonatomic,retain)UIButton *btn_clickUpdate;
@property(nonatomic,retain)UIView *NotifycontainerView;

@end

@implementation HomeDataVC_Flag
@synthesize allSer_collectionView;
@synthesize cellData;
@synthesize cellDataOfmore;


@synthesize imgProfile;
@synthesize lbl_profileComplete;
@synthesize lbl_whiteLine;
@synthesize lbl_percentage;
@synthesize btn_close;
@synthesize btn_clickUpdate;
@synthesize NotifycontainerView;
@synthesize profileComplete;


-(void)fetchData
{
    
    UITabBarController *tab=self.tabBarController;
    
    if (tab){
        NSLog(@"I have a tab bar");
        //[self.tabBarController setSelectedIndex:2];
        [self.tabBarController setSelectedIndex:singleton.tabSelectedIndex];

    }
    else{
        NSLog(@"I don't have");
    }
    
    
    NSInteger opentabIndex=self.tabBarController.selectedIndex;
    NSLog(@"opentabIndex=%ld",(long)opentabIndex);
    
    
    
    //------------ Handling loading service if user logout -----------------
    NSInteger currentIndexOfTab=[singleton getSelectedTabIndex];
    NSLog(@"currentIndexOfTab=%ld",(long)currentIndexOfTab);
    
    if (currentIndexOfTab==opentabIndex)
    {
        [self fetchDatafromDB];
        
    }
   /*
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FETCHHOMEDATA" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchData)
                                                 name:@"FETCHHOMEDATA" object:nil];
    */
    
}

-(void)callStateAPI
{
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    
}

-(void)profileCheck
{
//    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
//      //  [self addNotify];
//    }];
    
    
}
-(void)reloadEverything
{
    __block typeof(self) weakSelf = self;
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        //[self performSelector:@selector(reloadAfterDelay) withObject:nil afterDelay:1.0];
        [weakSelf reloadAfterDelay];
    }];
}

-(void)callFlagShipHomeApi {
    CommanAPI *common = [CommanAPI sharedSingleton];
    [common hitwebService_infoScreen];
    
}
- (void)viewDidLoad
{
    
  
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = false;
    [self callFlagShipHomeApi];

    

//    if (iPhoneX())
//    {
//
//        nvSearchView = [[NavigationSearchView alloc] init];
//        NSLog(@"An iPhone X Load UI");
//    }
    
    singleton = [SharedManager sharedSingleton];
    
    self.allSer_collectionView.alwaysBounceVertical = YES;
    //  [ self.allSer_collectionView registerClass:[headerView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderViewAllService"];
    
    flagStatusBar=FALSE;
  /*  [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchData)
                                                 name:@"FETCHHOMEDATA" object:nil];
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileCheck)
                                                 name:@"PROFILECOMPLETE" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadEverything)
                                                 name:@"ReloadEverything" object:nil];
   */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getDataFromNotification:)
                                                 name:@"LOADINFOFLAGDATA" object:nil];
    
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(reloadYourtable)                                                 name:@"CHECKTABLEEMPTYORNOT" object:nil];
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: ALL_SERVICE_TAB_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self adjustSearchBarView];
    }
    
    txt_searchField.placeholder = NSLocalizedString(@"search", @"");
    txt_searchField.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
  //   [self callStateAPI];
    
    if (!refreshController)
    {
        refreshController = [[UIRefreshControl alloc] init];
        //refreshController.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
        [refreshController addTarget:self action:@selector(getEvents:) forControlEvents:UIControlEventValueChanged];
        //self.refreshController = refresh;
        [allSer_collectionView addSubview:refreshController];
        // [self getEvents:refreshController];
    }
    
    if (noServiceView == nil)
    {
        noServiceView = [[ServiceNotAvailableView alloc] initWithFrame:CGRectMake(allSer_collectionView.frame.origin.x, allSer_collectionView.frame.size.height/2, allSer_collectionView.frame.size.width, allSer_collectionView.frame.size.height)];
        [self.view addSubview:noServiceView];
    }
    noServiceView.hidden = YES;
    noServiceView.backgroundColor = allSer_collectionView.backgroundColor;
    allSer_collectionView.delegate = self;
    allSer_collectionView.dataSource = self;
    
    // [allSer_collectionView registerClass:[AllServiceCVCell class] forCellWithReuseIdentifier:@"AllServiceCVCell"];
    
    allSer_collectionView.backgroundColor = [UIColor clearColor];
    
    allSer_collectionView.collectionViewLayout = [[CustomImageFlowLayout alloc] init];
//    if (@available(iOS 9, *)) {
//        [allSer_collectionView setRemembersLastFocusedIndexPath:true];
//    }
    //===== Start Code to add spinner at center of the view=====
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.frame = CGRectMake(fDeviceWidth/2-30, fDeviceHeight/2-30, 60, 60);
    spinner.backgroundColor=[UIColor clearColor];
    [spinner setColor:[UIColor grayColor]];
    //UIViewController *topvc=[self topMostController];
    [self.view addSubview:spinner];
    //===== Start spinner =====
    [spinner startAnimating];
    //
    //load on first time then stop while tab loaded
    //=====End Code to add spinner at center of the view=====
   // [self fetchDatafromDB];
     self.view.backgroundColor= [UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0];
    //self.allSer_collectionView.backgroundColor = self.view.backgroundColor;
   // [self addNavigationSearchTabView];
    BOOL isHintShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"HintShown_Flag"];
    
    // NSLog(isHintShown ? @"Yes" : @"No");
    //[self showHint];
    
    if (!isHintShown)
    {
        
        //hintShown = YES;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HintShown_Flag"];
        
        [self showHint];
        
    }
    else
    {
       // hintShown = NO;
    }
    [self showRetryOption:false];

    
    
//    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                                 action:@selector(handleTap)];
//    tapgesture.numberOfTapsRequired = 1;
//
//    [vw_RetryOption addGestureRecognizer:tapgesture];
  //  "something_went_wrong" = "Something went wrong";
    //"click_here_to_retry" = "Click here to retry";
   // "click_here_to_retry" = "<u>Click here</u> to retry";
    //[allSer_collectionView setContentOffset:CGPointZero animated:false];
   // [allSer_collectionView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self layoutCornerRadiusWithView:vwSearchBG radius:5];

}

-(void)fetchAllDataFromDB {
    _arrBannerData = (NSMutableArray*)[singleton.dbManager getFlagBannerData];
    table_data= (NSMutableArray*)[singleton.dbManager getInfoFlagServiceDataOnly];
    NSLog(@"Arr Banner data --  %@  ---, ",_arrBannerData);
    NSLog(@"Arr Table  data --  %@  ---, ",_tableData);
    [self reloadCollectionView];
    
}

-(void)setupRetryOptionView{
        lblRetryMsg.text = NSLocalizedString(@"something_went_wrong", nil);
        vw_RetryOption.backgroundColor = self.allSer_collectionView.backgroundColor;
        NSString * htmlString = NSLocalizedString(@"click_here_to_retry", nil);
        @try {
            htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx;}</style>",lblRetryMsg.font.fontName,lblRetryMsg.font.pointSize]];    }
        @catch (NSException *exception) {
            NSLog(@"%@", exception.reason);
        }
        @finally {
        }
        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                            documentAttributes:nil
                                                                         error:nil];
        [btnRetryOption setAttributedTitle:attrStr forState:UIControlStateNormal];
        [btnRetryOption setAttributedTitle:attrStr forState:UIControlStateSelected];
    
}
#pragma mark- add Navigation View to View
-(void)checkLoginSingupView {
    if ([SharedManager.sharedSingleton.isLoginViewShow isEqualToString:@"yes"]) {
       // [self showLoginSingupView];
        [self changeCollectionHeight:true];

    }else {
        [self changeCollectionHeight:false];
    }
}
-(void)showLoginSingupView{
    
      if (loginSingupView != nil) {
          [loginSingupView didShowCompleteView:true];
          [self changeCollectionHeight:true];
          return;
       }
        NSLog(@"An iPhone X Load UI");
        loginSingupView = [[LoginSingupView alloc] initWithZeroFrame];
        __weak typeof(self) weakSelf = self;
        loginSingupView.superVC = self;
        vw_line.hidden=TRUE;
        loginSingupView.didTapCrossButton = ^(UIButton* btnfilter)
        {
            [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                [weakSelf changeCollectionHeight:false];

            }];
        };

        [self.view addSubview:loginSingupView];
        loginSingupView.hidden = true;
        [loginSingupView didShowCompleteView:true];
       [self changeCollectionHeight:true];
    
}

-(void)changeCollectionHeight:(BOOL)showLogin {
    __weak typeof(self) weakSelf = self;
    NSTimeInterval time = showLogin ? 0.55 : 0.4;
    CGFloat height = 0.0;
    //CGFloat height;
    NSString *existingLanguageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([UIScreen mainScreen].bounds.size.height == 736)
    {
        height = 150;
        if (![existingLanguageSaved containsString:@"en"]) {
            height = 165;
        }
    }
    else
    {
        height = 170;
        if (![existingLanguageSaved containsString:@"en"]) {
            height = 175;
        }
    }
    if ([[SharedManager sharedSingleton].isLargeFont isEqualToString:@"yes"]) {
        if ([UIScreen mainScreen].bounds.size.height == 736)
        {
            height = 180;
        }
        else
        {
            height = 200;
        }
        
    }
    if (showLogin == false ) {
        height = 0.0;
        for (UIView *subview in [self.view subviews])
        {
            if (subview.tag == 7878) {
                height = 50.0;
            }
        }
    }
//    }else {
//        self.bottomConstraintCollView.constant = height;
//        [self.view layoutIfNeeded];
//        [self.view updateConstraints];
//    }
    [UIView animateWithDuration:time animations:^{
        weakSelf.bottomConstraintCollView.constant = height;
        [weakSelf.view layoutIfNeeded];
        [weakSelf.view updateConstraints];
      } completion:^(BOOL finished) {
        if (showLogin == false) {
            [weakSelf.allSer_collectionView scrollRectToVisible:CGRectMake(0, 0, fDeviceWidth, allSer_collectionView.frame.size.height) animated:false];
        }
    }];
    
}

-(void)checkAppUpdate {
    NSString *newVersion = [NSString stringWithFormat:@"%@",[[SharedManager sharedSingleton].arr_initResponse valueForKey:@"nver"]];
    NSString *showVersion = [[NSUserDefaults standardUserDefaults] valueForKey:APPUPDATE_SHOWN_VERSION];
    // BOOL isRemoved = [[NSUserDefaults standardUserDefaults] boolForKey:APPUPDATE_IS_REMOVED];
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    for (UIView *subview in [self.view subviews])
    {
        if (subview.tag == 7878) {
            [subview removeFromSuperview];
        }
    }
    BOOL isShown = false;
    if ([newVersion integerValue] > [showVersion integerValue] ) {
        
        AppUpdateView *updateView = nil ;
        if ([[UIScreen mainScreen]bounds].size.height == 812)
        {
            updateView  = [[AppUpdateView alloc] initWithFrame:CGRectMake(0, fDeviceHeight-tabBarHeight-NV_height,fDeviceWidth, NV_height)];
        }
        else
        {
            updateView  = [[AppUpdateView alloc] initWithFrame:CGRectMake(0, fDeviceHeight-1.95*NV_height,fDeviceWidth, NV_height)];
            
        }
        if (flagStatusBar==TRUE)
        {
            updateView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
        }
        updateView.tag = 7878;
        __block typeof(self) weakSelf = self;
        updateView.didRemoveApp = ^{
            [updateView removeFromSuperview];
            [weakSelf checkLoginSingupView];
        };
        [self.view addSubview:updateView];
        [self.view bringSubviewToFront:updateView];
        isShown = true;
        CGFloat height = 0.0;
        if (isShown ) {
            height = 50.0;
        }
        [UIView animateWithDuration:0.0 animations:^{
            weakSelf.bottomConstraintCollView.constant = height;
            [weakSelf.view layoutIfNeeded];
            [weakSelf.view updateConstraints];
        } completion:nil];
    }else {
        if ([SharedManager.sharedSingleton.isLoginViewShow isEqualToString:@"yes"]) {
            //  [self showLoginSingupView];
            [self changeCollectionHeight:true];
            
        }else {
            [self changeCollectionHeight:false];
            
        }
    }
    
    
}
- (void)getEvents:(UIRefreshControl *)refresh
{
    static BOOL refreshInProgress = NO;
    
    if (!refreshInProgress)
    {
        __block typeof(self) weakSelf = self;
        refreshInProgress = YES;
        [self fetchDatafromDB];
        [refresh endRefreshing];
        refreshInProgress = NO;
    }
}


-(void)adjustSearchBarView
{
    vwSearchBG.translatesAutoresizingMaskIntoConstraints = true;
    
    vwSearchBG.frame = CGRectMake(self.view.frame.size.width/2 - 150, vwSearchBG.frame.origin.y, 300, vwSearchBG.frame.size.height);
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
//    vwSearchBG.frame = CGRectMake(self.view.frame.size.width/2 - 150, vwSearchBG.frame.origin.y, 300, vwSearchBG.frame.size.height);
//
//    searchIconImage.frame = CGRectMake(vwSearchBG.frame.origin.x + 7, searchIconImage.frame.origin.y, searchIconImage.frame.size.width, searchIconImage.frame.size.height);
//
//    txt_searchField.frame = CGRectMake(searchIconImage.frame.origin.x + searchIconImage.frame.size.width + 5, txt_searchField.frame.origin.y, 265, txt_searchField.frame.size.height);
    
}
#pragma mark- Show Material Showcase to View


-(MaterialShowcase*)showcaseWithtext:(NSString*)text withView:(UIView*)view {
    
    MaterialShowcase *showcase = [[MaterialShowcase alloc] init];
    showcase.backgroundPromptColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:1];
    
    [showcase setTargetViewWithView:view];
    showcase.primaryText =@"";
    __block typeof(self) weakSelf = self;
    [showcase setDelegate:weakSelf];
    showcase.secondaryText = text;
    return  showcase;
    
}
//showCaseDidDismiss(showcase: MaterialShowcase)
-(void)showCaseWillDismissWithShowcase:(MaterialShowcase*)showcase {
    NSLog(@"showcase%@", showcase);
}
-(void)showCaseDidDismissWithShowcase:(MaterialShowcase*)showcase {
    NSLog(@"showcase%@", showcase);
    MaterialShowcase *show = nil ;
    UITextField *lbl = nil ;
    
    
    
    switch (showcase.tag) {
        case 6660:
        {
            if (iPhoneX())
            {
                
                //show = [self showcaseWithtext:NSLocalizedString(@"hint_notif", nil) withView:nvSearchView.notificationBarButton];
                
                
            }
            else
            {
                show = [self showcaseWithtext:NSLocalizedString(@"hint_notif", nil) withView:btn_notification];
            }
            [singleton traceEvents:@"Hint Screen Notification" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
            show.tag = 6661;
        }
            break;
        case 6661:
        {
            if (iPhoneX())
            {
                
              //  show = [self showcaseWithtext:NSLocalizedString(@"hint_sort", nil) withView:nvSearchView.filterBarButton];
                
                
            }
            else
            {
              //  show = [self showcaseWithtext:NSLocalizedString(@"hint_sort", nil) withView:btn_filter];
            }
            
            [singleton traceEvents:@"Hint Screen Sort" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
            show.tag = 6662;
        }
            break;
        case 6662:
        {
            
//            lbl = [[UITextField alloc]init];
//            if (iPhoneX())
//            {
//
//                lbl.frame=CGRectMake(CGRectGetMinX(txt_searchField.frame), CGRectGetMinY(nvSearchView.txtSearchView.frame), 70, CGRectGetHeight(nvSearchView.txtSearchView.frame));
//                lbl.text = nvSearchView.txtSearchView.placeholder;
//
//
//            }
//            else
//            {
//                // lbl = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(txt_searchField.frame), CGRectGetMinY(txt_searchField.frame), 70, CGRectGetHeight(txt_searchField.frame))];
//                lbl.frame=CGRectMake(CGRectGetMinX(txt_searchField.frame), CGRectGetMinY(txt_searchField.frame), 70, CGRectGetHeight(txt_searchField.frame));
//                lbl.text = txt_searchField.placeholder;
//
//            }
//
//
//            lbl.textAlignment = NSTextAlignmentCenter;
//            lbl.textColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:0.70];
//            show = [self showcaseWithtext:NSLocalizedString(@"hint_search", nil) withView:lbl];
//            show.tag = 6663;
            [singleton traceEvents:@"Hint Screen Search" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
        }
            break;
        case 6663:
        {
           // [self showHomeDialogueBox];
        }
        default:
            break;
    }
    if (show != nil ) {
        [show showWithCompletion:nil];
    }
    
}

-(void)showHint
{
    

    
    //MaterialShowcase *showcase = [[MaterialShowcase alloc] init];
    MaterialShowcase *showcase = [self showcaseWithtext:NSLocalizedString(@"hint_notif", nil) withView:_btnShowLogin];
    
    showcase.backgroundPromptColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:1];
    showcase.tag = 6668;
    //showcase.backgroundPromptColorAlpha = 0.96
  //  [showcase setTargetViewWithTabBar:self.tabBarController.tabBar itemIndex:4];
    showcase.primaryText =NSLocalizedString(@"login_to_umang", nil);
    //[showcase setDelegate:self];
    showcase.secondaryText = NSLocalizedString(@"click_login", nil);
    [showcase showWithCompletion:nil];    // 0 59 187
    

    
}
#pragma mark - Navigation Button Action

- (IBAction)didTapRetryOptionAction:(UIButton *)sender {
    [spinner startAnimating];
    [self callFlagShipHomeApi];
}

- (IBAction)didTapHelpSupportBtnAction:(UIButton *)sender {
    if (self.didTapLoginView != nil ) {
        self.didTapHelp(sender);
    }
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    vc.comingFrom = @"accountRecovery";

    [self.tabBarController.navigationController pushViewController:vc animated:YES];
}

- (IBAction)didTapLoginSingupBtnAction:(UIButton *)sender {
    if (self.didTapLoginView != nil ) {
        self.didTapLoginView(sender);
        [self checkLoginSingupView];
    }
}

#pragma mark -
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    // [allSer_collectionView.collectionViewLayout invalidateLayout];
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    
}
-(void)didTapChangeStateButtonAction:(UIButton*)sender
{
    [singleton traceEvents:@"Change State Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    customVC = [storyboard instantiateViewControllerWithIdentifier:@"CustomPickerVC"];
    
    //NSArray *arrState=[obj getStateList];
    //arrState = [arrState sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSArray  * servicesArray = [singleton.dbManager getServiceStateAvailable];
    NSArray *upcomingServicesArray = [singleton.dbManager getUpcommingStates];
    
    servicesArray = [servicesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    upcomingServicesArray = [upcomingServicesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    customVC.delegate=self;
    customVC.get_title_pass = NSLocalizedString(@"select_state", nil);
    customVC.get_arr_element=[servicesArray mutableCopy];
    customVC.arrUpcomingState = [upcomingServicesArray mutableCopy];
    customVC.get_TAG=TAG_ALLSERVICE_STATE;
    
  //  customVC.allServiceState = [headerView.btnStateSelected titleForState:UIControlStateNormal];
    __weak __typeof(self) weakSelf = self;
    customVC.finishState = ^(NSString *stateID) {
        
        state_id = stateID;
        
        state_id = state_id.length == 0 ? @"" : state_id;
        
        [[NSUserDefaults standardUserDefaults] setObject:state_id forKey:@"AllTabState"];
        [hud hideAnimated:YES];
        
        [weakSelf hitSetStateAPI:stateID];
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [weakSelf loadStateService];
        }];
    };
    // [customVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:customVC animated:NO completion:nil];
    [self.navigationController pushViewController:customVC animated:YES];
}
- (IBAction)segmentDidChangeValue:(UISegmentedControl *)sender
{
    
    switch (sender.selectedSegmentIndex) {
        case 0:
            [singleton traceEvents:@"All Segment" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
            [self reloadAfterDelay];
            break;
        case 1:
            [singleton traceEvents:@"Central Segment" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
            [self loadCentralServiceList];
            break;
        case 2:
            [singleton traceEvents:@"State Segment" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
            //break;
            [self loadStateService];
            break;
        default:
            // [self reloadAfterDelay];
            break;
    }
}
-(void)loadCentralServiceList
{
    
    singleton = [SharedManager sharedSingleton];
    NSArray *arrServiceData=[singleton.dbManager getCentralServiceData];
    NSLog(@"arrServiceData=%@",arrServiceData);
    table_data=[[NSMutableArray alloc]init];
    table_data=[arrServiceData mutableCopy];
    NSSortDescriptor *aphabeticDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
    table_data = [[table_data sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    allSer_collectionView.scrollEnabled = true;
    
   // [self addNotify];//add later
    [self reloadCollectionView];
    //[allSer_collectionView reloadData];
}
-(NSString*)getStateName
{
    obj=[[StateList alloc]init];
    state_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"AllTabState"];
    if (state_id.length != 0)
    {
        [self hitSetStateAPI:state_id];
    }
    return [obj getStateName:state_id];
}
-(void)loadStateService
{
    [hud hideAnimated:YES];
    allSer_collectionView.scrollEnabled = true;
    
    obj=[[StateList alloc]init];
    
    state_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"AllTabState"];
    
    if (state_id.length != 0)
    {
        [self hitSetStateAPI:state_id];
    }
    
    NSString *strState = [obj getStateName:state_id];
    
    singleton = [SharedManager sharedSingleton];
    
    NSArray *arrServiceData = nil ;
    if ([state_id isEqualToString:@"9999"] || [state_id isEqualToString:@""] || [[strState uppercaseString] isEqualToString:@"ALL"] || strState == nil || strState.length == 0)
    {
        strState = NSLocalizedString(@"all", nil);
        arrServiceData = [[singleton.dbManager getAllServiceDataNotCentral] mutableCopy];
    }else {
        arrServiceData = [[singleton.dbManager getServiceStateData:state_id] mutableCopy];
    }
    
    table_data=[[NSMutableArray alloc]init];
    NSSortDescriptor *aphabeticDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
    table_data = [[arrServiceData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    NSLog(@"table_data=%@",table_data);
    
    if ([table_data count]==0) {
        allSer_collectionView.scrollEnabled = false;
    }
 //   [self addNotify];//add later
    [self reloadCollectionView];
    //[allSer_collectionView reloadData];
    
}



- (void)closeBtnAction:(id)sender
{
    
    [singleton traceEvents:@"Close ProfileBar Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight );
    
    
    if (iPhoneX())
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        float yX= kiPhoneXNaviHeight4Search;
        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
        
        float heightX = heightVar-tabBarHeight ;
        allSer_collectionView.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
        
    }
    
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"SHOW_PROFILEBAR"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [NotifycontainerView removeFromSuperview];
    }];
    
    
}
-(void)setNoServiceFrame
{
    noServiceView.hidden = YES;
    vw_RetryOption.hidden = true;
    if (table_data.count == 0 ) {
        [self setupRetryOptionView];
        [self showRetryOption:true];
    }
    //[self showRetryOption:true];
    [self.view layoutIfNeeded];
}
#pragma mark -

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitSetStateAPI:(NSString*)stateId
{
    if ([stateId isEqualToString:@"9999"] || [stateId isEqualToString:@""])
    {
        return;
    }
    
    [hud hideAnimated:YES];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"status"];//This is Status of the account
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    // [dictBody setObject:stateId forKey:@"st"];  //get from mobile default email //not supported iphone
    
    [dictBody setObject:stateId forKey:@"stid"];  //get from mobile default email //not supported iphone
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_GETAMBLEM withBody:dictBody andTag:TAG_REQUEST_STATE completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         if (error == nil)
         {
             NSLog(@"Server Response = %@",response);
             //----- below value need to be forword to next view according to requirement after checking Android apk-----
             NSString *rc=[response valueForKey:@"rc"];
             NSString *rs=[response valueForKey:@"rs"];
             NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
             NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
             //----- End value need to be forword to next view according to requirement after checking Android apk-----
             NSString *rd=[response valueForKey:@"rd"];
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                 
             {
                 NSString*  stemblem=[[response valueForKey:@"pd"] valueForKey:@"stemblem"];
                 if (noServiceView != nil) {
                     [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                         [noServiceView setBackgroundImageWithUrl:stemblem];
                     }];
                     // [noServiceView setBackgroundImageWithUrl:stemblem];
                 }
                 /* {"rs":"S","rc":"00","rd":"Successful","pd":{"ostate":"20","abbr":"Haryana","stname":"Haryana","stemblem":"https://static.umang.gov.in/app/ico/emblem/haryana.png"},"gcmid":"","ntfp":"","ntft":"","plang":"","node":""}
                  
                  */
             }
             
         }
         else{
             NSLog(@"Error Occured = %@",error.localizedDescription);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                             message:error.localizedDescription
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                   otherButtonTitles:nil];
             [alert show];
             
         }
         
     }];
    
}



#pragma mark -
- (void)updateProgress
{
    // NSString* str = [NSString stringWithFormat:@"%@", profileComplete];
    NSString* str =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"PROFILE_COMPELTE_KEY"];//[NSString stringWithFormat:@"%@", profileComplete];
    
    if (str == nil)
    {
        str=@"0";
    }
    if ([str length]==0)
    {
        str=@"0";
        
    }
    
    
    CGFloat value = [str floatValue]/100;
    lbl_percentage.text=  [NSString stringWithFormat:@"%@%%",str];
  //  self.progress = value;
    
    
    
    [self.progressViews enumerateObjectsUsingBlock:^(THProgressView *progressView, NSUInteger idx, BOOL *stop) {
        [progressView setProgress:self.progress animated:YES];
        
    }];
    
}



-(void)clickUpdateAction:(id)sender
{
    
    // Reset here for login with registration here
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLoginWithRegistration"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    
    SharedManager *singleton  = [SharedManager sharedSingleton];
    singleton.shared_mpinflag = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinflag"];
    singleton.shared_mpinmand = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinmand"];
    if ([[singleton.shared_mpinflag lowercaseString] isEqualToString:@"false"])
    {
        
        NSLog(@"MPIN SHOWING STATUS IS FALSE, so  show it");
        
        dispatch_async(dispatch_get_main_queue(),^{
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate showMPIN_AlertBox];
            [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
             delegate.vw_MPIN_AlertBox.lbl_subtitle1.text = NSLocalizedString(@"setting_mpin_mandatory_this_screen", nil);
        });
        /*
         NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
         [[NSUserDefaults standardUserDefaults]synchronize];
         if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]||[lastTime isEqualToString:@"NR"])
         {
         // dont show first time
         }
         else
         {
         // ========= check to open show mpin alert box=====
         dispatch_async(dispatch_get_main_queue(),^{
         AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
         [delegate showHideSetMpinAlertBox ];
         [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
         
         });
         }
         */
    }
    else
    {
        // do nothing
        NSLog(@"MPIN SHOWING STATUS IS TRUE, so dont show it");
        
    [singleton traceEvents:@"Show User Profile Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    }
    
    
}



//----- End----------

//------- End of delegate methods of update profile------------



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
   
  //  [allSer_collectionView setContentOffset:CGPointZero animated:false];
    //[allSer_collectionView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    singleton = [SharedManager sharedSingleton];

    //[allSer_collectionView sendSubviewToBack:refreshController];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    //  [allSer_collectionView setScrollsToTop:YES];
    
    //------------- Network View Handle------------
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    //------------- Tab bar Handle------------
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    //appDelegate.shouldRotate = YES;
    // [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];
    //------------- Tab bar Handle------------
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
    vwSearchBG.layer.cornerRadius = 5.0;
    [txt_searchField setValue:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    // allSer_collectionView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
    
    
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.navigationBarHidden = true ;
    
    // UIEdgeInsets inset = UIEdgeInsetsMake(-20, 0, -50, 0);
    // allSer_collectionView.contentInset = inset;
    
    vw_line.frame=CGRectMake(0, 74, fDeviceWidth, 0.5);
    
    [self setNeedsStatusBarAppearanceUpdate];
    // [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:NSLocalizedString(@"home_small", @"")];
    
    // [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:NSLocalizedString(@"favourites_small", @"")];
    
    //[[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:NSLocalizedString(@"all_services_small", @"")];
    // [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:NSLocalizedString(@"help_live_chat", @"")];
    
    
    // [[self.tabBarController.tabBar.items objectAtIndex:4] setTitle:NSLocalizedString(@"more", @"")];
    
    
    
    
    flagrotation=TRUE;
    
    
    
   
    //[allSer_collectionView setContentOffset:CGPointMake(0,0) animated:YES];
    
    appDel.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"appDelegate.badgeCount=%d",appDelegate.badgeCount);
    
    //-------- Added later-------------
   /*  [self badgeHandling];
 
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(badgeHandling) name:@"NotificationRecievedComplete" object:nil];
    */
    
    self.tabBarController.tabBar.itemPositioning = UITabBarItemPositioningCentered;
    [hud hideAnimated:YES];
    
    if (refreshController)
    {
        [refreshController endRefreshing];
        [self.allSer_collectionView sendSubviewToBack:refreshController];
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
        
    }
    else
    {
       
    }
    
    [self setViewFont];
    [self showRetryOption:false];
    [self checkAppUpdate];
}


#pragma mark- Font Set to View
-(void)setViewFont{
    [btnRetryOption.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblRetryMsg.font = [AppFont regularFont:17.0];
    txt_searchField.font = [AppFont mediumFont:13.0];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
    //[_btnStateSelected.titleLabel setFont:[AppFont regularFont:18.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    // txt_searchField.font = [AppFont mediumFont:13.0];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
#pragma mark -


-(void)badgeHandling
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //open for testing purpose
    // NSString *badge=[NSString stringWithFormat:@"%d",1];
    //[[NSUserDefaults standardUserDefaults] setObject:badge forKey:@"BadgeValue"];
    
    app.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"appDelegate.badgeCount=%d",app.badgeCount);
    NSString*badgeValue=[NSString stringWithFormat:@"%d",app.badgeCount];
    
    if (app.badgeCount>0)
    {
        
        if (app.badgeCount>=10)      // 3 and 2 digit digit case
        {
            CustomBadge *badge = [CustomBadge customBadgeWithString:badgeValue withScale:.9];
            CGSize size = CGSizeMake(badge.frame.size.width, badge.frame.size.height);
            
            //CGPoint point = CGPointMake(btn_notification.frame.size.width+5-badge.frame.size.width, -14);
//            CGPoint  point = [UIScreen mainScreen].bounds.size.height == 812.0 ? CGPointMake(loginSingupView.notificationBarButton.frame.size.width+5-badge.frame.size.width, -14) :  CGPointMake(btn_notification.frame.size.width+5-badge.frame.size.width, -14);
//
//            CGRect rect = CGRectMake(point.x, point.y, size.width, size.height+8);
//            [badge setFrame:rect];
//            badge.userInteractionEnabled=NO;
//            badge.tag=786;
//
//            //  [btn_notification addSubview:badge];
//
//
//            [([UIScreen mainScreen].bounds.size.height == 812.0 ? loginSingupView.notificationBarButton : btn_notification) addSubview:badge];
            
        }
        else
        {
            CustomBadge * badge = [CustomBadge customBadgeWithString:badgeValue];
            //CGPoint point = CGPointMake(btn_notification.frame.size.width/2+8-badge.frame.size.width/2, -8);
            
//          //  CGPoint  point = [UIScreen mainScreen].bounds.size.height == 812.0 ? CGPointMake(loginSingupView.notificationBarButton.frame.size.width/2+8-badge.frame.size.width/2, -8) : CGPointMake(btn_notification.frame.size.width/2+8-badge.frame.size.width/2, -8);
//
//            CGRect rect = CGRectMake(point.x, point.y, 21, 21);
//            [badge setFrame:rect];
//            badge.userInteractionEnabled=NO;
//            badge.tag=786;
//            // [btn_notification addSubview:badge];
//            [([UIScreen mainScreen].bounds.size.height == 812.0 ? loginSingupView.notificationBarButton : btn_notification) addSubview:badge];
            
            
        }
        
        
        
    }
    else
    {
        // for (UIView *subview in [btn_notification subviews])
//        for (UIView *subview in [([UIScreen mainScreen].bounds.size.height == 812.0 ? loginSingupView.notificationBarButton : btn_notification) subviews])
//
//        {
//            if (subview.tag ==786) {
//                [subview removeFromSuperview];
//            }
//
//        }
        
        
    }
    
    
}


-(void)fetchDatafromDB
{
    //===== Start spinner =====
    // [spinner startAnimating];
    __block typeof(self) weakSelf = self;
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
       // [self performSelector:@selector(reloadAfterDelay) withObject:nil afterDelay:1.0];
        [weakSelf reloadAfterDelay];
    }];
}
- (void) getDataFromNotification:(NSNotification *) notification

{
//    if (spinner != nil) {
//         [spinner startAnimating];
//    }
    [self reloadEverything];

    
} 

-(void)reloadAfterDelay
{
   
        allSer_collectionView.scrollEnabled = true;
        singleton = [SharedManager sharedSingleton];
         [self fetchAllDataFromDB];
         [self setNoServiceFrame];
        [spinner stopAnimating];
        
    
}
-(void)reloadCollectionView{

    
    __block typeof(self) weakSelf = self;
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [weakSelf showRetryOption:false];
        if (table_data.count == 0) {
            [weakSelf showRetryOption:true];
        }
        [weakSelf.allSer_collectionView reloadData];
    }];
    
}
-(void)showRetryOption:(BOOL)show {
    vw_RetryOption.hidden = !show;
    self.allSer_collectionView.hidden = show;
    [self.view bringSubviewToFront:vw_RetryOption];
    
}
/*
 -(void)setupData
 {
 self.selectedMarks = [[NSMutableArray alloc] init];
 
 SharedManager *singleton=[SharedManager sharedSingleton];
 
 NSArray *fav_Arr=[singleton.fav_Mutable_Arr mutableCopy];
 
 table_data=[[NSMutableArray alloc]init];
 
 for (int i = 1; i < [fav_Arr count]; i++)
 {
 
 
 NSDictionary *cellData1 = [fav_Arr objectAtIndex:i];
 NSArray *AllServicesData = [cellData1 objectForKey:@"AllServices"];
 
 
 NSLog(@"cellData=%@ and AllServicesData=%@",cellData1,AllServicesData);
 
 for (int i=0; i<[AllServicesData count]; i++) {
 
 
 
 
 NSLog(@"Not Selected!");
 [table_data addObject:[AllServicesData objectAtIndex:i]];
 
 
 
 }
 }
 
 NSLog(@"tableData=%@",table_data);
 [allSer_collectionView reloadData];
 
 }*/

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
                        layout:(UICollectionViewLayout *) collectionViewLayout
        insetForSectionAtIndex:(NSInteger) section {
   
    return UIEdgeInsetsMake(10,13,0, 16); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
                   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    return 5.0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return table_data.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
#pragma mark - Banner Selection
- (void)didTapBannerWithBanner:(NSDictionary *)banner {
    NSLog(@"banner dic tap --%@", banner);
    [self selectedIndexNotify:banner];
}
-(void)selectedIndexNotify:(NSDictionary*)bannerData
{
    SharedManager *singleton=[SharedManager sharedSingleton];
    
    //----------Handle case for subType---------------
    NSString *subType =[bannerData valueForKey:@"BANNER_ACTION_TYPE"];
    //subType=[subType  lowercaseString];
    // Case to open app  for opening app with notification title/
    if([subType isEqualToString:@"openApp"])
    {
        //handle case in delegate for it do nothing
        
    }
    // Case to open app  with dialog message dialogmsg/msg
    
    else if([subType isEqualToString:@"openAppWithDialog"])
    {
        NSString *dialogMsg =[bannerData valueForKey:@"BANNER_DESC"];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:dialogMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
        
    }
    // Case to open  playstore url in external
    
    else if([subType isEqualToString:@"playstore"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[bannerData valueForKey:@"BANNER_ACTION_URL"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        
        
    }
    // Case to open app  inside webview with custom webview title
    
    else if([subType isEqualToString:@"webview"])
    {
        NSString *stringdata=[bannerData valueForKey:@"BANNER_ACTION_URL"];
        NSArray *stringArray = [stringdata componentsSeparatedByString: @"|"];
        NSString *url=[stringArray objectAtIndex:0];
        NSString *title=[stringArray objectAtIndex:1];
        [self openFAQWebVC:url withTitle:title];
        
    }
    // Case to open app  in mobile browser
    
    else if([subType isEqualToString:@"browser"]||[subType isEqualToString:@"youtube"]||[subType isEqualToString:@"url"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[bannerData valueForKey:@"BANNER_ACTION_URL"]];
        
        NSString* webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL* urltoOpen = [NSURL URLWithString:webStringURL];
        
        [[UIApplication sharedApplication] openURL:urltoOpen];
    }
    // Case to open app  with Screen Name like profile/settings etc
    
    else if([subType isEqualToString:@"openAppWithScreen"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[bannerData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        
        if ([screenName isEqualToString:@"settings"])
        {
            [self mySetting_Action];
            
        }
        if ([screenName isEqualToString:@"help"])
        {
            [self myHelp_Action];
            
        }
        else
        {
            //main
        }
        
        
    }
    // Case to open app  with tab name
    
    else if([subType isEqualToString:@"openAppWithTab"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[bannerData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        
        if ([screenName isEqualToString:NSLocalizedString(@"home_small", nil)])
        {
            tbc.selectedIndex=0;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"favourites_small", nil)])
        {
            tbc.selectedIndex=1;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"states", nil)])
        {
            //tbc.selectedIndex=0;ignore case
            
        }
        if ([screenName isEqualToString:  NSLocalizedString(@"all_services_small", nil)])
        {
            tbc.selectedIndex=2;
            
        }
        
        
        UIViewController *topvc=[self topMostController];
        [topvc presentViewController:tbc animated:NO completion:nil];
        
        
        
    }
    // Case to open app with service
    
    else  if([subType isEqualToString:NSLocalizedString(@"services", nil)]||[subType isEqualToString:@"service"])
    {
        
        if (bannerData)
        {
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            // BANNER_ACTION_URL
            
            // SERVICE_ID_to_Pass=[dic_serviceInfo valueForKey:@"SERVICE_ID"];
            //titleStr=[dic_serviceInfo valueForKey:@"SERVICE_NAME"];
            // _lbltitle.text = titleStr;
            //urlString=[dic_serviceInfo valueForKey:@"SERVICE_URL"];
            
            NSString *stringdata=[bannerData valueForKey:@"BANNER_ACTION_URL"];
            
            NSArray *stringArray = [stringdata componentsSeparatedByString: @"|"];
            
            NSMutableDictionary *bannerActionData=[NSMutableDictionary new];
            
            @try {
                
                NSString *service_url=[stringArray objectAtIndex:0];
                NSString *service_title=[stringArray objectAtIndex:1];
                NSString *service_id=[stringArray objectAtIndex:2];
                
                
                [bannerActionData setObject:service_id forKey:@"SERVICE_ID"];
                [bannerActionData setObject:service_title forKey:@"SERVICE_NAME"];
                [bannerActionData setObject:service_url forKey:@"SERVICE_URL"];
                
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            NSString *serviceId = [bannerData valueForKey:@"SERVICE_ID"];
            NSString *service_type =[singleton.dbManager getInfoFlagServiceCardType:serviceId];
            
            if (![service_type isEqualToString:@"I"])
            {
                if (self.didTapLoginViewForBanner != nil ) {
                    self.didTapLoginViewForBanner([UIButton new]);
                    [self checkLoginSingupView];
                }
               // [self showLoginSingupView];
                
            }else {
                HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
                [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                vc.dic_serviceInfo=bannerActionData;
                vc.tagComeFrom=@"OTHERS";
                vc.sourceState   = @"";
                //            if ([self.comingFrom isEqualToString:@"home"])
                //            {
                //                vc.sourceTab     = @"home";
                //            }
                //            else
                //            {
                //                vc.sourceTab     = @"state";
                //                vc.sourceState   = self.stateIdToPass;
                //            }
                
                vc.sourceSection = @"banner";
                vc.sourceBanner  = [NSString stringWithFormat:@"%@",[bannerData valueForKey:@"BANNER_ID"]];
                
                UIViewController *topvc=[self topMostController];
                [topvc presentViewController:vc animated:NO completion:nil];
            }
            
            
            
            
            
            
        }
        
    }
    // Case to open app  for rating view
    
//    else  if([subType isEqualToString:NSLocalizedString(@"Rating", nil)] || [subType isEqualToString:@"rating"])
//    {
//        [self rateUsClicked];
//        
//    }
    // Case to open app  for share [sharing message will recieve inside api)
    else  if([subType isEqualToString:@"share"] || [subType isEqualToString:NSLocalizedString(@"share", nil)])
    {
        [self shareContent];
    }
    //Default case
    else
    {
        
        
    }
    
    
}


-(void)shareContent
{
    SharedManager *singleton=[SharedManager sharedSingleton];
    NSString *textToShare =singleton.shareText;
    
    
    NSArray *objectsToShare = @[textToShare];
    
    [[UIPasteboard generalPasteboard] setString:textToShare];
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    
    UIViewController *topvc=[self topMostController];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [topvc presentViewController:controller animated:YES completion:nil];
        
    }
    
    //if iPad
    
    else {
        
        // Change Rect to position Popover
        
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        [popup presentPopoverFromRect:CGRectMake(topvc.view.frame.size.width/2, topvc.view.frame.size.height/4, 0, 0)inView:topvc.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}


//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)openFAQWebVC:(NSString *)url withTitle:(NSString*)title
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    vc.hidesBottomBarWhenPushed = true;
    
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
   // UIViewController *topvc=[self topMostController];//
   // [topvc.navigationController pushViewController:vc animated:YES];
    
    [self.navigationController pushViewController:vc animated:true];
    
    
}

-(void)mySetting_Action
{
     UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    ///[self.navigationController pushViewController:vc animated:YES];
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
}
-(void)myHelp_Action
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSLog(@"My Help Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    vc.comingFrom = @"accountRecovery";

    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
}


#pragma mark - header View
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader)
    {
        
        headerView  = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderCollectionReusableView" forIndexPath:indexPath];
        headerView.didSelect = self;
        [headerView setDataSourceWithArrBaners:_arrBannerData];
        return  headerView;
    }
    return reusableview;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGFloat height = 120;
    if (iPhoneX())
    {
        
        //return 150;
        height = 125;
        
    }
    
    else if ([[UIScreen mainScreen]bounds].size.height == 812)
    {
        
        //return 150;
        height = 135;
        
    }
    
    else if ([[UIScreen mainScreen]bounds].size.height >= 815)
    {
        
        //return 150;
        height = 215;
        
    }
    else if ([[UIScreen mainScreen]bounds].size.height == 736)
    {
        
        //return 150;
        height = 135;
        
    }
    else if ([[UIScreen mainScreen]bounds].size.height == 414)
    {
        
        //return 150;
        height = 135;
        
    }
    else if ([[UIScreen mainScreen]bounds].size.height == 480)
    {
        
        height = 105;
        
        
    }
    else if ([[UIScreen mainScreen]bounds].size.height == 568)
    {
        
        height = 105;
        
        
    }
    else
    {

        height =  120;
    }
    return CGSizeMake(fDeviceWidth, height);
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat height = 135 ;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        height = 220;
        
    }else {
        if (fDeviceWidth == 320)
        {
            height = 135;
        }
        else if (fDeviceWidth == 375)
        {
            height = 145;
        }
        else if (fDeviceWidth > 375)
        {
            height = 155;
        }
        
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return CGSizeMake((fDeviceWidth - 75 ) / 4.0, height);
    }
    return  CGSizeMake((fDeviceWidth - 60 ) / 3.0, height);
  
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[ UIScreen mainScreen] bounds].size.height >= 1024)
    {
        static NSString *identifier = @"AllServiceCVCellForiPad";
        AllServiceCVCellForiPad *allCellipad = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        @try
        {
            
            allCellipad.serviceTitle.text =[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row]];
            allCellipad.serviceTitle.font = [UIFont systemFontOfSize:18];
            
            
            NSURL *url=[NSURL URLWithString:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
            
            [allCellipad.serviceImage sd_setImageWithURL:url
                                        placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
            int tagvalue= (int)(indexPath.row);
            allCellipad.serviceFav.hidden=true;
            
            
            //            UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
            //            UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
            //
            //            [allCellipad.serviceFav setImage:btnImage1 forState:UIControlStateNormal];
            //            [allCellipad.serviceFav setImage:btnImage2 forState:UIControlStateSelected];
            
            
            
            NSString *serviceid=[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
            
            NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceid]];//get service status from db
            
            if ([serviceFav isEqualToString:@"true"])
            {
                allCellipad.serviceFav .selected=YES;
            }else{
                allCellipad.serviceFav .selected=NO;
            }
            
            
            cellData = (NSDictionary*)[table_data objectAtIndex:[indexPath row]];
            
            allCellipad.serviceInfo.celldata=[cellData mutableCopy];
            
            
            //allCellipad.serviceInfo.hidden = true;
            [allCellipad.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            allCellipad.serviceFav.usdata=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_NAME"]];
            [allCellipad.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *rating=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_RATING"]];
            
            float fCost = [rating floatValue];
            
            allCellipad.starRatingView.value = fCost;
            //[allCellipad.starRatingView setScore:fCost*2 withAnimation:NO];
            
            allCellipad.serviceTitle.font = [AppFont regularFont:14];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        //[allCellipad layoutIfNeeded];
        return allCellipad;
        
        
    }
    else
    {
        static NSString *identifier = @"AllServiceCVCell";
        AllServiceCVCell *allCell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        @try {
            allCell.serviceTitle.text =[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row]];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                allCell.serviceTitle.font = [UIFont systemFontOfSize:18];
            }
            NSURL *url=[NSURL URLWithString:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
            
            [allCell.serviceImage sd_setImageWithURL:url
                                    placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
            int tagvalue= (int)(indexPath.row);
            allCell.serviceFav.tag=tagvalue;
            allCell.serviceFav.hidden=true;

            
            //            UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
            //            UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
            //
            //            [allCell.serviceFav setImage:btnImage1 forState:UIControlStateNormal];
            //            [allCell.serviceFav setImage:btnImage2 forState:UIControlStateSelected];
            
            
            
            NSString *serviceid=[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
            
            NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceid]];//get service status from db
            
            if ([serviceFav isEqualToString:@"true"])
            {
                allCell.serviceFav .selected=YES;
            }else{
                allCell.serviceFav .selected=NO;
            }
            
            
            cellData = (NSDictionary*)[table_data objectAtIndex:[indexPath row]];
            
            allCell.serviceInfo.celldata=[cellData mutableCopy];
            
             //allCell.serviceInfo.hidden = true;
            [allCell.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            allCell.serviceFav.usdata=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_NAME"]];
            [allCell.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *rating=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_RATING"]];
            
            float fCost = [rating floatValue];
            
            allCell.starRatingView.value = fCost;
            //[allCell.starRatingView setScore:fCost*2 withAnimation:NO];
            
            allCell.serviceTitle.font = [AppFont regularFont:14];
        }
        @catch (NSException *exception)
        {
            
        }
        @finally
        {
            
        }
        
        //[allCell layoutIfNeeded];
        
        return allCell;
        
        
    }
    
    
    
    
}
#pragma mark - UICollection View Delegates

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    lastSelectedIndex = indexPath;
    [singleton traceEvents:@"Select Department" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.dic_serviceInfo=(NSDictionary*)[table_data objectAtIndex:indexPath.row];
    vc.tagComeFrom=@"OTHERS";
    
    
    
    
    [self presentViewController:vc animated:NO completion:nil];
    
    
}


//==================================
//       CUSTOM PICKER STARTS
//==================================
- (void)btnOpenSheet

{
    NSString *information=NSLocalizedString(@"information", nil);
    NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self callDetailServiceVC];
        }
            break;
        case 1:
        {
            NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
            [singleton traceEvents:@"Visit Map Option" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
            
            NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
            
            NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
            
            NSString *deptName=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_NAME"]];
            deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]])
                
            {
                NSLog(@"Map App Found");
                
                NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
                
                // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
                
                NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSLog(@"mapURL= %@",mapURL);
                
                [[UIApplication sharedApplication] openURL:mapURL];
                
            } else
            {
                NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
                
            }
            
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

//==================================
//          CUSTOM PICKER ENDS
//==================================

//------------ Show more info-------------
-(IBAction)moreInfo:(MyButton*)sender
{
    NSLog(@"Data 1 = %@",sender.celldata);
    [singleton traceEvents:@"More Department Option Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    self.cellDataOfmore= [[NSMutableDictionary alloc]init];
    cellDataOfmore=(NSMutableDictionary*)sender.celldata;
    [self btnOpenSheet];
    
    /*
     NSString *titlename=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_NAME"]];
     
     //  NSString *titleDesc=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_DESC"]];
     
     //  NSString *imageName=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_IMAGE"]];
     
     //  NSString *Type=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_CATEGORY"]];
     
     
     // [self displayContentController:[self getHomeDetailLayerLeftController]];
     
     
     UIAlertController * alert=   [UIAlertController
     alertControllerWithTitle:titlename
     message:nil
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* info = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"information", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     // [self displayContentController:[self getHomeDetailLayerLeftController]];
     
     [self callDetailServiceVC];
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* map = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"view_on_map", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     
     NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
     
     NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
     
     NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
     
     
     //  NSString* googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@",latitude, longitute];
     
     
     
     // [[UIApplication sharedApplication] openURL: [NSURL URLWithString: googleMapsURLString]];
     
     if ([[UIApplication sharedApplication] canOpenURL:
     [NSURL URLWithString:@"comgooglemaps://"]])
     
     {
     NSLog(@"Map App Found");
     
     NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f&center=%.6f,%.6f&zoom=15&views=traffic", [latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     
     
     
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
     
     
     } else
     {
     NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
     
     }
     
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* cancel = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"cancel", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     
     
     [alert addAction:info];
     [alert addAction:map];
     
     [alert addAction:cancel];
     
     
     UIViewController *vc=[self topMostController];
     [vc presentViewController:alert animated:NO completion:nil];
     */
}

-(void)callDetailServiceVC
{
    [singleton traceEvents:@"Department Information Option" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    vc.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    //UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [self presentViewController:vc animated:NO completion:nil];
    
}



-(itemMoreInfoVC*)getHomeDetailLayerLeftController
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    itemMoreVC = [storyboard instantiateViewControllerWithIdentifier:@"itemMoreInfoVC"];
    
    itemMoreVC.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    
    return itemMoreVC;
}


- (void) displayContentController: (UIViewController*) content;
{
    UIViewController *vc=[self topMostController];
    
    [vc addChildViewController:content];
    
    
    content.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    
    [vc.view addSubview:content.view];
    [content didMoveToParentViewController:vc];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil)
    //{
    //itemMoreVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        itemMoreVC.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil) {
    itemMoreVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        itemMoreVC.view.frame = CGRectMake(vc.view.frame.size.width, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}


//----------- END OF MORE INFO POP UP VIEW---------------

//- (UIViewController*) topMostController
//{
//    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
//
//    while (topController.presentedViewController) {
//        topController = topController.presentedViewController;
//    }
//
//    return topController;
//}



-(IBAction)fav_action:(MyFavButton*)sender
{
    [singleton traceEvents:@"Favourite Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    MyFavButton *button = (MyFavButton *)sender; //instance of UIButton
    int indexOfTheRow=(int)button.tag;   //tag of the button
    // NSLog(@"tabledata=%@",[table_data objectAtIndex:indexOfTheRow]);
    
    // Add image to button for pressed state
    //    UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
    //    UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
    //
    //    [button setImage:btnImage1 forState:UIControlStateNormal];
    //    [button setImage:btnImage2 forState:UIControlStateSelected];
    
    NSString *serviceId=[NSString stringWithFormat:@"%@",[[table_data objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_ID"]];
    
    NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceId]];
    
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
        button.selected=FALSE;
    }
    else
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
        button.selected=true;
    }
    
}





//-----------for filterview show----------
-(IBAction)btn_filterAction:(id)sender
{
    //code to open filter view here
    [singleton traceEvents:@"Filter Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    
    NSLog(@"Filter Pressed");
    
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    AddallserviceFilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddallserviceFilterVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    // [self presentViewController:vc animated:NO completion:nil];
    vc.hidesBottomBarWhenPushed = YES;
    
    
    [self.navigationController pushViewController:vc animated:YES];
    
}





//-----------for notification view show---
-(IBAction)btn_noticationAction:(id)sender
{
    //code to open notification view here
    [singleton traceEvents:@"Notification Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    
    NSLog(@"Notification Pressed");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
    
    ScrollNotificationVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ScrollNotificationVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:vc animated:NO completion:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
    
}
/*
 - (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
 [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
 }];
 return [super canPerformAction:action withSender:sender];
 }
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == txt_searchField)
    {
        [self openSearch];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)openSearch
{
    
    [singleton traceEvents:@"Advanced Search Bar" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    AdvanceSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AdvanceSearchVC"];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:vc animated:NO completion:nil];
    
    
    
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
