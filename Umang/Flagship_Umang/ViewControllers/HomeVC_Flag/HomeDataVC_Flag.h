//
//  HomeDataVC_Flag.h
//  UMANG
//
//  Created by Rashpinder on 07/09/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "HeaderCollectionReusableView.h"

#import "AllServiceCVCell.h"
#import "CustomImageFlowLayout.h"
#import "LoginSingupView.h"

#import "ScrollNotificationVC.h"
#import "HomeDetailVC.h"
#import "AdvanceSearchVC.h"

#import "UIImageView+WebCache.h"
#import "itemMoreInfoVC.h"
#import "DetailServiceNewVC.h"

#import "AddallserviceFilterVC.h"
#import "AllServiceCVCellForiPad.h"
#import "HomeTabVC.h"
#import "UserProfileVC.h"
#import "UIView+MGBadgeView.h"
#import "ShowUserProfileVC.h"

#import "CustomBadge.h"
#import "BadgeStyle.h"
#import "CustomPickerVC.h"

#import "StateList.h"
#import "RunOnMainThread.h"

#import "ServiceNotAvailableView.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"

#import "AddFavFilterVC.h"
#import "SharedManager.h"
#import "TabBarVC_Flag.h"

@interface HomeDataVC_Flag : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate> {
    IBOutlet UITextField *txt_searchField;
    
    NSMutableArray *table_data;
    
    SharedManager *singleton;
    IBOutlet UIView *vw_line;
    
    __weak IBOutlet UIButton *btnRetryOption;
    __weak IBOutlet UILabel *lblRetryMsg;
    __weak IBOutlet UIImageView *imageRetryOption;
    __weak IBOutlet UIView *vw_RetryOption;
    UIRefreshControl *refreshController;
    IBOutlet UIImageView *searchIconImage;
    TabBarVC_Flag *tabBar;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintCollView;
@property (weak, nonatomic) IBOutlet UIButton *btnStateSelected;
@property (weak, nonatomic) IBOutlet UIButton *btnShowLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnHelpSupport;

@property(nonatomic, retain) IBOutlet UICollectionView *allSer_collectionView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentServiceType;
@property(copy) void(^didTapLoginView)(UIButton *btnBack);
@property(copy) void(^didTapLoginViewForBanner)(UIButton *btnBack);

@property(copy) void(^didTapHelp)(UIButton *btnBack);

-(void)checkLoginSingupView;
@property (nonatomic,copy) NSString *comingFromNotification;

-(void)changeCollectionHeight:(BOOL)showLogin;
-(IBAction)btn_filterAction:(id)sender;
-(IBAction)btn_noticationAction:(id)sender;
@end
