//
//  FacebookUtility.h
//  SocialAuthentication
//
//  Created by aditi on 06/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.


#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>

@protocol FacebookUtilityDelegate <NSObject>

-(void)facebookLoginSuccessful:(NSMutableDictionary*)dict;
-(void)facebookLoginFailed;

@end

@interface FacebookUtility : NSObject

@property (nonatomic, strong)ACAccountStore *accountStore;
@property (nonatomic, strong)ACAccount *facebookAccount;
@property(nonatomic,assign) id <FacebookUtilityDelegate> delegate;
-(void)doFacebookAuthentication;

@end
