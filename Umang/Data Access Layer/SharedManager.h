//
//  SharedManager.h
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSKeychain.h"
#import <sqlite3.h>
#import "UMSqliteManager.h"
#import "Reachability.h"
#import "ProfileDataBO.h"
#import "UMAPIManager.h"

@interface SharedManager : NSObject
{
    NSMutableArray *fav_Mutable_Arr;
    ApiMode *apiMode;
}

@property(nonatomic,strong) ProfileDataBO *objUserProfile;

@property (retain, nonatomic)  Reachability* reach;

@property (nonatomic, assign) BOOL bannerStatus;
@property (nonatomic, copy) NSString *stateSelected;
@property (nonatomic, copy) NSString *stateCurrent;

@property (nonatomic, copy) NSString *tabSelected;
@property (nonatomic, copy) NSString *languageSelected;
@property (nonatomic, copy) NSString *notificationSelected;
@property (nonatomic, copy) NSString *notiTypeSelected;
@property (nonatomic, copy) NSString *fontSizeSelected;
@property (nonatomic, copy) NSString *isLargeFont;
@property (nonatomic, copy) NSString *isLoginViewShow;


@property(nonatomic,retain)ApiMode *apiMode;
@property (nonatomic, assign) NSInteger tabSelectedIndex;
@property (nonatomic, assign) NSInteger fontSizeSelectedIndex;

@property (nonatomic, assign) BOOL isNotificationEnabled;

@property (nonatomic, copy) NSString *profilestateSelected;
@property (nonatomic, copy) NSString * profileNameSelected;
@property (nonatomic, copy) NSString * profileDOBSelected;


@property (nonatomic, copy) NSString * profileUserAddress;

@property (nonatomic, copy) NSString * profileEmailSelected;
//@property (nonatomic, copy) NSString * profileAlternateMbSelected;



@property (nonatomic, copy) NSString *notiTypeGenderSelected;
@property (nonatomic, retain) NSString *notiTypeCitySelected;
@property (nonatomic, copy) NSString *notiTypDistricteSelected;

@property (nonatomic, retain) NSMutableArray *arrDocumentDownloading;
@property (nonatomic, retain) NSMutableArray *fav_Mutable_Arr;

@property (nonatomic, retain) NSMutableArray *arrNotificationFilterData;

@property (nonatomic, strong) NSMutableArray *arrMainCardData;
@property (nonatomic, strong) NSMutableArray *arrMainCategories;


@property (nonatomic, copy) NSString * search_category;
@property (nonatomic, copy) NSString * search_state;
@property (nonatomic, copy) NSString * search_servicetype;


@property (nonatomic, copy) NSString * SOCIAL_GOOGLE_FROM;
@property (nonatomic, retain) NSMutableArray *arrSocialLinked;
@property (nonatomic, retain) NSMutableArray *arrLocalSocialLinked;

@property (nonatomic,strong) NSMutableArray *questionsArray;
@property (nonatomic,strong) NSString *user_StateId;
@property (nonatomic,strong) NSString *user_EmblemUrl;


// Common

@property(nonatomic,strong) NSString *altermobileNumber;
@property(nonatomic,strong) NSString *mobileNumber;
@property(nonatomic,strong) NSString *imeiNumber;
@property(nonatomic,strong) NSString *deviceID;

@property(nonatomic,strong) NSString *user_mpin;
@property(nonatomic,strong) NSString *user_tkn;
@property(nonatomic,strong) NSString *lastFetchDate;
@property(nonatomic,strong) NSString *lastDirectoryFetchDate;

@property(nonatomic,strong) NSString *user_aadhar_number;


@property (nonatomic,strong) NSString *ndliGlobalCallBack;
@property (nonatomic,strong) NSString *ndliGlobalUserId;


@property(nonatomic,strong) NSString *shared_ntfp;
@property(nonatomic,strong) NSString *shared_ntft;

//----for database -----
@property(nonatomic,strong) NSString*databasePath;

@property(nonatomic,assign) sqlite3 *umangDB;

@property (nonatomic, strong) UMSqliteManager *dbManager;
@property (nonatomic, retain) NSMutableArray *arr_recent_service;
@property (nonatomic, retain) NSMutableArray *arr_recent_Searchservice;
@property (nonatomic, retain) NSMutableArray *arr_trend_Searchservice;


@property (nonatomic, retain) NSMutableDictionary *arr_initResponse;

@property(nonatomic,strong) NSString *devicek_tkn;
@property(nonatomic,strong)NSData* cachedata ;


@property(nonatomic,strong)NSString* user_Qualification ;
@property(nonatomic,strong)NSString* user_Occupation ;
@property(nonatomic,strong)NSString* user_profile_URL ;


@property (nonatomic, retain) NSMutableArray *arr_recent_keySearch;

@property (nonatomic, retain) NSMutableArray *statesList;

@property (nonatomic, retain) NSMutableArray *qualList;
@property (nonatomic, retain) NSMutableArray *occuList;

@property(nonatomic,strong) NSString *shareText;
@property(nonatomic,strong) NSString *phoneSupport;
@property(nonatomic,strong) NSString *emailSupport;


@property(nonatomic,assign) BOOL isArabicSelected;
@property(nonatomic,strong) NSString *temp_reg_mobile;


@property(nonatomic,strong) NSString * imageLocalpath;
//-(NSMutableArray*)prepareStaticDataForNotifications;
@property(nonatomic,strong) NSString *user_id;
@property(nonatomic,strong) NSString *loadServiceStatus;

//---- live chat is not tab-----
@property (nonatomic, retain) NSMutableArray *extraTabArray;
@property(nonatomic,strong) NSString *livChat_isTab;
//@property(nonatomic,strong) NSString *state_isTab;
@property(nonatomic,strong) NSString *chatBadgeCount;
@property(nonatomic,strong) NSMutableArray *AppTabOrders;


@property(nonatomic,strong) NSString *infoTab;

@property (nonatomic, copy) NSString *serviceDirstateSelected;
@property (nonatomic, copy) NSString *serviceDirstateCurrent;
@property (nonatomic,strong) NSString *user_SeriveDirStateId;


@property(nonatomic,strong) NSString *shared_mpinflag;
@property(nonatomic,strong) NSString *shared_mpinmand;


-(NSMutableArray *)createQuestionArray;
-(NSInteger)getSelectedTabIndex;
+ (SharedManager*)sharedSingleton;


-(NSString*) appUniqueID;
+ (NSString *) appVersion;

-(void)setStateForKeepMeLoggedIn;

-(void)setStateId:(NSString*)stateId;
-(NSString*)getStateId;
-(NSString* _Nullable )getKeyWithTag:( NSString* _Nullable )Tag;
-(void)traceEvents:(NSString*_Nullable)category withAction:(NSString*_Nullable)action withLabel:(NSString*_Nullable)label andValue:(int)value;


-(void)ServiceDirSetStateId:(NSString*)stateId;
-(NSString*)getServiceDirStateId;


/*
 SharedManager *singleton = [SharedManager sharedSingleton];
 [singleton traceEvents:@"EVENT NAME" withAction:@"EVENT ACTION" withLabel:@"EVENT LABEL"];
 */
@end
