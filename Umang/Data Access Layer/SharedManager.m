//
//  SharedManager.m
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "SharedManager.h"
#import "HomeCardBO.h"
#import "NotificationItemBO.h"
#import "HomeTabVC.h"
#import "AppConstants.h"
#import "SecKeyWrapper.h"
#import "APIConstant.h"
static SharedManager *sharedMySingleton = nil;

@implementation SharedManager
@synthesize fav_Mutable_Arr;
@synthesize bannerStatus;
@synthesize stateSelected;
@synthesize stateCurrent;
@synthesize tabSelected;
@synthesize languageSelected;
@synthesize notificationSelected;
@synthesize notiTypeSelected;
@synthesize fontSizeSelected;

@synthesize profilestateSelected;
@synthesize  notiTypeGenderSelected;
@synthesize  notiTypeCitySelected;
@synthesize  notiTypDistricteSelected;

@synthesize profileNameSelected;
@synthesize profileDOBSelected;

@synthesize profileEmailSelected;
//@synthesize profileAlternateMbSelected;



@synthesize questionsArray;
//------ Value for registration process
@synthesize mobileNumber;
@synthesize altermobileNumber;

@synthesize user_mpin;
@synthesize user_tkn;
@synthesize lastFetchDate;
@synthesize lastDirectoryFetchDate;

//----Value for database
@synthesize  databasePath;

@synthesize  umangDB;

@synthesize  dbManager;
@synthesize reach;

@synthesize arr_recent_service;
@synthesize arr_recent_Searchservice;
@synthesize arr_trend_Searchservice;

@synthesize loadServiceStatus;

@synthesize search_category;
@synthesize search_state;
@synthesize search_servicetype;
@synthesize arrSocialLinked;
@synthesize arrLocalSocialLinked;
@synthesize SOCIAL_GOOGLE_FROM;
@synthesize arr_initResponse;
@synthesize devicek_tkn;

@synthesize user_aadhar_number;
@synthesize cachedata;

@synthesize user_Qualification,user_Occupation;
@synthesize user_profile_URL;

@synthesize arr_recent_keySearch;
@synthesize profileUserAddress;

@synthesize statesList,qualList,occuList;
@synthesize shareText;

@synthesize  phoneSupport;
@synthesize  emailSupport;
@synthesize temp_reg_mobile;
@synthesize user_id;
@synthesize objUserProfile;
@synthesize imageLocalpath;

@synthesize shared_ntfp,shared_ntft;

@synthesize shared_mpinflag,shared_mpinmand;

@synthesize isNotificationEnabled;
@synthesize user_StateId,apiMode;

@synthesize serviceDirstateSelected,serviceDirstateCurrent,user_SeriveDirStateId;

@synthesize ndliGlobalCallBack,ndliGlobalUserId;

@synthesize AppTabOrders;

@synthesize infoTab;


#pragma mark Singleton Methods

+ (SharedManager*)sharedSingleton
{
    @synchronized(self) {
        if (sharedMySingleton == nil)
        {
            sharedMySingleton = [[super allocWithZone:NULL] init];
            sharedMySingleton.tabSelectedIndex = 0;
            [sharedMySingleton prepareHomeCardData];
            sharedMySingleton.isLargeFont = @"";
            [sharedMySingleton updateDefaultNotificationSettings];
            // Update Langugae Bundle
            NSString *localeLanguage = [[NSUserDefaults standardUserDefaults]  objectForKey:KEY_PREFERED_LOCALE];
            
            if ([localeLanguage isEqualToString:@"ur-IN"]) {
                sharedMySingleton.isArabicSelected = YES;
            }
            else{
                sharedMySingleton.isArabicSelected = NO;
            }
            
        }
        return sharedMySingleton;
    }
}

-(void)setStateForKeepMeLoggedIn{
    NSString *loggedInState = [[NSUserDefaults standardUserDefaults] valueForKey:KEEP_LOGGED_IN_STATE];
    if ([loggedInState isKindOfClass:[NSString class]]) {
        if ([loggedInState isEqualToString:KEEP_LOGGED_IN_TRUE]) {
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:kKeepMeLoggedIn];
        }
        else if ([loggedInState isEqualToString:KEEP_LOGGED_IN_FALSE]) {
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:kKeepMeLoggedIn];
        }
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:kKeepMeLoggedIn];
    }
}

-(NSMutableArray *)createQuestionArray
{
    [self.questionsArray removeAllObjects];
    
    
    NSArray *myArray = @[NSLocalizedString(@"question1", nil), NSLocalizedString(@"question2", nil), NSLocalizedString(@"question3", nil), NSLocalizedString(@"question4", nil), NSLocalizedString(@"question5", nil)];

    for (NSInteger i = 0; i < 5; i++)
    {
        NSDictionary *questionsDict = @{@"question":[NSString stringWithFormat:@"%@",[myArray objectAtIndex:i]] , @"questionId":[NSString stringWithFormat:@"112%ld",i+2]};
        
        [self.questionsArray addObject:questionsDict];
    }
    
    return self.questionsArray;
}



-(void)updateDefaultNotificationSettings{
    NSString *notifyStatus = [[NSUserDefaults standardUserDefaults] objectForKey:@"SELECTED_NOTIFICATION_STATUS"];
    
    if (notifyStatus) {
        sharedMySingleton.notificationSelected=notifyStatus;
    }
    else{
        sharedMySingleton.notificationSelected=NSLocalizedString(@"disabled", nil);
    }
}



+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedSingleton];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}



-(NSInteger)getSelectedTabIndex{
    
    SharedManager * singleton = [SharedManager sharedSingleton];
    singleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
    //sharedMySingleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
    if(singleton.tabSelectedIndex == 0)
    {
        
        singleton.tabSelectedIndex = 0 ;
    }
    
    [singleton setStateForKeepMeLoggedIn];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    if ([singleton.user_tkn length]<=0)
    {
        singleton.user_id=[defaults decryptedValueForKey:@"USER_ID"];
        if (singleton.user_id == nil || singleton.user_id.length == 0)
        {
            singleton.user_id = @"";
        }
        
        
        singleton.user_tkn=[defaults decryptedValueForKey:@"TOKEN_KEY"];
        if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
        {
            singleton.user_tkn = @"";
        }
        singleton.mobileNumber=[defaults decryptedValueForKey:@"MOBILE_KEY"];
        
        if (singleton.mobileNumber == nil || singleton.mobileNumber.length == 0)
        {
            singleton.mobileNumber = @"";
        }
        
        singleton.profileNameSelected=[defaults decryptedValueForKey:@"NAME_KEY"];
        if (singleton.profileNameSelected == nil || singleton.profileNameSelected.length == 0)
        {
            singleton.profileNameSelected = @"";
        }
        
        singleton.notiTypeGenderSelected=[defaults decryptedValueForKey:@"GENDER_KEY"];
        
        if (singleton.notiTypeGenderSelected == nil || singleton.notiTypeGenderSelected.length == 0)
        {
            singleton.notiTypeGenderSelected = @"";
        }
        
        singleton.profilestateSelected=[defaults decryptedValueForKey:@"STATE_KEY"];
        if (singleton.profilestateSelected == nil || singleton.profilestateSelected.length == 0)
        {
            singleton.profilestateSelected = @"";
        }
        
        singleton.notiTypDistricteSelected=[defaults decryptedValueForKey:@"DISTRICT_KEY"];
        
        if (singleton.notiTypDistricteSelected == nil || singleton.notiTypDistricteSelected.length == 0)
        {
            singleton.notiTypDistricteSelected = @"";
        }
        
        singleton.profileDOBSelected=[defaults decryptedValueForKey:@"DOB_KEY"];
        
        if (singleton.profileDOBSelected == nil || singleton.profileDOBSelected.length == 0)
        {
            singleton.profileDOBSelected = @"";
        }
        
        
        singleton.objUserProfile.objAadhar.aadhar_number=[defaults decryptedValueForKey:@"ADHAR_ADHARNUM_KEY"];
        
        if (singleton.objUserProfile.objAadhar.aadhar_number == nil || singleton.objUserProfile.objAadhar.aadhar_number.length == 0)
        {
            singleton.objUserProfile.objAadhar.aadhar_number = @"";
        }
        
        
        singleton.objUserProfile.objAadhar.aadhar_image_url=[defaults decryptedValueForKey:@"ADHAR_IMAGE_URL_KEY"];
        if (singleton.objUserProfile.objAadhar.aadhar_image_url == nil || singleton.objUserProfile.objAadhar.aadhar_image_url.length == 0)
        {
            singleton.objUserProfile.objAadhar.aadhar_image_url= @"";
        }
        
        singleton.objUserProfile.objAadhar.pincode=[defaults decryptedValueForKey:@"ADHAR_PINCODE_KEY"];
        
        if (singleton.objUserProfile.objAadhar.pincode == nil || singleton.objUserProfile.objAadhar.pincode.length == 0)
        {
            singleton.objUserProfile.objAadhar.pincode= @"";
        }
        
        
        singleton.objUserProfile.objAadhar.dob=[defaults decryptedValueForKey:@"ADHAR_DOB_KEY"];
        
        if ( singleton.objUserProfile.objAadhar.dob == nil ||  singleton.objUserProfile.objAadhar.dob.length == 0)
        {
             singleton.objUserProfile.objAadhar.dob= @"";
        }
        
        
        
        singleton.objUserProfile.objAadhar.email=[defaults decryptedValueForKey:@"ADHAR_EMAIL_KEY"];
        
        if (singleton.objUserProfile.objAadhar.email == nil ||  singleton.objUserProfile.objAadhar.email.length == 0)
        {
            singleton.objUserProfile.objAadhar.email= @"";
        }
        
        
        
        singleton.objUserProfile.objAadhar.gender=[defaults decryptedValueForKey:@"ADHAR_GENDER_KEY"];
        
        if (singleton.objUserProfile.objAadhar.gender == nil ||  singleton.objUserProfile.objAadhar.gender.length == 0)
        {
           singleton.objUserProfile.objAadhar.gender= @"";
        }
        
        
        singleton.objUserProfile.objAadhar.mobile_number=[defaults decryptedValueForKey:@"ADHAR_MOB_KEY"];
        
        if (singleton.objUserProfile.objAadhar.mobile_number == nil ||  singleton.objUserProfile.objAadhar.mobile_number.length == 0)
        {
           singleton.objUserProfile.objAadhar.mobile_number= @"";
        }
        
        
        singleton.objUserProfile.objAadhar.name=[defaults decryptedValueForKey:@"ADHAR_NAME_KEY"];
        
        if (singleton.objUserProfile.objAadhar.name == nil ||  singleton.objUserProfile.objAadhar.name.length == 0)
        {
            singleton.objUserProfile.objAadhar.name= @"";
        }
        
        
        singleton.objUserProfile.objAadhar.district=[defaults decryptedValueForKey:@"ADHAR_DISTRICT_KEY"];
        if (singleton.objUserProfile.objAadhar.district == nil ||  singleton.objUserProfile.objAadhar.district.length == 0)
        {
            singleton.objUserProfile.objAadhar.district= @"";
        }
        singleton.objUserProfile.objAadhar.father_name=[defaults decryptedValueForKey:@"ADHAR_FATHERNAME_KEY"];
        
        if (singleton.objUserProfile.objAadhar.father_name == nil ||  singleton.objUserProfile.objAadhar.father_name.length == 0)
        {
            singleton.objUserProfile.objAadhar.father_name= @"";
        }
        
        singleton.objUserProfile.objAadhar.state=[defaults decryptedValueForKey:@"ADHAR_STATE_KEY"];
        if (singleton.objUserProfile.objAadhar.state == nil ||  singleton.objUserProfile.objAadhar.state.length == 0)
        {
            singleton.objUserProfile.objAadhar.state= @"";
        }

        
        singleton.objUserProfile.objAadhar.street=[defaults decryptedValueForKey:@"ADHAR_STREET_KEY"];
        if (singleton.objUserProfile.objAadhar.street == nil ||  singleton.objUserProfile.objAadhar.street.length == 0)
        {
            singleton.objUserProfile.objAadhar.street= @"";
        }
        
        singleton.objUserProfile.objAadhar.district=[defaults decryptedValueForKey:@"ADHAR_SUBDISTRICT_KEY"];
        if (singleton.objUserProfile.objAadhar.district == nil ||  singleton.objUserProfile.objAadhar.district.length == 0)
        {
            singleton.objUserProfile.objAadhar.district= @"";
        }
        
        singleton.objUserProfile.objAadhar.vtc=[defaults decryptedValueForKey:@"ADHAR_VTC_KEY"];
        
        if (singleton.objUserProfile.objAadhar.vtc == nil ||  singleton.objUserProfile.objAadhar.vtc.length == 0)
        {
            singleton.objUserProfile.objAadhar.vtc= @"";
        }
        
        singleton.objUserProfile.objAadhar.vtc_code=[defaults decryptedValueForKey:@"ADHAR_VTCCODE_KEY"];
        
        if (singleton.objUserProfile.objAadhar.vtc_code == nil ||  singleton.objUserProfile.objAadhar.vtc_code.length == 0)
        {
            singleton.objUserProfile.objAadhar.vtc_code= @"";
        }
        
    }
    //========================================
    //========================================
    else
    {
        
        NSString *aadhar_number=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.aadhar_number];
        
        if([aadhar_number length]==0 || aadhar_number ==nil|| [aadhar_number isEqualToString:@"(null)"])
        {
            aadhar_number=@"";
        }
        
        
        NSString *aadhar_image_url=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.aadhar_image_url];
        
        if([aadhar_image_url length]==0|| aadhar_image_url ==nil|| [aadhar_image_url isEqualToString:@"(null)"])
        {
            aadhar_image_url=@"";
        }
        
        NSString *pincode=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.pincode];
        
        if([pincode length]==0 || pincode ==nil|| [pincode isEqualToString:@"(null)"])
        {
            pincode=@"";
        }
        
        
        NSString *dob=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.dob];
        
        if([dob length]==0 || dob ==nil|| [dob isEqualToString:@"(null)"])
        {
            dob=@"";
        }
        
        
        
        NSString *email=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.email];
        if([email length]==0 || email ==nil|| [email isEqualToString:@"(null)"])
        {
            email=@"";
        }
        
        
        NSString *gender=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.gender];
        
        if([gender length]==0 || gender ==nil|| [gender isEqualToString:@"(null)"])
        {
            gender=@"";
        }
        
        NSString *mobile_number=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.mobile_number];
        
        if([mobile_number length]==0 || mobile_number ==nil|| [mobile_number isEqualToString:@"(null)"])
        {
            mobile_number=@"";
        }
        
        
        NSString *name=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.name];
        
        if([name length]==0 || name ==nil|| [name isEqualToString:@"(null)"])
        {
            name=@"";
        }
        
        
        NSString *district=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.district];
        
        if([district length]==0 || district ==nil|| [district isEqualToString:@"(null)"])
        {
            district=@"";
        }
        
        
        
        NSString *father_name=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.father_name];
        
        if([father_name length]==0 || father_name ==nil|| [father_name isEqualToString:@"(null)"])
        {
            father_name=@"";
        }
        
        
        
        
        NSString *state=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.state];
        
        if([state length]==0 || state ==nil|| [state isEqualToString:@"(null)"])
        {
            state=@"";
        }
        
        NSString *street=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.street];
        
        if([street length]==0 || street ==nil|| [street isEqualToString:@"(null)"])
        {
            street=@"";
        }
        
        
        NSString *subdistrict=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.subdistrict];
        
        
        if([subdistrict length]==0 || subdistrict ==nil|| [subdistrict isEqualToString:@"(null)"])
        {
            subdistrict=@"";
        }
        
        
        
        NSString *vtc=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.vtc];
        
        if([vtc length]==0 || vtc ==nil|| [vtc isEqualToString:@"(null)"])
        {
            vtc=@"";
        }
        
        
        
        NSString *vtc_code=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.vtc_code];
        
        if([vtc_code length]==0 || vtc_code ==nil|| [vtc_code isEqualToString:@"(null)"])
        {
            vtc_code=@"";
        }
        
        NSString *aadhaar_address=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objAadhar.address];
        
        if([aadhaar_address length]==0 || aadhaar_address ==nil|| [aadhaar_address isEqualToString:@"(null)"])
        {
            aadhaar_address=@"";
        }
        
        
        
        NSString *notiTypeCitySelected=[NSString stringWithFormat:@"%@",singleton.notiTypeCitySelected];
        
        if([notiTypeCitySelected length]==0 || notiTypeCitySelected ==nil|| [notiTypeCitySelected isEqualToString:@"(null)"])
        {
            notiTypeCitySelected=@"";
        }
        
        
        NSString *altermobileNumber=[NSString stringWithFormat:@"%@",singleton.altermobileNumber];
        if([altermobileNumber length]==0 || altermobileNumber ==nil|| [altermobileNumber isEqualToString:@"(null)"])
        {
            altermobileNumber=@"";
        }
        
        
        NSString *user_Qualification=[NSString stringWithFormat:@"%@",singleton.user_Qualification];
        
        if([user_Qualification length]==0 || user_Qualification ==nil|| [user_Qualification isEqualToString:@"(null)"])
        {
            user_Qualification=@"";
        }
        
        
        NSString *user_Occupation=[NSString stringWithFormat:@"%@",singleton.user_Occupation];
        if([user_Occupation length]==0 || user_Occupation ==nil|| [user_Occupation isEqualToString:@"(null)"])
        {
            user_Occupation=@"";
        }
        
        NSString *profileEmailSelected=[NSString stringWithFormat:@"%@",singleton.profileEmailSelected];
        
        if([profileEmailSelected length]==0 || profileEmailSelected ==nil|| [profileEmailSelected isEqualToString:@"(null)"])
        {
            profileEmailSelected=@"";
        }
        
        NSString *user_profile_URL=[NSString stringWithFormat:@"%@",singleton.user_profile_URL];
        
        if([user_profile_URL length]==0 || user_profile_URL ==nil|| [user_profile_URL isEqualToString:@"(null)"])
        {
            user_profile_URL=@"";
        }
        NSString *user_tkn=[NSString stringWithFormat:@"%@",singleton.user_tkn];
        if([user_tkn length]==0 || user_tkn ==nil|| [user_tkn isEqualToString:@"(null)"])
        {
            user_tkn=@"";
        }
        NSString *mobileNumber=[NSString stringWithFormat:@"%@",singleton.mobileNumber];
        
        if([mobileNumber length]==0 || mobileNumber ==nil|| [mobileNumber isEqualToString:@"(null)"])
        {
            mobileNumber=@"";
        }
        
        NSString *profileNameSelected=[NSString stringWithFormat:@"%@",singleton.profileNameSelected];
        
        if([profileNameSelected length]==0 || profileNameSelected ==nil|| [profileNameSelected isEqualToString:@"(null)"])
        {
            profileNameSelected=@"";
        }
        
        
        NSString *notiTypeGenderSelected=[NSString stringWithFormat:@"%@",singleton.notiTypeGenderSelected];
        if([notiTypeGenderSelected length]==0 || notiTypeGenderSelected ==nil|| [notiTypeGenderSelected isEqualToString:@"(null)"])
        {
            notiTypeGenderSelected=@"";
        }
        
        
        
        NSString *profilestateSelected=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
        
        if([profilestateSelected length]==0 || profilestateSelected ==nil|| [profilestateSelected isEqualToString:@"(null)"])
        {
            profilestateSelected=@"";
        }
        
        
        
        NSString *notiTypDistricteSelected=[NSString stringWithFormat:@"%@",singleton.notiTypDistricteSelected];
        
        if([notiTypDistricteSelected length]==0 || notiTypDistricteSelected ==nil|| [notiTypDistricteSelected isEqualToString:@"(null)"])
        {
            notiTypDistricteSelected=@"";
        }
        
        
        
        NSString *profileDOBSelected=[NSString stringWithFormat:@"%@",singleton.profileDOBSelected];
        
        if([profileDOBSelected length]==0 || profileDOBSelected ==nil|| [profileDOBSelected isEqualToString:@"(null)"])
        {
            profileDOBSelected=@"";
        }
        
        
        
        NSString *user_id=[NSString stringWithFormat:@"%@",singleton.user_id];
        
        [defaults setAESKey:@"UMANGIOSAPP"];
        
        if ([user_id length]>0) {
            // [defaults setObject:user_id forKey:@"USER_ID"];
            [defaults encryptValue:user_id withKey:@"USER_ID"];
            
        }
        
        
        
        [defaults encryptValue:aadhar_number withKey:@"ADHAR_ADHARNUM_KEY"];
        [defaults encryptValue:aadhar_image_url withKey:@"ADHAR_IMAGE_URL_KEY"];
        [defaults encryptValue:pincode withKey:@"ADHAR_PINCODE_KEY"];
        [defaults encryptValue:dob withKey:@"ADHAR_DOB_KEY"];
        [defaults encryptValue:email withKey:@"ADHAR_EMAIL_KEY"];
        [defaults encryptValue:gender withKey:@"ADHAR_GENDER_KEY"];
        [defaults encryptValue:mobile_number withKey:@"ADHAR_MOB_KEY"];
        [defaults encryptValue:name withKey:@"ADHAR_NAME_KEY"];
        [defaults encryptValue:district withKey:@"ADHAR_DISTRICT_KEY"];
        [defaults encryptValue:father_name withKey:@"ADHAR_FATHERNAME_KEY"];
        [defaults encryptValue:state withKey:@"ADHAR_STATE_KEY"];
        [defaults encryptValue:street withKey:@"ADHAR_STREET_KEY"];
        [defaults encryptValue:subdistrict withKey:@"ADHAR_SUBDISTRICT_KEY"];
        [defaults encryptValue:vtc withKey:@"ADHAR_VTC_KEY"];
        [defaults encryptValue:vtc_code withKey:@"ADHAR_VTCCODE_KEY"];
        [defaults encryptValue:aadhaar_address withKey:@"ADHAR_ADDRESS_KEY"];
        
        
        ///---------- Mobile Credential----------------------------------
        [defaults encryptValue:notiTypeCitySelected withKey:@"CITY_KEY"];
        [defaults encryptValue:altermobileNumber withKey:@"ALTERMB_KEY"];
        [defaults encryptValue:user_Qualification withKey:@"QUALI_KEY"];
        [defaults encryptValue:user_Occupation withKey:@"OCCUP_KEY"];
        [defaults encryptValue:profileEmailSelected withKey:@"EMAIL_KEY"];
        [defaults encryptValue:user_profile_URL withKey:@"URLPROFILE_KEY"];
        [defaults encryptValue:user_tkn withKey:@"TOKEN_KEY"];
        [defaults encryptValue:mobileNumber withKey:@"MOBILE_KEY"];
        [defaults encryptValue:profileNameSelected withKey:@"NAME_KEY"];
        [defaults encryptValue:notiTypeGenderSelected withKey:@"GENDER_KEY"];
        [defaults encryptValue:profilestateSelected withKey:@"STATE_KEY"];
        [defaults encryptValue:notiTypDistricteSelected withKey:@"DISTRICT_KEY"];
        [defaults encryptValue:profileDOBSelected withKey:@"DOB_KEY"];
        
        
        
        [defaults synchronize];
        //------------------------- Encrypt Value------------------------
        
    }
    singleton.user_profile_URL=[defaults decryptedValueForKey:@"USER_PIC"];
    NSLog(@"user_profile_URL=%@",singleton.user_profile_URL);
    
    
    [defaults synchronize];
    
    
    //========================================
    //========================================
    NSLog(@"singleton.tabSelectedIndex=%ld",(long)singleton.tabSelectedIndex);
    return singleton.tabSelectedIndex;
    
    
    
}


-(void)prepareHomeCardData{
    
    
    self.arrMainCategories = [[NSMutableArray alloc] initWithObjects:@{ @"category": @"banner",@"category_icon": @"none"},@{ @"category": @"Recent",@"category_icon": @"icon_recent"},@{ @"category": @"Trending",@"category_icon": @"icon_trending"},@{ @"category": @"Top Rated",@"category_icon": @"top_rated"},@{ @"category": @"New & Updated",@"category_icon": @"icon_newupdated"},@{ @"category": @"Suggested",@"category_icon": @"suggested.png"}, nil];
    
    NSMutableArray *arrMainData = [NSMutableArray new];
    
    // Add Recent Category Data
    
    HomeCardBO *objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Recent", @"");
    objCard.categoryIcon = @"icon_recent";
    objCard.cardTitle = NSLocalizedString(@"Scholarship", @"");
    objCard.cardImage = @"passport";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Recent", @"");
    objCard.categoryIcon = @"icon_recent";
    objCard.cardTitle = @"EPFO";
    objCard.cardImage = @"epfo";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Recent", @"");
    objCard.categoryIcon = @"icon_recent";
    objCard.cardTitle = @"Passport";
    objCard.cardImage = @"passport";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Recent", @"");
    objCard.categoryIcon = @"icon_recent";
    objCard.cardTitle = @"PMAY";
    objCard.cardImage = @"pmay";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    // Add Trending Category Data
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Trending", @"");
    objCard.categoryIcon = @"icon_trending";
    objCard.cardTitle = @"eDisha";
    objCard.cardImage = @"edisha";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Trending", @"");
    objCard.categoryIcon = @"icon_trending";
    objCard.cardTitle = @"Sarthi";
    objCard.cardImage = @"sarthi";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Trending", @"");
    objCard.categoryIcon = @"icon_trending";
    objCard.cardTitle = @"Apple Sarkar";
    objCard.cardImage = @"aaple_sarkar";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName =  NSLocalizedString(@"Trending", @"");
    objCard.categoryIcon = @"icon_trending";
    objCard.cardTitle = @"Income Tax";
    objCard.cardImage = @"icon_income_tax";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    // Add Top Rated Category Data
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName =  NSLocalizedString(@"Top_Rated", @"");
    objCard.categoryIcon = @"top_rated.png";
    objCard.cardTitle = @"Postal Info";
    objCard.cardImage = @"postal_info";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Top_Rated", @"");
    objCard.categoryIcon = @"top_rated.png";
    objCard.cardTitle = @"ePost Office";
    objCard.cardImage = @"epost_office";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName =NSLocalizedString(@"Top_Rated", @"");
    objCard.categoryIcon = @"top_rated.png";
    objCard.cardTitle = @"Postal life Insurance";
    objCard.cardImage = @"postal_life_insurance";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Top_Rated", @"");
    objCard.categoryIcon = @"top_rated.png";
    objCard.cardTitle = @"NIC ORS";
    objCard.cardImage = @"nic_ors";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    
    
    // Add New & Updated Category Data
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"New_amp_Updated", @"");
    objCard.categoryIcon = @"icon_newupdated";
    objCard.cardTitle = @"SSC";
    objCard.cardImage = @"ssc";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"New_amp_Updated", @"");
    objCard.categoryIcon = @"icon_newupdated";
    objCard.cardTitle = @"PM National Fund";
    objCard.cardImage = @"pm_national_relief_fund";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"New_amp_Updated", @"");
    objCard.categoryIcon = @"icon_newupdated";
    objCard.cardTitle = @"Election Commission";
    objCard.cardImage = @"election_comission";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"New_amp_Updated", @"");
    objCard.categoryIcon = @"icon_newupdated";
    objCard.cardTitle = @"NPS";
    objCard.cardImage = @"nps";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    
    // Add Suggested Category Data
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Suggested", @"");
    objCard.categoryIcon = @"suggested.png";
    objCard.cardTitle = @"NREGA";
    objCard.cardImage = @"nrega";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Suggested", @"");
    objCard.categoryIcon = @"suggested.png";
    objCard.cardTitle = @"MSME";
    objCard.cardImage = @"msme";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Suggested", @"");
    objCard.categoryIcon = @"suggested.png";
    objCard.cardTitle = @"CPGRAMS";
    objCard.cardImage = @"cpgrams";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    
    objCard = [[HomeCardBO alloc] init];
    objCard.categoryName = NSLocalizedString(@"Suggested", @"");
    objCard.categoryIcon = @"suggested.png";
    objCard.cardTitle = @"Vahan";
    objCard.cardImage = @"vahan";
    objCard.cardDesc = @"Lorem ipsum is simply dummy text of the printing and typesetting...";
    [arrMainData addObject:objCard];
    objCard = nil;
    
    self.arrMainCardData = arrMainData;
}




- (id)init
{
    if (self = [super init]) {
        fav_Mutable_Arr = [[NSMutableArray alloc] init];
        AppTabOrders = [[NSMutableArray alloc] init];
        arr_recent_service= [[NSMutableArray alloc] init];
        arr_recent_Searchservice= [[NSMutableArray alloc] init];
        arr_trend_Searchservice= [[NSMutableArray alloc] init];
        
        arrSocialLinked= [[NSMutableArray alloc] init];
        arrLocalSocialLinked= [[NSMutableArray alloc] init];
        
        arr_recent_keySearch= [[NSMutableArray alloc] init];
        statesList=[[NSMutableArray alloc]init];
        qualList=[[NSMutableArray alloc]init];
        occuList=[[NSMutableArray alloc]init];
        
        
        arr_initResponse=[[NSMutableDictionary alloc] init]; //array used to init response
        mobileNumber=@"";
        user_mpin=@"";
        user_tkn=@"";
        lastFetchDate=@"";
        lastDirectoryFetchDate = @"";
        altermobileNumber=@"";
        
        bannerStatus = YES;
        stateSelected = NSLocalizedString(@"all", nil);
        tabSelected =  NSLocalizedString(@"home_small", nil);
        languageSelected = NSLocalizedString(@"english", nil);
        notificationSelected =  NSLocalizedString(@"enabled", nil);
        notiTypeSelected = NSLocalizedString(@"all", nil);
        fontSizeSelected= NSLocalizedString(@"normal", nil);
        
        infoTab =@"";
        loadServiceStatus=@"FALSE";
        
        shared_ntft=@"1";
        shared_ntfp=@"1";
        
        // USED BY DEFAULT FALSE VALUE FOR BOTH
       /* shared_mpinflag=@"TRUE"; // NOT SHOW
        shared_mpinmand=@"FALSE"; // SHOW
        [[NSUserDefaults standardUserDefaults] setValue:shared_mpinflag forKey:@"mpinflag"];
        [[NSUserDefaults standardUserDefaults] setValue:shared_mpinmand forKey:@"mpinmand"];
        [[NSUserDefaults standardUserDefaults] synchronize];
       */
        // USED BY DEFAULT FALSE VALUE FOR BOTH

        SOCIAL_GOOGLE_FROM=@"";
        
        notiTypeGenderSelected = @"";
        notiTypeCitySelected =@"";
        notiTypDistricteSelected = @"";
        
        profilestateSelected = @"";
        profileNameSelected = @"";
        profileDOBSelected = @"";
        
        profileEmailSelected= @"";
        search_category= NSLocalizedString(@"all", nil);
        search_state= @"";
        search_servicetype= NSLocalizedString(@"all", nil);
        devicek_tkn=@"";
        user_Qualification=@"";
        user_Occupation=@"";
        
        user_profile_URL=@"";
        profileUserAddress=@"";
        shareText=@"";
        
        phoneSupport=@"";
        emailSupport=@"";
        user_id=@"";
        temp_reg_mobile=@"";
        cachedata = [[NSData alloc] init];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        imageLocalpath = [documentsDirectory stringByAppendingPathComponent:
                          @"user_image.png" ];
        
        apiMode = [[ApiMode alloc] initCustomize];
        
        
    }
    
    return self;
    
}



+ (NSString *) appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

-(NSString *)appUniqueID {
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    NSString *strApplicationUUID = [SSKeychain passwordForService:appName account:KEYCHAIN_ACCOUNT_KEY];
    if (strApplicationUUID == nil) {
        strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SSKeychain setPassword:strApplicationUUID forService:appName account:KEYCHAIN_ACCOUNT_KEY];
    }
    
    
    strApplicationUUID = [strApplicationUUID stringByReplacingOccurrencesOfString:@"-" withString:@""];
    strApplicationUUID=[strApplicationUUID lowercaseString];
    
    return strApplicationUUID;
}


- (NSString *)stringUniqueID {
    
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    //  return (__bridge NSString *)string ;
    
    //---fix lint---
    NSString *str = (__bridge NSString *)string;
    CFRelease(string);
    return str;
    
    
    
}




-(void)ServiceDirSetStateId:(NSString*)stateId {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:stateId forKey:SERVICEDIRECTORYSTATE];
    [defaults synchronize];
}
-(NSString*)getServiceDirStateId {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strState = [defaults valueForKey:SERVICEDIRECTORYSTATE];
    return strState;
}






-(void)setStateId:(NSString*)stateId {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:stateId forKey:State_Selected];
    [defaults synchronize];
}
-(NSString*)getStateId {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strState = [defaults valueForKey:State_Selected];
    return strState;
}
-(NSString* _Nullable )getKeyWithTag:( NSString* )Tag {
    SecKeyWrapper *key = [SecKeyWrapper sharedWrapper];
    NSData *dataSalt = [key getKeyDataFromKeyChainWith:Tag];
    if (dataSalt == nil) {
        return nil;
    }
    return [NSString stringWithUTF8String:[dataSalt bytes]];
}


-(void)traceEvents:(NSString*)category withAction:(NSString*)action withLabel:(NSString*)label andValue:(int )value
{
    NSLog(@"value of Category Name =%@ , Action Name=%@ ,Label Name=%@ and Value= %d",category,action,label,value);

    
    
    __block NSString *category_name=category;
    __block NSString *action_name=action;
    __block NSString *label_name=label;
    
    
    //@([value intValue]);
    __block NSNumber *value_no=[NSNumber numberWithInt:value];

    //add background thread so main thread not effected
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if (category_name.length==0)
        {
            category_name=@"Default";
        }
        if (action_name.length==0)
        {
            action_name=@"Default";
            
        }
        if (label_name.length==0)
        {
            label_name=@"Default";
        }
        
        // Optional: automatically send uncaught exceptions to Google Analytics.
        [GAI sharedInstance].trackUncaughtExceptions = YES;
        // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
        [GAI sharedInstance].dispatchInterval = 20;
        // Optional: set debug to YES for extra debugging information.
        //  [GAI sharedInstance].debug = YES;
        // Create tracker instance.
        id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-87973874-2"];
        tracker.allowIDFACollection = YES;
        
        NSLog(@"value of Category Name =%@ , Action Name=%@ ,Label Name=%@ and Value= %@",category_name,action_name,label_name,value_no);
        
        NSDictionary *Dict = [[GAIDictionaryBuilder createEventWithCategory:category_name action:action_name label:label_name value:value_no] build];
        [tracker send:Dict];
        
       
    });
    
}



@end
/*
 #import "Singleton.h"
 
 Singleton *singleton = [Singleton sharedSingleton];
 singleton.firstMutableArray = ...
 singleton.secondMutableArray = ...*/
