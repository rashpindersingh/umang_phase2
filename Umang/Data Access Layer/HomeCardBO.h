//
//  HomeCardBO.h
//  Umang
//
//  Created by Administrator on 10/8/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeCardBO : NSObject

@property(nonatomic,strong) NSString *categoryName;
@property(nonatomic,strong) NSString *categoryIcon;

@property(nonatomic,strong) NSString *cardTitle;
@property(nonatomic,strong) NSString *cardImage;
@property(nonatomic,strong) NSString *cardDesc;
@property(nonatomic,assign) BOOL isFavorite;


@end
