//
//  EditProfileCell.h
//  Umang
//
//  Created by deepak singh rawat on 18/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileCell : UITableViewCell
@property(weak,nonatomic)IBOutlet UITextField *txt_value;
@end
