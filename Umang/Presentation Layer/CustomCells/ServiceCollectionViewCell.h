//
//  ServiceCollectionViewCell.h
//  Home
//
//  Created by deepak singh rawat on 26/09/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "MyButton.h"

@interface ServiceCollectionViewCell : UICollectionViewCell
@property (weak) IBOutlet UIImageView *serviceImage;
@property (weak) IBOutlet UILabel *serviceTitle;
//@property (weak) IBOutlet UIButton *serviceFav;
//@property (weak) IBOutlet UIButton *serviceInfo;
@property (weak) IBOutlet MyButton *serviceInfo;
@property(weak)IBOutlet UIView *vw_contain;

@property (weak, nonatomic) IBOutlet MyFavButton *serviceFav;

@property (nonatomic, strong)IBOutlet HCSStarRatingView *starRatingView;

@end
