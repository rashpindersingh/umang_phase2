//
//  FeedbackTextCell.h
//  Umang
//
//  Created by admin on 29/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackTextCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *txtViewFeedback;

@end
