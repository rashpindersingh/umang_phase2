//
//  NotificationViewCell.h
//  Umang
//
//  Created by admin on 9/26/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationItemBO.h"
#import "MyButton.h"

@protocol NSwipeTableViewCellDelegate <NSObject>

-(void)deleteNotificationItemFromTableRow:(NotificationItemBO*)object;
-(void)pannedCell:(id)cell;

@end

@interface NotificationViewCell : UITableViewCell

{
    CGPoint originalCenter;
    BOOL deleteOnDragRelease;
}

@property(nonatomic,weak) id <NSwipeTableViewCellDelegate> delegate;

@property(nonatomic,strong) UIPanGestureRecognizer *panGesture;

@property(nonatomic,strong) NotificationItemBO *objCellItem;

@property (weak, nonatomic) IBOutlet UIView *vwContentBG;

@property (weak, nonatomic) IBOutlet UIView *vwDeleteBG;

@property (weak, nonatomic) IBOutlet UIButton *btnClearCell;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;


@property (weak, nonatomic) IBOutlet MyFavButton *btn_fav;

@property (weak, nonatomic) IBOutlet UIImageView *imgVwType;

@property (weak, nonatomic) IBOutlet UILabel *lblNotificationHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblNotificationDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UIImageView *imgCategory;

-(void)resetFramesForPannedArea;

@end
