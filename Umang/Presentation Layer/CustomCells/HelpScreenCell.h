//
//  HelpScreenCell.h
//  Umang
//
//  Created by admin on 01/03/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpScreenCell : UITableViewCell



@property(nonatomic,retain)IBOutlet UIImageView *imgService;
@property(nonatomic,retain)IBOutlet UILabel *lblServiceTitle;
@property(nonatomic,retain)IBOutlet UILabel *lblServiceDescription;
@property (weak, nonatomic) IBOutlet UISwitch *btnSwitch;


@end
