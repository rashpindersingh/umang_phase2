//
//  HeaderCollectionReusableView.h
//  Umang
//
//  Created by admin on 11/10/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderCollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentService;
@property (strong, nonatomic) IBOutlet UIButton *btnStateSelected;

@end
@interface HeaderNearMeCollection : UICollectionReusableView


@end
