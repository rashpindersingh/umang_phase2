//
//  AadharCardViewCon.m
//  Umang
//
//  Created by admin on 08/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.




#import "AadharCardViewCon.h"
#import "AadharDataBO.h"
#import "AadharDetailInfoCell.h"
#import "AadharDetailProfileCell.h"



@interface AadharCardViewCon ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrAadharData;
    AadharDetailProfileCell  *headerProfileCell;
}


@end

@implementation AadharCardViewCon

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    headerProfileCell.isAadhardetailWithoutPic = NO;
//    headerProfileCell.isNotLinkedAadharCon = NO;
    
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    arrAadharData = [[NSMutableArray alloc]init];
    [self prepareTempDataForAadharCard];
    
    tbl_aadhaarDetail.dataSource = self;
    tbl_aadhaarDetail.delegate = self;
    
    
    tbl_aadhaarDetail.tableHeaderView = [self designAadharProfileView];
    
    
  
    
}



-(AadharDetailProfileCell*)designAadharProfileView

{
    if (headerProfileCell == nil)
    
    {
        headerProfileCell = [[[NSBundle mainBundle] loadNibNamed:@"AadharDetailProfileCell" owner:self options:nil] objectAtIndex:0];
        [headerProfileCell bindDataForHeaderView];
        
        
    }
    
    
    return headerProfileCell;
}




-(void)prepareTempDataForAadharCard
{
    
    AadharDataBO *objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Name";
    objAadharData.titleDescription = @"Parampreet Dhatt";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Date of Birth";
    objAadharData.titleDescription = @"28/12/2000";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Gender";
    objAadharData.titleDescription = @"Male";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Father's Name";
    objAadharData.titleDescription = @"Mohan Singh";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Address";
    objAadharData.titleDescription = @"1234, sector 68";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"City";
    objAadharData.titleDescription = @"Mohali";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"District";
    objAadharData.titleDescription = @"Mohali";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
     objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"State";
    objAadharData.titleDescription = @"Punjab";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Pincode";
    objAadharData.titleDescription = @"301001";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;

    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.001;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
        return 44.0;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return arrAadharData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
        
        static NSString *CellIdentifierNew = @"AadharDetailInfoCell";
        AadharDetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierNew];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        
      AadharDataBO  *objAadhar = arrAadharData [indexPath.row];
        cell.lblTitle.text = objAadhar.title;
        cell.lblTitleValue.text = objAadhar.titleDescription;
        
    
    
        
        
        return cell;
        
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
