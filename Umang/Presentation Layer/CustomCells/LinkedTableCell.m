//
//  LinkedTableCell.m
//  Umang
//
//  Created by admin on 04/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "LinkedTableCell.h"

@implementation LinkedTableCell

@synthesize img_socialprofile;
@synthesize lbl_titleName;
@synthesize lbl_linkedSite;
@synthesize btn_link;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
