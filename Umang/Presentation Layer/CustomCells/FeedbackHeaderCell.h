//
//  FeedbackHeaderCell.h
//  Umang
//
//  Created by admin on 29/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *feedbackSectionHeader;
@property (weak, nonatomic) IBOutlet UIButton *sectionHeaderImage;
@property (weak, nonatomic) IBOutlet UIView *vwHeader;
@property (weak, nonatomic) IBOutlet UIButton *sectionHeaderbtnfull;
@property (weak, nonatomic) IBOutlet UIView *vwForSection2;

@end
