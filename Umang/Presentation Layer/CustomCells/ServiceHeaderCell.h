//
//  ServiceHeaderCell.h
//  Umang
//
//  Created by admin on 19/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblServiceNotificatn;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceSubTitle;

@end
