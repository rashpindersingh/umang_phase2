//
//  AllServiceCVCellForiPad.h
//  Umang
//
//  Created by admin on 12/04/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "MyButton.h"

@interface AllServiceCVCellForiPad : UICollectionViewCell
@property (weak) IBOutlet UIImageView *serviceImage;
@property (weak) IBOutlet UILabel *serviceTitle;
@property (weak) IBOutlet MyFavButton *serviceFav;
@property (weak) IBOutlet MyButton *serviceInfo;
@property (nonatomic, strong)IBOutlet HCSStarRatingView *starRatingView;


@end
