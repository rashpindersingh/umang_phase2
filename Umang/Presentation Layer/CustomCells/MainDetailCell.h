//
//  MainDetailCell.h
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "AMRatingControl.h"

@interface MainDetailCell : UITableViewCell
@property (nonatomic, strong)AMRatingControl *imagesRatingControl;
@property (weak, nonatomic) IBOutlet UIView *ratingView;

@property (weak, nonatomic) IBOutlet UIButton *btnViewDetails;
@property (nonatomic, strong)IBOutlet UILabel *lbl_titlerateus;

;


@end
