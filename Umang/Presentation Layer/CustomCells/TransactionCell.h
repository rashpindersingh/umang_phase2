//
//  TransactionCell.h
//  Umang
//
//  Created by admin on 12/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgTransaction;
@property (weak, nonatomic) IBOutlet UILabel *headerTransaction;
@property (weak, nonatomic) IBOutlet UILabel *descriptnTransaction;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblDateNtime;

@end
