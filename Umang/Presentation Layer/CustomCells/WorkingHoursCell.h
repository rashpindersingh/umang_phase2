//
//  WorkingHoursCell.h
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkingHoursCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UIButton *btn_workinghours;
@property(nonatomic,retain)IBOutlet UILabel *lbl_workinghours;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *addressOfService;

@end
