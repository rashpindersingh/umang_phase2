//
//  ContactDetailServiceCell.m
//  Umang
//
//  Created by admin on 18/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ContactDetailServiceCell.h"

@implementation ContactDetailServiceCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.serviceContactCollection.dataSource = self;
    self.serviceContactCollection.delegate = self;
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flow.minimumInteritemSpacing = 0;
    flow.minimumLineSpacing = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        flow.minimumInteritemSpacing = 30;
    }
    self.serviceContactCollection.collectionViewLayout = flow;
    self.serviceContactCollection.scrollsToTop = true;
    self.serviceContactCollection.scrollEnabled = false;
    
    if (fDeviceWidth <= 320 ) {
        self.serviceContactCollection.scrollEnabled = true;
        self.serviceContactCollection.showsHorizontalScrollIndicator = true;
        self.serviceContactCollection.contentInset = UIEdgeInsetsMake(0,-5, 0, 0);
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setupDataSource:(NSMutableArray *)dataSource {
    self.dicContact = dataSource;
    [self.serviceContactCollection reloadData];
}
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger countArr = self.dicContact.count;
    return countArr;
    
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ServiceContactCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServiceContactCollectionCell" forIndexPath:indexPath];
    NSDictionary *dic = [self.dicContact objectAtIndex:indexPath.row];
    NSString *kContactType = @"kContactType";
    NSString *kImageName = @"kImageName";
    NSString *kAlpha = @"kalpha";
    
    cell.alpha = [NSString stringWithFormat:@"%@",[dic valueForKey:kAlpha]].floatValue ;
    cell.userInteractionEnabled = cell.alpha == 1.0 ? true : false;
    cell.imageOption.image = [UIImage imageNamed:[dic valueForKey:kImageName]];
    cell.lblTitleContact.text = [dic valueForKey:kContactType];
    cell.lblTitleContact.numberOfLines = 5;
    cell.lblTitleContact.font = [AppFont regularFont:12];
    cell.lblTitleContact.lineBreakMode = NSLineBreakByWordWrapping;
    cell.lblTitleContact.textAlignment = NSTextAlignmentCenter;
    cell.lblTitleContact.textColor = [UIColor colorWithHexString:@"1C1C1C"];
    if ([kContactType isEqualToString:NSLocalizedString(@"visit_website", nil)]) {
        //cell.lblTitleContact.textAlignment = NSTextAlignmentRight;
        
    }
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [self.dicContact objectAtIndex:indexPath.row];
    NSString *kContactType = @"kContactType";
    NSString *name = [dic valueForKey:kContactType];
    CGRect dynamicHeight = [self rectForText:name usingFont:[UIFont systemFontOfSize:16.0] boundedBySize:CGSizeMake(fDeviceWidth, 0)];
    CGFloat widthCount = fDeviceWidth / self.dicContact.count;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGFloat widthCount = (fDeviceWidth - 90) / self.dicContact.count;
        if (indexPath.row <= 1)  {
            return CGSizeMake(widthCount - 20, 80);
        }
        return CGSizeMake(widthCount + 5, 80);
    }
    CGFloat textWidth = dynamicHeight.size.width ;//MAX(, dynamicHeight.size.width + 20);
    CGFloat diff = (widthCount - textWidth ) / 1.4;
    CGFloat finalWidth = textWidth + diff;
    if (textWidth > widthCount )  {
        finalWidth = widthCount ;
    }
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (indexPath.row > 1)  {
        finalWidth = widthCount + 8;
        if ([currentSelectLang isEqualToString:@"ta-IN"] || [currentSelectLang isEqualToString:@"te-IN"])//te-IN if telgu
        {
            finalWidth = widthCount;
        }
        
    }
    
    
    return CGSizeMake(finalWidth, 80);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = [self.dicContact objectAtIndex:indexPath.row];
    // self.Contactblock([dic valueForKey:kImageName]);
    // NSString *kContactType = @"kContactType";
    NSString *kImageName = @"kImageName";
    self.ContactServiceBlock([dic valueForKey:kImageName]);
}
#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return 30;
    }
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return 30;
    }
    return 0.0;
}


@end
