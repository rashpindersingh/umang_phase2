//
//  LinkedTableCell.h
//  Umang
//
//  Created by admin on 04/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyButton.h"

@interface LinkedTableCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UIImageView *img_socialprofile;
@property(nonatomic,retain)IBOutlet UILabel *lbl_titleName;
@property(nonatomic,retain)IBOutlet UILabel *lbl_linkedSite;
//@property(nonatomic,retain)IBOutlet UIButton *btn_link;

@property (weak) IBOutlet MyButton *btn_link;

@end
