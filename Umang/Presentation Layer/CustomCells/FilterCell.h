//
//  FilterCell.h
//  Umang
//
//  Created by admin on 12/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckUncheck;
@property (weak, nonatomic) IBOutlet UIImageView *imgService;

@end
