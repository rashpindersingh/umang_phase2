//
//  SettingNotificationCell.h
//  Umang
//
//  Created by spice_digital on 30/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingNotificationCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UIImageView *img_service;
@property(nonatomic,retain)IBOutlet UILabel *lbl_serviceTitle;
@property(nonatomic,retain)IBOutlet UISwitch *btn_switch;
@property(nonatomic,retain)IBOutlet UIView *vw_line;

@end
