//
//  AllServiceCVCell.m
//  AllServiceTabVC
//
//  Created by deepak singh rawat on 28/09/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import "AllServiceCVCell.h"
#import <QuartzCore/QuartzCore.h>

#import "ScrollNotificationVC.h"


@implementation AllServiceCVCell
@synthesize starRatingView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [self addLayerAndShadow];
    
}

    
    
-(void)addLayerAndShadow
{
    
    self.layer.shadowOffset = CGSizeMake(0.2, 1.5);
    self.layer.shadowColor = [UIColor grayColor].CGColor;
    self.layer.shadowRadius = 2;
    self.layer.shadowOpacity = 0.5;
    self.layer.masksToBounds = NO;
    
    
    
    //add border
    
    UIView *borderView = [UIView new];
    borderView.backgroundColor = [UIColor whiteColor];
    borderView.frame = self.bounds;
    borderView.layer.cornerRadius = 5.0;
    borderView.clipsToBounds = true;
    // borderView.layer.borderWidth=0.1;
    //  borderView.layer.borderColor = [UIColor grayColor].CGColor;
    borderView.layer.masksToBounds = true;
    [self addSubview:borderView];
    [self sendSubviewToBack:borderView];
    
}




@end
