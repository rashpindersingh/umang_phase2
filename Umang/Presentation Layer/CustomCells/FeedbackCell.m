//
//  FeedbackCell.m
//  Umang
//
//  Created by admin on 29/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "FeedbackCell.h"

@implementation FeedbackCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(10,10,32,32);
    self.textLabel.frame=CGRectMake(50, 10, fDeviceWidth-50, 32);
    self.textLabel.backgroundColor=[UIColor clearColor];
}
@end
