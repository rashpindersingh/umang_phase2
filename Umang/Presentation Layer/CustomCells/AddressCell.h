//
//  AddressCell.h
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UILabel *lbl_title_address;
@property(nonatomic,retain)IBOutlet UILabel *lbl_address;
@property(nonatomic,retain)IBOutlet UIButton *btn_viewmap;

@end
