//
//  ServiceDirDetailVC.m
//  Umang
//
//  Created by spice_digital on 28/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ServiceDirDetailVC.h"

#import "NotificationSettingTitleCell.h"
#import "ServiceNameCell.h"
#import "AboutServiceCell.h"
#import "HelpCell.h"
#import "ContactCell.h"
#import "ServDirectDetailBO.h"

@interface ServiceDirDetailVC () <UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrEmailData;
    NSMutableArray *arrLinkData;
    //    ServiceNameCell *serviceName;
    //    AboutServiceCell *aboutCell;
    NotificationSettingTitleCell  *NotificationCell;
}
@end

@implementation ServiceDirDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //Google Tracking
    
    
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: SERVICE_DIRECTORY_DETAIL_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    // Do any additional setup after loading the view.
    
    _tblServicedtl.delegate = self;
    _tblServicedtl.dataSource = self;
    arrEmailData  = [[NSMutableArray alloc]init];
    arrLinkData = [[NSMutableArray alloc]init];
    
    [self prepareTempDataForEmail];
    [self prepareTempDataForLinkSection];
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——

    // [self setNeedsStatusBarAppearanceUpdate];
    //  [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:.1];
  /*  UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backBtnAction:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.navigationController.view addGestureRecognizer:gestureRecognizer];
    */
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
}


 





- (UIStatusBarStyle)preferredStatusBarStyle

{
    return UIStatusBarStyleDefault;
}


-(IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    // [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)prepareTempDataForEmail
{
    
    arrEmailData = [[NSMutableArray alloc]init];
    
    ServDirectDetailBO *objFeedback = [[ServDirectDetailBO alloc] init];
    objFeedback.strEmailId = @"cpfc@epfindia.gov.in";
    objFeedback.strEmailImage = @"passport.png";
    //objNotification.filterType = @"notification_type";
    
    [arrEmailData addObject:objFeedback];
    objFeedback = nil;
    
    
    
    objFeedback = [[ServDirectDetailBO alloc] init];
    objFeedback.strEmailId = @"cvo@epfindia.gov.in";
    objFeedback.strEmailImage = @"epfo.png";
    
    [arrEmailData addObject:objFeedback];
    objFeedback = nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0.001;
    }
    else
    {
        return 44.0;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    static NSString *CellIdentifier = @"NotificationSettingTitleCell";
    NotificationCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (NotificationCell == nil)
    {
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    
    if (section == 0)
    {
        
    }
    else if (section == 1)
    {
        NotificationCell.lblNotificatnSetting.text = @"ABOUT EPFO";
    }
    
    else if (section == 2)
    {
        NotificationCell.lblNotificatnSetting.text = @"CONTACT";
        
    }
    
    else if (section == 3)
    {
        NotificationCell.lblNotificatnSetting.text = @"Email";
    }
    
    else
    {
        NotificationCell.lblNotificatnSetting.text = @"Links";
    }
    
    
    
    NotificationCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    return NotificationCell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.section == 0) {
        return 120;
    }
    else if (indexPath.section ==1)
    {
        return 60;
    }
    else if (indexPath.section ==2)
    {
        return 110;
    }
    else
    {
        return 50;
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    switch (section)
    {
        case 0:
            return 1;
            break;
            
        case 1:
            return 1;
            break;
            
        case 2:
            return 1;
            break;
            
        case 3:
            return arrEmailData.count;
            break;
            
        case 4:
            return arrLinkData.count;
            break;
            
            
        default:
            break;
    }
    
    return 0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"ServiceNameCell";
        ServiceNameCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        cell.nameService.text = @"EPFO";
        cell.imgService.image = [UIImage imageNamed:@"epfo.png"];
        
        return cell;
        
    }
    else if (indexPath.section == 1)
    {
        
        static NSString *CellIdentifierType = @"AboutServiceCell";
        AboutServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
        
        cell.aboutServiceDescription.text = @"Lorem ipsum dolor sit amet,consecteturbadipiscing elit, sed do eiusmod tempor incididunt ut labour et dolore manga aliqua.";
        
        return cell;
        
    }
    
    else if (indexPath.section == 2)
    {
        
        static NSString *CellIdentifierType = @"ContactCell";
        ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
        
        cell.HeadOfficeName.text = @"EPFO HEAD OFFICE";
        cell.subHeaderlbl.text = @"Bhavishya Nidhi Bhawan";
        cell.addresslbl.text = @"14, Bhikaji Kama Place";
        cell.statePinLbl.text = @"New Delhi - 110 066,";
        cell.phoneLbl.text = @"011 26172671";
        
        return cell;
        
    }
    
    
    else if (indexPath.section == 3)
    {
        
        ServDirectDetailBO *objFeedback = nil;
        
        static NSString *CellIdentifierType = @"HelpCell";
        HelpCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
        
        {
            objFeedback = arrEmailData[indexPath.row];
            cell.lbl_celltitle.text = objFeedback.strEmailId;
            cell.img_cell.image = [UIImage imageNamed:objFeedback.strEmailImage];
        }
        
        return cell;
        
    }
    
    
    else
    {
        
        ServDirectDetailBO *objFeedback = nil;
        
        static NSString *CellIdentifierType = @"HelpCell";
        HelpCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
        
        {
            objFeedback = arrLinkData[indexPath.row];
            cell.lbl_celltitle.text = objFeedback.strEmailId;
            cell.img_cell.image = [UIImage imageNamed:objFeedback.strEmailImage];
        }
        
        return cell;
        
    }
}






-(void)prepareTempDataForLinkSection
{
    
    arrLinkData = [[NSMutableArray alloc]init];
    
    ServDirectDetailBO *objFeedback = [[ServDirectDetailBO alloc] init];
    objFeedback.strEmailId = @"http://twitter.com/socialepfo/";
    objFeedback.strEmailImage = @"passport.png";
    //objNotification.filterType = @"notification_type";
    
    [arrLinkData addObject:objFeedback];
    objFeedback = nil;
    
    
    
    objFeedback = [[ServDirectDetailBO alloc] init];
    objFeedback.strEmailId = @"http://www.facebook.com/socialepfo/";
    objFeedback.strEmailImage = @"epfo.png";
    
    [arrLinkData addObject:objFeedback];
    objFeedback = nil;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
*/
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
