//
//  LanguageSelectVC.h
//  Umang
//
//  Created by admin on 16/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageSelectVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblSelectLanguage;
- (IBAction)btnNextClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btndropDown;

@property (nonatomic, assign) BOOL isLanguageScreen;
@property (nonatomic, assign) BOOL languageScreenShown;

@end
