//
//  LoginWithOTPVC.h
//  Umang
//
//  Created by admin on 21/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginWithOTPVC : UIViewController
{
    SharedManager*  singleton ;
    
    
    IBOutlet UIScrollView *scrollview;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak,nonatomic) NSString *strMobileNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
//----- added for passing rty and tout-----
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
//----- added for passing rty and tout-----

@end
