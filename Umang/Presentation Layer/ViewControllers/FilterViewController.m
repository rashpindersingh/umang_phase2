//
//  FilterViewController.m
//  Umang
//
//  Created by admin on 07/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "FilterViewController.h"
#import "FilterCell.h"
#import "SortFilterCell.h"
#import "FilterServicesBO.h"
#import "DateOptionView.h"
#import "NotificationTypeCell.h"
#import "SearchFilterVC.h"
#import "StateList.h"
#import "StateCustomCell.h"
#import "SDCapsuleButton.h"
#import "CZPickerView.h"
#import "NotificationItemBO.h"
#import "UIImageView+WebCache.h"

#define BORDER_COLOR [UIColor colorWithRed:112.0/255.0 green:194.0/255.0 blue:138.0/255.0 alpha:1.0]


@interface FilterViewController ()<CZPickerViewDelegate,CZPickerViewDataSource,filterChangeDelegate>

{
    NSMutableArray *arrNotificationTypes;
    NSMutableArray *arrStateNames;
    NSMutableArray *arrNotiImage;
    NSArray *arry_state;
    FilterServicesBO *objFilter;
    UIView*  vw_dateToolbar;
    UIDatePicker *datepicker;
    UIButton*  btnDone;
    UIButton*  btncancel;
    __weak IBOutlet UIButton *btnReset;
    // BOOL isRegionalSelected;
    StateList *obj;
    NSArray* serviceCategory;
    NSMutableArray *arrFirstFilterItems;
    // NSMutableArray *arrSecondFilterItems;
    DateOptionView *fromDate;
    DateOptionView *toDate;
    SharedManager *singleton;
    
    DateOptionView *fromTime;
    DateOptionView *toTime;
    
    FilterCell *filterCell;
    
    UIDatePicker *dtPickerFrom;
    UIDatePicker *dtPickerTo;
    
    
    UIDatePicker *timePickerFrom;
    UIDatePicker *timePickerTo;
    
    NSString *selectedFirstFilter;
    // NSString *selectedSecondFilter;
    // NSMutableArray *arrStates;
    
    NSString *_selectedNotificationType;
    NSMutableArray *_arrNotificationSetting;
    // NSMutableArray *_arrServiceFilter;
    
    NSMutableArray *arrSelectedItemForPicker;
    
    BOOL resetFilter;
}


@end

@implementation FilterViewController
@synthesize isRegionalSelected,btnApply;
@synthesize btnReset;


@synthesize fromdatestring;
@synthesize todatestring;


- (void)filterChanged:(NSMutableArray*)parameters andCategoryDict:(NSMutableDictionary *)dict
{
    
    
    //For Notification Type
    
    resetFilter=TRUE;
    
    //-----------------
    // [_arrNotificationSetting removeAllObjects];
    
    if ([dict valueForKey:@"selectedNotification"] != nil)
    {
        if ([[dict valueForKey:@"selectedNotification"] isEqualToString:NSLocalizedString(@"promotional_small", nil)])
        {
            _selectedNotificationType = NSLocalizedString(@"promotional_small", nil);
        }
        else if ([[dict valueForKey:@"selectedNotification"] isEqualToString:NSLocalizedString(@"transactional_small", nil)])
        {
            _selectedNotificationType = NSLocalizedString(@"transactional_small", nil);
        }
        else
        {
            _selectedNotificationType = NSLocalizedString(@"all", nil);
        }
    }
    else
    {
        _selectedNotificationType = NSLocalizedString(@"all", nil);
    }
    
    
    if ([[dict valueForKey:@"selectedStates"] count] > 0)
    {
        isRegionalSelected = YES;
        arrStateNames = [dict valueForKey:@"selectedStates"];
        
        if ([arrStateNames containsObject:NSLocalizedString(@"all", nil)])
        {
            arrSelectedItemForPicker = [NSMutableArray new];
        }
        else
        {
            [arrSelectedItemForPicker removeAllObjects];
            
            [arry_state enumerateObjectsUsingBlock:^(id  _Nonnull object, NSUInteger idx, BOOL * _Nonnull stop)
             {
                 if ([arrStateNames containsObject:object])
                 {
                     [arrSelectedItemForPicker addObject:[NSNumber numberWithInteger:idx]];
                 }
             }];
        }
        
    }
    else
    {
        isRegionalSelected = NO;
        arrStateNames = [NSMutableArray new];
        arrSelectedItemForPicker = [NSMutableArray new];
    }
    
    // _arrNotificationSetting = parameters;
    
    [_tblFilter reloadData];
    [_tblFilter setNeedsLayout ];
    [_tblFilter layoutIfNeeded ];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    if (resetFilter==FALSE)
    {
        [self addNotificationSettingData];
    }
    [_tblFilter reloadData];
    [_tblFilter setNeedsLayout ];
    [_tblFilter layoutIfNeeded ];
    [_tblFilter reloadData];
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetNotification:) name:@"SendDate" object:nil];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    [super viewWillAppear:NO];
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    [_btnBackHome.titleLabel setFont:[AppFont regularFont:17.0]];
    [btnReset.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _lblFilter.font = [AppFont semiBoldFont:17];
    [btnApply.titleLabel setFont:[AppFont regularFont:20.0]];
}
#pragma mark -
- (void)viewDidLoad
{
    
    resetFilter =FALSE;
    singleton = [SharedManager sharedSingleton];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"dd/MM/yyyy HH:mm:ss"];
    todatestring= [formatter stringFromDate:[NSDate date]]; // Convert date to string
    
    
    
    
    NSDateFormatter *formatterstart = [[NSDateFormatter alloc] init];
    [formatterstart setDateFormat: @"dd/MM/yyyy 00:00:00"];
    
    fromdatestring = [formatterstart stringFromDate:[NSDate date]]; // Convert date to string
    
    
    /* NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
     [gregorian setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
     NSDateComponents *components = [gregorian components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
     [components setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
     NSDate *beginningOfToday = [gregorian dateFromComponents:components];
     
     todatestring = [formatter stringFromDate:beginningOfToday]; // Convert date to string
     */
    NSLog(@"fromdatestring :%@",fromdatestring);
    NSLog(@"todatestring :%@",todatestring);
    
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: NOTIFICATION_FILTER_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    //  [_btnBackHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    arrSelectedItemForPicker = [NSMutableArray new];
    
    obj=[[StateList alloc]init];
    _selectedNotificationType = NSLocalizedString(@"all", nil);
    [btnReset setTitle:NSLocalizedString(@"reset", nil) forState:UIControlStateNormal];
    [btnApply setTitle:[NSLocalizedString(@"apply_label", nil) uppercaseString] forState:UIControlStateNormal];
    [btnReset setTitle:NSLocalizedString(@"reset", nil) forState:UIControlStateNormal];
    [_btnBackHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_btnBackHome.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(_btnBackHome.frame.origin.x, _btnBackHome.frame.origin.y, _btnBackHome.frame.size.width, _btnBackHome.frame.size.height);
        
        [_btnBackHome setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _btnBackHome.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    _lblFilter.text = NSLocalizedString(@"filter", nil);
    
    
    arrStateNames = [NSMutableArray new];
    [arrStateNames addObject:NSLocalizedString(@"all", nil)];
    
    objFilter = [arrNotificationTypes objectAtIndex:0];
    
    
    
    isRegionalSelected = NO;
    
    [self prepareTempDataForServicesType];
    [self.tblFilter reloadData];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    
    [self addNavigationView];
}

#pragma mark- add Navigation View to View

-(void)addNavigationView
{
    _btnBackHome.hidden = true;
    _lblFilter.hidden = true;
    
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btnbackClicked:btnBack];
    };
    nvView.lblTitle.text = _lblFilter.text;
    
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    
//    if (iPhoneX())
//    {
//        CGRect tbFrame = self.tblFilter.frame;
//        tbFrame.origin.y = kiPhoneXNaviHeight
//        self.tblFilter.frame = tbFrame;
//        [self.view layoutIfNeeded];
//    }
}

#pragma mark -

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)addNotificationSettingData
{
    
    if (_arrNotificationSetting == nil) {
        _arrNotificationSetting = [[NSMutableArray alloc]init];
        
    }
    else
    {
        [_arrNotificationSetting removeAllObjects];
    }
    
    NSArray *arrSettingData  = [singleton.dbManager loadDataServiceData];
    
    for (int i = 0; i < arrSettingData.count; i++)
    {
        NSDictionary *dict = [arrSettingData objectAtIndex:i];
        NotificationItemBO *objSetting = [[NotificationItemBO alloc]init];
        objSetting.titleService = [dict valueForKey:@"SERVICE_NAME"];
        objSetting.imgService=[dict valueForKey:@"SERVICE_IMAGE"];
        objSetting.idService = [dict valueForKey:@"SERVICE_ID"];
        
        //        objSetting.ID = [dict valueForKey:@"SERVICE_ID"];
        //        objSetting.statusTypeForServices = [dict valueForKey:@"SERVICE_IS_HIDDEN"];
        
        [_arrNotificationSetting addObject: objSetting];
        
    }
    
    [_tblFilter reloadData];
    
}


-(void)setUpNavigationProperties{
    [self.navigationController.navigationBar setTranslucent:YES];
    
    [self.navigationController.navigationBar setShadowImage:nil];
    // "Pixel" is a solid white 1x1 image.
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    // self.navigationController.navigationBar.barTintColor = DEFAULT_NAV_BAR_COLOR;
    
    self.title =NSLocalizedString(@"filter", nil);
    [btnReset setTitle:NSLocalizedString(@"apply_label", nil) forState:UIControlStateNormal];
    
    // Add Reset Button on right side
    UIBarButtonItem *btnReset =    [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"reset", nil) style:UIBarButtonItemStyleDone target:self action:@selector(btnResetClicked)];
    self.navigationItem.rightBarButtonItem = btnReset;
}

-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}

-(void)prepareTempDataForServicesType{
    
    // Prepare State Data
    
    arry_state = [obj getStateList];
    
    arry_state = [arry_state sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    arrFirstFilterItems = [NSMutableArray new];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"alphabetic", nil) forKey:@"Title"];
    [dict setObject:@"icon_alphabatic.png" forKey:@"NormalState"];
    [dict setObject:@"icon_alphabatic_selected.png" forKey:@"SelectedState"];
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    selectedFirstFilter = NSLocalizedString(@"alphabetic", nil);
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"most_popular", nil) forKey:@"Title"];
    [dict setObject:@"icon_most_popular.png" forKey:@"NormalState"];
    [dict setObject:@"icon_most_popular_select.png" forKey:@"SelectedState"];
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"top_rated", nil) forKey:@"Title"];
    [dict setObject:@"icon_top_rated.png" forKey:@"NormalState"];
    [dict setObject:@"icon_top_rated_select.png" forKey:@"SelectedState"];
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    
    //For Notification Type
    arrNotificationTypes = [[NSMutableArray alloc]init];
    
    FilterServicesBO *objNotification = [[FilterServicesBO alloc] init];
    objNotification.serviceName = NSLocalizedString(@"all", nil);
    objNotification.strImg = @"icon_all.png";
    objNotification.filterType = @"notification_type";
    
    [arrNotificationTypes addObject:objNotification];
    objNotification = nil;
    
    objNotification = [[FilterServicesBO alloc] init];
    objNotification.serviceName =  NSLocalizedString(@"promotional_small", nil);
    [arrNotificationTypes addObject:objNotification];
    objNotification.strImg = @"icon_promotional.png";
    objNotification.filterType = @"notification_type";
    
    objNotification = nil;
    
    objNotification = [[FilterServicesBO alloc] init];
    objNotification.serviceName =  NSLocalizedString(@"transactional_small", nil);
    [arrNotificationTypes addObject:objNotification];
    objNotification.strImg = @"icon_transactional.png";
    objNotification.filterType = @"notification_type";
    
    objNotification = nil;
    
    
    objNotification = [[FilterServicesBO alloc] init];
    objNotification.serviceName =  NSLocalizedString(@"favourites_small", nil);
    [arrNotificationTypes addObject:objNotification];
    objNotification.strImg = @"notification_favorites";
    objNotification.filterType = @"notification_type";
    
    objNotification = nil;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (isRegionalSelected) {
        if (section == 4) {
            if ([arrStateNames count] > 0)
            {
                return 10;
            }
            else
            {
                return 0;
            }
        }
        return 50.0;
    }
    else{
        if (section == 0 || section == 1 || section == 2 || section == 3) {
            return 50.0;
        }
        else{
            return 0.001;
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    static NSString *CellIdentifier = @"NotificationHeaderCell";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    UILabel *lblSectionTitle = (UILabel*)[headerView viewWithTag:122];
    UIButton *btnClear = (UIButton*)[headerView viewWithTag:123];
    // [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateNormal];
    
    
    [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateSelected];
    [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateNormal];
    
    
    CGRect buttonClearType =  btnClear.frame;
    buttonClearType.origin.x = singleton.isArabicSelected ? 10 : 250;
    btnClear.frame = buttonClearType;
    
    
    CGRect labelFrame =  lblSectionTitle.frame;
    labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
    lblSectionTitle.frame = labelFrame;
    
    
    if (section == 0) {
        
        lblSectionTitle.text = NSLocalizedString(@"notification_type", nil);
        btnClear.hidden = YES;
    }
    else if (section == 1)
    {
        
        lblSectionTitle.text = NSLocalizedString(@"date_range", nil);
        btnClear.hidden = YES;
        
    }
    
    else if (section == 2)
    {
        
        lblSectionTitle.text = NSLocalizedString(@"services", nil);
        NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.isServiceSelected == YES"];
        NSArray *arrServices = [_arrNotificationSetting filteredArrayUsingPredicate:predicateFilter];
        
        if ([arrServices count]) {
            btnClear.hidden = NO;
        }
        else{
            btnClear.hidden = YES;
        }
        
        [btnClear addTarget:self action:@selector(btnClearFilterCategoriesClicked) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else if (section == 3)
    {
        
        lblSectionTitle.text = NSLocalizedString(@"state_label", nil);
        [btnClear addTarget:self action:@selector(btnClearStateClicked) forControlEvents:UIControlEventTouchUpInside];
        
        if ([arrStateNames count]) {
            btnClear.hidden = NO;
        }
        else{
            btnClear.hidden = YES;
        }
        
        
    }
    
    else if (section == 4)
    {
        btnClear.hidden = YES;
        lblSectionTitle.text = @"";
    }
    
    headerView.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    lblSectionTitle.font = [AppFont regularFont:13.0];
    btnClear.titleLabel.font = [AppFont mediumFont:13.0];
    return headerView;
}



-(void)btnClearStateClicked
{
    
    [arrStateNames removeAllObjects];
    [arrStateNames addObject:NSLocalizedString(@"all", nil)];
    [_tblFilter reloadData];
    
}
-(void)btnClearFilterCategoriesClicked

{
    [_arrNotificationSetting setValue:@NO forKey:@"isServiceSelected"];
    [_tblFilter reloadData];
    
}

- ( UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}// custom view for footer. will be adjusted to default or specified footer height


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.section == 0)
    {
        return  40;
    }
    else if (indexPath.section == 1)
    {
        return 90;
        
        
    }
    else if (indexPath.section == 2)
    {
        return 40;
    }
    else if (indexPath.section == 3)
    {
        
        return [self heightForDynamicCell];
    }
    else
        return 70;
    
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    if (section == 2) {
        return _arrNotificationSetting.count;
    }
    else if (section == 0)
    {
        return 4;
    }
    
    else{
        return 1;
    }
}



-(void)btnClickedFromCellForFistSection:(UIButton*)sender{
    SDCapsuleButton *superVw = (SDCapsuleButton*)[sender superview];
    selectedFirstFilter = [superVw btnTitle];
    
    [_tblFilter reloadData];
    
}

-(void)removeToolBarPicker{
    if (vw_dateToolbar) {
        [[vw_dateToolbar subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [vw_dateToolbar removeFromSuperview];
        vw_dateToolbar = nil;
    }
    
}

-(void)addToolBar
{
    [self removeToolBarPicker];
    
    vw_dateToolbar =[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-270.0, fDeviceWidth, 44)];
    
    vw_dateToolbar.backgroundColor=[UIColor blackColor];
    
    [self.view addSubview:vw_dateToolbar];
    
    btncancel = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btncancel setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    btncancel.titleLabel.textColor=[UIColor blueColor];
    
    btncancel.frame = CGRectMake(10.0,5.0,100.0, 40.0);
    [btncancel addTarget:self
                  action:@selector(HidePicker:)
        forControlEvents:UIControlEventTouchUpInside];
    [vw_dateToolbar addSubview:btncancel];
    
    
    
    
    
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDone.titleLabel.textColor=[UIColor blueColor];
    
    [btnDone setTitle:NSLocalizedString(@"ok", nil) forState:UIControlStateNormal];
    btnDone.frame = CGRectMake(fDeviceWidth-110,5,100.0, 40.0);
    [btnDone addTarget:self
                action:@selector(HidePicker:)
      forControlEvents:UIControlEventTouchUpInside];
    [vw_dateToolbar addSubview:btnDone];
    
    
}


-(void)showFromDatePicker:(DateOptionView*)from{
    
    if (dtPickerFrom == nil) {
        
        dtPickerFrom = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame)-225, CGRectGetWidth(self.view.frame), 225)];
        dtPickerFrom.datePickerMode = UIDatePickerModeDate;
        dtPickerFrom.backgroundColor = [UIColor whiteColor];
        
        dtPickerFrom.maximumDate = [NSDate date];
        
        [dtPickerFrom addTarget:self action:@selector(updateFromDate:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:dtPickerFrom];
    }
    [self addToolBar];
    
    
    
    dtPickerTo.hidden = YES;
    dtPickerFrom.hidden = NO;
}


-(void)showToDatePicker:(DateOptionView*)to
{
    if (dtPickerTo == nil) {
        dtPickerTo = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame)-225, CGRectGetWidth(self.view.frame), 225)];
        dtPickerTo.datePickerMode = UIDatePickerModeDate;
        [dtPickerTo addTarget:self action:@selector(updateToDate:) forControlEvents:UIControlEventValueChanged];
        
        
        dtPickerTo.maximumDate = [NSDate date];
        
        dtPickerTo.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:dtPickerTo];
    }
    
    
    [self addToolBar];
    
    dtPickerFrom.hidden = YES;
    dtPickerTo.hidden = NO;
}




-(void)showFromTimePicker:(DateOptionView*)from{
    
    if (timePickerFrom == nil) {
        
        timePickerFrom = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame)-225, CGRectGetWidth(self.view.frame), 225)];
        timePickerFrom.datePickerMode = UIDatePickerModeTime;
        timePickerFrom.backgroundColor = [UIColor whiteColor];
        [timePickerFrom addTarget:self action:@selector(updateFromTime:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:timePickerFrom];
    }
    [self addToolBar];
    
    
    
    timePickerTo.hidden = YES;
    timePickerFrom.hidden = NO;
}


-(void)showToTimePicker:(DateOptionView*)from{
    
    if (timePickerTo == nil) {
        
        timePickerTo = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame)-225, CGRectGetWidth(self.view.frame), 225)];
        timePickerTo.datePickerMode = UIDatePickerModeTime;
        timePickerTo.backgroundColor = [UIColor whiteColor];
        [timePickerTo addTarget:self action:@selector(updateToTime:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:timePickerTo];
    }
    [self addToolBar];
    
    
    
    timePickerTo.hidden = NO;
    timePickerFrom.hidden = YES;
}




-(IBAction)HidePicker:(id)sender
{
    
    [self removeToolBarPicker];
    
    dtPickerTo.hidden = YES;
    dtPickerFrom.hidden = YES;
    
    timePickerTo.hidden = YES;
    timePickerFrom.hidden = YES;
    
}



-(void)updateFromDate:(UIDatePicker*)dtPicker
{
    if (dtPickerTo != nil)
    {
        dtPickerFrom.maximumDate = dtPickerTo.date;
    }
    else
    {
        dtPickerFrom.maximumDate = [NSDate date];
    }
    
    [fromDate updateContentValues:dtPicker.date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy 00:00:00"];
    fromdatestring = [formatter stringFromDate:dtPickerFrom.date]; // Convert date to string
    NSLog(@"fromdatestring %@",fromdatestring);
    // NSLog(@"%@",[fromDate updateContentValues:dtPicker.date]);
    [_tblFilter reloadData];
    
}


-(void)updateToDate:(UIDatePicker*)dtPicker
{
    
    
    
    
    if (dtPickerFrom != nil)
    {
        dtPickerTo.minimumDate = dtPickerFrom.date;
    }
    else
    {
        dtPickerTo.minimumDate = [NSDate date];
    }
    
    [toDate updateContentValues:dtPicker.date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"dd/MM/yyyy 23:59:59"];
    
    
    //NSDateFormatter *formatterstart = [[NSDateFormatter alloc] init];
    // [formatterstart setDateFormat: @"dd/MM/yyyy 00:00:00"];
    
    todatestring = [formatter stringFromDate:dtPicker.date]; // Convert date to string
    
    
    //todatestring = [formatter stringFromDate:dtPickerTo.date]; // Convert date to string
    NSLog(@"todatestring %@",todatestring);
    
    
    [_tblFilter reloadData];
}


-(void)updateFromTime:(UIDatePicker*)timePicker
{
    [fromTime updateValues:timePicker.date];
    [_tblFilter reloadData];
    
}


-(void)updateToTime:(UIDatePicker*)timePicker
{
    [toTime updateValues:timePicker.date];
    [_tblFilter reloadData];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    if (indexPath.section == 0)
    {
        //  FilterServicesBO *objFilter = nil;
        
        static NSString *CellIdentifierType = @"NotificationTypeCell";
        NotificationTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
        
        [cell.btnCheckUncheck addTarget:self action:@selector(btnCheckboxClickedForNotifications:) forControlEvents:UIControlEventTouchUpInside];
        cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        
        
        objFilter = arrNotificationTypes[indexPath.row];
        cell.btnCheckUncheck.tag = 500+indexPath.row;
        cell.lblType.text = objFilter.serviceName;
        cell.lblType.font = [UIFont systemFontOfSize:16.0];
        cell.imgNotification.image = [UIImage imageNamed:objFilter.strImg];
        
        if ([objFilter.serviceName isEqualToString:_selectedNotificationType])
        {
            //cell.btnCheckUncheck.backgroundColor = [UIColor lightGrayColor];
            [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"icon_radio_button_select.png"] forState:UIControlStateNormal];
            
        }
        else{
            // cell.btnCheckUncheck.backgroundColor = [UIColor colorWithRed:46.0/255.0 green:156.0/255.0 blue:78.0/255.0 alpha:1.0];
            [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"icon_radio_button.png"] forState:UIControlStateNormal];
            
        }
        
        
        if (singleton.isArabicSelected) {
            
            CGRect frameImage = cell.imgNotification.frame;
            frameImage.origin.x = fDeviceWidth - 50;
            cell.imgNotification.frame = frameImage;
            
            
            CGRect frameLabelType = cell.lblType.frame;
            frameLabelType.origin.x = fDeviceWidth - 50 - frameLabelType.size.width;
            cell.lblType.frame = frameLabelType;
            cell.lblType.textAlignment = NSTextAlignmentRight;
            
            CGRect frameBtn = cell.btnCheckUncheck.frame;
            frameBtn.origin.x = 10;
            cell.btnCheckUncheck.frame = frameBtn;
            
            
        }
        else{
            
            CGRect frameImage = cell.imgNotification.frame;
            frameImage.origin.x = 10;
            cell.imgNotification.frame = frameImage;
            
            
            CGRect frameLabelType = cell.lblType.frame;
            frameLabelType.origin.x = 50;
            cell.lblType.frame = frameLabelType;
            cell.lblType.textAlignment = NSTextAlignmentLeft;
            
            
            CGRect frameBtn = cell.btnCheckUncheck.frame;
            frameBtn.origin.x = fDeviceWidth - 50;
            cell.btnCheckUncheck.frame = frameBtn;
            
        }
        
        
        return cell;
        
        
    }
    
    else if (indexPath.section == 1)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateOptionViewCell"];
        
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat halfScreenWidth = self.view.frame.size.width/2;
        CGFloat componentWidth = 100;
        CGFloat componentHeight = 70;
        
        CGFloat xCord = (halfScreenWidth - componentWidth)/2;
        
        UIImageView *imgArrow= [[UIImageView alloc]initWithFrame:CGRectMake(halfScreenWidth-20, 30, 30, 30)];
        [cell.contentView addSubview:imgArrow];
        imgArrow.image = [UIImage imageNamed:@"arrow_grey.png"];
        
        
        // Design Left Section
        if (fromDate == nil)
        {
            
            fromDate = [[DateOptionView alloc] initWithFrame:CGRectMake(xCord, 10, componentWidth, componentHeight) designedForState:NSLocalizedString(@"from", nil)];
            [fromDate addTarget:self action:@selector(showFromDatePicker:) forControlEvents:UIControlEventTouchUpInside];
            [fromDate updateContentValues:[NSDate date]];
        }
        [cell.contentView addSubview:fromDate];
        
        xCord = halfScreenWidth + (halfScreenWidth - componentWidth)/2;
        
        
        // Design Right Section
        if (toDate == nil) {
            toDate = [[DateOptionView alloc] initWithFrame:CGRectMake(xCord, 10, componentWidth, componentHeight) designedForState:NSLocalizedString(@"to", nil)];
            [toDate addTarget:self action:@selector(showToDatePicker:) forControlEvents:UIControlEventTouchUpInside];
            [toDate updateContentValues:[NSDate date]];
            
        }
        [cell.contentView addSubview:toDate];
        
        
        
        //  CGRect frameFromDate = fromDate.frame;
        //  CGRect frameToDate = toDate.frame;
        
        //  fromDate.frame = frameToDate;
        // toDate.frame = frameFromDate;
        
        
        
        return cell;
        
    }
    
    
    else if (indexPath.section == 2)
    {
        
        static NSString *CellIdentifierType = @"TypeCellIdentifier";
        filterCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
        [filterCell.btnCheckUncheck addTarget:self action:@selector(btnCheckboxClickedForServices:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        NotificationItemBO *objNotifiBO = [_arrNotificationSetting objectAtIndex:indexPath.row];
        filterCell.lblType.text  =  objNotifiBO.titleService;
        
        
        
        NSURL *url=  [NSURL URLWithString:objNotifiBO.imgService];
        
        [filterCell.imgService sd_setImageWithURL:url
                                 placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
        
        
        
        filterCell.btnCheckUncheck.tag = 500+indexPath.row;
        
        
        
        filterCell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        
        if (objNotifiBO.isServiceSelected)
        {
            
            [filterCell.btnCheckUncheck setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            
            [filterCell.btnCheckUncheck setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
            
        }
        
        filterCell.lblType.font = [AppFont regularFont:14];
        
        return filterCell;
    }
    
    
    else
    {
        static NSString *cellIdentifierState = @"StateNameCell";
        StateCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifierState];
        cell.textLabel.text = @"";
        
        // Remove Previous contents
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        if (indexPath.section == 3)
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            // Add Capsule Buttons for Selected States
            [self designCellContentForStateNamesOnCelView:cell.contentView];
        }
        else{
            
            UIView *tempVw = [[UIView alloc] initWithFrame:CGRectMake(10, 10, CGRectGetWidth(self.view.frame) - 20, 50)];
            tempVw.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5].CGColor;
            tempVw.layer.borderWidth = 1.0;
            tempVw.layer.cornerRadius  = 2.0;
            tempVw.clipsToBounds = YES;
            tempVw.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:tempVw];
            
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.textLabel.text = NSLocalizedString(@"select_state", nil);
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            cell.textLabel.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        }
        
        cell.textLabel.font = [AppFont regularFont:14];
        
        return cell;
    }
    
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGRect imgTypeFrame =  filterCell.imgService.frame;
    imgTypeFrame.origin.x = singleton.isArabicSelected ? fDeviceWidth - 40: 15;
    filterCell.imgService.frame = imgTypeFrame;
    
    
    CGRect labelType =  filterCell.lblType.frame;
    labelType.origin.x = singleton.isArabicSelected ? fDeviceWidth - 50 - labelType.size.width : 60;
    filterCell.lblType.frame = labelType;
    filterCell.lblType.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    CGRect btnCheck =  filterCell.btnCheckUncheck.frame;
    btnCheck.origin.x = singleton.isArabicSelected ? 15 : CGRectGetWidth(_tblFilter.frame) - 35;
    filterCell.btnCheckUncheck.frame = btnCheck;
    
}



-(void)btnCheckboxClickedForNotifications:(UIButton*)btnSender{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btnSender.tag-500 inSection:1];
    [self checkUncheckFilterOptions:indexPath];
}



-(void)btnCheckboxClickedForServices:(UIButton*)btnSender
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btnSender.tag-500 inSection:2];
    [self checkUncheckFilterOptions:indexPath];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0)
    {
        [self checkUncheckFilterOptions:indexPath];
    }
    else if (indexPath.section == 4){
        
        [self showStatePicker];
    }
    else if (indexPath.section == 2)
    {
        [self checkUncheckFilterOptions:indexPath];
    }
}


-(void)checkUncheckFilterOptions:(NSIndexPath*)indexPath{
    // FilterServicesBO *objFilter = nil;
    
    if (indexPath.section == 0)
    {
        
        objFilter = arrNotificationTypes[indexPath.row];
        
        _selectedNotificationType = objFilter.serviceName;
        
    }
    
    
    else if (indexPath.section == 2)
    {
        objFilter = _arrNotificationSetting[indexPath.row];
        objFilter.isServiceSelected = !objFilter.isServiceSelected;
        
    }
    
    [self.tblFilter reloadData];
}

-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (alertView.tag == 102)
    { // handle the altdev
    }
}



- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


-(void)GetNotification:(NSNotification*)Sender{
    
    NSDictionary *userDateInfo = Sender.userInfo;
    NSLog(@"userInfo===%@",userDateInfo);
}


- (IBAction)btnDoneClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}
-(CGFloat)heightForDynamicCell
{
    
    CGFloat xCord = 10;
    CGFloat heightPadding = 45;
    CGFloat padding = 10;
    
    if ([arrStateNames count] == 0) {
        return 0.0;
    }
    else
    {
        CGFloat height = 60.0;
        for (int i = 0; i<arrStateNames.count; i++)
        {
            NSString *stateName = arrStateNames[i];
            CGFloat screenWidth = _tblFilter.frame.size.width;
            
            CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 30.0)];
            frame.size.width = frame.size.width + 40;
            
            CGFloat rightMargin = self.view.frame.size.width - xCord;
            if (rightMargin < frame.size.width) {
                xCord = 10;
                height+=heightPadding;
                NSLog(@"New Item, Height Increased = %lf",height);
            }
            
            xCord+=frame.size.width + padding;
        }
        
        return height;
    }
    
}

-(void)designCellContentForStateNamesOnCelView:(UIView*)contentView{
    
    
    CGFloat xCord = 10;
    CGFloat yCord = 15;
    CGFloat padding = 10;
    CGFloat heightPadding = 45;
    for (int i = 0; i<arrStateNames.count; i++) {
        
        NSString *stateName = arrStateNames[i];
        
        CGFloat screenWidth = _tblFilter.frame.size.width;
        
        CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 30.0)];
        frame.size.width = frame.size.width +30;
        
        CGFloat rightMargin = self.view.frame.size.width - xCord;
        if (rightMargin < frame.size.width) {
            xCord = 10;
            yCord+=heightPadding;
        }
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrame:CGRectMake(xCord, yCord, frame.size.width, 30)];
        [btn setBtnTitle:stateName];
        [btn addTarget:self action:@selector(btnCapsuleStateClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 2300+i;
        [contentView addSubview:btn];
        
        if ([stateName isEqualToString:NSLocalizedString(@"all", nil)])
        {
            btn.btnCross.hidden = YES;
        }
        else
        {
            btn.btnCross.hidden = NO;
        }
        xCord+=frame.size.width + padding;
    }
}


-(void)btnCrossClicked:(UIButton*)btnCross{
    SDCapsuleButton *btn = (SDCapsuleButton*)[btnCross superview];
    if ([[btn btnTitle] isEqualToString:NSLocalizedString(@"all", nil)]) {
        return;
    }
    
    NSInteger row = btn.tag - 2300;
    
    if (row < arrStateNames.count) {
        [arrStateNames removeObjectAtIndex:row];
        [arrSelectedItemForPicker removeObjectAtIndex:row];
        [_tblFilter reloadData];
    }
    
}


-(void)btnCapsuleStateClicked:(SDCapsuleButton*)btnState{
    NSInteger row = btnState.tag - 2300;
    NSLog(@"State Clicked : %li",(long)row);
}

- (IBAction)btnAlbhabatic:(id)sender
{
    
}




- (IBAction)btnbackClicked:(id)sender
{
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btnApplyClicked:(id)sender
{
    NSMutableDictionary *dictFilterOptions = [NSMutableDictionary new];
    
    // First Fetch all categories
    NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.isServiceSelected == YES"];
    
    NSArray *arrServices = [_arrNotificationSetting filteredArrayUsingPredicate:predicateFilter];
    
    NSMutableArray *arrSelectedCategories = [NSMutableArray new];
    
    if ([arrServices count])
    {
        NSMutableArray *serviceIdarray=[[NSMutableArray alloc]init];
        for (int i = 0; i<arrServices.count; i++)
            
        {
            
            NotificationItemBO *objSetting = arrServices[i];
            [arrSelectedCategories addObject:objSetting.titleService];
            [serviceIdarray addObject:objSetting.idService];
        }
        
        [dictFilterOptions setObject:arrSelectedCategories forKey:@"category_type"];
        [dictFilterOptions setObject:serviceIdarray forKey:@"service_list"];
        
    }
    
    // Now fetch all states
    if ([arrStateNames count])
    {
        [dictFilterOptions setObject:arrStateNames forKey:@"state_name"];
    }
    
    /*  if (_selectedNotificationType)
     {
     [dictFilterOptions setObject:_selectedNotificationType forKey:@"notification_type"];
     }
     */
    if (![_selectedNotificationType isEqualToString:NSLocalizedString(@"all", nil)])
    {
        [dictFilterOptions setObject:_selectedNotificationType forKey:@"notification_type"];
    }
    
    //NSLog(@"lbl time %@",[fromDate getfromDateValue:fromDate.lblDate]);
    
    
    
    
    
    
    
    [dictFilterOptions setObject:fromdatestring  forKey:@"start_date"];
    [dictFilterOptions setObject:todatestring forKey:@"end_date"];
    
    [dictFilterOptions setObject:NSLocalizedString(@"alphabetic", nil) forKey:@"sort_by"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SearchFilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SearchFilterVC"];
    vc.dictFilterParams = dictFilterOptions;
    vc.isFromHomeFilter = NO;//notification from
    
    vc.delegate = self;
    vc.filterBOArray = [NSMutableArray new];
    vc.notificationBOArray = [NSMutableArray new];
    
    vc.filterBOArray = arrNotificationTypes;
    vc.notificationBOArray = _arrNotificationSetting;
    
    
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


- (IBAction)btnResetClicked:(id)sender
{
    selectedFirstFilter = NSLocalizedString(@"alphabetic", nil);
    _selectedNotificationType = NSLocalizedString(@"all", nil);
    [arrStateNames removeAllObjects];
    [_arrNotificationSetting setValue:@NO forKey:@"isServiceSelected"];
    
    [arrStateNames addObject:NSLocalizedString(@"all", nil)];
    
    
    [self.tblFilter reloadData];
}


//getFilteredNotifData   (at apply button click)

#pragma mark- State Picker

-(void)showStatePicker
{
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"profile_state", nil) cancelButtonTitle: nil  confirmButtonTitle:NSLocalizedString(@"apply_label", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowRadioButtons = YES;
    picker.isClearOptionRequired = NO;
    picker.allowMultipleSelection = YES;
    picker.arrPreviousItemSelected = arrSelectedItemForPicker;
    [picker show];
    
}

- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row{
    
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:arry_state[row]
                               attributes:@{
                                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                                            }];
    return att;
}


- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    return arry_state[row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return arry_state.count;
    
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows
{
    [arrStateNames removeAllObjects];
    
    [arrSelectedItemForPicker removeAllObjects];
    NSArray * newArray =
    [[NSOrderedSet orderedSetWithArray:rows] array];
    
    [arrSelectedItemForPicker addObjectsFromArray:rows];
    
    for (NSNumber *n in newArray)
    {
        NSInteger row = [n integerValue];
        NSLog(@"%@ is chosen!", arry_state[row]);
        [arrStateNames addObject:arry_state[row]];
    }
    if ([arrStateNames count] == [arry_state count] || [arrStateNames count] == 0) {
        [arrStateNames removeAllObjects];
        [arrStateNames addObject:NSLocalizedString(@"all", nil)];
    }
    
    [_tblFilter reloadData];
}


- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView
{
    NSLog(@"Canceled.");
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    [_tblFilter reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end

