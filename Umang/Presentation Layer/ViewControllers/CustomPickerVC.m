//
//  CustomPickerVC.m
//  Umang
//
//  Created by spice on 19/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//



#import "CustomPickerVC.h"
#import "CustomPickerCell.h"
#import "NSBundle+Language.h"
#import "AppDelegate.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "UIView+Toast.h"
#import "StateList.h"

@interface CustomPickerVC ()
{
    MBProgressHUD *hud;
    NSString *notificationSelected;
    NSString *serviceState;
    NSString *stateIDSerice;
}

@property(nonatomic,retain)NSMutableArray *table_data;
@property(nonatomic,strong)NSMutableArray *upcoming_table_data;
@property(nonatomic,retain)NSIndexPath *selectIndexPath;
@property(nonatomic,retain)NSString *tempLanguage;

@end

@implementation CustomPickerVC
@synthesize table_data,upcoming_table_data;
@synthesize get_arr_element,get_title_pass,get_TAG;

@synthesize selectIndexPath;

@synthesize delegate; // for call back delegate


-(void)viewWillDisappear:(BOOL)animated
{
    //[delegate backToParentView]; // call delegate so user can back from it
    [super viewWillDisappear:NO];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    singleton = [SharedManager sharedSingleton];
    self.tagBack=@"YES";
    
    self.tempLanguage=singleton.languageSelected;//save the temp value in it
    
    
    
    // self.view.backgroundColor=[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0];
    // Do any additional setup after loading the view.
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: CUSTOM_PICKER_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    vw_line.frame=CGRectMake(0, 74, fDeviceWidth, 0.5);
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    table_State.contentInset = UIEdgeInsetsMake(-20,0,0,0);
    
    
    if ([get_TAG isEqualToString:TAG_STATE] || [get_TAG isEqualToString:TAG_ALLSERVICE_STATE])
    {
        table_State.contentInset = UIEdgeInsetsMake(0,0,0,0);
    }
    
    if ([get_TAG isEqualToString:TAG_SERVICETYPE_SEARCH]) {
        btn_reset.hidden=true;
    }
    if ([get_TAG isEqualToString:TAG_CATEGORY_SEARCH]) {
        btn_reset.hidden=true;
    }
    if ([get_TAG isEqualToString:TAG_SERVICESTATE_SEARCH]) {
        btn_reset.hidden=true;
    }
    [self addNavigationView];
    
    // Do any additional setup after loading the view.
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btn_reset.hidden = true;
    lbl_header.hidden = true;
    
   
    // should be in english only in case of serviceDirectory
    
    //btnHelp.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];

    NSLog(@"value of TAG_STATE_SERVICEDIR=%@",TAG_STATE_SERVICEDIR);

    if ([get_TAG isEqualToString:TAG_STATE_SERVICEDIR])
    {
        [nvView.leftBackButton setTitle:@"Back" forState:UIControlStateNormal];
        
    }
    
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
       
        [weakSelf btn_settingClicked:btnBack];
    };
    
    lbl_header.text=get_title_pass;
    
    if ([get_TAG isEqualToString:TAG_LANG]) {
        
        
        lbl_header.text=NSLocalizedString(@"language", nil);
    }
    if ([get_TAG isEqualToString:TAG_FONTSIZE]) {
        
        
        lbl_header.text=NSLocalizedString(@"font_size", nil);
    }
    
    nvView.lblTitle.text = lbl_header.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tbFrame = table_State.frame;
        tbFrame.origin.y = kiPhoneXNaviHeight
        table_State.frame = tbFrame;
        [self.view layoutIfNeeded];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    
    [btn_reset setTitle:NSLocalizedString(@"reset", nil) forState:UIControlStateNormal];
    //[btn_setting setTitle:NSLocalizedString(@"settings", nil) forState:UIControlStateNormal];
    
    [btn_setting setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btn_setting.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btn_setting.frame.origin.x, btn_setting.frame.origin.y, btn_setting.frame.size.width, btn_setting.frame.size.height);
        
        [btn_setting setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btn_setting.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    table_data=[[NSMutableArray alloc]init];
    
    table_data=[get_arr_element mutableCopy];
    
    
    upcoming_table_data = [NSMutableArray new];
    upcoming_table_data = [self.arrUpcomingState mutableCopy];
    
    
    if ([get_TAG isEqualToString:TAG_ALLSERVICE_STATE])
    {
        [table_data insertObject:NSLocalizedString(@"all", nil) atIndex:0];
        [table_State reloadData];
    }
    
    //[self setHeightOfTableView];
    
    // [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:0.1];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    
    
    if ([get_TAG isEqualToString:TAG_ALLSERVICE_STATE])
    {
        serviceState = self.allServiceState;
    }
    
    // should be in english only in case of serviceDirectory
   
   
    [super viewWillAppear:NO];
}
#pragma mark- Font Set to View

-(void)setViewFont
{
    [btn_setting.titleLabel setFont:[AppFont regularFont:17.0]];
    lbl_header.font = [AppFont semiBoldFont:17.0];
}

-(void)setHeightOfTableView
{
    table_State.contentInset = UIEdgeInsetsMake(-20,0,0,0);
    
    
    /**** set frame size of tableview according to number of cells ****/
    float height=45.0f*[table_data count]+42;
    
    if (height>400) {
        
    }
    else
    {
        [table_State setFrame:CGRectMake(table_State.frame.origin.x, table_State.frame.origin.y, table_State.frame.size.width,height-3)];
        
    }
    table_State.scrollEnabled = YES;
    
    //table_helpView.layer.backgroundColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
    table_State.layer.borderWidth=1;
    table_State.layer.borderColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
    
    //[table_State reloadData];
    
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([get_TAG isEqualToString:TAG_STATE] || [get_TAG isEqualToString:TAG_ALLSERVICE_STATE]|| [get_TAG isEqualToString:TAG_STATE_SERVICEDIR])
    {
        return 2;
    }
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    return  40;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //check header height is valid
    
    if ([get_TAG isEqualToString:TAG_STATE] || [get_TAG isEqualToString:TAG_ALLSERVICE_STATE])
    {
        
        if ([table_data count] > 0 && section == 0)
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
            headerView.backgroundColor=[UIColor clearColor];
            
            UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 12, fDeviceWidth, 25)];
            
            headerLabel.text = NSLocalizedString(@"states_available_services", nil);
            
            headerLabel.font = [AppFont regularFont:14.0];
            headerLabel.textColor = [UIColor darkGrayColor];
            
            [headerView addSubview:headerLabel];
            
            return headerView;
        }
        else if ([upcoming_table_data count] > 0 && section == 1)
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
            headerView.backgroundColor=[UIColor clearColor];
            
            UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 12, fDeviceWidth, 25)];
            headerLabel.text = NSLocalizedString(@"states_upcoming_services", nil);
            
            headerLabel.font = [AppFont regularFont:14.0];
            headerLabel.textColor = [UIColor darkGrayColor];
            
            [headerView addSubview:headerLabel];
            
            return headerView;
        }
        else
        {
            
            return nil;
        }
        
        
        
    }
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
    headerView.backgroundColor=[UIColor clearColor];
    
    
    return headerView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([get_TAG isEqualToString:TAG_STATE] || [get_TAG isEqualToString:TAG_ALLSERVICE_STATE] ||[get_TAG isEqualToString:TAG_STATE_SERVICEDIR])
    {
        if (section == 0)
        {
            return [table_data  count];
        }
        else if (section == 1)
        {
            return [upcoming_table_data  count];
        }
    }
    
    return [table_data  count];
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
   
    
    
    if([get_TAG isEqualToString:TAG_LANG])
    {
        NSString *titlestr=NSLocalizedString(@"change_language_dialog_msg", nil);
        NSString *msg=NSLocalizedString(@"change_language_dialog_msg2", nil);
        
        
        UIAlertController *okAlertCntrlr = [UIAlertController alertControllerWithTitle:titlestr message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAlert = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
        {
            //[self.textFieldOldPassword becomeFirstResponder];
            
            if (![self connected])
            {
                // Not connected
                NSLog(@"Device is not connected to the Internet");
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"network_error_txt",nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok",nil), nil];
                [alert show];
                
                self.tagBack=@"YES";
                
            } else
            {
                // Connected. Do some Internet stuff
                self.tagBack=@"NO";
                
                if (self.arrLanguageLocale)
                    
                {
                    
                    
                    self.selectedLocale = [_arrLanguageLocale objectAtIndex:indexPath.row];
                    NSLog(@"selceted %@",self.selectedLocale);
                }
                
                selectIndexPath=indexPath;
                
                [self stateSelectAction:selectIndexPath];
                                
                [tableView cellForRowAtIndexPath:selectIndexPath].accessoryType = UITableViewCellAccessoryCheckmark;
                
                [table_State reloadData];
                
                [self btn_settingClicked:self];
                
                
                
                
            }
            
            
            
            
            
            
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        
        [okAlertCntrlr addAction:okAlert];
        [okAlertCntrlr addAction:cancelAction];
        [self presentViewController:okAlertCntrlr animated:YES completion:nil];
        
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:titlestr
//                                                     message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"cancel", nil),NSLocalizedString(@"ok", nil), nil];
//        alert.tag = 1010;
//        [alert show];
        
        
        
    }
    else if ([get_TAG isEqualToString:TAG_NOTIFICATION])
    {
        notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexPath.row]];
        if ([notificationSelected isEqualToString:singleton.notiTypeSelected]) {
            return;
        }
        
        
        if ([notificationSelected isEqualToString:NSLocalizedString(@"promotional_small", nil)]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"turn_off_notif_dialog_title_txt", nil)
                                                            message:NSLocalizedString(@"turn_off_notif_dialog_msg_txt", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                                  otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
            alert.tag=223;
            [alert show];
            return;
        }
        
        else{
            
            singleton.notiTypeSelected = notificationSelected;
            [[NSUserDefaults standardUserDefaults] setObject:singleton.notiTypeSelected forKey:@"SELECTED_NOTITYPE"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self stateSelectAction:indexPath];
            
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
            [table_State reloadData];
            
            [self btn_settingClicked:self];
            
        }
    }
    if([get_TAG isEqualToString:TAG_STATE_SERVICEDIR])
    {
        //====== check for active internet connection to change tabs
        if (![self connected])
        {
            // Not connected
            [self showToast:NSLocalizedString(@"please_check_network_and_try_again", nil)];
            NSLog(@"Device is not connected to the Internet");
        } else
        {
            // Connected. Do some Internet stuff
            NSLog(@"Device is connected to the Internet");
            [self stateSelectAction:indexPath];
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
            [table_State reloadData];
            
        }
        
        
    }
    if([get_TAG isEqualToString:TAG_STATE])
    {
        //====== check for active internet connection to change tabs
        if (![self connected])
        {
            // Not connected
            [self showToast:NSLocalizedString(@"please_check_network_and_try_again", nil)];
            NSLog(@"Device is not connected to the Internet");
        } else
        {
            // Connected. Do some Internet stuff
            NSLog(@"Device is connected to the Internet");
            [self stateSelectAction:indexPath];
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
            [table_State reloadData];
            
        }
      
        
    }
    
    else
    {
        
        if (![get_TAG isEqualToString:TAG_LANG] && ![get_TAG isEqualToString:TAG_NOTIFICATION])
        {
            [self stateSelectAction:indexPath];
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
            [table_State reloadData];
            
            [self btn_settingClicked:self];
        }
        
        
    }
        
    
}


-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1010)
    { // handle the altdev
        if (buttonIndex==1)
        {
            
            if (![self connected]) {
                // Not connected
                NSLog(@"Device is not connected to the Internet");
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"network_error_txt",nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok",nil), nil];
                [alert show];
                
                self.tagBack=@"YES";
                
            } else {
                // Connected. Do some Internet stuff
                self.tagBack=@"NO";
                
                [self stateSelectAction:selectIndexPath];
                /* [UITableView cellForRowAtIndexPath:selectIndexPath].accessoryType = UITableViewCellAccessoryCheckmark;*/
                [table_State reloadData];
                
                [self btn_settingClicked:self];
            }
            
            /* NSURL *scriptUrl = [NSURL URLWithString:@"http://www.google.com/"];
             NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
             if (data)
             {
             self.tagBack=@"NO";
             
             [self stateSelectAction:selectIndexPath];
             
             [table_State reloadData];
             
             [self btn_settingClicked:self];
             
             
             }
             else
             {
             NSLog(@"Device is not connected to the Internet");
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please check your network/data connectivity and try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
             [alert show];
             
             self.tagBack=@"YES";
             
             }
             */
            
        }
    }
    else if (alertView.tag == 223){
        if(alertView.cancelButtonIndex != buttonIndex){
            singleton.notiTypeSelected = notificationSelected;
            [[NSUserDefaults standardUserDefaults] setObject:singleton.notiTypeSelected forKey:@"SELECTED_NOTITYPE"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [table_State reloadData];
            
            [self btn_settingClicked:self];
        }
    }
    if (alertView.tag == 989)
    { // handle the altdev
        if (buttonIndex==0)
        {
            singleton.languageSelected =self.tempLanguage;//save the temp value in it
            // Update Langugae Bundle
            [[NSUserDefaults standardUserDefaults] setObject:self.tempLanguage forKey:KEY_PREFERED_LOCALE];
            [NSBundle setLanguage:self.tempLanguage];
            
            
            
            if ([self.tempLanguage isEqualToString:@"ur-IN"])
            {
                singleton.isArabicSelected = YES;
            }
            else{
                singleton.isArabicSelected = NO;
            }
            [table_State reloadData];
            
            self.tagBack=@"YES";
        }
    }
    
    
}



-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomPickerCell";
    
    CustomPickerCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CustomPickerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.lbl_title.font = [AppFont regularFont:17.0];
    cell.lbl_title.textColor = [UIColor blackColor];
    
    NSString* txt;
    
    if ([get_TAG isEqualToString:TAG_STATE] || [get_TAG isEqualToString:TAG_ALLSERVICE_STATE]||[get_TAG isEqualToString:TAG_STATE_SERVICEDIR])
    {
        if (indexPath.section == 0)
        {
            txt  =[table_data objectAtIndex:indexPath.row];
            cell.lbl_title.textColor = [UIColor blackColor];
            
        }
        else if (indexPath.section == 1)
        {
            txt  =[upcoming_table_data objectAtIndex:indexPath.row];
            cell.lbl_title.textColor = [UIColor darkGrayColor];
        }
    }
    else
    {
        txt  =[table_data objectAtIndex:indexPath.row];
    }
    
    txt= [txt stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[txt substringToIndex:1] uppercaseString]];
    cell.lbl_title.text=txt;
    
    
    if ([get_TAG isEqualToString:TAG_TAB])
    {
        
        if ([cell.lbl_title.text isEqualToString:singleton.tabSelected])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
        
        
    }
    if ([get_TAG isEqualToString:TAG_LANG]) {
        if ([cell.lbl_title.text isEqualToString:singleton.languageSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
        
    }
    if ([get_TAG isEqualToString:TAG_STATE_SERVICEDIR]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.serviceDirstateSelected])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.lbl_title.font = [AppFont semiBoldFont:17.0];
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    if ([get_TAG isEqualToString:TAG_STATE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.stateSelected])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.lbl_title.font = [AppFont semiBoldFont:17.0];
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    if ([get_TAG isEqualToString:TAG_ALLSERVICE_STATE])
    {
        
        if ([cell.lbl_title.text isEqualToString:serviceState])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.lbl_title.font = [AppFont semiBoldFont:17.0];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    if ([get_TAG isEqualToString:TAG_NOTIFICATION]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypeSelected])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    if ([get_TAG isEqualToString:TAG_FONTSIZE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.fontSizeSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    if ([get_TAG isEqualToString:TAG_STATE_PROFILE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.profilestateSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    if ([get_TAG isEqualToString:TAG_GENDER]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypeGenderSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    /*if ([get_TAG isEqualToString:TAG_MARITALSTATUS]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.profileMaritalStatus]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }*/
    
    
    if ([get_TAG isEqualToString:TAG_CITY]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypeCitySelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    if ([get_TAG isEqualToString:TAG_DISTRICT]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypDistricteSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    if ([get_TAG isEqualToString:TAG_SERVICETYPE_SEARCH]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.search_servicetype]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    if ([get_TAG isEqualToString:TAG_CATEGORY_SEARCH]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.search_category]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    if ([get_TAG isEqualToString:TAG_SERVICESTATE_SEARCH]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.search_state])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    return cell;
}
- (void) startFontNotifier
{
    
    // All instances of TestClass will be notified
    
    
    //    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:singleton.fontSizeSelected forKey:@"FONT_SELECT_KEY"];
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:singleton.fontSizeSelectedIndex] forKey:@"FONT_SELECT_KEY_INDEX"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"FONTCHANGENOTIFICATION" object:nil userInfo:userInfo];
    
}

- (void)stateSelectAction:(NSIndexPath *)index
{
    
    // Add image to button for normal state
    
    int indexrow=(int)index.row;
    
    //NSLog(@"Selected State=%@",[table_data objectAtIndex:indexrow]);
    
    
    if ([get_TAG isEqualToString:TAG_TAB])
    {
        
        singleton.tabSelectedIndex = indexrow;
        singleton.tabSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        [[NSUserDefaults standardUserDefaults] setInteger:singleton.tabSelectedIndex forKey:@"SELECTED_TAB_INDEX"];
        
    }
    if ([get_TAG isEqualToString:TAG_LANG])
    {
        singleton.languageSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        StateList *obj = [[StateList alloc] init];
        singleton.user_StateId = [obj getStateCode:singleton.stateSelected];
        [singleton setStateId:singleton.user_StateId];
    }
    if ([get_TAG isEqualToString:TAG_STATE_SERVICEDIR])
    {
        
        if (index.section == 0)
        {
            singleton.serviceDirstateSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        }
        else if (index.section == 1)
        {
            singleton.serviceDirstateSelected=[NSString stringWithFormat:@"%@",[upcoming_table_data objectAtIndex:indexrow]];
        }
        
        
        //StateList *obj = [[StateList alloc] init];
       // singleton.user_SeriveDirStateId = [obj getStateCode:singleton.serviceDirstateSelected];
         singleton.user_SeriveDirStateId  = [singleton.dbManager getStateCodeEnglish:singleton.serviceDirstateSelected];

        NSLog(@"user_SeriveDirStateId=%@",singleton.user_SeriveDirStateId);
        
        [singleton ServiceDirSetStateId:singleton.user_SeriveDirStateId];
        
        //[self hitSetStateAPI:singleton.user_SeriveDirStateId];
    }
    
    if ([get_TAG isEqualToString:TAG_STATE])
    {
        
        if (index.section == 0)
        {
            singleton.stateSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        }
        else if (index.section == 1)
        {
            singleton.stateSelected=[NSString stringWithFormat:@"%@",[upcoming_table_data objectAtIndex:indexrow]];
        }
        
        
        StateList *obj = [[StateList alloc] init];
        [obj hitStateQualifiAPI];
        singleton.user_StateId = [obj getStateCode:singleton.stateSelected];
        
        NSLog(@"singleton.user_StateId=%@",singleton.user_StateId);
        
        [singleton setStateId:singleton.user_StateId];
        
        [self hitSetStateAPI:singleton.user_StateId];
    }
    if ([get_TAG isEqualToString:TAG_ALLSERVICE_STATE])
    {
        
        if (index.section == 0)
        {
            serviceState=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        }
        else if (index.section == 1)
        {
            serviceState=[NSString stringWithFormat:@"%@",[upcoming_table_data objectAtIndex:indexrow]];
        }
        
        StateList *obj = [[StateList alloc] init];
        stateIDSerice = [obj getStateCode:serviceState];
        
    }
    if ([get_TAG isEqualToString:TAG_NOTIFICATION])
    {
        singleton.notiTypeSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        
        [[NSUserDefaults standardUserDefaults] setObject:singleton.notiTypeSelected forKey:@"SELECTED_NOTITYPE"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    if ([get_TAG isEqualToString:TAG_GENDER])
    {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.notiTypeGenderSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    
    /*if ([get_TAG isEqualToString:TAG_MARITALSTATUS])
    {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.profileMaritalStatus=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }*/
    
    if ([get_TAG isEqualToString:TAG_CITY]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.notiTypeCitySelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    if ([get_TAG isEqualToString:TAG_DISTRICT]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.notiTypDistricteSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    
    if ([get_TAG isEqualToString:TAG_STATE_PROFILE]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.profilestateSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        
        
        
    }
    
    if ([get_TAG isEqualToString:TAG_FONTSIZE])
    {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        
        
        singleton.fontSizeSelectedIndex = indexrow;
        singleton.fontSizeSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.isLargeFont = @"";
        if (indexrow == 2) {
            singleton.isLargeFont = @"yes";
        }
        [[NSUserDefaults standardUserDefaults] setInteger:singleton.fontSizeSelectedIndex forKey:@"SELECTED_FONTSIZE_INDEX"];
        
        [self startFontNotifier];
    }
    
    if ([get_TAG isEqualToString:TAG_SERVICETYPE_SEARCH]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.search_servicetype=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    if ([get_TAG isEqualToString:TAG_CATEGORY_SEARCH]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.search_category=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    if ([get_TAG isEqualToString:TAG_SERVICESTATE_SEARCH]) {
        // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
        singleton.search_state=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
    }
    
}

//----- hitAPI for IVR OTP call Type registration ------

-(void)hitInitAPIForStateChange
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    singleton = [SharedManager sharedSingleton];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
    {
        [hud hideAnimated:YES];
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            singleton.arr_initResponse=[[NSMutableDictionary alloc]init];
            singleton.arr_initResponse=[response valueForKey:@"pd"];
            NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
            
            
            [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
            
            
            
            NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
            NSLog(@"value of abbr=%@",abbr);
            
            if ([abbr length]==0) {
                
                abbr=@"";
                
            }
            singleton.user_StateId = [singleton.arr_initResponse valueForKey:@"ostate"];
            [singleton setStateId:singleton.user_StateId];
            NSString*  infoTab=[singleton.arr_initResponse valueForKey:@"infotab"];
[[NSUserDefaults standardUserDefaults] setObject:[infoTab capitalizedString] forKey:@"infotab"];
            NSString *emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
            emblemString = emblemString.length == 0 ? @"":emblemString;
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:[abbr capitalizedString] forKey:@"ABBR_KEY"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        else
        {
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"network_error_txt",nil)  delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok",nil) , nil];
            alert.tag=989;
            [alert show];
        }
        
    }];
    
}
-(void)hitSetStateAPI:(NSString*)stateId
{
    if ([stateId isEqualToString:@"9999"])
    {
        stateId=@"";

    }
    if ([stateId length]==0)
    {
        stateId=@"";
    }
    if ([singleton.mobileNumber length]==0)
    {
        singleton.mobileNumber=@"";
    }
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"status"];//This is Status of the account
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    // [dictBody setObject:stateId forKey:@"st"];  //get from mobile default email //not supported iphone
    
    [dictBody setObject:stateId forKey:@"stid"];  //get from mobile default email //not supported iphone
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_SETSTATE withBody:dictBody andTag:TAG_REQUEST_STATE completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
    {
        [hud hideAnimated:YES];
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                NSString*  abbr=[[response valueForKey:@"pd"] valueForKey:@"abbr"];
                NSString*  stateID=[[response valueForKey:@"pd"] valueForKey:@"ostate"];
                
                
                if ([get_TAG isEqualToString:TAG_STATE_SERVICEDIR])
                {
                    singleton.user_SeriveDirStateId = stateID;

                }
                else
                {
                singleton.user_StateId = stateID;
                }
                NSString*  stname=[[response valueForKey:@"pd"] valueForKey:@"stname"];
                
                NSLog(@"value of abbr=%@",abbr);
                NSLog(@"value of stid=%@",singleton.user_StateId);
                NSLog(@"value of stname=%@",stname);
                
                
                if ([abbr length]==0) {
                    abbr=@"";
                }
                
                NSString *emblemString = [[response valueForKey:@"pd"] valueForKey:@"stemblem"];
                emblemString = emblemString.length == 0 ? @"":emblemString;
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[abbr capitalizedString] forKey:@"ABBR_KEY"];
                [singleton setStateId:singleton.user_StateId];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                
                
                
                
                if (abbr.length != 0)
                {
                    
                    
                    
                    
                   // NSString * tordc = [NSString stringWithFormat:@"%@",[singleton.arr_initResponse  valueForKey:@"tord"]];
                  //  NSArray *array =[tordc componentsSeparatedByString:@"|,"];
                    NSArray *array =[NSArray arrayWithArray:singleton.AppTabOrders];

                    NSMutableArray *tempTabOrder =[NSMutableArray new];
                    NSString *home = [NSLocalizedString(@"home_small", @"") capitalizedString];
                    NSString *flagship = [NSLocalizedString(@"flagship", @"") capitalizedString];
                    NSString *fav = [NSLocalizedString(@"favourites_small", @"") capitalizedString];
                    NSString *allservices = [NSLocalizedString(@"all_services_small", @"") capitalizedString];
                    NSString *state = NSLocalizedString(@"state_txt", @"") ;
                    for (int i =0; i<[array count]; i++)
                    {
                        NSString *tempItem = [array objectAtIndex:i];
                        if ([tempItem containsString:@"home"])
                        {
                            NSLog(@"string contains home!");
                            [tempTabOrder addObject: home];
                        }
                        else if ([tempItem containsString:@"HomeWithFav"]) {
                            NSLog(@"string contains flagship!");
                            [tempTabOrder addObject: home];
                        }
                        else if ([tempItem containsString:@"flagship"]) {
                            NSLog(@"string contains flagship!");
                            [tempTabOrder addObject: flagship];
                        }
                        else if ([tempItem containsString:@"fav"]) {
                            NSLog(@"string contains fav!");
                            [tempTabOrder addObject: fav];
                        }
                        else if ([tempItem containsString:@"allservices"]) {
                            NSLog(@"string contains allservices!");
                            [tempTabOrder addObject: allservices];
                        }
                        else if ([tempItem containsString:@"state"]) {
                            NSLog(@"string contains state!");
                            [tempTabOrder addObject: state];
                        }
                    }
                    NSLog(@" tempTabOrder=%@",tempTabOrder);
                    if ([tempTabOrder count]>0) {
                        NSUInteger i = [tempTabOrder indexOfObject: state];
                        
                       
                        
                        //[[self.tabBarController.tabBar.items objectAtIndex:i] setTitle:abbreviation];
                        UITabBarItem *tabItem = [[[self.tabBarController tabBar] items] objectAtIndex:i];
                        UIFont *font =[UIFont systemFontOfSize: 10];
                        NSDictionary *attributes = @{NSFontAttributeName: font};
                        [tabItem setTitle:abbr];
                        [tabItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                }
                
                
            /*
                if ([abbr length]!=0) {
                    UITabBarItem *tabItem = [[[self.tabBarController tabBar] items] objectAtIndex:3];
                    
                    NSLog(@"tabitem title=%@ abbr=%@",tabItem.title,abbr);
                    
                    //[tabItem setTitle:abbr];
                    
                    UIFont *font =[UIFont systemFontOfSize: 10];
                    
                    NSDictionary *attributes = @{NSFontAttributeName: font};
                    [tabItem setTitle:abbr];
                    [tabItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
                }*/
                
                if ([get_TAG isEqualToString:TAG_STATE_SERVICEDIR])
                {
                   // singleton.user_SeriveDirStateId = stateID;
                    
                }
                else
                {
                    [self hitInitAPIForStateChange];
                }
                
                
                [self btn_settingClicked:self];
                
                /* {
                 gcmid = "";
                 node = "";
                 ntfp = "";
                 ntft = "";
                 pd =     {
                 abbr = "arunachal pradesh";
                 stid = 23;
                 stname = "arunachal pradesh";
                 };
                 plang = "";
                 rc = 00;
                 rd = "Successful.";
                 rs = S;
                 }
                 
                 */
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            NSString *toast=[NSString stringWithFormat:@"%@",error.localizedDescription];
            [self showToast:toast];
            
          /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            */
        }
        
    }];
    
}



- (IBAction)btn_resetClicked:(id)sender
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Reset all settings?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:no];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)hitInitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    singleton = [SharedManager sharedSingleton];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            singleton.arr_initResponse=[[NSMutableDictionary alloc]init];
            singleton.arr_initResponse=[response valueForKey:@"pd"];
            NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
            
            
            [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
            
            
            
            NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
            NSLog(@"value of abbr=%@",abbr);
            
            if ([abbr length]==0) {
                
                abbr=@"";
                
            }
            singleton.user_StateId = [singleton.arr_initResponse valueForKey:@"ostate"];
            [singleton setStateId:singleton.user_StateId];
            NSString*  infoTab=[singleton.arr_initResponse valueForKey:@"infotab"];
[[NSUserDefaults standardUserDefaults] setObject:[infoTab capitalizedString] forKey:@"infotab"];
            NSString *emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
            emblemString = emblemString.length == 0 ? @"":emblemString;
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:[abbr capitalizedString] forKey:@"ABBR_KEY"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //------------------------- Encrypt Value------------------------
            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
            // Encrypt
            [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
            NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSHTTPCookie *cookie;
            for (cookie in [storage cookies]) {
                
                [storage deleteCookie:cookie];
                
            }
            NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
            [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            //------------------------- Encrypt Value------------------------
            
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                // get the data here
                [singleton.dbManager deleteBannerHomeData];
                [singleton.dbManager  deleteAllServices];
                [singleton.dbManager  deleteSectionData];
                
                
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   //-------------check condition temp-----
                                   //                                   [[NSUserDefaults standardUserDefaults] setObject:singleton.tabSelected  forKey:@"SELECTED_TAB"];
                                   
                                   [[NSUserDefaults standardUserDefaults]synchronize];
                                   
                                   singleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
                                   
                                   
                                   
                                   //                                   NSLog(@"selected tab is: %@",selectedTab);
                                   //                                   NSLog(@"selected tab home_small: %@",NSLocalizedString(@"home_small", nil));
                                   //                                   NSLog(@"selected tab favourites_small: %@",NSLocalizedString(@"favourites_small", nil));
                                   //                                   NSLog(@"selected tab all_services_small: %@",NSLocalizedString(@"all_services_small", nil));
                                   //                                   if ([selectedTab isEqualToString:NSLocalizedString(@"home_small", nil)]) {
                                   //                                       singleton.tabSelected=NSLocalizedString(@"home_small", nil);
                                   //
                                   //                                   }
                                   //                                   else if ([selectedTab isEqualToString:NSLocalizedString(@"favourites_small", nil)]) {
                                   //                                       singleton.tabSelected=NSLocalizedString(@"favourites_small", nil);
                                   //
                                   //                                   }
                                   //                                   else if ([selectedTab isEqualToString:NSLocalizedString(@"all_services_small", nil)]) {
                                   //                                       singleton.tabSelected=NSLocalizedString(@"all_services_small", nil);
                                   //
                                   //
                                   //                                   }
                                   //                                   else{
                                   //                                       singleton.tabSelected=NSLocalizedString(@"home_small", nil);
                                   //
                                   //
                                   //                                   }
                                   //
                                   
                                   
                                   
                                   
                                   [self performSelector:@selector(homeviewJump) withObject:nil afterDelay:0.2];
                                   
                               });
            });
            
            
            
            
            // jump to home view  tab
            
            /* facebooklink
             faq
             forceupdate
             googlepluslink
             opensource
             privacypolicy
             splashScreen
             tabordering
             termsandcondition
             twitterlink
             ver
             vermsg
             */
            
            //------ save value in nsuserdefault for relanch app
            // [[NSUserDefaults standardUserDefaults] setObject:response forKey:@"TOUR_Key"];
            //[[NSUserDefaults standardUserDefaults] synchronize];
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"network_error_txt",nil)  delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok",nil) , nil];
            alert.tag=989;
            [alert show];
            
            //NSString *toast=[NSString stringWithFormat:@"%@",error.localizedDescription];
            
            //[self showToast:toast];
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
        }
        
    }];
    
}

-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionCenter];
}

-(void)homeviewJump
{
    
    AppDelegate *appD = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [appD updateAppLanguage:@"NO"];
    
    
}

-(void)afterlanguageChangeSettings
{
    //Note if internet is not available then user cannot able to change language
    //steps to follow
    //hitInitAPI
    // delete service data
    //delete service section
    //jump to home view
    ///----- check for internet connection ------------------
    /*  NSURL *scriptUrl = [NSURL URLWithString:@"http://www.google.com/"];
     NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
     if (data)
     {
     NSLog(@"Device is connected to the Internet");
     [self hitInitAPI];
     
     }
     else
     {
     NSLog(@"Device is not connected to the Internet");
     }
     */
    if (![self connected]) {
        
        //add here alert
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"network_error_txt",nil)  delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok",nil) , nil];
        alert.tag=989;
        [alert show];
        
        
        
        // Not connected
        
    } else {
        // Connected. Do some Internet stuff
        NSLog(@"Device is connected to the Internet");
        [self hitInitAPI];
    }
    
    
    
}





- (IBAction)btn_settingClicked:(id)sender
{
    //
    if ([get_TAG isEqualToString:TAG_ALLSERVICE_STATE])
    {
        if (stateIDSerice != nil && stateIDSerice.length != 0)
        {
            self.finishState(stateIDSerice);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    if ([get_TAG isEqualToString:TAG_SERVICETYPE_SEARCH]) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    if ([get_TAG isEqualToString:TAG_CATEGORY_SEARCH]) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    if ([get_TAG isEqualToString:TAG_SERVICESTATE_SEARCH]) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    if ([get_TAG isEqualToString:TAG_LANG])
    {
        if ([self.tagBack isEqualToString:@"YES"]) {
            // [self dismissViewControllerAnimated:NO completion:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else
        {
            // Update Langugae Bundle
            [[NSUserDefaults standardUserDefaults] setObject:self.selectedLocale forKey:KEY_PREFERED_LOCALE];
            [NSBundle setLanguage:self.selectedLocale];
            if ([self.selectedLocale isEqualToString:@"ur-IN"]) {
                singleton.isArabicSelected = YES;
            }
            else{
                singleton.isArabicSelected = NO;
            }
            
            
            [self afterlanguageChangeSettings];
            
            
        }
    }
    
    
    else
        [self.navigationController popViewControllerAnimated:YES];
    
    
    
}



#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

@end
