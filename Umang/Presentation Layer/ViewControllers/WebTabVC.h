//
//  WebTabVC.h
//  DynamicTabs
//
//  Created by admin on 06/07/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebTabVC : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *vw_web;
}
@property(nonatomic,retain)NSString* webUrlAction;
@end
