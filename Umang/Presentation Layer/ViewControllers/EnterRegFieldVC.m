//
//  EnterRegFieldVC.m
//  Umang
//
//  Created by deepak singh rawat on 01/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "EnterRegFieldVC.h"
#import "CustomPickerCell.h"

@interface EnterRegFieldVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    NSArray *searchResults;
    
    __weak IBOutlet UISearchBar *searchBar;
    
    NSString *isvalueselected;
}
@property(nonatomic,retain)NSMutableArray *table_data;

@end

@implementation EnterRegFieldVC
@synthesize table_data;
@synthesize get_arr_element,get_title_pass,get_TAG;


- (void)viewDidLoad
{
    isvalueselected=@"No";
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: ENTER_REGISTRATION_FIELD_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    [btn_back setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    searchBar.placeholder = NSLocalizedString(@"search", @"");
   // searchBar
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btn_back.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btn_back.frame.origin.x, btn_back.frame.origin.y, btn_back.frame.size.width, btn_back.frame.size.height);
        
        [btn_back setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btn_back.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    singleton = [SharedManager sharedSingleton];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    vw_line.frame=CGRectMake(0, 74, fDeviceWidth, 0.5);
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    //table_State.contentInset = UIEdgeInsetsMake(-20,0,0,0);
    
    [super viewDidLoad];
    
    [self.view bringSubviewToFront:searchBar];
//    searchBar.showsCancelButton = YES;
//    UIView* view=searchBar.subviews[0];
//    for (UIView *subView in view.subviews) {
//        if ([subView isKindOfClass:[UIButton class]]) {
//            UIButton *cancelButton = (UIButton*)subView;
//            [cancelButton setTitle: forState:UIControlStateNormal];
//        }
//    }
    
    // Do any additional setup after loading the view.
    [self addNavigationView];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    UIButton *cancelButton;
    UIView *topView = searchBar.subviews[0];
    for (UIView *subView in topView.subviews) {
        if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
            cancelButton = (UIButton*)subView;
        }
    }
    if (cancelButton) {
        [cancelButton setTitle:NSLocalizedString(@"cancel", @"") forState:UIControlStateNormal];
    }
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(btn_backClicked:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.navigationController.view addGestureRecognizer:gestureRecognizer];
    // __weak id weakSelf = self;
    //self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    lbl_header.text=get_title_pass;
    table_data=[[NSMutableArray alloc]init];
    table_data=[get_arr_element mutableCopy];
    //[self setHeightOfTableView];
    CGFloat yValue = kNaviHeight;
    if (iPhoneX()) {
        yValue = kiPhoneXNaviHeight;
    }
    if ([get_TAG isEqualToString:TAG_GENDER])
    {
        searchBar.hidden=TRUE;
        [table_State setFrame:CGRectMake(0,  yValue, fDeviceWidth, fDeviceHeight - yValue)];
        [self.view layoutIfNeeded];
    }
    else
    {
        searchBar.hidden=FALSE;
        [table_State setFrame:CGRectMake(0,  yValue, fDeviceWidth, fDeviceHeight - yValue)];
        CGRect searchFrame = searchBar.frame;
        searchFrame.origin.y = kiPhoneXNaviHeight;
        searchBar.frame = searchFrame;
        CGRect tblFrame = table_State.frame;
        tblFrame.origin.y = 56.0 + kiPhoneXNaviHeight;
        tblFrame.size.height = fDeviceHeight - 56.0 + kiPhoneXNaviHeight;
        table_State.frame = tblFrame;
        [self.view layoutIfNeeded];
    }
   
    
    // [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:0.1];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
    
    [self setViewFont];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btn_back.hidden = true;
    lbl_header.hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btn_backClicked:btnBack];
    };
    nvView.lblTitle.text = get_title_pass;
    //[nvView.rightBarButton setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    
    
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btn_back.titleLabel setFont:[AppFont regularFont:17.0]];
    lbl_header.font = [AppFont semiBoldFont:17.0];
}

-(void)setHeightOfTableView
{
    // table_State.contentInset = UIEdgeInsetsMake(-20,0,0,0);
    
    
    /**** set frame size of tableview according to number of cells ****/
    float height=45.0f*[table_data count]+42;
    
    if (height>400) {
        
    }
    else
    {
        [table_State setFrame:CGRectMake(table_State.frame.origin.x, table_State.frame.origin.y, table_State.frame.size.width,height-3)];
        
    }
    table_State.scrollEnabled = YES;
    
    //table_helpView.layer.backgroundColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
    table_State.layer.borderWidth=1;
    table_State.layer.borderColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
    
    //[table_State reloadData];
    
}


//- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0.001f;
//}



//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    if (scrollView.contentOffset.y<=0) {
//        scrollView.contentOffset = CGPointZero;
//    }
//}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Here you must return the number of sectiosn you want
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  0.001;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    //check header height is valid
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,0)];
    headerView.backgroundColor=[UIColor clearColor];
    
    
    return headerView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if (searchResults.count == 0) {
            for (UIView *view in tableView.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    ((UILabel *)view).text = NSLocalizedString(@"no_result_found", @"");
                }
            }
        }
        return [searchResults count];
        
    } else {
        return [table_data count];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       isvalueselected=@"Yes";
                       [self valueSelectedAction:indexPath];
                       
                       dispatch_async(dispatch_get_main_queue(), ^(void){
                           
                           [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
                           [table_State reloadData];
                       });

                   });
    
    [self btn_backClicked:self];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}



-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    return YES;
    
}






- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self beginswith[c] %@", searchText];
    NSArray *results = [table_data filteredArrayUsingPredicate:resultPredicate];
    
    
    
    
    searchResults = [results sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    [table_State reloadData];
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomPickerCell";
    
    //CustomPickerCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    CustomPickerCell *cell =(CustomPickerCell *) [table_State dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    if (cell == nil) {
        cell = [[CustomPickerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        
        if([searchResults count]>0)
        {
            cell.lbl_title.text=[searchResults objectAtIndex:indexPath.row];
            
        }
        
    }
    else
    {
        cell.lbl_title.text=[table_data objectAtIndex:indexPath.row];
        
    }
    
    if ([get_TAG isEqualToString:TAG_STATE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.stateSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[AppFont regularFont:17]];

        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    
    if ([get_TAG isEqualToString:TAG_STATE_PROFILE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.profilestateSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[AppFont regularFont:17]];

        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    if ([get_TAG isEqualToString:TAG_GENDER]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypeGenderSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[AppFont regularFont:17]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    if ([get_TAG isEqualToString:TAG_CITY]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypeCitySelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[AppFont regularFont:17]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    
    
    if ([get_TAG isEqualToString:TAG_DISTRICT]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.notiTypDistricteSelected]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[AppFont regularFont:17]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    
    if ([get_TAG isEqualToString:TAG_OCCUPATION_PROFILE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.user_Occupation]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[AppFont regularFont:17]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    if ([get_TAG isEqualToString:TAG_QUALIFI_PROFILE]) {
        
        if ([cell.lbl_title.text isEqualToString:singleton.user_Qualification]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [cell.lbl_title setFont:[AppFont regularFont:17]];
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    [cell.lbl_title setFont:[AppFont regularFont:17]];

    // cell.accessoryView.backgroundColor = [UIColor clearColor]; // Not working
    
    
    
    return cell;
}


- (void)valueSelectedAction:(NSIndexPath *)index
{
    
    // Add image to button for normal state
    
    int indexrow=(int)index.row;
    
    // NSLog(@"Selected State=%@",[table_data objectAtIndex:indexrow]);
    
    if (self.searchDisplayController.active)
    {
        
        
        if ([get_TAG isEqualToString:TAG_STATE]) {
            singleton.stateSelected=[[NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexrow]] mutableCopy];
            
        }
        
        if ([get_TAG isEqualToString:TAG_GENDER]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.notiTypeGenderSelected=[[NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexrow]]mutableCopy];
        }
        
        
        if ([get_TAG isEqualToString:TAG_DISTRICT]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.notiTypDistricteSelected=[[NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexrow]]mutableCopy];
        }
        
        if ([get_TAG isEqualToString:TAG_STATE_PROFILE]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.profilestateSelected=[[NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexrow]]mutableCopy];
            singleton.notiTypDistricteSelected=@"";

        }
        
        if ([get_TAG isEqualToString:TAG_CITY]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.notiTypeCitySelected=[[NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexrow]]mutableCopy];
        }
        
        
        if ([get_TAG isEqualToString:TAG_OCCUPATION_PROFILE]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.user_Occupation=[[NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexrow]]mutableCopy];
            
        }
        
        
        
        if ([get_TAG isEqualToString:TAG_QUALIFI_PROFILE]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.user_Qualification=[[NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexrow]]mutableCopy];
            
            
        }
        
        
    }
    
    
    
    else
    {
        
        
        
        
        if ([get_TAG isEqualToString:TAG_STATE]) {
            singleton.stateSelected=[[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]]mutableCopy];

        }
        
        if ([get_TAG isEqualToString:TAG_GENDER]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.notiTypeGenderSelected=[[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]]mutableCopy];
        }
        
        
        if ([get_TAG isEqualToString:TAG_DISTRICT]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.notiTypDistricteSelected=[[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]]mutableCopy];
        }
        
        if ([get_TAG isEqualToString:TAG_STATE_PROFILE]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.profilestateSelected=[[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]]mutableCopy];
            singleton.notiTypDistricteSelected=@"";

        }
        
        if ([get_TAG isEqualToString:TAG_CITY]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.notiTypeCitySelected=[[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]]mutableCopy];
        }
        
        
        if ([get_TAG isEqualToString:TAG_OCCUPATION_PROFILE]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.user_Occupation=[[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]]mutableCopy];
            
        }
        
        
        
        if ([get_TAG isEqualToString:TAG_QUALIFI_PROFILE]) {
            // singleton.notificationSelected=[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]];
            singleton.user_Qualification=[[NSString stringWithFormat:@"%@",[table_data objectAtIndex:indexrow]]mutableCopy];
            
            
        }
        
        
        
    }
    
    
    
    
    
    
}



- (void) startNotifierUpdateProlfe
{
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STARTNOTIFIERUPD" object:isvalueselected];
    
    
    
}

- (void) startNotifierIsFromNotify
{
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STARTNOTIFIERREGIS" object:nil];
    
    
    
}
//- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
//    if ([searchResults count] == 0) {
//
//    }
//}

- (IBAction)btn_backClicked:(id)sender
{
    // [self
    //    CATransition *transition = [CATransition animation];
    //    transition.duration = 0.001;
    //    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionFade;
    //    transition.subtype = kCATransitionFromLeft;
    // [self.view.window.layer addAnimation:transition forKey:nil];
    //  [self dismissViewControllerAnimated:NO completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
    // [self dismissViewControllerAnimated:YES completion:nil];
    
    
    if ([self.TAG_FROM isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        
        [self performSelector:@selector(startNotifierUpdateProlfe) withObject:nil afterDelay:1];
        // [self startNotifierUpdateProlfe];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if ([self.TAG_FROM isEqualToString:@"ISFROMREGISTRATION"]) {
        
        [self startNotifierIsFromNotify];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/


@end
