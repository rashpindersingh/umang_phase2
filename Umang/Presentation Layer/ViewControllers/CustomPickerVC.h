//
//  CustomPickerVC.h
//  Umang
//
//  Created by spice on 19/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

//---------- Add a custom delegate to pass data or reload the parent view -----
@protocol sendParentdataProtocol <NSObject>

//-(void)backToParentView:(NSArray *)array; //if need to pass data
-(void)backToParentView;

@end
//---------- End Add a custom delegate to pass data or reload the parent view -----


@interface CustomPickerVC : UIViewController
{
    IBOutlet UITableView *table_State;
    IBOutlet UIButton *btn_setting;
    IBOutlet UIButton *btn_reset;
    
    IBOutlet UILabel *lbl_header;
    SharedManager *singleton;
    IBOutlet UIView *vw_line;
    
}
@property(weak,nonatomic)NSString *get_TAG;
@property(weak,nonatomic)NSString *get_title_pass;
@property(retain,nonatomic)NSMutableArray *get_arr_element;
@property(nonatomic,assign)id delegate;
@property(retain,nonatomic)NSArray *arrLanguageLocale;
@property(retain,nonatomic)NSString *selectedLocale;
@property (nonatomic,strong)NSMutableArray *arrUpcomingState;

@property (nonatomic,copy)NSString *allServiceState;
@property(copy) void(^finishState)(NSString *stateID);


@property(retain,nonatomic)NSString *tagBack;

- (IBAction)btn_resetClicked:(id)sender;

- (IBAction)btn_settingClicked:(id)sender;
@end
