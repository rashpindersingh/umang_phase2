//
//  ShowMoreServiceVC.m
//  Umang
//
//  Created by spice_digital on 14/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ShowMoreServiceVC.h"
#import "HCSStarRatingView.h"
#import "itemMoreInfoVC.h"

#import "AllServiceCVCell.h"
#import "CustomImageFlowLayout.h"
#import "addFilterShowMoreVC.h"

#import "ScrollNotificationVC.h"
#import "HomeDetailVC.h"
#import "UIImageView+WebCache.h"
#import "DetailServiceNewVC.h"

#import "AllServiceCVCellForiPad.h"

@interface ShowMoreServiceVC ()<UIActionSheetDelegate>
{
    SharedManager *singleton;
    itemMoreInfoVC *itemMoreVC;
    
}
@property (nonatomic, strong) HCSStarRatingView *starRatingView;
@property(nonatomic,retain)NSArray *sampleData ;
@property(nonatomic,retain)NSMutableArray *tableData ;
@property (nonatomic, strong)   NSMutableArray *selectedMarks; // You need probably to save the selected cells for use in the future.
@property(nonatomic,retain)NSDictionary *cellData;
@property(nonatomic,retain)NSMutableDictionary * cellDataOfmore;

@end

@implementation ShowMoreServiceVC
@synthesize  categ_title;
@synthesize arr_service;
@synthesize btn_backTitle;

@synthesize allSer_collectionView;

@synthesize indexUsed;
@synthesize cellData;
@synthesize cellDataOfmore;
@synthesize btn_back;
-(IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)viewDidLoad {
    
    NSLog(@"XXXXXXX--------> value of indexUsed selected=%d",self.indexUsed);
    
    
    singleton=[SharedManager sharedSingleton];
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:SHOW_MORE_SERVICE_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    lbl_title.text=categ_title;
    
    // btn_backTitle=NSLocalizedString(@"home_small", nil);
    
    
    
    [btn_back setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
    [btn_back setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateSelected];
    
    NSLog(@"arr_service=%@",arr_service);
    
    [btn_filter setTitle:NSLocalizedString(@"filter", nil) forState:UIControlStateNormal];
    [btn_filter setTitle:NSLocalizedString(@"filter", nil) forState:UIControlStateSelected];
    
    allSer_collectionView.delegate = self;
    allSer_collectionView.dataSource = self;
    
    
    allSer_collectionView.backgroundColor = [UIColor clearColor];
    
    allSer_collectionView.collectionViewLayout = [[CustomImageFlowLayout alloc] init];
    
    //  allSer_collectionView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
    //  self.view.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
    self.view.backgroundColor= [UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0];
    
    
    
    self.navigationController.navigationBar.hidden = NO;
    
    vw_line.frame=CGRectMake(0, 64, fDeviceWidth, 0.5);
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    //   tbl_showMore.contentInset = UIEdgeInsetsMake(-1.0f, 0.0f, 0.0f, 0.0);
    table_data=[[NSMutableArray alloc]init];
    table_data =[arr_service mutableCopy];
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self addNavigationView];
    

}

-(void)addNavigationView

{
    
    btn_back.hidden = true;
    
    lbl_title.hidden = true;
    
    btn_filter.hidden = true;
    
    //  .hidden = true;
    
    NavigationView *nvView = [[NavigationView alloc] initRightBarButton];
    
    __weak typeof(self) weakSelf = self;
    
    nvView.didTapBackButton = ^(id btnBack) {
        
        [weakSelf backBtnAction:btnBack];
        
    };
    
    nvView.lblTitle.text = lbl_title.text;
    
    [nvView.rightBarButton setTitle:btn_filter.currentTitle forState:UIControlStateNormal];
    
    nvView.didTapRightBarButton = ^(id btnFilter) {
        
        [weakSelf btn_filterAction:btnFilter];
        
    };
    
    [self.view addSubview:nvView];
    
    [self.view layoutIfNeeded];
    
}

/*
 - (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
 {
 return 0.001f;
 }
 
 - (NSString*) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger)section
 {
 return nil;
 
 }
 
 - (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
 {
 
 return 0.001;
 }
 
 */

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 135 ;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        height = 220;
        
    }else {
        if (fDeviceWidth == 320)
        {
            height = 135;
        }
        else if (fDeviceWidth == 375)
        {
            height = 145;
        }
        else if (fDeviceWidth > 375)
        {
            height = 160;
        }
        
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return CGSizeMake((fDeviceWidth - 75 ) / 4.0, height);
    }
    return  CGSizeMake((fDeviceWidth - 60 ) / 3.0, height);
    /*
    if ([[ UIScreen mainScreen] bounds].size.height == 1024)
    {
        return CGSizeMake(150, 220);
    }
    if ([[ UIScreen mainScreen] bounds].size.height <= 568)
    {
        return CGSizeMake(90, 135);
    }
    return CGSizeMake(105, 150);*/
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //AllServiceCVCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    // AllServiceCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.dic_serviceInfo=[table_data objectAtIndex:indexPath.row];
    //vc.hidesBottomBarWhenPushed = YES;
    vc.tagComeFrom=@"OTHERS";
    
    vc.sourceTab     = @"home_more";
    vc.sourceState   = @"";
    vc.sourceSection = [NSString stringWithFormat:@"%@",[[table_data objectAtIndex:indexPath.row] valueForKey:@"serviceCard"]];
    vc.sourceBanner  = @"";
    
    [self presentViewController:vc animated:NO completion:nil];
    
    //  [self.navigationController pushViewController:vc animated:YES];
    //self.hidesBottomBarWhenPushed=NO;
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    //  [self fetchDatafromDB];
    [self setNeedsStatusBarAppearanceUpdate];
    
    /* UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backBtnAction:)];
     [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
     [self.view addGestureRecognizer:gestureRecognizer];
     */
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:NO];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btn_back.titleLabel setFont:[AppFont regularFont:17.0]];
    [btn_filter.titleLabel setFont:[AppFont regularFont:17.0]];
    lbl_title.font = [AppFont semiBoldFont:17.0];
    [btn_filter sizeToFit];
    CGRect frameFilter = btn_filter.frame;
    frameFilter.origin.x = fDeviceWidth - (frameFilter.size.width + 2);
    btn_filter.frame = frameFilter;
    
    
}
-(void)fetchDatafromDB
{
    NSArray *arrServiceSection=[[singleton.dbManager loadDataServiceSection]objectAtIndex:indexUsed];
    
    NSLog(@"arrServiceSection=%@",arrServiceSection);
    
    NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
    NSLog(@"arrServiceData=%@",arrServiceData);
    
    
    
    NSArray *itemsService = [[arrServiceSection valueForKey:@"SECTION_SERVICES"] componentsSeparatedByString:@","];
    NSLog(@"itemsService=%@",itemsService);
    
    NSMutableArray *section_service=[[NSMutableArray alloc]init];
    
    for (int j=0; j<[itemsService count]; j++)
    {
        NSArray *sectionserviceitem=[singleton.dbManager getServiceData:[itemsService objectAtIndex:j]];
        NSLog(@"sectionserviceitem=%@",sectionserviceitem);
        if ([sectionserviceitem count]!=0) {
            [section_service addObject:[sectionserviceitem objectAtIndex:0]];
            
        }
    }
    
    
    
    
    self.selectedMarks = [[NSMutableArray alloc] init];
    table_data=[[NSMutableArray alloc]init];
    table_data =[section_service mutableCopy];
    NSLog(@"tableData=%@",table_data);
    [allSer_collectionView reloadData];
    
    
    
    
    
}



- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
                        layout:(UICollectionViewLayout *) collectionViewLayout
        insetForSectionAtIndex:(NSInteger) section {
    
    return UIEdgeInsetsMake(20,13,0, 16); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
                   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    return 5.0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return table_data.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[ UIScreen mainScreen] bounds].size.height >= 1024)
    {
        static NSString *identifier = @"AllServiceCVCellForiPad";
        AllServiceCVCellForiPad *allCellipad = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        {
            allCellipad.serviceTitle.text =[[table_data valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row];
            //  cell.serviceImage.image=[UIImage imageNamed:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
            
            allCellipad.serviceTitle.font = [AppFont regularFont:18.0];

            NSURL *url=[NSURL URLWithString:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
            
            [allCellipad.serviceImage sd_setImageWithURL:url
                                        placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
            allCellipad.serviceFav.tag=indexPath.row;
            //int tagvalue= (int)(indexPath.row);
            
            
            //            UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
            //            UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
            //
            //            [allCellipad.serviceFav setImage:btnImage1 forState:UIControlStateNormal];
            //            [allCellipad.serviceFav setImage:btnImage2 forState:UIControlStateSelected];
            
            
            
            
            
            NSString *serviceid=[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
            
            NSString *serviceFav=[singleton.dbManager getServiceFavStatus:serviceid];//get service status from db
            
            if ([serviceFav isEqualToString:@"true"]) {
                allCellipad.serviceFav .selected=YES;
            }else{
                allCellipad.serviceFav .selected=NO;
            }
            
            
            
            cellData =(NSDictionary*) [table_data objectAtIndex:[indexPath row]];
            
            allCellipad.serviceInfo.celldata=[cellData mutableCopy];
            
            
            [allCellipad.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            allCellipad.serviceFav.usdata=[cellData objectForKey:@"SERVICE_NAME"];
            [allCellipad.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *rating=[cellData objectForKey:@"SERVICE_RATING"];
            
            float fCost = [rating floatValue];
            
            allCellipad.starRatingView.value = fCost;

            //[allCellipad.starRatingView setScore:fCost*2 withAnimation:NO];
            
            
            
            
        }
        return allCellipad;
        
        
    }
    
    else
    {
        static NSString *identifier = @"AllServiceCVCell";
        AllServiceCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        // cell.serviceImage.image = [UIImage imageNamed:@"ad"];;
        cell.serviceTitle.text =[[table_data valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row];
        //  cell.serviceImage.image=[UIImage imageNamed:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
        cell.serviceTitle.font = [AppFont regularFont:14.0];

        NSURL *url=[NSURL URLWithString:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
        
        [cell.serviceImage sd_setImageWithURL:url
                             placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
        
        
        
        cell.serviceFav.tag=indexPath.row;
        //int tagvalue= (int)(indexPath.row);
        
        
        //        UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
        //        UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
        //
        //        [cell.serviceFav setImage:btnImage1 forState:UIControlStateNormal];
        //        [cell.serviceFav setImage:btnImage2 forState:UIControlStateSelected];
        //
        
        
        
        
        NSString *serviceid=[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
        
        NSString *serviceFav=[singleton.dbManager getServiceFavStatus:serviceid];//get service status from db
        
        if ([serviceFav isEqualToString:@"true"]) {
            cell.serviceFav .selected=YES;
        }else{
            cell.serviceFav .selected=NO;
        }
        
        
        
        cellData =(NSDictionary*) [table_data objectAtIndex:[indexPath row]];
        
        cell.serviceInfo.celldata=[cellData mutableCopy];
        
        
        [cell.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.serviceFav.usdata=[cellData objectForKey:@"SERVICE_NAME"];
        [cell.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSString *rating=[cellData objectForKey:@"SERVICE_RATING"];
        
        float fCost = [rating floatValue];
        cell.starRatingView.value = fCost;

        //allCellipad.starRatingView.value = fCost;

        //[cell.starRatingView setScore:fCost*2 withAnimation:NO];
        
        
        
        
        
        return cell;
    }
}


//==================================
//       CUSTOM PICKER STARTS
//==================================
- (void)btnOpenSheet

{
    NSString *titlename=[NSString stringWithFormat:@"%@",[self.cellDataOfmore objectForKey:@"SERVICE_NAME"]];
    NSString *information=NSLocalizedString(@"information", nil);
    NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:titlename
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self callDetailServiceVC];
        }
            break;
        case 1:
        {
            NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
            
            NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
            
            NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
            
            NSString *deptName=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_NAME"]];
            deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]])
                
            {
                NSLog(@"Map App Found");
                
                NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
                
                // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
                
                NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSLog(@"mapURL= %@",mapURL);
                
                [[UIApplication sharedApplication] openURL:mapURL];
                
            } else
            {
                NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
                
            }
            
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

//==================================
//          CUSTOM PICKER ENDS
//==================================


//------------ Show more info-------------
-(IBAction)moreInfo:(MyButton*)sender
{
    NSLog(@"Data 1 = %@",sender.celldata);
    
    self.cellDataOfmore=[[NSMutableDictionary alloc]init];
    
    self.cellDataOfmore=(NSMutableDictionary*)sender.celldata;
    
    [self btnOpenSheet];
    
    
    /*
     NSString *titlename=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_NAME"]];
     
     // NSString *titleDesc=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_DESC"]];
     
     //  NSString *imageName=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_IMAGE"]];
     
     //  NSString *Type=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_CATEGORY"]];
     
     
     // [self displayContentController:[self getHomeDetailLayerLeftController]];
     
     
     UIAlertController * alert=   [UIAlertController
     alertControllerWithTitle:titlename
     message:nil
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* info = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"information", nil)                           style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     //[self displayContentController:[self getHomeDetailLayerLeftController]];
     [self callDetailServiceVC];
     
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* map = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"view_on_map", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     
     NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
     
     NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
     
     NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
     
     // NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%@,%@&daddr=%@,%@",latitude, longitute, latitude,longitute];
     
     //   NSString* googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@",latitude, longitute];
     
     
     //  [[UIApplication sharedApplication] openURL: [NSURL URLWithString: googleMapsURLString]];
     
     
     if ([[UIApplication sharedApplication] canOpenURL:
     [NSURL URLWithString:@"comgooglemaps://"]])
     
     {
     NSLog(@"Map App Found");
     
     NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f&center=%.6f,%.6f&zoom=15&views=traffic", [latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     
     
     
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
     
     
     } else
     {
     NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
     
     }
     
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* cancel = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"cancel", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     
     
     [alert addAction:info];
     [alert addAction:map];
     
     [alert addAction:cancel];
     
     
     UIViewController *vc=[self topMostController];
     [vc presentViewController:alert animated:NO completion:nil];
     */
}

-(void)callDetailServiceVC
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    vc.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [topvc presentViewController:vc animated:NO completion:nil];
    
}


- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


-(itemMoreInfoVC*)getHomeDetailLayerLeftController
{
    
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    //if (itemMoreVC == nil) {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    itemMoreVC = [storyboard instantiateViewControllerWithIdentifier:@"itemMoreInfoVC"];
    //}
    
    itemMoreVC.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    
    return itemMoreVC;
}


- (void) displayContentController: (UIViewController*) content;
{
    UIViewController *vc=[self topMostController];
    
    [vc addChildViewController:content];
    
    
    
    content.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    
    [vc.view addSubview:content.view];
    [content didMoveToParentViewController:vc];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil)
    //{
    //itemMoreVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        itemMoreVC.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil) {
    itemMoreVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        itemMoreVC.view.frame = CGRectMake(vc.view.frame.size.width, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}



//----------- END OF MORE INFO POP UP VIEW---------------


-(IBAction)fav_action:(MyFavButton*)sender
{
    
    MyFavButton *button = (MyFavButton *)sender; //instance of UIButton
    int indexOfTheRow=(int)button.tag;   //tag of the button
    NSLog(@"tabledata=%@",[table_data objectAtIndex:indexOfTheRow]);
    
    //    // Add image to button for pressed state
    //    UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
    //    UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
    //
    //    [button setImage:btnImage1 forState:UIControlStateNormal];
    //    [button setImage:btnImage2 forState:UIControlStateSelected];
    
    NSString *serviceId=[[table_data objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_ID"];
    
    NSString *serviceFav=[singleton.dbManager getServiceFavStatus:serviceId];
    
    
    
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
        button.selected=FALSE;
    }
    else
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
        button.selected=true;
    }
    
    //refresh data from database
    
    
}



//-----------for filterview show----------
-(IBAction)btn_filterAction:(id)sender
{
    //code to open filter view here
    
    NSLog(@"Filter Pressed");
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    addFilterShowMoreVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"addFilterShowMoreVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    // [self presentViewController:vc animated:NO completion:nil];
    vc.filterIndex=indexUsed;
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 
 
 */



@end
