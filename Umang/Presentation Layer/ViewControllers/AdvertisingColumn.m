
//===============================================================================//
//  AdvertisingColumn.m
//  Umang
//
//  Created by spice on 02/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "AdvertisingColumn.h"
#import "UIImageView+WebCache.h"

#import "SecuritySettingVC.h"

#import "HelpViewController.h"
#import "SettingsViewController.h"
#import "UserProfileVC.h"
#import "ShowUserProfileVC.h"

#import "SocialMediaViewController.h"
#import "FeedbackVC.h"
#import "AadharCardViewCon.h"
#import "NotLinkedAadharVC.h"
#import "RateUsVCViewController.h"
#import "HomeDetailVC.h"

#import "FAQWebVC.h"




@implementation AdvertisingColumn
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, fDeviceWidth, CGRectGetHeight(self.frame)-25)];
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.delegate = self;//UIscrollViewDelegate
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.pagingEnabled = YES;
        NSLog(@"scrollview frame=%f",_scrollView.frame.size.height);
        [self addSubview:_scrollView];
        
        
        // UIView *containerView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-25, CGRectGetWidth(self.frame), 20)];
        // UIView *containerView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-25, fDeviceWidth, 20)];
        containerView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-28, fDeviceWidth, 20)];
        if ([[UIScreen mainScreen]bounds].size.height >= 815)
        {
            
            //return 220;
            // containerView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-25, fDeviceWidth, 20)];
            containerView.frame  = CGRectMake(0, _scrollView.frame.size.height + 1, fDeviceWidth, 24);
            
            
        }
        if ([[UIScreen mainScreen]bounds].size.height == 812)
        {
            containerView.frame  = CGRectMake(0, CGRectGetHeight(self.frame)-32, fDeviceWidth, 22);
        }
        if ([[UIScreen mainScreen]bounds].size.height == 736)
        {
            //return 150;
            // containerView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-25, fDeviceWidth, 20)];
            containerView.frame  = CGRectMake(0, CGRectGetHeight(self.frame)-28, fDeviceWidth, 22);
        }
        if ([[UIScreen mainScreen]bounds].size.height == 480)
        {
            
            //return 150;
            containerView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-38, fDeviceWidth, 20)];
            
        }
        if ([[UIScreen mainScreen]bounds].size.height == 568)
        {
            
            //return 150;
            containerView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-38, fDeviceWidth, 20)];
            
        }
        
        
        
        
        containerView.backgroundColor = [UIColor clearColor];
        
        [self addSubview:containerView];
        
        _pageControl = [[StyledPageControl alloc] initWithFrame:CGRectMake(0,5, fDeviceWidth, 8)];
        //_pageControl.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [_pageControl setCurrentPage:0];
        [_pageControl setDiameter:8];
        
        
        //_pageControl.pageIndicatorTintColor = [[UIColor grayColor] colorWithAlphaComponent:.6];
        //_pageControl.currentPageIndicatorTintColor = [UIColor grayColor];
        
        [_pageControl setCoreNormalColor:[UIColor lightGrayColor]];
        [_pageControl setCoreSelectedColor:[UIColor grayColor]];
        [_pageControl setGapWidth:4];
        
        
        _pageControl.backgroundColor = [UIColor clearColor];
        [_pageControl setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        
        [_pageControl setPageControlStyle:PageControlStyleDefault];
        
        
        [containerView addSubview:_pageControl];
        
        /*_scrollView.backgroundColor=[UIColor orangeColor];
         containerView.backgroundColor=[UIColor blueColor];
         _pageControl.backgroundColor=[UIColor orangeColor];*/
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
        [[NSRunLoop  currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
        [_timer setFireDate:[NSDate distantFuture]];
        
    }
    return self;
}
//-------------------------------------------------------------------------------------------

-(void)updateFrameOfAllComponents{
    _scrollView.frame  = CGRectMake(0, 0, fDeviceWidth, CGRectGetHeight(self.frame)-25);
    // containerView.frame = CGRectMake(0, CGRectGetHeight(self.frame)-25, fDeviceWidth, 20);
    
    containerView.frame  = CGRectMake(0, CGRectGetHeight(self.frame)-28, fDeviceWidth, 20);
    
    NSLog(@"value of height=%f",[[UIScreen mainScreen]bounds].size.height);
    
    CGFloat rotatewidth=[[UIScreen mainScreen]bounds].size.height;
    if ([[UIScreen mainScreen]bounds].size.height >= 740)
    {
        
        containerView.frame  = CGRectMake(0, _scrollView.frame.size.height + 1, fDeviceWidth, 24);
        
        
    }
    if ([[UIScreen mainScreen]bounds].size.height == 736||[[UIScreen mainScreen]bounds].size.height == 414)
    {
        if(rotatewidth==414)
        {
            //return 150;
            containerView.frame  = CGRectMake(0, CGRectGetHeight(self.frame)-27, fDeviceWidth, 22);
        }
        else
        {
            containerView.frame  = CGRectMake(0, CGRectGetHeight(self.frame)-32, fDeviceWidth, 22);
            
        }
    }
    
    if ([[UIScreen mainScreen]bounds].size.height == 480||[[UIScreen mainScreen]bounds].size.height == 320||[[UIScreen mainScreen]bounds].size.height == 568)
    {
        
        //return 150;
        // _scrollView.frame  = CGRectMake(0, 0, fDeviceWidth, CGRectGetHeight(self.frame)-40);
        
        if(rotatewidth==480||rotatewidth==568)
        {
            containerView.frame  = CGRectMake(0, CGRectGetHeight(self.frame)-38, rotatewidth, 20);
            
        }
        else
        {
            _scrollView.frame  = CGRectMake(0, 0, fDeviceWidth, CGRectGetHeight(self.frame));
            containerView.frame  = CGRectMake(0, CGRectGetHeight(self.frame)-25, rotatewidth, 20);
        }
        
    }
    
    
    
    // _pageControl.frame  = CGRectMake(fDeviceWidth/2-30, 0, CGRectGetWidth(containerView.frame)-20, 20);
    _pageControl.frame  = CGRectMake(0,4, fDeviceWidth, 8);
    
    _scrollView.contentOffset = CGPointMake(0.0, 0.0);
    [_pageControl setCurrentPage:0];
    [[_scrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
}



-(void)timerAction:(NSTimer *)timer{
    if (_totalNum>1) {
        CGPoint newOffset = _scrollView.contentOffset;
        newOffset.x = newOffset.x + CGRectGetWidth(_scrollView.frame);
        NSLog(@"newOffset.x = %f",newOffset.x);
        if (newOffset.x > (CGRectGetWidth(_scrollView.frame) * (_totalNum-1))) {
            newOffset.x = 0 ;
        }
        int index = newOffset.x / CGRectGetWidth(_scrollView.frame);
        newOffset.x = index * CGRectGetWidth(_scrollView.frame);
        [_scrollView setContentOffset:newOffset animated:YES];
    }else{
        [_timer setFireDate:[NSDate distantFuture]];
    }
}

#pragma mark- PageControl绑定ScrollView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isMemberOfClass:[UITableView class]]) {
        
    }else {
        int index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
        _pageControl.currentPage = index;
        for (UIView *view in scrollView.subviews) {
            if(view.tag == index){
                
            }else{
                
            }
        }
    }
    //    NSLog(@"string%f",scrollView.contentOffset.x);
}
- (void)setArray:(NSArray *)testArray{
    
    imgArray=[NSArray arrayWithArray:testArray];
    
    //CGSize size = [[UIScreen mainScreen] bounds].size;
    // CGFloat frameX = size.width;
    
    _totalNum = [imgArray count];
    if (_totalNum>0) {
        
        for (int i = 0; i<_totalNum; i++)
        {
            
            //UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(i*fDeviceWidth,0, fDeviceWidth+3, CGRectGetHeight(_scrollView.frame))];
            
            
            UIImageView *img = [[UIImageView alloc]init];
            
            //  img.frame = CGRectMake(i*fDeviceWidth+2, 0, fDeviceWidth-4, AD_height+15-25);
            
            img.frame = CGRectMake(i*fDeviceWidth, 0, fDeviceWidth, _scrollView.frame.size.height);
            
            
            if ([[UIScreen mainScreen]bounds].size.height == 736)
            {
                
                //return 150;
                // img.frame = CGRectMake(i*fDeviceWidth+2, 0, fDeviceWidth-4, AD_height+25-25);
                img.frame = CGRectMake(i*fDeviceWidth, 0, fDeviceWidth, AD_height+25-25);
                
            }
            if ([[UIScreen mainScreen]bounds].size.height == 480)
            {
                
                //return 150;
                // img.frame = CGRectMake(i*fDeviceWidth+2,0, fDeviceWidth-4, AD_height+25-38);
                img.frame = CGRectMake(i*fDeviceWidth,0, fDeviceWidth, AD_height+25-38);
                
            }
            
            
            NSLog(@"fdeviceWidth=%f",fDeviceWidth);
            
            if (fDeviceWidth<=500) {
                img.contentMode = UIViewContentModeScaleAspectFit;
                
            }
            else
            {
                // img.contentMode = UIViewContentModeScaleAspectFill;
                img.contentMode = UIViewContentModeScaleAspectFit;
                // img.contentMode = UIViewContentModeScaleAspectFit;
                
            }
            
            
            img.clipsToBounds = true;
            
            
            
            NSString* category_icon=[[imgArray objectAtIndex:i] valueForKey:@"BANNER_IMAGE_URL"];
            dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
                NSURL *url=[NSURL URLWithString:category_icon];
                //set your image on main thread.
                //                [img sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"img_loadertime.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                //                    dispatch_async(dispatch_get_main_queue(),^{
                //                        img.image = image;
                //                        NSLog(@"banner Image Size ---%@ 0---- with imageViewe Hroight ---&%@",image, img);
                ////                    //[img sd_setImageWithURL:url
                ////                                                   placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
                //                    });
                //                }];
                dispatch_async(dispatch_get_main_queue(),^{
                    [img sd_setImageWithURL:url
                           placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
                });
            });
            
            
            
            
            img.userInteractionEnabled = YES;
            
            UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
            [img setTag:i];
            
            [singleTap setNumberOfTapsRequired:1];
            [img addGestureRecognizer:singleTap];
            
            [_scrollView addSubview:img];
        }
        // _pageControl.numberOfPages = _totalNum;
        [_pageControl setNumberOfPages:_totalNum];
        
        
        /* CGRect frame;
         frame = _pageControl.frame;
         frame.size.width = 15*_totalNum;
         _pageControl.frame = frame;*/
    }else{
        //  UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_scrollView.frame), CGRectGetHeight(_scrollView.frame))];
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(2, 0, fDeviceWidth-4, CGRectGetHeight(_scrollView.frame))];
        
        
        [img setImage:[UIImage imageNamed:@"img_loadertime.png"]];
        // img.userInteractionEnabled = YES;
        //img.contentMode = UIViewContentModeScaleAspectFill;
        img.contentMode = UIViewContentModeScaleAspectFit;
        
        
        
        
        // img.layer.cornerRadius = 10;
        img.clipsToBounds = YES;
        [_scrollView addSubview:img];
    }
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(_scrollView.frame)*_totalNum,CGRectGetHeight(_scrollView.frame));
}



-(void)singleTapping:(UIGestureRecognizer *)recognizer
{
    NSLog(@"image clicked");
    
    UIImageView *tableGridImage = (UIImageView*)recognizer.view;
    
    /*
     "BANNER_ACTION_TYPE" = url;
     "BANNER_ACTION_URL" = "https://www.google.co.in/?gws_rd=cr&ei=lcl0Uur4NofArAfG6oCQCQ";
     "BANNER_DESC" = umang;
     "BANNER_IMAGE_URL" = "https://static.umang.gov.in/app/hero/banner3.png";
     ID = 99;
     */
    
    
    int index=(int)tableGridImage.tag;
    
    if ([imgArray count]>0) {
        
        
        NSString* actionType=[[imgArray objectAtIndex:index] valueForKey:@"BANNER_ACTION_TYPE"];
        NSString* actionURL=[[imgArray objectAtIndex:index] valueForKey:@"BANNER_ACTION_URL"];
        NSString* actionDes=[[imgArray objectAtIndex:index] valueForKey:@"BANNER_DESC"];
        //NSString* actionID=[[imgArray objectAtIndex:index] valueForKey:@"ID"];
        NSString* bannerid=[[imgArray objectAtIndex:index] valueForKey:@"BANNER_ID"];

        
        
        NSMutableDictionary *bannerDic=[NSMutableDictionary new];
        [bannerDic setObject:actionType forKey:@"BANNER_ACTION_TYPE"];
        [bannerDic setObject:actionURL forKey:@"BANNER_ACTION_URL"];
        [bannerDic setObject:actionDes forKey:@"BANNER_DESC"];
        [bannerDic setObject:bannerid forKey:@"BANNER_ID"];

        //  [bannerDic setObject:actionID forKey:@"ID"];
        
        
        [self selectedIndexNotify:bannerDic];
        
        
    }
}






-(void)selectedIndexNotify:(NSDictionary*)bannerData
{
    SharedManager *singleton=[SharedManager sharedSingleton];
    
    
    //----------Handle case for subType---------------
    NSString *subType =[bannerData valueForKey:@"BANNER_ACTION_TYPE"];
    //subType=[subType  lowercaseString];
    // Case to open app  for opening app with notification title/
    if([subType isEqualToString:@"openApp"])
    {
        //handle case in delegate for it do nothing
        
    }
    // Case to open app  with dialog message dialogmsg/msg
    
    else if([subType isEqualToString:@"openAppWithDialog"])
    {
        NSString *dialogMsg =[bannerData valueForKey:@"BANNER_DESC"];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:dialogMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
        
    }
    // Case to open  playstore url in external
    
    else if([subType isEqualToString:@"playstore"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[bannerData valueForKey:@"BANNER_ACTION_URL"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        
        
    }
    // Case to open app  inside webview with custom webview title
    
    else if([subType isEqualToString:@"webview"])
    {
       // NSString *title =[bannerData valueForKey:@"BANNER_ACTION_TYPE"];
      //  NSString *url=[NSString stringWithFormat:@"%@",[bannerData valueForKey:@"BANNER_ACTION_URL"]];
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
      //  [self openFAQWebVC:url withTitle:title];
        
        
        NSString *stringdata=[bannerData valueForKey:@"BANNER_ACTION_URL"];
        NSArray *stringArray = [stringdata componentsSeparatedByString: @"|"];
            NSString *url=[stringArray objectAtIndex:0];
            NSString *title=[stringArray objectAtIndex:1];
        [self openFAQWebVC:url withTitle:title];

        
        /*
         "BANNER_ACTION_TYPE" = webview;
         "BANNER_ACTION_URL" = "https://self4society.mygov.in|Self4Society";
         */
        
    }
    // Case to open app  in mobile browser
    
    else if([subType isEqualToString:@"browser"]||[subType isEqualToString:@"youtube"]||[subType isEqualToString:@"url"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[bannerData valueForKey:@"BANNER_ACTION_URL"]];
        
        NSString* webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL* urltoOpen = [NSURL URLWithString:webStringURL];
        
        [[UIApplication sharedApplication] openURL:urltoOpen];
        
        
    }
    // Case to open app  with Screen Name like profile/settings etc
    
    else if([subType isEqualToString:@"openAppWithScreen"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[bannerData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        
        if ([screenName isEqualToString:@"settings"])
        {
            [self mySetting_Action];
            
        }
        if ([screenName isEqualToString:@"help"])
        {
            [self myHelp_Action];
            
        }
        if ([screenName isEqualToString:@"social"])
        {
            [self socialMediaAccount];
            
        }
        if ([screenName isEqualToString:@"aadhaar"])
        {
            if (singleton.objUserProfile.objAadhar.aadhar_number.length)
            {
                
                [self AadharCardViewCon];
                
            }
            else
            {
                [self NotLinkedAadharVC];
                
            }
        }
        if ([screenName isEqualToString:@"feedback"])
        {
            [self FeedbackVC];
            
        }
        if ([screenName isEqualToString:@"accountsettings"])
        {
            [self accountSettingAction];
        }
        if ([screenName isEqualToString:@"myprofile"])
        {
            [self myProfile_Action];
            
        }
        else
        {
            //main
        }
        
        
    }
    // Case to open app  with tab name
    
    else if([subType isEqualToString:@"openAppWithTab"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[bannerData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        
        if ([screenName isEqualToString:NSLocalizedString(@"home_small", nil)])
        {
            tbc.selectedIndex=0;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"favourites_small", nil)])
        {
            tbc.selectedIndex=1;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"states", nil)])
        {
            //tbc.selectedIndex=0;ignore case
            
        }
        if ([screenName isEqualToString:  NSLocalizedString(@"all_services_small", nil)])
        {
            tbc.selectedIndex=2;
            
        }
        
        
        UIViewController *topvc=[self topMostController];
        [topvc presentViewController:tbc animated:NO completion:nil];
        
        
        
    }
    // Case to open app with service
    
    else  if([subType isEqualToString:NSLocalizedString(@"services", nil)]||[subType isEqualToString:@"service"])
    {
        
        if (bannerData)
        {
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            // BANNER_ACTION_URL
            
            // SERVICE_ID_to_Pass=[dic_serviceInfo valueForKey:@"SERVICE_ID"];
            //titleStr=[dic_serviceInfo valueForKey:@"SERVICE_NAME"];
            // _lbltitle.text = titleStr;
            //urlString=[dic_serviceInfo valueForKey:@"SERVICE_URL"];
            
            NSString *stringdata=[bannerData valueForKey:@"BANNER_ACTION_URL"];
            
            NSArray *stringArray = [stringdata componentsSeparatedByString: @"|"];
            
            NSMutableDictionary *bannerActionData=[NSMutableDictionary new];
            
            @try {
                
                NSString *service_url=[stringArray objectAtIndex:0];
                NSString *service_title=[stringArray objectAtIndex:1];
                NSString *service_id=[stringArray objectAtIndex:2];
                
                
                [bannerActionData setObject:service_id forKey:@"SERVICE_ID"];
                [bannerActionData setObject:service_title forKey:@"SERVICE_NAME"];
                [bannerActionData setObject:service_url forKey:@"SERVICE_URL"];
                
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            
            
            HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
            [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            vc.dic_serviceInfo=bannerActionData;
            vc.tagComeFrom=@"OTHERS";
            vc.sourceState   = @"";
            if ([self.comingFrom isEqualToString:@"home"])
            {
                vc.sourceTab     = @"home";
            }
            else
            {
                vc.sourceTab     = @"state";
                vc.sourceState   = self.stateIdToPass;
            }
            
            vc.sourceSection = @"banner";
            vc.sourceBanner  = [NSString stringWithFormat:@"%@",[bannerData valueForKey:@"BANNER_ID"]];
            
            UIViewController *topvc=[self topMostController];
            [topvc presentViewController:vc animated:NO completion:nil];
            
            
        }
        
    }
    // Case to open app  for rating view
    
    else  if([subType isEqualToString:NSLocalizedString(@"Rating", nil)] || [subType isEqualToString:@"rating"])
    {
        [self rateUsClicked];
        
    }
    // Case to open app  for share [sharing message will recieve inside api)
    else  if([subType isEqualToString:@"share"] || [subType isEqualToString:NSLocalizedString(@"share", nil)])
    {
        [self shareContent];
    }
    //Default case
    else
    {
        
        
    }
    
    
}


-(void)shareContent
{
    SharedManager *singleton=[SharedManager sharedSingleton];
    NSString *textToShare =singleton.shareText;
    
    
    NSArray *objectsToShare = @[textToShare];
    
    [[UIPasteboard generalPasteboard] setString:textToShare];
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    
    UIViewController *topvc=[self topMostController];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [topvc presentViewController:controller animated:YES completion:nil];
        
    }
    
    //if iPad
    
    else {
        
        // Change Rect to position Popover
        
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        [popup presentPopoverFromRect:CGRectMake(topvc.view.frame.size.width/2, topvc.view.frame.size.height/4, 0, 0)inView:topvc.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}


//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)openFAQWebVC:(NSString *)url withTitle:(NSString*)title
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
  //  [topvc.navigationController pushViewController:vc animated:YES];
    
    vc.isfrom =@"WEBVIEWHANDLE";
    [topvc presentViewController:vc animated:NO completion:nil];

    
    
}
-(void)rateUsClicked
{
    NSLog(@"My Help Action");
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    RateUsVCViewController  *vc = [storyboard instantiateViewControllerWithIdentifier:@"RateUsVCViewController"];
    //  vc.hidesBottomBarWhenPushed = YES;
    UIViewController *topvc=[self topMostController];
    
    //[vc bringSubviewToFront:topvc.view];
    
    [topvc presentViewController:vc animated:NO completion:nil ];
    // [topvc.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)NotLinkedAadharVC
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    // SettingsViewController
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotLinkedAadharVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NotLinkedAadharVC"];
    vc.hidesBottomBarWhenPushed = YES;
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
}

-(void)AadharCardViewCon
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    // SettingsViewController
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AadharCardViewCon *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharCardViewCon"];
    vc.hidesBottomBarWhenPushed = YES;
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
}
-(void)FeedbackVC
{
    
    // SettingsViewController
     UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FeedbackVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)socialMediaAccount
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    // SettingsViewController
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SocialMediaViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SocialMediaViewController"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)myProfile_Action
{
    NSLog(@"My Profile Action");
    
    
    
    
    // SettingsViewController
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
    
    /*UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kMainStoryBoard bundle:nil];
     
     UserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];*/
    vc.hidesBottomBarWhenPushed = YES;
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
}

-(void)mySetting_Action
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSLog(@"My Setting Action");
    
    // SettingsViewController
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SettingsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    
    
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
}
-(void)myHelp_Action
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSLog(@"My Help Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
}
-(void)accountSettingAction
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSLog(@"My account setting Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SecuritySettingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecuritySettingVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
    
}



- (void)openTimer{
    [_timer setFireDate:[NSDate distantPast]];
}
- (void)closeTimer{
    [_timer setFireDate:[NSDate distantFuture]];
}



@end

