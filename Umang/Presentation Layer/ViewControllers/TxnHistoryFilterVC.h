//
//  TxnHistoryFilterVC.h
//  Umang
//
//  Created by Kuldeep Saini on 1/9/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TxnHistoryFilterVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblFilter;

@end
