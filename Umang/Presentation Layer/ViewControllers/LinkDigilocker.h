//
//  LinkDigilocker.h
//  Umang
//
//  Created by admin on 16/02/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkDigilocker : UIViewController<UITextFieldDelegate>
{
    IBOutlet UIView *vw_txfield;
    IBOutlet UITextField *txt_username;
    IBOutlet UITextField *txt_userpass;
    IBOutlet UIButton *btn_login;
    IBOutlet UILabel *lbl_linkdigilock;
    IBOutlet UILabel *lbl_title;
    

    
}

-(IBAction)backBtnAction:(id)sender;
-(IBAction)submitBtnAction:(id)sender;

@end
