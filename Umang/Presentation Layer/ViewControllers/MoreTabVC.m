//
//  MoreTabVC.m
//
//
//  Created by spice_digital on 18/10/16.
//
//

#import "MoreTabVC.h"
#import "MoreCell.h"
#import <QuartzCore/QuartzCore.h>

#import "SecuritySettingVC.h"
#import "SocialMediaViewController.h"
#import "NotLinkedAadharVC.h"
#import "SettingsViewController.h"
#import "HelpViewController.h"
#import "UserProfileVC.h"
#import "ShowUserProfileVC.h"

#import "ServiceDirectoryVC.h"
#import "RateUsVCViewController.h"
#import "AadharCardViewCon.h"
#import "DetailServiceVC.h"
//#import "LoginViewController.h"
#import "AboutInfoVC.h"
#import "LoginAppVC.h"
#import "TransactionalHistoryVC.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "FeedbackVC.h"
#import "UserProfileVC.h"
#import "DigiLockerVC.h"
#import "ShowUserProfileVC.h"
#import "UMANG-Swift.h"

#import "UIImageView+WebCache.h"
#import "TabBarVC.h"

#import "NSBundle+Language.h"
#import "FAQWebVC.h"
#import "DigilockerMenuVC.h"
#define fDeviceWidth ([UIScreen mainScreen].bounds.size.width)
#define fDeviceHeight ([UIScreen mainScreen].bounds.size.height)

#import "SocialAuthentication.h"
#import "UmangWebChatVC.h"
#import "DigiLockerWebVC.h"
#import "DigiLockLoginVC.h"
#import "UIView+Toast.h"

#import "DigiLockCreateAccountVC.h"
#import "DigiLockHomeVC.h"
#import "LiveChatVC.h"
#import "RunOnMainThread.h"
@interface MoreTabVC ()
{
    SharedManager * singleton;
    MBProgressHUD *hud ;
    
    RateUsVCViewController *rateView;
    NSMutableArray *tabarray;
    
    
    //------------profile parameter---------------
    //----- local values  to be save and show from API-----------
    NSString *str_name;
    NSString *str_gender;
    NSString *str_dob;
    NSString *str_qualification;
    NSString *str_occupation;
    NSString *str_state;
    NSString *str_district;
    NSString *str_registerMb;
    NSString *str_emailAddress;
    NSString *str_alternateMb;
    NSString *str_emailVerifyStatus;
    NSString *str_amnosVerifyStatus;
    NSString *str_address;
    
    NSString *str_Url_pic;
    
    NSMutableArray *socialpd;
    
    
    
    NSString *stringGender;
    
    BOOL isDidLoad;
    BOOL isLiveChatexit;
    BOOL includeServiceDir;
}
@property(nonatomic,retain)NSMutableArray *table_data;
@property(nonatomic,retain)NSMutableArray *table_imgdata;
@property(nonatomic,retain)IBOutlet UITableView *tbl_more;

@end

@implementation MoreTabVC

@synthesize table_data;
@synthesize table_imgdata;
@synthesize tbl_more;




- (void)viewDidLoad
{
    //test case for downtime
   // AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
   // delegate.testCase=426;
    
    isDidLoad = true;
    singleton = [SharedManager sharedSingleton];
    
    NSLog(@"singleton.user_tkn=%@",singleton.user_tkn);
    NSLog(@"Singleton Profile Name %@",singleton.profileNameSelected);
    
    //NSLog(@"singleton.user_tkn=%@",[defaults stringForKey:@"USER_ID"]);
    
    
    [self refreshData]; ///load default frame of image first time

    
    [self hitFetchProfileAPI];
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:MORE_TAB_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    // img_profileView.image=[UIImage imageNamed:@"aadhar_photo"];
    
    
    
    img_blurProfile.contentMode = UIViewContentModeScaleAspectFill;
    
    tbl_more.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    
    self.navigationController.navigationBar.hidden = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
   
    
    [self.tbl_more setShowsHorizontalScrollIndicator:NO];
    [self.tbl_more setShowsVerticalScrollIndicator:NO];
    
    
    
    lbl_name.font = [UIFont systemFontOfSize:lbl_name.font.pointSize weight:UIFontWeightRegular];
    lbl_age_gender.font = [UIFont systemFontOfSize:lbl_age_gender.font.pointSize weight:UIFontWeightLight];
    lbl_Place.font = [UIFont systemFontOfSize:lbl_Place.font.pointSize weight:UIFontWeightLight];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        tbl_more.frame = CGRectMake(80, tbl_more.frame.origin.y + 60, self.view.frame.size.width - 160, tbl_more.frame.size.height);
        
        img_blurProfile.frame = CGRectMake(img_blurProfile.frame.origin.x, img_blurProfile.frame.origin.y, img_blurProfile.frame.size.width, 60);
        
        tbl_more.backgroundColor= [UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0];
        
        tbl_more.alwaysBounceVertical = NO;
        
        self.view.backgroundColor = tbl_more.backgroundColor;
    }
    
}
-(void)addDataSource {
    if (!includeServiceDir) {
        includeServiceDir = [[[NSUserDefaults standardUserDefaults] valueForKey:@"Enable_ServiceDir"] boolValue];
    }else {
        return;
    }
    @try {
        NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
        tabarray=[NSMutableArray new];
        tabarray=[singleton.extraTabArray mutableCopy];
        //======== Check for live chat exist or not ==============
        NSMutableDictionary *dic=[NSMutableDictionary new];
        [dic setObject:@"LIVE CHAT" forKey:@"TABNAME"];
        [dic setObject:@"livechat" forKey:@"tabAction"];
        [dic setObject:@"live_chat" forKey:@"ICON_URL"];
        
        
        NSInteger anIndex=[tabarray indexOfObject:dic];
        if(NSNotFound == anIndex)
        {
            NSLog(@"not found at index=%ld",(long)anIndex);
            
            isLiveChatexit=FALSE;
            
            if (includeServiceDir)
            {
                /* table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"my_profile", nil),NSLocalizedString(@"aadhaar_profile", nil),NSLocalizedString(@"social_media_accounts", nil),NSLocalizedString(@"transaction_history", nil),NSLocalizedString(@"service_directory", nil),NSLocalizedString(@"digi_locker", nil),NSLocalizedString(@"settings", nil),NSLocalizedString(@"share", nil),NSLocalizedString(@"rate_us", nil),NSLocalizedString(@"send_feedback", nil),NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"about", nil),NSLocalizedString(@"logout", nil),nil];
                 
                 table_imgdata=[[NSMutableArray alloc]initWithObjects:@"more_umang_profile",@"more_aadhaar",@"more_social_media_account",@"more_transaction_history",@"icon_service_directory",@"more_digi_locker",@"more_settings",@"more_share",@"more_rate_us",@"more_send_feedback",@"more_help_support",@"more_about",@"more_logout", nil];
                 
                 */
                
                
                table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"my_profile", nil),NSLocalizedString(@"social_media_accounts", nil),NSLocalizedString(@"transaction_history", nil),NSLocalizedString(@"service_directory", nil),NSLocalizedString(@"digi_locker", nil),NSLocalizedString(@"settings", nil),NSLocalizedString(@"tell_friend", nil),NSLocalizedString(@"rate_us", nil),NSLocalizedString(@"send_feedback", nil),NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"about", nil),NSLocalizedString(@"logout", nil),nil];
                
                table_imgdata=[[NSMutableArray alloc]initWithObjects:@"more_umang_profile",@"more_social_media_account",@"more_transaction_history",@"icon_service_directory",@"more_digi_locker",@"more_settings",@"more_share",@"more_rate_us",@"more_send_feedback",@"more_help_support",@"more_about",@"more_logout", nil];
            }
            else
            {
                /*
                 table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"my_profile", nil),NSLocalizedString(@"aadhaar_profile", nil),NSLocalizedString(@"social_media_accounts", nil),NSLocalizedString(@"transaction_history", nil),NSLocalizedString(@"digi_locker", nil),NSLocalizedString(@"settings", nil),NSLocalizedString(@"share", nil),NSLocalizedString(@"rate_us", nil),NSLocalizedString(@"send_feedback", nil),NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"about", nil),NSLocalizedString(@"logout", nil),nil];
                 
                 table_imgdata=[[NSMutableArray alloc]initWithObjects:@"more_umang_profile",@"more_aadhaar",@"more_social_media_account",@"more_transaction_history",@"more_digi_locker",@"more_settings",@"more_share",@"more_rate_us",@"more_send_feedback",@"more_help_support",@"more_about",@"more_logout", nil];*/
                
                table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"my_profile", nil),NSLocalizedString(@"social_media_accounts", nil),NSLocalizedString(@"transaction_history", nil),NSLocalizedString(@"digi_locker", nil),NSLocalizedString(@"settings", nil),NSLocalizedString(@"tell_friend", nil),NSLocalizedString(@"rate_us", nil),NSLocalizedString(@"send_feedback", nil),NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"about", nil),NSLocalizedString(@"logout", nil),nil];
                
                table_imgdata=[[NSMutableArray alloc]initWithObjects:@"more_umang_profile",@"more_social_media_account",@"more_transaction_history",@"more_digi_locker",@"more_settings",@"more_share",@"more_rate_us",@"more_send_feedback",@"more_help_support",@"more_about",@"more_logout", nil];
            }
            
            
            
            
            
        }
        else
            
        {
            NSLog(@" found=%ld",(long)anIndex);
            [tabarray removeObjectAtIndex:anIndex]; //remove object from tab array for the live chat item as it need to be shown in section B
            
            
            isLiveChatexit=TRUE;
            
            //set badge count to nil when user click on tab bar
            //[[self.tabBarController.tabBar.items objectAtIndex:0] setBadgeValue:nil];
            
            
            
            if (includeServiceDir)
            {
                /*  table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"my_profile", nil),NSLocalizedString(@"aadhaar_profile", nil),NSLocalizedString(@"social_media_accounts", nil),NSLocalizedString(@"transaction_history", nil),NSLocalizedString(@"service_directory", nil),NSLocalizedString(@"digi_locker", nil),NSLocalizedString(@"settings", nil),NSLocalizedString(@"share", nil),NSLocalizedString(@"rate_us", nil),NSLocalizedString(@"send_feedback", nil),NSLocalizedString(@"help_live_chat", nil),
                 NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"about", nil),NSLocalizedString(@"logout", nil),nil];
                 
                 table_imgdata=[[NSMutableArray alloc]initWithObjects:@"more_umang_profile",@"more_aadhaar",@"more_social_media_account",@"more_transaction_history",@"icon_service_directory",@"more_digi_locker",@"more_settings",@"more_share",@"more_rate_us",@"more_send_feedback",@"live_chat",@"more_help_support",@"more_about",@"more_logout", nil];
                 */
                
                
                table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"my_profile", nil),NSLocalizedString(@"social_media_accounts", nil),NSLocalizedString(@"transaction_history", nil),NSLocalizedString(@"service_directory", nil),NSLocalizedString(@"digi_locker", nil),NSLocalizedString(@"settings", nil),NSLocalizedString(@"tell_friend", nil),NSLocalizedString(@"rate_us", nil),NSLocalizedString(@"send_feedback", nil),NSLocalizedString(@"help_live_chat", nil),
                            NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"about", nil),NSLocalizedString(@"logout", nil),nil];
                
                table_imgdata=[[NSMutableArray alloc]initWithObjects:@"more_umang_profile",@"more_social_media_account",@"more_transaction_history",@"icon_service_directory",@"more_digi_locker",@"more_settings",@"more_share",@"more_rate_us",@"more_send_feedback",@"live_chat",@"more_help_support",@"more_about",@"more_logout", nil];
            }
            else
            {
                table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"my_profile", nil),NSLocalizedString(@"social_media_accounts", nil),NSLocalizedString(@"transaction_history", nil),NSLocalizedString(@"digi_locker", nil),NSLocalizedString(@"settings", nil),NSLocalizedString(@"tell_friend", nil),NSLocalizedString(@"rate_us", nil),NSLocalizedString(@"send_feedback", nil),NSLocalizedString(@"help_live_chat", nil),
                            NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"about", nil),NSLocalizedString(@"logout", nil),nil];
                
                table_imgdata=[[NSMutableArray alloc]initWithObjects:@"more_umang_profile",@"more_social_media_account",@"more_transaction_history",@"more_digi_locker",@"more_settings",@"more_share",@"more_rate_us",@"more_send_feedback",@"live_chat",@"more_help_support",@"more_about",@"more_logout", nil];
            }
            
            
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

-(void)refreshData
{
    
    img_blurProfile.image=[UIImage imageNamed:@"img_bgprofile"];
    
    
    //code to be executed in the background
    
    img_profileView.layer.cornerRadius = img_profileView.frame.size.height /2;
    img_profileView.layer.masksToBounds = YES;
    img_profileView.layer.borderWidth = 2;
    img_profileView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    
    UIImage *tempImg;
    if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_male", nil)])
    {
        tempImg=[UIImage imageNamed:@"male_avatar"];
        
    }
    else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_female", nil)]) {
        tempImg=[UIImage imageNamed:@"female_avatar"];
        
    }
    else
    {
        tempImg=[UIImage imageNamed:@"user_placeholder"];
        
        
    }
    
    
    
    UIImage *tempImg1 ;
    @try {
        if([[NSFileManager defaultManager] fileExistsAtPath:singleton.imageLocalpath])
        {
            // ur code here
            NSLog(@"file present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            
            tempImg1 = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
            if (tempImg1==nil) {
                
                tempImg1=tempImg;
                
            }
            img_profileView.image=tempImg1;
        } else {
            // ur code here**
            NSLog(@"Not present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            tempImg1=tempImg;
            
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    if ([singleton.notiTypeGenderSelected length]!=0)
    {
        lbl_age_gender.text=[NSString stringWithFormat:@"%@",singleton.notiTypeGenderSelected];
    }
    if ([singleton.profileDOBSelected length]!=0)
    {
        // Convert string to date object
        @try {
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd-MM-yyyy"];
            NSDate *date1 = [dateFormat dateFromString:singleton.profileDOBSelected];
            NSDate *date2 = [NSDate date];
            NSDateComponents *components;
            NSInteger days;
            
            components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit fromDate: date1 toDate: date2 options: 0];
            days = [components day];
            
            int yearsnew = (int)days/365;
            NSLog(@"There are %d days in yearsnew the two dates.", yearsnew);
            
            if(yearsnew<0)
            {
                yearsnew=-yearsnew;
            }
            NSLog(@"There are %d days in between the two dates.", yearsnew);
            if (yearsnew==0)
            {
                if ([singleton.notiTypeGenderSelected length]!=0) {
                    
                    lbl_age_gender.text=[NSString stringWithFormat:@"%@",singleton.notiTypeGenderSelected];
                }
                
            }
            else
            {
                if ([singleton.notiTypeGenderSelected length]!=0) {
                    
                    lbl_age_gender.text=[NSString stringWithFormat:@"%d,%@",yearsnew,singleton.notiTypeGenderSelected];
                }
                else
                {
                    lbl_age_gender.text=[NSString stringWithFormat:@"%d",yearsnew];
                    
                }
            }
            
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
    }
    
    if([singleton.profileNameSelected length]!=0)
    {
        lbl_name.text=singleton.profileNameSelected;
        img_profileView.hidden=FALSE;
        
    }
    else
    {
        lbl_name.text=@"";
    }
    if([singleton.mobileNumber length]!=0)
    {
        lbl_Place.text=[NSString stringWithFormat:@"%@",singleton.mobileNumber];
    }
    
    
    [tbl_more reloadData];
    
    
    
    
    [self refreshImage:tempImg1];
    
    
    
}



-(void)refreshImage:(UIImage*)tempImg1
{
    
    // img_profileView.image=tempImg;
    
    
    
    
    
    NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
    if ([singleton.user_profile_URL length]!=0) {
        
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //------------------------- Encrypt Value------------------------
        // [img_profileView sd_setImageWithURL:url
        //       placeholderImage:tempImg1];
        
        [img_profileView sd_setImageWithURL:url placeholderImage:tempImg1 options:0];
        
    }
    else
    {
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //------------------------- Encrypt Value------------------------
        
        img_profileView.image = tempImg1;
        
    }
    
    
    
    
    
    
}






-(void)refreshDataForLanguage{
    table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"my_profile", nil),NSLocalizedString(@"aadhaar_profile", nil),NSLocalizedString(@"social_media_accounts", nil),NSLocalizedString(@"transaction_history", nil),NSLocalizedString(@"digi_locker", nil),NSLocalizedString(@"settings", nil),NSLocalizedString(@"tell_friend", nil),NSLocalizedString(@"rate_us", nil),NSLocalizedString(@"send_feedback", nil),NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"about", nil),NSLocalizedString(@"logout", nil),nil];
    
    [tbl_more reloadData];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    // [self hitFetchProfileAPI];
    [self addDataSource];
    if (isDidLoad == false ){
        [self refreshData];
    }
    isDidLoad = false;
    [self removeRateUSview];
    //------------- Network View Handle------------
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatBadgeIconReloadData) name:NOTIFICATION_CHAT_MESSAGE object:nil];
    //------------- Tab bar Handle------------
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDel.shouldRotate = YES;
    //------------- Tab bar Handle------------
    
    
    
    singleton = [SharedManager sharedSingleton];
    /* [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:NSLocalizedString(@"home_small", @"")];
     
     [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:NSLocalizedString(@"favourites_small", @"")];
     
     [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:NSLocalizedString(@"all_services_small", @"")];
     [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:NSLocalizedString(@"help_live_chat", @"")];
     
     
     [[self.tabBarController.tabBar.items objectAtIndex:4] setTitle:NSLocalizedString(@"more", @"")];
     */
    [self setNeedsStatusBarAppearanceUpdate];
    // [self refreshDataForLanguage];
    
    
    
   
    
    [tbl_more reloadData];
//    [tbl_more setNeedsLayout ];
//    [tbl_more layoutIfNeeded ];
//    [tbl_more reloadData];
   // [tbl_more setContentOffset:CGPointZero animated:YES];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    self.tabBarController.tabBar.itemPositioning = UITabBarItemPositioningCentered;
    
    [self setViewFont];
    [super viewWillAppear:NO];
    
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    
    lbl_name.font = [AppFont semiBoldFont:16.0];
    lbl_age_gender.font = [AppFont regularFont:14.0];
    lbl_Place.font = [AppFont mediumFont:15.0];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}

#pragma mark -
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_CHAT_MESSAGE object:nil]; //addObserver:self selector:@selector(chatBadgeIconReloadData) name:NOTIFICATION_CHAT_MESSAGE object:nil];
}
-(void)chatBadgeIconReloadData{
    
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [tbl_more reloadData];
    }];
    
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    [tbl_more reloadData];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    
    [tbl_more reloadData];
}




- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



-(void)rateUsClicked
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    RateUsVCViewController *content = [storyboard instantiateViewControllerWithIdentifier:@"RateUsVCViewController"];
    content.hidesBottomBarWhenPushed = YES;
    [self addChildViewController:content];
    content.view.center = self.view.center;
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
    
    
}

- (void)removeRateUSview
{
    for (int i=0; i<[self.childViewControllers count]; i++)
    {
        @try {
            UIViewController *removeView=(UIViewController*)[self.childViewControllers objectAtIndex:i];
            
            if ([removeView isKindOfClass:[RateUsVCViewController class]])
            {
                
                
                [UIView animateWithDuration:0.4 animations:^{
                    removeView.view.frame = CGRectMake(0, 0, removeView.view.frame.size.width, removeView.view.frame.size.height);
                } completion:^(BOOL finished) {
                    [removeView willMoveToParentViewController:nil];
                    [removeView.view removeFromSuperview];
                    [removeView removeFromParentViewController];
                    // [self.navigationController popViewControllerAnimated:YES];
                    
                }];
                
                
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
    }
}








- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 48;
    }
    return 50;
    
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Here you must return the number of sectiosn you want
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //Here, for each section, you must return the number of rows it will contain
    if (section==0)
    {
//        BOOL includeServiceDir = [[[NSUserDefaults standardUserDefaults] valueForKey:@"Enable_ServiceDir"] boolValue];
        
        if (includeServiceDir)
        {
            //return 7;
            return 6;
            
        }
        //return 6;
        return 5;
        
    }
    if (section==1)
    {
        return [tabarray count];
    }
    else
    {
        if(isLiveChatexit==FALSE)
            return 6;
        else
            return 7;
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //For each section, you must return here it's label
    if(section == 0) return @"";
    if(section == 1)
    {
        if ([tabarray count]>0) {
            return @"MORE TABS";
            
        }
        else
            return @"";
        
    }
    
    else
        //return NSLocalizedString(@"support", nil);
        return @"";
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0)
    {
        
        return 0.00001;
        
    }
    if (section==1)
    {
        if ([tabarray count]>0) {
            
            return 40.0;
        }
        else
            return 0.0001;
    }
    
    else
    {
        return 40;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0;
}





- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    if (section == 0)
    {
    }
    
    else
    {
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
        headerView.backgroundColor=[UIColor clearColor];
        
        
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(15,15,300, 30);
        
        CGRect labelFrame =  label.frame;
        labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
        label.frame = labelFrame;
        
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:0.298 green:0.337 blue:0.424 alpha:1.0];
        
        label.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
        
        
        
        label.text = [self tableView:tableView titleForHeaderInSection:section];
        
        label.font = [AppFont regularFont:14.0];
        
        [headerView addSubview:label];
        
        return headerView;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell1 forRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if ([cell1 isKindOfClass:[MoreCell class]])
    {
        
        MoreCell *morecell = (MoreCell*)cell1;
        
        if (singleton.isArabicSelected==TRUE)
        {
            morecell.img_cell.transform = CGAffineTransformMakeRotation(-M_PI);
            morecell.lbl_celltitle.transform = CGAffineTransformMakeRotation(-M_PI);
            morecell.transform = CGAffineTransformMakeRotation(180*0.0174532925);
            
            morecell.img_adhgarVerified.transform= CGAffineTransformMakeRotation(180*0.0174532925);
            morecell.lbl_celltitle.textAlignment=NSTextAlignmentRight;//right here
            
        }
        
        
        // morecell.lbl_celltitle.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        
        
        
        
        // update frame
        
        
        
        //  CGRect imgvwFrame = morecell.img_cell.frame;
        
        // imgvwFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 80 : 15;
        
        // morecell.img_cell.frame = imgvwFrame;
        
        
        
        
        
        
        
        // CGRect lblFrameTitle = morecell.lbl_celltitle.frame;
        
        // lblFrameTitle.origin.x = singleton.isArabicSelected ? 30 : 60;
        
        // morecell.lbl_celltitle.frame = lblFrameTitle;
        //  morecell.lbl_celltitle.backgroundColor=[UIColor greenColor];
        
        //CGRect lblFrame = morecell.img_adhgarVerified.frame;
        
        //lblFrame.origin.x = singleton.isArabicSelected ? 15 : CGRectGetWidth(tableView.frame) - 80;
        
        // morecell.img_adhgarVerified.frame = lblFrame;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [tbl_more setNeedsDisplay];
            
        });
        
        
    }
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"MoreCell";
    
    MoreCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[MoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.accessoryView = nil;
    [[cell viewWithTag:228899] removeFromSuperview]; // chat Count Badge
    
    /* cell.lbl_celltitle.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
     
     
     // update frame
     
     CGRect imgvwFrame = cell.img_cell.frame;
     imgvwFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 80 : 15;
     cell.img_cell.frame = imgvwFrame;
     
     
     
     
     CGRect lblFrame = cell.lbl_celltitle.frame;
     lblFrame.origin.x = singleton.isArabicSelected ? 10 : 60;
     cell.lbl_celltitle.frame = lblFrame;
     */
    
    
    if ([indexPath section]==0)
    {
        
        /*if (indexPath.row==1)//Aadhar Card
         
         {
         if (singleton.objUserProfile.objAadhar.aadhar_number.length)
         {
         
         cell.img_adhgarVerified.hidden=FALSE;
         // CGRect lblFrame = cell.img_adhgarVerified.frame;
         // lblFrame.origin.x = singleton.isArabicSelected ? 15 : CGRectGetWidth(tableView.frame) - 80;
         // cell.img_adhgarVerified.frame = lblFrame;
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
         
         
         }
         else
         {
         cell.img_adhgarVerified.hidden=TRUE;
         cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
         
         }
         
         
         
         }
         
         else
         {*/
        cell.img_adhgarVerified.hidden=TRUE;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
        // }
        
        cell.lbl_celltitle.text=[table_data objectAtIndex:indexPath.row];
        cell.img_cell.image=[UIImage imageNamed:[table_imgdata objectAtIndex:indexPath.row]];
        
        //return cell;
    }
    
    
    if ([indexPath section]==1)
    {
        
        
        cell.img_adhgarVerified.hidden=TRUE;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
        cell.lbl_celltitle.text=[[tabarray valueForKey:@"TABNAME"] objectAtIndex:indexPath.row];
        // Live Chat Badge Icon Update ------
        
        //  [dic setObject:tabAction forKey:@"tabAction"];
        /* NSURL *url=[NSURL URLWithString:[[tabarray valueForKey:@"ICON_URL"] objectAtIndex:indexPath.row]];
         
         [cell.img_cell sd_setImageWithURL:url
         placeholderImage:[UIImage imageNamed:@"icon_language"]
         options:SDWebImageRefreshCached];*/
        NSString *imgName=[[tabarray valueForKey:@"ICON_URL"] objectAtIndex:indexPath.row];
        
        
        
        cell.img_cell.image=[UIImage imageNamed:imgName];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //return cell;
    }
    
    
    
    
    else if ([indexPath section]== 2)
    {
//        BOOL includeServiceDir = [[[NSUserDefaults standardUserDefaults] valueForKey:@"Enable_ServiceDir"] boolValue];
        
        NSInteger number = 0;
        
        if (includeServiceDir)
        {
            // number = 7;
            number = 6;
            
        }
        else
        {
            //number = 6;
            number = 5;
            
        }
        
        
        cell.lbl_celltitle.text=[table_data objectAtIndex:indexPath.row + number];
        NSString *strUrlImage = [table_imgdata objectAtIndex:indexPath.row + number];
        cell.img_cell.image=[UIImage imageNamed:strUrlImage];
        cell.img_adhgarVerified.hidden=TRUE;
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        NSString *checkliveChat=[table_data objectAtIndex:indexPath.row + number];
        if ([checkliveChat isEqualToString:NSLocalizedString(@"help_live_chat", nil)])
        {
            
            if(isLiveChatexit==TRUE)
            {
                NSInteger countUnreadMessages=[singleton.chatBadgeCount integerValue]; //set the value of count of unread message
                
                if(countUnreadMessages>0)
                {
                    NSString *badgeValue = [NSString stringWithFormat:@"%lu", (long)countUnreadMessages];
                    
                    CGRect frameTitle = cell.lbl_celltitle.frame;
                    frameTitle.origin.x = fDeviceWidth - 55;
                    UILabel *commentsCount;
                    if (countUnreadMessages < 10)
                        commentsCount = [[UILabel alloc]initWithFrame:CGRectMake(frameTitle.origin.x, 15, 20, 20)];
                    else if (countUnreadMessages < 100)
                        commentsCount = [[UILabel alloc]initWithFrame:CGRectMake(frameTitle.origin.x - 10, 12, 26, 26)];
                    else if (countUnreadMessages < 1000)
                    {
                        commentsCount = [[UILabel alloc]initWithFrame:CGRectMake(frameTitle.origin.x - 20, 10, 30, 30)];
                    }
                    
                    commentsCount.text = badgeValue;
                    commentsCount.textAlignment = NSTextAlignmentCenter;
                    commentsCount.textColor = [UIColor whiteColor];
                    commentsCount.backgroundColor = [UIColor redColor];
                    commentsCount.tag = 228899;
                    commentsCount.layer.cornerRadius = commentsCount.frame.size.width / 2.0;
                    commentsCount.layer.masksToBounds = true;
                    commentsCount.clipsToBounds = true;
                    [cell addSubview:commentsCount];
                    
                }
                else
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            else
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                
            }
            
        }
        
        
        //return cell;
        
    }
    
    cell.lbl_celltitle.font = [AppFont regularFont:17.0];
    
    return cell;
}



-(IBAction)openMoreTab:(NSDictionary*)Cellinfo
{
    
    
    /*  {
     "ACTION_URL" = "www.google.com";
     "ICON_URL" = "icon_language";
     TABNAME = GOOGLE;
     tabAction = Google;
     }
     */
    
    NSString *tabaction=[NSString stringWithFormat:@"%@",[Cellinfo valueForKey:@"tabAction"]];
    
    if ([tabaction isEqualToString:@"livechat"])
    {
        //open live chat
        [self LiveChatOpen];
    }
    else
    {
        //open webview with URL load
        
        NSString *url=[NSString stringWithFormat:@"%@",[Cellinfo valueForKey:@"ACTION_URL"]];
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        NSString *title=[NSString stringWithFormat:@"%@",[Cellinfo valueForKey:@"TABNAME"]];
        [self openFAQWebVC:url withTitle:title];
    }
}


-(void)openFAQWebVC:(NSString *)url withTitle:(NSString*)title
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)LiveChatOpen
{
    NSLog(@"My LiveChatOpen Action");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Chat" bundle:nil];
    LiveChatVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LiveChatVC"];
    
    
    vc.hidesBottomBarWhenPushed = YES;
    vc.comingFromString = @"MoreHelp";
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    if ([indexPath section]==0)
    {
        
        
        if (indexPath.row==0)//My profile
        {
            // Update Langugae Bundle
            [singleton traceEvents:@"Open My Profile" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

            [self myProfile_Action];

            
        }
        /*  if (indexPath.row==1)//Aadhar Card
         
         {
         if (singleton.objUserProfile.objAadhar.aadhar_number.length)
         {
         
         [self AadharCardViewCon];
         }
         else
         {
         [self NotLinkedAadharVC];
         
         }
         
         
         }
         */
        //if (indexPath.row==2)//My Digit Locker //socialmediaaccounts
        if (indexPath.row==1){
            [singleton traceEvents:@"Open Social Media Account" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

            [self socialMediaAccount];

        }
        
        //if (indexPath.row==3)//Transactional History
        if (indexPath.row==2)//Transactional History
        {
            [singleton traceEvents:@"Open Transaction History" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

            [self myTransHistory];

        }
        
        
//        BOOL includeServiceDir = [[[NSUserDefaults standardUserDefaults] valueForKey:@"Enable_ServiceDir"] boolValue];
        
        
        if (includeServiceDir)
        {
            //if (indexPath.row==4)//service directory
            if (indexPath.row==3)//service directory
                
            {
                [singleton traceEvents:@"Open Service Directory" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self ServiceDirectoryVC];

                
            }
            // if (indexPath.row==5)//My Digit Locker
            if (indexPath.row==4)//My Digit Locker
            {
                [singleton traceEvents:@"Open My DigiLocker" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self myDigiLock_Action];

                
            }
            
            if (indexPath.row==5)
                //if (indexPath.row==6)//Setting
            {
                [singleton traceEvents:@"Open Settings" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self mySetting_Action];

            }
        }
        else
        {
            if (indexPath.row==3)//My Digit Locker
                // if (indexPath.row==4)//My Digit Locker
            {
                [singleton traceEvents:@"Open My DigiLocker" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self myDigiLock_Action];

            }
            
            if (indexPath.row==4)//Setting
                //if (indexPath.row==5)//Setting
            {
                [singleton traceEvents:@"Open Settings" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self mySetting_Action];
            }
        }
        
        
        
    }
    
    else if ([indexPath section]==1)
    {
        
        //NSLog(@"tab selected=%@",[[tabarray valueForKey:@"URL"] objectAtIndex:indexPath.row]);
        
        [singleton traceEvents:@"Open More Tab" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

        NSDictionary *cellInfo=(NSDictionary*)[tabarray objectAtIndex:indexPath.row];
        [self openMoreTab:cellInfo];
    }
    
    else if ([indexPath section]==2)
    {
        
        /* if (indexPath.row==0) //livechat
         {
         [self LiveChatOpen];
         
         
         }*/
        
        
        if (isLiveChatexit==FALSE)
        {
            if (indexPath.row==0) //RateUs
            {
                [singleton traceEvents:@"Open Share Us" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self shareContent];
                
                
            }
            
            else if (indexPath.row==1) //RateUs
            {
                [singleton traceEvents:@"Open Rate Us" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self rateUsClicked];
                
            }
            
            
            
            else if (indexPath.row==2) //RateUs
            {
                [singleton traceEvents:@"Open Feedback" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self FeedbackVC];
                
            }
            else if (indexPath.row==3) //About Us
            {
                [singleton traceEvents:@"Open Help" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self myHelp_Action];
                
                
            }
            else if (indexPath.row==4) //Help
            {
                [singleton traceEvents:@"Open About Us" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self AboutInfoVC];
                
            }
            else if (indexPath.row==5) //Logout
            {
                
                [singleton traceEvents:@"Logout" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                UIAlertView *alertVw = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"logout_msg_txt", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"no", nil) otherButtonTitles:NSLocalizedString(@"yes", nil), nil];
                [alertVw show];
            }
            
        }
        else
        {
            if (indexPath.row==0) //RateUs
            {
                [singleton traceEvents:@"Open Share Us" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self shareContent];
                
                
            }
            
            else if (indexPath.row==1) //RateUs
            {
                [singleton traceEvents:@"Open Rate Us" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self rateUsClicked];
                
            }
            
            
            
            else if (indexPath.row==2) //feedback
            {
                [singleton traceEvents:@"Open Feedback" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self FeedbackVC];
                
            }
            else if (indexPath.row==3) //live chat
            {
                [singleton traceEvents:@"Open Live Chat" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self LiveChatOpen];
                
            }
            
            else if (indexPath.row==4)// Help
            {
                [singleton traceEvents:@"Open Help" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self myHelp_Action];
                
            }
            else if (indexPath.row==5) //About Us
            {
                [singleton traceEvents:@"Open About Us" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                [self AboutInfoVC];
                
                
            }
            
            else if (indexPath.row==6) //Logout
            {
                
                [singleton traceEvents:@"Logout" withAction:@"Clicked" withLabel:@"More Tab" andValue:0];

                UIAlertView *alertVw = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"logout_msg_txt", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"no", nil) otherButtonTitles:NSLocalizedString(@"yes", nil), nil];
                [alertVw show];
            }
            
        }
        
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)AboutInfoVC
{
    // SettingsViewController
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    AboutInfoVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AboutInfoVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
   
    
   /*
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Flagship" bundle:nil];
    
    AccountRecoveryVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AccountRecoveryVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    */
    
    

}


-(void)NotLinkedAadharVC
{
    // SettingsViewController
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NotLinkedAadharVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NotLinkedAadharVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        
        [self myLogout_Action];
    }
}




-(void)ServiceDirectoryVC
{
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"DetailService" bundle:nil];
    ServiceDirectoryVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ServiceDirectoryVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)AadharCardViewCon
{
    // SettingsViewController
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    AadharCardViewCon *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharCardViewCon"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)FeedbackVC
{
    
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
   // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FeedbackVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}


-(void)socialMediaAccount
{
    
    // Reset here for login with registration here
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLoginWithRegistration"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    SharedManager *singleton  = [SharedManager sharedSingleton];
    singleton.shared_mpinflag = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinflag"];
    singleton.shared_mpinmand = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinmand"];
    
    if ([[singleton.shared_mpinflag lowercaseString] isEqualToString:@"false"])
    {
        
        NSLog(@"MPIN SHOWING STATUS IS FALSE, so  show it");
        
        dispatch_async(dispatch_get_main_queue(),^{
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate showMPIN_AlertBox];
            [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
             delegate.vw_MPIN_AlertBox.lbl_subtitle1.text = NSLocalizedString(@"setting_mpin_mandatory_this_screen", nil);
        });
        /*
         NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
         [[NSUserDefaults standardUserDefaults]synchronize];
         if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]||[lastTime isEqualToString:@"NR"])
         {
         // dont show first time
         }
         else
         {
         // ========= check to open show mpin alert box=====
         dispatch_async(dispatch_get_main_queue(),^{
         AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
         [delegate showHideSetMpinAlertBox ];
         [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
         
         });
         }
         */
    }
    else
    {
        // do nothing
        NSLog(@"MPIN SHOWING STATUS IS TRUE, so dont show it");
        
        
    // SettingsViewController
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SocialMediaViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SocialMediaViewController"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)myProfile_Action
{
    NSLog(@"My Profile Action");
    
   
    // Reset here for login with registration here
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLoginWithRegistration"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    SharedManager *singleton  = [SharedManager sharedSingleton];
    singleton.shared_mpinflag = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinflag"];
    singleton.shared_mpinmand = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinmand"];
    
    if ([[singleton.shared_mpinflag lowercaseString] isEqualToString:@"false"])
    {
        
        NSLog(@"MPIN SHOWING STATUS IS FALSE, so  show it");
        
        dispatch_async(dispatch_get_main_queue(),^{
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate showMPIN_AlertBox];
            [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
             delegate.vw_MPIN_AlertBox.lbl_subtitle1.text = NSLocalizedString(@"setting_mpin_mandatory_this_screen", nil);
        });
        /*
         NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
         [[NSUserDefaults standardUserDefaults]synchronize];
         if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]||[lastTime isEqualToString:@"NR"])
         {
         // dont show first time
         }
         else
         {
         // ========= check to open show mpin alert box=====
         dispatch_async(dispatch_get_main_queue(),^{
         AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
         [delegate showHideSetMpinAlertBox ];
         [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
         
         });
         }
         */
    }
    else
    {
        // do nothing
        NSLog(@"MPIN SHOWING STATUS IS TRUE, so dont show it");
        
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
        
        ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
        
        vc.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:vc animated:YES];
        
        
    }
    
    
   
}

-(void)myTransHistory
{
    NSLog(@"My myTransHistory");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    TransactionalHistoryVC  *vc = [storyboard instantiateViewControllerWithIdentifier:@"TransactionalHistoryVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    //  [alert show];
    
    
    
}

-(void)shareContent
{
    NSString *textToShare =singleton.shareText;
    // NSURL *myWebsite = [NSURL URLWithString:@"http://www.umang.com/"];
    
    NSArray *objectsToShare = @[textToShare];
    //[[UIPasteboard generalPasteboard] setString:textToShare];
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
    /* UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
     
     //if iPhone
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
     [self presentViewController:controller animated:YES completion:nil];
     }
     //if iPad
     else {
     // Change Rect to position Popover
     UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
     [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
     }
     
     
     [self presentViewController:controller animated:YES completion:nil];
     
     */
    
}

-(void)myDigiLock_Action
{
    NSLog(@"My Digi locker Action");
    //        /*DigiLockerVC  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DigiLockerVC"];
    //        vc.hidesBottomBarWhenPushed = YES;
    //        [self.navigationController pushViewController:vc animated:YES];*/
    //        DigiLockerWebVC  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DigiLockerWebVC"];
    //        vc.hidesBottomBarWhenPushed = YES;
    //        [self.navigationController pushViewController:vc animated:YES];
    //    /*
    //     //open to load digilocker new
    //
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DigiLocker" bundle:nil];
    //    DigiLockLoginVC  *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockLoginVC"];
    //    vc.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:vc animated:YES];
    //    */
    
    /*  NSLog(@"My Digi Action");
     
     DigilockerMenuVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DigilockerMenuVC"];
     vc.hidesBottomBarWhenPushed = YES;
     [self.navigationController pushViewController:vc animated:YES];
     */
    
    
    //DigiLockCreateAccountVC
    
    //    DigiLockerWebVC  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DigiLockerWebVC"];
    //    vc.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:vc animated:YES];
    //
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
//
//     DigiLockerHomeVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockerHomeVC"];
//    vc.hidesBottomBarWhenPushed = YES;
//   // [self.navigationController pushViewController:vc animated:YES];
    
    //return;
    
    NSString *accessStr = [[[NSUserDefaults standardUserDefaults] valueForKey:@"AccessTokenDigi"] lowercaseString];
    NSString *dlink = [singleton.objUserProfile.objGeneral.dlink lowercaseString] ;
    if ((accessStr.length != 0 && [accessStr isEqualToString:@"y"]) || (dlink.length != 0 && [dlink isEqualToString:@"y"]))
    {
        
       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDigiLockerStoryBoard bundle:nil];

        DigiLockerHomeVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockerHomeVC"];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        DigiLockHomeVC *digiView = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockHomeVC"];
//
//        digiView.hidesBottomBarWhenPushed = YES;
//
//        [self.navigationController pushViewController:digiView animated:YES];
    }
    else
    {
        //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
        DigilockerMenuVC  *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigilockerMenuVC"];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
    
    
}
-(void)mySetting_Action
{
    NSLog(@"My Setting Action");
    // SettingsViewController/Users/admin/Downloads/StagingUmang1.0.73/Umang/Presentation Layer/ViewControllers/MoreTabVC.m
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}



-(void)myHelp_Action
{
    NSLog(@"My Help Action");
    
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)myLogout_Action
{
    NSLog(@"My Logout Action");
    
    
    NSString *jsonStringtoSave=[[NSUserDefaults standardUserDefaults] objectForKey:@"jsonStringtoSave"];;
    
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
    if([jsonStringtoSave length]!=0)
        
    {
        
        NSData *data = [jsonStringtoSave dataUsingEncoding:NSUTF8StringEncoding];
        
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSDictionary *epfoData_dic=[json valueForKey:@"epfoData"];
        NSDictionary *npsData_dic = [json valueForKey:@"npsData"];
        
        if (epfoData_dic != nil)
        {
            
            NSDictionary *value_dic=[epfoData_dic valueForKey:@"value"];
            NSString *uid=[value_dic valueForKey:@"uid"];
            //[self hitEPFOLogoutApiWithUAN:uid];
        }
        
        if (npsData_dic != nil)
        {
            
            NSDictionary *value_dic=[epfoData_dic valueForKey:@"value"];
            NSString *uid=[value_dic valueForKey:@"userid"];
            //[self hitNPSLogoutApiWithUserIdNps:uid];
        }
    }
    else
    {
        
    }
    
    [self hitAPI];
    
}


-(void)hitEPFOLogoutApiWithUAN:(NSString *)uanNumber
{
    
    NSLog(@"%@",singleton.user_id);
    
    if ([singleton.mobileNumber length]==0)
    {
        singleton.mobileNumber=@"";
    }
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:@"12" forKey:@"usag"];
    [dictBody setObject:@"12" forKey:@"usrid"];
    [dictBody setObject:@"12" forKey:@"srvid"];
    [dictBody setObject:@"app" forKey:@"pltfrm"];
    
    [dictBody setObject:uanNumber forKey:@"uan"];
    [dictBody setObject:@"2065" forKey:@"lac"];
    [dictBody setObject:@"" forKey:@"lat"];
    [dictBody setObject:@"" forKey:@"lon"];
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    
    [dictBody setObject:selectedLanguage forKey:@"lang"];
    
    [dictBody setObject:@"app" forKey:@"mode"];
    
    
    [objRequest hitWebServiceDeptAPIWithPostMethodForDept:@"epfo" andIsPost:YES isAccessTokenRequired:NO webServiceURL:UM_API_LOGOUT_EPFO withBody:dictBody andTag:TAG_REQUEST_LOGOUT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        [hud hideAnimated:YES];
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc = [response valueForKey:@"rc"];
            NSString *rs = [response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                //singleton.user_tkn=tkn
                
                
                NSLog(@"logout successful");
                
            }
            
        }
        else
        {
            
        }
        
        [self hitAPI];
        
    }];
    
}
-(void)hitNPSLogoutApiWithUserIdNps:(NSString *)userId
{
    if ([singleton.mobileNumber length]==0)
    {
        singleton.mobileNumber=@"";
    }
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:@"12" forKey:@"usag"];
    [dictBody setObject:@"12" forKey:@"usrid"];
    [dictBody setObject:@"12" forKey:@"srvid"];
    [dictBody setObject:@"app" forKey:@"pltfrm"];
    
    [dictBody setObject:@"2065" forKey:@"lac"];
    [dictBody setObject:@"" forKey:@"lat"];
    [dictBody setObject:@"" forKey:@"lon"];
    [dictBody setObject:@"1234567890" forKey:@"imei"];
    
    
    
    [dictBody setObject:@"12" forKey:@"did"];
    [dictBody setObject:@"ios" forKey:@"os"];
    [dictBody setObject:@"" forKey:@"acesstkn"];
    [dictBody setObject:userId forKey:@"userid"];
    [dictBody setObject:@"10.0" forKey:@"maver"];
    [dictBody setObject:@"10.0" forKey:@"mosver"];
    [dictBody setObject:singleton.user_id forKey:@"uid"];
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    
    [dictBody setObject:selectedLanguage forKey:@"lang"];
    
    [dictBody setObject:@"app" forKey:@"mode"];
    
    
    
    [objRequest hitWebServiceDeptAPIWithPostMethodForDept:@"nps" andIsPost:YES isAccessTokenRequired:NO webServiceURL:UM_API_LOGOUT_NPS withBody:dictBody andTag:TAG_REQUEST_LOGOUT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                //singleton.user_tkn=tkn;
                
            }
            
        }
        else
        {
            
        }
        
        [self hitAPI];
        
        
    }];
    
}

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    
    
    if ([singleton.mobileNumber length]==0)
    {
        singleton.mobileNumber=@"";
    }
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"status"];//This is Status of the account
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LOGOUT withBody:dictBody andTag:TAG_REQUEST_LOGOUT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);

            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                //singleton.user_tkn=tkn;
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setBool:NO forKey:@"LOGIN_KEY"];
                //[defaults setValue:@"" forKey:@"TOKEN_KEY"];
                
                [defaults synchronize];
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
                [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                //——Remove Sharding Value——
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"jsonStringtoSave"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //----- remove sharding--------
                
                
                
                
                singleton.profileUserAddress = @"";
                singleton.stateSelected = @"";
                singleton.imageLocalpath=@"";
                singleton.notiTypeGenderSelected=@"";
                singleton.profileNameSelected =@"";
                singleton.profilestateSelected=@"";
                singleton.notiTypeCitySelected=@"";
                singleton.notiTypDistricteSelected=@"";
                singleton.profileDOBSelected=@"";
                singleton.altermobileNumber=@"";
                singleton.user_Qualification=@"";
                singleton.user_Occupation=@"";
                singleton.user_profile_URL=@"";
                singleton.profileEmailSelected=@"";
                singleton.mobileNumber=@"";
                singleton.user_id=@"";
                singleton.user_tkn=@"";
                singleton.user_mpin=@"";
                singleton.user_aadhar_number=@"";
                singleton.objUserProfile = nil;
                
                singleton.imageLocalpath=@"";
                singleton.stateSelected = @"";
                
                //added infor tab
                NSString *infotab=[NSString stringWithFormat:@"%@",[singleton.arr_initResponse valueForKey:@"infotab"]];
                singleton.infoTab = infotab;
                
                singleton.arr_initResponse = nil;
                
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"AccessTokenDigi"];
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"RefreshTokenDigi"];
                //[[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"dialogueID"];
                
                [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:[NSString stringWithFormat:@"userdisconnected~~~%@~~~exit",singleton.user_tkn] toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"lastFetchV1"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"AllTabState"];
                
                [[[AppDelegate sharedDelegate] xmppHandler] teardownStream];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Enable_ServiceDir"];
                
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastDirectoryFetchDate"];
                
                //delete service directory
                 [singleton.dbManager deleteServicesDirectory];
                
                @try {
                    [singleton.arr_recent_service removeAllObjects];
                    //Blank recent service from NSUserDefaults

                    [[NSUserDefaults standardUserDefaults] setObject: [NSKeyedArchiver archivedDataWithRootObject:singleton.arr_recent_service] forKey:@"recentServicesKey"];

                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                
                [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
                
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_ID"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
                
                //------------------------- Encrypt Value------------------------
                
                NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                NSHTTPCookie *cookie;
                for (cookie in [storage cookies])
                {
                    
                    [storage deleteCookie:cookie];
                    
                }
                NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
                [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                //——Remove Sharding Value——
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //----- remove sharding--------
                
                
                
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"EMB_STR"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"InitAPIResponse"];
                
                // Logout from social frameworks as well.
                SocialAuthentication *objSocial = [[SocialAuthentication alloc] init];
                [objSocial logoutFromAllSocialFramewors];
                objSocial = nil;
                
                
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //------------------------- Encrypt Value------------------------
                
                
                
                
                //[singleton.dbManager  deleteAllNotifications];
                [singleton.dbManager deleteBannerHomeData];
                [singleton.dbManager  deleteAllServices];
                [singleton.dbManager  deleteSectionData];
                [singleton.dbManager deleteBannerStateData];
                
                
                //[[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SELECTED_TAB_INDEX"];
                //[[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"]
                [self deleteCache];
                [singleton setStateId:@""];
                
                [defaults synchronize];
                
                //[self removePrefrence];
                
                //added new method to clear values
                [singleton.dbManager clearSingletonProfileValue];
                
                
                [self alertwithMsg:rd];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}

-(void)deleteCache {
    NSFileManager *filemgr;
    
    filemgr = [NSFileManager defaultManager];
    
    if ([filemgr removeItemAtPath: [NSHomeDirectory() stringByAppendingString:@"/Documents/OfflineCache"] error: NULL]  == YES)
        NSLog (@"Remove successful");
    else
        NSLog (@"Remove failed");
    
    
}

-(void)removePrefrence
{
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}



-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}


-(void)alertwithMsg:(NSString*)msg
{
    singleton.imageLocalpath=@"";
    
    [self showToast:NSLocalizedString(@"logout_succesful", nil)];
    
    [self openNextView];
    
    /*  UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"logout", nil) message:NSLocalizedString(@"logout_succesful", nil) preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
     {
     
     [self openNextView];
     }];
     [alert addAction:cancelAction];
     [self presentViewController:alert animated:YES completion:nil];*/
}




-(void)openNextView
{
    
    //singleton=[SharedManager sharedSingleton];
    
    singleton.dbManager = [[UMSqliteManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    
    [singleton.dbManager createUmangDB];
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [singleton.reach stopNotifier];
    /*  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     LoginViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     [self presentViewController:vc animated:YES completion:nil];
     */
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"LoginStoryboard" bundle:nil];
    //UIStoryboard *storyboard = [self grabStoryboard];
    LoginAppVC *vc;
    //----later add
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:kKeepMeLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------
    
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
//
//    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
//    }
//    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//    appDel.window.rootViewController = vc;
//    // appDelegate.shouldRotate = NO;
//    [self presentViewController:vc animated:YES completion:nil];
    
    
    [self callFlagShipHomeApi];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate setFlagshipTabBarVC:@"NO"];

    
}

-(void)callFlagShipHomeApi {
    CommanAPI *common = [CommanAPI sharedSingleton];
    [common hitInitAPI];
    
}

- (void)moreBtnPressed:(UIButton*)sender{
}
/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
 cell.textLabel.text = @"DEDE me";
 cell.detailTextLabel.text = @"Please...";
 return cell;
 }
 */

- (CGFloat)horizontalOffset{
    return 50.0f;
}

- (void)didTapHeaderImageView:(UIImageView *)imageView
{
    NSLog(@"The header imageview was tapped: %@", imageView.description);
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert", nil) message:@"Do You want to Edit Profile?" delegate:nil cancelButtonTitle:NSLocalizedString(@"yes", nil) otherButtonTitles:NSLocalizedString(@"no", nil) , nil] show];
    
}

/*- (UIImage *)blurredImageWithImage:(UIImage *)sourceImage{
 
 //  Create our blurred image
 CIContext *context = [CIContext contextWithOptions:nil];
 CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
 
 //  Setting up Gaussian Blur
 CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
 [filter setValue:inputImage forKey:kCIInputImageKey];
 [filter setValue:[NSNumber numberWithFloat:8.0f] forKey:@"inputRadius"];
 CIImage *result = [filter valueForKey:kCIOutputImageKey];
 
 //  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
 // up exactly to the bounds of our original image
 CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
 
 UIImage *retVal = [UIImage imageWithCGImage:cgImage];
 return retVal;
 }*/

//----- hitAPI for IVR OTP call Type registration ------
/*-(void)hitFetchProfileAPI
 {
 // hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 // Set the label text.
 // hud.label.text = NSLocalizedString(@"Please wait...", @"Please wait...");
 NSString *mnum=singleton.mobileNumber;
 
 if ([mnum length]==0) {
 mnum=@"";
 }
 UMAPIManager *objRequest = [[UMAPIManager alloc] init];
 NSMutableDictionary *dictBody = [NSMutableDictionary new];
 [dictBody setObject:mnum forKey:@"mno"];//Enter mobile number of user
 [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
 [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
 [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
 [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
 [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
 
 
 
 
 [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
 // [hud hideAnimated:YES];
 if (error == nil) {
 NSLog(@"Server Response = %@",response);
 
 //----- below value need to be forword to next view according to requirement after checking Android apk-----
 
 NSString *rc=[response valueForKey:@"rc"];
 NSString *rs=[response valueForKey:@"rs"];
 
 //----- End value need to be forword to next view according to requirement after checking Android apk-----
 NSString *rd=[response valueForKey:@"rd"];
 
 
 
 
 if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
 
 {
 
 //  singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
 
 //refreshData
 
 
 singleton.user_profile_URL=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"pic"];
 NSLog(@"singleton.user_profile_URL =%@ ",singleton.user_profile_URL);
 
 
 singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
 
 if ([singleton.user_id length]==0) {
 singleton.user_id=@"";
 }
 // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 
 //[defaults setObject:singleton.user_id forKey:@"USER_ID"];
 
 // [defaults synchronize];
 //------------------------- Encrypt Value------------------------
 [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
 // Encrypt
 [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
 [[NSUserDefaults standardUserDefaults] synchronize];
 //------------------------- Encrypt Value------------------------
 
 
 
 singleton.mobileNumber =[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"mno"];
 
 [self refreshData];
 
 
 if ([[response objectForKey:@"socialpd"] isKindOfClass:[NSDictionary class]]) {
 singleton.objUserProfile = nil;
 singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"socialpd"]];
 }
 if ([[response objectForKey:@"pd"] isKindOfClass:[NSDictionary class]]) {
 singleton.objUserProfile = nil;
 singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
 }
 
 
 
 }
 
 }
 else{
 NSLog(@"Error Occured = %@",error.localizedDescription);
 
 }
 
 }];
 
 }
 
 */



//----- hitAPI for IVR OTP call Type registration ------
-(void)hitFetchProfileAPI
{
    //hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    // hud.label.text = NSLocalizedString(@"Please wait...", @"Please wait...");
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        // [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            
            NSString *responseStr=[NSString stringWithFormat:@"%@",response];
            if ([responseStr length]>0)
            {
                
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    
                {
                    
                    @try {
                        
                        
                        //singleton.user_tkn=tkn;
                        NSMutableArray *aadharpd=[[NSMutableArray alloc]init];
                        aadharpd=[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];
                        
                        
                        NSMutableArray *generalpd=[[NSMutableArray alloc]init];
                        generalpd=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                        
                        
                        socialpd=[[NSMutableArray alloc]init];
                        
                        singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                        
                        NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                        
                        if ([abbreviation length]==0)
                        {
                            abbreviation=@"";
                        }
                        
                        NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                        
                        emblemString = emblemString.length == 0 ? @"":emblemString;
                        
                        [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                        [singleton setStateId:singleton.user_StateId];
                        
                        //-------- Add later----------
                        singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                        singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                        //-------- Add later----------
                        
                        
                        
                        if ([singleton.user_id length]==0) {
                            singleton.user_id=@"";
                        }
                        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        
                        // [defaults setObject:singleton.user_id forKey:@"USER_ID"];
                        
                        //[defaults synchronize];
                        
                        
                        //------------------------- Encrypt Value------------------------
                        
                        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                        // Encrypt
                        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        //------------------------- Encrypt Value------------------------
                        
                        
                        //NSString *identifier =( [[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        socialpd=(NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        
                        
                        
                        //----- local values  to be save and show from API-----------
                        
                        
                        
                        //NSString * sid=[generalpd valueForKey:@"sid"];
                        
                        str_name=[generalpd valueForKey:@"nam"];
                        str_registerMb =[generalpd valueForKey:@"mno"];
                        str_alternateMb=[generalpd valueForKey:@"amno"];
                        //str_city=[generalpd valueForKey:@"cty"];
                        str_state=[generalpd valueForKey:@"st"];
                        
                        // state_id=[obj getStateCode:str_state];
                        
                        
                        str_district=[generalpd valueForKey:@"dist"];
                        str_dob=[generalpd valueForKey:@"dob"];
                        str_gender=[generalpd valueForKey:@"gndr"];
                        str_emailAddress=[generalpd valueForKey:@"email"];
                        str_emailVerifyStatus=[generalpd valueForKey:@"emails"];
                        str_amnosVerifyStatus=[generalpd valueForKey:@"amnos"];
                        str_Url_pic=[generalpd valueForKey:@"pic"];
                        str_address=[generalpd valueForKey:@"addr"];
                        
                        NSLog(@"str_gender=%@",str_gender);
                        
                        /* if (str_gender.length>0) {
                         _tblUserProfile.tableHeaderView =  [self designHeaderView];
                         }
                         */
                        // NSString *quali_id=[generalpd valueForKey:@"qual"];
                        // NSString *Occu_id=[generalpd valueForKey:@"occup"];
                        
                        
                        // str_qualification=[obj getqualName:quali_id];
                        // str_occupation=[obj getOccuptname:Occu_id];
                        
                        NSLog(@"str_Url_pic  =>>> %@",str_Url_pic);
                        
                        
                        
                        // if ([str_Url_pic length]!=0 ) {
                        
                        singleton.user_profile_URL=str_Url_pic;
                        // }
                        
                        /*
                         "gcmid": "",
                         "opid": "",
                         "refid": "",
                         "tkn": "",
                         */
                        
                        if ([[response objectForKey:@"socialpd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"socialpd"]];
                        }
                        if ([[response objectForKey:@"pd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                        }
                        singleton.mobileNumber =[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"mno"];
                        
                        
                        [self setProfileData];
                        
                        [self refreshData];
                        
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                }
            }
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];
             */
            [self setProfileData];
            
        }
        
    }];
    
}

-(NSString *)UpperFirstWord:(NSString*)inputString
{
    
    
    if ([inputString length]!=0) {
        inputString = [inputString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[inputString substringToIndex:1] uppercaseString]];
    }
    return inputString;
}


-(void)setProfileData
{
    
    //----- local values  to be save and show from API-----------
    
    
    str_name=[self UpperFirstWord:str_name];
    // str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    
    
    
    
    //if ([str_Url_pic length]!=0 ) {
    
    singleton.user_profile_URL=str_Url_pic;
    //  }
    
    
    
    
    
    
    if ([str_gender length]!=0) {
        str_gender=[str_gender uppercaseString];
        
        if ([str_gender isEqualToString:@"M"] ||[str_gender isEqualToString:@"MALE"]) {
            stringGender= NSLocalizedString(@"gender_male", nil);
            singleton.notiTypeGenderSelected= NSLocalizedString(@"gender_male", nil);
            
        }
        else  if ([str_gender isEqualToString:@"F"]|| [str_gender isEqualToString:@"FEMALE"]) {
            singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_female", nil);
            stringGender=NSLocalizedString(@"gender_female", nil);
            
        }
        else  if ([str_gender isEqualToString:@"T"]||[str_gender isEqualToString:@"t"]) {
            singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_transgender", nil);
            stringGender=NSLocalizedString(@"gender_transgender", nil);
            
        }
        
        else
        {
            singleton.notiTypeGenderSelected=@"";
            stringGender=@"";
        }
        
        
    }
    
    str_name=[self UpperFirstWord:str_name];
    //str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    //singleton.notiTypeGenderSelected
    singleton.profileNameSelected =str_name;
    singleton.profilestateSelected=str_state;
    //singleton.stateSelected = str_state;
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=str_district;
    singleton.profileDOBSelected=str_dob;
    singleton.altermobileNumber=str_alternateMb;
    singleton.user_Qualification=str_qualification;
    singleton.user_Occupation=str_occupation;
    singleton.profileEmailSelected=str_emailAddress;
    singleton.profileUserAddress=str_address;
    
    NSLog(@"str_gender=%@",str_gender);
    if ([str_gender isEqualToString:@"M"]) {
        
        stringGender=NSLocalizedString(@"gender_male", nil);
        
    }
    else if ([str_gender isEqualToString:@"F"]) {
        
        stringGender=NSLocalizedString(@"gender_female", nil);
        
        
    }
    else if ([str_gender isEqualToString:@"T"])
    {
        stringGender=NSLocalizedString(@"gender_transgender", nil);
        
        
        
    }
    else {
        stringGender=@"";
        
        
        
    }
    
    
    // [self loadHtml];
    
    
    
    
}

@end

