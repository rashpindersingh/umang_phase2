//
//  FavouriteTabVC.m
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "FavouriteTabVC.h"
#import "FavouriteCell.h"
#import "ScrollNotificationVC.h"
#import "NotificationFilterVC.h"

#import "AdvanceSearchVC.h"
#import "UIImageView+WebCache.h"
#import "HomeDetailVC.h"
#import "DetailServiceNewVC.h"

#import "AddFavFilterVC.h"
#import "HomeTabVC.h"
#import "UserProfileVC.h"
#import "ShowUserProfileVC.h"

#import "MBProgressHUD.h"
#import "UIView+MGBadgeView.h"

#import "CustomBadge.h"
#import "BadgeStyle.h"

#import "StateList.h"
#import "RunOnMainThread.h"


static float NV_height= 50;
static const CGSize progressViewSize = { 95.0f, 25.0f };

@interface FavouriteTabVC ()<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    SharedManager *singleton;
    __weak IBOutlet UIView *vwSearchBG;
    
    BOOL flagrotation;
    
    
    
    
    BOOL flagStatusBar;
    
    MBProgressHUD *hud ;
    
    IBOutlet UIButton *btn_notification;
    
    StateList *obj;
    NavigationSearchView *nvSearchView ;

}
@property(nonatomic,retain)IBOutlet UITableView *table_favView;

@property(nonatomic,retain)NSArray *sampleData ;
@property(nonatomic,retain)NSMutableArray *tableData ;
@property (weak, nonatomic) IBOutlet UILabel *lbl_favourite;

@property (strong, nonatomic) NSArray *collectionData;
@property(nonatomic,retain)NSDictionary *cellData;
@property (weak, nonatomic) IBOutlet UILabel *txtNoFav;
@property(nonatomic,retain)NSDictionary *cellDataOfmore;


@property (nonatomic, strong) NSString *profileComplete;

@property (nonatomic) CGFloat progress;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSArray *progressViews;





@property (nonatomic, strong)UIImageView *imgProfile;
@property (nonatomic, strong)UILabel *lbl_profileComplete;
@property (nonatomic, strong)UILabel *lbl_whiteLine;
@property (nonatomic, strong)UILabel *lbl_percentage;
@property (nonatomic, strong)UIButton *btn_close;
@property (nonatomic, strong)UIButton *btn_clickUpdate;

@property (nonatomic, strong) UIView *NotifycontainerView;
@end

@implementation FavouriteTabVC
@synthesize table_favView;
@synthesize tableData;
@synthesize profileComplete;

@synthesize imgProfile;
@synthesize lbl_profileComplete;
@synthesize lbl_whiteLine;
@synthesize lbl_percentage;
@synthesize btn_close;
@synthesize btn_clickUpdate;
@synthesize NotifycontainerView;


-(void)fetchData
{
    
    UITabBarController *tab=self.tabBarController;
    
    if (tab){
        NSLog(@"I have a tab bar");
       // [self.tabBarController setSelectedIndex:1];
        [self.tabBarController setSelectedIndex:singleton.tabSelectedIndex];

    }
    else{
        NSLog(@"I don't have");
    }
    
    NSInteger opentabIndex=self.tabBarController.selectedIndex;
    NSLog(@"opentabIndex=%ld",(long)opentabIndex);
    //------------ Handling loading service if user logout -----------------
    NSInteger currentIndexOfTab=[singleton getSelectedTabIndex];
    
    if (currentIndexOfTab==opentabIndex)
    {
        //vw_noServiceFound.alpha=1;
        [self fetchDatafromDB];
        [self addNotify];
    }
    [hud hideAnimated:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FETCHHOMEDATA" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchData)
                                                 name:@"FETCHHOMEDATA" object:nil];
    
    
}





-(void)tableEmptyCheckData
{
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSLog(@"=========>hurrah called before and after  add value here as well===>%@",singleton.loadServiceStatus);
        if ([singleton.loadServiceStatus isEqualToString:@"TRUE"])
        {
            
            // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CHECKTABLEEMPTYORNOT" object:nil];
            
            [self checkServiceStatus];
            
            
        }
        if ([singleton.loadServiceStatus isEqualToString:@"FALSE"])
        {
            //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"CHECKTABLEEMPTYORNOT" object:nil];
            [self checkServiceStatus];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [table_favView reloadData];
        });
        
        
        
    });
    
    
}

-(void)callStateAPI
{
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    
    
}

-(void)profileCheck
{
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [self addNotify];
    }];
    
    
}

- (void)viewDidLoad
{
    if (iPhoneX())
    {
        
        nvSearchView = [[NavigationSearchView alloc] init];
        NSLog(@"An iPhone X Load UI");
    }
    [super viewDidLoad];
    
    singleton = [SharedManager sharedSingleton];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableEmptyCheckData)                                                 name:@"CHECKTABLEEMPTYORNOT" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileCheck)
                                                 name:@"PROFILECOMPLETE" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadAfterDelay)
                                                 name:@"ReloadEverything" object:nil];;
    
    flagStatusBar=FALSE;
    
    
    // vw_noServiceFound.alpha=0;
    vw_noServiceFound.hidden=TRUE;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchData)
                                                 name:@"FETCHHOMEDATA" object:nil];
    
    
    
    NSLog(@"singleton.user_tkn=%@",singleton.user_tkn);
    //NSLog(@"singleton.user_tkn=%@",[defaults stringForKey:@"USER_ID"]);
    
    
    _txtNoFav.text = NSLocalizedString(@"no_favourites", nil);
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:FAVORITE_TAB_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    
    [txt_searchField setValue:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    txt_searchField.placeholder = NSLocalizedString(@"search", @"");
    
    vwSearchBG.layer.cornerRadius = 5.0;
    //self.view.backgroundColor=[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0];
    
    self.view.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    table_favView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.hidden = YES;
    
    
    table_favView.contentInset = UIEdgeInsetsMake(-1.0f, 0.0f, 0.0f, 0.0);
    vw_line.frame=CGRectMake(0, 74,fDeviceWidth, 0.5);
    vw_noServiceFound.backgroundColor=[UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0f];
    
    refreshController = [[UIRefreshControl alloc] init];
    //refreshController.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refreshController addTarget:self action:@selector(getEvents:) forControlEvents:UIControlEventValueChanged];
    //self.refreshController = refresh;
    [table_favView addSubview:refreshController];
    [self getEvents:refreshController];
    
    txt_searchField.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    [self callStateAPI];
    //[self hitAPI];
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self adjustSearchBarView];
    }
    
    [self addNavigationSearchTabView];
    
}

#pragma mark- add Navigation View to View

-(void)addNavigationSearchTabView{
    
    if (iPhoneX())
    {
        NSLog(@"An iPhone X Load UI");
        
        //NavigationSearchView *nvSearchView = [[NavigationSearchView alloc] init];
        __weak typeof(self) weakSelf = self;
        vw_line.hidden=TRUE;
        
        nvSearchView.didTapFilterBarButton= ^(id btnfilter)
        {
            
            [weakSelf btn_filterAction:btnfilter];
            
        };
        
        
        nvSearchView.didTapNotificationBarButton= ^(id btnNotification)
        {
            
            [weakSelf btn_noticationAction:btnNotification];
            
        };
        
        nvSearchView.didTapSearchTextfield= ^(id txtSearchView)
        {
            
            [weakSelf openSearch];
            
            
        };
        
        [self.view addSubview:nvSearchView];
        
        CGRect table = table_favView.frame;
        table.origin.y = kiPhoneXNaviHeight4Search;
        table.size.height = fDeviceHeight - kiPhoneXNaviHeight4Search;
        table_favView.frame = table;
        [self.view layoutIfNeeded];
        
        
    }
    else
    {
        NSLog(@"Not an iPhone X use default UI");
    }
    
}

-(void)adjustSearchBarView
{
    
    vwSearchBG.frame = CGRectMake(self.view.frame.size.width/2 - 150, vwSearchBG.frame.origin.y, 300, vwSearchBG.frame.size.height);
    
    searchIconImage.frame = CGRectMake(vwSearchBG.frame.origin.x + 7, searchIconImage.frame.origin.y, searchIconImage.frame.size.width, searchIconImage.frame.size.height);
    
    txt_searchField.frame = CGRectMake(searchIconImage.frame.origin.x + searchIconImage.frame.size.width + 5, txt_searchField.frame.origin.y, 265, txt_searchField.frame.size.height);
    
}


//==================================
//       CUSTOM PICKER STARTS
//==================================
- (void)btnOpenSheet

{
    NSString *information=NSLocalizedString(@"information", nil);
    NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [singleton traceEvents:@"Department Information" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
            
            [self callDetailServiceVC];
        }
            break;
        case 1:
        {
            NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
            
            [singleton traceEvents:@"View On Map" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
            
            NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
            
            NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
            
            NSString *deptName=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_NAME"]];
            deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]])
                
            {
                NSLog(@"Map App Found");
                
                
                NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
                
                // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
                
                NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSLog(@"mapURL= %@",mapURL);
                
                [[UIApplication sharedApplication] openURL:mapURL];
                
            } else
            {
                NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
                
            }
            
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

//==================================
//          CUSTOM PICKER ENDS
//==================================

- (IBAction)btnMoreInfoClicked:(UIButton *)sender
{
    
    
    self.cellData = (NSDictionary*)[tableData objectAtIndex:[sender tag]];
    
    
    NSLog(@"Data 1 = %@",self.cellData);
    
    self.cellDataOfmore=(NSMutableDictionary*)self.cellData;
    
    [singleton traceEvents:@"More Button" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
    
    [self btnOpenSheet];
    
    
    
    /*  NSString *titlename=[NSString stringWithFormat:@"%@",[self.cellData objectForKey:@"SERVICE_NAME"]];
     
     
     UIAlertController * alert=   [UIAlertController
     alertControllerWithTitle:titlename
     message:nil
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* info = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"information", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     //[self displayContentController:[self getHomeDetailLayerLeftController]];
     [self callDetailServiceVC];
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* map = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"view_on_map", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     
     NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellData  valueForKey:@"SERVICE_LONGITUDE"]]);
     
     NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellData valueForKey:@"SERVICE_LATITUDE"]];
     
     NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellData  valueForKey:@"SERVICE_LONGITUDE"]];
     
     
     // NSString* googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@",latitude, longitute];
     
     
     
     // [[UIApplication sharedApplication] openURL: [NSURL URLWithString: googleMapsURLString]];
     
     
     if ([[UIApplication sharedApplication] canOpenURL:
     [NSURL URLWithString:@"comgooglemaps://"]])
     
     {
     NSLog(@"Map App Found");
     
     NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f&center=%.6f,%.6f&zoom=15&views=traffic", [latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     
     
     
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
     
     
     } else
     {
     NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
     
     }
     
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     
     // [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* cancel = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"cancel", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     
     
     [alert addAction:info];
     [alert addAction:map];
     
     [alert addAction:cancel];
     
     
     UIViewController *vc=[self topMostController];
     [vc presentViewController:alert animated:NO completion:nil];
     
     */
    
}
-(void)callDetailServiceVC
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    vc.dic_serviceInfo=self.cellData;//change it to URL on demand
    
    UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [topvc presentViewController:vc animated:NO completion:nil];
    
}

//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (void)getEvents:(UIRefreshControl *)refresh
{
    static BOOL refreshInProgress = NO;
    
    if (!refreshInProgress)
    {
        refreshInProgress = YES;
        
        // refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing"]; // let the user know refresh is in progress
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            // get the data here
            [self fetchDatafromDB];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // when done, update the model and the UI here
                
                // refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"]; // reset the message
                [refresh endRefreshing];
                
                refreshInProgress = NO;
            });
        });
    }
}




- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001f;
}

- (NSString*) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//
//    return 0.001;
//}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    //------------- Network View Handle------------
    [super viewWillAppear: animated];
    [self fetchDatafromDB];
    flagrotation=TRUE;
    
    
    
    [self addNotify];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    //------------- Tab bar Handle------------
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    //appDelegate.shouldRotate = YES;
    // [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];
    //------------- Tab bar Handle------------
    
    
    //==== Rashpinder  Changes =====
    // table_favView.layer.borderWidth=0.5;
    // table_favView.layer.borderColor= [UIColor colorWithRed:189/255.0 green:188/255.0 blue:193/255.0 alpha:1.0].CGColor;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    //    [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:NSLocalizedString(@"home_small", @"")];
    //
    //    [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:NSLocalizedString(@"favourites_small", @"")];
    //
    //    [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:NSLocalizedString(@"all_services_small", @"")];
    //    [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:NSLocalizedString(@"help_live_chat", @"")];
    //
    //
    //    [[self.tabBarController.tabBar.items objectAtIndex:4] setTitle:NSLocalizedString(@"more", @"")];
    
    
    
    /*NSInteger fontIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_FONTSIZE_INDEX"];
     
     if (fontIndex==0)
     {
     CGFloat fontsize=14.0;
     [[UILabel appearance] setFont:[UIFont systemFontOfSize:fontsize]];
     
     
     }
     if (fontIndex==1)
     {
     CGFloat fontsize=16.0;
     [[UILabel appearance] setFont:[UIFont systemFontOfSize:fontsize]];
     
     }
     if (fontIndex==2)
     {
     CGFloat fontsize=18.0;
     [[UILabel appearance] setFont:[UIFont systemFontOfSize:fontsize]];
     
     }*/
    
    appDel.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //-------- Added later-------------
    [self badgeHandling];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(badgeHandling) name:@"NotificationRecievedComplete" object:nil];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    self.tabBarController.tabBar.itemPositioning = UITabBarItemPositioningCentered;
    
    [self setViewFont];
}

#pragma mark- Font Set to View
-(void)setViewFont{
    
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    txt_searchField.font = [AppFont mediumFont:13.0];
    noFavouriteFoundLabel.font = [AppFont mediumFont:18.0];
    toAddFavouriteLabel.font = [AppFont regularFont:15.0];
    
}
#pragma mark -

-(void)badgeHandling
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //open for testing purpose
    // NSString *badge=[NSString stringWithFormat:@"%d",1];
    //[[NSUserDefaults standardUserDefaults] setObject:badge forKey:@"BadgeValue"];
    
    app.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"appDelegate.badgeCount=%d",app.badgeCount);
    NSString*badgeValue=[NSString stringWithFormat:@"%d",app.badgeCount];
    
    if (app.badgeCount>0)
    {
        
        if (app.badgeCount>=10)      // 3 and 2 digit digit case
        {
            CustomBadge *badge = [CustomBadge customBadgeWithString:badgeValue withScale:.9];
            CGSize size = CGSizeMake(badge.frame.size.width, badge.frame.size.height);
            
            //CGPoint point = CGPointMake(btn_notification.frame.size.width+5-badge.frame.size.width, -14);
            CGPoint  point = [UIScreen mainScreen].bounds.size.height == 812.0 ? CGPointMake(nvSearchView.notificationBarButton.frame.size.width+5-badge.frame.size.width, -14) :  CGPointMake(btn_notification.frame.size.width+5-badge.frame.size.width, -14);
            
            CGRect rect = CGRectMake(point.x, point.y, size.width, size.height+8);
            [badge setFrame:rect];
            badge.userInteractionEnabled=NO;
            badge.tag=786;
            
            //  [btn_notification addSubview:badge];
            
            
            [([UIScreen mainScreen].bounds.size.height == 812.0 ? nvSearchView.notificationBarButton : btn_notification) addSubview:badge];
            
        }
        else
        {
            CustomBadge * badge = [CustomBadge customBadgeWithString:badgeValue];
            //CGPoint point = CGPointMake(btn_notification.frame.size.width/2+8-badge.frame.size.width/2, -8);
            
            CGPoint  point = [UIScreen mainScreen].bounds.size.height == 812.0 ? CGPointMake(nvSearchView.notificationBarButton.frame.size.width/2+8-badge.frame.size.width/2, -8) : CGPointMake(btn_notification.frame.size.width/2+8-badge.frame.size.width/2, -8);
            
            CGRect rect = CGRectMake(point.x, point.y, 21, 21);
            [badge setFrame:rect];
            badge.userInteractionEnabled=NO;
            badge.tag=786;
            // [btn_notification addSubview:badge];
            [([UIScreen mainScreen].bounds.size.height == 812.0 ? nvSearchView.notificationBarButton : btn_notification) addSubview:badge];
            
            
        }
        
        
        
    }
    else
    {
        // for (UIView *subview in [btn_notification subviews])
        for (UIView *subview in [([UIScreen mainScreen].bounds.size.height == 812.0 ? nvSearchView.notificationBarButton : btn_notification) subviews])
            
        {
            if (subview.tag ==786) {
                [subview removeFromSuperview];
            }
            
        }
        
        
    }
    
    
}












/*
 
 - (void)viewDidLayoutSubviews
 {
 [super viewDidLayoutSubviews];
 dispatch_async(dispatch_get_main_queue(), ^{
 
 
 if ( flagrotation==TRUE) {
 
 NSString* showProfilestatus =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"SHOW_PROFILEBAR"];//[NSString stringWithFormat:@"%@", profileComplete];
 if ([showProfilestatus isEqualToString:@"YES"])
 {
 
 CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
 
 table_favView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-50);
 
 
 }
 
 flagrotation=FALSE;
 
 }
 
 
 
 
 });
 }
 */
-(void)reloadAfterDelay
{
    singleton = [SharedManager sharedSingleton];
    
    tableData=[[NSMutableArray alloc]init];
    
    
    //NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
    NSArray *arrServiceData=[singleton.dbManager loadAllDataServiceDataIncludeFlag];

    NSLog(@"arrServiceData=%@",arrServiceData);
    
    
    for (int i=0; i<[arrServiceData count]; i++) {
        
        NSDictionary *oldDict = (NSDictionary *)[arrServiceData objectAtIndex:i];
        
        //[temp replaceObjectAtIndex:0 withObject:newDict];
        
        NSString *favStatus = [oldDict objectForKey:@"SERVICE_IS_FAV"]; //test the user exist
        if([favStatus isEqualToString:@"true"])                   //checking user exist or not
        {
            
            NSLog(@"Not Selected!");
            @try {
                if ([tableData containsObject:[arrServiceData objectAtIndex:i]]) // YES
                {
                    NSLog(@"contain element");
                }
                else
                {
                    [tableData addObject:[arrServiceData objectAtIndex:i]];
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
        }
        else
        {
            
            
            NSLog(@"Selected!");
        }
        
        
    }
    
    
    @try {
        
        
        NSSortDescriptor *aphabeticDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
        tableData = [[tableData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [table_favView reloadData];
        [self addNotify];//add later
        [self checkServiceStatus];

        
    });
    NSLog(@"tableData=%@",tableData);
    // SERVICE_NAME
    
}
-(void)fetchDatafromDB
{
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        
        [self performSelector:@selector(reloadAfterDelay) withObject:nil afterDelay:0.1];
        
    }];
    
}






- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //Here, for each section, you must return the number of rows it will contain
    
    return [tableData count];
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return 120;
    }
    
    return 110;
    
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    [table_favView beginUpdates];
    [table_favView endUpdates];
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    
    //[self addNotify];
    //[table_favView reloadData];
}


//------- Start delegate methods of update profile------------
/*
 -(void)addNotify
 {
 
 
 
 CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
 
 
 [table_favView beginUpdates];
 [table_favView endUpdates];
 
 
 // Allocate a reachability object
 for (UIView *subview in [self.view subviews])
 {
 if ([subview isKindOfClass:[profileBarVC class]])
 {
 [subview removeFromSuperview];
 }
 }
 
 
 
 viewFromNib = [[NSBundle mainBundle] loadNibNamed:@"profileBarVC" owner:nil options:nil].firstObject;
 
 
 viewFromNib.delegate=self;
 //viewFromNib.frame=CGRectMake(0, fDeviceHeight-100, fDeviceWidth, 50);
 
 viewFromNib.frame=CGRectMake(0, table_favView.frame.origin.y+table_favView.frame.size.height, fDeviceWidth, 50);
 
 // code here
 //UIViewController *vc=[weakSelf topMostController];
 [self.view addSubview:viewFromNib];
 
 
 }
 
 
 -(void)closePressed
 {
 NSLog(@"home tabremove");
 CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
 
 table_favView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight);
 [viewFromNib removeFromSuperview];
 
 }
 
 
 -(void)updateProfile
 {
 NSLog(@"home clickUpdateAction");
 UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
 vc.hidesBottomBarWhenPushed = YES;
 [self.navigationController pushViewController:vc animated:YES];
 }
 
 */
//------- End of delegate methods of update profile------------

//------- End of delegate methods of update profile------------






/*- (void)viewWillLayoutSubviews {
 // Your adjustments accd to
 // viewController.bounds
 CGRect rect;
 rect = [[UIApplication sharedApplication] statusBarFrame]; // Get status bar frame dimensions
 NSLog(@"Statusbar frame: %1.0f, %1.0f, %1.0f, %1.0f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
 // flagStatusBar=TRUE;
 
 if (rect.size.height>=40)
 {
 
 flagStatusBar=TRUE;
 if (flagrotation==TRUE)
 {
 [self addNotify];
 }
 }
 else
 {
 flagStatusBar=FALSE;
 NSString* str =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"PROFILE_COMPELTE_KEY"];
 // if ([str isEqualToString:@"100"])
 if ([str length]>0)
 {
 
 @try {
 if ([str intValue]>=80)
 {
 flagStatusBar=TRUE;
 if (flagrotation==TRUE)
 {
 [self addNotify];
 }
 }
 } @catch (NSException *exception) {
 
 } @finally {
 
 }
 }
 }
 [super viewWillLayoutSubviews];
 }
 */

-(void)addNotify
{
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    // Rashpinder Changes IPad
    CGRect tblFrame = CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight);
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        tblFrame = CGRectMake(80,tblFrame.origin.y + 20, self.view.frame.size.width - 160, tblFrame.size.height - 40);
        
    }
    if (iPhoneX())
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        float yX= kiPhoneXNaviHeight4Search;
        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
        
        float heightX = heightVar-tabBarHeight ;
        tblFrame=CGRectMake(0,yX,fDeviceWidth,heightX);
        
    }
    table_favView.frame= tblFrame;
    
    // end Changes
    NSString* showProfilestatus =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"SHOW_PROFILEBAR"];//[NSString stringWithFormat:@"%@", profileComplete];
    
    if ([showProfilestatus isEqualToString:@"YES"])
    {
        
        
        
        // tableView_home.frame=CGRectMake(0,69,377,fDeviceHeight-69-NV_height);
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        // Rashpinder Changes IPad
        
        CGRect tblFrame = CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-NV_height);
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            tblFrame = CGRectMake(80,tblFrame.origin.y + 20, self.view.frame.size.width - 160, tblFrame.size.height - 40);
        }
        if (iPhoneX())
        {
            CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
            
            float yX= kiPhoneXNaviHeight4Search;
            float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
            
            float heightX = heightVar-tabBarHeight-NV_height ;
            tblFrame=CGRectMake(0,yX,fDeviceWidth,heightX);
            
        }
        table_favView.frame= tblFrame;
        
        for (UIView *subview in [self.view subviews])
        {
            if (subview.tag == 7) {
                [subview removeFromSuperview];
            }
        }
        
        
        
        // UIEdgeInsets inset = UIEdgeInsetsMake(-20, 0,0, 0);
        // tableView_home.contentInset = inset;
        
        
        //----------- Add later can remove it----
        if ([[UIScreen mainScreen]bounds].size.height == 812)
        {
            NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-tabBarHeight-NV_height,fDeviceWidth, NV_height)];
            if (flagStatusBar==TRUE)
            {
                NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
                
            }
        }
        else
        {
            NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-1.95*NV_height,fDeviceWidth, NV_height)];
            if (flagStatusBar==TRUE)
            {
                NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
                
            }
        }
        
//        NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-1.95*NV_height,fDeviceWidth, NV_height)];
//
//        if (flagStatusBar==TRUE)
//        {
//            NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
//
//        }
        
        
        NotifycontainerView.backgroundColor = [UIColor clearColor];
        NotifycontainerView.tag=7;
        [self.view addSubview:NotifycontainerView];
        
        
        //-------- background image view--------------
        UIImageView *bg_img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,fDeviceWidth, NV_height)];
        bg_img.image=[UIImage imageNamed:@"img_popup_notify.png"];
        //bg_img.tag=7;
        [NotifycontainerView addSubview:bg_img];
        
        
        
        //  dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", NULL);
        //  dispatch_async(queue, ^{
        //code to be executed in the background
        
        
        //------- Profile image view-------
        UIImageView *user_img=[[UIImageView alloc]initWithFrame:CGRectMake(10,5,35,35)];
        
        
        
        UIImage *tempImg;
        if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_male", nil)]) {
            tempImg=[UIImage imageNamed:@"male_avatar"];
            
        }
        else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_female", nil)]) {
            tempImg=[UIImage imageNamed:@"female_avatar"];
            
        }
        else
        {
            tempImg=[UIImage imageNamed:@"user_placeholder"];
            
            
        }
        
        
        
        user_img.image=tempImg;
        
        //dispatch_async(dispatch_queue_create("com.getImage", NULL), ^(void) {
        //NSLog(@"singleton.imageLocalpath=%@",singleton.imageLocalpath);
        
        UIImage *tempImg1 ;
        
        if([[NSFileManager defaultManager] fileExistsAtPath:singleton.imageLocalpath])
        {
            // ur code here
            NSLog(@"file present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            
            tempImg1 = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
            if (tempImg1==nil) {
                
                tempImg1=tempImg;
                
            }
            user_img.image=tempImg1;
        } else {
            // ur code here**
            NSLog(@"Not present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            tempImg1=tempImg;
            
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        singleton.user_profile_URL=[defaults decryptedValueForKey:@"USER_PIC"];
        NSLog(@"user_profile_URL=%@",singleton.user_profile_URL);
        
        
        [defaults synchronize];
        
        NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
        
        if(![[url absoluteString] isEqualToString:@""])
        {
            
            
            NSURLRequest* request = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse * response,
                                                       NSData * data,
                                                       NSError * error) {
                                       if (!error){
                                           if(data == nil)
                                           {
                                               user_img.image = tempImg1;
                                               
                                           }
                                           else
                                           {
                                               UIImage* image = [[UIImage alloc] initWithData:data];
                                               //added later to resolve if server image path is invalid image
                                               if(image == nil)
                                               {
                                                   image = tempImg1;
                                                   
                                               }
                                               //== end of  image nil

                                               user_img.image = image;
                                           }
                                           // do whatever you want with image
                                       }
                                       else
                                       {
                                           user_img.image = tempImg1;
                                           
                                       }
                                       
                                   }];
            
        }
        else
        {
            user_img.image = tempImg1;
            
        }
        
        // });
        
        user_img.layer.cornerRadius = user_img.frame.size.height /2;
        user_img.layer.masksToBounds = YES;
        user_img.layer.borderWidth = 0.1;
        //user_img.tag=7;
        [NotifycontainerView addSubview:user_img];
        
        // img_blurProfile.image=[self blurredImageWithImage:img_profileView.image];//comment this code
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //code to be executed on the main thread when background task is finished
            //update UI with new database inserted means reload uicollectionview
            [table_favView reloadData];
            
        });
        //  });
        
        
        
        
        
        
        //-------Label Complete text----------
        
        CGSize stringcompletesize = [NSLocalizedString(@"profile_completion", nil) sizeWithFont:[UIFont systemFontOfSize:12]];
        //or whatever font you're using
        
        
        
        
        
        
        
        
        lbl_profileComplete=[[UILabel alloc]init];
        
        [lbl_profileComplete setFrame:CGRectMake(51,6,stringcompletesize.width, stringcompletesize.height)];
        
        lbl_profileComplete.text= NSLocalizedString(@"profile_completion", nil);
        
        lbl_profileComplete.font=[UIFont systemFontOfSize:12];
        lbl_profileComplete.textColor=[UIColor whiteColor];
        lbl_profileComplete.adjustsFontSizeToFitWidth = YES;
        //lbl_profileComplete.tag=7;
        [NotifycontainerView addSubview:lbl_profileComplete];
        
        
        
        
        
        
        
        //-------Button Click Update ----------
        
        
        btn_clickUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
        
        // btn_clickUpdate.frame = CGRectMake(52,20,290,22);
        
        
        @try {
            btn_clickUpdate.titleLabel.adjustsFontSizeToFitWidth = YES;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        CGSize stringsize = [NSLocalizedString(@"update_profile_txt", nil) sizeWithFont:[UIFont systemFontOfSize:12]];
        //or whatever font you're using
        
        
        
        
        [btn_clickUpdate setFrame:CGRectMake(52,25,stringsize.width, stringsize.height)];
        
        
        [btn_clickUpdate addTarget:self
                            action:@selector(clickUpdateAction:)
                  forControlEvents:UIControlEventTouchUpInside];
        [btn_clickUpdate setTitle:NSLocalizedString(@"update_profile_txt", nil) forState:UIControlStateNormal];
        
        [btn_clickUpdate setTitle:NSLocalizedString(@"update_profile_txt", nil) forState:UIControlStateSelected];
        btn_clickUpdate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        btn_clickUpdate.titleLabel.font=[UIFont systemFontOfSize:12];
        // btn_clickUpdate.tag=7;
        
        [NotifycontainerView addSubview:btn_clickUpdate];
        
        
        //---- Show Custom Progress Profile View--------
        
        
        
        UIProgressView *progressView;
        progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        progressView.progressTintColor =[UIColor colorWithRed:12.0/255.0 green:118.0/255.0 blue:157.0/255.0 alpha:1.0];
        [[progressView layer]setFrame:CGRectMake(lbl_profileComplete.frame.origin.x+lbl_profileComplete.frame.size.width+5,9,fDeviceWidth-188-50,10)];
        progressView.trackTintColor = [UIColor whiteColor];
        [[progressView layer]setCornerRadius:2];
        progressView.layer.masksToBounds = TRUE;
        progressView.clipsToBounds = TRUE;
        
        
        
        NSString* str =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"PROFILE_COMPELTE_KEY"];//[NSString stringWithFormat:@"%@", profileComplete];
        //str=@"90";
        if (str == nil)
        {
            str=@"0";
        }
        if ([str length]==0)
        {
            str=@"0";
            
        }
        
        CGFloat value = [str floatValue]/100;
        lbl_percentage.text=  [NSString stringWithFormat:@"%@%%",str];
        // self.progress=value;
        [progressView setProgress:value];  ///15
        
        
        
        [NotifycontainerView addSubview:progressView];
        
        
        
        
        
        //-------Label Percentage text----------
        lbl_percentage=[[UILabel alloc]initWithFrame:CGRectMake(progressView.frame.origin.x+progressView.frame.size.width+5,2,30,22)];
        
        
        
        if ([str intValue]>=70)
            
        {
            
            [self closeBtnAction:self];
        }
        
        
        
        lbl_percentage.text=[NSString stringWithFormat:@"%@%%",str];
        lbl_percentage.font=[UIFont boldSystemFontOfSize:11];
        lbl_percentage.textColor=[UIColor whiteColor];
        @try {
            lbl_percentage.adjustsFontSizeToFitWidth = YES;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        //lbl_percentage.tag=7;
        [NotifycontainerView addSubview:lbl_percentage];
        
        
        //-------Button Close ----------
        
        btn_close = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn_close.frame = CGRectMake(fDeviceWidth-30,NV_height/2-8, 22, 22);
        
        [btn_close addTarget:self
                      action:@selector(closeBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        UIImage *buttonImagePressed = [UIImage imageNamed:@"btn_close_notify.png"];
        [btn_close setImage:buttonImagePressed forState:UIControlStateNormal];
        [btn_close setImage:buttonImagePressed forState:UIControlStateSelected];
        
        btn_close.tag=7;
        
        [NotifycontainerView addSubview:btn_close];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [table_favView reloadData];
        });
        // [tableView_home reloadData];
    }
    else
    {
        
        [self closeBtnAction:self];
        
        
    }
}

- (void)closeBtnAction:(id)sender
{
    
    [singleton traceEvents:@"Close Profile Bar" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
    
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"SHOW_PROFILEBAR"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    CGRect tblFrame = CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight);
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        tblFrame = CGRectMake(80,tblFrame.origin.y + 20, self.view.frame.size.width - 160, tblFrame.size.height - 40);
        
    }
    if (iPhoneX())
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        float yX= kiPhoneXNaviHeight4Search;
        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
        
        float heightX = heightVar-tabBarHeight ;
        tblFrame=CGRectMake(0,yX,fDeviceWidth,heightX);
        
    }
    table_favView.frame= tblFrame;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [table_favView reloadData];
    });
    
    [NotifycontainerView removeFromSuperview];
}





- (void)updateProgress
{
    // NSString* str = [NSString stringWithFormat:@"%@", profileComplete];
    NSString* str =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"PROFILE_COMPELTE_KEY"];//[NSString stringWithFormat:@"%@", profileComplete];
    
    if (str == nil)
    {
        str=@"0";
    }
    if ([str length]==0)
    {
        str=@"0";
        
    }
    
    CGFloat value = [str floatValue]/100;
    lbl_percentage.text=  [NSString stringWithFormat:@"%@%%",str];
    self.progress=value;
    
    
    
    [self.progressViews enumerateObjectsUsingBlock:^(THProgressView *progressView, NSUInteger idx, BOOL *stop) {
        [progressView setProgress:self.progress animated:YES];
        
    }];
    
}



-(void)clickUpdateAction:(id)sender
{
    
    // Reset here for login with registration here
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLoginWithRegistration"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    SharedManager *singleton  = [SharedManager sharedSingleton];
    singleton.shared_mpinflag = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinflag"];
    singleton.shared_mpinmand = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinmand"];
    if ([[singleton.shared_mpinflag lowercaseString] isEqualToString:@"false"])
    {
        
        NSLog(@"MPIN SHOWING STATUS IS FALSE, so  show it");
        
        dispatch_async(dispatch_get_main_queue(),^{
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate showMPIN_AlertBox];
            [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
             delegate.vw_MPIN_AlertBox.lbl_subtitle1.text = NSLocalizedString(@"setting_mpin_mandatory_this_screen", nil);
        });
        /*
        NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]||[lastTime isEqualToString:@"NR"])
        {
            // dont show first time
        }
        else
        {
            // ========= check to open show mpin alert box=====
            dispatch_async(dispatch_get_main_queue(),^{
                AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [delegate showHideSetMpinAlertBox ];
                [delegate.vw_MPIN_AlertBox showHideClose:@"false"];

            });
        }
         */
    }
    else
    {
        // do nothing
        NSLog(@"MPIN SHOWING STATUS IS TRUE, so dont show it");
        
    
    [singleton traceEvents:@"Update Profile from Profile Bar" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    }
    
}



//----- End----------


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"FavouriteCell";
    
    FavouriteCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[FavouriteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    
    // dispatch_async(dispatch_get_main_queue(), ^{
    
    
    cell.lbl_serviceName.text=[[tableData objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"];
    cell.lbl_serviceDesc.text=[[tableData objectAtIndex:indexPath.row] valueForKey:@"SERVICE_DEPTDESCRIPTION"];
    
    NSString *imageName= [[tableData objectAtIndex:indexPath.row] valueForKey:@"SERVICE_IMAGE"];
    
    
    NSURL *url=[NSURL URLWithString:imageName];
    
    [cell.img_serviceCateg sd_setImageWithURL:url
                             placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
    
    
    
    
    cell.lbl_nouse.text = @"";
    
    
    //---------- Set business Unit color here---------------------------------
    // cell.lbl_serviceCateg.backgroundColor = [UIColor clearColor];
    //NSLog(@"get_BUcolorcode=%@",get_BUcolorcode);
    // cell.lbl_serviceCateg.textColor=[UIColor whiteColor];
    
    
//    NSString *category = [[tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_CATEGORY"];
//    NSMutableArray *arrCat = [[NSMutableArray alloc] init];
//    /*NSArray *arr = [category componentsSeparatedByString:@","];
//     for (NSString *cat in arr) {
//     [arrCat addObject:cat];
//     }*/
//    [arrCat addObject:category];
    NSString *multi_Category = [[self.tableData  objectAtIndex:indexPath.row] valueForKey:@"MULTI_CATEGORY_NAME"];
    
    
    // MULTI_CATEGORY_NAME
    NSMutableArray *arrCat = [[NSMutableArray alloc] init];
    /*NSArray *arr = [category componentsSeparatedByString:@","];
     for (NSString *cat in arr) {
     [arrCat addObject:cat];
     }*/
    //[arrCat addObject:category];
    if ([multi_Category containsString:@","]) {
        NSArray *arrMulti = [multi_Category componentsSeparatedByString:@","];
        [arrCat addObjectsFromArray:arrMulti];
    }else {
        [arrCat addObject:multi_Category];
    }
    
    NSString *stateId = [[tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_STATE"];
    NSString *stateName = [obj getStateName:stateId];
    if (stateId.length == 0 || [stateId isEqualToString:@"99"]) {
        stateName =  NSLocalizedString(@"central", @"");
    }
    NSMutableArray *otherStatesNames = [[NSMutableArray alloc] init] ;
    if (stateId.length != 0 && ![stateId isEqualToString:@"99"])
    {
        NSString *otherStateId = [[tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_OTHER_STATE"];
        if (otherStateId.length != 0) {
            NSString *newString = [otherStateId substringToIndex:[otherStateId length]-1];
            NSString *finalString = [newString substringFromIndex:1];
            if (finalString.length != 0)
            {
                NSArray *otherStates = [finalString componentsSeparatedByString:@"|"];
                NSLog(@"%@",otherStates);
                for (NSString *stateIDS in otherStates) {
                    [otherStatesNames addObject: [obj getStateName:stateIDS]];
                }
            }
        }
        
    }
    if ([otherStatesNames containsObject:stateName]) {
        [otherStatesNames removeObject:stateName];
    }
    NSMutableArray *arrState = [[NSMutableArray alloc] initWithObjects:stateName, nil];
    if (otherStatesNames.count != 0) {
        [arrState addObjectsFromArray:otherStatesNames];
    }
    /*NSString *otherStateID = [[tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_OTHER_STATE"];
     if (otherStateID.length != 0 && ![otherStateID isEqualToString:@"99"]) {
     NSArray *arr = [otherStateID componentsSeparatedByString:@","];
     for (NSString *stateID in arr) {
     [arrState addObject:[obj getStateName:stateID]];
     }
     }*/
    
    //  NSString *stateName = [obj getStateName:stateId];
    
    //cell.lbl_serviceCateg.text= category;
    
    CGFloat fontSize = 12.0;
    
    [cell addCategoryStateItemsToScrollView:arrCat state:arrState withFont:fontSize cell:cell andTagIndex:indexPath];
    
    //        CGRect frame = [self rectForText:category usingFont:[UIFont systemFontOfSize:fontSize] boundedBySize:CGSizeMake(self.view.frame.size.width, cell.lbl_serviceCateg.frame.size.height)];
    
    
    
    //        cell.lbl_serviceCateg.frame=CGRectMake(cell.lbl_serviceCateg.frame.origin.x, cell.lbl_serviceCateg.frame.origin.y, frame.size.width+20,cell.lbl_serviceCateg.frame.size.height);
    //        cell.lbl_serviceCateg.layer.cornerRadius = 10.0;
    //        cell.lbl_serviceCateg.layer.backgroundColor=[UIColor colorWithRed:235/255.0 green:86/255.0 blue:2/255.0 alpha:1.0].CGColor;
    
    
    [cell.btn_serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btn_serviceFav.tag=indexPath.row ;
    
    cell.btn_moreInfo.tag=indexPath.row;
    
    
    [cell.btn_moreInfo  addTarget:self action:@selector(btnMoreInfoClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString *rating=[[tableData  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_RATING"];
    //[cellData objectForKey:@"SERVICE_RATING"];
    
    cell.lbl_rating.text=rating;
    
    
    cell.lbl_serviceName.font = [AppFont semiBoldFont:16.0];
    cell.lbl_serviceDesc.font = [AppFont lightFont:14.0];
    cell.lbl_serviceDesc.numberOfLines = 2;
    cell.lbl_serviceDesc.lineBreakMode = NSLineBreakByWordWrapping;
    cell.lbl_rating.font = [AppFont regularFont:13.0];
    
    
    
    //});
    
    
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    if ([cell isKindOfClass:[FavouriteCell class]])
    {
        FavouriteCell *favCell = (FavouriteCell*)cell;
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //            favCell.lblName.font = [UIFont systemFontOfSize:16.0];
            //            favCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            //            [favCell setNeedsDisplay];
            //        });
            
            //  }
            
            
            CGRect buttonImageAdvanceFrame =  favCell.img_serviceCateg.frame;
            buttonImageAdvanceFrame.origin.x = singleton.isArabicSelected ?CGRectGetWidth(tableView.frame) - 62 : 12;
            favCell.img_serviceCateg.frame = buttonImageAdvanceFrame;
            
            
            CGRect lblServiceTitle =  favCell.lbl_serviceName.frame;
            lblServiceTitle.origin.x = singleton.isArabicSelected ? 10  : 80;
            lblServiceTitle.size.width = favCell.frame.size.width - 100;
            favCell.lbl_serviceName.frame = lblServiceTitle;
            favCell.lbl_serviceName.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            
            CGRect lbl_servicedescFrame =  favCell.lbl_serviceDesc.frame;
            lbl_servicedescFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
            lbl_servicedescFrame.size.width = favCell.frame.size.width - 100;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                lbl_servicedescFrame.origin.y = CGRectGetMaxY(lblServiceTitle);
                lbl_servicedescFrame.size.height = 45;
            }
            
            favCell.lbl_serviceDesc.frame = lbl_servicedescFrame;
            favCell.lbl_serviceDesc.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            CGRect lblRatingFrame =  favCell.lbl_rating.frame;
            lblRatingFrame.origin.x = singleton.isArabicSelected ? favCell.frame.size.width - 95  : 80;
            favCell.lbl_rating.frame = lblRatingFrame;
            //        cell.lbl_rating.backgroundColor = [UIColor redColor];
            
            CGRect starImageFrame =  favCell.img_star.frame;
            starImageFrame.origin.x = singleton.isArabicSelected ? lblRatingFrame.origin.x -20  : lblRatingFrame.origin.x +lblRatingFrame.size.width -8;
            favCell.img_star.frame = starImageFrame;
            
            
            
            
            CGRect categoryLabelFrame =  favCell.lbl_serviceCateg.frame;
            categoryLabelFrame.origin.x = singleton.isArabicSelected ? starImageFrame.origin.x - 90  : starImageFrame.origin.x + 30 ;
            favCell.lbl_serviceCateg.frame = categoryLabelFrame;
            
            
            CGRect btnFavFrame =  favCell.btn_serviceFav.frame;
            btnFavFrame.origin.x = singleton.isArabicSelected ? 10  : favCell.frame.size.width - 30 ;
            favCell.btn_serviceFav.frame = btnFavFrame;
            
            CGRect btnMoreFrame =  favCell.btn_moreInfo.frame;
            btnMoreFrame.origin.x = singleton.isArabicSelected ? 5  : favCell.frame.size.width - 36 ;
            favCell.btn_moreInfo.frame = btnMoreFrame;
            
        });
    }
}

-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}




- (IBAction)fav_action:(MyFavButton *)sender
{
    
    [singleton traceEvents:@"Favourite button" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
    
    MyFavButton *button = (MyFavButton *)sender;
    // Add image to button for normal state
    NSLog(@"sender=%ld",(long)[sender tag]);
    
    int indexOfTheRow=(int)button.tag;
    
    NSLog(@"indexOfTheRow=%d",indexOfTheRow);
    
    
    // NSString *text = [NSString stringWithFormat:@"%@",indexOfTheRow];
    
    NSString *serviceFav=[NSString stringWithFormat:@"%@",[[tableData  objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_IS_FAV"]];
    
    NSString *serviceId=[[tableData objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_ID"];
    

    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
        button.selected=FALSE;
        [tableData removeObjectAtIndex:indexOfTheRow];
        
        
    }
    else
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
        button.selected=true;
    }
    
    
    singleton.loadServiceStatus =@"FALSE";//if added load service status
    
    [self checkServiceStatus];
    dispatch_async(dispatch_get_main_queue(), ^{
        [table_favView reloadData];
    });
    
}
-(void)checkServiceStatus
{
    if ([tableData count]>0) {
        // self.lbl_favourite.hidden=true;
        //vw_noServiceFound.alpha=0;
        
        vw_noServiceFound.hidden=TRUE;
        table_favView.hidden=false;
        [hud hideAnimated:YES];

    }
    else
    {
        // vw_noServiceFound.alpha=1;
        
        vw_noServiceFound.hidden=FALSE;
        
        // self.lbl_favourite.hidden=false;
        table_favView.hidden=true;
        
        NSString *localString=[NSString stringWithFormat:@"%@",NSLocalizedString(@"tap_fav_icon_help_text", nil)];
        localString = [localString stringByReplacingOccurrencesOfString:@"u2665"
                                                             withString:@"♥"];
        NSLog(@"str=%@",localString);
        
        toAddFavouriteLabel.text = localString;
        
        noFavouriteFoundLabel.text = NSLocalizedString(@"no_favourites", nil);
        
    }
    
    /*if ([singleton.loadServiceStatus isEqualToString:@"TRUE"])
     {
     vw_noServiceFound.hidden=TRUE;
     
     
     
     }
     if ([singleton.loadServiceStatus isEqualToString:@"FALSE"])
     {
     vw_noServiceFound.hidden=FALSE;
     
     }
     
     
     
     }*/
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *cellData = [tableData objectAtIndex:indexPath.row];
    if (cellData)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        vc.dic_serviceInfo=cellData;
        vc.tagComeFrom=@"OTHERS";
        
        [singleton traceEvents:@"Department Page" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
        
        vc.sourceTab     = @"favourites";
        vc.sourceState   = @"";
        vc.sourceSection = @"";
        vc.sourceBanner  = @"";
        
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}


//-----------for filterview show----------
-(IBAction)btn_filterAction:(id)sender
{
    //code to open filter view here
    
    NSLog(@"Filter Pressed");
    
    
    
    // NotificationFilterVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationFilterVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    // [self presentViewController:vc animated:NO completion:nil];
    // vc.hidesBottomBarWhenPushed = YES;
    
    // [self.navigationController pushViewController:vc animated:YES];
    
    [singleton traceEvents:@"Filter Button" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
    
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    AddFavFilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddFavFilterVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    // [self presentViewController:vc animated:NO completion:nil];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}





//-----------for notification view show---
-(IBAction)btn_noticationAction:(id)sender
{
    //code to open notification view here
    
    NSLog(@"Notification Pressed");
    
    [singleton traceEvents:@"Notification button" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
    
    ScrollNotificationVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ScrollNotificationVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:vc animated:NO completion:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}



/*
 - (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
*/

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == txt_searchField)
    {
        [self openSearch];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)openSearch
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [singleton traceEvents:@"Search Bar Clicked" withAction:@"Clicked" withLabel:@"Favourite Tab" andValue:0];
    
    AdvanceSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AdvanceSearchVC"];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:vc animated:NO completion:nil];
    
}



@end

