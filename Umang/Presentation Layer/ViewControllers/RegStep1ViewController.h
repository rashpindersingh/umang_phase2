//
//  RegStep1ViewController.h
//  SpiceRegistration
//
//  Created by spice_digital on 21/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegStep1ViewController : UIViewController
{
    SharedManager*  singleton ;
}
@property (weak, nonatomic) IBOutlet UIView *vwTextBG;
//----- added for passing rty and tout-----
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
//----- added for passing rty and tout-----

@end
