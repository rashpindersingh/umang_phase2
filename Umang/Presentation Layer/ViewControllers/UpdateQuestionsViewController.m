//
//  UpdateQuestionsViewController.m
//  Umang
//
//  Created by Lokesh Jain on 10/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "UpdateQuestionsViewController.h"
#import "CZPickerView.h"
#import "SharedManager.h"
#import "UpdMpinVC.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "AadharRegAfterMobileVC.h"
#import "ProfileAfterAdharVC.h"
#import "UIView+Toast.h"
#import "ResetPinVC.h"
#import "EnterMobileOTPVC.h"
#import "ForgotMPINVC.h"

#import "MobileRegistrationVC.h"
#import "AadharRegModuleVC.h"
#import "ProfileAfterMobileVC.h"
#import "QuestionsViewController.h"
#import "UMANG-Swift.h"

#import "RunOnMainThread.h"
#import "SecuritySettingVC.h"

#import "UMANG-Swift.h"

@interface UpdateQuestionsViewController ()<CZPickerViewDelegate,CZPickerViewDataSource>
    {
        SharedManager *objShared;
        NSIndexPath *selectedButtonIndexPath;
        
        NSString *selectedQuestionStrRow1;
        NSString *selectedQuestionStrRow2;
        
        NSString *enteredAnswerStrRow1;
        NSString *enteredAnswerStrRow2;
        
        MBProgressHUD *hud ;
        NSMutableArray *sampleArray;
        
        
        BOOL chekDismiss;
        
        __weak IBOutlet UIButton *btnBack;
        IBOutlet NSLayoutConstraint *screenTitleWidthConstraint;
    }
    
@end

@implementation UpdateQuestionsViewController
@synthesize tagCheckRecovery;
-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
    {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           if ([view isKindOfClass:[UITextField class]])
                           {
                               
                               UITextField *txtfield = (UITextField *)view;
                               NSString *fonttxtFieldName = txtfield.font.fontName;
                               CGFloat fonttxtsize =txtfield.font.pointSize;
                               txtfield.font = nil;
                               
                               txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                               
                               [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                           }
                           
                           
                       });
        
        if ([view isKindOfClass:[UITextView class]])
        {
            
            UITextView *txtview = (UITextView *)view;
            NSString *fonttxtviewName = txtview.font.fontName;
            CGFloat fontbtnsize =txtview.font.pointSize;
            
            txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
            
        }
        
        
        if ([view isKindOfClass:[UILabel class]])
        {
            UILabel *lbl = (UILabel *)view;
            NSString *fontName = lbl.font.fontName;
            CGFloat fontSize = lbl.font.pointSize;
            
            lbl.font = [UIFont fontWithName:fontName size:fontSize];
        }
        
        
        
        if ([view isKindOfClass:[UIButton class]])
        {
            UIButton *button = (UIButton *)view;
            NSString *fontbtnName = button.titleLabel.font.fontName;
            CGFloat fontbtnsize = button.titleLabel.font.pointSize;
            
            [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
        }
        
        if (isSubViews)
        {
            
            for (UIView *sview in view.subviews)
            {
                [self setFontforView:sview andSubViews:YES];
            }
        }
        
    }
    
- (void)viewDidLoad
    {
        [super viewDidLoad];
        
        
        if (self.view.frame.size.height == 480)
        {
            self.questionTable.scrollEnabled = YES;
        }
        
        if (self.view.frame.size.width > 500)
        {
            self.nextButton.frame = CGRectMake(200, self.nextButton.frame.origin.y, 368, 65);
        }
        
        [self.nextButton setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
        [self.nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.nextButton.layer.cornerRadius = 3.0f;
        self.nextButton.clipsToBounds = YES;
        
        // Do any additional setup after loading the view.
        
        //NSLocalizedString(@"help_email_help", nil);
        [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
        
        NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
        if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
        {
            
            CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                           @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
            
            CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
            
            [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
            btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
            
        }
        
        self.questionTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        
        
        if ([self.tagComingFrom isEqualToString:@"Set MPIN"] || [self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"])
        {
            self.securityQuestionLabel.text = NSLocalizedString(@"security_question", nil);
            self.securityDescriptionLabel.text = NSLocalizedString(@"security_question_heading", nil);
        }
        else if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        {
            self.securityQuestionLabel.text = NSLocalizedString(@"security_question", nil);
            self.securityDescriptionLabel.text = NSLocalizedString(@"ask_question_heading", nil);
        }
        else
        {
            self.securityQuestionLabel.text = NSLocalizedString(@"update_security_ques", nil);
            self.securityDescriptionLabel.text = NSLocalizedString(@"security_question_heading", nil);
        }
        
        id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:SECURITY_QUESTION_SCREEN];
        [[GAI sharedInstance].defaultTracker send:
         [[GAIDictionaryBuilder createScreenView] build]];
        
        objShared = [SharedManager sharedSingleton];
        objShared.questionsArray = [NSMutableArray new];
        
        
        if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        {
            objShared.questionsArray = [objShared createQuestionArray];
            sampleArray = [NSMutableArray new];
            
            NSLog(@"%@",self.responseQuestionArray);
            
            [sampleArray removeAllObjects];
            
            [[objShared createQuestionArray] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                NSDictionary *dict = obj;
                
                if ([[dict valueForKey:@"questionId"] isEqualToString:[self.responseQuestionArray objectAtIndex:0]] || [[dict valueForKey:@"questionId"] isEqualToString:[self.responseQuestionArray objectAtIndex:1]])
                {
                    [sampleArray addObject:obj];
                }
            }];
            
            objShared.questionsArray = sampleArray;
            
            NSLog(@"%@",objShared.questionsArray);
        }
        else
        {
            objShared.questionsArray = [objShared createQuestionArray];
        }
        
        
        UITapGestureRecognizer *sortViewGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignFields:)];
        sortViewGesture.delegate = self;
        [self.view addGestureRecognizer:sortViewGesture];
        
        
        self.questionTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            screenTitleWidthConstraint.constant = 400;
        }
        else
        {
            if (fDeviceWidth > 320)
            {
                screenTitleWidthConstraint.constant = 220;
            }
        }
        [self addNavigationView];
    }
    
    
-(void)viewWillAppear:(BOOL)animated
    {
        [super viewWillAppear:animated];
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
        //———— Add to handle network bar of offline——
        
        //——————— Add to handle portrait mode only———
        /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
         AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
         appDelegate.shouldRotate = NO;
         [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
        //——————— Add to handle portrait mode only———
        
        
        [self setViewFont];
    }
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    self.securityQuestionLabel.font = [AppFont semiBoldFont:17.0];
    self.securityDescriptionLabel.font = [AppFont mediumFont:18.0];
    [self.nextButton.titleLabel setFont:[AppFont mediumFont:19.0]];
}
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif
    
- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
    
    
-(void)resignFields:(UITapGestureRecognizer *)gesture
    {
        [self.view endEditing:YES];
    }
    //- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
    //{
    //    return ![NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"];
    //}
    
- (IBAction)backButtonPressed:(UIButton *)sender
    {
        
        
        if ([self.tagComingFrom isEqualToString:@"UPDATEFROMSECURITY_MPINBOX"])
        {
            [self dismissViewControllerAnimated:false completion:nil];
        }
        if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        {
            
            [self.navigationController popViewControllerAnimated:true];
            //        UIViewController *vc = self.presentingViewController;
            //        while (vc.presentingViewController) {
            //            vc = vc.presentingViewController;
            //
            //            if ([vc isKindOfClass:[ForgotMPINVC class]])
            //            {
            //                chekDismiss = YES;
            //                [vc dismissViewControllerAnimated:YES completion:nil];
            //            }
            //        }
            //
            //        if (!chekDismiss)
            //        {
            //            UIViewController *vc1 = self.presentingViewController;
            //            while (vc1.presentingViewController) {
            //                vc1 = vc1.presentingViewController;
            //            }
            //            [vc1 dismissViewControllerAnimated:YES completion:NULL];
            //        }
            
        }
        else if ([self.tagComingFrom isEqualToString:@"Set MPIN"])
        {
            UIViewController *vc = self.presentingViewController;
            while (vc.presentingViewController) {
                vc = vc.presentingViewController;
                
                //MobileRegistrationVC
                
                if ([vc isKindOfClass:[MobileRegister_FlagVC class]])
                {
                    [vc dismissViewControllerAnimated:YES completion:nil];
                }
                
            }
        }
        else if ([self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"])
        {
            UIViewController *vc = self.presentingViewController;
            while (vc.presentingViewController) {
                vc = vc.presentingViewController;
                
                if ([vc isKindOfClass:[AadharRegModuleVC class]])
                {
                    [vc dismissViewControllerAnimated:YES completion:nil];
                }
                
            }
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
- (IBAction)nextButtonClicked:(UIButton *)sender
    {
        [[SharedManager sharedSingleton] traceEvents:@"Next Button" withAction:@"Clicked" withLabel:@"Security Questions Screen" andValue:0];
        if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        {
            
            if (selectedQuestionStrRow1.length != 0 &&  enteredAnswerStrRow1.length != 0)
            {
                enteredAnswerStrRow1 = [enteredAnswerStrRow1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                if (enteredAnswerStrRow1.length < 2)
                {
                    
                    UIAlertController *okAlertCntrlr = [UIAlertController alertControllerWithTitle:@"Umang" message:NSLocalizedString(@"ans_cannot_less_than_two", nil) preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAlert = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        //[self.textFieldOldPassword becomeFirstResponder];
                        
                    }];
                    [okAlertCntrlr addAction:okAlert];
                    [self presentViewController:okAlertCntrlr animated:YES completion:nil];
                }
                else
                {
                    
                    
                    /*Call NEw Servic*/
                    __block NSString *firstQuestionID;
                    NSMutableArray *allQuestionArray = [objShared createQuestionArray];
                    
                    [allQuestionArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
                     {
                         NSDictionary *dict = obj;
                         
                         if ([[dict valueForKey:@"question"] isEqualToString:selectedQuestionStrRow1])
                         {
                             firstQuestionID = [dict valueForKey:@"questionId"];
                         }
                     }];
                    
                    NSString *answerStrEncrypted=[[enteredAnswerStrRow1 lowercaseString] sha256HashFor:[enteredAnswerStrRow1 lowercaseString]];
                    
                    NSMutableArray *quesAns = [[NSMutableArray alloc] initWithObjects:@{@"quesId" : firstQuestionID,@"ans" : answerStrEncrypted},nil];
                    
                    
                    [self hitAPIToValidateAnswerWithArray:quesAns];
                    
                }
            }
            
            
            
        }
        else
        {
            if (selectedQuestionStrRow1.length != 0 && selectedQuestionStrRow2.length != 0 && enteredAnswerStrRow1.length != 0 && enteredAnswerStrRow2.length !=0)
            {
                //UpdMpinVC
                
                enteredAnswerStrRow1 = [enteredAnswerStrRow1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                enteredAnswerStrRow2 = [enteredAnswerStrRow2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                if (enteredAnswerStrRow1.length < 2 || enteredAnswerStrRow2.length < 2)
                {
                    UIAlertController *okAlertCntrlr = [UIAlertController alertControllerWithTitle:@"Umang" message:NSLocalizedString(@"ans_cannot_less_than_two", nil) preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAlert = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        //[self.textFieldOldPassword becomeFirstResponder];
                        
                    }];
                    [okAlertCntrlr addAction:okAlert];
                    [self presentViewController:okAlertCntrlr animated:YES completion:nil];
                    
                }
                else
                {
                    
                    
                    __block NSString *firstQuestionID;
                    __block NSString *secondQuestionID;
                    NSMutableArray *allQuestionArray = [objShared createQuestionArray];
                    
                    [allQuestionArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
                     {
                         NSDictionary *dict = obj;
                         
                         if ([[dict valueForKey:@"question"] isEqualToString:selectedQuestionStrRow1])
                         {
                             firstQuestionID = [dict valueForKey:@"questionId"];
                         }
                         else if ([[dict valueForKey:@"question"] isEqualToString:selectedQuestionStrRow2])
                         {
                             secondQuestionID = [dict valueForKey:@"questionId"];
                         }
                     }];
                    
                    
                    NSString *answer1StrEncrypted=[[enteredAnswerStrRow1 lowercaseString] sha256HashFor:[enteredAnswerStrRow1 lowercaseString]];
                    
                    
                    NSString *answer2StrEncrypted=[[enteredAnswerStrRow2 lowercaseString] sha256HashFor:[enteredAnswerStrRow2 lowercaseString]];
                    
                    NSMutableArray *quesAns = [[NSMutableArray alloc] initWithObjects:@{@"quesId" : firstQuestionID,@"ans" : answer1StrEncrypted},@{@"quesId" : secondQuestionID,@"ans" : answer2StrEncrypted},nil];
                    
                    if ([self.tagComingFrom isEqualToString:@"Set MPIN"])
                    {
                        [self hitAPIWithQuestionArray:quesAns];
                    }
                    else if ([self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"])
                    {
                        [self hitAPIAdhaarWithQuestionsArray:quesAns];
                    }
                    else
                    {
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UpdMpinVC *updateVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdMpinVC"];
                        updateVC.TAG_FROM   = @"UPDATEQUESTION";
                        if ([self.tagComingFrom isEqualToString:@"UPDATEFROMSECURITY_MPINBOX"]) {
                            __block typeof(self) weakSelf = self;
                            updateVC.didShowHomeVC = ^{
                                [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                                    [weakSelf dismissViewControllerAnimated:true completion:nil];
                                }];
                                //[weakSelf backButtonPressed:[UIButton new]];
                            };
                            updateVC.TAG_FROM   = @"UPDATEFROMSECURITY_MPINBOX";
                        }
                        
                        __block NSString *firstQuestionID;
                        __block NSString *secondQuestionID;
                        NSMutableArray *allQuestionArray = [objShared createQuestionArray];
                        
                        [allQuestionArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
                         {
                             NSDictionary *dict = obj;
                             
                             if ([[dict valueForKey:@"question"] isEqualToString:selectedQuestionStrRow1])
                             {
                                 firstQuestionID = [dict valueForKey:@"questionId"];
                             }
                             else if ([[dict valueForKey:@"question"] isEqualToString:selectedQuestionStrRow2])
                             {
                                 secondQuestionID = [dict valueForKey:@"questionId"];
                             }
                         }];
                        
                        NSString *answer1StrEncrypted=[[enteredAnswerStrRow1 lowercaseString] sha256HashFor:[enteredAnswerStrRow1 lowercaseString]];
                        
                        
                        NSString *answer2StrEncrypted=[[enteredAnswerStrRow2 lowercaseString] sha256HashFor:[enteredAnswerStrRow2 lowercaseString]];
                        
                        NSMutableArray *quesAns = [[NSMutableArray alloc] initWithObjects:@{@"quesId" : firstQuestionID,@"ans" : answer1StrEncrypted},@{@"quesId" : secondQuestionID,@"ans" : answer2StrEncrypted},nil];
                        
                        updateVC.quesAnswArray = [NSMutableArray new];
                        updateVC.quesAnswArray = quesAns;
                        
                        [self.navigationController pushViewController:updateVC animated:YES];
                        
                        NSLog(@"Push UPDMPINVC UPDATEQUESTION");
                    }
                    
                }
                
            }
        }
        
        
    }
    
-(void)checkNextButton
    {
        
        if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        {
            if (selectedQuestionStrRow1.length != 0  && enteredAnswerStrRow1.length != 0)
            {
                //[self.nextButton setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
                
                [self.nextButton setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
            }
            else
            {
                //[self.nextButton setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
                
                [self.nextButton setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
            }
        }
        else
        {
            if (selectedQuestionStrRow1.length != 0 && selectedQuestionStrRow2.length != 0 && enteredAnswerStrRow1.length != 0 && enteredAnswerStrRow2.length !=0)
            {
                //[self.nextButton setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
                [self.nextButton setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
            }
            else
            {
                //[self.nextButton setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
                [self.nextButton setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
            }
        }
        
        
    }
    
#pragma mark - Security Questions resisteration module methods
    
-(void)hitAPIToValidateAnswerWithArray:(NSMutableArray *)questionArray
    {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        // Set the label text.
        hud.label.text = NSLocalizedString(@"loading",nil);
        
        //NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",self.mpinStr,SaltMPIN];
        //NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
        
        
        
        //682647029382
        //    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
        //    NSString *mpinStrEncrypted=[mpinStr sha256HashFor:encrytSHA256];
        //
        //objShared.user_mpin=self.mpinStr; //save value in local db
        
        
        
        UMAPIManager *objRequest = [[UMAPIManager alloc] init];
        
        NSMutableDictionary *dictBody = [NSMutableDictionary new];
        [dictBody setObject:objShared.mobileNumber forKey:@"mno"];//Enter mobile number of user//singleton.mobileNumber closed
        [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
        [dictBody setObject:@"" forKey:@"mpin"];
        [dictBody setObject:questionArray forKey:@"quesAnsList"];
        
        
        if (objShared.user_aadhar_number.length) {
            [dictBody setObject:objShared.user_aadhar_number forKey:@"aadhr"];
        }
        
        //Type for which OTP to be intiate eg register,login,forgot mpin
        NSLog(@"Dict body is :%@",dictBody);
        
        
        [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_VALIDATEANSWER withBody:dictBody andTag:TAG_REQUEST_VALIDATE_ANSWER completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
            [hud hideAnimated:YES];
            
            if (error == nil) {
                NSLog(@"Server Response = %@",response);
                
                //----- below value need to be forword to next view according to requirement after checking Android apk-----
                
                NSString *rc=[response valueForKey:@"rc"];
                NSString *rs=[response valueForKey:@"rs"];
                NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
                
                NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
                //----- End value need to be forword to next view according to requirement after checking Android apk-----
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
                {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kFlagShip_Storyboard bundle:nil];
                    
                    SetMPINRecoveryVC *rpvc = [storyboard instantiateViewControllerWithIdentifier:@"SetMPINRecoveryVC"];
                    rpvc.isQtsn = @"yes";
                    rpvc.arrayQstn = questionArray;
                    [self.navigationController pushViewController:rpvc animated:true];
                }
                
            }
            else
            {
                
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                    message:error.localizedDescription
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                          otherButtonTitles:nil];
                alertView.tag = 1111;
                [alertView show];
                
                
                /* UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                 {
                 UIViewController *vc = self.presentingViewController;
                 while (vc.presentingViewController)
                 {
                 vc = vc.presentingViewController;
                 
                 if ([vc isKindOfClass:[ForgotMPINVC class]])
                 {
                 chekDismiss = YES;
                 [vc dismissViewControllerAnimated:YES completion:nil];
                 }
                 }
                 
                 if (!chekDismiss)
                 {
                 UIViewController *vc1 = self.presentingViewController;
                 while (vc1.presentingViewController) {
                 vc1 = vc1.presentingViewController;
                 }
                 [vc1 dismissViewControllerAnimated:YES completion:NULL];
                 }
                 }];
                 
                 [alert addAction:cancelAction];
                 [self presentViewController:alert animated:YES completion:nil];*/
                
            }
            
        }];
    }
    
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
    {
        if (alertView.tag == 1111 && buttonIndex == 0)
        {
            
            if ([tagCheckRecovery isEqualToString:@"RecoveryTAG"])
            {
                //[self.navigationController popViewControllerAnimated:true];
                NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                
                /*
                 
                 <__NSArrayM 0x6000016d6160>(
                 <TabBarVC_Flag: 0x7fdb250c5c00>,
                 <LoginAppVC: 0x7fdb2611e000>,
                 <UMANG.AccountRecoveryVC: 0x7fdb24daa3e0>,
                 <UMANG.VerifyOTPVC: 0x7fdb26263a00>,
                 <UMANG.RecoveryOptionsVC: 0x7fdb24c82460>,
                 <UpdateQuestionsViewController: 0x7fdb24ed8f80>
                 )
                 
                 */
                
                
                for (UIViewController *aViewController in allViewControllers)
                {
                    if ([aViewController isKindOfClass:[SecuritySettingVC class]])
                    {
                        // [aViewController removeFromParentViewController];
                        [self.navigationController popToViewController:aViewController
                                                              animated:YES];
                        
                        // [self closeView];
                        
                    }
                    else  if ([aViewController isKindOfClass:[AccountRecoveryVC class]])
                    {
                        // [aViewController removeFromParentViewController];
                        [self.navigationController popToViewController:aViewController
                                                              animated:YES];
                        
                        // [self closeView];
                        
                    }
                }
            }
            else
            {
                UIViewController *vc = self.presentingViewController;
                while (vc.presentingViewController)
                {
                    vc = vc.presentingViewController;
                    
                    if ([vc isKindOfClass:[ForgotMPINVC class]])
                    {
                        chekDismiss = YES;
                        [vc dismissViewControllerAnimated:YES completion:nil];
                    }
                }
                
                if (!chekDismiss)
                {
                    UIViewController *vc1 = self.presentingViewController;
                    while (vc1.presentingViewController) {
                        vc1 = vc1.presentingViewController;
                    }
                    [vc1 dismissViewControllerAnimated:YES completion:NULL];
                }
            }
        }
        else if (alertView.tag == 2222 && buttonIndex == 0)
        {
            UIViewController *vc = self.presentingViewController;
            while (vc.presentingViewController) {
                vc = vc.presentingViewController;
                
                if ([vc isKindOfClass:[AadharRegModuleVC class]])
                {
                    [vc dismissViewControllerAnimated:YES completion:nil];
                }
                
            }
        }
    }
-(void)hitAPIAdhaarWithQuestionsArray:(NSMutableArray *)questionArray
    {
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        // Set the label text.
        hud.label.text = NSLocalizedString(@"loading",nil);
        
        NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",self.mpinStr,SaltMPIN];
        NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
        
        
        
        //682647029382
        //    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
        //    NSString *mpinStrEncrypted=[mpinStr sha256HashFor:encrytSHA256];
        //
        objShared.user_mpin=self.mpinStr; //save value in local db
        
        
        
        UMAPIManager *objRequest = [[UMAPIManager alloc] init];
        
        NSMutableDictionary *dictBody = [NSMutableDictionary new];
        [dictBody setObject:@"" forKey:@"mno"];//Enter mobile number of user//singleton.mobileNumber closed
        [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
        [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];
        [dictBody setObject:questionArray forKey:@"quesAnsList"];
        
        
        if (objShared.user_aadhar_number.length) {
            [dictBody setObject:objShared.user_aadhar_number forKey:@"aadhr"];
        }
        
        //Type for which OTP to be intiate eg register,login,forgot mpin
        NSLog(@"Dict body is :%@",dictBody);
        
        
        [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_SET_MPIN withBody:dictBody andTag:TAG_REQUEST_SET_MPIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
            [hud hideAnimated:YES];
            
            if (error == nil) {
                NSLog(@"Server Response = %@",response);
                
                //----- below value need to be forword to next view according to requirement after checking Android apk-----
                
                NSString *rc=[response valueForKey:@"rc"];
                NSString *rs=[response valueForKey:@"rs"];
                NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
                
                NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
                //----- End value need to be forword to next view according to requirement after checking Android apk-----
                NSString *rd=[response valueForKey:@"rd"];
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
                {
                    objShared.user_tkn=tkn;
                    //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                    //[[NSUserDefaults standardUserDefaults]synchronize];
                    
                    //------------------------- Encrypt Value------------------------
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:objShared.user_tkn withKey:@"TOKEN_KEY"];
                    //------NR SET NEW REGISTRATION Start CASE--------------
                    objShared.lastFetchDate=@"NR";
                    [[NSUserDefaults standardUserDefaults] encryptValue:objShared.lastFetchDate withKey:@"lastFetchDate"];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //------------------------- Encrypt Value------------------------
                    
                    
                    
                    
                    // Parse user profile data
                    objShared.objUserProfile = nil;
                    objShared.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                    [self alertOpenWithAdhaarMsg:rd];
                }
                
            }
            else
            {
                
                NSLog(@"Error Occured = %@",error.localizedDescription);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                message:error.localizedDescription
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil];
                alert.tag = 2222;
                [alert show];
                
            }
            
        }];
        
    }
    
-(void)hitAPIWithQuestionArray:(NSMutableArray *)questionArray
    {
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        // Set the label text.
        hud.label.text = NSLocalizedString(@"loading",nil);
        
        NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",self.mpinStr,SaltMPIN];
        NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
        
        
        
        //682647029382
        //    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
        //    NSString *mpinStrEncrypted=[mpinStr sha256HashFor:encrytSHA256];
        //
        objShared.user_mpin = self.mpinStr; //save value in local db
        
        
        
        UMAPIManager *objRequest = [[UMAPIManager alloc] init];
        
        NSMutableDictionary *dictBody = [NSMutableDictionary new];
        [dictBody setObject:objShared.mobileNumber forKey:@"mno"];//Enter mobile number of user
        [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
        [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];
        [dictBody setObject:questionArray forKey:@"quesAnsList"];
        
        
        if (objShared.user_aadhar_number.length) {
            [dictBody setObject:objShared.user_aadhar_number forKey:@"aadhr"];
        }
        
        //Type for which OTP to be intiate eg register,login,forgot mpin
        NSLog(@"Dict body is :%@",dictBody);
        
        
        [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_SET_MPIN withBody:dictBody andTag:TAG_REQUEST_SET_MPIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
            [hud hideAnimated:YES];
            
            if (error == nil) {
                NSLog(@"Server Response = %@",response);
                
                //----- below value need to be forword to next view according to requirement after checking Android apk-----
                
                NSString *rc=[response valueForKey:@"rc"];
                NSString *rs=[response valueForKey:@"rs"];
                NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
                
                NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
                //----- End value need to be forword to next view according to requirement after checking Android apk-----
                NSString *rd=[response valueForKey:@"rd"];
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
                {
                    objShared.user_tkn=tkn;
                    
                    //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                    //[[NSUserDefaults standardUserDefaults]synchronize];
                    
                    
                    
                    //------------------------- Encrypt Value------------------------
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:objShared.user_tkn withKey:@"TOKEN_KEY"];
                    //------NR SET NEW REGISTRATION Start CASE--------------
                    objShared.lastFetchDate=@"NR";
                    [[NSUserDefaults standardUserDefaults] encryptValue:objShared.lastFetchDate withKey:@"lastFetchDate"];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //------------------------- Encrypt Value------------------------
                    
                    
                    // Parse user profile data
                    objShared.objUserProfile = nil;
                    objShared.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                    [self alertwithMsg:rd];
                }
                
            }
            else{
                NSLog(@"Error Occured = %@",error.localizedDescription);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                message:error.localizedDescription
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
            
        }];
        
    }
    
-(void)showToast :(NSString *)toast
    {
        [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
    }
    
-(void)alertOpenWithAdhaarMsg:(NSString *)msg
    {
        //-------Added later---------------
        [[NSUserDefaults standardUserDefaults] setInteger:kDashboardScreenCase forKey:kInitiateScreenKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //-------Added later---------------
        
        
        //869441
        NSString *errormsg=[NSString stringWithFormat:@"%@",NSLocalizedString(@"aadhaar_linked_popup_msg", nil)];
        [self showToast:errormsg];
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ProfileAfterAdharVC *vc = [sb instantiateViewControllerWithIdentifier:@"ProfileAfterAdharVC"];
        // vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        //  [self presentViewController:vc animated:YES completion:NULL];
        
        
        // profileEdit.Edit_Profile_Choose =  IsFromAadharScreen;
        vc.tagFrom=@"ISFROMREGISTRATION";
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
-(void)alertwithMsg:(NSString*)msg
    {
        __block typeof(self) weakSelf = self;
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [weakSelf openNextView];

        }];
        
    }
    
    
    
-(void)openNextView
    {
        //-------Added later---------------
        //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kKeepMeLoggedIn];
        [[NSUserDefaults standardUserDefaults] setInteger:kDashboardScreenCase forKey:kInitiateScreenKey];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        //-------Added later---------------
        
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ProfileAfterMobileVC *profileEdit = [sb instantiateViewControllerWithIdentifier:@"ProfileAfterMobileVC"];
        // profileEdit.Edit_Profile_Choose = IsFromMobileScreen;
        profileEdit.tagFrom=@"ISFROMREGISTRATION";
        [profileEdit setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:profileEdit animated:NO completion:nil];
        
        /*AadharRegAfterMobileVC *vc;
         if ([[UIScreen mainScreen]bounds].size.height == 1024)
         {
         vc = [[AadharRegAfterMobileVC alloc] initWithNibName:@"AadharRegAfterMobileVC_iPad" bundle:nil];
         //loadNibNamed:@"MobileRegistrationVC_ipad" owner:self options:nil];
         // cell = (MobileRegistrationVC_ipad *)[nib objectAtIndex:0];
         [self presentViewController:vc animated:YES completion:nil];
         
         
         }
         
         else
         
         {
         
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
         vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharRegAfterMobileVC"];
         //profileEdit.tagFrom=@"ISFROMREGISTRATION";
         [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
         [self presentViewController:vc animated:NO completion:nil];
         }*/
    }
#pragma mark - UITableView Delegate and Data Source Methods
    
    
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
    {
        QuestionCell *questionTableCell = (QuestionCell *)cell;
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 44)];
        questionTableCell.answerTextfield.leftView = paddingView;
        questionTableCell.answerTextfield.leftViewMode = UITextFieldViewModeAlways;
        
    }
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        {
            return 1;
        }
        
        return 2;
    }
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        return 160.0f;
    }
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        QuestionCell *cell = (QuestionCell *)[tableView dequeueReusableCellWithIdentifier:@"questionCellIdentifier"];
        
        cell = nil;
        
        if (cell == nil)
        {
            cell = (QuestionCell *)[tableView dequeueReusableCellWithIdentifier:@"questionCellIdentifier"];
        }
        
        /*for (id object in cell.contentView.subviews)
         {
         [object removeFromSuperview];
         }*/
        
        cell.changeQuestionButton.tag = indexPath.row;
        cell.answerTextfield.tag = indexPath.row;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.changeButton setTitle:[NSLocalizedString(@"change", nil) uppercaseString] forState:UIControlStateNormal];
        cell.questionNumberLabel.text = [NSString stringWithFormat:@"Question %ld.",indexPath.row + 1];
        
        if (indexPath.row == 0)
        {
            cell.questionNumberLabel.text = NSLocalizedString(@"question_1", nil);
            if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
            {
                cell.questionNumberLabel.text = [[NSLocalizedString(@"question_1", nil) componentsSeparatedByString:@" "] firstObject];
                
            }
        }
        else if (indexPath.row == 1)
        {
            cell.questionNumberLabel.text = NSLocalizedString(@"question_2", nil);
        }
        
        cell.answerTextfield.placeholder = NSLocalizedString(@"your_answer", nil);
        
        //[self.tagComingFrom isEqualToString:@"Set MPIN"]
        //[self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"]
        //if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        
        if ([self.tagComingFrom isEqualToString:@"Set MPIN"] || [self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"])
        {
            [cell.changeQuestionButton setTitle:NSLocalizedString(@"choose_question", nil) forState:UIControlStateNormal];
        }
        else if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        {
            [cell.changeQuestionButton setTitle:NSLocalizedString(@"choose_question", nil) forState:UIControlStateNormal];
        }
        else
        {
            [cell.changeQuestionButton setTitle:NSLocalizedString(@"choose_question", nil) forState:UIControlStateNormal];
        }
        
        
        
        if (cell.changeQuestionButton.tag == 0)
        {
            
            if (selectedQuestionStrRow1.length == 0)
            {
                
                
                cell.answerTextfield.userInteractionEnabled = NO;
            }
            else
            {
                
                cell.answerTextfield.userInteractionEnabled = YES;
                [cell.changeQuestionButton setTitle:selectedQuestionStrRow1 forState:UIControlStateNormal];
            }
            
            
            cell.answerTextfield.text = enteredAnswerStrRow1;
            
            
            if (selectedQuestionStrRow1 == nil || [selectedQuestionStrRow1 isEqualToString:@""])
            {
                cell.changeButton.hidden = YES;
                cell.answerTextfield.alpha=0.8;
                cell.changeQuestionView.alpha=0.8;
                
            }
            else
            {
                cell.changeButton.hidden = NO;
                cell.answerTextfield.alpha=1;
                cell.changeQuestionView.alpha=1;
                if ([cell.answerTextfield.text length]==0)
                {
                    [cell.answerTextfield becomeFirstResponder];
                    
                }
                
            }
            
        }
        else if (cell.changeQuestionButton.tag == 1)
        {
            
            if (selectedQuestionStrRow1.length == 0)
            {
                cell.answerTextfield.userInteractionEnabled = NO;
                cell.changeQuestionButton.userInteractionEnabled = NO;
            }
            else
            {
                cell.changeQuestionButton.userInteractionEnabled = YES;
                
                
                if (selectedQuestionStrRow2.length == 0)
                {
                    cell.answerTextfield.userInteractionEnabled = NO;
                }
                else
                {
                    cell.answerTextfield.userInteractionEnabled = YES;
                    // [cell.answerTextfield becomeFirstResponder];
                }
                
            }
            
            cell.answerTextfield.text = enteredAnswerStrRow2;
            
            
            if (selectedQuestionStrRow2 == nil || [selectedQuestionStrRow2 isEqualToString:@""])
            {
                cell.changeButton.hidden = YES;
                cell.answerTextfield.alpha=0.8;
                cell.changeQuestionView.alpha=0.8;
            }
            else
            {
                cell.answerTextfield.alpha=1;
                cell.changeQuestionView.alpha=1;
                
                cell.changeButton.hidden = NO;
                [cell.changeQuestionButton setTitle:selectedQuestionStrRow2 forState:UIControlStateNormal];
                cell.answerTextfield.userInteractionEnabled = YES;
                // [cell.answerTextfield becomeFirstResponder];
                if ([cell.answerTextfield.text length]==0)
                {
                    [cell.answerTextfield becomeFirstResponder];
                    
                }
            }
            
            
        }
        
        
        cell.changeQuestionButton.tag = indexPath.row;
        cell.changeButton.tag = indexPath.row;
        
        [cell.changeQuestionButton addTarget:self action:@selector(questionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.changeButton addTarget:self action:@selector(questionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.questionNumberLabel.font = [AppFont mediumFont:17.0];
        cell.changeButton.titleLabel.font = [AppFont semiBoldFont:16.0];
        
        if (fDeviceWidth == 320)
        {
            cell.changeQuestionButton.titleLabel.font = [AppFont regularFont:14];
            cell.answerTextfield.font = [AppFont regularFont:14];
            cell.questionNumberLabel.font = [AppFont mediumFont:16.0];
            cell.changeButton.titleLabel.font = [AppFont semiBoldFont:14.0];
        }
        else
        {
            cell.changeQuestionButton.titleLabel.font = [AppFont regularFont:16.0];
            cell.answerTextfield.font = [AppFont regularFont:16.0];
        }
        
        
        return cell;
    }
    
    
-(void)questionButtonClicked:(UIButton *)sender
    {
        NSLog(@"Question clicked");
        
        
        selectedButtonIndexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
        
        if (![self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        {
            if (selectedButtonIndexPath.row == 0)
            {
                NSLog(@"%@",selectedQuestionStrRow2);
                
                objShared.questionsArray = [objShared createQuestionArray];
                
                [objShared.questionsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    NSDictionary *dict = obj;
                    
                    if ([[dict valueForKey:@"question"] isEqualToString:selectedQuestionStrRow2])
                    {
                        [objShared.questionsArray removeObject:obj];
                        *stop = YES;
                    }
                    
                }];
                
            }
            else
            {
                NSLog(@"%@",selectedQuestionStrRow1);
                
                objShared.questionsArray = [objShared createQuestionArray];
                
                [objShared.questionsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    NSDictionary *dict = obj;
                    
                    if ([[dict valueForKey:@"question"] isEqualToString:selectedQuestionStrRow1])
                    {
                        [objShared.questionsArray removeObject:obj];
                        *stop = YES;
                    }
                    
                }];
            }
        }
        else
        {
            
            
        }
        
        
        
        
        
        [self showQuestions];
        
    }
    
-(void)showQuestions
    {
        
        /*CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"choose_question", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"Cancel", nil)];
         picker.delegate = self;
         picker.dataSource = self;
         picker.allowMultipleSelection = NO;
         picker.allowRadioButtons = NO;
         picker.needFooterView = NO;
         picker.headerTitleColor = [UIColor colorWithRed:12.0/255.0 green:97.0/255.0 blue:161.0/255.0 alpha:1.0];
         picker.isClearOptionRequired = NO;
         picker.isLanguageScreenSelected = YES;
         [picker show];*/
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DetailService" bundle:nil];
        
        __block QuestionsViewController *customVC = [storyboard instantiateViewControllerWithIdentifier:@"QuestionsViewController"];
        
        customVC.screenTitleString = NSLocalizedString(@"choose_question", nil);
        customVC.questionsArray = [NSMutableArray new];
        customVC.questionsArray = objShared.questionsArray;
        customVC.tagComingFrom = self.tagComingFrom;
        __weak typeof(self) weakSelf = self;
        
        customVC.finishState = ^(NSString *questionString) {
            QuestionCell* cell = (QuestionCell *)[weakSelf tableView:weakSelf.questionTable cellForRowAtIndexPath:selectedButtonIndexPath];
            if (selectedButtonIndexPath.row == 0)
            {
                selectedQuestionStrRow1 = questionString;
            }
            else
            {
                selectedQuestionStrRow2 = questionString;
            }
            [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                [cell.changeQuestionButton setTitle:questionString forState:UIControlStateNormal];
                [weakSelf checkNextButton];
                [weakSelf.questionTable reloadData];
                //[customVC dismissViewControllerAnimated:false completion:nil];
            }];
        };
        
        
        if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
        {
            //[customVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:customVC animated:YES completion:nil];
        }
        else if ([self.tagComingFrom isEqualToString:@"Set MPIN"])
        {
            //[customVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:customVC animated:YES completion:nil];
        }
        else if ([self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"])
        {
            //[customVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:customVC animated:YES completion:nil];
        }
        else
        {
            [self.navigationController pushViewController:customVC animated:YES];
        }
        
    }
#pragma mark- add Navigation View to View
    
-(void)addNavigationView{
    btnBack.hidden = true;
    _securityQuestionLabel.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backButtonPressed:btnBack];
    };
    nvView.lblTitle.text = _securityQuestionLabel.text;
    
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tbFrame = self.questionTable.frame;
        tbFrame.origin.y = kiPhoneXNaviHeight
        self.questionTable.frame = tbFrame;
        [self.view layoutIfNeeded];
    }
}
    
#pragma mark - CZPicker delegate Methods
    
- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row
    {
        
        NSString *selectedQuestionString = [[objShared.questionsArray objectAtIndex:row] valueForKey:@"question"];
        
        QuestionCell* cell = (QuestionCell *)[self tableView:self.questionTable cellForRowAtIndexPath:selectedButtonIndexPath];
        
        [cell.changeQuestionButton setTitle:selectedQuestionString forState:UIControlStateNormal];
        
        
        if (selectedButtonIndexPath.row == 0)
        {
            selectedQuestionStrRow1 = selectedQuestionString;
        }
        else
        {
            selectedQuestionStrRow2 = selectedQuestionString;
        }
        
        [self checkNextButton];
        
        [self.questionTable reloadData];
    }
    
- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row
    {
        
        return [[objShared.questionsArray objectAtIndex:row] valueForKey:@"question"];
        
    }
    
- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
    {
        return objShared.questionsArray.count;
    }
    
- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView
    {
        NSLog(@"Canceled.");
    }
    
#pragma mark - UITextField Delegate Methods
    
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
    {
        return YES;
    }
- (void)textFieldDidBeginEditing:(UITextField *)textField
    {
        if (self.view.frame.size.width == 320)
        {
            if (textField.tag == 0)
            {
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
                }];
            }
            else if (textField.tag == 1)
            {
                
                if (self.view.frame.size.height < 500)
                {
                    self.securityDescriptionLabel.hidden = YES;
                }
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - 120, self.view.frame.size.width, self.view.frame.size.height);
                }];
            }
            
        }
        
        else if (self.view.frame.size.width == 375)
        {
            if (textField.tag == 1)
            {
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - 20, self.view.frame.size.width, self.view.frame.size.height);
                }];
            }
        }
        else if (self.view.frame.size.width == 414)
        {
            if (textField.tag == 1)
            {
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
                }];
            }
        }
    }
    
- (void)textFieldDidEndEditing:(UITextField *)textField
    {
        
        if (self.view.frame.size.height < 500)
        {
            self.securityDescriptionLabel.hidden = NO;
        }
        [UIView animateWithDuration:0.3 animations:^{
            
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        }];
        
        if (textField.tag == 0)
        {
            enteredAnswerStrRow1 = textField.text;
        }
        else if (textField.tag == 1)
        {
            enteredAnswerStrRow2 = textField.text;
        }
        
        [self checkNextButton];
        [self setFontforView:self.view andSubViews:YES];
    }
    
    
- (BOOL)textFieldShouldReturn:(UITextField *)textField
    {
        [textField resignFirstResponder];
        return YES;
    }
    
#pragma mark
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    
    @end


#pragma mark - Question Cell Interface and Implementation

@interface QuestionCell()
    @end

@implementation QuestionCell
    
    @end

