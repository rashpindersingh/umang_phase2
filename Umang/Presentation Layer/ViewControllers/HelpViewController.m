//
//  HelpViewController.m
//  Umang
//
//  Created by spice on 21/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "HelpViewController.h"
#import "HelpCell.h"
#import "LiveChatVC.h"
#import "FAQWebVC.h"
#import "FeedbackVC.h"
#import "SubmitQueryVC.h"
#import "ReportIssueVC.h"
#import <MessageUI/MessageUI.h>
#import "UmangWebChatVC.h"
#import "EmailSupportViewController.h"

@interface HelpViewController ()<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,UIGestureRecognizerDelegate>
{
    SharedManager *singleton;
}
@property(nonatomic,retain)NSMutableArray *table_data;
@property(nonatomic,retain)NSMutableArray *table_imgdata;
@property(nonatomic,retain)IBOutlet UITableView *table_HelpView;


@end

@implementation HelpViewController
@synthesize table_data;
@synthesize table_imgdata;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: HELP_VIEW_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
        
    singleton = [SharedManager sharedSingleton];
    
    //call customer care
    
    _lblHelp.text = NSLocalizedString(@"help_and_support", nil);
    [_btnBackMore setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_btnBackMore.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(_btnBackMore.frame.origin.x, _btnBackMore.frame.origin.y, _btnBackMore.frame.size.width, _btnBackMore.frame.size.height);
        [_btnBackMore setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _btnBackMore.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    /* table_data = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"help_phone_help", nil),NSLocalizedString(@"help_email_help", nil),NSLocalizedString(@"help_live_chat", nil),NSLocalizedString(@"help_report_an_issue", nil),NSLocalizedString(@"help_faq", nil),NSLocalizedString(@"user_manual", nil), nil];
     
     table_imgdata = [[NSMutableArray alloc]initWithObjects:@"phone_helpline.png",@"email_help.png",@"live_chat.png",@"report_issue",@"faq_NewHelpIcon",@"user_manual.png", nil];
     */
    
    if (singleton.user_tkn.length == 0 && [self.comingFrom isEqualToString:@"accountRecovery"])
    {
        table_data = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"help_phone_help", nil),NSLocalizedString(@"help_email_help", nil),NSLocalizedString(@"help_faq", nil),NSLocalizedString(@"user_manual", nil), nil];
        
        table_imgdata = [[NSMutableArray alloc]initWithObjects:@"phone_helpline.png",@"email_help.png",@"faq_Updated",@"user_manual.png", nil];
    }
    else
    {
        table_data = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"help_phone_help", nil),NSLocalizedString(@"help_email_help", nil),NSLocalizedString(@"help_live_chat", nil),NSLocalizedString(@"help_faq", nil),NSLocalizedString(@"user_manual", nil), nil];
        
        table_imgdata = [[NSMutableArray alloc]initWithObjects:@"phone_helpline.png",@"email_help.png",@"live_chat.png",@"faq_Updated",@"user_manual.png", nil];
    }
    
    
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    [self addNavigationView];
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    if (scrollView.contentOffset.y<=0) {
//        scrollView.contentOffset = CGPointZero;
//    }
//}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    _btnBackMore.hidden = true;
    _lblHelp.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backbtnAction:btnBack];
    };
    nvView.lblTitle.text = NSLocalizedString(@"help_and_support", "");
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tbFrame = table_helpView.frame;
        tbFrame.origin.y = kiPhoneXNaviHeight
        table_helpView.frame = tbFrame;
        [self.view layoutIfNeeded];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:NO];
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [_btnBackMore.titleLabel setFont:[AppFont regularFont:17.0]];
    _lblHelp.font = [AppFont semiBoldFont:17];
    
}
-(void)setHeightOfTableView
{
    
    /**** set frame size of tableview according to number of cells ****/
    float height=45.0f*[table_data count]+80.0f;
    
    [table_helpView setFrame:CGRectMake(table_helpView.frame.origin.x, table_helpView.frame.origin.y, table_helpView.frame.size.width,height-3)];
    
    if (table_helpView.frame.size.height < fDeviceHeight-65)
    {
        table_helpView.scrollEnabled = NO;
    }
    else
    {
        table_helpView.scrollEnabled = YES;
        
    }
    //table_helpView.layer.backgroundColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
    table_helpView.layer.borderWidth=0.5;
    table_helpView.layer.borderColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
    
    
}




- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Here you must return the number of sectiosn you want
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //Here, for each section, you must return the number of rows it will contain
    if (section==0)
    {
        if (singleton.user_tkn.length == 0 && [self.comingFrom isEqualToString:@"accountRecovery"])
        {
            return 2;
        }
        return 3;
    }
    else
        return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //For each section, you must return here it's label
    if(section == 0) return NSLocalizedString(@"customer_care_lbl", nil);
    else
        
        return NSLocalizedString(@"more_help_lbl", nil);
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  40;
}




- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    //check header height is valid
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
    headerView.backgroundColor=[UIColor clearColor];
    
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(15,15,300, 21);
    
    CGRect labelFrame =  label.frame;
    labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
    label.frame = labelFrame;
    
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:0.298 green:0.337 blue:0.424 alpha:1.0];
    
    label.font = [UIFont systemFontOfSize:14];
    
    
    
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    
    
    [headerView addSubview:label];
    
    return headerView;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"HelpCell";
    
    HelpCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (cell == nil) {
        cell = [[HelpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    // cell.lbl_celltitle.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    // update frame
    
    /*
     CGRect imgvwFrame = cell.img_cell.frame;
     imgvwFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 80 : 15;
     cell.img_cell.frame = imgvwFrame;
     CGRect lblFrame = cell.lbl_celltitle.frame;
     lblFrame.origin.x = singleton.isArabicSelected ? 100 : 60;
     cell.lbl_celltitle.frame = lblFrame;
     */
    if (singleton.isArabicSelected==TRUE)
    {
        cell.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        cell.img_cell.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        cell.lbl_celltitle.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        cell.lbl_celltitle.textAlignment= NSTextAlignmentRight;
    }
    
    if ([indexPath section]==0) {
        cell.lbl_celltitle.text=[table_data objectAtIndex:indexPath.row];
        //cell.img_cell.image=[UIImage imageNamed:[table_imgdata objectAtIndex:indexPath.row]];
        
        
        dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(concurrentQueue, ^{
            UIImage *image = nil;
            image = [UIImage imageNamed:[table_imgdata objectAtIndex:indexPath.row]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.img_cell.image=image;
                cell.img_cell.contentMode=UIViewContentModeScaleAspectFit;
                
            });
            
        });
        
    }
    else
    {
        
        NSInteger rowSelection;
        
        if (singleton.user_tkn.length == 0 && [self.comingFrom isEqualToString:@"accountRecovery"])
        {
            rowSelection = indexPath.row + 2;
        }
        else
        {
            rowSelection = indexPath.row + 3;
        }
        
        cell.lbl_celltitle.text=[table_data objectAtIndex:rowSelection];
        // cell.img_cell.image=[UIImage imageNamed:[table_imgdata objectAtIndex:indexPath.row+3]];
        
        dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(concurrentQueue, ^{
            UIImage *image = nil;
            image = [UIImage imageNamed:[table_imgdata objectAtIndex:rowSelection]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.img_cell.image=image;
                cell.img_cell.contentMode=UIViewContentModeScaleAspectFit;
                
            });
            
        });
        
        
    }    
    
    cell.lbl_celltitle.font = [AppFont regularFont:17];

    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section]==0)
    {
        
        if (indexPath.row==0)//Phone Call
        {

            if (singleton.user_tkn.length == 0 && [self.comingFrom isEqualToString:@"accountRecovery"])
            {
                NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",singleton.phoneSupport];
                
                mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
            }
            else
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
                
                EmailSupportViewController *emailSupportVC = [storyboard instantiateViewControllerWithIdentifier:@"EmailSupport"];
                
                emailSupportVC.comingFrom = @"PhoneSupport";
                [self.navigationController pushViewController:emailSupportVC animated:YES];
            }
            
        }
        
        if (indexPath.row==1)//Email
        {
            
            if (singleton.user_tkn.length == 0 && [self.comingFrom isEqualToString:@"accountRecovery"])
            {
                MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
                if ([MFMailComposeViewController canSendMail]) {
                    
                    
                    NSString *receipt=singleton.emailSupport;
                    // Email Subject
                    NSString *emailTitle = @"Umang Services Email";
                    // Email Content
                    NSString *messageBody = @"Sent by Umang App!";
                    // To address
                    // To address
                    NSArray *toRecipents = [NSArray arrayWithObject:receipt];
                    
                    picker.mailComposeDelegate = self;
                    [picker setSubject:emailTitle];
                    [picker setMessageBody:messageBody isHTML:YES];
                    [picker setToRecipients:toRecipents];
                    
                    // Present mail view controller on screen
                    [self presentViewController:picker animated:YES completion:NULL];
                }
            }
            else
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
                
                EmailSupportViewController *emailSupportVC = [storyboard instantiateViewControllerWithIdentifier:@"EmailSupport"];
                
                emailSupportVC.comingFrom = @"EmailSupport";
                [self.navigationController pushViewController:emailSupportVC animated:YES];
            }
            
        }
        if (indexPath.row==2)//Live Chat
        {
            
            [self openXmppChat];
            
            //[self LiveChatOpen];
            
            /*XMPPChatViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"XMPPChat"];
             vc.comingFromString = @"Help";
             vc.hidesBottomBarWhenPushed = YES;
             
             [self.navigationController pushViewController:vc animated:YES];*/
            
        }
        /* if (indexPath.row==3)//Submit a Query
         {
         SubmitQueryVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SubmitQueryVC"];
         
         vc.hidesBottomBarWhenPushed = YES;
         
         [self.navigationController pushViewController:vc animated:YES];
         
         
         }*/
        
        if (indexPath.row==3)//Submit a Query
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

            ReportIssueVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ReportIssueVC"];
            
            vc.hidesBottomBarWhenPushed = YES;
            
            [self.navigationController pushViewController:vc animated:YES];
            
            
        }
        
        
        
    }
    else
    {
        if (indexPath.row==0)//FAQ
        {
            [self openFAQWebVC];
            
        }
        if (indexPath.row==1)//User Manual
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

            FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
            if ([singleton.arr_initResponse  count]>0) {
                vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"usrman"];
                
            }
            vc.titleOpen= NSLocalizedString(@"user_manual", nil);
            //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self.navigationController pushViewController:vc animated:YES];
            
            
            
            
        }
        //    if (indexPath.row==2) //close for now
        //    {
        //
        //
        //
        //    }
    }
    
}

-(void)openXmppChat
{
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Chat" bundle:nil];
    LiveChatVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LiveChatVC"];
    vc.hidesBottomBarWhenPushed = YES;
    vc.comingFromString = @"Help";
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)emailAction
{
    //  [dic_serviceInfo valueForKey:@"SERVICE_EMAIL"]
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        
        NSString *receipt=singleton.emailSupport;
        // Email Subject
        NSString *emailTitle = NSLocalizedString(@"customer_support", nil);
        // Email Content
        NSString *messageBody = @"Sent by Umang App!";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:receipt];
        
        picker.mailComposeDelegate = self;
        [picker setSubject:emailTitle];
        [picker setMessageBody:messageBody isHTML:YES];
        [picker setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    
    
    
    // UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    // [topvc presentViewController:mc animated:NO completion:nil];
    
    
    
}

//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}



- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}




/*singleton = [SharedManager sharedSingleton];
 
 urlString=[singleton.arr_initResponse valueForKey:@"faq"];
 */

-(void)openFAQWebVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.isfrom = @"Settings";
    
    if ([singleton.arr_initResponse  count]>0)
    {
        vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"faq"];
    }
    
    vc.titleOpen= NSLocalizedString(@"help_faq", nil);
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)LiveChatOpen
{
    NSLog(@"My LiveChatOpen Action");
    
    // SettingsViewController
    
    /* UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Under Construction" message:@"This module is under construction" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
     [alert show];
     
     */
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    UmangWebChatVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UmangWebChatVC"];
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


-(IBAction)backbtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
