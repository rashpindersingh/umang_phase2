//
//  ServiceDirectoryVC.m
//  Umang
//
//  Created by admin on 10/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ServiceDirectoryVC.h"
#import "ServiceDirDetailVC.h"
#import "UMAPIManager.h"
#import "SharedManager.h"
#import "RunOnMainThread.h"
#import "UIImageView+WebCache.h"
#import "ServiceDirDetails.h"
#import "MBProgressHUD.h"


#import "StateList.h"
#import "CustomPickerVC.h"
#import "UMAPIManager.h"


@interface ServiceDirectoryVC ()
{
    
    NSDictionary *services;
    NSArray *serviceSectionTitles;
    NSArray *serviceIndexTitles;
    
    __weak IBOutlet UIButton *btnState;
    SharedManager *singleton;
    //NSMutableArray *arrDirectoryData;
    
    NSMutableArray *serviceOnUmangArray;
    NSMutableArray *serviceNotOnUmangArray;
    
    CGFloat _currentKeyboardHeight;
    CGFloat tableHeight;
    MBProgressHUD *hud ;

    
    CustomPickerVC *customVC;
    StateList *obj;
    dispatch_group_t serviceGroup;
    dispatch_group_t stateApiGroup;
    
    IBOutlet NSLayoutConstraint *headerHeightConstraint;
}
@property (nonatomic, strong)NSString *SD_state_id;
@property (nonatomic, strong)NSString *SD_oldStateId;



@end

@implementation ServiceDirectoryVC

#pragma mark- Font Set to View
-(void)setViewFont
{
    [_backButton.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    self.screenTitle.font = [AppFont semiBoldFont:17.0];
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
#pragma mark - View LifeCycle
-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.SD_oldStateId =  singleton.user_SeriveDirStateId;
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setStateID];

    [super viewWillAppear:NO];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    
    [self.backButton setTitle:@"Back" forState:UIControlStateNormal];
    self.screenTitle.text = @"Service Directory";
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    NSLog(@"self.SD_oldStateId====> %@ \n self.SD_state_id =%@",self.SD_oldStateId,self.SD_state_id);

    
    
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
  //  if (self.SD_oldStateId != nil && self.SD_oldStateId.length != 0) {
        if ([self.SD_oldStateId isEqualToString:self.SD_state_id])
        {
            NSLog(@"Do nothing state not changed");
        }
        else
        {
            NSLog(@"Do  state  changed");

            [self startDispatchGroup];

        }
   // }
        }];
    [self setViewFont];
    [self btnStateLayoutChanges];
    
}


// Call this funtion by passing value to check umang service with state wise
-(void)listOfAvailableServiceinUmangWithStateId :(NSString*)state_id
{
    
    NSArray *ServiceOnUmangPreferState=[singleton.dbManager getStateWiseUMANGServicesDirData:state_id];
    
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:SERVICE_DIRECTORY_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    singleton = [SharedManager sharedSingleton];
    obj=[[StateList alloc]init];
    self.SD_state_id = @"";

    [self startStateListGroup];

    // [self listOfAvailableServiceinUmangWithStateId : @"20"]; //test purpose open here close it
    //arrDirectoryData=[NSMutableArray new];
    
    serviceOnUmangArray = [NSMutableArray new];
    serviceNotOnUmangArray = [NSMutableArray new];
    
    
    //self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    
    self.view.backgroundColor= [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.noRecordView.hidden = YES;
    
    self.tblServiceDirectory.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.segmentController setTitle:@"Services on UMANG" forSegmentAtIndex:0];
    [self.segmentController setTitle:@"" forSegmentAtIndex:1];
    [self.segmentController setTitle:@"Other Services" forSegmentAtIndex:2];
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    [self.segmentController setTitleTextAttributes:attributes
                                          forState:UIControlStateNormal];
    
    self.searchBar.placeholder = @"Search";
    self.noRecordFoundLabel.text = @"No services available";
    
    self.segmentController.translatesAutoresizingMaskIntoConstraints = YES;

    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
       // self.segmentController.translatesAutoresizingMaskIntoConstraints = YES;
        self.segmentController.frame = CGRectMake(self.view.frame.size.width/2 - 200, self.segmentController.frame.origin.y, 400, self.segmentController.frame.size.height);
        [self.segmentController setWidth:200 forSegmentAtIndex:0];
        [self.segmentController setWidth:0 forSegmentAtIndex:1];
        [self.segmentController setWidth:200 forSegmentAtIndex:2];
    }
    else
    {
        self.segmentController.frame = CGRectMake(10, self.segmentController.frame.origin.y, self.view.frame.size.width-20, self.segmentController.frame.size.height);
        
        [self.segmentController setWidth:(self.segmentController.frame.size.width)/2 forSegmentAtIndex:0];
        [self.segmentController setWidth:0 forSegmentAtIndex:1];
        [self.segmentController setWidth:(self.segmentController.frame.size.width)/2 forSegmentAtIndex:2];
        
    }
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
    [self hitAPIToFetchServiceDirectory];
    btnState.hidden=false;

    [self fetchDataAccordingToSegment];
    
    
    UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    
    // To change background color
    //searchField.backgroundColor = [UIColor colorWithRed:232.0/255.0f green:232.0/255.0f blue:234.0/255.0f alpha:1.0f];
    //searchField.backgroundColor = [UIColor redColor];
    
    // To change text color
    searchField.textColor = [UIColor blackColor];
    
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor colorWithRed:100.0/255.0f green:100.0/255.0f blue:100.0/255.0f alpha:1.0f];
    
    
    if ([[UIScreen mainScreen]bounds].size.height == 812)
    {
        self.backButtonTopConstraint.constant = -5.0f;
        self.screenTitleTopConstraint.constant = -10.0f;
    }
    [self btnStateLayoutChanges];
    
}

#pragma mark -

- (UIStatusBarStyle)preferredStatusBarStyle

{
    return UIStatusBarStyleDefault;
}

- (IBAction)btnMoreClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)fetchDataAccordingToSegment
{
    if (self.segmentController.selectedSegmentIndex == 0)
    {
        [self fetchDatafromDB];
    }
    else
    {
        [self getServicesNotOnUmang];
    }
}

#pragma mark - UISegment Selector Method

- (IBAction)segmentValueChange:(UISegmentedControl *)sender
{
    self.searchBar.text = @"";
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar resignFirstResponder];
    
    switch (sender.selectedSegmentIndex) {
        case 0:
            btnState.hidden=false;
            headerHeightConstraint.constant = 120.0f;
            [self fetchDataAccordingToSegment];
            break;
        case 1:
            break;
          
        case 2:
            btnState.hidden=true;
            headerHeightConstraint.constant = 80.0f;
            [self fetchDataAccordingToSegment];
            break;
        default:
            // [self reloadAfterDelay];
            break;
    }
}

-(void)setStateID
{
    
    singleton.user_SeriveDirStateId = [singleton getServiceDirStateId];
    
    NSLog(@"user_StateId user_StateId%@",singleton.user_SeriveDirStateId);
    if (singleton.user_SeriveDirStateId.length != 0)
    {
       // singleton.serviceDirstateSelected = [obj getStateName:singleton.user_SeriveDirStateId];
        singleton.serviceDirstateSelected = [singleton.dbManager getStateEnglishName:singleton.user_SeriveDirStateId];

        
        
    }
    if ([[singleton.serviceDirstateSelected uppercaseString] isEqualToString:@"ALL"] || singleton.serviceDirstateSelected == nil || singleton.serviceDirstateSelected.length == 0 ||
        [[singleton.serviceDirstateSelected uppercaseString] isEqualToString:NSLocalizedString(@"all", nil)])
    {
        // do nothing make it blank
        self.SD_state_id= @"";
    }
    //self.SD_state_id = [obj getStateCode:singleton.serviceDirstateSelected];
    
   self.SD_state_id  = [singleton.dbManager getStateCodeEnglish:singleton.serviceDirstateSelected];

    
    
}


#pragma mark - Button State Action with Layout Changes...

- (IBAction)didTapBtnStateAction:(UIButton *)sender
{
    [singleton traceEvents:@"State Change Button" withAction:@"Clicked" withLabel:@"Service Directory" andValue:0];
    singleton.serviceDirstateCurrent = self.SD_state_id;
    [self initStateData];
}

-(void)initStateData
{
    
    singleton = [SharedManager sharedSingleton];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    customVC = [storyboard instantiateViewControllerWithIdentifier:@"CustomPickerVC"];
    
   
    NSMutableArray *tempServiceArray = [[singleton.dbManager getServiceStateAvailableInEnglishOnly] mutableCopy];

   // NSMutableArray *tempServiceArray = [[singleton.dbManager getServiceStateAvailable] mutableCopy];
    
    [tempServiceArray addObject:@"All"];
    NSArray  * servicesArray = tempServiceArray;
    
    servicesArray = [servicesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];


    // NSArray *upcomingServicesArray = [singleton.dbManager getUpcommingStates];
   // upcomingServicesArray = [upcomingServicesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    if ([[singleton.serviceDirstateSelected uppercaseString] isEqualToString:@"ALL"] || singleton.serviceDirstateSelected == nil || singleton.serviceDirstateSelected.length == 0 ||
        [[singleton.serviceDirstateSelected uppercaseString] isEqualToString:NSLocalizedString(@"all", nil)])
    {
        singleton.serviceDirstateSelected = @"All";
    }
    
    customVC.delegate=self;
    
    //customVC.get_title_pass = NSLocalizedString(@"choose_your_state", nil);
    
    //should be in english only
    customVC.get_title_pass =@"Choose Your State";
    customVC.get_arr_element=[servicesArray mutableCopy];
   // customVC.arrUpcomingState = [upcomingServicesArray mutableCopy]; //code commit as not to show upcomming array
    customVC.get_TAG=TAG_STATE_SERVICEDIR;
    [self.navigationController pushViewController:customVC animated:YES];
    
}


-(void)callStateAPI
{
    //[obj hitStateQualifiAPI];
    [self hitStateQualifiAPI];
}


-(void)hitStateQualifiAPI
{
    
    //singleton = [SharedManager sharedSingleton];
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];//Enter mobile number of user
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    __weak typeof(self) weakSelf = self;
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FSTQU withBody:dictBody andTag:TAG_REQUEST_UNSET_FAVORITE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            singleton.statesList=[[NSMutableArray alloc]init];
            singleton.qualList=[[NSMutableArray alloc]init];
            singleton.occuList=[[NSMutableArray alloc]init];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                singleton.statesList=[[response valueForKey:@"pd"]valueForKey:@"statesList"];
                singleton.qualList=[[response valueForKey:@"pd"]valueForKey:@"qualList"];
                singleton.occuList=[[response valueForKey:@"pd"]valueForKey:@"occuList"];
                
            }
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            
        }
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [weakSelf setStateID];
            [weakSelf stateNameDelay];
        }];
        if (stateApiGroup != nil) {
            dispatch_group_leave(stateApiGroup);
            // serviceGroup = nil ;
        }
    }];
}

-(void)refreshData
{
    serviceGroup = nil;
    stateApiGroup = nil;
    __weak typeof(self) weakSelf = self;
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [weakSelf startDispatchGroup];
    }];
  
}


#pragma mark - Dispathc Group All Api Data

-(void)startStateListGroup {
    
    if (stateApiGroup == nil ) {
        stateApiGroup = dispatch_group_create();
    }
    dispatch_group_enter(stateApiGroup);
    [self callStateAPI];
    __weak typeof(self) weakSelf = self;
    dispatch_group_notify(stateApiGroup,dispatch_get_main_queue(),^{
        // Assess any errors
        [weakSelf startDispatchGroup];
        stateApiGroup = nil ;
        // Now call the final completion block
    });
    return;
    
    
}
-(void)startDispatchGroup
{
    if (serviceGroup == nil )
    {
        serviceGroup = dispatch_group_create();
    }
    // Create the dispatch group
    // Start the first service
    if (![singleton.serviceDirstateCurrent isEqualToString:singleton.user_SeriveDirStateId])
    {
        NSLog(@"self.SD_state_id ==== >%@",self.SD_state_id);
        if ([self.SD_state_id isEqualToString:@"9999"])
        {
         //do nothing
        }
        else
        {
            dispatch_group_enter(serviceGroup);
            [self fetchDataAccordingToSegment];

        }
    }
    dispatch_group_enter(serviceGroup);
    [self fetchDataAccordingToSegment];
    __weak typeof(self) weakSelf = self;
    dispatch_group_notify(serviceGroup,dispatch_get_main_queue(),^{
        [_tblServiceDirectory reloadData];
        serviceGroup = nil ;
    });
    
}

-(NSString*)stateNameDelay
{
    
    NSString *localA=singleton.serviceDirstateSelected;
    localA = [localA isEqualToString:@"All"] ? NSLocalizedString(@"all", nil): localA;
    
    if ([[singleton.serviceDirstateSelected uppercaseString] isEqualToString:@"ALL"] || singleton.serviceDirstateSelected == nil || singleton.serviceDirstateSelected.length == 0)
    {
        //localA =   NSLocalizedString(@"all", nil);
        localA =@"All";

    }
    
    return localA.uppercaseString;
    
}


-(void)btnStateLayoutChanges
{
    btnState.userInteractionEnabled = YES;
    btnState.titleLabel.font = [AppFont regularFont:14];
    [btnState setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnState.layer.cornerRadius = 15;
    btnState.clipsToBounds = YES;
   [btnState setImage:[UIImage imageNamed:@"dropdown_arrow_Lang"] forState:UIControlStateNormal];
    NSString *strState = [self stateNameDelay] ;
    if ( [[strState uppercaseString] isEqualToString:@"ALL"] || strState == nil || strState.length == 0)
    {
        strState = @"All";///NSLocalizedString(@"all", nil);
    }
    [btnState setTitle:strState forState:UIControlStateNormal];
    [btnState sizeToFit];
    CGRect btnFrame = btnState.frame;
    btnFrame.size.width = btnFrame.size.width + 30;
    btnFrame.size.height = 30;
    btnFrame.origin.x = self.view.center.x - (btnFrame.size.width / 2);
    //btnFrame.origin.y = btnY;
    btnState.translatesAutoresizingMaskIntoConstraints = true;
    btnState.frame = btnFrame;
    [btnState setContentEdgeInsets:UIEdgeInsetsMake(0,0, 0, 0)];
    [btnState setImageEdgeInsets:UIEdgeInsetsMake(2,CGRectGetWidth(btnState.frame) - 32, 0, 0)];
    [btnState setTitleEdgeInsets:UIEdgeInsetsMake(0,-38, 0, 0)];
    [self.view layoutIfNeeded ];
    [self.view updateConstraintsIfNeeded];
    
    //[btnState setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(btnState.frame) - 25, 0, 0)];
    //[btnState setTitleEdgeInsets:UIEdgeInsetsMake(0,-40, 0, 0)];
    
}

#pragma mark - KeyboardNotifications
- (void)keyboardWillShow:(NSNotification*)notification {
    
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    // Write code to adjust views accordingly using deltaHeight
    _currentKeyboardHeight = kbSize.height;
    
    //tableHeight = self.tblServiceDirectory.frame.size.height;
    
    //self.tblServiceDirectory.frame =   CGRectMake(self.tblServiceDirectory.frame.origin.x, self.tblServiceDirectory.frame.origin.y, self.tblServiceDirectory.frame.size.width, self.tblServiceDirectory.frame.size.height - _currentKeyboardHeight);
    
    
    self.tableViewBottomConstraint.constant = _currentKeyboardHeight;
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    
    self.tableViewBottomConstraint.constant = 0;
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [serviceSectionTitles count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewHeader = [UIView.alloc initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 28)];
    UILabel *lblTitle = [UILabel.alloc initWithFrame:CGRectMake(15, 3, 136, 21)];
    lblTitle.text = [serviceSectionTitles objectAtIndex:section];
    
    [lblTitle setFont:[UIFont systemFontOfSize:15 weight:UIFontWeightSemibold]];
    [lblTitle setTextColor:[UIColor blackColor]];
    [lblTitle setTextAlignment:NSTextAlignmentLeft];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    
    
    viewHeader.backgroundColor = [UIColor colorWithRed:242.0/255.0f green:242.0/255.0f blue:242.0/255.0f alpha:1.0f];
    [viewHeader addSubview:lblTitle];
    
    return viewHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSString *sectionTitle = [serviceSectionTitles objectAtIndex:section];
    NSArray *sectionAnimals = [services objectForKey:sectionTitle];
    return [sectionAnimals count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [serviceSectionTitles objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceDirectoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSString *sectionTitle = [serviceSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionService = [services objectForKey:sectionTitle];
    NSString *service = [sectionService objectAtIndex:indexPath.row];
    
    cell.departmentNameLabel.text = service;

    if (self.segmentController.selectedSegmentIndex == 0)
    {
        [serviceOnUmangArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            //if ([[service lowercaseString] isEqualToString:[[[serviceOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_NAME"] lowercaseString]])
            
            if ([[service lowercaseString] isEqualToString:[[[serviceOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_DISNAME"] lowercaseString]])

            {
                NSURL *url = [NSURL URLWithString:[[serviceOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_IMAGE"]];
                
                [cell.deptImage setImage:nil forState:UIControlStateNormal];
                
                [cell.deptImage.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"img_loadertime.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                 {
                     if (image == nil)
                     {
                         cell.deptImage.clipsToBounds = YES;
                         cell.deptImage.layer.cornerRadius = cell.deptImage.layer.frame.size.width/2;
                         
                         [cell.deptImage setImage:nil forState:UIControlStateNormal];
                         
                         cell.deptImage.backgroundColor = [UIColor colorWithRed:21.0f/255.0f green:87.0/255.0f blue:159.0/255.0f alpha:1.0];
                         
                         unichar name = [service characterAtIndex:0];
                         NSCharacterSet *numericSet = [NSCharacterSet decimalDigitCharacterSet];
                         if ([numericSet characterIsMember:name]) {
                             name = [@"#" characterAtIndex:0];
                         }
                         
                         [cell.deptImage setTitle:[[NSString stringWithCharacters:&name length:1]uppercaseString] forState:UIControlStateNormal];
                         
                     }
                     else
                     {
                         cell.deptImage.clipsToBounds = NO;
                         cell.deptImage.layer.cornerRadius = 0;
                         
                         [cell.deptImage setImage:image forState:UIControlStateNormal];
                         cell.deptImage.backgroundColor = [UIColor clearColor];
                         [cell.deptImage setTitle:@"" forState:UIControlStateNormal];
                     }
                     
                 }];
                
                
                *stop = YES;
            }
        }];
    }
    else
    {
        [serviceNotOnUmangArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
         //   if ([[service lowercaseString] isEqualToString:[[[serviceNotOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_NAME"] lowercaseString]])
            if ([[service lowercaseString] isEqualToString:[[[serviceNotOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_DISNAME"] lowercaseString]])

            {
                NSURL *url = [NSURL URLWithString:[[serviceNotOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_IMAGE"]];
                
                [cell.deptImage setImage:nil forState:UIControlStateNormal];
                
                [cell.deptImage.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"img_loadertime.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                 {
                     if (image == nil)
                     {
                         cell.deptImage.clipsToBounds = YES;
                         cell.deptImage.layer.cornerRadius = cell.deptImage.layer.frame.size.width/2;
                         
                         [cell.deptImage setImage:nil forState:UIControlStateNormal];
                         
                         cell.deptImage.backgroundColor = [UIColor colorWithRed:21.0f/255.0f green:87.0/255.0f blue:159.0/255.0f alpha:1.0];
                         
                         unichar name = [service characterAtIndex:0];
                         NSCharacterSet *numericSet = [NSCharacterSet decimalDigitCharacterSet];
                         if ([numericSet characterIsMember:name]) {
                             name = [@"#" characterAtIndex:0];
                         }
                         
                         [cell.deptImage setTitle:[[NSString stringWithCharacters:&name length:1]uppercaseString] forState:UIControlStateNormal];
                         
                     }
                     else
                     {
                         cell.deptImage.clipsToBounds = NO;
                         cell.deptImage.layer.cornerRadius = 0;
                         
                         [cell.deptImage setImage:image forState:UIControlStateNormal];
                         cell.deptImage.backgroundColor = [UIColor clearColor];
                         [cell.deptImage setTitle:@"" forState:UIControlStateNormal];
                     }
                     
                 }];
                
                
                *stop = YES;
            }
        }];
    }
    
    
    
    
    cell.departmentNameLabel.font = [AppFont regularFont:16.0];
    
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    //  return animalSectionTitles;
    return serviceIndexTitles;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [serviceSectionTitles indexOfObject:title];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    NSString *sectionTitle = [serviceSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionService = [services objectForKey:sectionTitle];
    NSString *service = [sectionService objectAtIndex:indexPath.row];
    
    if (self.segmentController.selectedSegmentIndex == 0)
    {
        [serviceOnUmangArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
           // if ([[service lowercaseString] isEqualToString:[[[serviceOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_NAME"] lowercaseString]])
                if ([[service lowercaseString] isEqualToString:[[[serviceOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_DISNAME"] lowercaseString]])

            {
                [self openDetailVCWithDictionary:[serviceOnUmangArray objectAtIndex:idx]];
                *stop = YES;
            }
        }];
    }
    else
    {
        [serviceNotOnUmangArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
           // if ([[service lowercaseString] isEqualToString:[[[serviceNotOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_NAME"] lowercaseString]])
            if ([[service lowercaseString] isEqualToString:[[[serviceNotOnUmangArray objectAtIndex:idx] valueForKey:@"SERVICE_DISNAME"] lowercaseString]])

            {
                [self openDetailVCWithDictionary:[serviceNotOnUmangArray objectAtIndex:idx]];
                *stop = YES;
            }
        }];
    }
    
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UISearchBar Delegate Methods

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
// return NO to not become first responder
{
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    [singleton traceEvents:@"Search Button" withAction:@"Clicked" withLabel:@"Service Directory" andValue:0];

    
    if ([searchBar.text isEqualToString:@""])
    {
        if (self.segmentController.selectedSegmentIndex == 0)
        {
            [self fetchDataAccordingToSegment];
        }
        else
        {
            [self fetchDataAccordingToSegment];
        }
        
        [self.searchBar resignFirstResponder];
    }
    else
    {
        
        NSLog(@"Services %@",services);
        
        NSMutableArray *arrServiceNames;
        
        
        if (self.segmentController.selectedSegmentIndex == 0)
        {
           // arrServiceNames = [[serviceOnUmangArray valueForKey:@"SERVICE_NAME"] mutableCopy];
            arrServiceNames = [[serviceOnUmangArray valueForKey:@"SERVICE_DISNAME"] mutableCopy];

        }
        else
        {
           // arrServiceNames = [[serviceNotOnUmangArray valueForKey:@"SERVICE_NAME"] mutableCopy];
            arrServiceNames = [[serviceNotOnUmangArray valueForKey:@"SERVICE_DISNAME"] mutableCopy];

        }
        
        
        NSMutableDictionary  *filteredTableData = [[NSMutableDictionary alloc] init];
        
        for (NSString* name in arrServiceNames)
        {
            bool isMatch = false;
            if(searchBar.text.length == 0)
            {
                isMatch = true;
                [self fetchDataAccordingToSegment];
            }
            else
            {
                // If we have a search string, check to see if it matches the food's name or description
                NSRange nameRange = [name rangeOfString:searchBar.text options:NSCaseInsensitiveSearch];
                
                NSRange descriptionRange = [name rangeOfString:searchBar.text options:NSCaseInsensitiveSearch];
                
                if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
                    isMatch = true;
            }
            
            // If we have a match...
            if(isMatch)
            {
                // Find the first letter of the food's name. This will be its gropu
                NSString* firstLetter = [[name substringToIndex:1] uppercaseString];
                
                // Check to see if we already have an array for this group
                NSMutableArray* arrayForLetter = (NSMutableArray*)[filteredTableData objectForKey:firstLetter];
                if(arrayForLetter == nil)
                {
                    // If we don't, create one, and add it to our dictionary
                    arrayForLetter = [[NSMutableArray alloc] init];
                    [filteredTableData setValue:arrayForLetter forKey:firstLetter];
                }
                
                // Finally, add the food to this group's array
                [arrayForLetter addObject:name];
            }
        }
        
        // Make a copy of our dictionary's keys, and sort them
        serviceIndexTitles = [[filteredTableData allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        services = filteredTableData;
        
        serviceSectionTitles = serviceIndexTitles;
        
        // Finally, refresh the table
        [self.tblServiceDirectory reloadData];
    }
    
}



- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    
    [singleton traceEvents:@"Search Cancel Button" withAction:@"Clicked" withLabel:@"Service Directory" andValue:0];

    [searchBar setShowsCancelButton:NO animated:YES];
    
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    
    if (self.segmentController.selectedSegmentIndex == 0)
    {
        [self fetchDataAccordingToSegment];
    }
    else
    {
        [self fetchDataAccordingToSegment];
    }
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""])
    {
        if (self.segmentController.selectedSegmentIndex == 0)
        {
            [self fetchDataAccordingToSegment];
        }
        else
        {
            [self fetchDataAccordingToSegment];
        }
        
        [self.searchBar resignFirstResponder];
    }
    else
    {
        
        NSLog(@"Services %@",services);
        
        NSMutableArray *arrServiceNames;
        
        if (self.segmentController.selectedSegmentIndex == 0)
        {
           // arrServiceNames = [[serviceOnUmangArray valueForKey:@"SERVICE_NAME"] mutableCopy];
            arrServiceNames = [[serviceOnUmangArray valueForKey:@"SERVICE_DISNAME"] mutableCopy];

        }
        else
        {
            //arrServiceNames = [[serviceNotOnUmangArray valueForKey:@"SERVICE_NAME"] mutableCopy];
            arrServiceNames = [[serviceNotOnUmangArray valueForKey:@"SERVICE_DISNAME"] mutableCopy];

        }
        
        
        NSMutableDictionary  *filteredTableData = [[NSMutableDictionary alloc] init];
        
        for (NSString* name in arrServiceNames)
        {
            bool isMatch = false;
            if(searchText.length == 0)
            {
                isMatch = true;
                [self fetchDataAccordingToSegment];
            }
            else
            {
                // If we have a search string, check to see if it matches the food's name or description
                NSRange nameRange = [name rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                NSRange descriptionRange = [name rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
                    isMatch = true;
            }
            
            // If we have a match...
            if(isMatch)
            {
                // Find the first letter of the food's name. This will be its gropu
                NSString* firstLetter = [[name substringToIndex:1] uppercaseString];
                
                // Check to see if we already have an array for this group
                NSMutableArray* arrayForLetter = (NSMutableArray*)[filteredTableData objectForKey:firstLetter];
                if(arrayForLetter == nil)
                {
                    // If we don't, create one, and add it to our dictionary
                    arrayForLetter = [[NSMutableArray alloc] init];
                    [filteredTableData setValue:arrayForLetter forKey:firstLetter];
                }
                
                // Finally, add the food to this group's array
                [arrayForLetter addObject:name];
            }
        }
        
        // Make a copy of our dictionary's keys, and sort them
        serviceIndexTitles = [[filteredTableData allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        services = filteredTableData;
        
        
        serviceSectionTitles = serviceIndexTitles;
        
        // Finally, refresh the table
        [self.tblServiceDirectory reloadData];
    }
}

#pragma mark - Open Detail View
-(void)openDetailVCWithDictionary:(NSDictionary *)dict
{
    [singleton traceEvents:@"Open Service Detail" withAction:@"Clicked" withLabel:@"Service Directory" andValue:0];

    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    
    /*ServiceDirDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ServiceDirDetailVC"];
     vc.hidesBottomBarWhenPushed = YES;
     [self.navigationController pushViewController:vc animated:YES];*/
    
    ServiceDirDetails *vc = [storyboard instantiateViewControllerWithIdentifier:@"ServiceDirDetails"];
    
    if (self.segmentController.selectedSegmentIndex == 0)
    {
        vc.isFrom = @"from_umang";
    }
    else
    {
        vc.isFrom = @"not_on_umang";
    }
    
    vc.dic_serviceInfo = dict;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    //UIViewController *topvc=[self topMostController];
    
    //[topvc presentViewController:vc animated:NO completion:nil];
    
}
- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}
#pragma mark - DB Methods

-(void)insertServiceDirectoryToDBTask:(NSDictionary*)response
{
    
    @try
    {
        //------- Code to insert data in database with service list-------
        NSMutableArray *addServiceDirList=[[NSMutableArray alloc]init];
        addServiceDirList =[[[response valueForKey:@"pd"] valueForKey:@"addServiceList"] mutableCopy];
        
        NSLog(@"response =%@",response);
        //----------- Update service list--------
        
        NSMutableArray *updateServiceDirList=[[NSMutableArray alloc]init];
        updateServiceDirList =[[[response valueForKey:@"pd"] valueForKey:@"updateServiceList"] mutableCopy];
        
        
        //----------- deleteServiceList--------
        
        NSMutableArray *deleteServiceDirList=[[NSMutableArray alloc]init];
        deleteServiceDirList =[[[response valueForKey:@"pd"] valueForKey:@"deleteServiceList"] mutableCopy];
        
        
        
        
        singleton.lastDirectoryFetchDate =[NSString stringWithFormat:@"%@",[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"]];
        
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.lastDirectoryFetchDate withKey:@"lastFetchServiceDirectoryDate"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        //------------------------- Encrypt Value------------------------
        
        //---------------------------------------------------------------------------
        //---------------------------------------------------------------------------
        //---------------------------------------------------------------------------
        
        
        
        
        
        
        dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
        dispatch_async(serialQueue, ^{
            if(addServiceDirList && [addServiceDirList count]>=1)
            {
                [self addServiceDirToDBTask:addServiceDirList]; //insert
                
            }
            
            dispatch_async(serialQueue, ^{
                
                
                if(updateServiceDirList && [updateServiceDirList count]>=1)
                {
                    [self updateServiceDirToDBTask:updateServiceDirList]; //update
                    
                }   dispatch_async(serialQueue, ^{
                    
                    if(deleteServiceDirList && [deleteServiceDirList count]>=1)
                    {
                        [self deleteServiceDirToDBTask:deleteServiceDirList];
                        
                    }
                    
                    
                    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                        
                        NSArray *newInsertedDirectoryData=[singleton.dbManager getAllServicesDirData];
                        
                        if (newInsertedDirectoryData.count != serviceOnUmangArray.count)
                        {
                            [self fetchDataAccordingToSegment];
                        }
                        
                        
                    }];
                });
            });
        }
                       );
        
    }
    
    
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
    
    
}





-(void)addServiceDirToDBTask:(NSMutableArray*)addServiceList
{
    
    
    for (int i=0; i<[addServiceList count]; i++)
    {
        
        @try {
            NSString* serviceId =[[addServiceList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* serviceName =[[addServiceList objectAtIndex:i]valueForKey:@"serviceName"];
            NSString* serviceDesc =[[addServiceList objectAtIndex:i]valueForKey:@"description"];
            NSString* serviceImg=[[addServiceList objectAtIndex:i]valueForKey:@"logo"];
            NSString* serviceLat =[[addServiceList objectAtIndex:i]valueForKey:@"lat"];
            NSString* serviceLong =[[addServiceList objectAtIndex:i]valueForKey:@"lon"];
            NSString* servicePhoneno=[[addServiceList objectAtIndex:i]valueForKey:@"contact"];
            NSString* serviceWebsite =[[addServiceList objectAtIndex:i]valueForKey:@"website"];
            NSString* serviceEmail=[[addServiceList objectAtIndex:i]valueForKey:@"email"];
            NSString* serviceAddress =[[addServiceList objectAtIndex:i]valueForKey:@"deptAddress"];
            NSString* serviceWorkingHour=[[addServiceList objectAtIndex:i]valueForKey:@"workingHours"];
            NSString* serviceOtherInfo =[[addServiceList objectAtIndex:i]valueForKey:@"info"];
            NSString* serviceNativeApp = [[addServiceList objectAtIndex:i]valueForKey:@"nativeApp"];
            NSString* isavailable = [[addServiceList objectAtIndex:i]valueForKey:@"isavailable"];
            NSString* nativeAppName = [[addServiceList objectAtIndex:i]valueForKey:@"nativeAppName"];
            
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId =@"";
            }
            if (serviceName == (NSString *)[NSNull null]||[serviceName length]==0) {
                serviceName =@"";
            }
            if (serviceDesc == (NSString *)[NSNull null]||[serviceDesc length]==0) {
                serviceDesc =@"";
            }
            if (serviceImg == (NSString *)[NSNull null]||[serviceImg length]==0) {
                serviceImg =@"";
            }
            if (serviceLat == (NSString *)[NSNull null]||[serviceLat length]==0) {
                serviceLat =@"";
            }
            if (serviceLong == (NSString *)[NSNull null]||[serviceLong length]==0) {
                serviceLong=@"";
            }
            
            if (servicePhoneno == (NSString *)[NSNull null]||[servicePhoneno length]==0) {
                servicePhoneno=@"";
            }
            if (serviceWebsite == (NSString *)[NSNull null]||[serviceWebsite length]==0) {
                serviceWebsite=@"";
            }
            if (serviceEmail == (NSString *)[NSNull null]||[serviceEmail length]==0) {
                serviceEmail=@"";
            }
            if (serviceAddress == (NSString *)[NSNull null]||[serviceAddress length]==0) {
                serviceAddress=@"";
            }
            if (serviceWorkingHour == (NSString *)[NSNull null]||[serviceWorkingHour length]==0) {
                serviceWorkingHour=@"";
            }
            if (serviceOtherInfo == (NSString *)[NSNull null]||[serviceOtherInfo length]==0) {
                serviceOtherInfo=@"";
            }
            if (serviceNativeApp == (NSString *)[NSNull null]||[serviceNativeApp length]==0) {
                serviceNativeApp=@"";
            }
            if (isavailable == (NSString *)[NSNull null]||[isavailable length]==0) {
                isavailable =@"";
            }
            
            if (nativeAppName == (NSString *)[NSNull null]||[nativeAppName length]==0) {
                nativeAppName =@"";
            }
            
            
            NSString* otherwebsite = [[addServiceList objectAtIndex:i]valueForKey:@"otherwebsite"];
            
            
            if (otherwebsite == (NSString *)[NSNull null]||[otherwebsite length]==0) {
                otherwebsite =@"";
            }
            NSLog(@"otherwebsite=%@",otherwebsite);
            
            
            NSString* andLink = [[addServiceList objectAtIndex:i]valueForKey:@"andlink"];
            
            if (andLink == (NSString *)[NSNull null]||[andLink length]==0) {
                andLink =@"";
            }
            NSLog(@"andLink=%@",andLink);
            
            
            // ==========   NEW parameter added in Service Directory  =======
            
            /*
             adding below parameter
             "categoryid": "13", CATEGORY_ID
             "stateid": "99", STATE_ID
             "otherstate": "", OTHER_STATE
             "categoryname": "Utility" CATEGORY_NAME
             */
            
            
            
            NSString* categoryid = [[addServiceList objectAtIndex:i]valueForKey:@"categoryid"];
            
            if (categoryid == (NSString *)[NSNull null]||[categoryid length]==0) {
                categoryid =@"";
            }
            NSLog(@"categoryid=%@",categoryid);
            
            
            
            
            NSString* stateid = [[addServiceList objectAtIndex:i]valueForKey:@"stateid"];
            
            if (stateid == (NSString *)[NSNull null]||[stateid length]==0) {
                stateid =@"";
            }
            NSLog(@"stateid=%@",stateid);
            
            
            
            
            NSString* otherstate = [[addServiceList objectAtIndex:i]valueForKey:@"otherstate"];
            
            
            
            
            if ([otherstate length]!=0)
            {
                otherstate=[NSString stringWithFormat:@"|%@|",otherstate];
            }
            
            NSLog(@"otherstate Other State=%@",otherstate);
            
            if (otherstate == (NSString *)[NSNull null]||[otherstate length]==0)
            {
                otherstate=@"";
            }
            

            
            
            
            
            NSString* categoryname = [[addServiceList objectAtIndex:i]valueForKey:@"categoryname"];
            
            if (categoryname == (NSString *)[NSNull null]||[categoryname length]==0) {
                categoryname =@"";
            }
            NSLog(@"categoryname=%@",categoryname);
            
            
            //======== New parameter added in the Database======
            NSString* disname = [[addServiceList objectAtIndex:i]valueForKey:@"disname"];
            
            if([disname length]==0 || disname ==nil|| [disname isEqualToString:@"(null)"])
            {
                
                NSString *serviceNameStr = [[addServiceList objectAtIndex:i]valueForKey:@"serviceName"];
                
                if ([serviceNameStr length]==0 || serviceNameStr ==nil|| [serviceNameStr isEqualToString:@"(null)"])
                {
                    disname =@"";//serviceName
                }
                else
                {
                    disname = serviceNameStr;
                }
                
            }

            NSString* depttype =[[addServiceList objectAtIndex:i]valueForKey:@"depttype"];
            if (depttype == (NSString *)[NSNull null]||[depttype length]==0) {
                depttype=@"";
            }
            
            
            NSString* multicatid =[[addServiceList objectAtIndex:i]valueForKey:@"multicatid"];
            if (multicatid == (NSString *)[NSNull null]||[multicatid length]==0) {
                multicatid=@"";
            }
            
            NSString* multicatname =[[addServiceList objectAtIndex:i]valueForKey:@"multicatname"];
            if (multicatname == (NSString *)[NSNull null]||[multicatname length]==0) {
                multicatname=@"";
            }
            
            [singleton.dbManager insertServicesDirData:serviceId serviceName:serviceName  serviceDesc:serviceDesc
                                            serviceImg:serviceImg serviceLat:serviceLat serviceLong:serviceLong servicePhoneno:servicePhoneno  serviceWebsite:serviceWebsite serviceEmail:serviceEmail serviceAddress:serviceAddress  serviceWorkingHour:serviceWorkingHour  serviceOtherInfo:serviceOtherInfo  serviceNativeApp:serviceNativeApp isavailable:isavailable nativeAppName:nativeAppName otherwebsite:otherwebsite andLink:andLink categoryid:categoryid stateid:stateid otherstate:otherstate categoryname:categoryname disname:disname depttype:depttype multicatid:multicatid multicatname:multicatname];
            
            
           /* [singleton.dbManager insertServicesDirData:serviceId serviceName:serviceName  serviceDesc:serviceDesc
                                            serviceImg:serviceImg serviceLat:serviceLat serviceLong:serviceLong servicePhoneno:servicePhoneno  serviceWebsite:serviceWebsite serviceEmail:serviceEmail serviceAddress:serviceAddress  serviceWorkingHour:serviceWorkingHour  serviceOtherInfo:serviceOtherInfo  serviceNativeApp:serviceNativeApp isavailable:isavailable nativeAppName:nativeAppName otherwebsite:otherwebsite andLink:andLink categoryid:categoryid stateid:stateid otherstate:otherstate categoryname:categoryname disname:disname];
            */
            
            //close 2
            /*
             
             
             
             
             
             
             
             
             
             [singleton.dbManager insertServicesDirData:serviceId serviceName:serviceName  serviceDesc:serviceDesc
             serviceImg:serviceImg serviceLat:serviceLat serviceLong:serviceLong servicePhoneno:servicePhoneno  serviceWebsite:serviceWebsite serviceEmail:serviceEmail serviceAddress:serviceAddress  serviceWorkingHour:serviceWorkingHour  serviceOtherInfo:serviceOtherInfo  serviceNativeApp:serviceNativeApp isavailable:isavailable nativeAppName:nativeAppName otherwebsite:otherwebsite andLink:andLink];
             
             */
            
            //close 1
            /* [singleton.dbManager insertServicesDirData:serviceId serviceName:serviceName  serviceDesc:serviceDesc
             serviceImg:serviceImg serviceLat:serviceLat serviceLong:serviceLong servicePhoneno:servicePhoneno  serviceWebsite:serviceWebsite serviceEmail:serviceEmail serviceAddress:serviceAddress  serviceWorkingHour:serviceWorkingHour  serviceOtherInfo:serviceOtherInfo  serviceNativeApp:serviceNativeApp isavailable:isavailable nativeAppName:nativeAppName];
             */
            
            
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
        
    }
    
}

//---- [singleton.dbManager open];
-(void)updateServiceDirToDBTask:(NSMutableArray*)updateServiceList
{
    
    for (int i=0; i<[updateServiceList count]; i++)
    {
        
        
        @try {
            NSString* serviceId =[[updateServiceList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* serviceName =[[updateServiceList objectAtIndex:i]valueForKey:@"serviceName"];
            NSString* serviceDesc =[[updateServiceList objectAtIndex:i]valueForKey:@"description"];
            NSString* serviceImg=[[updateServiceList objectAtIndex:i]valueForKey:@"logo"];
            NSString* serviceLat =[[updateServiceList objectAtIndex:i]valueForKey:@"lat"];
            NSString* serviceLong =[[updateServiceList objectAtIndex:i]valueForKey:@"lon"];
            NSString* servicePhoneno=[[updateServiceList objectAtIndex:i]valueForKey:@"contact"];
            NSString* serviceWebsite =[[updateServiceList objectAtIndex:i]valueForKey:@"website"];
            NSString* serviceEmail=[[updateServiceList objectAtIndex:i]valueForKey:@"email"];
            NSString* serviceAddress =[[updateServiceList objectAtIndex:i]valueForKey:@"deptAddress"];
            NSString* serviceWorkingHour=[[updateServiceList objectAtIndex:i]valueForKey:@"workingHours"];
            NSString* serviceOtherInfo =[[updateServiceList objectAtIndex:i]valueForKey:@"info"];
            NSString* serviceNativeApp = [[updateServiceList objectAtIndex:i]valueForKey:@"nativeApp"];
            NSString* isavailable = [[updateServiceList objectAtIndex:i]valueForKey:@"isavailable"];
            NSString* nativeAppName = [[updateServiceList objectAtIndex:i]valueForKey:@"nativeAppName"];
            
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId =@"";
            }
            if (serviceName == (NSString *)[NSNull null]||[serviceName length]==0) {
                serviceName =@"";
            }
            if (serviceDesc == (NSString *)[NSNull null]||[serviceDesc length]==0) {
                serviceDesc =@"";
            }
            if (serviceImg == (NSString *)[NSNull null]||[serviceImg length]==0) {
                serviceImg =@"";
            }
            if (serviceLat == (NSString *)[NSNull null]||[serviceLat length]==0) {
                serviceLat =@"";
            }
            if (serviceLong == (NSString *)[NSNull null]||[serviceLong length]==0) {
                serviceLong=@"";
            }
            
            if (servicePhoneno == (NSString *)[NSNull null]||[servicePhoneno length]==0) {
                servicePhoneno=@"";
            }
            if (serviceWebsite == (NSString *)[NSNull null]||[serviceWebsite length]==0) {
                serviceWebsite=@"";
            }
            if (serviceEmail == (NSString *)[NSNull null]||[serviceEmail length]==0) {
                serviceEmail=@"";
            }
            if (serviceAddress == (NSString *)[NSNull null]||[serviceAddress length]==0) {
                serviceAddress=@"";
            }
            if (serviceWorkingHour == (NSString *)[NSNull null]||[serviceWorkingHour length]==0) {
                serviceWorkingHour=@"";
            }
            if (serviceOtherInfo == (NSString *)[NSNull null]||[serviceOtherInfo length]==0) {
                serviceOtherInfo=@"";
            }
            if (serviceNativeApp == (NSString *)[NSNull null]||[serviceNativeApp length]==0) {
                serviceNativeApp=@"";
            }
            if (isavailable == (NSString *)[NSNull null]||[isavailable length]==0) {
                isavailable =@"";
            }
            if (nativeAppName == (NSString *)[NSNull null]||[nativeAppName length]==0) {
                nativeAppName =@"";
            }
            
            
            NSString* otherwebsite = [[updateServiceList objectAtIndex:i]valueForKey:@"otherwebsite"];
            
            if (otherwebsite == (NSString *)[NSNull null]||[otherwebsite length]==0) {
                otherwebsite =@"";
            }
            NSLog(@"otherwebsite=%@",otherwebsite);
            NSString* andLink = [[updateServiceList objectAtIndex:i]valueForKey:@"andlink"];
            
            if (andLink == (NSString *)[NSNull null]||[andLink length]==0) {
                andLink =@"";
            }
            NSLog(@"andLink=%@",andLink);
            
            
            
            
            // ==========   NEW parameter added in Service Directory  =======
            
            /*
             adding below parameter
             "categoryid": "13", CATEGORY_ID
             "stateid": "99", STATE_ID
             "otherstate": "", OTHER_STATE
             "categoryname": "Utility" CATEGORY_NAME
             */
            
            
            
            NSString* categoryid = [[updateServiceList objectAtIndex:i]valueForKey:@"categoryid"];
            
            if (categoryid == (NSString *)[NSNull null]||[categoryid length]==0) {
                categoryid =@"";
            }
            NSLog(@"categoryid=%@",categoryid);
            
            
            
            
            NSString* stateid = [[updateServiceList objectAtIndex:i]valueForKey:@"stateid"];
            
            if (stateid == (NSString *)[NSNull null]||[stateid length]==0) {
                stateid =@"";
            }
            NSLog(@"stateid=%@",stateid);
            
            
            
            
            NSString* otherstate = [[updateServiceList objectAtIndex:i]valueForKey:@"otherstate"];
            
            if ([otherstate length]!=0)
            {
                otherstate=[NSString stringWithFormat:@"|%@|",otherstate];
            }
            
            NSLog(@"otherstate Other State=%@",otherstate);
            
            if (otherstate == (NSString *)[NSNull null]||[otherstate length]==0)
            {
                otherstate=@"";
            }
            

            
            
            NSString* categoryname = [[updateServiceList objectAtIndex:i]valueForKey:@"categoryname"];
            
            if (categoryname == (NSString *)[NSNull null]||[categoryname length]==0) {
                categoryname =@"";
            }
            NSLog(@"categoryname=%@",categoryname);
            
            
            
            
            
            //======== New parameter added in the Database======
            NSString* disname = [[updateServiceList objectAtIndex:i]valueForKey:@"disname"];
            
            if([disname length]==0 || disname ==nil|| [disname isEqualToString:@"(null)"])
                
            {
                NSString *serviceNameStr = [[updateServiceList objectAtIndex:i]valueForKey:@"serviceName"];
                
                if ([serviceNameStr length]==0 || serviceNameStr ==nil|| [serviceNameStr isEqualToString:@"(null)"])
                {
                    disname =@"";//serviceName
                }
                else
                {
                    disname = serviceNameStr;
                }
            }

            
            
            NSString* depttype =[[updateServiceList objectAtIndex:i]valueForKey:@"depttype"];
            
            
            if (depttype == (NSString *)[NSNull null]||[depttype length]==0) {
                depttype=@"";
            }
            
            
            
            
            NSString* multicatid =[[updateServiceList objectAtIndex:i]valueForKey:@"multicatid"];
            if (multicatid == (NSString *)[NSNull null]||[multicatid length]==0) {
                multicatid=@"";
            }
            
            NSString* multicatname =[[updateServiceList objectAtIndex:i]valueForKey:@"multicatname"];
            if (multicatname == (NSString *)[NSNull null]||[multicatname length]==0) {
                multicatname=@"";
            }
            
            

            
            
            [singleton.dbManager updateServicesDirData:serviceId serviceName:serviceName  serviceDesc:serviceDesc
                                            serviceImg:serviceImg serviceLat:serviceLat serviceLong:serviceLong servicePhoneno:servicePhoneno  serviceWebsite:serviceWebsite serviceEmail:serviceEmail serviceAddress:serviceAddress  serviceWorkingHour:serviceWorkingHour  serviceOtherInfo:serviceOtherInfo  serviceNativeApp:serviceNativeApp isavailable:isavailable nativeAppName:nativeAppName otherwebsite:otherwebsite andLink:andLink categoryid:categoryid stateid:stateid otherstate:otherstate categoryname:categoryname disname:disname depttype:depttype multicatid:multicatid multicatname:multicatname];
            
            
          /*  [singleton.dbManager updateServicesDirData:serviceId serviceName:serviceName  serviceDesc:serviceDesc
                                            serviceImg:serviceImg serviceLat:serviceLat serviceLong:serviceLong servicePhoneno:servicePhoneno  serviceWebsite:serviceWebsite serviceEmail:serviceEmail serviceAddress:serviceAddress  serviceWorkingHour:serviceWorkingHour  serviceOtherInfo:serviceOtherInfo  serviceNativeApp:serviceNativeApp isavailable:isavailable nativeAppName:nativeAppName otherwebsite:otherwebsite andLink:andLink categoryid:categoryid stateid:stateid otherstate:otherstate categoryname:categoryname disname:disname ];
            */
            
            //close 2 db change
            /*
             [singleton.dbManager updateServicesDirData:serviceId serviceName:serviceName  serviceDesc:serviceDesc
             serviceImg:serviceImg serviceLat:serviceLat serviceLong:serviceLong servicePhoneno:servicePhoneno  serviceWebsite:serviceWebsite serviceEmail:serviceEmail serviceAddress:serviceAddress  serviceWorkingHour:serviceWorkingHour  serviceOtherInfo:serviceOtherInfo  serviceNativeApp:serviceNativeApp isavailable:isavailable nativeAppName:nativeAppName otherwebsite:otherwebsite andLink:andLink ];
             */
            //close 1 db changes
            /*
             [singleton.dbManager updateServicesDirData:serviceId serviceName:serviceName  serviceDesc:serviceDesc
             serviceImg:serviceImg serviceLat:serviceLat serviceLong:serviceLong servicePhoneno:servicePhoneno  serviceWebsite:serviceWebsite serviceEmail:serviceEmail serviceAddress:serviceAddress  serviceWorkingHour:serviceWorkingHour  serviceOtherInfo:serviceOtherInfo  serviceNativeApp:serviceNativeApp isavailable:isavailable nativeAppName:nativeAppName];
             
             */
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
}




-(void)deleteServiceDirToDBTask:(NSMutableArray*)deleteServiceList
{
    
    for (int i=0; i<[deleteServiceList count]; i++)
    {
        NSString* serviceId =[[deleteServiceList objectAtIndex:i]valueForKey:@"serviceId"];
        
        
        //NSLog(@"deleteServiceData");
        
        //code to be executed in the background
        [singleton.dbManager deleteServiceDirDataWithId:serviceId];
    }
    
}

#pragma mark - Method to fetch Services Not on Umang

-(void)getServicesNotOnUmang
{
    singleton = [SharedManager sharedSingleton];
    [serviceOnUmangArray removeAllObjects];
    
    if (singleton.user_tkn.length == 0)
    {
        serviceNotOnUmangArray=[[singleton.dbManager getInfoFlagOTHERServicesDirData] mutableCopy];
    }
    else
    {
        serviceNotOnUmangArray=[[singleton.dbManager getOTHERServicesDirData] mutableCopy];
    }
    
    
    
    if (serviceNotOnUmangArray.count == 0)
    {
        self.noRecordView.hidden = NO;
    }
    else
    {
        self.noRecordView.hidden = YES;
    }
    
    
   // NSMutableArray *arrServiceNames = [[serviceNotOnUmangArray valueForKey:@"SERVICE_NAME"] mutableCopy];
    
    NSMutableArray *arrServiceNames = [[serviceNotOnUmangArray valueForKey:@"SERVICE_DISNAME"] mutableCopy];

    
    
    NSArray *sortedStrings =
    [arrServiceNames sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableDictionary *serviceDirDict = [NSMutableDictionary new];
    
    for (NSString *name in sortedStrings)
    {
        
        NSString *key =  [[name substringToIndex: 1] uppercaseString];
        
        if ([serviceDirDict objectForKey:key] != nil)
        {
            
            NSMutableArray *tempArray = [serviceDirDict objectForKey:key];
            [tempArray addObject: name];
            
            [serviceDirDict setObject:tempArray forKey:key];
            
        }
        else
        {
            NSMutableArray *tempArray = [[NSMutableArray alloc] initWithObjects: name, nil];
            [serviceDirDict setObject:tempArray forKey:key];
            
        }
    }
    
    services = serviceDirDict;
    
    serviceSectionTitles = [[services allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    serviceIndexTitles = [[services allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];;
    
    _tblServiceDirectory.sectionIndexColor = [UIColor colorWithRed:0.0/255.0f green:89.0/255.0f blue:157.0/255.0f alpha:1.0f];
    
    [self.tblServiceDirectory reloadData];
}

#pragma mark - API to Fetch Directory

-(void)fetchDatafromDB
{
    singleton = [SharedManager sharedSingleton];
    [serviceNotOnUmangArray removeAllObjects];
    
    //arrDirectoryData=[[singleton.dbManager getAllUMANGServicesDirData] mutableCopy];
  
    if (singleton.user_tkn.length == 0)
    {
        serviceOnUmangArray = [[singleton.dbManager getInfoFlagStateWiseUMANGServicesDirData:self.SD_state_id] mutableCopy];
    }
    else
    {
        serviceOnUmangArray = [[singleton.dbManager getStateWiseUMANGServicesDirData:self.SD_state_id] mutableCopy];
    }
    
    

    // NSLog(@"%@",[singleton.dbManager getAllUMANGServicesDirData]);
    // NSLog(@"%@",[singleton.dbManager getAllServicesDirData]);
    
    if (serviceOnUmangArray.count == 0)
    {
        self.noRecordView.hidden = NO;
    }
    else
    {
        self.noRecordView.hidden = YES;
    }
    
    
 //   NSMutableArray *arrServiceNames = [[serviceOnUmangArray valueForKey:@"SERVICE_NAME"] mutableCopy];
    NSMutableArray *arrServiceNames = [[serviceOnUmangArray valueForKey:@"SERVICE_DISNAME"] mutableCopy];

    
    
    NSArray *sortedStrings =
    [arrServiceNames sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableDictionary *serviceDirDict = [NSMutableDictionary new];
    
    for (NSString *name in sortedStrings)
    {
        
        NSString *key =  [[name substringToIndex: 1] uppercaseString];
        
        if ([serviceDirDict objectForKey:key] != nil)
        {
            
            NSMutableArray *tempArray = [serviceDirDict objectForKey:key];
            [tempArray addObject: name];
            
            [serviceDirDict setObject:tempArray forKey:key];
            
        }
        else
        {
            NSMutableArray *tempArray = [[NSMutableArray alloc] initWithObjects: name, nil];
            [serviceDirDict setObject:tempArray forKey:key];
            
        }
    }
    
    services = serviceDirDict;
    
    serviceSectionTitles = [[services allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    serviceIndexTitles = [[services allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    _tblServiceDirectory.sectionIndexColor = [UIColor colorWithRed:0.0/255.0f green:89.0/255.0f blue:157.0/255.0f alpha:1.0f];
    
    [self.tblServiceDirectory reloadData];
}

-(void)hitRetryMethod
{
    [self fetchDataAccordingToSegment]; //in case of fail api
}

-(void)hitAPIToFetchServiceDirectory
{
    
    
    NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastDirectoryFetchDate"];
    
    @try {
        //NSLog(@"lastTime=%@ %lu",lastTime,(unsigned long)[lastTime length]);
        if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]) {
            lastTime=@"";
        }
        
    } @catch (NSException *exception)
    {
        
    } @finally
    {
        
    }
    
    
    
    
    singleton.lastDirectoryFetchDate = lastTime;
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"peml"];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *aadhr= singleton.objUserProfile.objAadhar.aadhar_number;
    if (singleton.objUserProfile.objAadhar.aadhar_number.length)
    {
        [dictBody setObject:aadhr forKey:@"aadhr"];
    }
    
    [dictBody setObject:singleton.lastDirectoryFetchDate forKey:@"ldate"];
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_GET_SERVICE_DIRECTORY withBody:dictBody andTag:TAG_REQUEST_SERVICE_DIRECTORY completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         
         if (error == nil)
         {
             
             [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                 
                 @try
                 {
                     if ([response isKindOfClass:[NSNull class]] ){
                         //do something
                         
                         //NSLog(@"Null response");
                         [self fetchDataAccordingToSegment];
                     }
                     
                     NSString *str_response=[NSString stringWithFormat:@"%@",response];
                     
                     //NSLog(@"str_response=%@",str_response);
                     if (str_response==nil)
                     {
                         
                         [self fetchDataAccordingToSegment];
                         
                     }
                     //NSLog(@"Null response");
                     
                 }
                 
                 @catch (NSException *exception)
                 {
                     
                 }
                 
                 @finally
                 {
                     
                 }
             }];
             
             NSString *forceUpdate=[[response valueForKey:@"pd"] valueForKey:@"forceUpdate"];
             
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                 
             {
                 
                 if ([[forceUpdate uppercaseString] isEqualToString:@"Y"])
                 {
                     
                     [singleton.dbManager deleteServicesDirectory];
                 }
                 
                 
                 dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
                 dispatch_async(serialQueue, ^{
                     [self insertServiceDirectoryToDBTask:response]; //insert into DB
                 });
                 
                 
                 
                 singleton.lastDirectoryFetchDate = [NSString stringWithFormat:@"%@",[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"]];
                 
                 //------------------------- Encrypt Value------------------------
                 [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                 // Encrypt
                 [[NSUserDefaults standardUserDefaults] encryptValue:singleton.lastDirectoryFetchDate withKey:@"lastDirectoryFetchDate"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 //------------------------- Encrypt Value------------------------
                 
             }
             
         }
         else
         {
             //[self hitRetryMethod];
         }
         
     }];
}

#pragma mark -
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


@end

#pragma mark - Service Directory Cell Interface and Implementation

@interface ServiceDirectoryCell()
@end

@implementation ServiceDirectoryCell

@end


