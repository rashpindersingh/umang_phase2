//
//  HintViewController.h
//  Home
//
//  Created by deepak singh rawat on 01/10/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HintViewController :UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIButton *btn_skip;
    IBOutlet UIButton *btn_next;
    IBOutlet UILabel *lbl_line;
}

-(IBAction)finishPressed:(id)sender;
- (IBAction)skipPressed:(id)sender;



@property(nonatomic,assign) int page;
@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIPageControl *pageControl;
@property (strong, nonatomic) NSArray *pageImages;


- (IBAction)changePage:(id)sender;


@end
