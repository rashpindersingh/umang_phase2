//
//  WKPaymentViewController.m
//
//
//  Created by Lokesh Jain on 12/02/18.
//

#import "WKPaymentViewController.h"
#import "StateList.h"

@interface WKPaymentViewController ()<WKNavigationDelegate,WKUIDelegate>
{
    SharedManager *singleton;
    StateList *obj;
    
    
    
    NSString *str_name;
    NSString *str_gender;
    NSString *str_dob;
    NSString *str_qualification;
    NSString *str_occupation;
    NSString *str_state;
    NSString *str_district;
    NSString *str_registerMb;
    NSString *str_emailAddress;
    NSString *str_alternateMb;
    NSString *str_emailVerifyStatus;
    NSString *str_amnosVerifyStatus;
    NSString *str_address;
    
    NSString *str_Url_pic;
    
    NSMutableArray *socialpd;
    UIActivityIndicatorView *activityIndicator;
}

@end

@implementation WKPaymentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:self.backButton.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(self.backButton.frame.origin.x, self.backButton.frame.origin.y, self.backButton.frame.size.width, self.backButton.frame.size.height);
        
        [self.backButton setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        self.backButton.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    
    singleton = [SharedManager sharedSingleton];
    obj       = [[StateList alloc]init];
    
    
    self.webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
    
    [self.view addSubview:self.webView];
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    [self.view addSubview: activityIndicator];
    
    NSString *urlAddress =self.urlString;
    
    if ([urlAddress isEqualToString:@"<null>"])
    {
        urlAddress=@"";
    }
    
    NSString *encodedAddress = [urlAddress stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURL *postURI=[NSURL URLWithString:encodedAddress];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:postURI];
    
    [request setHTTPShouldHandleCookies:YES];
    
    if([cookies count]>1)
    {
        [self addCookies:cookies forRequest:request];
    }
    
    NSLog(@"WkPayment View");
    
    [self.webView loadRequest: request];
    
    
    //[self hitFetchProfileAPI];
}
- (IBAction)backButtonClicked:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //[self.navigationController popViewControllerAnimated:YES];
}


-(void)hitFetchProfileAPI
{
    //hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    // hud.label.text = NSLocalizedString(@"Please wait...", @"Please wait...");
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        // [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            
            NSString *responseStr=[NSString stringWithFormat:@"%@",response];
            if ([responseStr length]>0)
            {
                
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    
                {
                    
                    @try {
                        
                        
                        //singleton.user_tkn=tkn;
                        NSMutableArray *aadharpd=[[NSMutableArray alloc]init];
                        aadharpd=[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];
                        
                        
                        NSMutableArray *generalpd=[[NSMutableArray alloc]init];
                        generalpd=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                        
                        
                        
                        socialpd=[[NSMutableArray alloc]init];
                        
                        singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                        NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                        
                        if ([abbreviation length]==0) {
                            abbreviation=@"";
                        }
                        
                        singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                        [singleton setStateId:singleton.user_StateId];
                        
                        NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                        
                        emblemString = emblemString.length == 0 ? @"":emblemString;
                        
                        [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                        
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        //-------- Add later----------
                        singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                        singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                        //-------- Add later----------
                        
                        
                        if ([singleton.user_id length]==0) {
                            singleton.user_id=@"";
                        }
                        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        
                        // [defaults setObject:singleton.user_id forKey:@"USER_ID"];
                        
                        //[defaults synchronize];
                        
                        
                        //------------------------- Encrypt Value------------------------
                        
                        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                        // Encrypt
                        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        //------------------------- Encrypt Value------------------------
                        
                        
                        //NSString *identifier =( [[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        socialpd=(NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        
                        
                        
                        //----- local values  to be save and show from API-----------
                        
                        
                        
                        // NSString * sid=[generalpd valueForKey:@"sid"];
                        
                        str_name=[generalpd valueForKey:@"nam"];
                        str_registerMb =[generalpd valueForKey:@"mno"];
                        str_alternateMb=[generalpd valueForKey:@"amno"];
                        //str_city=[generalpd valueForKey:@"cty"];
                        str_state=[generalpd valueForKey:@"st"];
                        
                        // state_id=[obj getStateCode:str_state];
                        
                        
                        str_district=[generalpd valueForKey:@"dist"];
                        str_dob=[generalpd valueForKey:@"dob"];
                        str_gender=[generalpd valueForKey:@"gndr"];
                        str_emailAddress=[generalpd valueForKey:@"email"];
                        str_emailVerifyStatus=[generalpd valueForKey:@"emails"];
                        str_amnosVerifyStatus=[generalpd valueForKey:@"amnos"];
                        str_Url_pic=[generalpd valueForKey:@"pic"];
                        str_address=[generalpd valueForKey:@"addr"];
                        
                        NSLog(@"str_gender=%@",str_gender);
                        
                        /* if (str_gender.length>0) {
                         _tblUserProfile.tableHeaderView =  [self designHeaderView];
                         }
                         */
                        NSString *quali_id=[generalpd valueForKey:@"qual"];
                        NSString *Occu_id=[generalpd valueForKey:@"occup"];
                        
                        
                        str_qualification=[obj getqualName:quali_id];
                        str_occupation=[obj getOccuptname:Occu_id];
                        
                        NSLog(@"str_Url_pic  =>>> %@",str_Url_pic);
                        /*
                         "gcmid": "",
                         "opid": "",
                         "refid": "",
                         "tkn": "",
                         */
                        
                        if ([[response objectForKey:@"socialpd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"socialpd"]];
                        }
                        if ([[response objectForKey:@"pd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                        }
                        
                        [self setProfileData];
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                }
            }
            
            
        }
        else
        {
            
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];
             */
            [self setProfileData];
        }
        
    }];
    
}

-(NSString *)UpperFirstWord:(NSString*)inputString
{
    
    if ([inputString length]!=0) {
        inputString = [inputString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[inputString substringToIndex:1] uppercaseString]];
    }
    return inputString;
}


-(void)setProfileData
{
    
    //----- local values  to be save and show from API-----------
    
    
    str_name=[self UpperFirstWord:str_name];
    // str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    
    
    
    
    //  if ([str_Url_pic length]!=0 ) {
    
    singleton.user_profile_URL=str_Url_pic;
    // }
    
    
    
    
    
    
    if ([str_gender length]!=0)
    {
        str_gender=[str_gender uppercaseString];
        
    }
    
    str_name=[self UpperFirstWord:str_name];
    //str_gender=str_dob;
    str_qualification = [self UpperFirstWord:str_qualification];
    str_occupation = [self UpperFirstWord:str_occupation];
    str_state = [self UpperFirstWord:str_state];
    str_district = [self UpperFirstWord:str_district];
    str_address = [self UpperFirstWord:str_address];
    str_registerMb = [self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb = [self UpperFirstWord:str_alternateMb];
    //singleton.notiTypeGenderSelected
    singleton.profileNameSelected = str_name;
    singleton.profilestateSelected = str_state;
    singleton.notiTypeCitySelected = @"";
    singleton.notiTypDistricteSelected = str_district;
    singleton.profileDOBSelected = str_dob;
    singleton.altermobileNumber = str_alternateMb;
    singleton.user_Qualification = str_qualification;
    singleton.user_Occupation = str_occupation;
    singleton.profileEmailSelected = str_emailAddress;
    singleton.profileUserAddress = str_address;
    
    NSLog(@"str_gender=%@",str_gender);
    /* if ([str_gender isEqualToString:@"M"]) {
     
     stringGender=NSLocalizedString(@"gender_male", nil);
     
     }
     else if ([str_gender isEqualToString:@"F"]) {
     
     stringGender=NSLocalizedString(@"gender_female", nil);
     
     
     }
     else if ([str_gender isEqualToString:@"T"])
     {
     stringGender=NSLocalizedString(@"gender_transgender", nil);
     
     
     
     }
     else {
     stringGender=@"";
     
     
     
     }
     */
    
    [self loadHtml];
    
    
    
    
}

-(NSMutableDictionary*)getCommonParametersForRequestBody
{
    
    SharedManager *objShared = [SharedManager sharedSingleton];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    //    // Mobile Number
    //    [dict setObject:@"8000000000" forKey:@"mno"];
    // IMEI
    //[dict setObject:[objShared appUniqueID] forKey:@"imei"];
    [dict setObject:@"" forKey:@"imei"];
    
    // Device ID
    
    [dict setObject:[objShared appUniqueID] forKey:@"did"];
    
    // IMSI
    // [dict setObject:[objShared appUniqueID] forKey:@"imsi"];
    [dict setObject:@"" forKey:@"imsi"];
    
    // Handset Make
    [dict setObject:@"Apple" forKey:@"hmk"];
    
    // Handset Model
    [dict setObject:[[UIDevice currentDevice] model] forKey:@"hmd"];
    
    // Rooted
   // [dict setObject:[self isJailbroken]?@"yes":@"no" forKey:@"rot"];
    [dict setObject: [objShared.dbManager isLibertyPatchJailBroken]?@"yes":@"no" forKey:@"rot"];

    // OS
    [dict setObject:@"ios" forKey:@"os"];
    
    // ver
    //[dict setObject:[SharedManager appVersion] forKey:@"ver"];
    [dict setObject:APP_VERSION forKey:@"ver"];
    
    // Country Code
    [dict setObject:@"404" forKey:@"mcc"];
    
    // Mobile Network Code
    [dict setObject:@"02" forKey:@"mnc"];
    
    // Lcoation Area Code
    [dict setObject:@"2065" forKey:@"lac"];
    
    // Cell ID
    [dict setObject:@"11413" forKey:@"clid"];
    
    
    // Location Based Params
    // Accuracy
    [dict setObject:@"" forKey:@"acc"];
    
    // Latitude Code
    [dict setObject:@"" forKey:@"lat"];
    
    // Longitude Code
    [dict setObject:@"" forKey:@"lon"];
    
    // State Name
    //[dict setObject:@"" forKey:@"st"];
    
    
    // Selected Language Name
    /*  NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
     if (selectedLanguage == nil) {
     selectedLanguage = @"en";
     }
     
     selectedLanguage = @"en";
     
     [dict setObject:selectedLanguage forKey:@"lang"];*/
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    bool checklang=[singleton.dbManager getServiceLanguage:[self.homeDict valueForKey:@"SERVICE_ID"] withDeviceLang:selectedLanguage];
    
    NSString *language=@"";
    if (checklang==true)
    {
        language=selectedLanguage; //add condition for it
        
    }
    else
    {
        language=@"en";
    }
    
    
    //NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    [dict setObject:language forKey:@"lang"];
    
    //[dict setObject:@"en" forKey:@"lang"];
    NSString *node = [[NSUserDefaults standardUserDefaults] objectForKey:@"NODE_KEY"];
    
    if (node == nil)
    {
        node =@"";
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [dict setObject:node forKey:@"node"];
    //-------- Add Sharding logic here-------
    // Selected Mode (Domain(Web / App/Mobile Web)
    [dict setObject:@"app" forKey:@"mod"];
    
    
    return dict;
}


- (BOOL)isJailbroken
{
    BOOL jailbroken = NO;
    NSArray *jailbrokenPath = [NSArray arrayWithObjects:@"/Applications/Cydia.app",  @"/Applications/RockApp.app",  @"/Applications/Icy.app",  @"/usr/sbin/sshd",  @"/usr/bin/sshd",  @"/usr/libexec/sftp-server",  @"/Applications/WinterBoard.app",  @"/Applications/SBSettings.app",  @"/Applications/MxTube.app",  @"/Applications/IntelliScreen.app",  @"/Library/MobileSubstrate/DynamicLibraries/Veency.plist",  @"/Applications/FakeCarrier.app",  @"/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",  @"/private/var/lib/apt",  @"/Applications/blackra1n.app",  @"/private/var/stash",  @"/private/var/mobile/Library/SBSettings/Themes",  @"/System/Library/LaunchDaemons/com.ikey.bbot.plist",  @"/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",  @"/private/var/tmp/cydia.log",  @"/private/var/lib/cydia", nil];for(NSString *string in jailbrokenPath)
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:string]){
            jailbroken = YES;
            break;}
    }
    return jailbroken;
}
-(void)loadHtml
{
    
    
    // SharedManager *singleton = [SharedManager sharedSingleton];
    
    //-------- Code to load cookies----------------
    
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    
    //-------- Code to load cookies----------------
    
    
    NSString *sid=[self.homeDict valueForKey:@"SERVICE_ID"];
    NSString *nam=singleton.profileNameSelected ;
    NSString *addr=singleton.profileUserAddress ;
    
    NSString *st=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
    NSLog(@"st=%@",st);
    if([st length]==0||[st isEqualToString:@"9999"])
    {
        st=@"";
    }
    
    
    NSString *cty=@"";
    NSString *dob=singleton.profileDOBSelected;
    NSString *gndr=str_gender;
    //NSString *qual=;
    
    NSString *qual=[obj getQualiListCode:singleton.user_Qualification];
    NSString *occup=[obj getOccuptCode:singleton.user_Occupation];
    
    NSString *email=singleton.profileEmailSelected;
    NSString *amno=singleton.altermobileNumber;
    NSString *lang=@""; //add condition for it
    
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    //NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    bool checklang=[singleton.dbManager getServiceLanguage:[self.homeDict valueForKey:@"SERVICE_ID"] withDeviceLang:selectedLanguage];
    
    if (checklang==true)
    {
        lang=selectedLanguage; //add condition for it
        
    }
    else
    {
        lang=@"en";
    }
    
    
    
    
    // NSString *aadhr= singleton.user_aadhar_number;
    NSString *aadhr= singleton.objUserProfile.objAadhar.aadhar_number;
    
    NSString *ntfp=@"1";
    NSString *ntft=@"1";
    NSString *pic=singleton.user_profile_URL;
    NSString *psprt=@"";
    NSString *dist=singleton.notiTypDistricteSelected;
    NSString *amnos=@""; //pass it
    NSString *emails=@"";//pass it
    NSString *ivrlang=@""; //pass it
    //NSString *gcmid=singleton.devicek_tkn;
    NSString *gcmid=[obj getStateCode:singleton.profilestateSelected ];
    
    
    
    // st=[self getStateEnglishName:gcmid];
    
    //NSString *gcmid=[obj getStateCode:[@"Uttarakhand" lowercaseString]];
    
    NSString *mno=singleton.mobileNumber;
    NSString *uid=singleton.user_id; //pass it
    
    if ([uid length]==0) {
        uid=@"";
    }
    
    
    
    if([nam length]==0)
    {
        nam=@"";
    }
    if([addr length]==0)
    {
        addr=@"";
    }
    
    if([dob length]==0)
    {
        dob=@"";
    }
    
    if([gndr length]==0)
    {
        gndr =@"";
    }
    
    if([qual length]==0)
    {
        qual=@"";
    }
    
    if([occup length]==0)
    {
        occup=@"";
    }
    
    if([email length]==0)
    {
        email=@"";
    }
    
    if([amno length]==0)
    {
        amno=@"";
    }
    
    
    
    if([pic length]==0)
    {
        pic=@"";
    }
    
    if([dist length]==0)
    {
        dist=@"";
    }
    if([aadhr length]==0)
    {
        aadhr=@"";
    }
    
    
    
    
    
    
    
    NSString *adhr_name=singleton.objUserProfile.objAadhar.name;
    NSString *adhr_dob=singleton.objUserProfile.objAadhar.dob;
    NSString *adhr_co=singleton.objUserProfile.objAadhar.father_name;
    NSString *adhr_gndr=singleton.objUserProfile.objAadhar.gender;
    NSString *adhr_imgurl=singleton.objUserProfile.objAadhar.aadhar_image_url;
    NSString *adhr_state=singleton.objUserProfile.objAadhar.state;
    NSString *adhr_dist=singleton.objUserProfile.objAadhar.district;
    NSString *adhr_mobilenumber = singleton.objUserProfile.objAadhar.mobile_number;
    
    if([adhr_state length]==0)
    {
        adhr_state=@"";
    }
    NSLog(@"adhar=%@",adhr_state);
    
    if([adhr_name length]==0)
    {
        adhr_name=@"";
    }
    
    if([adhr_dob length]==0)
    {
        adhr_dob=@"";
    }
    
    if([adhr_co length]==0)
    {
        adhr_co=@"";
    }
    if(adhr_mobilenumber == nil || [adhr_mobilenumber length]==0)
    {
        adhr_mobilenumber=@"";
    }
    
    
    
    if([adhr_gndr isEqualToString:@"M"])
    {
        adhr_gndr=@"Male";
    }
    if([adhr_gndr isEqualToString:@"F"])
    {
        adhr_gndr=@"Female";
    }
    
    if([adhr_gndr length]==0)
    {
        adhr_gndr=@"";
    }
    
    if([adhr_imgurl length]==0)
    {
        adhr_imgurl=@"";
    }
    
    
    if([adhr_dist length]==0)
    {
        adhr_dist=@"";
    }
    
    if (adhr_mobilenumber.length != 0)
    {
        mno = adhr_mobilenumber;
    }
    
    if([gcmid length]==0)
    {
        gcmid=@"";
    }
    
    if([gcmid isEqualToString:@"9999"])
    {
        gcmid=@"";
    }
    
    
    
    //------------------------ If needed Open this code-----------------------
    
    // st=@"";
    // dist=@"";
    if (![lang isEqualToString:@"en"]) {
        dist=@"";
        st=@"";
    }
    
    
    
    // STATETABSELECT
    //------------------------ Close this code-----------------------
    
    
    NSMutableDictionary *userinfo = [NSMutableDictionary new];
    [userinfo setObject:sid forKey:@"sid"];
    [userinfo setObject:nam forKey:@"nam"];
    [userinfo setObject:addr forKey:@"addr"];
    [userinfo setObject:st forKey:@"st"];
    [userinfo setObject:cty forKey:@"cty"];
    [userinfo setObject:dob forKey:@"dob"];
    [userinfo setObject:gndr forKey:@"gndr"];
    [userinfo setObject:qual forKey:@"qual"];
    [userinfo setObject:email forKey:@"email"];
    [userinfo setObject:amno forKey:@"amno"];
    [userinfo setObject:lang forKey:@"lang"];
    
    
    [userinfo setObject:ntfp forKey:@"ntfp"];
    [userinfo setObject:ntft forKey:@"ntft"];
    [userinfo setObject:pic forKey:@"pic"];
    [userinfo setObject:psprt forKey:@"psprt"];
    [userinfo setObject:occup forKey:@"occup"];
    [userinfo setObject:dist forKey:@"dist"];
    [userinfo setObject:amnos forKey:@"amnos"];
    [userinfo setObject:emails forKey:@"emails"];
    [userinfo setObject:ivrlang forKey:@"ivrlang"];
    [userinfo setObject:gcmid forKey:@"gcmid"];
    [userinfo setObject:mno forKey:@"mno"];
    [userinfo setObject:uid forKey:@"uid"];
    
    
    NSMutableDictionary * adhrinfo = [NSMutableDictionary new];
    [adhrinfo setObject:adhr_name forKey:@"adhr_name"];
    [adhrinfo setObject:adhr_dob forKey:@"adhr_dob"];
    [adhrinfo setObject:adhr_co forKey:@"adhr_co"];
    [adhrinfo setObject:adhr_gndr forKey:@"adhr_gndr"];
    [adhrinfo setObject:adhr_imgurl forKey:@"adhr_imgurl"];
    [adhrinfo setObject:adhr_dist forKey:@"adhr_dist"];
    [adhrinfo setObject:adhr_mobilenumber forKey:@"mno"];
    [adhrinfo setObject:adhr_state forKey:@"adhr_state"];
    
    
    NSString *tkn=singleton.user_tkn;
    
    
    
    if([tkn length]==0)
    {
        tkn=@"";
    }
    
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
    [dictBody setObject:@"app" forKey:@"mod"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:lang forKey:@"lang"];
    [dictBody setObject:tkn forKey:@"tkn"];
    [dictBody setObject:mno forKey:@"mno"];
    [dictBody setObject:[self.homeDict valueForKey:@"SERVICE_ID"] forKey:@"service_id"];
    
    if (singleton.objUserProfile.objAadhar.aadhar_number.length)
    {
        [dictBody setObject:aadhr forKey:@"aadhr"];
        
        
    }
    
    //---------- add userinfo -------------------
    [dictBody setObject:userinfo forKey:@"userinfo"];
    //---------- add adhrinfo-------------------
    
    
    
    if (singleton.objUserProfile.objAadhar.aadhar_number.length)
    {
        [dictBody setObject:adhrinfo forKey:@"adhrinfo"];
        
        
    }
    else
    {
        //dont add it
    }
    
    
    
    NSString *checkLinkStatus=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"LINKDIGILOCKERSTATUS"];
    
    if ([checkLinkStatus isEqualToString:@"YES"])
    {
        
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        
        NSString *username=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"digilocker_username"];
        NSString *password=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"digilocker_password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSMutableDictionary *digilocker = [NSMutableDictionary new];
        [digilocker setObject:username forKey:@"username"];
        [digilocker setObject:password forKey:@"password"];
        
        [dictBody setObject:digilocker forKey:@"digilocker"];
        
        
    }
    
    
    NSString *urlAddress =self.urlString;
    
    if ([urlAddress isEqualToString:@"<null>"])
    {
        urlAddress=@"";
    }
    
    if (self.paymentStateName.length==0)
    {
        self.paymentStateName=@"";
        
    }
    if (self.paymentStateId.length==0)
    {
        self.paymentStateId=@"";
    }
    
    if ([self.paymentIsStateSelected isEqualToString:@"TRUE"])
    {
        
        [dictBody setObject:self.paymentStateName forKey:@"tab_state_name"];
        [dictBody setObject:self.paymentStateId forKey:@"tab_state_id"];
        
    }
    else
    {
        self.paymentStateName=@"";
        self.paymentStateId=@"";
    }
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dictBody options:0 error:&err];
    NSString * contentToEncode = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    contentToEncode = [contentToEncode stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"contentToEncode %@",contentToEncode);
    
    NSString *contentEncoding = @"application/vnd.umang.web+json; charset=UTF-8";
    
    NSURL *postURI=[NSURL URLWithString:urlAddress];
    
    NSString *verb = @"GET";
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"EEE, d MMM yyyy HH:mm:ss z"];
    NSString *currentDate = [dateFormatter stringFromDate: [NSDate date]]; //Put whatever date you want to convert
    NSString *currentDatetrim = [currentDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString *USERNAME=@"UM4NG";
    NSString *contentMd5=[NSString stringWithFormat:@"%@",[contentToEncode MD5]];
    
    NSString *contentMd5trim = [contentMd5 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString *postURItrim=[postURI.path stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *toSign =[NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@",verb,contentMd5trim,contentEncoding,currentDatetrim,postURItrim];
    
    
    NSString *hmac=[self hmacsha1:toSign secret:SaltSHA1MAC];

    NSString *hmactrim=[hmac stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *XappAuth=[NSString stringWithFormat:@"%@:%@",USERNAME,hmactrim];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:postURI];
    
    [request addValue:XappAuth forHTTPHeaderField:@"X-App-Authorization"];
    [request addValue:currentDate forHTTPHeaderField:@"X-App-Date"];
    [request addValue:contentMd5trim forHTTPHeaderField:@"X-App-Content"];
    [request addValue:contentToEncode forHTTPHeaderField:@"X-App-Data"];
    [request addValue:contentEncoding forHTTPHeaderField:@"Content-Type"];
    
    
    //---- Adding in cookies--------------
    
    [request setHTTPShouldHandleCookies:YES];
    
    if([cookies count]>1)
    {
        [self addCookies:cookies forRequest:request];
    }
    //---- Adding in cookies--------------
    
    [self.webView loadRequest: request];
    
    
}

- (NSString *)hmacsha1:(NSString *)data secret:(NSString *)key
{
    NSString*tempKey=[key mutableCopy];
    NSString*tempdata=[data mutableCopy];
    const char *cKey  = [tempKey cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [tempdata cStringUsingEncoding:NSASCIIStringEncoding];
    if(cKey == NULL ||[tempKey length]==0)
    {
        cKey  = [tempKey UTF8String];
    }
    if(cData == NULL ||[tempdata length]==0)
    {
        cData  = [tempdata UTF8String];
    }
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    //NSString *hash = [HMAC base64EncodedString];
    NSString *hash = [HMAC base64EncodedStringWithOptions:0];
    return hash;
}

- (void)addCookies:(NSArray *)cookies forRequest:(NSMutableURLRequest *)request
{
    if ([cookies count] > 0)
    {
        NSHTTPCookie *cookie;
        NSString *cookieHeader = nil;
        for (cookie in cookies)
        {
            if (!cookieHeader)
            {
                cookieHeader = [NSString stringWithFormat: @"%@=%@",[cookie name],[cookie value]];
            }
            else
            {
                cookieHeader = [NSString stringWithFormat: @"%@; %@=%@",cookieHeader,[cookie name],[cookie value]];
            }
        }
        if (cookieHeader)
        {
            [request setValue:cookieHeader forHTTPHeaderField:@"Cookie"];
        }
    }
}

#pragma mark - WKWebview Methods

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    NSString *urlString = [[navigationAction.request URL] absoluteString];
    
    if ([urlString hasPrefix:@"ios::traceUrl:"])
    {
        NSArray *components = [urlString componentsSeparatedByString:@"::"];
        NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *data = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_JsonArray = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:data options:0 error:nil],nil];
        
        if ([self.webView canGoBack])
        {
            // [webView goBack];
            // [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
            
            @try {
                NSString *nextUrl=[NSString stringWithFormat:@"%@",[[arr_JsonArray valueForKey:@"next"] objectAtIndex:0]];
                if ([nextUrl hasSuffix:@"#/"])
                {
                    
                    [self.backButton setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
                    
                }
                else
                {
                    [self.backButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
                    
                    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
                    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
                    {
                        
                        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                                       @{NSFontAttributeName: [UIFont systemFontOfSize:self.backButton.titleLabel.font.pointSize]}];
                        
                        CGRect framebtn=CGRectMake(self.backButton.frame.origin.x, self.backButton.frame.origin.y, self.backButton.frame.size.width, self.backButton.frame.size.height);
                        
                        [self.backButton setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
                        self.backButton.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
                        
                    }
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
        }
        else
        {
            [self.backButton setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
        }
        
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
        
    }
    else if ([urlString hasPrefix:@"ios::closepayment::"])
    {
        NSArray *components = [urlString componentsSeparatedByString:@"::"];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try
        {
            
            NSString *urlString = [json valueForKey:@"url"];
            
            [self.delegate loadPreviousWebViewWithURLString:urlString andServicedict:self.homeDict];
            
            [self dismissViewControllerAnimated:NO completion:nil];
            
            
        } @catch (NSException *exception)
        {
            
        } @finally
        {
            
        }
        
        
    }
    
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error
{
    [activityIndicator stopAnimating];
    NSLog(@"%@",error.description);
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation
{
    [activityIndicator startAnimating];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    [activityIndicator stopAnimating];
}


- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler();
                                                      }]];
    [self presentViewController:alertController animated:YES completion:^{}];
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"No"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler(false);
                                                      }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler(true);
                                                      }]];
    
    
    [self presentViewController:alertController animated:YES completion:^{}];
}
#pragma mark -
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end

