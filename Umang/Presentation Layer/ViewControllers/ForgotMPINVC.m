
//
//  ForgotMPINVC.m
//  Umang
//
//  Created by admin on 21/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ForgotMPINVC.h"
#import "UMAPIManager.h"

#import "MBProgressHUD.h"
#import "EnterMobileOTPVC.h"


#define MAX_LENGTH 10
#define kOFFSET_FOR_KEYBOARD 80.0

@interface ForgotMPINVC ()<UIScrollViewDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UIButton *btnNext;
    __weak IBOutlet UITextField *txtMobileNumber;
    __weak IBOutlet UIView *vwTxtBG;
    MBProgressHUD *hud ;
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblSendSMS;
    __weak IBOutlet UILabel *lblForgotMPIN;
    __weak IBOutlet UILabel *lblRegisteredNum;
    
    IBOutlet UIScrollView *scrollview;
}

@end

@implementation ForgotMPINVC
@synthesize tout,rtry;
@synthesize tagFrom;

/*
 - (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
*/
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (fDeviceHeight<=568)
    {
     [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (fDeviceHeight<=568)
    {

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    }
    
    [self setFontforView:self.view andSubViews:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568)
    {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [txtMobileNumber resignFirstResponder];
        
    }
    }
    
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setFontforView:self.view andSubViews:YES];
    [self setViewFont];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btnBack.hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btnBackClicked:btnBack];
    };
    [self.view addSubview:nvView];
    if (iPhoneX()) {
        CGRect scroll = scrollview.frame;
        scroll.origin.y = kiPhoneXNaviHeight;
        scroll.size.height = fDeviceHeight - kiPhoneXNaviHeight;
        scrollview.frame = scroll;
        [self.view layoutIfNeeded];
    }
    
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [btnNext.titleLabel setFont:[AppFont mediumFont:19.0]];
    lblForgotMPIN.font = [AppFont semiBoldFont:27.0];
    lblRegisteredNum.font = [AppFont semiBoldFont:16.0];
    lblSendSMS.font = [AppFont mediumFont:13.0];
   // countryCode.font = [AppFont mediumFont:22];
    txtMobileNumber.font = [AppFont mediumFont:22];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   
    if (self.view.frame.size.width > 500)
    {
        btnNext.frame = CGRectMake(200, btnNext.frame.origin.y, 368, 65);
    }
    
    [btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnNext.layer.cornerRadius = 3.0f;
    btnNext.clipsToBounds = YES;
    
    if([tagFrom isEqualToString:@"INSIDESETTINGS"])
    {
        txtMobileNumber.userInteractionEnabled=FALSE;
        txtMobileNumber.text=self.strMobileNumber;
        
    }
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:FORGOT_MPIN_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];

    
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    lblForgotMPIN.text = NSLocalizedString(@"initiate_forgot_mpin", nil);
    lblRegisteredNum.text = NSLocalizedString(@"enter_registered_mobile_num", nil);
    
    lblSendSMS.text = NSLocalizedString(@"forgor_mpin_mobile_text", nil);
    
    txtMobileNumber.clearButtonMode = UITextFieldViewModeWhileEditing;

    
    txtMobileNumber.delegate=self;
    scrollview.delegate=self;
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollview.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height;
                       scrollview.contentSize = contentRect.size;
                   });
    
    if (_strMobileNumber)
    {
        txtMobileNumber.text =  _strMobileNumber;
    }
       if (_strMobileNumber.length == 10) {
           btnNext.enabled= YES;
             [self enableBtnNext:YES];

    }
    else
    {
          btnNext.enabled=NO;
    }
    singleton = [SharedManager sharedSingleton];
    
    
  
    self.view.userInteractionEnabled = YES;
    
    [txtMobileNumber addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    vwTxtBG.layer.borderColor = [UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1.0].CGColor;
   // vwTxtBG.layer.borderWidth = 2.0;
 //   vwTxtBG.layer.cornerRadius = 4.0;
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    [self addNavigationView];
}

-(void)hideKeyboard
{
    [scrollview setContentOffset:
     CGPointMake(0, -scrollview.contentInset.top) animated:YES];
    
    

    [txtMobileNumber resignFirstResponder];
}
- (IBAction)btnBackClicked:(id)sender
{
    
    if([tagFrom isEqualToString:@"INSIDESETTINGS"])
    {
        //[self.navigationController popViewControllerAnimated:YES];
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.text.length >= MAX_LENGTH)
    {
        textField.text = [textField.text substringToIndex:MAX_LENGTH];
        
         btnNext.enabled=YES;
        [self enableBtnNext:YES];
        
        // NSLog(@"got it");
    }
    else
    {
         btnNext.enabled=NO;
        [self enableBtnNext:NO];
    }
}

-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        [self hideKeyboard];
        
        //[btnNext setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[btnNext setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];

    }
    else
    {
        //[btnNext setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[btnNext setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [btnNext setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
    NSString *phoneRegex =@"[6789][0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
}


- (IBAction)btnNextClicked:(id)sender
{
    //jump to next step here
    if ([self validatePhone:txtMobileNumber.text]!=TRUE) {
        
        NSLog(@"Wrong Mobile");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:NSLocalizedString(@"enter_correct_phone_number", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        [self hitAPI];

        
    }
}



-(void)hitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);    

    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:txtMobileNumber.text forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"sms" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile contact //not supported iphone
    [dictBody setObject:@"frgtmpn" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    
    
    singleton.mobileNumber=txtMobileNumber.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FORGOT_MPIN withBody:dictBody andTag:TAG_REQUEST_INIT_REG completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
          //  NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
         //   NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
         //   NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
            
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            

            
            
            //------ Sharding Logic parsing---------------
            

            tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
            rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
            

            
        //    NSString *rc=[response valueForKey:@"rc"];
        //    NSString *rd=[response valueForKey:@"rd"];
        //    NSString *rs=[response valueForKey:@"rs"];
            
          
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

                EnterMobileOTPVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"EnterMobileOTPVC"];
                [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                vc.TYPE_LOGIN_CHOOSEN = ISFROMFORGOTMPIN;
                vc.tout=tout;
                vc.rtry=rtry;
                vc.lblScreenTitleName.text = NSLocalizedString(@"verify_otp_label", nil);
                [self presentViewController:vc animated:YES completion:nil];

            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
