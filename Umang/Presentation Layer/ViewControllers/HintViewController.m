//
//  HintViewController.m
//  Home
//
//  Created by deepak singh rawat on 01/10/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import "HintViewController.h"

@interface HintViewController ()

@end

@implementation HintViewController
@synthesize scrollView;
@synthesize page,pageImages;
@synthesize pageControl;

- (void)viewDidLoad
{
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: HINT_VIEW_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];

    
    [(UIButton*)btn_skip setTitle:NSLocalizedString(@"skip", @"") forState:UIControlStateNormal];
    [(UIButton*)btn_next setTitle:NSLocalizedString(@"next", @"") forState:UIControlStateNormal];
    
    
    
    // Create the data model
    
    
    pageImages = @[@"hint1.png", @"hint2.png", @"hint3.png"];
    
    
    // Bound the screen according to width size of the screen
    CGSize size = [[UIScreen mainScreen] bounds].size;
    CGFloat frameX = size.width;
    CGFloat frameY = size.height; //padding for UIpageControl
    
    //Add scrollview programatically
    //scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frameX, frameY)];
    scrollView.pagingEnabled = YES;
    scrollView.delegate=self;
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.contentSize = CGSizeMake(frameX, frameY);
    //[self.view addSubview: scrollView];
    self.view.backgroundColor=[[UIColor clearColor] colorWithAlphaComponent:0.0];
    int x=0;
    // Add images and text according to data model
    for(int i = 0; i < [pageImages count]; i++)
    {
        
        //=============== Progratically adding center UIImageview===============
        UIImage *image = [UIImage imageNamed:[pageImages objectAtIndex:i]];
        UIImageView*  imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(x,0, frameX, frameY);
        [scrollView addSubview:imageView];
        
        x=x+frameX;
        
    }
    
    scrollView.contentSize = CGSizeMake(frameX*[pageImages count], frameY);
    scrollView.showsHorizontalScrollIndicator = NO;//hide scrollbar
    scrollView.showsVerticalScrollIndicator = NO;
    //--------- Add page control in the scrollview ----------
    pageControl.numberOfPages = 3; //as we added 3 diff views
    pageControl.currentPage = 0;
    pageControl.pageIndicatorTintColor =[UIColor lightGrayColor] ;
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    pageControl.backgroundColor = [UIColor clearColor];
    [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: pageControl];
    
    
   // BOOL isHintShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"HintShown"];

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    [super viewDidLoad];
    
    //For hint page shown only once
      
}

#pragma Scrollview delegate function for scroll with page enable
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    CGFloat frameX = size.width;
    
    
    //CGFloat pageWidth = self.scrollView.frame.size.width;
    page = floor((self.scrollView.contentOffset.x - frameX / 2 ) / frameX) + 1; //this provide you the page number
    pageControl.currentPage = page;// this displays the white dot as current page
    
    [self changeBtnName:page];
}


// -------- Method to change page on click of page controll--------------
- (IBAction)changePage:(id)sender
{
    UIPageControl *pager=sender;
    NSInteger myInteger = pager.currentPage;
    page = (int) myInteger;
    NSLog(@"page=%d",page);
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    
    [self changeBtnName:page];
    
    
    [scrollView addSubview:btn_skip];
    [scrollView addSubview:lbl_line];
    [scrollView addSubview:btn_next];
    
}

//------- Method to change name of the UIButton---------

-(void)changeBtnName:(int)pageIndex
{
    if (pageIndex==2) {
        btn_skip.hidden=TRUE;
        lbl_line.hidden=TRUE;
        [(UIButton*)btn_next setTitle:NSLocalizedString(@"finish", @"") forState:UIControlStateNormal];
    }
    else
    {
        lbl_line.hidden=FALSE;
        
        btn_skip.hidden=FALSE;
        [(UIButton*)btn_next setTitle:NSLocalizedString(@"next", @"") forState:UIControlStateNormal];
        
    }
}

//------ Method Finish Press action ------------------
-(IBAction)finishPressed:(id)sender
{
    NSLog(@"title of button=%@",btn_next.titleLabel.text);
    
    if ([btn_next.titleLabel.text isEqualToString:NSLocalizedString(@"finish", @"")])
    {
        //call skip pressed action
        [self skipPressed:self];
    }
    else
    {
        
        // NSLog(@"page=%d",page);
        page=page+1;// get current page number globally define and add +1 to get next
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        [scrollView scrollRectToVisible:frame animated:YES];
        [self changeBtnName:page];
        
    }
    
}

//--------------Method Skip Pressed to go to next View by skip current--------------
- (IBAction)skipPressed:(id)sender
{
    
    [UIView animateWithDuration:0.4 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

@end
