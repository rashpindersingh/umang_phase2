//
//  RegStep2ViewController.h
//  SpiceRegistration
//
//  Created by spice_digital on 21/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegStep2ViewController : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIView *resendOTPview;
    IBOutlet UIScrollView *scrollView;

}
typedef enum
{
    
    ISFROMFORGOTMPIN= 100,
    ISFROMLOGINWITHOTP,
    Is_From_Choose_Registration_With_Mobile,
    Link_Aadhar_After_Mobile,
    IS_FROM_MOBILE_NUMBER_REGISTRATION,
    IS_FROM_AADHAR_LOGIN,
    IS_FROM_AADHAR_REGISTRATION,
    IS_FROM_PROFILE_LINK,
    
}TYPE_LOGINSCREEN;



//--- for handling all resend and call option------
typedef enum
{
    
    ISFROMADHARREGISTRATIONVC= 100,
    ISFROMFORGOTMPINVC,
    ISFROMLOGINWITHOTPVC,
    ISFROMLOGINAPPVC,
    ISFROMREGISTRATIONSTEP1VC,
    
}TYPE_RESEND_OTP_TYPE;
@property (assign,nonatomic )  TYPE_RESEND_OTP_TYPE TYPE_RESEND_OTP_FROM;
@property(nonatomic,retain)NSString *altmobile;
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
@property(nonatomic,retain)NSString *TAGFROM;

//--- for handling all resend and call option------


@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property(nonatomic,retain)IBOutlet UIScrollView *scrollView;

@property(strong,nonatomic) NSString *strAadharNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblScreenTitleName;
@property (nonatomic,assign) BOOL isFromForgotMPIN;
@property (nonatomic,assign) BOOL isFromLoginWithOTP;
@property (assign,nonatomic )  TYPE_LOGINSCREEN TYPE_LOGIN_CHOOSEN;

@end
