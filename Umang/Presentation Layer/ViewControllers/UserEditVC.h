//
//  UserEditVC.h
//  Umang
//
//  Created by admin on 30/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserEditVC : UIViewController
{
    SharedManager *singleton;
    IBOutlet UIButton *btn_save;
}

@property(nonatomic,strong)NSData *imgtopass;

-(IBAction)btnBackAction:(id)sender;
@property(strong,nonatomic)NSString *tagFrom;
@property(nonatomic,retain)NSDictionary *dic_info;
@property(nonatomic,retain)UIImage *user_img;

@end

