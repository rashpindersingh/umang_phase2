//
//  DigiLockerWebVC.m
//  Umang
//
//  Created by deepak singh rawat on 21/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "DigiLockerWebVC.h"
#import "MBProgressHUD.h"
#import <WebKit/WebKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import "UIView+Toast.h"
#import "FAQWebVC.h"
#import "Base64.h"
#import "UIImageView+WebCache.h"
#import "UMAPIManager.h"
#import "NSString+MD5.h"
#import "MBProgressHUD.h"
#import "StateList.h"
#import "FAQWebVC.h"
//#import "NSData+MD5.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSData+Base64.h"
#import <MobileCoreServices/MobileCoreServices.h>


@interface DigiLockerWebVC ()<WKScriptMessageHandler,WKUIDelegate,WKNavigationDelegate,UIWebViewDelegate,UIGestureRecognizerDelegate>
{
    SharedManager *singleton;
    NSString *urlString;
    MBProgressHUD *hud;
    UIWebView *wvPopUp;
    bool flagBack;
    
    NSData* data ;
    NSURL *postURI;
    
    //------------profile parameter---------------
    //----- local values  to be save and show from API-----------
    NSString *str_name;
    NSString *str_gender;
    NSString *str_dob;
    NSString *str_qualification;
    NSString *str_occupation;
    NSString *str_state;
    NSString *str_district;
    NSString *str_registerMb;
    NSString *str_emailAddress;
    NSString *str_alternateMb;
    NSString *str_emailVerifyStatus;
    NSString *str_amnosVerifyStatus;
    NSString *str_address;
    
    __weak IBOutlet UIButton *btnBack;
    NSString *str_Url_pic;
    
    NSMutableArray *socialpd;
    
    
    
    StateList *obj;
    
    NSMutableArray *statelocalPlist;
    //------------End parameter--------
    bool flagViewAppear;
    
    BOOL flag_chat;
    BOOL flag_homebtn;
    
    UIActivityIndicatorView *activityIndicator ;
}
@property(nonatomic,retain)NSString *camera_failCallback;
@property(nonatomic,retain)NSString *camera_successCallback;
@property(nonatomic,retain)NSString *camera_img_base64;
@property(nonatomic,retain)NSString *authTokenKey;

@property (nonatomic, retain) IBOutlet UIBarButtonItem *web_back_btn;

@property(nonatomic,retain)NSString *str_gender;
@property(nonatomic,retain)NSString *urlString;
@property(nonatomic,retain)NSString*service_ID;

@property(nonatomic,retain)NSString*mainDocumentURL;

@property(nonatomic,retain)NSString*previousURL;
@property(nonatomic,retain)NSString*nextUrl;
@end

@implementation DigiLockerWebVC
@synthesize urltoOpen;
@synthesize titleOpen;
@synthesize camera_failCallback;
@synthesize camera_successCallback;
@synthesize camera_img_base64;
@synthesize nextUrl,previousURL;

@synthesize mainDocumentURL;

@synthesize service_ID;
@synthesize str_gender;
@synthesize urlString;



@synthesize authTokenKey;


- (void)viewDidUnload
{
    self.webView = nil;
    
    [super viewDidUnload];
}

/*
 
 - (NSString *)hmacsha1:(NSString *)data1 secret:(NSString *)key {
 
 const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
 const char *cData = [data1 cStringUsingEncoding:NSASCIIStringEncoding];
 
 unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
 
 CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
 
 NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
 
 NSString *hash = [HMAC base64EncodedString];
 
 return hash;
 }
 */


- (NSString *)hmacsha1:(NSString *)data1 secret:(NSString *)key
{
    NSString*tempKey=[key mutableCopy];
    NSString*tempdata=[data1 mutableCopy];
    const char *cKey  = [tempKey cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [tempdata cStringUsingEncoding:NSASCIIStringEncoding];
    if(cKey == NULL ||[tempKey length]==0)
    {
        cKey  = [tempKey UTF8String];
    }
    if(cData == NULL ||[tempdata length]==0)
    {
        cData  = [tempdata UTF8String];
    }
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    //NSString *hash = [HMAC base64EncodedString];
    NSString *hash = [HMAC base64EncodedStringWithOptions:0];
    return hash;
}








-(NSMutableDictionary*)getCommonParametersForRequestBody
{
    
    SharedManager *objShared = [SharedManager sharedSingleton];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    //    // Mobile Number
    //    [dict setObject:@"8000000000" forKey:@"mno"];
    // IMEI
    //[dict setObject:[objShared appUniqueID] forKey:@"imei"];
    [dict setObject:@"" forKey:@"imei"];
    
    // Device ID
    
    
    [dict setObject:[objShared appUniqueID] forKey:@"did"];
    
    // IMSI
    // [dict setObject:[objShared appUniqueID] forKey:@"imsi"];
    [dict setObject:@"" forKey:@"imsi"];
    
    // Handset Make
    [dict setObject:@"Apple" forKey:@"hmk"];
    
    // Handset Model
    [dict setObject:[[UIDevice currentDevice] model] forKey:@"hmd"];
    
    
    
    
    // Rooted
   // [dict setObject:[self isJailbroken]?@"yes":@"no" forKey:@"rot"];
    [dict setObject: [objShared.dbManager isLibertyPatchJailBroken]?@"yes":@"no" forKey:@"rot"];

    // OS
    [dict setObject:@"ios" forKey:@"os"];
    
    // ver
    [dict setObject:APP_VERSION forKey:@"ver"];
    
    // Country Code
    [dict setObject:@"404" forKey:@"mcc"];
    
    // Mobile Network Code
    [dict setObject:@"02" forKey:@"mnc"];
    
    // Lcoation Area Code
    [dict setObject:@"2065" forKey:@"lac"];
    
    // Cell ID
    [dict setObject:@"11413" forKey:@"clid"];
    
    
    // Location Based Params
    // Accuracy
    [dict setObject:@"" forKey:@"acc"];
    
    // Latitude Code
    [dict setObject:@"" forKey:@"lat"];
    
    // Longitude Code
    [dict setObject:@"" forKey:@"lon"];
    
    // State Name
    //[dict setObject:@"" forKey:@"st"];
    
    
    // Selected Language Name
    /*  NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
     if (selectedLanguage == nil) {
     selectedLanguage = @"en";
     }
     
     selectedLanguage = @"en";
     
     [dict setObject:selectedLanguage forKey:@"lang"];*/
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    /* bool checklang=[singleton.dbManager getServiceLanguage:service_ID withDeviceLang:selectedLanguage];
     
     NSString *language=@"";
     if (checklang==true)
     {
     language=selectedLanguage; //add condition for it
     
     }
     else
     {
     language=@"en";
     }*/
    
    
    //NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    [dict setObject:selectedLanguage forKey:@"lang"];
    
    //[dict setObject:@"en" forKey:@"lang"];
    NSString *node = [[NSUserDefaults standardUserDefaults] objectForKey:@"NODE_KEY"];
    
    if (node == nil)
    {
        node =@"";
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [dict setObject:node forKey:@"node"];
    //-------- Add Sharding logic here-------
    // Selected Mode (Domain(Web / App/Mobile Web)
    [dict setObject:@"app" forKey:@"mod"];
    
    
    return dict;
}

-(BOOL)isJailbroken {
    NSURL* url = [NSURL URLWithString:@"cydia://package/com.example.package"];
    return [[UIApplication sharedApplication] canOpenURL:url];
}


/*- (BOOL)isJailbroken
 {
 BOOL jailbroken = NO;
 NSArray *jailbrokenPath = [NSArray arrayWithObjects:@"/Applications/Cydia.app",  @"/Applications/RockApp.app",  @"/Applications/Icy.app",  @"/usr/sbin/sshd",  @"/usr/bin/sshd",  @"/usr/libexec/sftp-server",  @"/Applications/WinterBoard.app",  @"/Applications/SBSettings.app",  @"/Applications/MxTube.app",  @"/Applications/IntelliScreen.app",  @"/Library/MobileSubstrate/DynamicLibraries/Veency.plist",  @"/Applications/FakeCarrier.app",  @"/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",  @"/private/var/lib/apt",  @"/Applications/blackra1n.app",  @"/private/var/stash",  @"/private/var/mobile/Library/SBSettings/Themes",  @"/System/Library/LaunchDaemons/com.ikey.bbot.plist",  @"/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",  @"/private/var/tmp/cydia.log",  @"/private/var/lib/cydia", nil];for(NSString *string in jailbrokenPath)
 {
 if ([[NSFileManager defaultManager] fileExistsAtPath:string]){
 jailbroken = YES;
 break;}
 }
 return jailbroken;
 }*/



- (void)viewDidLoad {
    authTokenKey=@"";
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:DIGILOCKER_WEB_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    //  lbltitle.text=titleOpen;
    
    NSString *titlestr=NSLocalizedString(@"digi_locker", nil);
    lbltitle.text=titlestr;
    
    singleton = [SharedManager sharedSingleton];
    //lbltitle.text=titleOpen;
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    _webView.delegate=self;
    
    flag_homebtn=FALSE;
    
    
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        // [self hitFetchProfileAPI];
        
    });
    self.webView.delegate = self;
    
    flagViewAppear=TRUE;
    
    [self hitFetchProfileAPI];
    
    
    [btnBack setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
    // urlString=[dic_serviceInfo valueForKey:@"URL"];
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    self.view.backgroundColor = [UIColor colorWithRed:241/255.0 green:246/255.0 blue:246/255.0 alpha:1.0];
    
    self.webView.scalesPageToFit = YES;
    self.webView.backgroundColor= [UIColor colorWithRed:241/255.0 green:246/255.0 blue:246/255.0 alpha:1.0];
    
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    // activityIndicator.backgroundColor=[UIColor grayColor];
    [self.view addSubview: activityIndicator];
    
    
    
    
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}

-(void)hitFetchProfileAPI
{
    //hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    // hud.label.text = NSLocalizedString(@"Please wait...", @"Please wait...");
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        // [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rs=[response valueForKey:@"rs"];
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            //NSString *rd=[response valueForKey:@"rd"];
            
            
            NSString *responseStr=[NSString stringWithFormat:@"%@",response];
            if ([responseStr length]>0)
            {
                
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    
                {
                    
                    @try {
                        
                        
                        //singleton.user_tkn=tkn;
                        NSMutableArray *aadharpd=[[NSMutableArray alloc]init];
                        aadharpd=[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];
                        
                        
                        NSMutableArray *generalpd=[[NSMutableArray alloc]init];
                        generalpd=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                        
                        
                        
                        socialpd=[[NSMutableArray alloc]init];
                        
                        singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                        //-------- Add later----------
                        singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                        singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                        //-------- Add later----------
                        
                        
                        
                        if ([singleton.user_id length]==0) {
                            singleton.user_id=@"";
                        }
                        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        
                        // [defaults setObject:singleton.user_id forKey:@"USER_ID"];
                        
                        //[defaults synchronize];
                        
                        
                        //------------------------- Encrypt Value------------------------
                        
                        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                        // Encrypt
                        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        //------------------------- Encrypt Value------------------------
                        
                        
                        //NSString *identifier =( [[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        socialpd=(NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        
                        
                        
                        //----- local values  to be save and show from API-----------
                        
                        
                        
                        // NSString * sid=[generalpd valueForKey:@"sid"];
                        
                        str_name=[generalpd valueForKey:@"nam"];
                        str_registerMb =[generalpd valueForKey:@"mno"];
                        str_alternateMb=[generalpd valueForKey:@"amno"];
                        //str_city=[generalpd valueForKey:@"cty"];
                        str_state=[generalpd valueForKey:@"st"];
                        
                        // state_id=[obj getStateCode:str_state];
                        
                        
                        str_district=[generalpd valueForKey:@"dist"];
                        str_dob=[generalpd valueForKey:@"dob"];
                        str_gender=[generalpd valueForKey:@"gndr"];
                        str_emailAddress=[generalpd valueForKey:@"email"];
                        str_emailVerifyStatus=[generalpd valueForKey:@"emails"];
                        str_amnosVerifyStatus=[generalpd valueForKey:@"amnos"];
                        str_Url_pic=[generalpd valueForKey:@"pic"];
                        str_address=[generalpd valueForKey:@"addr"];
                        
                        NSLog(@"str_gender=%@",str_gender);
                        
                        
                        NSString *quali_id=[generalpd valueForKey:@"qual"];
                        NSString *Occu_id=[generalpd valueForKey:@"occup"];
                        
                        
                        str_qualification=[obj getqualName:quali_id];
                        str_occupation=[obj getOccuptname:Occu_id];
                        
                        NSLog(@"str_Url_pic  =>>> %@",str_Url_pic);
                        
                        
                        if ([[response objectForKey:@"socialpd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"socialpd"]];
                        }
                        if ([[response objectForKey:@"pd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                        }
                        
                        [self setProfileData];
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                }
            }
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            [self setProfileData];
            
        }
        
    }];
    
}

-(NSString *)UpperFirstWord:(NSString*)inputString
{
    
    
    if ([inputString length]!=0) {
        inputString = [inputString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[inputString substringToIndex:1] uppercaseString]];
    }
    return inputString;
}


-(void)setProfileData
{
    
    //----- local values  to be save and show from API-----------
    
    
    str_name=[self UpperFirstWord:str_name];
    // str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    
    
    
    
    //  if ([str_Url_pic length]!=0 ) {
    
    singleton.user_profile_URL=str_Url_pic;
    //   }
    
    
    
    
    
    
    if ([str_gender length]!=0) {
        str_gender=[str_gender uppercaseString];
        
        /*  if ([str_gender isEqualToString:@"M"]) {
         stringGender= NSLocalizedString(@"gender_male", nil);
         singleton.notiTypeGenderSelected= NSLocalizedString(@"gender_male", nil);
         
         }
         else  if ([str_gender isEqualToString:@"F"]) {
         singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_female", nil);
         stringGender=NSLocalizedString(@"gender_female", nil);
         
         }
         else  if ([str_gender isEqualToString:@"T"]||[str_gender isEqualToString:@"t"]) {
         singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_transgender", nil);
         stringGender=NSLocalizedString(@"gender_transgender", nil);
         
         }
         
         else
         {
         singleton.notiTypeGenderSelected=@"";
         stringGender=@"";
         }
         */
        
    }
    
    str_name=[self UpperFirstWord:str_name];
    //str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    //singleton.notiTypeGenderSelected
    singleton.profileNameSelected =str_name;
    singleton.profilestateSelected=str_state;
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=str_district;
    singleton.profileDOBSelected=str_dob;
    singleton.altermobileNumber=str_alternateMb;
    singleton.user_Qualification=str_qualification;
    singleton.user_Occupation=str_occupation;
    singleton.profileEmailSelected=str_emailAddress;
    singleton.profileUserAddress=str_address;
    
    NSLog(@"str_gender=%@",str_gender);
    
    
    [self loadHtml];
    
    
    
    
}





- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


//---------Loading HTML On webview----------
-(void)loadHtml
{
    
    
    // SharedManager *singleton = [SharedManager sharedSingleton];
    
    
    
    NSString *sid=@"";
    NSString *nam=singleton.profileNameSelected ;
    NSString *addr=singleton.profileUserAddress ;
    
    NSString *st=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
    NSLog(@"st=%@",st);
    if([st length]==0||[st isEqualToString:@"9999"])
    {
        st=@"";
    }
    
    
    NSString *cty=@"";
    NSString *dob=singleton.profileDOBSelected;
    NSString *gndr=str_gender;
    //NSString *qual=;
    
    NSString *qual=[obj getQualiListCode:singleton.user_Qualification];
    NSString *occup=[obj getOccuptCode:singleton.user_Occupation];
    
    NSString *email=singleton.profileEmailSelected;
    NSString *amno=singleton.altermobileNumber;
    NSString *lang=@""; //add condition for it
    
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    lang=selectedLanguage;
    //===fix lint-----
    NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    /* bool checklang=[singleton.dbManager getServiceLanguage:service_ID withDeviceLang:selectedLanguage];
     
     if (checklang==true)
     {
     lang=selectedLanguage; //add condition for it
     
     }
     else
     {
     lang=@"en";
     }
     */
    
    
    
    // NSString *aadhr= singleton.user_aadhar_number;
    NSString *aadhr= singleton.objUserProfile.objAadhar.aadhar_number;
    
    NSString *ntfp=@"1";
    NSString *ntft=@"1";
    NSString *pic=singleton.user_profile_URL;
    NSString *psprt=@"";
    NSString *dist=singleton.notiTypDistricteSelected;
    NSString *amnos=@""; //pass it
    NSString *emails=@"";//pass it
    NSString *ivrlang=@""; //pass it
    //NSString *gcmid=singleton.devicek_tkn;
    NSString *gcmid=[obj getStateCode:singleton.profilestateSelected ];
    
    
    
    // st=[self getStateEnglishName:gcmid];
    
    //NSString *gcmid=[obj getStateCode:[@"Uttarakhand" lowercaseString]];
    
    NSString *mno=singleton.mobileNumber;
    NSString *uid=singleton.user_id; //pass it
    
    if ([uid length]==0) {
        uid=@"";
    }
    
    
    
    if([nam length]==0)
    {
        nam=@"";
    }
    if([addr length]==0)
    {
        addr=@"";
    }
    
    if([dob length]==0)
    {
        dob=@"";
    }
    
    if([gndr length]==0)
    {
        gndr =@"";
    }
    
    if([qual length]==0)
    {
        qual=@"";
    }
    
    if([occup length]==0)
    {
        occup=@"";
    }
    
    if([email length]==0)
    {
        email=@"";
    }
    
    if([amno length]==0)
    {
        amno=@"";
    }
    
    
    
    if([pic length]==0)
    {
        pic=@"";
    }
    
    if([dist length]==0)
    {
        dist=@"";
    }
    if([aadhr length]==0)
    {
        aadhr=@"";
    }
    
    
    
    
    
    NSString *adhr_name=singleton.objUserProfile.objAadhar.name;
    NSString *adhr_dob=singleton.objUserProfile.objAadhar.dob;
    NSString *adhr_co=singleton.objUserProfile.objAadhar.father_name;
    NSString *adhr_gndr=singleton.objUserProfile.objAadhar.gender;
    NSString *adhr_imgurl=singleton.objUserProfile.objAadhar.aadhar_image_url;
    NSString *adhr_state=singleton.objUserProfile.objAadhar.state;
    NSString *adhr_dist=singleton.objUserProfile.objAadhar.district;
    
    if([adhr_name length]==0)
    {
        adhr_name=@"";
    }
    
    if([adhr_dob length]==0)
    {
        adhr_dob=@"";
    }
    
    if([adhr_co length]==0)
    {
        adhr_co=@"";
    }
    
    if([adhr_gndr length]==0)
    {
        adhr_gndr=@"";
    }
    
    if([adhr_imgurl length]==0)
    {
        adhr_imgurl=@"";
    }
    
    if([adhr_state length]==0)
    {
        adhr_state=@"";
    }
    if([adhr_dist length]==0)
    {
        adhr_dist=@"";
    }
    //--- fix lint
    NSLog(@"adhr_state=%@",adhr_state);
    
    
    
    if([gcmid length]==0)
    {
        gcmid=@"";
    }
    
    if([gcmid isEqualToString:@"9999"])
    {
        gcmid=@"";
    }
    
    
    //------------------------ If needed Open this code-----------------------
    
    // st=@"";
    // dist=@"";
    if (![lang isEqualToString:@"en"]) {
        dist=@"";
        st=@"";
    }
    
    //------------------------ Close this code-----------------------
    
    
    NSMutableDictionary *userinfo = [NSMutableDictionary new];
    [userinfo setObject:sid forKey:@"sid"];
    [userinfo setObject:nam forKey:@"nam"];
    [userinfo setObject:addr forKey:@"addr"];
    [userinfo setObject:st forKey:@"st"];
    [userinfo setObject:cty forKey:@"cty"];
    [userinfo setObject:dob forKey:@"dob"];
    [userinfo setObject:gndr forKey:@"gndr"];
    [userinfo setObject:qual forKey:@"qual"];
    [userinfo setObject:email forKey:@"email"];
    [userinfo setObject:amno forKey:@"amno"];
    [userinfo setObject:lang forKey:@"lang"];
    
    
    [userinfo setObject:ntfp forKey:@"ntfp"];
    [userinfo setObject:ntft forKey:@"ntft"];
    [userinfo setObject:pic forKey:@"pic"];
    [userinfo setObject:psprt forKey:@"psprt"];
    [userinfo setObject:occup forKey:@"occup"];
    [userinfo setObject:dist forKey:@"dist"];
    [userinfo setObject:amnos forKey:@"amnos"];
    [userinfo setObject:emails forKey:@"emails"];
    [userinfo setObject:ivrlang forKey:@"ivrlang"];
    [userinfo setObject:gcmid forKey:@"gcmid"];
    [userinfo setObject:mno forKey:@"mno"];
    [userinfo setObject:uid forKey:@"uid"];
    
    
    
    
    
    
    
    NSMutableDictionary * adhrinfo = [NSMutableDictionary new];
    [adhrinfo setObject:adhr_name forKey:@"adhr_name"];
    [adhrinfo setObject:adhr_dob forKey:@"adhr_dob"];
    [adhrinfo setObject:adhr_co forKey:@"adhr_co"];
    [adhrinfo setObject:adhr_gndr forKey:@"adhr_gndr"];
    [adhrinfo setObject:adhr_imgurl forKey:@"adhr_imgurl"];
    [adhrinfo setObject:adhr_dist forKey:@"adhr_dist"];
    
    
    
    NSString *tkn=singleton.user_tkn;
    
    
    
    if([tkn length]==0)
    {
        tkn=@"";
    }
    
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
    [dictBody setObject:@"app" forKey:@"mod"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:lang forKey:@"lang"];
    [dictBody setObject:tkn forKey:@"tkn"];
    [dictBody setObject:mno forKey:@"mno"];
    //[dictBody setObject:service_ID forKey:@"service_id"];
    [dictBody setObject:@"" forKey:@"service_id"];
    
    if (singleton.objUserProfile.objAadhar.aadhar_number.length)
    {
        [dictBody setObject:aadhr forKey:@"aadhr"];
        
        
    }
    
    //---------- add userinfo -------------------
    [dictBody setObject:userinfo forKey:@"userinfo"];
    //---------- add adhrinfo-------------------
    
    
    
    if (singleton.objUserProfile.objAadhar.aadhar_number.length)
    {
        [dictBody setObject:adhrinfo forKey:@"adhrinfo"];
        
        
    }
    else
    {
        //dont add it
    }
    
    
    
    NSString *checkLinkStatus=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"LINKDIGILOCKERSTATUS"];
    
    if ([checkLinkStatus isEqualToString:@"YES"])
    {
        
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        
        NSString *username=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"digilocker_username"];
        NSString *password=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"digilocker_password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSMutableDictionary *digilocker = [NSMutableDictionary new];
        [digilocker setObject:username forKey:@"username"];
        [digilocker setObject:password forKey:@"password"];
        
        
        
        [dictBody setObject:digilocker forKey:@"digilocker"];
        
        //------------------------- Encrypt Value------------------------
        
    }
    
    
    //urlString=@"https://web.umang.gov.in/diglock/api/deptt/diglockerHtml";
    //urlString=@"https://devweb.umang.gov.in/diglock/api/deptt/diglockerHtml";
    
    urlString=@"https://stgweb.umang.gov.in/diglock/api/deptt/diglockerHtml";
    
    NSString *urlAddress =urlString;//<null>
    
    if ([urlAddress isEqualToString:@"<null>"])
    {
        urlAddress=@"";
    }
    
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dictBody options:0 error:&err];
    NSString * contentToEncode = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    //NSLog(@"contentToEncode %@",contentToEncode);
    
    contentToEncode = [contentToEncode stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"contentToEncode %@",contentToEncode);
    
    NSString *contentEncoding = @"application/vnd.umang.web+json; charset=UTF-8";
    
    postURI=[NSURL URLWithString:urlAddress];
    
    NSString *verb = @"GET";
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"EEE, d MMM yyyy HH:mm:ss z"];
    NSString *currentDate = [dateFormatter stringFromDate: [NSDate date]]; //Put whatever date you want to convert
    NSString *currentDatetrim = [currentDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString *USERNAME=@"UM4NG";
    NSString *contentMd5=[NSString stringWithFormat:@"%@",[contentToEncode MD5]];
    
    NSString *contentMd5trim = [contentMd5 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString *postURItrim=[postURI.path stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *toSign =[NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@",verb,contentMd5trim,contentEncoding,currentDatetrim,postURItrim];
    NSString *hmac=[self hmacsha1:toSign secret:SaltSHA1MAC];
    //NSLog(@"hmac %@",hmac);
    NSString *hmactrim=[hmac stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *XappAuth=[NSString stringWithFormat:@"%@:%@",USERNAME,hmactrim];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:postURI];
    self.webView.delegate=self;
    [request addValue:XappAuth forHTTPHeaderField:@"X-App-Authorization"];
    [request addValue:currentDate forHTTPHeaderField:@"X-App-Date"];
    [request addValue:contentMd5trim forHTTPHeaderField:@"X-App-Content"];
    [request addValue:contentToEncode forHTTPHeaderField:@"X-App-Data"];
    [request addValue:contentEncoding forHTTPHeaderField:@"Content-Type"];
    
    
    [self.webView loadRequest: request];
    
    
}

//-------- WEB CACHE CODE START------------

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [activityIndicator startAnimating];
    
    [self updateButtons];
    
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    
    NSLog(@"error=%@",error);
    if([error code] == NSURLErrorCancelled)
        return;
    /*  if (error) {
     [self.webView loadData:singleton.cachedata MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:postURI];
     }*/
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [activityIndicator stopAnimating];
    
    [self updateButtons];
    
}

#pragma mark - Updating the UI



-(NSString *)JSONString:(NSString *)aString {
    NSMutableString *s = [NSMutableString stringWithString:aString];
    [s replaceOccurrencesOfString:@"\"" withString:@"\\\"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"/" withString:@"\\/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\n" withString:@"\\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\b" withString:@"\\b" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\f" withString:@"\\f" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\r" withString:@"\\r" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\t" withString:@"\\t" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    return [NSString stringWithString:s];
}


//--------------------------------------------------------------
//-----------------END JAVASCRIPT METHODS-----------------------
//--------------------------------------------------------------




- (void)updateButtons
{
    self.web_back_btn.enabled = self.webView.canGoBack;
    
    
    NSURL * currentURL = _webView.request.URL.absoluteURL;
    
    NSLog(@"currentURL = %@",currentURL);
    
    
    /*
     NSLog(@"mainDocumentURL = %@",mainDocumentURL);
     
     
     if ([mainDocumentURL hasSuffix:@"#/"])
     {
     
     
     }
     */
    
    
    if ([self.webView canGoBack])
    {
        if(flag_homebtn==TRUE)
        {
            [btnBack setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
            //flag_homebtn=FALSE;
        }
        else
        {
            [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
            
            NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
            if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
            {
                
                // [btnBack sizeToFit];
                
                CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                               @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
                
                //or whatever font you're using
                CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
                
                
                [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
                btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
                
            }
        }
        
    }
    else
    {
        [btnBack setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
        
        
        
    }
    
    
    
}


//-------- WEB CACHE CODE START------------

-(BOOL)webView:(UIWebView *)mainWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSLog(@"request: %@", request.URL.relativePath);
    NSString *url = request.URL.absoluteString;
    NSLog(@"url: %@", url);
    
    NSString *requestString = [[request URL] absoluteString];
    NSLog(@"requestString -->%@",requestString);
    
    flag_homebtn=FALSE;
    
    // if we are selecting start or end time...
    if ([url rangeOfString:@"fetch&ver=1.1&parent"].location != NSNotFound)
    {
        // 3. time has been selected - close the pop-up window
        if ([url rangeOfString:@"back"].location == 0)
        {
            [wvPopUp removeFromSuperview];
            wvPopUp = nil;
            return NO;
        }
        
        // 2. we're loading it and have already created it, etc. so just let it load
        if (wvPopUp)
            return YES;
        
        // 1. we have a 'popup' request - create the new view and display it
        UIWebView *wv = [self popUpWebview];
        [wv loadRequest:request];
        return NO;
    }
    
    if ([[[request URL] absoluteString] hasSuffix:@"#/"])
    {
        
        // mainDocumentURL = [[request URL]absoluteString];
        NSURL * currentURL = _webView.request.URL.absoluteURL;
        mainDocumentURL = [NSString stringWithFormat:@"%@",currentURL];
        
        
        NSLog(@"mainDocumentURL = %@",mainDocumentURL);
        
        flag_homebtn=TRUE;
        [self updateButtons];
        return NO;
        //return YES;
        
        
    }
    
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::traceUrl:"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *data = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_JsonArray = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:data options:0 error:nil],nil];
        
        
        
        /*
         {
         next = "https://stgweb.umang.gov.in/diglock/digilocker.jsp#/digLogin";
         previous = "https://stgweb.umang.gov.in/diglock/digilocker.jsp#/chooseOption";
         }
         
         {
         next = "https://stgweb.umang.gov.in/diglock/digilocker.jsp#/chooseOption";
         previous = "https://stgweb.umang.gov.in/diglock/digilocker.jsp#/digLogin";
         }
         
         
         */
        
        if ([self.webView canGoBack])
        {
            // [webView goBack];
            // [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
            
            @try {
                nextUrl=[NSString stringWithFormat:@"%@",[[arr_JsonArray valueForKey:@"next"] objectAtIndex:0]];
                previousURL=[NSString stringWithFormat:@"%@",[[arr_JsonArray valueForKey:@"previous"] objectAtIndex:0]];
                
                
                
                
                /* if ([previousURL hasSuffix:@"https://web.umang.gov.in/diglock/digilocker.jsp#/chooseOption"]&&[nextUrl hasSuffix:@"https://web.umang.gov.in/diglock/digilocker.jsp#/digLogin"]
                 )*/
                if ([previousURL hasSuffix:@"https://stgweb.umang.gov.in/diglock/digilocker.jsp#/chooseOption"]&&[nextUrl hasSuffix:@"https://stgweb.umang.gov.in/diglock/digilocker.jsp#/digLogin"]
                    )
                {
                    
                    [btnBack setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
                    
                    
                    [self backbtnAction:self];
                }
                
                /*else if ([nextUrl hasSuffix:@"https://web.umang.gov.in/diglock/digilocker.jsp#/chooseOption"]&&([previousURL hasSuffix:@"https://web.umang.gov.in/diglock/digilocker.jsp#chooseOption"]||[previousURL hasSuffix:@"https://web.umang.gov.in/diglock/digilocker.jsp#/chooseOption"]))*/
                else if ([nextUrl hasSuffix:@"https://stgweb.umang.gov.in/diglock/digilocker.jsp#/chooseOption"]&&([previousURL hasSuffix:@"https://stgweb.umang.gov.in/diglock/digilocker.jsp#chooseOption"]||[previousURL hasSuffix:@"https://stgweb.umang.gov.in/diglock/digilocker.jsp#/chooseOption"]))
                {
                    
                    
                    [btnBack setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
                    
                    
                    // [self backbtnAction:self];
                    
                    // [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
                    
                }
                
                
                
                /*  else if ([nextUrl hasSuffix:@"#/"])
                 {
                 
                 [btnBack setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
                 
                 }*/
                else
                {
                    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
                    
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            
            
        }
        else
        {
            [btnBack setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
            
            
        }
        
        
        
        return NO;
        
    }
    
    //   //window.location="iOS:downloadDocuments::"+{"Status":"Yes"};
    if ([url hasPrefix:@"ios::downloadDocuments::"]||[url hasPrefix:@"ios:downloadDocuments::"]||[url hasPrefix:@"iOS::downloadDocuments::"])
    {
        
        
        // [self openGallary];
        
        NSError *error;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Digilocker"];
        
        
        NSLog(@"%@",[self listFileAtPath:dataPath]);
        
        NSArray *myArray=[[self listFileAtPath:dataPath] mutableCopy];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:myArray options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSString * digidownString=[self JSONString:jsonString];
        
        NSString *javascriptString = [NSString stringWithFormat:@"returnlistDownloadDocuments('%@');",digidownString];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
        return NO;
    }
    
    
    
    
    
    if ([url hasPrefix:@"ios::downloadFile::"])
    {
        NSArray *components = [url componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        
        //  /" "
        //  argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        
        
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"\"/\"" withString:@""];
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"///" withString:@""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"\"" withString:@""];
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"/\"" withString:@""];
        
        
        NSData *data = [argsAsString
                        dataUsingEncoding:NSUTF8StringEncoding];
        id result = [NSJSONSerialization JSONObjectWithData:data
                                                    options:NSJSONReadingAllowFragments
                                                      error:NULL];
        
        NSLog(@"result = %@", result);
        
        
        // url = "https:dept.umang.gov.indigiLockerApiws1pulltest";
        //https://dept.umang.gov.in/digiLockerApi/ws1/pulltest
        @try {
            NSString *url=[result valueForKey:@"url"];
            // url=[url stringByReplacingOccurrencesOfString: @"https" withString:@"https://","];
            NSString *data=[result valueForKey:@"data"];
            
            [self downloadFile:url withjsonString:data];
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        return NO;
    }
    
    
    
    
    
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openChooseFrom::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            NSString *requestFor=[json valueForKey:@"requestFor"];
            NSString *success=[json valueForKey:@"success"];
            NSString *failure=[json valueForKey:@"failure"];
            
            
            
            
            
            [self openChooseFrom:requestFor withsuccessCallBack:success withfailureCallback:failure];
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    //window.location="iOS::getAuthToken::"+{"token":"PMKVY"};
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getAuthToken"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        
        
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *jsondata = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_Json = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:jsondata options:0 error:nil],nil];
        
        @try {
            authTokenKey=[[arr_Json objectAtIndex:0] valueForKey:@"token"];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        return NO;
        
        
    }
    
    
    //window.location="iOS::getAuthToken::"+{"token":"PMKVY"};
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::setAuthToken"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *javascriptString = [NSString stringWithFormat:@"setAuthTokenCallBack('%@');", authTokenKey];
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        return NO;
    }
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::downloadFileBase64"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        
        // data:application/pdf;base64,
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *jsondata = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_Json = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:jsondata options:0 error:nil],nil];
        
        @try {
            NSString* base64String=[[arr_Json objectAtIndex:0] valueForKey:@"base64"];
            
            
            /*
             ios::downloadFileBase64::{"base64":"","filetype":"","file_name":""}
             */
            
            NSString* filetype=[[arr_Json objectAtIndex:0] valueForKey:@"filetype"];
            NSString* file_name=[[arr_Json objectAtIndex:0] valueForKey:@"file_name"];
            
            NSData *base64data = [[NSData alloc] initWithData:[NSData dataWithBase64EncodedString:base64String]];
            
            [self openPDFfile:base64data withfilename:file_name withType:filetype];
            
            // [self openPDFfile:base64data];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        return NO;
    }
    
    //iOS::didselectafile::{"filetype":"","file_name":""}
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::didselectafile"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        
        // data:application/pdf;base64,
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *jsondata = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_Json = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:jsondata options:0 error:nil],nil];
        
        @try {
            
            NSString* filetype=[[arr_Json objectAtIndex:0] valueForKey:@"filetype"];
            NSString* file_name=[[arr_Json objectAtIndex:0] valueForKey:@"file_name"];
            
            //  [self openPDFfile:base64data withfilename:file_name withType:filetype];
            
            [self openDidSelectfile:file_name withType:filetype];
            
            // [self openPDFfile:base64data];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        return NO;
    }
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::downloadFileBytes"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        
        
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *jsondata = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_Json = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:jsondata options:0 error:nil],nil];
        
        @try {
            NSString* base64String=[[arr_Json objectAtIndex:0] valueForKey:@"base64"];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        return NO;
        
    }
    
    
    
    
    
    
    return YES;
}




-(NSMutableArray *)listFileAtPath:(NSString *)path
{
    //-----> LIST ALL FILES <-----//
    NSLog(@"LISTING ALL FILES FOUND");
    
    int count;
    
    
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
    
    NSMutableArray *digiDownInfo=[NSMutableArray new];
    
    for (count = 0; count < (int)[directoryContent count]; count++)
    {
        
        NSString *filename = [NSString stringWithFormat:@"%@",[directoryContent objectAtIndex:count]];
        
        NSString *filepath =  [NSString stringWithFormat:@"%@",[path stringByAppendingPathComponent:[directoryContent objectAtIndex:count]]];
        NSString *mimeType =  [NSString stringWithFormat:@"%@",[self fileMIMEType:filepath]];
        
        NSLog(@"%@ filepath=%@ mimeType=%@", [directoryContent objectAtIndex:count],filepath,mimeType);
        
        NSMutableDictionary *digifileDic=[NSMutableDictionary new];
        if ([filename length]==0) {
            filename=@"";
        }
        
        if ([filepath length]==0) {
            filepath=@"";
        }
        if ([mimeType length]==0) {
            mimeType=@"";
        }
        [digifileDic setValue:filename forKey:@"filename"];
        [digifileDic setValue:filepath forKey:@"filepath"];
        [digifileDic setValue:mimeType forKey:@"mimeType"];
        
        [digiDownInfo addObject:digifileDic];
        
        
        
    }
    return digiDownInfo;
    //return directoryContent;
}



- (NSString*) fileMIMEType:(NSString*) file
{
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[file pathExtension], NULL);
    CFStringRef MIMEType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    return (__bridge NSString *)MIMEType ;
}




-(void)openDidSelectfile:(NSString*)filename withType:(NSString*)type

{
    
    //NSLog(@"OpenWebViewFeedback");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.titleOpen=@"PDF View";
    vc.urltoOpen=@"";
    vc.isfrom=@"WEBVIEWHANDLEPDFOPEN";
    vc.file_name=filename;
    vc.file_type=type;
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    // [self performSelector:@selector(openPDFfile:) withObject:pdfDocumentData afterDelay:1];
    
    
}







-(void)openPDFfile:(NSData*)pdfDocumentData withfilename:(NSString*)name withType:(NSString*)type

{
    
    //NSLog(@"OpenWebViewFeedback");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.pdfData=pdfDocumentData;
    vc.titleOpen=@"PDF View";
    vc.urltoOpen=@"";
    vc.isfrom=@"WEBVIEWHANDLEPDF";
    vc.file_name=name;
    vc.file_type=type;
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    // [self performSelector:@selector(openPDFfile:) withObject:pdfDocumentData afterDelay:1];
    
    
}



- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    
    // Unsure why WKWebView calls this controller - instead of it's own parent controller
    if (self.presentedViewController) {
        [self.presentedViewController presentViewController:viewControllerToPresent animated:flag completion:completion];
    } else {
        [super presentViewController:viewControllerToPresent animated:flag completion:completion];
    }
}


-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if (self.presentedViewController)
    {
        [super dismissViewControllerAnimated:flag completion:completion];
    }
    else if(flagBack==TRUE)
    {
        [super dismissViewControllerAnimated:flag completion:completion];
        
    }
}





-(void)openChooseFrom :(NSString*)requestFor withsuccessCallBack:(NSString*)successCallBack withfailureCallback:(NSString*)failureCallback
{
    //NSLog(@"openChooseFrom");
    
    
    camera_failCallback=failureCallback;
    camera_successCallback=successCallBack;
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@""
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"camera", nil),NSLocalizedString(@"gallery", nil), nil];
    
    actionSheet.delegate=self;
    [actionSheet showInView:self.view];
    
    
    
    
}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    switch (buttonIndex) {
        case 0:
            [self openCamera];
            break;
        case 1:
            [self openGallary];
            break;
            
        default:
            break;
    }
}




-(void)openCamera
{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            
            
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
                
                [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
                
                
                
                
                
            } else {
                
                [self presentViewController:picker animated:NO completion:nil];
                
            }
            
        });
        
        
        
        
        
        
    }
}

-(void)openGallary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            
            
        }
        else
        {
            [self presentViewController:picker animated:NO completion:nil];
        }
        
    });
    
    
}




- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    // self.imageView.image = chosenImage;
    // dispatch_async(dispatch_get_main_queue(), ^
    //              {
    
    //this works, image is displayed
    UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerEditedImage];
    //this doesn't work, image is nil
    // UIImage *afterCrop = [info objectForKey:UIImagePickerControllerEditedImage];
    //self.imgView_user.image=image;
    
    CGSize destinationSize = CGSizeMake(100, 100);
    UIGraphicsBeginImageContext(destinationSize);
    [beforeCrop drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    camera_img_base64 = [self encodeToBase64String:newImage];
    
    
    
    
    
    
    NSString *returnString=[NSString stringWithFormat:@"data:image/png;base64,%@",camera_img_base64];
    
    
    NSString *javascriptString = [NSString stringWithFormat:@"setTimeout(%@('%@'),0);",camera_successCallback,returnString];
    
    NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    
    NSLog(@"newString  =%@",newString);
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
    
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
//    NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",camera_failCallback,@"Action canceled"];
//    // [self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
//    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImageJPEGRepresentation(image,100) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    // return [UIImageJPEGRepresentation(image, <#CGFloat compressionQuality#>)]
}



-(void)downloadFile:(NSString*)fileurl withjsonString:(NSString*)jsonString
{
    //NSLog(@"downloadFile");
    // startDigiLockerDownload();
    
    
    
    NSString *uri=[jsonString valueForKey:@"uri"];
    NSString *source=[jsonString valueForKey:@"source"];
    NSString *txn=[jsonString valueForKey:@"txn"];
    NSString *sharedTill=[jsonString valueForKey:@"sharedTill"];
    NSString *filename=[jsonString valueForKey:@"filename"];
    NSString *contentType=[jsonString valueForKey:@"contentType"];
    NSString *docType=[jsonString valueForKey:@"docType"];
    NSString *docId=[jsonString valueForKey:@"docId"];
    
    NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                               @"cache-control": @"no-cache"};
    
    NSString * params =[NSString stringWithFormat:@"uri=%@&source=%@&txn=%@&sharedTill=%@&filename=%@&contentType=%@&docType=%@&docId=%@&trkr=testing",uri,source,txn,sharedTill,filename,contentType,docType,docId];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fileurl]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        if([contentType isEqualToString:@"image/jpg"])
                                                        {
                                                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                                
                                                                // get the data here
                                                                
                                                                UIImage *image = [UIImage imageWithData:data];
                                                                
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                                    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                                                                    
                                                                    [self showToast:@"Your Document Save to Photo Library Successfully!"];
                                                                });
                                                            });
                                                            
                                                            
                                                        }
                                                        
                                                        else
                                                        {
                                                            [self saveDataToPDF:data];
                                                        }
                                                        
                                                        
                                                    }
                                                }];
    [dataTask resume];
    
}


- (void)saveDataToPDF:(NSData *)pdfDocumentData
{
    //Create the pdf document reference
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)pdfDocumentData);
    CGPDFDocumentRef document = CGPDFDocumentCreateWithProvider(dataProvider);
    
    //Create the pdf context
    CGPDFPageRef page = CGPDFDocumentGetPage(document, 1); //Pages are numbered starting at 1
    CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
    CFMutableDataRef mutableData = CFDataCreateMutable(NULL, 0);
    
    ////NSLog(@"w:%2.2f, h:%2.2f",pageRect.size.width, pageRect.size.height);
    CGDataConsumerRef dataConsumer = CGDataConsumerCreateWithCFData(mutableData);
    CGContextRef pdfContext = CGPDFContextCreate(dataConsumer, &pageRect, NULL);
    
    
    if (CGPDFDocumentGetNumberOfPages(document) > 0)
    {
        //Draw the page onto the new context
        //page = CGPDFDocumentGetPage(document, 1); //Pages are numbered starting at 1
        
        CGPDFContextBeginPage(pdfContext, NULL);
        CGContextDrawPDFPage(pdfContext, page);
        CGPDFContextEndPage(pdfContext);
    }
    else
    {
        //NSLog(@"Failed to create the document");
    }
    
    CGContextRelease(pdfContext); //Release before writing data to disk.
    
    //Write to disk
    // [(__bridge NSData *)mutableData writeToFile:@"/Users/David/Desktop/test.pdf" atomically:YES];
    
    NSString *pdfPath =[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"downloadfile.pdf"];
    [(__bridge NSData *)mutableData writeToFile:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"downloadfile.pdf"] atomically:YES];
    //Clean up
    CGDataProviderRelease(dataProvider); //Release the data provider
    CGDataConsumerRelease(dataConsumer);
    CGPDFDocumentRelease(document);
    CFRelease(mutableData);
    // [self performSelector:@selector(openPDFfile:) withObject:pdfDocumentData afterDelay:1];
    [self savePdfAsImage:pdfPath];
}

-(void)openPDFfile:(NSData*)pdfDocumentData

{
    
    //NSLog(@"OpenWebViewFeedback");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.pdfData=pdfDocumentData;
    vc.titleOpen=@"PDF View";
    vc.urltoOpen=@"";
    vc.isfrom=@"WEBVIEWHANDLEPDF";
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    // [self performSelector:@selector(openPDFfile:) withObject:pdfDocumentData afterDelay:1];
    
    
}
//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)savePdfAsImage:(NSString*)pdfPath
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // get the data here
        
        
        NSURL* url = [NSURL fileURLWithPath: pdfPath];
        
        CGPDFDocumentRef document = CGPDFDocumentCreateWithURL ((CFURLRef) url);
        
        UIGraphicsBeginImageContext(CGSizeMake(596,842));
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        
        CGContextTranslateCTM(currentContext, 0, 842);
        CGContextScaleCTM(currentContext, 1.0, -1.0); // make sure the page is the right way up
        
        CGPDFPageRef page = CGPDFDocumentGetPage (document, 1); // first page of PDF is page 1 (not zero)
        CGContextDrawPDFPage (currentContext, page);  // draws the page in the graphics context
        
        UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil); // do error
            
            [self showToast:@"Your Document Save to Photo Library Successfully!"];
        });
    });
    
    
    
    
    
    //NSString* imagePath = [localDocuments stringByAppendingPathComponent: @"test.png"];
    // [UIImagePNGRepresentation(image) writeToFile: imagePath atomically:YES];
    
    
}




-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
    [self removePopUp];
}

-(void)removePopUp
{
    [wvPopUp removeFromSuperview];
    wvPopUp = nil;
    
}

-(UIWebView*)popUpWebview
{
    
    UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 60, fDeviceWidth, fDeviceHeight-60)];
    
    webView.scalesPageToFit=YES;
    webView.delegate=self;
    wvPopUp=webView;
    [self.view addSubview:webView];
    return webView;
}


-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (wvPopUp)
    {
        NSError *error=nil;
        NSString *jsFromFile=[NSString stringWithContentsOfURL:[[NSBundle mainBundle]URLForResource:@"JS2" withExtension:@"txt"] encoding:NSUTF8StringEncoding error:&error];
        
        __unused NSString* jsOverrides=[webView stringByEvaluatingJavaScriptFromString:jsFromFile];
        JSContext *openerContext = [self.webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
        
        
        
        JSContext *popupContext=[webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
        popupContext[@"window"][@"opener"]=openerContext[@"window"];
        
    }
    
    
    if(flag_chat==TRUE)
    {
        //[self performSelector:@selector(openchat) withObject:nil afterDelay:3];
        flag_chat=FALSE;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [activityIndicator stopAnimating];
    
    
    [self updateButtons];
    
}
/*
 -(BOOL)webView:(UIWebView *)mainWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
 
 NSString *requestString = [[request URL] absoluteString];
 NSLog(@"requestString -->%@",requestString);
 
 if ([[[request URL] absoluteString] hasPrefix:@"ios::traceUrl:"])
 {
 NSArray *components = [requestString componentsSeparatedByString:@"::"];
 NSString *functionName = (NSString*)[components objectAtIndex:1];
 //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
 NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
 return NO;
 }
 return YES;
 }
 
 */
/*- (void)webViewDidStartLoad:(UIWebView *)webView {
 
 //   hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 
 }
 
 
 - (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
 //  [hud hideAnimated:YES];
 
 
 
 
 }
 
 
 - (void)    DidFinishLoad:(UIWebView *)webView {
 
 // [hud hideAnimated:YES];
 
 
 }
 */


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    
    //  [self loadHtml];
    
    // [self setNeedsStatusBarAppearanceUpdate];
    //  [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:.1];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gobackWebview)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    // [self.navigationController.view addGestureRecognizer:gestureRecognizer];
    [self.webView addGestureRecognizer:gestureRecognizer];
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}



-(void)gobackWebview
{
    [self.webView goBack];
    
}

-(IBAction)backbtnAction:(id)sender
{
    // flagBack=TRUE;
    // self.webView = nil;
    
    if ([btnBack.titleLabel.text isEqualToString:NSLocalizedString(@"back", nil)])
    {
        [_webView goBack];
        
    }
    
    else if ([btnBack.titleLabel.text isEqualToString:NSLocalizedString(@"home_small", nil)])
    {
        
        
        
        flagBack=TRUE;
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
    {
        flagBack=TRUE;
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
