//
//  UserProfileVC.m
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "UserProfileVC.h"
#import "ProfileCell.h"
#import "UserEditVC.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "StateList.h"

#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface UserProfileVC ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>
{
    NSMutableArray *arrGeneralInformation;
    NSMutableArray *arrAccountInformation;
    __weak IBOutlet UIButton *btnEdit;
    UIView *vwHeader;
    
    SharedManager *singleton;
    MBProgressHUD *hud ;
    
    __weak IBOutlet UILabel *lblUmangHeader;
    
    //----- local values  to be save and show from API-----------
    NSString *str_name;
    NSString *str_gender;
    NSString *str_dob;
    NSString *str_qualification;
    NSString *str_occupation;
    NSString *str_state;
    NSString *str_district;
    NSString *str_registerMb;
    NSString *str_emailAddress;
    NSString *str_alternateMb;
    NSString *str_emailVerifyStatus;
    NSString *str_amnosVerifyStatus;
    NSString *str_address;
    
    __weak IBOutlet UIButton *btnBack;
    NSString *str_Url_pic;
    
    NSMutableArray *socialpd;
    
    
    
    
    //-----------------------------------------
    
    
    
    NSMutableArray *arrDataGeneralInformation;
    NSMutableArray *arrDataAccountInformation;
    
    UIImageView *imageUserProfile;
    
    StateList *obj;
    
    NSString *stringGender;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tblUserProfile;
@property (weak, nonatomic) IBOutlet UIView *vwTableBG;


@property (retain, nonatomic)NSString *goimg;

@property (retain, nonatomic)NSString *fbid;

@property (retain, nonatomic)NSString *facebookId;

@property (retain, nonatomic)NSString *twitimg;


@end

@implementation UserProfileVC
@synthesize  goimg;

@synthesize  fbid;

@synthesize  facebookId;

@synthesize  twitimg;

-(void)fillDataInString
{
    
    SharedManager *sharedObject = [SharedManager sharedSingleton];
    str_name = sharedObject.objUserProfile.objAadhar.name;
    str_gender = sharedObject.objUserProfile.objAadhar.gender;
    str_dob = sharedObject.objUserProfile.objAadhar.dob;
    
    str_state= sharedObject.objUserProfile.objAadhar.state;
    str_district = sharedObject.objUserProfile.objAadhar.district;
    str_address = sharedObject.objUserProfile.objAadhar.street;
    /*user_email =
     user_altermob =
     imageUserProfile =*/
    
    
}



- (IBAction)btneditAction:(id)sender
{
    @try {
        
        
        
        if([str_name length]==0)
        {
            str_name=@"";
        }
        
        if([str_gender length]==0)
        {
            str_gender=@"";
        }
        
        if([str_dob length]==0)
        {
            str_dob=@"";
        }
        
        if([str_qualification length]==0)
        {
            str_qualification=@"";
        }
        
        if([str_occupation length]==0)
        {
            str_occupation=@"";
        }
        
        if([str_state length]==0)
        {
            str_state=@"";
        }
        
        if([str_district length]==0)
        {
            str_district=@"";
        }
        
        
        if([str_registerMb length]==0)
        {
            str_registerMb=@"";
        }
        
        if([str_emailAddress length]==0)
        {
            str_emailAddress=@"";
        }
        
        if([str_alternateMb length]==0)
        {
            str_alternateMb=@"";
        }
        
        if([str_emailVerifyStatus length]==0)
        {
            str_emailVerifyStatus=@"";
        }
        
        if([str_amnosVerifyStatus length]==0)
        {
            str_amnosVerifyStatus=@"";
        }
        if([str_address length]==0)
        {
            str_address=@"";
        }
        if([str_Url_pic length]==0)
        {
            str_Url_pic=@"";
        }
        
        /* if([socialpd count]==0)
         {
         socialpd=@"";
         }*/
        
        if ([socialpd count]>0)
        {
            if ([socialpd count]>0)
            {
                
                goimg=[socialpd valueForKey:@"goimg"];
                twitimg=[socialpd valueForKey:@"twitimg"];
                fbid=[socialpd valueForKey:@"fbid"];
                
                facebookId=@"";
                //facebookId=[socialpd valueForKey:@"fbid"];
                if (![fbid isEqualToString:@""])
                {
                    facebookId=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookId];
                }
                
                
                
                
                
            }
        }
        
        //singleton.notiTypeGenderSelected
        singleton.profileNameSelected =str_name;
        singleton.profilestateSelected=str_state;
        singleton.notiTypeCitySelected=@"";
        singleton.notiTypDistricteSelected=str_district;
        singleton.profileDOBSelected=str_dob;
        singleton.altermobileNumber=str_alternateMb;
        singleton.user_Qualification=str_qualification;
        singleton.user_Occupation=str_occupation;
        singleton.profileEmailSelected=str_emailAddress;
        
        
        
        NSMutableDictionary *userInfo=[NSMutableDictionary new];
        [userInfo setObject:str_name forKey:@"str_name"];
        [userInfo setObject:str_gender forKey:@"str_gender"];
        [userInfo setObject:str_dob forKey:@"str_dob"];
        [userInfo setObject:str_qualification forKey:@"str_qualification"];
        [userInfo setObject:str_occupation forKey:@"str_occupation"];
        [userInfo setObject:str_state forKey:@"str_state"];
        [userInfo setObject:str_district forKey:@"str_district"];
        [userInfo setObject:str_registerMb forKey:@"str_registerMb"];
        [userInfo setObject:str_emailAddress forKey:@"str_emailAddress"];
        [userInfo setObject:str_alternateMb forKey:@"str_alternateMb"];
        [userInfo setObject:str_emailVerifyStatus forKey:@"str_emailVerifyStatus"];
        [userInfo setObject:str_address forKey:@"str_address"];
        [userInfo setObject:str_amnosVerifyStatus forKey:@"str_amnosVerifyStatus"];
        [userInfo setObject:str_Url_pic forKey:@"str_Url_pic"];
        
        
        if([goimg length]==0)
        {
            goimg=@"";
        }
        if([twitimg length]==0)
        {
            twitimg=@"";
        }
        if([facebookId length]==0)
        {
            facebookId=@"";
        }
        
        
        
        
        [userInfo setObject:goimg forKey:@"goimg"];
        [userInfo setObject:twitimg forKey:@"twitimg"];
        [userInfo setObject:facebookId forKey:@"facebookId"];
        
        
        
        
        
        NSLog(@"str_Url_pic  ====> %@",str_Url_pic);
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    /*
     EditUserInfoVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditUserInfoVC"];
     vc.dic_info=userInfo;
     vc.tagFrom=@"ISFROMPROFILEUPDATE";
     //vc.userInfo=[]
     vc.hidesBottomBarWhenPushed = YES;
     [self.navigationController pushViewController:vc animated:YES];*/
    
}


- (IBAction)btnBackAction:(id)sender {
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
    
}



-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(-10.0, 1.0);
    vwItem.layer.shadowColor = [UIColor grayColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}
- (IBAction)btnEditClicked:(id)sender
{
    @try {
        
        
        
        if([str_name length]==0)
        {
            str_name=@"";
        }
        
        if([str_gender length]==0)
        {
            str_gender=@"";
        }
        
        if([str_dob length]==0)
        {
            str_dob=@"";
        }
        
        if([str_qualification length]==0)
        {
            str_qualification=@"";
        }
        
        if([str_occupation length]==0)
        {
            str_occupation=@"";
        }
        
        if([str_state length]==0)
        {
            str_state=@"";
        }
        
        if([str_district length]==0)
        {
            str_district=@"";
        }
        
        
        if([str_registerMb length]==0)
        {
            str_registerMb=@"";
        }
        
        if([str_emailAddress length]==0)
        {
            str_emailAddress=@"";
        }
        
        if([str_alternateMb length]==0)
        {
            str_alternateMb=@"";
        }
        
        if([str_emailVerifyStatus length]==0)
        {
            str_emailVerifyStatus=@"";
        }
        
        if([str_amnosVerifyStatus length]==0)
        {
            str_amnosVerifyStatus=@"";
        }
        if([str_address length]==0)
        {
            str_address=@"";
        }
        if([str_Url_pic length]==0)
        {
            str_Url_pic=@"";
        }
        if ([socialpd count]>0)
        {
            
            goimg=[socialpd valueForKey:@"goimg"];
            twitimg=[socialpd valueForKey:@"twitimg"];
            fbid=[socialpd valueForKey:@"fbid"];
            
            facebookId=@"";
            //facebookId=[socialpd valueForKey:@"fbid"];
            if (![fbid isEqualToString:@""])
            {
                facebookId=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",fbid];
            }
            
            
            
            
            
            
        }
        
        
        
        
        //singleton.notiTypeGenderSelected
        singleton.profileNameSelected =str_name;
        singleton.profilestateSelected=str_state;
        singleton.notiTypeCitySelected=@"";
        singleton.notiTypDistricteSelected=str_district;
        singleton.profileDOBSelected=str_dob;
        singleton.altermobileNumber=str_alternateMb;
        singleton.user_Qualification=str_qualification;
        singleton.user_Occupation=str_occupation;
        singleton.profileEmailSelected=str_emailAddress;
        
        
        
        NSMutableDictionary *userInfo=[NSMutableDictionary new];
        [userInfo setObject:str_name forKey:@"str_name"];
        [userInfo setObject:str_gender forKey:@"str_gender"];
        [userInfo setObject:str_dob forKey:@"str_dob"];
        [userInfo setObject:str_qualification forKey:@"str_qualification"];
        [userInfo setObject:str_occupation forKey:@"str_occupation"];
        [userInfo setObject:str_state forKey:@"str_state"];
        [userInfo setObject:str_district forKey:@"str_district"];
        [userInfo setObject:str_registerMb forKey:@"str_registerMb"];
        [userInfo setObject:str_emailAddress forKey:@"str_emailAddress"];
        [userInfo setObject:str_alternateMb forKey:@"str_alternateMb"];
        [userInfo setObject:str_emailVerifyStatus forKey:@"str_emailVerifyStatus"];
        [userInfo setObject:str_address forKey:@"str_address"];
        [userInfo setObject:str_amnosVerifyStatus forKey:@"str_amnosVerifyStatus"];
        [userInfo setObject:str_Url_pic forKey:@"str_Url_pic"];
        
        if([goimg length]==0)
        {
            goimg=@"";
        }
        if([twitimg length]==0)
        {
            twitimg=@"";
        }
        if([facebookId length]==0)
        {
            facebookId=@"";
        }
        
        
        
        
        [userInfo setObject:goimg forKey:@"goimg"];
        [userInfo setObject:twitimg forKey:@"twitimg"];
        [userInfo setObject:facebookId forKey:@"facebookId"];
        
        
        
        
        
        //[userInfo setObject:socialpd forKey:@"socialpd"];
        
        NSLog(@"str_Url_pic  ====> %@",str_Url_pic);
        
        
        // if ([str_Url_pic length]!=0 ) {
        
        singleton.user_profile_URL=str_Url_pic;
        //}
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UserEditVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserEditVC"];
        //vc.userInfo=[]
        vc.dic_info=userInfo;
        vc.tagFrom=@"ISFROMPROFILEUPDATE";
        vc.imgtopass =  UIImagePNGRepresentation(imageUserProfile.image);
        vc.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:vc animated:YES];
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}


- (void)viewDidLoad
{
    
    //Google Tracking
    /* id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
     [tracker set:kGAIScreenName value:USER_PROFILE_SCREEN];
     [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];*/
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    stringGender=@"";
    singleton = [SharedManager sharedSingleton];
    // Do any additional setup after loading the view.
    //self.navigationController.navigationBarHidden=true;
    
    
    lblUmangHeader.text = NSLocalizedString(@"general", nil);
    
    [btnEdit setTitle:NSLocalizedString(@"edit", nil) forState:UIControlStateNormal];
    
    
    
    self.navigationItem.title = @"";
    
    self.navigationController.navigationBarHidden=true;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    // Do any additional setup after loading the view.
    
    //  [self fillDataInString];
    
    
    
    
    
    arrGeneralInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"name_caps", nil),NSLocalizedString(@"gender_caps", nil),NSLocalizedString(@"date_of_birth_caps", nil),NSLocalizedString(@"qualication_caps", nil),NSLocalizedString(@"occupation_caps", nil),NSLocalizedString(@"state_txt_caps", nil),NSLocalizedString(@"district_caps", nil),NSLocalizedString(@"address_caps", nil), nil];
    
    arrAccountInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"email_address", nil),NSLocalizedString(@"alt_mob_num", nil), nil];
    [_tblUserProfile reloadData];
    [super viewDidLoad];
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


-(UIImage *)makeRoundedImage:(UIImage *) image
{
    float radius=image.size.height /2;
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration {
    _tblUserProfile.tableHeaderView = nil;
    _tblUserProfile.tableHeaderView =  [self designHeaderView];
    
}



-(UIView *)designHeaderView
{
    singleton = [SharedManager sharedSingleton];
    
    if (vwHeader) {
        [[vwHeader subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [vwHeader removeFromSuperview];
        vwHeader = nil;
    }
    
    vwHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tblUserProfile.frame.size.width, 200.0)];
    [_tblUserProfile addSubview:vwHeader];
    vwHeader.backgroundColor = [UIColor whiteColor];
    
    
    imageUserProfile =[[UIImageView alloc]initWithFrame:CGRectMake((fDeviceWidth/2)-70, 30, 140, 140)];
    // imageUserProfile.contentMode = UIViewContentModeCenter;
    //imageUserProfile.contentMode=UIViewContentModeScaleAspectFit;
    
    imageUserProfile.contentMode=UIViewContentModeScaleAspectFill;
    
    /* imageUserProfile.layer.backgroundColor=[[UIColor clearColor] CGColor];
     imageUserProfile.layer.borderColor = [UIColor whiteColor].CGColor;
     imageUserProfile.layer.borderWidth = 1.0;
     imageUserProfile.layer.cornerRadius = 70.0;
     */
    
    imageUserProfile.layer.cornerRadius = imageUserProfile.frame.size.height /2;
    imageUserProfile.layer.masksToBounds = YES;
    imageUserProfile.layer.borderWidth = 2;
    imageUserProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    
    [vwHeader addSubview: imageUserProfile];
    
    UIView  *vwHeaderLine = [[UIView alloc]initWithFrame:CGRectMake(0,vwHeader.frame.size.height-0.5, vwHeader.frame.size.width,0.5 )];
    vwHeaderLine.backgroundColor = [UIColor lightGrayColor];
    [vwHeader addSubview:vwHeaderLine];
    
    
    
    NSLog(@"str_gender=%@",str_gender);
    if ([str_gender length] >0)
    {
        
        
        UIImage *tempImg;
        if ([str_gender isEqualToString:@"M"] ||[str_gender isEqualToString:@"MALE"]) {
            
            stringGender=NSLocalizedString(@"gender_male", nil);
            tempImg=[UIImage imageNamed:@"male_avatar"];
            
        }
        else if ([str_gender isEqualToString:@"F"]||[str_gender isEqualToString:@"FEMALE"]) {
            
            stringGender=NSLocalizedString(@"gender_female", nil);
            
            tempImg=[UIImage imageNamed:@"female_avatar"];
            
        }
        else if ([str_gender isEqualToString:@"T"]||[str_gender isEqualToString:@"t"])
        {
            stringGender=NSLocalizedString(@"gender_transgender", nil);
            
            tempImg=[UIImage imageNamed:@"user_placeholder"];
            
            
        }
        else
        {
            stringGender=@"";
            
            tempImg=[UIImage imageNamed:@"user_placeholder"];
            
        }
        
        imageUserProfile.image=tempImg;
        
        // dispatch_async(dispatch_queue_create("com.getImage", NULL), ^(void) {
        NSLog(@"singleton.imageLocalpath=%@",singleton.imageLocalpath);
        
        
        
        
        UIImage *tempImg1 ;
        
        if([[NSFileManager defaultManager] fileExistsAtPath:singleton.imageLocalpath])
        {
            // ur code here
            NSLog(@"file present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            
            tempImg1 = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
            if (tempImg1==nil) {
                
                tempImg1=tempImg;
                
            }
            imageUserProfile.image=tempImg1;
        } else {
            // ur code here**
            NSLog(@"Not present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            tempImg1=tempImg;
            
        }
        
        /*  UIImage *tempImg1 ;
         if ([singleton.imageLocalpath rangeOfString:@"user_image.png"].location == NSNotFound) {
         NSLog(@"singleton.imageLocalpath does not contain bla");
         //[imageUserProfile sd_setImageWithURL:url
         //                 placeholderImage:tempImg];
         
         tempImg1=tempImg;
         
         } else {
         NSLog(@"singleton.imageLocalpath contains bla!");
         
         tempImg1 = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
         if (tempImg1==nil) {
         tempImg1=tempImg;
         
         }
         imageUserProfile.image=tempImg1;
         }
         */
        
        
        
        
        
        NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
        
        if(![[url absoluteString] isEqualToString:@""])
        {
            
            
            @try {
                [imageUserProfile sd_setImageWithURL:url
                                    placeholderImage:tempImg1];
                
                
                /*
                 NSURLRequest* request = [NSURLRequest requestWithURL:url];
                 
                 
                 [NSURLConnection sendAsynchronousRequest:request
                 queue:[NSOperationQueue mainQueue]
                 completionHandler:^(NSURLResponse * response,
                 NSData * data,
                 NSError * error) {
                 if (!error){
                 
                 
                 // dispatch_async(dispatch_queue_create("com.getImage", NULL), ^(void) {
                 
                 
                 UIImage *image=[UIImage imageWithData:data];
                 if (image==nil) {
                 
                 imageUserProfile.image=tempImg1;
                 }
                 else{
                 imageUserProfile.image=image;
                 
                 //yourImageURL is valid
                 dispatch_async(dispatch_get_main_queue(), ^{
                 
                 imageUserProfile.image=image;
                 });
                 
                 }
                 
                 
                 }
                 else
                 {
                 imageUserProfile.image = tempImg1;
                 
                 }
                 
                 }];
                 
                 */
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
        }
        
        
        
        
        // });
        
    }
    else
    {
        imageUserProfile.image=[UIImage imageNamed:@"user_placeholder"];
        NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
        
        if(![[url absoluteString] isEqualToString:@""])
        {
            [imageUserProfile sd_setImageWithURL:url
                                placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
        }
    }
    
    
    
    imageUserProfile.clipsToBounds=YES;
    
    [_tblUserProfile reloadData];
    return vwHeader;
    
    
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


#pragma mark- Font Set to View
-(void)setViewFont
{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    
    //lblHeaderTitle.font = [AppFont semiBoldFont:22.0];
    
    //[btn_save.titleLabel setFont:[AppFont regularFont:17.0]];
    
}
#pragma mark -
-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    @try {
        singleton = [SharedManager sharedSingleton];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (![self connected]) {
                // Not connected
                [_tblUserProfile reloadData];
                
            } else {
                // Connected. Do some Internet stuff
                
                
                
                
                dispatch_queue_t queue = dispatch_queue_create("com.spice.umang", NULL);
                dispatch_async(queue, ^{
                    obj=[[StateList alloc]init];
                    //NSLog(@"Device is connected to the Internet");
                    [obj hitStateQualifiAPI];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self hitFetchProfileAPI];
                        
                    });
                    
                });
                
            }
            
            
            
            
            
        });
        
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    /* UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(btnBackAction:)];
     [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
     [self.navigationController.view addGestureRecognizer:gestureRecognizer];
     */
    _tblUserProfile.tableHeaderView =  [self designHeaderView];
    
    
    /* [_tblUserProfile reloadData];
     [_tblUserProfile setNeedsLayout ];
     [_tblUserProfile layoutIfNeeded ];
     [_tblUserProfile reloadData];
     (/
     [super viewWillAppear:NO];
     }
     
     
     -(void)viewDidAppear:(BOOL)animated{
     [super viewDidAppear:animated];
     
     /* [_tblUserProfile reloadData];
     [_tblUserProfile setNeedsLayout ];
     [_tblUserProfile layoutIfNeeded ];
     [_tblUserProfile reloadData];
     */
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    [super viewWillAppear:animated];
    
    
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    return nil;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *vwHeaderTitle = [[UIView alloc]initWithFrame:CGRectMake(0, vwHeader.frame.size.height, _tblUserProfile.frame.size.width, 50.0)];
    
    
    vwHeaderTitle.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:234.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    
    
    
    UILabel *lblSectionTitle = [[UILabel alloc]initWithFrame:CGRectMake(7,28, 200, 21.0)];
    lblSectionTitle.font = [UIFont systemFontOfSize:13.0];
    lblSectionTitle.textColor = [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
    lblSectionTitle.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    CGRect labelFrame =  lblSectionTitle.frame;
    
    labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 200 : 15;
    lblSectionTitle.frame = labelFrame;
    
    
    
    
    [vwHeaderTitle addSubview:lblSectionTitle];
    // lblSectionTitle.textColor = [UIColor grayColor];
    //  lblSectionTitle.font = [UIFont systemFontOfSize:14.0];
    //  lblSectionTitle.adjustsFontSizeToFitWidth = YES;
    
    
    //for grey header Design
    
    
    
    
    if (section == 0)
    {
        // vwGray.hidden = YES;
        lblSectionTitle.text = NSLocalizedString(@"general_information", nil);
    }
    else
        
    {
        // vwGray.hidden = NO;
        lblSectionTitle.text =  NSLocalizedString(@"account_information", nil);
    }
    
    
    
    
    return vwHeaderTitle;
}


-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([arrDataGeneralInformation count]>0)
    {
        
        
        if (indexPath.section==0)
        {
            
            if(indexPath.row==7)
            {
                
                CGFloat retVal = 75.0;
                
                NSString *txtService =  [arrDataGeneralInformation objectAtIndex:indexPath.row];
                
                CGRect dynamicHeight = [self rectForText:txtService usingFont:[UIFont systemFontOfSize:17.0] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
                //retVal = MAX(75, dynamicHeight.size.height) +5;
                
                
                //return retVal;
                
                return retVal+dynamicHeight.size.height;
                
                
                
                
                
                
                //return 75.0;
                
            }else
            {
                return 75.0;
                
            }
            
        }
    }
    return 75.0;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60.0;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    
    return 0.001;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented
{
    return 2;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    if (section == 0) {
        return  arrGeneralInformation.count;
    }
    
    else{
        return arrAccountInformation.count;
    }
}




-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    
    
    if ([cell isKindOfClass:[ProfileCell class]])
    {
        ProfileCell *profileCell = (ProfileCell*)cell;
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // profileCell.lblName.font = [UIFont systemFontOfSize:16.0];
            // profileCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            // editCell.lblName.font = [UIFont systemFontOfSize:16.0];
            // editCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            NSInteger fontIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_FONTSIZE_INDEX"];
            
            CGFloat fontsize;
            if (fontIndex==0)
            {
                fontsize=14.0;
                [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
            }
            if (fontIndex==1)
            {
                fontsize=16.0;
                [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
                
            }
            if (fontIndex==2)
            {
                fontsize=18.0;
                [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
                
            }
            
            
            
            [profileCell setNeedsDisplay];
            
        });
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    /*static NSString *CellIdentifier = @"ProfileCell";
     ProfileCell *profileCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
     */
    static NSString *CellIdentifier = @"ProfileCell";
    
    ProfileCell *profileCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (profileCell == nil) {
        profileCell = [[ProfileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    
    profileCell.lblName.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    // [profileCell.lblName setFont:[UIFont systemFontOfSize:16]];
    //[profileCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
    //cell.textLabel.font = [UIFont systemFontOfSize:12.0];
    
    UILabel *lblTemp = (UILabel*)[profileCell.contentView viewWithTag:888];
    if (lblTemp) {
        [lblTemp removeFromSuperview];
        lblTemp = nil;
    }
    @try {
        
        if (indexPath.section == 0)
        {
            profileCell.lblName.text = [arrGeneralInformation objectAtIndex:indexPath.row];
            
            profileCell.txtNameFields.text= [arrDataGeneralInformation objectAtIndex:indexPath.row];
            //profileCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            
            profileCell.btn_verify.hidden=TRUE;
            profileCell.btn_verify.enabled=FALSE;
            if(indexPath.row==0||indexPath.row==1||indexPath.row==2||indexPath.row==3||indexPath.row==4||indexPath.row==5||indexPath.row==6)
            {
                profileCell.btn_verify.hidden=TRUE;
                profileCell.btn_verify.enabled=FALSE;
                
            }
            /*  if(indexPath.row==5)
             {
             [profileCell.lblName setFont:[UIFont systemFontOfSize:16]];
             [profileCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
             }
             
             if(indexPath.row==6)
             {
             [profileCell.lblName setFont:[UIFont systemFontOfSize:16]];
             [profileCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
             }*/
            
            if(indexPath.row==7)
            {
                
                //profileCell.lblName.frame=CGRectMake(15, 0, fDeviceWidth,40);
                // profileCell.lblName.backgroundColor=[UIColor grayColor];
                
                UILabel *lblAddressDesc=[[UILabel alloc]initWithFrame:CGRectMake(15, profileCell.lblName.frame.origin.y+profileCell.lblName.frame.size.height+20, fDeviceWidth-30, profileCell.frame.size.height)];
                lblAddressDesc.backgroundColor=[UIColor clearColor];
                lblAddressDesc.tag = 888;
                CGRect dynamicHeight = [self rectForText:[arrDataGeneralInformation objectAtIndex:indexPath.row] usingFont:[UIFont systemFontOfSize:14.0] boundedBySize:CGSizeMake(fDeviceWidth-40, 1000.0)];
                
                CGRect frameBG = profileCell.frame;
                frameBG.size.height = MAX(75.0, dynamicHeight.size.height) + 40;
                // frameBG.origin.y = 30;
                //cell.frame = frameBG;
                
                CGRect frametxtfield=lblAddressDesc.frame;
                lblAddressDesc.frame=CGRectMake(frametxtfield.origin.x, frametxtfield.origin.y, frametxtfield.size.width, frameBG.size.height);
                
                
                lblAddressDesc.text = [arrDataGeneralInformation objectAtIndex:indexPath.row];
                
                lblAddressDesc.textAlignment = NSTextAlignmentLeft;
                //[lblAddressDesc setFont:[UIFont systemFontOfSize:16]];
                
                [lblAddressDesc setNumberOfLines:0];
                [lblAddressDesc sizeToFit];
                
                [profileCell.contentView addSubview:lblAddressDesc];
                
                
                // profileCell.txtNameFields.clearButtonMode = UITextFieldViewModeWhileEditing;
                
                //                UIView *vw_line=[[UIView alloc]init];
                //                if ([lbl_desc.text  isEqualToString:@""]) {
                //                    vw_line.frame=CGRectMake(15,profileCell.frame.size.height-1, fDeviceWidth-15, 0.5);
                //                }
                //                else
                //                {
                //                    vw_line.frame=CGRectMake(15,lbl_desc.frame.origin.y+lbl_desc.frame.size.height-1, fDeviceWidth-15, 0.5);
                //                }
                //
                //                vw_line.backgroundColor=[UIColor lightGrayColor];
                //                [profileCell.contentView addSubview:vw_line];
                
                // profileCell.lbldesc.text=[arrDataGeneralInformation objectAtIndex:indexPath.row];
                
                
                profileCell.txtNameFields.hidden=TRUE;
                
            }
            else
            {
                profileCell.txtNameFields.hidden=FALSE;
                
            }
            profileCell.btn_verify.hidden=TRUE;
            profileCell.btn_verify.enabled=FALSE;
        }
        else
        {
            //[profileCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            //  profileCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            
            profileCell.txtNameFields.hidden=FALSE;
            
            profileCell.lblName.text = [arrAccountInformation objectAtIndex:indexPath.row];
            profileCell.txtNameFields.text= [arrDataAccountInformation objectAtIndex:indexPath.row];
            if (indexPath.row==0)
            {
                if ([str_emailVerifyStatus isEqualToString:@"0"])
                {
                    if ([str_emailAddress length]!=0)
                    {
                        
                        profileCell.btn_verify.hidden=FALSE;
                        profileCell.btn_verify.enabled=TRUE;
                        [profileCell.btn_verify setImage:[UIImage imageNamed:@"img_error_mpin"] forState:UIControlStateNormal];
                        
                    }
                    else
                    {
                        profileCell.btn_verify.hidden=TRUE;
                        profileCell.btn_verify.enabled=FALSE;
                        
                        
                    }
                }
                else  if ([str_emailVerifyStatus isEqualToString:@"1"])
                {
                    if ([str_emailAddress length]!=0)
                    {
                        
                        profileCell.btn_verify.hidden=FALSE;
                        profileCell.btn_verify.enabled=FALSE;
                        [profileCell.btn_verify setImage:[UIImage imageNamed:@"dialog_check"] forState:UIControlStateNormal];
                        [profileCell.btn_verify setBackgroundImage:[UIImage imageNamed:@"dialog_check"] forState:UIControlStateDisabled];
                        
                    }
                    
                    else
                    {
                        profileCell.btn_verify.hidden=TRUE;
                        profileCell.btn_verify.enabled=FALSE;
                        
                        
                    }
                    
                    
                }
            }
            else
            {
                profileCell.btn_verify.hidden=TRUE;
                profileCell.btn_verify.enabled=FALSE;
                [profileCell.btn_verify setImage:[UIImage imageNamed:@"dialog_check"] forState:UIControlStateNormal];
                
            }
            
        }
        
        [profileCell.btn_verify addTarget:self action:@selector(resendAction:) forControlEvents:UIControlEventTouchUpInside];
        
        profileCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    NSInteger fontIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_FONTSIZE_INDEX"];
    CGFloat fontsize;
    if (fontIndex==0)
    {
        fontsize=14.0;
        [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
    }
    if (fontIndex==1)
    {
        fontsize=16.0;
        [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
        
    }
    if (fontIndex==2)
    {
        fontsize=18.0;
        [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
        
    }
    
    
    return profileCell;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(NSString *)UpperFirstWord:(NSString*)inputString
{
    
    
    if ([inputString length]!=0) {
        inputString = [inputString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[inputString substringToIndex:1] uppercaseString]];
    }
    return inputString;
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitFetchProfileAPI
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            
            NSString *responseStr=[NSString stringWithFormat:@"%@",response];
            if ([responseStr length]>0)
            {
                
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    
                {
                    
                    @try {
                        
                        
                        //singleton.user_tkn=tkn;
                        NSMutableArray *aadharpd=[[NSMutableArray alloc]init];
                        aadharpd=[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];
                        
                        
                        NSMutableArray *generalpd=[[NSMutableArray alloc]init];
                        generalpd=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                        
                        
                        
                        socialpd=[[NSMutableArray alloc]init];
                        
                        singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                        
                        NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                        
                        if ([abbreviation length]==0) {
                            abbreviation=@"";
                        }
                        
                        NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                        
                        emblemString = emblemString.length == 0 ? @"":emblemString;
                        [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"TabKey"] isEqualToString:@"YES"])
                        {
                            if (abbreviation.length != 0)
                            {
                              //  [[self.tabBarController.tabBar.items objectAtIndex:singleton.tabSelectedIndex] setTitle:abbreviation];
                                
                                
                                
                              //  NSString * tordc = [NSString stringWithFormat:@"%@",[singleton.arr_initResponse  valueForKey:@"tord"]];
                              //  NSArray *array =[tordc componentsSeparatedByString:@"|,"];
                                NSArray *array =[NSArray arrayWithArray:singleton.AppTabOrders];

                                NSMutableArray *tempTabOrder =[NSMutableArray new];
                                NSString *home = [NSLocalizedString(@"home_small", @"") capitalizedString];
                                NSString *flagship = [NSLocalizedString(@"flagship", @"") capitalizedString];
                                NSString *fav = [NSLocalizedString(@"favourites_small", @"") capitalizedString];
                                NSString *allservices = [NSLocalizedString(@"all_services_small", @"") capitalizedString];
                                NSString *state = NSLocalizedString(@"state_txt", @"") ;
                                for (int i =0; i<[array count]; i++)
                                {
                                    NSString *tempItem = [array objectAtIndex:i];
                                    if ([tempItem containsString:@"home"])
                                    {
                                        NSLog(@"string contains home!");
                                        [tempTabOrder addObject: home];
                                    }
                                    else if ([tempItem containsString:@"HomeWithFav"]) {
                                        NSLog(@"string contains flagship!");
                                        [tempTabOrder addObject: home];
                                    }
                                    else if ([tempItem containsString:@"flagship"]) {
                                        NSLog(@"string contains flagship!");
                                        [tempTabOrder addObject: flagship];
                                    }
                                    else if ([tempItem containsString:@"fav"]) {
                                        NSLog(@"string contains fav!");
                                        [tempTabOrder addObject: fav];
                                    }
                                    else if ([tempItem containsString:@"allservices"]) {
                                        NSLog(@"string contains allservices!");
                                        [tempTabOrder addObject: allservices];
                                    }
                                    else if ([tempItem containsString:@"state"]) {
                                        NSLog(@"string contains state!");
                                        [tempTabOrder addObject: state];
                                    }
                                }
                                NSLog(@" tempTabOrder=%@",tempTabOrder);
                                if ([tempTabOrder count]>0) {
                                    NSUInteger i = [tempTabOrder indexOfObject: state];
                                   
                                     [[self.tabBarController.tabBar.items objectAtIndex:i] setTitle:abbreviation];

                                }
                                
                                
                                
                                
                            }
                        }
                        
                        
                        singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                        [singleton setStateId:singleton.user_StateId];
                        
                        //-------- Add later----------
                        singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                        singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                        //-------- Add later----------
                        
                        NSString *profileComplete=[generalpd valueForKey:@"pc"];
                        
                        if ([profileComplete length]>0)
                        {
                            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                            // Encrypt
                            [[NSUserDefaults standardUserDefaults] encryptValue:profileComplete withKey:@"PROFILE_COMPELTE_KEY"];
                            // [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        
                        
                        if ([singleton.user_id length]==0) {
                            singleton.user_id=@"";
                        }
                        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        
                        // [defaults setObject:singleton.user_id forKey:@"USER_ID"];
                        
                        // [defaults synchronize];
                        
                        
                        NSString *imageUrl=[generalpd valueForKey:@"pic"];
                        singleton.user_profile_URL=imageUrl;
                        if ([singleton.user_profile_URL length]==0)
                        {
                            singleton.user_profile_URL=@"";
                        }
                        //------------------------- Encrypt Value------------------------
                        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                        // Encrypt
                        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        //------------------------- Encrypt Value------------------------
                        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                        // Encrypt
                        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        //------------------------- Encrypt Value------------------------
                        
                        
                        
                        
                        //NSString *identifier =( [[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        socialpd=(NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        
                        
                        
                        //----- local values  to be save and show from API-----------
                        
                        
                        
                        //NSString * sid=[generalpd valueForKey:@"sid"];
                        
                        str_name=[generalpd valueForKey:@"nam"];
                        str_registerMb =[generalpd valueForKey:@"mno"];
                        str_alternateMb=[generalpd valueForKey:@"amno"];
                        //str_city=[generalpd valueForKey:@"cty"];
                        str_state=[generalpd valueForKey:@"st"];
                        
                        // state_id=[obj getStateCode:str_state];
                        
                        
                        str_district=[generalpd valueForKey:@"dist"];
                        str_dob=[generalpd valueForKey:@"dob"];
                        str_gender=[generalpd valueForKey:@"gndr"];
                        str_emailAddress=[generalpd valueForKey:@"email"];
                        str_emailVerifyStatus=[generalpd valueForKey:@"emails"];
                        str_amnosVerifyStatus=[generalpd valueForKey:@"amnos"];
                        str_Url_pic=[generalpd valueForKey:@"pic"];
                        str_address=[generalpd valueForKey:@"addr"];
                        
                        NSLog(@"str_gender=%@",str_gender);
                        
                        /* if (str_gender.length>0) {
                         _tblUserProfile.tableHeaderView =  [self designHeaderView];
                         }
                         */
                        NSString *quali_id=[generalpd valueForKey:@"qual"];
                        NSString *Occu_id=[generalpd valueForKey:@"occup"];
                        
                        
                        str_qualification=[obj getqualName:quali_id];
                        str_occupation=[obj getOccuptname:Occu_id];
                        
                        NSLog(@"str_Url_pic  =>>> %@",str_Url_pic);
                        // singleton.notiTypeGenderSelected=str_gender;
                        
                        /*
                         "gcmid": "",
                         "opid": "",
                         "refid": "",
                         "tkn": "",
                         */
                        
                        
                        [self setProfileData];
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                }
            }
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];
             */
            //  [self setProfileData];//closed
            
        }
        
    }];
    
}


- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}



-(void)setProfileData
{
    
    //----- local values  to be save and show from API-----------
    
    
    str_name=[self UpperFirstWord:str_name];
    // str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    
    
    
    
    //if ([str_Url_pic length]!=0 ) {
    
    singleton.user_profile_URL=str_Url_pic;
    //}
    
    
    
    
    
    
    if ([str_gender length]!=0)
    {
        str_gender=[str_gender uppercaseString];
        
        if ([str_gender isEqualToString:@"M"]) {
            stringGender= NSLocalizedString(@"gender_male", nil);
            singleton.notiTypeGenderSelected= NSLocalizedString(@"gender_male", nil);
            
        }
        else  if ([str_gender isEqualToString:@"F"]) {
            singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_female", nil);
            stringGender=NSLocalizedString(@"gender_female", nil);
            
        }
        else  if ([str_gender isEqualToString:@"T"]||[str_gender isEqualToString:@"t"]) {
            singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_transgender", nil);
            stringGender=NSLocalizedString(@"gender_transgender", nil);
            
        }
        
        else
        {
            singleton.notiTypeGenderSelected=@"";
            stringGender=@"";
        }
        
        
    }
    
    else
    {
        singleton.notiTypeGenderSelected=@"";
        stringGender=@"";
    }
    
    
    
    str_name=[self UpperFirstWord:str_name];
    //str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    
    
    
    //singleton.notiTypeGenderSelected
    singleton.profileNameSelected =str_name;
    singleton.profilestateSelected=str_state;
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=str_district;
    singleton.profileDOBSelected=str_dob;
    singleton.altermobileNumber=str_alternateMb;
    singleton.user_Qualification=str_qualification;
    singleton.user_Occupation=str_occupation;
    singleton.profileEmailSelected=str_emailAddress;
    singleton.profileUserAddress=str_address;
    
    
    
    
    
    
    
    arrDataGeneralInformation = [[NSMutableArray alloc]initWithObjects:singleton.profileNameSelected,stringGender,singleton.profileDOBSelected,singleton.user_Qualification,singleton.user_Occupation,singleton.profilestateSelected,singleton.notiTypDistricteSelected,singleton.profileUserAddress, nil];
    
    
    arrDataAccountInformation = [[NSMutableArray alloc]initWithObjects:singleton.profileEmailSelected,singleton.altermobileNumber, nil];
    
    
    
    
    
    
    
    NSLog(@"str_gender=%@",str_gender);
    UIImage *tempImg;
    
    NSLog(@"str_gender=%@",str_gender);
    if ([str_gender length] >0)
    {
        
        if ([str_gender isEqualToString:@"M"]) {
            
            stringGender=NSLocalizedString(@"gender_male", nil);
            tempImg=[UIImage imageNamed:@"male_avatar"];
            
        }
        else if ([str_gender isEqualToString:@"F"]) {
            
            stringGender=NSLocalizedString(@"gender_female", nil);
            
            tempImg=[UIImage imageNamed:@"female_avatar"];
            
        }
        else if ([str_gender isEqualToString:@"T"])
        {
            stringGender=NSLocalizedString(@"gender_transgender", nil);
            
            tempImg=[UIImage imageNamed:@"user_placeholder"];
            
            
        }
        else {
            stringGender=@"";
            
            tempImg=[UIImage imageNamed:@"user_placeholder"];
            
            
        }
        
        
        imageUserProfile.image=tempImg;
        
        dispatch_async(dispatch_queue_create("com.getImage", NULL), ^(void) {
            NSLog(@"singleton.imageLocalpath=%@",singleton.imageLocalpath);
            
            
            
            
            UIImage *tempImg1 ;
            
            if([[NSFileManager defaultManager] fileExistsAtPath:singleton.imageLocalpath])
            {
                // ur code here
                NSLog(@"file present singleton.imageLocalpath=%@",singleton.imageLocalpath);
                
                tempImg1 = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
                if (tempImg1==nil) {
                    
                    tempImg1=tempImg;
                    
                }
                imageUserProfile.image=tempImg1;
            } else {
                // ur code here**
                NSLog(@"Not present singleton.imageLocalpath=%@",singleton.imageLocalpath);
                tempImg1=tempImg;
                
            }
            
            
            /*  UIImage *tempImg1 ;
             if ([singleton.imageLocalpath rangeOfString:@"user_image.png"].location == NSNotFound) {
             NSLog(@"singleton.imageLocalpath does not contain bla");
             //[imageUserProfile sd_setImageWithURL:url
             //                 placeholderImage:tempImg];
             
             tempImg1=tempImg;
             
             } else {
             NSLog(@"singleton.imageLocalpath contains bla!");
             
             tempImg1 = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
             imageUserProfile.image=tempImg1;
             }
             */
            
            NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
            
            if(![[url absoluteString] isEqualToString:@""])
            {
                
                /*[imageUserProfile sd_setImageWithURL:url
                 placeholderImage:tempImg1
                 options:SDWebImageRefreshCached];*/
                
                
                
                @try {
                    /* [imageUserProfile sd_setImageWithURL:url
                     placeholderImage:tempImg1];*/
                    
                    //  [imageUserProfile sd_setImageWithURL:url
                    //                    placeholderImage:tempImg1];
                    
                    
                    NSURLRequest* request = [NSURLRequest requestWithURL:url];
                    [NSURLConnection sendAsynchronousRequest:request
                                                       queue:[NSOperationQueue mainQueue]
                                           completionHandler:^(NSURLResponse * response,
                                                               NSData * data,
                                                               NSError * error)
                     //harry@0370
                     //
                     {
                         if (!error){
                             
                             
                             dispatch_async(dispatch_queue_create("com.getImage", NULL), ^(void) {
                                 
                                 
                                 UIImage *image=[UIImage imageWithData:data];
                                 if (image==nil) {
                                     //yourImageURL is not valid
                                     // image=[UIImage imageNamed:@"placeholder.png"];
                                     
                                     imageUserProfile.image=tempImg1;
                                     [_tblUserProfile reloadData];
                                     
                                 }
                                 else{
                                     
                                     //yourImageURL is valid
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         //show your image
                                         imageUserProfile.image=image;
                                         [_tblUserProfile reloadData];
                                         
                                     });
                                     
                                 }
                                 
                             });
                             // do whatever you want with image
                         }
                         else
                         {
                             imageUserProfile.image = tempImg1;
                             [_tblUserProfile reloadData];
                             
                         }
                         
                     }];
                    
                    
                    
                    
                    
                    
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                
                
                
                
            }
            
            
            
            
        });
        
    }
    else
    {
        imageUserProfile.image=[UIImage imageNamed:@"user_placeholder"];
        
        
        //-===================================================
        //-===================================================
        //-===================================================
        
        NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
        
        if(![[url absoluteString] isEqualToString:@""])
        {
            [imageUserProfile sd_setImageWithURL:url
                                placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
        }
        
        
        //-===================================================
        //-===================================================
        //-===================================================
        //-===================================================
        
        
        
        
        
    }
    
    
    
    imageUserProfile.clipsToBounds=YES;
    _tblUserProfile.tableHeaderView =  [self designHeaderView];
    [_tblUserProfile reloadData];
    
    
    
}



-(void)resendAction:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]
                          
                          initWithTitle:NSLocalizedString(@"resend_email_heading", nil)
                          message:NSLocalizedString(@"resend_email_heading_desp", nil)
                          delegate:self
                          cancelButtonTitle:nil
                          otherButtonTitles:NSLocalizedString(@"cancel_caps", nil),NSLocalizedString(@"yes", nil), nil];
    alert.tag = 1000;
    [alert show];
}






-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1000 && buttonIndex == 1)
    { // handle the altdev
        
        [self hitResendEmail];
    }
}





//----- hitAPI for IVR OTP call Type registration ------
-(void)hitResendEmail
{
    
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.profileEmailSelected forKey:@"email"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_RESENDEMAILVERIFY withBody:dictBody andTag:TAG_REQUEST_RESENDEMAILVERIFY completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"success", nil)
                                                                message:rd
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil];
                [alert show];
                
                
            }
            
        }
        else{
            
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/
/*
 
 
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

