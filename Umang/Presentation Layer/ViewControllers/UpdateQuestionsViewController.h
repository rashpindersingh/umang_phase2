//
//  UpdateQuestionsViewController.h
//  Umang
//
//  Created by Lokesh Jain on 10/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateQuestionsViewController : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *securityQuestionLabel;
@property (weak, nonatomic) IBOutlet UILabel *securityDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UITableView *questionTable;

@property (nonatomic,copy) NSString *tagCheckRecovery;

@property (nonatomic,copy) NSString *tagComingFrom;
@property (nonatomic,copy) NSString *mpinStr;
@property (nonatomic,strong)NSMutableArray *responseQuestionArray;

@property (nonatomic,strong)NSString *otpString;
    
@end


#pragma mark - Question Cell Interface
@interface QuestionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *questionNumberLabel;
@property (weak, nonatomic) IBOutlet UIView *changeQuestionView;
@property (weak, nonatomic) IBOutlet UIButton *changeQuestionButton;
@property (weak, nonatomic) IBOutlet UITextField *answerTextfield;
@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@property (weak, nonatomic) IBOutlet UILabel *topLine;
@property (weak, nonatomic) IBOutlet UILabel *bottomLine;

@end
