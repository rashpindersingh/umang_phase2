//
//  AboutInfoVC.h
//  Umang
//
//  Created by deepak singh rawat on 23/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutInfoVC : UIViewController
{
    SharedManager*singleton;
    
    IBOutletCollection(NSLayoutConstraint) NSArray *topConstraint;
    __weak IBOutlet UILabel *lblPrivacyPolicy;
    __weak IBOutlet UILabel *lblTermsservice;

    __weak IBOutlet UIButton *btnPrivacyPolicy;
    __weak IBOutlet UIButton *btnTermsService;
    __weak IBOutlet UILabel *lblVersion;
    __weak IBOutlet UILabel *lblCopyrgt;
    __weak IBOutlet UILabel *lblUmang;
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblTitleAbout;
    
    __weak IBOutlet UILabel *lblBuildVersion_Date;
    __weak IBOutlet UILabel *umangDescLabel;
    
    IBOutlet UIButton *cancelRefButton;
}
-(IBAction)backbtnAction:(id)sender;
-(IBAction)termAction:(id)sender;
-(IBAction)privacyAction:(id)sender;


@end
