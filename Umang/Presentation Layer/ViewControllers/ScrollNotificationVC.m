//
//  ScrollNotificationVC.m
//  Umang
//
//  Created by admin on 26/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ScrollNotificationVC.h"
//#import "NotificationViewController.h"
#import "NotificationViewCell.h"
#import "NotificationItemBO.h"
#import "FilterViewController.h"
#import "NotificationSearchVC.h"
#import "SettingNotificationVwCon.h"


#define kGenButtonTag 455
#import "UIImageView+WebCache.h"



#import "SettingsViewController.h"
#import "HelpViewController.h"
#import "SocialMediaViewController.h"
#import "NotLinkedAadharVC.h"
#import "AadharCardViewCon.h"
#import "FeedbackVC.h"
#import "SecuritySettingVC.h"
#import "UserProfileVC.h"
#import "ShowUserProfileVC.h"

#import "RateUsVCViewController.h"


#import "FAQWebVC.h"
#import "HomeDetailVC.h"


static float NV_height= 50;


@interface ScrollNotificationVC ()<NSwipeTableViewCellDelegate>
{
    NSMutableArray *arrMainData;
    NSMutableArray *arrTableData;
    
    //  NSMutableArray *_arrNotificationService;
    
    __weak IBOutlet UILabel *lblNoNotificatn;
    UIView *NotifycontainerView;
    // NotificationFilterVC *vwContNotification;
    
    UILabel *lbl_profileComplete;
    UILabel *lbl_whiteLine;
    UILabel *lbl_percentage;
    UIButton *btn_close;
    UIButton *btn_clickUpdate;
    NotificationViewCell *currentNotificationCell;
    SharedManager *singleton;
    IBOutlet UIView *vw_noServiceFound;
    
}
@property(nonatomic,assign) int segment;
@end

@implementation ScrollNotificationVC
@synthesize segment;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setObject:0 forKey:@"BadgeValue"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0]; // this one
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDel.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    
    
    _btnClearAll.enabled = NO;
    _btnClearAll.hidden = YES;
    
    [_backBtnHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_backBtnHome.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(_backBtnHome.frame.origin.x, _backBtnHome.frame.origin.y, _backBtnHome.frame.size.width, _backBtnHome.frame.size.height);
        
        [_backBtnHome setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _backBtnHome.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:NOTIFICATION_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    // Do any additional setup after loading the view.
    // self.tblVwNotifications.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationController.navigationBarHidden=true;
    self.tblVwNotifications.layoutMargins = UIEdgeInsetsZero;
    UIView *vwLineHide = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.5)];
    vw_noServiceFound.backgroundColor=[UIColor clearColor];
    lblNoNotificatn.text = NSLocalizedString(@"no_notifications", nil);
    [self.tblVwNotifications addSubview:vwLineHide];
    vwLineHide.backgroundColor = [UIColor whiteColor];
    
    _lblNotificationHeader.text = NSLocalizedString(@"notifications", @"");
    
    [_btnClearAll setTitle:NSLocalizedString(@"clear_all", @"") forState:UIControlStateNormal];
    
    [_segmentFilter setTitle:NSLocalizedString(@"all", @"") forSegmentAtIndex:0];
    [_segmentFilter setTitle:NSLocalizedString(@"promotional_small", @"") forSegmentAtIndex:1];
    [_segmentFilter setTitle:NSLocalizedString(@"transactional_small", @"") forSegmentAtIndex:2];
    //[_segmentFilter setTitle:NSLocalizedString(@"favourites_small", nil) forSegmentAtIndex:3];
    
    segment=0;
    
    [self setNeedsStatusBarAppearanceUpdate];
    [self notiticationCellXib];
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        self.segmentFilter.frame = CGRectMake(self.view.frame.size.width/2 - 300, self.segmentFilter.frame.origin.y, 600, self.segmentFilter.frame.size.height);
        
        for (int i = 0; i < 3; i++)
        {
            [self.segmentFilter setWidth:200.0 forSegmentAtIndex:i];
        }
        
    }
    else
    {
        self.segmentFilter.frame = CGRectMake(10, self.segmentFilter.frame.origin.y, self.view.frame.size.width-20, self.segmentFilter.frame.size.height);
        
        for (int i = 0; i < 3; i++)
        {
            [self.segmentFilter setWidth:(self.segmentFilter.frame.size.width)/3 forSegmentAtIndex:i];
        }
    }
    
}


-(NotificationViewCell*)notiticationCellXib

{
    UINib *nib = [UINib nibWithNibName:@"NotificationViewCell" bundle:nil];
    [[self tblVwNotifications] registerNib:nib forCellReuseIdentifier:@"NotificationViewCellXIB"];
    if (currentNotificationCell == nil)
        
    {
        
        currentNotificationCell = [[[NSBundle mainBundle] loadNibNamed:@"NotificationViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    return currentNotificationCell;
}
-(void)addNotificationViewServiceData
{
    arrTableData = [[NSMutableArray alloc]init];
    
    arrTableData  = [[singleton.dbManager getNotifData:singleton.user_id] mutableCopy];
    
    arrMainData= [[NSMutableArray alloc]init];
    arrMainData= [[singleton.dbManager getNotifData:singleton.user_id] mutableCopy];
    
    
    [self checkServiceStatus];
    
    [_tblVwNotifications reloadData];
    
}

-(void)checkServiceStatus
{
    if ([arrMainData count]>0) {
        // self.lbl_favourite.hidden=true;
        
        vw_noServiceFound.hidden=TRUE;
        _tblVwNotifications.hidden=false;
    }
    else
    {
        vw_noServiceFound.hidden=FALSE;
        
        // self.lbl_favourite.hidden=false;
        _tblVwNotifications.hidden=true;
        
    }
    
}



-(void)addNotify
{
    
    for (UIView *subview in [self.view subviews])
    {
        if (subview.tag == 7) {
            [subview removeFromSuperview];
        }
    }
    
    
    //----------- Add later can remove it----
    
    NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0,100,fDeviceWidth, NV_height)];
    NotifycontainerView.backgroundColor = [UIColor clearColor];
    NotifycontainerView.tag=7;
    [self.view addSubview:NotifycontainerView];
    
    
    //-------- background image view--------------
    UIView *bg_img=[[UIView alloc]initWithFrame:CGRectMake(0, 0,fDeviceWidth, NV_height)];
    bg_img.backgroundColor = [UIColor colorWithRed:53.0/255.0 green:170.0/255.0 blue:97.0/255.0 alpha:1.0];
    [NotifycontainerView addSubview:bg_img];
    
    
    
    
    //-------Label Complete text----------
    lbl_profileComplete=[[UILabel alloc]initWithFrame:CGRectMake(60,0,fDeviceWidth-120,50)];
    lbl_profileComplete.text= NSLocalizedString(@"disabled_notif_txt", nil);
    lbl_profileComplete.font=[UIFont systemFontOfSize:13];
    lbl_profileComplete.textColor=[UIColor whiteColor];
    lbl_profileComplete.numberOfLines = 0;
    lbl_profileComplete.textAlignment = NSTextAlignmentCenter;
    [NotifycontainerView addSubview:lbl_profileComplete];
    
    
    UIButton *btnTransparent = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, fDeviceWidth-40, 50)];
    btnTransparent.backgroundColor = [UIColor clearColor];
    [NotifycontainerView addSubview:btnTransparent];
    [btnTransparent addTarget:self action:@selector(btnTransparentClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //-------Button Close ----------
    
    btn_close = [UIButton buttonWithType:UIButtonTypeCustom];
    
    btn_close.frame = CGRectMake(fDeviceWidth-30, 12, 22, 22);
    
    [btn_close addTarget:self
                  action:@selector(closeBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    UIImage *buttonImagePressed = [UIImage imageNamed:@"btn_close_notify.png"];
    [btn_close setImage:buttonImagePressed forState:UIControlStateNormal];
    [btn_close setImage:buttonImagePressed forState:UIControlStateSelected];
    [NotifycontainerView addSubview:btn_close];
    
    
    //-------Button Click Update ----------
    
    
}


-(void)btnTransparentClicked
{
    NSLog(@"Setting Pressed");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingNotificationVwCon *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingNotificationVwCon"];
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:vc animated:NO completion:nil];
    
    
    [self.navigationController pushViewController:vc animated:YES];
    
}



- (void)closeBtnAction:(id)sender
{
    NSLog(@"inside Close Btn");
    
    
    _tblVwNotifications.frame=CGRectMake(0,100,self.view.frame.size.width,fDeviceHeight-120);
    
    
    [NotifycontainerView removeFromSuperview];
}





- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

//-(void)prepareTempData
//{
//
//    arrMainData = [[SharedManager sharedSingleton] prepareStaticDataForNotifications];
//    arrTableData = [[NSMutableArray alloc] initWithArray:arrMainData];
//
//    [_tblVwNotifications reloadData];
//
//}
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *strMsg = [[arrTableData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_MSG"];
    CGRect size = [self rectForText:strMsg usingFont:[AppFont regularFont:14] boundedBySize:CGSizeMake(fDeviceWidth - 100, 0)];
    
    return size.size.height + 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return arrTableData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    _btnClearAll.enabled = YES;
    _btnClearAll.hidden = NO;
    
    
    static NSString *CellIdentifier = @"NotificationViewCellXIB";
    NotificationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"NotificationViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    /* NotificationItemBO *objNotification = arrTableData[indexPath.row];
     cell.objCellItem = objNotification;
     */
    cell.lblNotificationHeader.text = [[arrTableData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_TITLE"];
    
    cell.lblNotificationDesc.text =[[arrTableData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_MSG"];
    
    
    // comment this line and add below code
    // cell.lblDate.text =[[arrTableData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_RECEIVE_DATE_TIME"];
    
    //============== Start of Change date format==============
    
    NSString *dbDateStr = [NSString stringWithFormat:@"%@",[[arrTableData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_RECEIVE_DATE_TIME"]];
    
    // Convert string to date with dd/MM/yyyy HH:mm:ss formate
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSDate *Dbdate = [dateFormat dateFromString:dbDateStr];
    NSLog(@"Database date from string %@",Dbdate);
    
    // Convert Dbdate object to desired output format
    [dateFormat setDateFormat:@"dd MMM YYYY HH:mm"];
    NSString* dateStr = [dateFormat stringFromDate:Dbdate];
    NSLog(@"dateStr %@",dateStr);
    cell.lblDate.text=dateStr;
    
    //============== End of Change date format==============
    
    
    
    
    
    
    
    
    
    
    
    NSURL *url=[NSURL URLWithString:[[arrTableData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_IMG"]];
    [cell.imgVwType sd_setImageWithURL:url
                      placeholderImage:[UIImage imageNamed:@"umang_new_logo"]];
    
    
    [cell.btn_fav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
    cell.btn_fav.tag=indexPath.row ;
    [cell.btnShare  addTarget:self action:@selector(didTapShareNotificationBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnShare.tag=indexPath.row + 111;
    NSString *notifId=[[arrTableData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_ID"];
    NSString *serviceFav=[singleton.dbManager getNotifyFavStatus:notifId withUser_id:singleton.user_id];
    if ([serviceFav isEqualToString:@"true"])
    {
        cell.btn_fav.selected=YES;
    }else{
        cell.btn_fav.selected=NO;
    }
    
    cell.imgCategory.backgroundColor = [UIColor clearColor];
    cell.layoutMargins = UIEdgeInsetsZero;
    NSString *type=[[arrTableData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_TYPE"];
    
    if ([type isEqualToString:@"promo"]||[type isEqualToString:NSLocalizedString(@"promo", nil)]||[type isEqualToString:@"Promo"])
    {
        
        //comment this line and add code for hide promo image under promo segment
        //cell.imgCategory.image = [UIImage imageNamed:@"promo"];
        
        //code change here for hide image at promo time
        if (segment==1) //current segment in promo hide promo image
        {
            cell.imgCategory.image = nil;
            
        }
        else
        {
            cell.imgCategory.image = [UIImage imageNamed:@"promo"];
            
        }
        
        
    }
    else{
        cell.imgCategory.image = nil;
    }
    
    //UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textPressed:)];
    //UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(textPressed:)];
    //[cell.lblNotificationDesc addGestureRecognizer:tap];
    //[cell.lblNotificationDesc addGestureRecognizer:longPress];
    //[cell.lblNotificationDesc setUserInteractionEnabled:YES];
    
    
    cell.lblNotificationHeader.font = [AppFont regularFont:16.0];
    cell.lblNotificationDesc.font = [AppFont lightFont:14.0];
    cell.lblDate.font = [AppFont mediumFont:13.0];
    
    return cell;
}

/*- (void)textPressed:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateRecognized &&
        [gestureRecognizer.view isKindOfClass:[UILabel class]]) {
        UILabel *someLabel = (UILabel *)gestureRecognizer.view;
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        [pasteboard setString:someLabel.text];
        //let the user know you copied the text to the pasteboard and they can no paste it somewhere else
    }
}*/

-(void)didTapShareNotificationBtnAction:(UIButton*)sender {
    
    NSString *textToShare = [[arrTableData objectAtIndex:(sender.tag % 111)] valueForKey:@"NOTIF_MSG"];;
    NSArray *objectsToShare = @[textToShare];
    [[UIPasteboard generalPasteboard] setString:textToShare];
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
}

- (IBAction)fav_action:(MyFavButton*)sender  {
    
    
    MyFavButton *button = (MyFavButton *)sender;
    // Add image to button for normal state
    NSLog(@"sender=%ld",(long)[sender tag]);
    
    int indexOfTheRow=(int)button.tag;
    
    
    
    NSLog(@"selected segment=%d",segment);
    
    NSLog(@"indexOfTheRow=%d",indexOfTheRow);
    //NSString *text ;
    //    // Add image to button for pressed state
    //    UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
    //    UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
    //
    //    [button setImage:btnImage1 forState:UIControlStateNormal];
    //    [button setImage:btnImage2 forState:UIControlStateSelected];
    
    
    
    //text = [NSString stringWithFormat:@"%d",indexOfTheRow];
    
    //  NSString *serviceFav=[NSString stringWithFormat:@"%@",[[arrTableData objectAtIndex:indexOfTheRow] valueForKey:@"NOTIF_IS_FAV"]];//NOTIF_IS_FAV
    
    NSString *notifId=[[arrTableData objectAtIndex:indexOfTheRow] valueForKey:@"NOTIF_ID"];
    
    NSString *serviceFav=[singleton.dbManager getNotifyFavStatus:notifId withUser_id:singleton.user_id];
    
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        [singleton.dbManager updateNotifIsFav:notifId notifIsFav:@"false" withUser_id:singleton.user_id];
        
        
        
        if (segment==3) {
            [arrTableData removeObjectAtIndex:indexOfTheRow];
            
        }
        
        button.selected=FALSE;
        
    }
    else
    {
        [singleton.dbManager updateNotifIsFav:notifId notifIsFav:@"true" withUser_id:singleton.user_id];
        button.selected=true;
    }
    
    //[self getFavouriteNotification];
    if (segment==3)
    {
        
        
        [_tblVwNotifications beginUpdates];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfTheRow inSection:0];
        [_tblVwNotifications deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [_tblVwNotifications endUpdates];
        
    }
    //[self checkServiceStatus];
    [self performSelector:@selector(reloadTableWhenFavButtonAction) withObject:nil afterDelay:1.0];
    
}
-(void)reloadTableWhenFavButtonAction {
    [_tblVwNotifications reloadData];
    
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Clear";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        //---- Delete from Database--------
        NSString *notifID=[[arrTableData objectAtIndex:indexPath.row]valueForKey:@"NOTIF_ID"];
        [singleton.dbManager deleteNotification:notifID withUser_id:singleton.user_id];
        //---- Delete from Database--------
        
        [arrTableData removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
}

-(void)pannedCell:(id)cell
{
    currentNotificationCell = (NotificationViewCell*)cell;
}

-(void)deleteNotificationItemFromTableRow:(NotificationItemBO*)object
{
    NSInteger index = 0;
    if (object != nil)
    {
        index = [arrTableData indexOfObject:object];
        if (index < arrTableData.count) {
            
            
            //---- Delete from Database--------
            NSString *notifID=[[arrTableData objectAtIndex:index]valueForKey:@"NOTIF_ID"];
            [singleton.dbManager deleteNotification:notifID withUser_id:singleton.user_id];
            //---- Delete from Database--------
            [arrTableData removeObjectAtIndex:index];
            
        }
    }
    
    //    [_tblVwNotifications reloadData];
    
    [_tblVwNotifications beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [_tblVwNotifications deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [_tblVwNotifications endUpdates];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //------fix lint-----
    
    //NSMutableDictionary *notifyData=[NSMutableDictionary new];
    // notifyData=(NSMutableDictionary*)[arrTableData objectAtIndex:indexPath.row];
    
   
    NSMutableDictionary *notifyData = (NSMutableDictionary *)[arrTableData objectAtIndex:indexPath.row];
    
    @try {
        
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [notifyData valueForKey:@"NOTIF_MSG"];
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
  
    
    [self selectedIndexNotify:notifyData];
    
    
    [singleton traceEvents:@"Notification Clicked" withAction:@"Clicked" withLabel:@"Notification" andValue:0];
    
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
/*
 
 "CURRENT_TIME_MILLS" = "1482235367.394016";
 ID = 1;
 "NOTIF_DATE" = "12-20-2016";
 "NOTIF_DIALOG_MSG" = "";
 "NOTIF_ID" = "cd3d26c2-231f-4044-81f1-3a5faad2d867";
 "NOTIF_IMG" = "https://static.umang.gov.in/app/ico/service/ncert.png";
 "NOTIF_IS_FAV" = false;
 "NOTIF_MSG" = "This is a test trans CBSE message.";
 "NOTIF_RECEIVE_DATE_TIME" = "20/12/2016 17:32:47";
 "NOTIF_SCREEN_NAME" = "";
 "NOTIF_STATE" = 31;
 "NOTIF_SUB_TYPE" = rating;
 "NOTIF_TIME" = "17:29:07";
 "NOTIF_TITLE" = ORS;
 "NOTIF_TYPE" = promo;
 "NOTIF_URL" = "https://web.umang.gov.in/uw/api/deptt/ncert/";
 "NOTIF_WEBPAGE_TITLE" = "Custom Title";
 "SERVICE_ID" = 9;
 
 
 */

-(void)selectedIndexNotify:(NSDictionary*)notifyData
{
    //----------Handle case for subType---------------
    NSString *subType =[notifyData valueForKey:@"NOTIF_SUB_TYPE"];
    //subType=[subType  lowercaseString];
    // Case to open app  for opening app with notification title/
    if([subType isEqualToString:@"openApp"])
    {
        //handle case in delegate for it do nothing
        
    }
    // Case to open app  with dialog message dialogmsg/msg
    
    else if([subType isEqualToString:@"openAppWithDialog"])
    {
        NSString *title =[notifyData valueForKey:@"NOTIF_TITLE"];
        NSString *dialogMsg =[notifyData valueForKey:@"NOTIF_DIALOG_MSG"];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:dialogMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
        
    }
    // Case to open  playstore url in external
    
    else if([subType isEqualToString:@"playstore"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_URL"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        
        
    }
    // Case to open app  inside webview with custom webview title
    
    else if([subType isEqualToString:@"webview"])
    {
        NSString *title =[notifyData valueForKey:@"NOTIF_WEBPAGE_TITLE"];
        NSString *url=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_URL"]];
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        [self openFAQWebVC:url withTitle:title];
        
    }
    // Case to open app  in mobile browser
    
    else if([subType isEqualToString:@"browser"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_URL"]];
        NSString* webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL* urltoOpen = [NSURL URLWithString:webStringURL];
        
        [[UIApplication sharedApplication] openURL:urltoOpen];
        
    }
    // Case to open app  with Screen Name like profile/settings etc
    
    else if([subType isEqualToString:@"openAppWithScreen"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        
        if ([screenName isEqualToString:@"settings"])
        {
            [self mySetting_Action];
            
        }
        if ([screenName isEqualToString:@"help"])
        {
            [self myHelp_Action];
            
        }
        if ([screenName isEqualToString:@"social"])
        {
            [self socialMediaAccount];
            
        }
        if ([screenName isEqualToString:@"aadhaar"])
        {
            if (singleton.objUserProfile.objAadhar.aadhar_number.length)
            {
                
                [self AadharCardViewCon];
                
            }
            else
            {
                [self NotLinkedAadharVC];
                
            }
        }
        if ([screenName isEqualToString:@"feedback"])
        {
            [self FeedbackVC];
            
        }
        if ([screenName isEqualToString:@"accountsettings"])
        {
            [self accountSettingAction];
        }
        if ([screenName isEqualToString:@"myprofile"])
        {
            [self myProfile_Action];
            
        }
        else
        {
            //main
        }
        
        
    }
    
    
    
    // Case to open app  with tab name
    
    else if([subType isEqualToString:@"openAppWithTab"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        
        
        if ([screenName isEqualToString:NSLocalizedString(@"home_small", nil)])
        {
            tbc.selectedIndex=0;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"favourites_small", nil)])
        {
            tbc.selectedIndex=1;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"states", nil)])
        {
            //tbc.selectedIndex=0;ignore case
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"all_services_small", nil)])
        {
            tbc.selectedIndex=2;
        }
        
        
        [self presentViewController:tbc animated:NO completion:nil];
        
        
        
    }
    // Case to open app with service
    
    else  if([subType isEqualToString:NSLocalizedString(@"services", nil)])
    {
        
        if (notifyData)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
            [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            vc.dic_serviceInfo=notifyData;
            vc.tagComeFrom=@"NOTIFICATIONVIEW";
            
            vc.sourceTab     = @"notification";
            vc.sourceState   = @"";
            vc.sourceBanner  = @"";
            
            switch (self.segmentFilter.selectedSegmentIndex)
            {
                case 0:
                {
                    vc.sourceSection = @"all";
                }
                    break;
                    
                case 1:
                {
                    vc.sourceSection = @"promo";
                }
                    break;
                    
                case 2:
                {
                    vc.sourceSection = @"trans";
                }
                    break;
                default:
                    break;
            }
            
            
            [self presentViewController:vc animated:NO completion:nil];
            
            
        }
        
    }
    // Case to open app  for rating view
    
    else  if([subType isEqualToString:NSLocalizedString(@"Rating", nil)] || [subType isEqualToString:@"rating"])
    {
        [self rateUsClicked];
        
    }
    // Case to open app  for share [sharing message will recieve inside api)
    else  if([subType isEqualToString:NSLocalizedString(@"share", nil)] || [subType isEqualToString:@"share"])
    {
        [self shareContent];
    }
    //Default case
    else
    {
        
        
    }
    
    
}

-(void)shareContent
{
    NSString *textToShare =singleton.shareText;
    
    NSArray *objectsToShare = @[textToShare];
    
    [[UIPasteboard generalPasteboard] setString:textToShare];
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    
    //if iPad
    
    else {
        
        // Change Rect to position Popover
        
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}

-(void)openFAQWebVC:(NSString *)url withTitle:(NSString*)title
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    
    //  [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)rateUsClicked
{
    NSLog(@"My Help Action");
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RateUsVCViewController  *vc = [storyboard instantiateViewControllerWithIdentifier:@"RateUsVCViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)NotLinkedAadharVC
{
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotLinkedAadharVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NotLinkedAadharVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)AadharCardViewCon
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AadharCardViewCon *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharCardViewCon"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)FeedbackVC
{
    
   // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    FeedbackVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)socialMediaAccount
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SocialMediaViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SocialMediaViewController"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)myProfile_Action
{
    NSLog(@"My Profile Action");
    
    
    
    
    // SettingsViewController
    //    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kMainStoryBoard bundle:nil];
    //
    //    UserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)mySetting_Action
{
    NSLog(@"My Setting Action");
    
    // SettingsViewController
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
-(void)myHelp_Action
{
    NSLog(@"My Help Action");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)accountSettingAction
{
    NSLog(@"My account setting Action");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SecuritySettingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecuritySettingVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}




- (IBAction)btnSearchIconClicked:(id)sender
{
    //aditi
    NSLog(@"Search Pressed");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NotificationSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NotificationSearchVC"];
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:vc animated:NO completion:nil];
    
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)btnSettingIconClicked:(id)sender
{
    NSLog(@"Setting Pressed");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingNotificationVwCon *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingNotificationVwCon"];
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:vc animated:NO completion:nil];
    
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


- (IBAction)btnFilterClicked:(id)sender
{
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    FilterViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    singleton=[SharedManager sharedSingleton];
    
    
    
    if([singleton.shared_ntft isEqualToString:@"0"]&&[singleton.shared_ntfp isEqualToString:@"0"])
    {
        singleton.notificationSelected=NSLocalizedString(@"disabled", nil);
        
        
    }
    if([singleton.shared_ntft isEqualToString:@"1"]&&[singleton.shared_ntfp isEqualToString:@"1"])
    {
        singleton.notificationSelected=NSLocalizedString(@"enabled", nil);
    }
    if([singleton.notificationSelected isEqualToString:NSLocalizedString(@"enabled", nil)])
        
    {
        _tblVwNotifications.frame=CGRectMake(0,100,self.view.frame.size.width,fDeviceHeight-120);
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            _tblVwNotifications.frame=CGRectMake(80 ,120,self.view.frame.size.width - 160,fDeviceHeight-140);
        }
        
        [NotifycontainerView removeFromSuperview];
    }
    else
    {
        _tblVwNotifications.frame=CGRectMake(0,150,self.view.frame.size.width,fDeviceHeight-170);
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            _tblVwNotifications.frame=CGRectMake(80,170,self.view.frame.size.width - 160,fDeviceHeight-190);
        }
        [self addNotify];
        
        // [_tblVwNotifications reloadData];
        
    }
    [self addNotificationViewServiceData];
    
    // if(segment)
    //{
    //_segmentFilter.selectedSegmentIndex=segment;
    
    
    @try
    {
        [self resetSegmentSelected:segment];
    }
    @catch (NSException *exception)
    {
        
    } @finally {
        
    }
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    [super viewWillAppear:NO];
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [_backBtnHome.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _lblNotificationHeader.font = [AppFont semiBoldFont:17];
    [_btnClearAll.titleLabel setFont:[AppFont regularFont:20.0]];
    lblNoNotificatn.font = [AppFont regularFont:14.0];
}
#pragma mark - 
-(IBAction)btnBackClicked:(id)sender
{
    NSLog(@"inside back");
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)resetSegmentSelected:(int)selectIndex
{
    arrMainData= [[NSMutableArray alloc]init];
    arrMainData= [[singleton.dbManager getNotifData:singleton.user_id] mutableCopy];
    
    switch (selectIndex) {
        case 0:
        {
            // All Case
            [arrTableData removeAllObjects];
            arrTableData =arrMainData;
        }
            break;
        case 1:
        {
            // Promotional Case
            [arrTableData removeAllObjects];
            // objSetting.notificationStatusPT
           // NSPredicate *predicateFilter = [NSPredicate predicateWithFormat:@"self.NOTIF_TYPE CONTAINS %@", @"Promo"];
            
           NSPredicate *predicateFilter = [NSPredicate predicateWithFormat:@"self.NOTIF_TYPE CONTAINS %@", @"promo"];
            NSArray *filterArr=[arrMainData filteredArrayUsingPredicate:predicateFilter];
            if(filterArr)
            {
                [arrTableData addObjectsFromArray:filterArr];
            }
            else
            {
                //do nothing
            }
        }
            break;
        case 2:
        {
            // Transactional Case
            [arrTableData removeAllObjects];
            NSPredicate *predicateFilter = [NSPredicate predicateWithFormat:@"self.NOTIF_TYPE CONTAINS %@", @"trans"];
            //  NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.statusType == 101"];
            if([arrMainData filteredArrayUsingPredicate:predicateFilter])
            {
                [arrTableData addObjectsFromArray:[arrMainData filteredArrayUsingPredicate:predicateFilter]];
            }
            else
            {
                
            }
            
        }
            break;
        case 3:
        {
            [arrTableData removeAllObjects];
            
            NSPredicate *predicateFilter = [NSPredicate predicateWithFormat:@"self.NOTIF_IS_FAV CONTAINS %@", @"true"];
            //  NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.statusType == 101"];
            if([arrMainData filteredArrayUsingPredicate:predicateFilter])
            {
                [arrTableData addObjectsFromArray:[arrMainData filteredArrayUsingPredicate:predicateFilter]];
            }
            else
            {
                
            }
            
        }
            break;
            
        default:
            break;
    }
    if([arrTableData count]==0)
    {
        _btnClearAll.enabled = NO;
        _btnClearAll.hidden = YES;
        
    }
    else
    {
        _btnClearAll.enabled = YES;
        _btnClearAll.hidden = NO;
        
    }
    [_tblVwNotifications reloadData];
    
}




- (IBAction)segmentOptionClicked:(UISegmentedControl *)sender {
    
    segment=(int)self.segmentFilter.selectedSegmentIndex;
    
    arrMainData= [[NSMutableArray alloc]init];
    arrMainData= [[singleton.dbManager getNotifData:singleton.user_id] mutableCopy];
    
    switch (sender.selectedSegmentIndex) {
        case 0:
        {
            // All Case
            [arrTableData removeAllObjects];
            arrTableData =arrMainData;
        }
            break;
        case 1:
        {
            // Promotional Case
            [arrTableData removeAllObjects];
            // objSetting.notificationStatusPT
           
            NSPredicate *predicatePromoFilter = [NSPredicate predicateWithFormat:@"self.NOTIF_TYPE CONTAINS %@", @"Promo"];
            NSPredicate *predicateFilter = [NSPredicate predicateWithFormat:@"self.NOTIF_TYPE CONTAINS %@", @"promo"];
            
            NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicatePromoFilter, predicateFilter]];
            
            NSArray *filterArr=[arrMainData filteredArrayUsingPredicate:predicate];
            if(filterArr)
            {
                [arrTableData addObjectsFromArray:filterArr];
            }
            else
            {
                //do nothing
            }
        }
            break;
        case 2:
        {
            // Transactional Case
            [arrTableData removeAllObjects];
            NSPredicate *predicateFilter = [NSPredicate predicateWithFormat:@"self.NOTIF_TYPE CONTAINS %@", @"trans"];
            //  NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.statusType == 101"];
            if([arrMainData filteredArrayUsingPredicate:predicateFilter])
            {
                [arrTableData addObjectsFromArray:[arrMainData filteredArrayUsingPredicate:predicateFilter]];
            }
            else
            {
                
            }
            
        }
            break;
        case 3:
        {
            /*[arrTableData removeAllObjects];
             
             NSPredicate *predicateFilter = [NSPredicate predicateWithFormat:@"self.NOTIF_IS_FAV CONTAINS %@", @"true"];
             //  NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.statusType == 101"];
             if([arrMainData filteredArrayUsingPredicate:predicateFilter])
             {
             [arrTableData addObjectsFromArray:[arrMainData filteredArrayUsingPredicate:predicateFilter]];
             }
             else
             {
             
             }*/
            
        }
            break;
            
        default:
            break;
    }
    if([arrTableData count]==0)
    {
        _btnClearAll.enabled = NO;
        _btnClearAll.hidden = YES;
        
    }
    else
    {
        _btnClearAll.enabled = YES;
        _btnClearAll.hidden = NO;
        
    }
    [_tblVwNotifications reloadData];
}



-(void)getFavouriteNotification
{
    [arrTableData removeAllObjects];
    
}

- (IBAction)btnSettingClicked:(id)sender
{
}


- (IBAction)btnClearAllDataClicked:(id)sender {
    
    
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"notif_clear_msg", nil) message:NSLocalizedString(@"notif_clear_msg", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"yes", nil),NSLocalizedString(@"no", nil), nil];
    alert.tag=1000;
    [alert show];
    
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag==1000)
    {
        
        
        NSLog(@"index button=%ld",(long)buttonIndex);
        if (buttonIndex == 0)
        {
            _btnClearAll.enabled = NO;
            _btnClearAll.hidden = YES;
            
            [arrTableData removeAllObjects];
            [_tblVwNotifications reloadData];
            [singleton.dbManager deleteAllNotifications:singleton.user_id];
        }
        else
        {
            //do nothing
        }
    }
    
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    if (currentNotificationCell) {
        [currentNotificationCell resetFramesForPannedArea];
    }
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 
 */
@end

