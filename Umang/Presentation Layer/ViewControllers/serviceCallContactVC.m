//
//  serviceCallContactVC.m
//  Umang
//
//  Created by admin on 03/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "serviceCallContactVC.h"

#import "serviceCallCell.h"

@interface serviceCallContactVC ()<UITableViewDelegate,UITableViewDataSource>
{
    SharedManager *singleton;

    IBOutlet UITableView *tbl_callcontact;
    IBOutlet UIView *vw_tableContainer;
}
@property(nonatomic,retain)NSString *callNumber;
@end

@implementation serviceCallContactVC
@synthesize  tablearray;
@synthesize callNumber;

- (void)viewDidLoad {
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:SERVICE_CALL_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    

    NSLog(@"tablearray=%@",tablearray);
    
    singleton = [SharedManager sharedSingleton];
    
    self.view.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.3];
    
    [self addShadowToTheView:self.view];
     

    tbl_callcontact.backgroundColor=[UIColor whiteColor];
    tbl_callcontact.tableFooterView = [UIView new];//remove empty table rows

    float height=[tablearray count]*45;
    
    if (height>fDeviceHeight)
    {
        height=200;
    }
    CGRect tblframe=tbl_callcontact.frame;
    tbl_callcontact.frame=CGRectMake(tblframe.origin.x, tblframe.origin.y, tblframe.size.width, height);
    
    
    [self addShadowToTheView:vw_tableContainer];
    [tbl_callcontact reloadData];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
    
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"";
    
    
}
#pragma mark - UITableViewDelegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0001;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 45.0;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return tablearray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
     //serviceCallCell.h
    
    
    static NSString *CellIdentifier = @"serviceCallCell";
    
    serviceCallCell *cell = (serviceCallCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure cell
    if (!cell)
    {
        cell = [[serviceCallCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    cell.lbl_contact.text=[tablearray objectAtIndex:indexPath.row];
    
    //cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    
    callNumber=[NSString stringWithFormat:@"%@",[tablearray objectAtIndex:indexPath.row]];
    
    callNumber = [callNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",callNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
    

    
    
}



- (IBAction)btn_close_Action:(id)sender {
    
    
    [UIView animateWithDuration:0.4 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}

*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
