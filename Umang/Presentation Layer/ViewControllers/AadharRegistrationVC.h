//
//  AadharRegistrationVC.h
//  Umang
//
//  Created by admin on 05/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    
    IS_FROM_REG_STEP3_SCREEN= 1002,
    IS_FROM_CHOOSE_REGISTRATION,
    Is_From_Aadhar_Registration,
    IS_FROM_NOT_LINKED,
    
    
}TYPE_AADHAR_COMING_FROM;



@interface AadharRegistrationVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btn_acceptTerm;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (assign,nonatomic )  TYPE_AADHAR_COMING_FROM TYPE_AADHAR_REGISTRATION_FROM;


//----- added for passing rty and tout-----
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
//----- added for passing rty and tout-----


@end
