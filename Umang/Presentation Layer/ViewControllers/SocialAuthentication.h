//
//  SocialAuthentication.h
//  Umang
//
//  Created by Kuldeep Saini on 11/28/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>
// For social login
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>
#import <TwitterKit/TwitterKit.h>
#import "SocialUserBO.h"


@protocol SocialAuthDelegate <NSObject,GIDSignInUIDelegate>

@required

// For Facebook Authentication
-(void)facebookAuthenticationSuccessful:(SocialUserBO*)object;
-(void)facebookAuthenticationFailed:(NSError*)error;

// For Google Authentication
-(void)googleAuthenticationSuccessful:(SocialUserBO*)object;
-(void)googleAuthenticationFailed:(NSError*)error;

// For Twitter Authentication
-(void)twitterAuthenticationSuccessful:(SocialUserBO*)object;
-(void)twitterAuthenticationFailed:(NSError*)error;

@end


@interface SocialAuthentication : NSObject

@property(nonatomic,weak)id <SocialAuthDelegate> delegate;


-(void)loginWithFacebookFromController:(id)vwCont;
-(void)loginWithGoogleFromController:(id)vwCont;
-(void)loginWithTwitterFromController:(id)vwCont;


-(void)logoutFromAllSocialFramewors;




@end
