//
//  AdvertisingColumn.h
//  Umang
//
//  Created by spice on 02/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StyledPageControl.h"

@interface AdvertisingColumn : UIView<UIScrollViewDelegate>{
    
    NSTimer *_timer;
    NSArray *imgArray;
    UIView *containerView;
}

@property (nonatomic,strong) UIScrollView *scrollView;
//@property (nonatomic,strong) UIPageControl *pageControl;
@property (nonatomic) StyledPageControl *pageControl;

@property (nonatomic,strong) UILabel *imageNum;
@property (nonatomic) NSInteger totalNum;

@property (nonatomic,copy) NSString *comingFrom;
@property (nonatomic,copy) NSString *stateIdToPass;

- (void)setArray:(NSArray *)imgArray;

- (void)openTimer;
- (void)closeTimer;

-(void)updateFrameOfAllComponents;
-(void)selectedIndexNotify:(NSDictionary*)bannerData;
@end
