//
//  NotificationFilterVC.m
//  Umang
//
//  Created by admin on 12/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "NotificationFilterVC.h"
#import "FilterCell.h"
#import "SortFilterCell.h"
#import "FilterServicesBO.h"
#import "SearchFilterVC.h"
#import "StateCustomCell.h"
#import "SDCapsuleButton.h"
#import "CZPickerView.h"
#import "StateList.h"
#import "HomeFilterResultsVC.h"

#import "NotificationItemBO.h"
#define BORDER_COLOR [UIColor colorWithRed:112.0/255.0 green:194.0/255.0 blue:138.0/255.0 alpha:1.0]

@interface NotificationFilterVC () <CZPickerViewDelegate,CZPickerViewDataSource>

{
    NSMutableArray *arrNotificationTypes;
    // NSMutableArray *arrStateNames;
    NSString *_selectedNotificationType;
    
    __weak IBOutlet UIButton *btnClear;
    BOOL isRegionalSelected;
    NSArray *arry_state;
    NSMutableArray *arrFirstFilterItems;
    NSMutableArray *arrSecondFilterItems;
    
    NSString *selectedFirstFilter;
    NSString *selectedSecondFilter;
    NSMutableArray *arrStates;
    SharedManager *singleton;
    NSMutableArray *_arrServiceFilter;
    NSMutableArray *_arrStates;
    
    StateList *objStateList;
    
    NSMutableArray *arrSelectedItemForPicker;
}

@end

@implementation NotificationFilterVC
@synthesize isRegionalSelected,btnApply;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:FILTER_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    
    arrSelectedItemForPicker = [NSMutableArray new];
    
    _arrStates = [NSMutableArray new];
    _selectedNotificationType = NSLocalizedString(@"all", nil);
    
    
    [btnApply setTitle:[NSLocalizedString(@"apply_label", nil) uppercaseString] forState:UIControlStateNormal];
    
    [_btnReset setTitle:NSLocalizedString(@"reset", nil) forState:UIControlStateNormal];
    [_btnBackHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_btnBackHome.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(_btnBackHome.frame.origin.x, _btnBackHome.frame.origin.y, _btnBackHome.frame.size.width, _btnBackHome.frame.size.height);
        
        [_btnBackHome setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _btnBackHome.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    _lblFilter.text = NSLocalizedString(@"filter", nil);
    
    //    [self setUpNavigationProperties];
    // arrStateNames = [NSMutableArray new];
    isRegionalSelected = NO;
    
    [self prepareTempDataForServicesType];
    [self.tblFilter reloadData];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    singleton = [SharedManager sharedSingleton];
    
    [self addFilterServiceData];
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.tblFilter.frame = CGRectMake(80, self.tblFilter.frame.origin.y, self.view.frame.size.width - 160, self.tblFilter.frame.size.height);
        
        self.tblFilter.backgroundColor = [UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0];
        
        self.view.backgroundColor = self.tblFilter.backgroundColor;
    }
    
    // Do any additional setup after loading the view.
}

-(void)addFilterServiceData{
    if (_arrServiceFilter == nil) {
        _arrServiceFilter = [NSMutableArray new];
    }
    else{
        [_arrServiceFilter removeAllObjects];
    }
    
    NSArray *arrService = [singleton.dbManager loadServiceCategory];
    
    for (int i = 0; i< arrService.count; i++) {
        NSDictionary *dict = arrService [i];
        
        FilterServicesBO *objFilter = [[FilterServicesBO alloc]init];
        objFilter.serviceName = [dict valueForKey:@"SERVICE_CATEGORY"];
        [_arrServiceFilter addObject:objFilter];
    }
    
    [_tblFilter reloadData];
}




-(void)setUpNavigationProperties{
    //    [self.navigationController.navigationBar setTranslucent:YES];
    //
    //    [self.navigationController.navigationBar setShadowImage:nil];
    //    // "Pixel" is a solid white 1x1 image.
    //    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.navigationController.navigationBar.barTintColor =DEFAULT_NAV_BAR_COLOR;
    self.navigationController.navigationBarHidden = NO;
    self.title = NSLocalizedString(@"filter", nil);
    
    // Add Reset Button on right side
    //    UIBarButtonItem *btnReset =    [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"reset", nil) style:UIBarButtonItemStyleDone target:self action:@selector(btnResetClicked)];
    //    self.navigationItem.rightBarButtonItem = btnReset;
}




- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}


-(void)prepareTempDataForServicesType{
    
    
    // Prepare State Data
    
    objStateList = [[StateList alloc] init];
    // arry_state = [objStateList getStateList];
    
    //arry_state = [arry_state sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    arrFirstFilterItems = [NSMutableArray new];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"alphabetic", nil) forKey:@"Title"];
    [dict setObject:@"icon_alphabatic" forKey:@"NormalState"];
    [dict setObject:@"icon_alphabatic_selected.png" forKey:@"SelectedState"];
    //objNotification.filterType = @"notification_type";
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    selectedFirstFilter =NSLocalizedString(@"alphabetic", nil);
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"most_popular", nil) forKey:@"Title"];
    [dict setObject:@"icon_most_popular.png" forKey:@"NormalState"];
    [dict setObject:@"icon_most_popular_select.png" forKey:@"SelectedState"];
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"top_rated", nil) forKey:@"Title"];
    [dict setObject:@"icon_top_rated.png" forKey:@"NormalState"];
    [dict setObject:@"icon_top_rated_select.png" forKey:@"SelectedState"];
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    
    arrSecondFilterItems = [NSMutableArray new];
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"all", nil) forKey:@"Title"];
    [dict setObject:@"icon_all.png" forKey:@"NormalState"];
    [dict setObject:@"icon_all_select.png" forKey:@"SelectedState"];
    [arrSecondFilterItems addObject:dict];
    dict = nil;
    
    selectedSecondFilter = NSLocalizedString(@"regional", nil);
    isRegionalSelected = YES;
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"centralgovernment", nil) forKey:@"Title"];
    [dict setObject:@"icon_central-_govt.png" forKey:@"NormalState"];
    [dict setObject:@"icon_central-_govt_select.png" forKey:@"SelectedState"];
    [arrSecondFilterItems addObject:dict];
    dict = nil;
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"regional", nil) forKey:@"Title"];
    [dict setObject:@"icon_regional-1.png" forKey:@"NormalState"];
    [dict setObject:@"icon_regional_selected.png" forKey:@"SelectedState"];
    [arrSecondFilterItems addObject:dict];
    dict = nil;
    if (self.stateSelected.length == 0) {
        [_arrStates addObject:NSLocalizedString(@"all", nil)];
    }else {
        [_arrStates addObject:self.stateSelected];
        singleton.stateSelected = self.stateSelected;
    }
    
    //    NSLog(@"stateSelected=%@",singleton.stateSelected);
    //    state_id=[obj getStateCode:singleton.stateSelected];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (isRegionalSelected) {
        if (section == 3) {
            if ([_arrServiceFilter count] > 0)
            {
                return 10.0;
            }
            else
            {
                return 10;
            }
        }
        return 50.0;
    }
    else{
        if (section == 0 || section == 1 || section == 4) {
            return 50.0;
        }
        else{
            return 0.001;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    static NSString *CellIdentifier = @"NotificationHeaderCell";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    UILabel *lblSectionTitle = (UILabel*)[headerView viewWithTag:122];
    UIButton *btnClear = (UIButton*)[headerView viewWithTag:123];
    
    [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateSelected];
    [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateNormal];
    
    if (section == 0) {
        lblSectionTitle.text = NSLocalizedString(@"sort_by", nil);
        btnClear.hidden = YES;
        
    }
    else if (section == 1)
    {
        
        lblSectionTitle.text = NSLocalizedString(@"alll_label", nil);
        btnClear.hidden = YES;
        
    }
    else if (section == 2)
    {
        if (isRegionalSelected)
        {
            lblSectionTitle.text = NSLocalizedString(@"state_label", nil);
            [btnClear setTitle:NSLocalizedString(@"clear", nil) forState:UIControlStateNormal];
            [btnClear addTarget:self action:@selector(btnClearStateClicked) forControlEvents:UIControlEventTouchUpInside];
            if ([_arrServiceFilter count]) {
                btnClear.hidden = NO;
            }
            else{
                btnClear.hidden = YES;
            }
            
        }
        else{
            return nil;
        }
    }
    else if (section == 3)
    {
        if (isRegionalSelected) {
            
            lblSectionTitle.text = @"";
        }
        else{
            return nil;
            
        }
        btnClear.hidden = YES;
        
    }
    
    else
    {
        [btnClear addTarget:self action:@selector(btnClearFilterCategoriesClicked) forControlEvents:UIControlEventTouchUpInside];
        
        lblSectionTitle.text = NSLocalizedString(@"cat_label", nil);
        
        // First Fetch all categories
        NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.isServiceSelected == YES"];
        NSArray *arrServices = [_arrServiceFilter filteredArrayUsingPredicate:predicateFilter];
        
        if ([arrServices count]) {
            btnClear.hidden = NO;
        }
        else{
            btnClear.hidden = YES;
        }
    }
    headerView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    return headerView;
}

-(void)btnClearStateClicked
{
    //    [_arrServiceFilter removeAllObjects];
    //    [_tblFilter reloadData];
    
    [arrSelectedItemForPicker removeAllObjects];
    [_arrStates removeAllObjects];
    [_arrStates addObject:NSLocalizedString(@"all", nil)];
    
    [_tblFilter reloadData];
    
}
-(void)btnClearFilterCategoriesClicked
{
    [_arrServiceFilter setValue:@NO forKey:@"isServiceSelected"];
    [_tblFilter reloadData];
    
}

- ( UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}// custom view for footer. will be adjusted to default or specified footer height



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ((indexPath.section == 0) || (indexPath.section == 1))
    {
        return  100;
    }
    else if (indexPath.section == 2) {
        return [self heightForDynamicCell];
    }
    else if (indexPath.section == 3)
    {
        return 70.0;
    }
    else
        return 40.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    switch (section)
    {
        case 0:
            return 1;
            break;
            
        case 1:
            return 1;
            break;
            
        case 2:
            return isRegionalSelected?1:0;
            break;
            
        case 3:
            return isRegionalSelected?1:0;
            break;
            
        case 4:
            return _arrServiceFilter.count;
            break;
            
            
        default:
            break;
    }
    
    return 0;
}



-(void)btnClickedFromCellForFistSection:(UIButton*)sender{
    SDCapsuleButton *superVw = (SDCapsuleButton*)[sender superview];
    selectedFirstFilter = [superVw btnTitle];
    
    [_tblFilter reloadData];
    
}

-(void)btnClickedFromCellForSecondSection:(UIButton*)sender{
    SDCapsuleButton *superVw = (SDCapsuleButton*)[sender superview];
    selectedSecondFilter = [superVw btnTitle];
    if ([selectedSecondFilter isEqualToString:NSLocalizedString(@"regional", nil)]) {
        isRegionalSelected = YES;
    }
    else{
        isRegionalSelected = NO;
    }
    
    [_tblFilter reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"SortCellIdentifier";
        SortFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat xCord = 10;
        CGFloat yCord = 13;
        
        CGFloat width = 100;
        CGFloat height = 75;
        
        CGFloat screenWidth = self.tblFilter.frame.size.width - 2*xCord;
        CGFloat paddingWidth = (screenWidth/2 - width/2);
        for (int i = 0 ;  i < arrFirstFilterItems.count ; i++) {
            
            NSMutableDictionary *dictItem = arrFirstFilterItems[i];
            SDCapsuleButton *btnFilter = [[SDCapsuleButton alloc] initWithFrameForFilterButtons:CGRectMake(xCord, yCord, width, height) withTitle:[dictItem objectForKey:@"Title"] withNormalImage:[dictItem objectForKey:@"NormalState"] andSelectedImage:[dictItem objectForKey:@"SelectedState"]];
            [cell.contentView addSubview:btnFilter];
            [btnFilter.btnMain addTarget:self action:@selector(btnClickedFromCellForFistSection:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([selectedFirstFilter isEqualToString:[dictItem objectForKey:@"Title"]]) {
                [btnFilter setSelected:YES];
            }
            else{
                [btnFilter setSelected:NO];
            }
            
            if (i==0) {
                xCord = paddingWidth;
            }
            else{
                xCord = screenWidth - width;
            }
        }
        
        
        return cell;
        
    }
    else if (indexPath.section == 1)
    {
        static NSString *CellIdentifierNew = @"SortCellIdentifier";
        SortFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierNew];
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat xCord = 10;
        CGFloat yCord = 13;
        
        CGFloat width = 100;
        CGFloat height = 75;
        
        CGFloat screenWidth = tableView.frame.size.width - 2*xCord;
        CGFloat paddingWidth = (screenWidth/2 - width/2);
        for (int i = 0 ;  i < arrSecondFilterItems.count ; i++) {
            
            NSMutableDictionary *dictItem = arrSecondFilterItems[i];
            SDCapsuleButton *btnFilter = [[SDCapsuleButton alloc] initWithFrameForFilterButtons:CGRectMake(xCord, yCord, width, height) withTitle:[dictItem objectForKey:@"Title"] withNormalImage:[dictItem objectForKey:@"NormalState"] andSelectedImage:[dictItem objectForKey:@"SelectedState"]];
            [cell.contentView addSubview:btnFilter];
            
            [btnFilter.btnMain addTarget:self action:@selector(btnClickedFromCellForSecondSection:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([selectedSecondFilter isEqualToString:[dictItem objectForKey:@"Title"]]) {
                [btnFilter setSelected:YES];
            }
            else{
                [btnFilter setSelected:NO];
            }
            
            
            if (i==0) {
                xCord = paddingWidth;
            }
            else{
                xCord = screenWidth - width;
            }
        }
        
        
        return cell;
        
    }
    
    else if (indexPath.section == 2 || indexPath.section == 3)
    {
        
        
        static NSString *cellIdentifierState = @"StateNameCell";
        StateCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifierState];
        cell.textLabel.text = @"";
        // Remove Previous contents
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        if (indexPath.section == 2) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            // Add Capsule Buttons for Selected States
            [self designCellContentForStateNamesOnCelView:cell.contentView];
        }
        else{
            
            UIView *tempVw = [[UIView alloc] initWithFrame:CGRectMake(10, 10, CGRectGetWidth(self.tblFilter.frame) - 20, 50)];
            tempVw.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5].CGColor;
            tempVw.layer.borderWidth = 1.0;
            tempVw.layer.cornerRadius  = 2.0;
            tempVw.clipsToBounds = YES;
            tempVw.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:tempVw];
            
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.textLabel.text = NSLocalizedString(@"select_state", nil);
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.textLabel.font = [AppFont regularFont:14];

        return cell;
        
    }
    
    else
    {
        
        static NSString *CellIdentifierType = @"TypeCellIdentifier";
        FilterCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
        [cell.btnCheckUncheck addTarget:self action:@selector(btnCheckboxClickedForServices:) forControlEvents:UIControlEventTouchUpInside];
        
        //        FilterServicesBO *objCategory = [_arrServiceFilter objectAtIndex:indexPath.row];
        //        cell.lblType.text = objCategory.serviceName;
        //
        //
        //
        //        cell.btnCheckUncheck.tag = 1000+indexPath.row;
        //        cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        //        [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateNormal];
        //
        //          if ([objCategory.serviceName isEqualToString:_selectedNotificationType])
        //          {
        //              [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"icon_radio_button_select.png"] forState:UIControlStateNormal];
        //
        //          }
        //          else{
        //              // cell.btnCheckUncheck.backgroundColor = [UIColor colorWithRed:46.0/255.0 green:156.0/255.0 blue:78.0/255.0 alpha:1.0];
        //              [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"icon_radio_button.png"] forState:UIControlStateNormal];
        //
        //          }
        //
        //        if (objCategory.isServiceSelected)
        //        {
        //            [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateNormal];
        //        }
        //        else
        //        {
        //            [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
        //
        //        }
        cell.lblType.font = [AppFont regularFont:15.0];

        return cell;
        
    }
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[FilterCell class]]) {
        
        FilterCell *cellFilter = (FilterCell*)cell;
        
        
        FilterServicesBO *objCategory = [_arrServiceFilter objectAtIndex:indexPath.row];
        cellFilter.lblType.text = objCategory.serviceName;
        
        
        cellFilter.btnCheckUncheck.tag = 1000+indexPath.row;
        cellFilter.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        [cellFilter.btnCheckUncheck setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
        
        if (objCategory.isServiceSelected)
        {
            [cellFilter.btnCheckUncheck setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cellFilter.btnCheckUncheck setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
            
        }
        
        
        
        CGRect labelType =  cellFilter.lblType.frame;
        labelType.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
        cellFilter.lblType.frame = labelType;
        
        CGRect btnCheck =  cellFilter.btnCheckUncheck.frame;
        btnCheck.origin.x = singleton.isArabicSelected ? 10 : CGRectGetWidth(_tblFilter.frame) - 35;
        cellFilter.btnCheckUncheck.frame = btnCheck;
    }
}




-(void)btnCheckboxClickedForServices:(UIButton*)btnSender
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btnSender.tag-1000 inSection:4];
    [self checkUncheckFilterOptions:indexPath];
}


//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
    }
    else if (indexPath.section == 3){
        
        [self showStatePicker];
    }
    else if (indexPath.section == 4){
        
        [self checkUncheckFilterOptions:indexPath];
    }
}



-(void)checkUncheckFilterOptions:(NSIndexPath*)indexPath
{
    FilterServicesBO *objFilter = nil;
    
    if (indexPath.section == 4){
        objFilter = _arrServiceFilter[indexPath.row];
    }
    
    objFilter.isServiceSelected = !objFilter.isServiceSelected;
    
    [self.tblFilter reloadData];
    
    
}

//- (IBAction)btnDoneClicked:(id)sender {
//
//    [self.navigationController popViewControllerAnimated:YES];
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

-(CGFloat)heightForDynamicCell{
    
    CGFloat xCord = 10;
    CGFloat heightPadding = 45;
    CGFloat padding = 10;
    
    if ([_arrStates count] == 0) {
        return 0.0;
    }
    else
    {
        CGFloat height = 60.0;
        for (int i = 0; i<_arrStates.count; i++)
        {
            NSString *stateName = _arrStates[i];
            CGFloat screenWidth = _tblFilter.frame.size.width;
            
            CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 30.0)];
            frame.size.width = frame.size.width + 40;
            
            CGFloat rightMargin = self.tblFilter.frame.size.width - xCord;
            if (rightMargin < frame.size.width) {
                xCord = 10;
                height+=heightPadding;
                NSLog(@"New Item, Height Increased = %lf",height);
            }
            
            xCord+=frame.size.width + padding;
        }
        
        return height;
    }
    
}

-(void)designCellContentForStateNamesOnCelView:(UIView*)contentView{
    
    
    CGFloat xCord = 10;
    CGFloat yCord = 15;
    CGFloat padding = 10;
    CGFloat heightPadding = 45;
    for (int i = 0; i<_arrStates.count; i++) {
        
        NSString *stateName = _arrStates[i];
        
        CGFloat screenWidth = _tblFilter.frame.size.width;
        
        CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 30.0)];
        frame.size.width = frame.size.width + 40;
        
        CGFloat rightMargin = self.tblFilter.frame.size.width - xCord;
        if (rightMargin < frame.size.width)
        {
            xCord = 10;
            yCord+=heightPadding;
        }
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrame:CGRectMake(xCord, yCord, frame.size.width, 30)];
        [btn setBtnTitle:stateName];
        [btn addTarget:self action:@selector(btnCapsuleStateClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 2300+i;
        [contentView addSubview:btn];
        
        if ([stateName isEqualToString:NSLocalizedString(@"all", nil)])
        {
            btn.btnCross.hidden = YES;
        }
        else
        {
            btn.btnCross.hidden = NO;
        }
        
        xCord+=frame.size.width + padding;
    }
}


-(void)btnCrossClicked:(UIButton*)btnCross{
    //    SDCapsuleButton *btn = (SDCapsuleButton*)[btnCross superview];
    //    NSInteger row = btn.tag - 2300;
    //    if (row < _arrServiceFilter.count) {
    //        [_arrServiceFilter removeObjectAtIndex:row];
    //        [_tblFilter reloadData];
    //    }
    
    
    
    SDCapsuleButton *btn = (SDCapsuleButton*)[btnCross superview];
    NSInteger row = btn.tag - 2300;
    if (row < _arrStates.count) {
        [_arrStates removeObjectAtIndex:row];
        [_tblFilter reloadData];
        
    }
    if (_arrStates.count == 0)
    {
        [_arrStates addObject:NSLocalizedString(@"all", nil)];
        
        [_tblFilter reloadData];
    }
}


-(void)btnCapsuleStateClicked:(SDCapsuleButton*)btnState{
    NSInteger row = btnState.tag - 2300;
    NSLog(@"State Clicked : %li",(long)row);
}

- (IBAction)btnAlbhabatic:(id)sender
{
    
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:NO];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [_btnBackHome.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btnReset.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _lblFilter.font = [AppFont semiBoldFont:17];
    [btnApply.titleLabel setFont:[AppFont regularFont:20.0]];
}

- (IBAction)btnbackClicked:(id)sender
{
    
    //   [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btnApplyClicked:(id)sender
{
    NSLog(@"Apply Pressed");
    
    // Get Filtered Content Here
    NSMutableDictionary *dictFilterOptions = [NSMutableDictionary new];
    
    // First Fetch all categories
    NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.isServiceSelected == YES"];
    NSArray *arrServices = [_arrServiceFilter filteredArrayUsingPredicate:predicateFilter];
    
    NSMutableArray *arrSelectedCategories = [NSMutableArray new];
    if ([arrServices count])
    {
        for (int i = 0; i<arrServices.count; i++) {
            FilterServicesBO *objFilterCateogry = arrServices[i];
            [arrSelectedCategories addObject:objFilterCateogry.serviceName];
        }
        [dictFilterOptions setObject:arrSelectedCategories forKey:@"category_type"];
    }
    // Now fetch all states
    
    if ([selectedSecondFilter isEqualToString:NSLocalizedString(@"regional", nil)])
    {
        if ([_arrStates count])
        {
            [dictFilterOptions setObject:_arrStates forKey:@"state_name"];
        }
        
    }
    else
    {
        
    }
    // Fetch filter data
    
    // NSString *sortBy = [selectedFirstFilter stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *sortBy =[NSString stringWithFormat:@"%@",selectedFirstFilter ];
    
    if (sortBy) {
        [dictFilterOptions setObject:sortBy forKey:@"sort_by"];
    }
    
    
    //NSString *serviceType = [selectedSecondFilter stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *serviceType = selectedSecondFilter;
    
    if (serviceType)
    {
        
        [dictFilterOptions setObject:serviceType forKey:@"service_type"];
        
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HomeFilterResultsVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeFilterResultsVC"];
    vc.dictFilterParams = dictFilterOptions;
    vc.isFromHomeFilter = NO;
    vc.isFromStateFilter = YES;
    
    vc.delegate = self;
    vc.filterBOArray = [NSMutableArray new];
    
    vc.filterBOArray = _arrServiceFilter;
    
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
    /*[self.navigationController pushViewController:vc animated:YES];
     SearchFilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SearchFilterVC"];
     vc.dictFilterParams = dictFilterOptions;
     vc.isFromHomeFilter = YES;
     vc.hidesBottomBarWhenPushed = YES;
     [self.navigationController pushViewController:vc animated:YES];*/
}


- (IBAction)btnResetClicked:(id)sender
{
    
    selectedSecondFilter = NSLocalizedString(@"all", nil);
    selectedFirstFilter = NSLocalizedString(@"alphabetic", nil);
    if ([selectedSecondFilter isEqualToString:NSLocalizedString(@"regional", nil)]) {
        isRegionalSelected = YES;
    }
    else{
        isRegionalSelected = NO;
    }
    
    [arrStates removeAllObjects];
    
    [_arrServiceFilter setValue:@NO forKey:@"isServiceSelected"];
    [arrStates setValue:@NO forKey:@"isServiceSelected"];
    
    
    
    
    [self.tblFilter reloadData];
    
}



#pragma mark- State Picker

-(void)showStatePicker
{
    arry_state = [objStateList getStateList];
    
    arry_state = [arry_state sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"profile_state", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"done", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowRadioButtons = YES;
    picker.isClearOptionRequired = NO;
    picker.allowMultipleSelection = YES;
    [picker show];
    
    // [picker setSelectedRows:arrSelectedItemForPicker];
    
}
- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row{
    
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:arry_state[row]
                               attributes:@{
                                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                                            }];
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    return arry_state[row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView {
    return arry_state.count;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows {
    [_arrStates removeAllObjects];
    
    [arrSelectedItemForPicker removeAllObjects];
    [arrSelectedItemForPicker addObjectsFromArray:rows];
    
    for (NSNumber *n in rows) {
        NSInteger row = [n integerValue];
        NSLog(@"%@ is chosen!", arry_state[row]);
        [_arrStates addObject:arry_state[row]];
    }
    if ([_arrStates count] == [arry_state count] || [_arrStates count] == 0) {
        [_arrStates removeAllObjects];
        [_arrStates addObject:NSLocalizedString(@"all", nil)];
    }
    
    
    
    [_tblFilter reloadData];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    [_tblFilter reloadData];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    NSLog(@"Canceled.");
    if (singleton.stateSelected.length == 0 || [_arrStates count] == 0) {
        [_arrStates removeAllObjects];
        [_arrStates addObject:NSLocalizedString(@"all", nil)];
    }else {
        
    }
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/


@end
