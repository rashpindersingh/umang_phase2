
//
//  AadharCardViewCon.m
//  Umang
//
//  Created by admin on 08/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.




#import "AadharCardViewCon.h"
#import "AadharDataBO.h"
#import "AadharDetailInfoCell.h"
#import "AadharDetailProfileCell.h"
#import "MoreTabVC.h"
#import "AadharHelpLinkVC.h"

#import "HelpViewController.h"
#import "AadharRegViaNotLinkModuleVC.h"
#import "UpdMpinVC.h"
#import "NotLinkedAadharVC.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "newAadharDetailTableViewCell.h"


@interface AadharCardViewCon ()<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *arrAadharData;
    newAadharDetailTableViewCell  *headerProfileCell;
    __weak IBOutlet UILabel *lblHeaderTitle;
    
    __weak IBOutlet UIButton *btnMore;
    UpdMpinVC *updMPinVC;
    MBProgressHUD *hud;
    
    BOOL flagRefresh;
    
    BOOL flagToggle;
    NSMutableArray *Langarray;
    UIRefreshControl *refreshController;
    
}
@property(nonatomic,retain)NSMutableDictionary *lable_dic;
@property(nonatomic,retain)NSMutableDictionary *localvalue_dic;


@end

@implementation AadharCardViewCon
@synthesize tagFrom;


-(IBAction)btnAddmoreInfo:(id)sender
{
    
    /*UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
     delegate:self
     cancelButtonTitle:NSLocalizedString(@"cancel", nil)
     destructiveButtonTitle:nil
     otherButtonTitles:NSLocalizedString(@"refresh", nil),NSLocalizedString(@"re_synk", nil),NSLocalizedString(@"unlink_aadhaar", nil),NSLocalizedString(@"help", nil), nil];
     
     [actionSheet showInView:self.view];*/
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"re_synk", nil),NSLocalizedString(@"unlink_aadhaar", nil),NSLocalizedString(@"help", nil), nil];
    
    [actionSheet showInView:self.view];
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (buttonIndex ==0) {
        NSLog(@"Re-Sync");
        
        [self resyncMethod];
        
        
    }
    if (buttonIndex ==1) {
        NSLog(@"Unlink");
        
        [self UnlinkMethod];
        
        
    }
    if (buttonIndex ==2) {
        NSLog(@"Help");
        
        [self HelpMethod];
        
    }
    
    
    /*  if (buttonIndex ==0) {
     NSLog(@"Refresh");
     [self refreshview];
     
     }
     if (buttonIndex ==1) {
     NSLog(@"Re-Sync");
     
     [self resyncMethod];
     
     
     }
     if (buttonIndex ==2) {
     NSLog(@"Unlink");
     
     [self UnlinkMethod];
     
     
     }
     if (buttonIndex ==3) {
     NSLog(@"Help");
     
     [self HelpMethod];
     
     }
     
     */
    // [actionSheet buttonTitleAtIndex:buttonIndex]
    
    
}

//----Unlink Method------------

-(void)HelpMethod
{
    NSLog(@"Inside Help Method");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSLog(@"My Help Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AadharHelpLinkVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharHelpLinkVC"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}



//----Unlink Method------------

-(void)UnlinkMethod
{
    
    
    NSLog(@"Inside Unlink Method");
    NSString *msg = [NSString stringWithFormat:@"%@\n\n%@",NSLocalizedString(@"aadhaar_unlink_sub_heading", nil),NSLocalizedString(@"delete_profile_txt3",nil)];
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"aadhaar_unlink_heading", nil)                         message:msg
                          delegate:self
                          cancelButtonTitle:nil
                          otherButtonTitles:NSLocalizedString(@"no", nil),NSLocalizedString(@"yes", nil), nil];
    alert.tag = 1010;
    [alert show];
    
    
    // [self openMPINview];
    
}

-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1010)
    { // handle the altdev
        if (buttonIndex==1)
        {
            [self openMPINview];
            
        }
    }
}


-(void)openMPINview
{
    [self displayContentController:[self getHomeDetailLayerLeftController]];
    
}



-(UpdMpinVC*)getHomeDetailLayerLeftController
{
    SharedManager *singleton = [SharedManager sharedSingleton];
    NSMutableDictionary* dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"fbid"];//
    [dictBody setObject:@"" forKey:@"goid"];//
    [dictBody setObject:@"" forKey:@"twitid"];//
    [dictBody setObject:@"aadhaar" forKey:@"type"];//
    [dictBody setObject:@"" forKey:@"mno"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //token
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:singleton.objUserProfile.objAadhar.aadhar_number
                 forKey:@"aadhr"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    updMPinVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdMpinVC"];
    updMPinVC.dic_info=dictBody;//change it to URL on demand
    updMPinVC.TAG_FROM=@"UNLINKAADHAAR";
    return updMPinVC;
}



- (void) displayContentController:(UIViewController*)content;
{
    // UIViewController *vc=[self topMostController];
    
    [self addChildViewController:content];
    
    content.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide{
    // UIViewController *vc=[self topMostController];
    
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        updMPinVC.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    //  UIViewController *vc=[self topMostController];
    //if (itemMoreVC == nil) {
    updMPinVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        updMPinVC.view.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}

//----resynchMethod------------
-(void)resyncMethod
{
    NSLog(@"Inside resync Method");
    SharedManager *singleton = [SharedManager sharedSingleton];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AadharRegViaNotLinkModuleVC *vc;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        vc = [[AadharRegViaNotLinkModuleVC alloc] initWithNibName:@"AadharRegViaNotLinkModuleVC_iPad" bundle:nil];
    }
    else
    {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharRegViaNotLinkModuleVC"];
    }
    
    
    vc.str_aadhaar_Value=singleton.objUserProfile.objAadhar.aadhar_number;
    vc.tagFrom=@"RESYNCAADHAARCARD";
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    //AadharRegViaNotLinkModuleVC_iPad
    
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitFetchProfileAPI
{
    
    SharedManager *singleton = [SharedManager sharedSingleton];
    
    if (flagRefresh==FALSE)
    {
        //no loader
    }
    else
    {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        // Set the label text.
        hud.label.text = NSLocalizedString(@"loading",nil);
    }
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
    
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        if (flagRefresh==FALSE)
        {
            
        }
        else
        {
            [hud hideAnimated:YES];
            
        }
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                
                
                NSLog(@"Inside refresh method");
                
                
                if (singleton.objUserProfile.objAadhar.aadhar_number.length)
                {
                    NSLog(@"data found");
                    [self getAadhaarData];
                    
                }
                else
                {
                    
                    //[self NotLinkedAadharVC];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    
                }
                singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                
                NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                
                if ([abbreviation length]==0)
                {
                    abbreviation=@"";
                }
                
                NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                emblemString = emblemString.length == 0 ? @"":emblemString;
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
                
                //-------- Add later----------
                singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                //-------- Add later----------
                
                self.lable_dic=(NSMutableDictionary*)[[response valueForKey:@"pd"] valueForKey:@"aadharpdlbl"];
                
                //------- Save to locak
                [[NSUserDefaults standardUserDefaults] setObject:self.lable_dic forKey:@"LABLE_DIC"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                
                //uidai_pia_gender
                self.localvalue_dic=[NSMutableDictionary new];
                
                //------------- local data set for value----------
                NSString* uidai_ldata_name=[NSString stringWithFormat:@"%@",[[[response valueForKey:@"pd"] valueForKey:@"aadharpd"] valueForKey:@"uidai_ldata_name"]];
                if (uidai_ldata_name.length==0) {
                    uidai_ldata_name=@"";
                }
                [self.localvalue_dic setValue:uidai_ldata_name forKey:@"uidai_ldata_name"];
                
                
                NSString* uidai_ldata_co=[NSString stringWithFormat:@"%@",[[[response valueForKey:@"pd"] valueForKey:@"aadharpd"] valueForKey:@"uidai_ldata_co"]];
                if (uidai_ldata_co.length==0) {
                    uidai_ldata_co=@"";
                }
                [self.localvalue_dic setValue:uidai_ldata_co forKey:@"uidai_ldata_co"];
                
                
                
                
                NSString* consent_status=[NSString stringWithFormat:@"%@",[[[response valueForKey:@"pd"] valueForKey:@"aadharpd"] valueForKey:@"consent_status"]];
                if (consent_status.length==0) {
                    consent_status=@"";
                }
                [self.localvalue_dic setValue:consent_status forKey:@"consent_status"];
                
                
                //------ set language local------
                
                NSString* uidai_ldata_lang=[NSString stringWithFormat:@"%@",[[[response valueForKey:@"pd"] valueForKey:@"aadharpd"] valueForKey:@"uidai_ldata_lang"]];
                if (uidai_ldata_lang.length==0) {
                    uidai_ldata_lang=@"en";
                    
                    
                }
                NSArray *filtered = [Langarray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(shortName == %@)", uidai_ldata_lang]];
                
                NSString*LangName;
                if([filtered count]>0)
                {
                    LangName=[[filtered objectAtIndex:0] valueForKey:@"lanugageName"];
                    
                }
                if ([LangName length]==0) {
                    LangName=NSLocalizedString(@"english", nil);
                }
                
                // lanugageName = English;
                // shortName = en;
                
                
                [self.localvalue_dic setValue:LangName forKey:@"uidai_ldata_lang"];
                
                
                
                
                
                
                //------- Save to locak
                [[NSUserDefaults standardUserDefaults] setObject:self.localvalue_dic forKey:@"LOCALVALUE_DIC"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                
                if ([singleton.user_id length]==0) {
                    singleton.user_id=@"";
                }
                //------------------------- Encrypt Value------------------------
                // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                //[defaults setObject:singleton.user_id forKey:@"USER_ID"];
                
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                
                // flagToggle=TRUE;
                [self getAadhaarData];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:NSLocalizedString(@"ok", nil)
             otherButtonTitles:nil];
             [alert show];
             */
            
            NSString *errormsg=[NSString stringWithFormat:@"%@",error.localizedDescription];
            [self showToast:errormsg];
        }
        
    }];
    
}

-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}

-(void)prepareTempDataForLanguageType
{
    
    NSMutableArray * arrLanguageNames =[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"english", nil),NSLocalizedString(@"hindi", nil),NSLocalizedString(@"assamese", nil),NSLocalizedString(@"bengali", nil),NSLocalizedString(@"gujarati", nil),NSLocalizedString(@"kannada", nil),NSLocalizedString(@"malayalam", nil),NSLocalizedString(@"marathi", nil),NSLocalizedString(@"oriya", nil),NSLocalizedString(@"punjabi", nil),NSLocalizedString(@"tamil", nil),NSLocalizedString(@"telugu", nil),NSLocalizedString(@"urdu", nil), nil];
    
    
    // NSArray *smallName = @[@"en",@"hi-IN",@"as-IN",@"bn-IN",@"gu-IN",@"kn-IN",@"ml-IN",@"mr-IN",@"or-IN",@"pa-IN",@"ta-IN",@"te-IN",@"ur-IN"];
    
    NSArray *smallName = @[@"en",@"hi",@"as",@"bn",@"gu",@"kn",@"ml",@"mr",@"or",@"pa",@"ta",@"te",@"ur"];
    Langarray=[[NSMutableArray alloc]init];
    
    
    for (int i=0; i<[arrLanguageNames count]; i++)
    {
        
        NSString *lanugageName=[NSString stringWithFormat:@"%@",[arrLanguageNames objectAtIndex:i]];
        NSString *shortName=[NSString stringWithFormat:@"%@",[smallName objectAtIndex:i]];
        
        
        NSMutableDictionary *lanugagePair=[NSMutableDictionary new];
        
        [lanugagePair setValue:lanugageName forKey:@"lanugageName"];
        [lanugagePair setValue:shortName forKey:@"shortName"];
        
        [Langarray addObject:lanugagePair];
        
        
        
    }
    
    NSLog(@"Langarray=%@",Langarray);
    
}




//----Refresh Method------------

-(void)refreshview
{
    flagRefresh=TRUE;
    [self hitFetchProfileAPI];
}





-(void)viewWillAppear:(BOOL)animated
{
    
    self.lable_dic = (NSMutableDictionary*)[[[NSUserDefaults standardUserDefaults] objectForKey:@"LABLE_DIC"] mutableCopy];
    self.localvalue_dic = (NSMutableDictionary*)[[[NSUserDefaults standardUserDefaults] objectForKey:@"LOCALVALUE_DIC"] mutableCopy];
    
    if ([self.lable_dic objectForKey:@"test"]) {
        self.lable_dic=[NSMutableDictionary new];
    }
    if ([self.localvalue_dic objectForKey:@"test"])
    {
        self.localvalue_dic=[NSMutableDictionary new];
    }
    
    
    NSLog(@"self.lable_dic=%@",self.lable_dic);
    
    NSLog(@"self.localvalue_dic =%@",self.localvalue_dic );
    
    //——————— Add to handle portrait mode only———
    
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    
    
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    [self getAadhaarData];
    
    
    //[[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    
    if (!refreshController)
    {
        refreshController = [[UIRefreshControl alloc] init];
        //refreshController.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
        [refreshController addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventValueChanged];
        //self.refreshController = refresh;
        [tbl_aadhaarDetail addSubview:refreshController];
        // [self getEvents:refreshController];
    }
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:YES];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnMore.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeaderTitle.font = [AppFont semiBoldFont:17.0];
    
    //lblHeaderDescriptiom.font = [AppFont semiBoldFont:17.0];
    
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}

- (void)refreshTable:(UIRefreshControl *)refresh
{
    static BOOL refreshInProgress = NO;
    
    if (!refreshInProgress)
    {
        
        
        
        refreshInProgress = YES;
        NSLog(@"Refresh");
        [self refreshview];
        [refresh endRefreshing];
        refreshInProgress = NO;
        
        
        
    }
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

//- (void)orientationChanged:(NSNotification *)notification{
//    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
//}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation
{
    
    //[self drawTour:[tourdic valueForKey:@"tour"]];
    
    [self getAadhaarData];
    
}



-(void)getAadhaarData
{
    
    arrAadharData = [[NSMutableArray alloc]init];
    
    tbl_aadhaarDetail.dataSource = self;
    tbl_aadhaarDetail.delegate = self;
    
    [self prepareTempDataForAadharCard];
    
    tbl_aadhaarDetail.tableHeaderView = [self designAadharProfileView];
    
    [tbl_aadhaarDetail reloadData];
    
}


-(void)prepareTempDataForAadharCard
{
    
    SharedManager *sharedObject = [SharedManager sharedSingleton];
    
    AadharDataBO *objAadharData = [[AadharDataBO alloc] init];
    
    NSString *languagelocal=[NSString stringWithFormat:@"%@",[self.localvalue_dic valueForKey:@"uidai_ldata_lang"]];
    
    if (languagelocal.length<=0||[languagelocal isEqualToString:@"(null)"]) {
        languagelocal= @"";
    }
    headerProfileCell.lbllangLocal.text=languagelocal;
    
    
    if (flagToggle==TRUE) //here means set local data
    {
        
        NSString *title=[NSString stringWithFormat:@"%@",[self.lable_dic valueForKey:@"name"]];
        
        if (title.length<=0||[title isEqualToString:@"(null)"]) {
            title= NSLocalizedString(@"profile_name", nil);
        }
        objAadharData.title =  title;
        
        
        NSString *discription=[NSString stringWithFormat:@"%@", [self.localvalue_dic valueForKey:@"uidai_ldata_name"]];
        
        if (discription.length<=0||[discription isEqualToString:@"(null)"]) {
            discription=  sharedObject.objUserProfile.objAadhar.name;
        }
        
        
        objAadharData.titleDescription = discription;
        
        
    }
    else
    {
        
        // objAadharData.title =  NSLocalizedString(@"profile_name", nil);
        
        objAadharData.title =  @"Name";
        
        objAadharData.titleDescription = sharedObject.objUserProfile.objAadhar.name;
        
    }
    
    
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    if (flagToggle==TRUE) //here means set local data
    {
        
        
        NSString *title=[NSString stringWithFormat:@"%@",[self.lable_dic valueForKey:@"dob"]];
        
        if (title.length<=0||[title isEqualToString:@"(null)"]) {
            title= NSLocalizedString(@"profile_dob", nil);
        }
        objAadharData.title =  title;
        
        
    }
    else
    {
        //objAadharData.title =  NSLocalizedString(@"profile_dob", nil);
        
        objAadharData.title =  @"Date of Birth";
        
    }
    objAadharData.titleDescription = sharedObject.objUserProfile.objAadhar.dob;
    
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    
    
    
    @try {
        NSString *genderValue;
        
        if (flagToggle==TRUE) //here means set local data
        {
            
            
            
            if ([sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"M"]||[sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"m"]|| [sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"MALE"]){
                objAadharData.titleDescription = NSLocalizedString(@"gender_male", nil);
                genderValue= NSLocalizedString(@"gender_male", nil);
            }
            if ([sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"F"]||[sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"f"]||[sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"FEMALE"]) {
                objAadharData.titleDescription =NSLocalizedString(@"gender_female", nil);
                genderValue= NSLocalizedString(@"gender_female", nil);
                
            }
            if ([sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"T"]||[sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"t"]) {
                objAadharData.titleDescription = NSLocalizedString(@"gender_transgender", nil);
                genderValue= NSLocalizedString(@"gender_transgender", nil);
                
            }
            
            
            NSString *title=[NSString stringWithFormat:@"%@",[self.lable_dic valueForKey:@"gender"]];
            
            if (title.length<=0||[title isEqualToString:@"(null)"]) {
                title= NSLocalizedString(@"profile_gender", nil);
            }
            objAadharData.title =  title;
            
            
            NSString *discription=[NSString stringWithFormat:@"%@", [self.lable_dic valueForKey:@"gen"]];
            
            if (discription.length<=0||[discription isEqualToString:@"(null)"]) {
                discription= genderValue;
            }
            
            
            objAadharData.titleDescription = discription;
            
            
            
        }
        else
        {
            if ([sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"M"]||[sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"m"]|| [sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"MALE"]){
                objAadharData.titleDescription = NSLocalizedString(@"gender_male", nil);
                // genderValue= NSLocalizedString(@"gender_male", nil);
                genderValue= @"Male";
                
            }
            if ([sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"F"]||[sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"f"]||[sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"FEMALE"]) {
                objAadharData.titleDescription =NSLocalizedString(@"gender_female", nil);
                //genderValue= NSLocalizedString(@"gender_female", nil);
                genderValue= @"Female";
                
            }
            if ([sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"T"]||[sharedObject.objUserProfile.objAadhar.gender isEqualToString:@"t"]) {
                objAadharData.titleDescription = NSLocalizedString(@"gender_transgender", nil);
                genderValue= @"Other";
                
            }
            //objAadharData.title =  NSLocalizedString(@"profile_gender", nil);
            objAadharData.title =  @"Gender";
            objAadharData.titleDescription = genderValue;
            
        }
        
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    
    objAadharData = [[AadharDataBO alloc] init];
    
    if (flagToggle==TRUE) //here means set local data
    {
        
        
        
        
        
        NSString *title=[NSString stringWithFormat:@"%@",[self.lable_dic valueForKey:@"co"]];
        
        if (title.length<=0||[title isEqualToString:@"(null)"]) {
            title=  @"C/O";
        }
        objAadharData.title =  title;
        
        
        NSString *discription=[NSString stringWithFormat:@"%@", [self.localvalue_dic valueForKey:@"uidai_ldata_co"]];
        
        if (discription.length<=0||[discription isEqualToString:@"(null)"]) {
            discription=  sharedObject.objUserProfile.objAadhar.father_name;
        }
        
        
        objAadharData.titleDescription = discription;
        
        
        
    }
    else
    {
        objAadharData.title =  @"C/O";
        objAadharData.titleDescription = sharedObject.objUserProfile.objAadhar.father_name;
        
        
        
    }
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    
    
    if (flagToggle==TRUE) //here means set local data
    {
        
        
        
        
        NSString *title=[NSString stringWithFormat:@"%@",[self.lable_dic valueForKey:@"address"]];
        
        if (title.length<=0||[title isEqualToString:@"(null)"]) {
            title= NSLocalizedString(@"address", nil);
        }
        objAadharData.title =  title;
        
        
        NSString *discription=[NSString stringWithFormat:@"%@", [self.localvalue_dic valueForKey:@"consent_status"]];
        
        if (discription.length<=0||[discription isEqualToString:@"(null)"]) {
            discription=  sharedObject.objUserProfile.objAadhar.address;
        }
        
        
        objAadharData.titleDescription = discription;
        
        
        
        
        
    }
    else
    {
        //objAadharData.title =  NSLocalizedString(@"address", nil);
        objAadharData.title =  @"Address";
        
        objAadharData.titleDescription = sharedObject.objUserProfile.objAadhar.address;
        
    }
    
    
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    [tbl_aadhaarDetail reloadData];
    
    
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    NSInteger fontIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_FONTSIZE_INDEX"];
    
    dispatch_async(dispatch_get_main_queue(), ^
    {
        if ([view isKindOfClass:[UITextField class]])
        {
            UITextField *txtfield = (UITextField *)view;
            NSString *fonttxtFieldName = txtfield.font.fontName;
            
            txtfield.font = nil;
            
            float fontsize=15.0;
            
            if (fontIndex == 0)
            {
                fontsize=14.0;
            }
            else if (fontIndex == 1)
            {
                fontsize=16.0;
            }
            else if (fontIndex == 2)
            {
                fontsize=18.0;
            }
            
            txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fontsize];
        }
        
    });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        float fontsize=15.0;
        
        if (fontIndex == 0)
        {
            fontsize=14.0;
        }
        
        else if (fontIndex == 1)
        {
            fontsize=16.0;
        }
        
        else if (fontIndex == 2)
        {
            fontsize=18.0;
        }
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontsize];
        
    }
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        float fontsize=15.0;
        
        if (fontIndex == 0)
        {
            fontsize=14.0;
        }
        else if (fontIndex == 1)
        {
            fontsize=16.0;
        }
        else if (fontIndex == 2)
        {
            fontsize=18.0;
        }
        
        lbl.font = [UIFont fontWithName:fontName size:fontsize];
    }
    
    if ([view isKindOfClass:[UILabel class]])
    {
        
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        
        float fontsize=15.0;
        
        if (fontIndex == 0)
        {
            fontsize=14.0;
        }
        else if (fontIndex == 1)
        {
            fontsize=16.0;
        }
        else if (fontIndex == 2)
        {
            fontsize=18.0;
        }
        
        lbl.font = [UIFont fontWithName:fontName size:fontsize];
        
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        
        float fontsize=15.0;
        
        if (fontIndex == 0)
        {
            fontsize=14.0;
        }
        else if (fontIndex == 1)
        {
            fontsize=16.0;
        }
        else if (fontIndex == 2)
        {
            fontsize=18.0;
        }
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontsize]];
        
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

- (void)viewDidLoad
{
    
    [self prepareTempDataForLanguageType];
    
    
    flagRefresh=FALSE;
    
    
    
    [self hitFetchProfileAPI];
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: AADHAR_CARD_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    // Do any additional setup after loading the view from its nib.
    
    //    headerProfileCell.isAadhardetailWithoutPic = NO;
    //    headerProfileCell.isNotLinkedAadharCon = NO;
    lblHeaderTitle.text = NSLocalizedString(@"aadhaar", nil);
    [btnMore setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        // [btnBack sizeToFit];
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnMore.titleLabel.font.pointSize]}];
        
        //or whatever font you're using
        CGRect framebtn=CGRectMake(btnMore.frame.origin.x, btnMore.frame.origin.y, btnMore.frame.size.width, btnMore.frame.size.height);
        
        
        [btnMore setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnMore.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    
    [self getAadhaarData];
    
    
    
    self.navigationController.navigationBarHidden=true;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    /* NSString *toggleStatus = [[NSUserDefaults standardUserDefaults]
     stringForKey:@"ONOFFKEY"];
     
     if ([toggleStatus isEqualToString:@"ON"])
     {
     flagToggle=FALSE;
     
     }
     else
     {
     
     flagToggle=TRUE;
     
     }
     [self togleLanguage:self];
     */
    [super viewDidLoad];
    
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(newAadharDetailTableViewCell*)designAadharProfileView

{
    if (headerProfileCell == nil)
        
    {
        headerProfileCell = [[[NSBundle mainBundle] loadNibNamed:@"newAadharDetailTableViewCell" owner:self options:nil] objectAtIndex:0];
        headerProfileCell.TYPE_AADHAR_CHOOSEN = AADHARWITHPIC1;
        [headerProfileCell bindDataForHeaderView];
        [headerProfileCell.btn_switch addTarget:self action:@selector(togleLanguage:) forControlEvents:UIControlEventValueChanged];
        
        headerProfileCell.lbllangDefault.text=NSLocalizedString(@"english", nil);
        
        
        /*
         
         
         NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
         if (selectedLanguage == nil)
         {
         selectedLanguage = @"en";
         }
         else{
         NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
         selectedLanguage = [arrTemp firstObject];
         }
         
         
         */
        
        
        NSString *languagelocal=[NSString stringWithFormat:@"%@",[self.localvalue_dic valueForKey:@"uidai_ldata_lang"]];
        if (languagelocal.length<=0 || [languagelocal isEqualToString:@"(null)"]) {
            languagelocal=@"";
        }
        headerProfileCell.lbllangLocal.text=languagelocal;
        
        //[self.localvalue_dic setValue:consent_status forKey:@"uidai_ldata_lang"];
        
        
    }
    
    
    return headerProfileCell;
}


-(IBAction)togleLanguage:(id)sender
{
    
    
    /*
     
     consent_status =Address
     uidai_ldata_name =Name
     uidai_ldata_lang=language local
     uidai_ldata_co=C/O
     
     "aadharpdlbl": {
                 "name": "ਨਾਮ",
                 "dob": "ਜਨਮ ਦੀ ਮਿਤੀ",
                 "gender": "ਲਿੰਗ",
                 "co": "C/o",
                 "address": "ਪਤਾ",
                 "gen": "ਪੁਰਸ਼"
             }
     */
    NSString *valueToSave;
    if (flagToggle==FALSE)
    {
        flagToggle=TRUE;
        [headerProfileCell.btn_switch setOn:YES];
        [self getAadhaarData];
        
        valueToSave = @"ON";
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"ONOFFKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        
        
        flagToggle=FALSE;
        [headerProfileCell.btn_switch setOn:NO];
        
        valueToSave = @"OFF";
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"ONOFFKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self getAadhaarData];
        
        
    }
    if ([valueToSave isEqualToString:@"ON" ])
    {
        headerProfileCell.lbllangDefault.textColor = [UIColor grayColor];
        headerProfileCell.lbllangLocal.textColor = [UIColor blackColor];
    }
    else
    {
        headerProfileCell.lbllangDefault.textColor = [UIColor blackColor];
        headerProfileCell.lbllangLocal.textColor = [UIColor grayColor];
    }
    
    [tbl_aadhaarDetail reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.001;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    if (indexPath.row==4)
    {
        CGFloat retVal = 116.0;
        SharedManager *singleton = [SharedManager sharedSingleton];
        
        NSString *txtService = singleton.objUserProfile.objAadhar.aadhar_number;
        CGRect dynamicHeight = [self rectForText:txtService usingFont:[UIFont systemFontOfSize:17.0] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
        retVal = MAX(116, dynamicHeight.size.height) +20;
        
        //    NSLog(@"Height = %lf",retVal);
        
        return retVal;
        
    }
    else
        return 44.0;
    
}
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return arrAadharData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    static NSString *CellIdentifierNew = @"AadharDetailInfoCell";
    AadharDetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierNew];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    AadharDataBO  *objAadhar = arrAadharData [indexPath.row];
    cell.lblTitle.text = objAadhar.title;
    
    
    
    
    
    if (indexPath.row==4) {
        
        CGRect dynamicHeight = [self rectForText:objAadhar.titleDescription usingFont:[UIFont systemFontOfSize:14.0] boundedBySize:CGSizeMake(tbl_aadhaarDetail.frame.size.width-40, 1000.0)];
        
        
        CGRect frameBG = cell.frame;
        frameBG.size.height = MAX(116.0, dynamicHeight.size.height) + 40;
        // frameBG.origin.y = 30;
        //cell.frame = frameBG;
        
        CGRect frametxtfield=cell.lblTitleValue.frame;
        cell.lblTitleValue.frame=CGRectMake(frametxtfield.origin.x, frametxtfield.origin.y, frametxtfield.size.width, frameBG.size.height);
        
        
        cell.lblTitleValue.text = objAadhar.titleDescription;
        //  cell.lblTitleValue.backgroundColor=[UIColor greenColor];
        
        cell.lblTitleValue.textAlignment = NSTextAlignmentLeft;
        
        [cell.lblTitleValue setNumberOfLines:0];
        [cell.lblTitleValue sizeToFit];
    }
    else
    {
        cell.lblTitleValue.text = objAadhar.titleDescription;
        
    }
    cell.lblTitle.font = [AppFont regularFont:14.0];
    cell.lblTitleValue.font = [AppFont regularFont:14.0];
    return cell;
    
    
    
}
//083089
- (IBAction)btnMoreClicked:(id)sender
{
    
    if ([tagFrom isEqualToString:@"RegistrationView"]) {
        
        [(UINavigationController *)self.presentingViewController  popViewControllerAnimated:NO];
        [self dismissViewControllerAnimated:YES completion:nil];
        //[self dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
        // [self.navigationController popViewControllerAnimated:YES];
        
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[MoreTabVC class]]) {
                [self.navigationController popToViewController:aViewController animated:YES];
            }
        }
        
        
        
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}



/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
