//

//  TxnHistoryFilterVC.m

//  Umang

//

//  Created by Kuldeep Saini on 1/9/17.

//  Copyright © 2017 SpiceDigital. All rights reserved.

//



#import "TxnHistoryFilterVC.h"

#import "SDCapsuleButton.h"

#import "CZPickerView.h"

#import "MBProgressHUD.h"

#import "UMAPIManager.h"

#import "SelectCategoryVC.h"

#import "FilterServicesBO.h"

#import "NotificationItemBO.h"

#import "TxnFilterResultsVC.h"

#import "UIView+Toast.h"

@interface TxnHistoryFilterVC ()<UITableViewDelegate,UITableViewDataSource>

{
    
    
    
    NSString *selectedFirstFilter;
    NSString *selectedSecondFilter;
    
    NSMutableArray *arrFirstFilterItems;
    NSMutableArray *arrSecondFilterItems;
    
    MBProgressHUD *hud;
    
    NSInteger selectedOption;
    
    NSString *sortby;
    
    NSString *serviceName;
    NSString *categoryName;
    
    //    NSMutableArray *_arrSelectedCategories;
    
    //    NSMutableArray *_arrSelectedServices;
    
    
    __weak IBOutlet UIButton *btnApply;
    
    
    __weak IBOutlet UILabel *lblTransaction;
    __weak IBOutlet UIButton *btnBack;
    
    
    
    //   for service
    
    
    
    NSMutableArray *_arrServiceFilter;
    
    NSMutableArray *_arrCategoryFilter;
    
    
    
    NSMutableArray *_arrSelectedCategories;
    
    NSMutableArray *_arrSelectedServices;
    
    
    
    BOOL _isCategorySelected;
    
    SharedManager *singleton;
    
    
    IBOutlet UIButton *resetButton;
    
}

@property(nonatomic,retain)NSMutableArray *arrResponse;

@property(nonatomic,retain)NSString *page;

@end



@implementation TxnHistoryFilterVC
@synthesize arrResponse;
@synthesize page;

- (IBAction)transactionHistoryResetAction:(UIButton *)sender
{
    
    [singleton traceEvents:@"Reset Button" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

    serviceName=@"";
    categoryName=@"";
    page=@"1";
    sortby=@"alpha";
    [_arrSelectedCategories removeAllObjects];
    [_arrSelectedServices removeAllObjects];
    selectedFirstFilter = NSLocalizedString(@"alphabetic", nil);
    selectedSecondFilter = NSLocalizedString(@"filter_categories", nil);
    _isCategorySelected = YES;
    selectedOption = -1;
    [_tblFilter reloadData];
    
}


- (void)viewDidLoad {
    singleton = [SharedManager sharedSingleton];

    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: TRANSACTIONAL_FILTER_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    // sortby=@"date_time";
    sortby=@"alpha";
    serviceName=@"";
    categoryName=@"";
    page=@"1";
    _arrSelectedCategories = [NSMutableArray new];
    _arrSelectedServices = [NSMutableArray new];
    selectedFirstFilter = NSLocalizedString(@"alphabetic", nil);
    selectedSecondFilter = NSLocalizedString(@"filter_categories", nil);
    _isCategorySelected = YES;
    selectedOption = -1;
    
    
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [btnApply setTitle:[NSLocalizedString(@"apply_label", nil) uppercaseString] forState:UIControlStateNormal];
    
    lblTransaction.text = NSLocalizedString(@"trans_filter_screen", nil);
    
  
  
    
    
    [self prepareTableData];
    
    [self addFilterCategoryData];
    
    [self addNotificationServiceData];
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.tblFilter.frame = CGRectMake(80, self.tblFilter.frame.origin.y, self.view.frame.size.width - 160, self.tblFilter.frame.size.height);
        
        self.tblFilter.backgroundColor = [UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0];
        
        self.view.backgroundColor = self.tblFilter.backgroundColor;
    }
    
    [resetButton setTitle:NSLocalizedString(@"reset", nil) forState:UIControlStateNormal];
    [self addNavigationView];
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    [_tblFilter reloadData];
    [self setViewFont];
    [super viewWillAppear:NO];
    
}
#pragma mark- add Navigation View to View

-(void)addNavigationView
{
    btnBack.hidden = true;
    resetButton.hidden = true;
    // lblHeaderTransSearch.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] initRightBarButton];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btnBackClicked:btnBack];
    };
    nvView.lblTitle.text = NSLocalizedString(@"trans_filter_screen", "");
    [nvView.rightBarButton setTitle:NSLocalizedString(@"reset", nil) forState:UIControlStateNormal];
    nvView.didTapRightBarButton = ^(id btnReset) {
        [weakSelf transactionHistoryResetAction:(UIButton*)btnReset];
    };
    
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
//    if (iPhoneX()) {
//        CGRect tbFrame = self.tblFilter.frame;
//        tbFrame.origin.y = kiPhoneXNaviHeight
//        self.tblFilter.frame = tbFrame;
//        [self.view layoutIfNeeded];
//    }
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [resetButton.titleLabel setFont:[AppFont regularFont:17.0]];
    [btnApply.titleLabel setFont:[AppFont regularFont:20.0]];
    lblTransaction.font = [AppFont semiBoldFont:17.0];
}

#pragma mark -

-(void)hitAPIForApplyTransaction

{
    
    NSMutableArray*  categoryNameArray=[NSMutableArray new]; //its alloc with data of categories
    
    for (int i = 0; i<_arrSelectedCategories.count; i++)
        
    {
        
        FilterServicesBO *objFilter = _arrSelectedCategories[i];
        
        [categoryNameArray addObject:objFilter.serviceName];
    }
    
    NSMutableArray *catArray=[NSMutableArray new];
    for (int i=0;i<[categoryNameArray count]; i++)
    {
        
        NSString *categNametoPass=[categoryNameArray objectAtIndex:i];
        //NSString *cgidtoadd=[singleton.dbManager getServiceCategoryId:categNametoPass];
        NSString *cgidtoadd=[singleton.dbManager getMultiCategoryID:categNametoPass];

        if ([cgidtoadd length]>0) {
            [catArray addObject:cgidtoadd];
            
        }
    }
    NSString *cgid = [catArray componentsJoinedByString:@","];
    
    
    
    
    
    //------- Get Id for service All-------------------------------
    
    
    NSMutableArray *  serviceNameArray=[NSMutableArray new];//its alloc with data of service names
    for (int i = 0; i<_arrSelectedServices.count; i++)
        
    {
        NotificationItemBO *objFilter = _arrSelectedServices[i];
        [serviceNameArray addObject:objFilter.titleService];
    }
    
    NSMutableArray *sridArray=[NSMutableArray new];
    for (int i=0;i<[serviceNameArray count]; i++)
    {
        NSString *serviceNametoPass=[serviceNameArray objectAtIndex:i];
        NSString *newsrid=[singleton.dbManager getServiceId:serviceNametoPass];
        
        
        if ([newsrid length]>0) {
            [sridArray addObject:newsrid];
            
        }
    }
    NSString *srid = [sridArray componentsJoinedByString:@","]; //CSV value of string
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //serviceName
    //categoryName
    // NSString *cgid=categoryName;//[singleton.dbManager getServiceCategoryId:categoryName];
    //  NSString *srid=[singleton.dbManager getServiceId:serviceName];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    
    // Set the label text.
    
    
    
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:srid forKey:@"srid"];
    
    [dictBody setObject:cgid forKey:@"cgid"];
    
    
    
    [dictBody setObject:sortby forKey:@"sortby"];//(by default will be alpha and for date time send date_time)
    
    
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:@"" forKey:@"keywd"];
    [dictBody setObject:page forKey:@"page"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:@"" forKey:@"lang"];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    
    
    
    //"page": null,
    
    
    
    NSLog(@"Dict body is :%@",dictBody);
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_TRANSACTION_FILTER withBody:dictBody andTag:TAG_REQUEST_TRANSACTION_FILTER completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        
        [hud hideAnimated:YES];
        
        if (error == nil) {
            
            
            NSLog(@"Server Response = %@",response);
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                NSString *pages=[[response valueForKey:@"pd"] valueForKey:@"page"];
                
                NSArray *pageArray = [pages componentsSeparatedByString:@"|"];
                
                NSString*  temppage =[pageArray objectAtIndex:0];
                if ([temppage isEqualToString:@"0"])
                {
                    
                }
                else
                {
                    page=[pageArray objectAtIndex:1];
                }
                
                NSLog(@"page = %@",page);
                if (arrResponse == nil) {
                    arrResponse=[[NSMutableArray alloc]init];
                }else{
                    [arrResponse removeAllObjects];
                }
                [arrResponse addObjectsFromArray:[[response valueForKey:@"pd"] valueForKey:@"fetchUserService"]];
                
                [self openResultsView];
                
                
            }
            else{
                
                NSString *serverError = [response objectForKey:@"rd"];
                if ([serverError isKindOfClass:[NSString class]]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                    message:serverError
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                }
                NSLog(@"Error Occured = %@",error.localizedDescription);
                
            }
        }else{
            
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:NSLocalizedString(@"ok", nil)
             otherButtonTitles:nil];
             [alert show];
             */
            
            //  [self showToast:NSLocalizedString(@"network_error_txt",nil)];
            /* NSString *errormsg=[NSString stringWithFormat:@"%@",error.localizedDescription];
             [self showToast:errormsg];*/
            
            
            //added new -----
            arrResponse=[[NSMutableArray alloc]init];
            [self openResultsView];
            
        }
        
    }];
    
    
    
}



-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}



-(void)openResultsView
{
    [singleton traceEvents:@"Show Filter Result" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

    
    NSMutableDictionary *dictFilterOptions = [NSMutableDictionary new];
    
    NSString *sortBy = [sortby stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (sortBy) {
        [dictFilterOptions setObject:sortBy forKey:@"sort_by"];
    }
    
    
    NSMutableArray*  categoryNameArray=[NSMutableArray new]; //its alloc with data of categories
    
    for (int i = 0; i<_arrSelectedCategories.count; i++)
        
    {
        
        FilterServicesBO *objFilter = _arrSelectedCategories[i];
        
        [categoryNameArray addObject:objFilter.serviceName];
    }
    
    [dictFilterOptions setObject:categoryNameArray forKey:@"category_type"];
    
    NSMutableArray *  serviceNameArray=[NSMutableArray new];//its alloc with data of service names
    for (int i = 0; i<_arrSelectedServices.count; i++)
        
    {
        NotificationItemBO *objFilter = _arrSelectedServices[i];
        [serviceNameArray addObject:objFilter.titleService];
    }
    
    [dictFilterOptions setObject:serviceNameArray forKey:@"service_type"];
    
    
    
    
    //    NSString *serviceType = [serviceName stringByReplacingOccurrencesOfString:@" " withString:@""];
    //
    //    if (serviceType)
    //    {
    //
    //        [dictFilterOptions setObject:serviceType forKey:@"service_type"];
    //
    //    }
    //
    //
    //    NSString *categoryType = [categoryName stringByReplacingOccurrencesOfString:@" " withString:@""];
    //
    //    if (serviceType)
    //    {
    //
    //        [dictFilterOptions setObject:categoryType forKey:@"category_type"];
    //
    //    }
    //
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    TxnFilterResultsVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"TxnFilterResultsVC"];
    vc.dictFilterParams = dictFilterOptions;
    vc.arrTransactionHistory = arrResponse;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}



- (IBAction)btnApplyClicked:(id)sender

{
    [singleton traceEvents:@"Apply Button" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

    page=@"1";//add this paging
    
    
    [self hitAPIForApplyTransaction];
    /*
     
     
     UIAlertView *alertview =[[UIAlertView alloc]initWithTitle:@"Under Construction" message:@"This is under construction" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
     
     
     
     [alertview show];
     */
    
    
}



-(void)prepareTableData{
    
    
    
    arrFirstFilterItems = [NSMutableArray new];
    
    
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"alphabetic", nil) forKey:@"Title"];
    [dict setObject:@"icon_alphabatic.png" forKey:@"NormalState"];
    [dict setObject:@"icon_alphabatic_selected.png" forKey:@"SelectedState"];
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    
    selectedFirstFilter = NSLocalizedString(@"alphabetic", nil);
    
    
    //    selectedFirstFilter = NSLocalizedString(@"date_time", nil);
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"date_time", nil) forKey:@"Title"];
    [dict setObject:@"txndatetime" forKey:@"NormalState"];
    [dict setObject:@"txndatetime" forKey:@"SelectedState"];
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    
    
    
    arrSecondFilterItems = [NSMutableArray new];
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"filter_categories", nil) forKey:@"Title"];
    [dict setObject:@"icon_categories.png" forKey:@"NormalState"];
    [dict setObject:@"icon_categories_select.png" forKey:@"SelectedState"];
    [arrSecondFilterItems addObject:dict];
    dict = nil;
    
    selectedSecondFilter =NSLocalizedString(@"filter_categories", nil);
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"filter_services", nil) forKey:@"Title"];
    [dict setObject:@"icon_servies.png" forKey:@"NormalState"];
    [dict setObject:@"icon_servies_select.png" forKey:@"SelectedState"];
    [arrSecondFilterItems addObject:dict];
    dict = nil;
    
    
    //  [_tblFilter reloadData];
    
    
}



//add service data

-(void)addNotificationServiceData

{
    
    
    
    if (_arrServiceFilter == nil) {
        
        _arrServiceFilter = [[NSMutableArray alloc]init];
        
        
        
    }
    
    else
        
    {
        
        [_arrServiceFilter removeAllObjects];
        
    }
    
    
    
    NSArray *arrData  = [singleton.dbManager loadDataServiceData];
    
    
    
    for (int i = 0; i < arrData.count; i++)
        
    {
        
        NSDictionary *dict = [arrData objectAtIndex:i];
        
        NotificationItemBO *objSetting = [[NotificationItemBO alloc]init];
        
        objSetting.titleService = [dict valueForKey:@"SERVICE_NAME"];
        
        objSetting.imgService=[dict valueForKey:@"SERVICE_IMAGE"];
        
        
        
        //        objSetting.ID = [dict valueForKey:@"SERVICE_ID"];
        
        //        objSetting.statusTypeForServices = [dict valueForKey:@"SERVICE_IS_HIDDEN"];
        
        
        
        [_arrServiceFilter addObject: objSetting];
        
        
        
    }
    
    
    
    // [_tblFilter reloadData];
    
    
    
}





//add category Data

-(void)addFilterCategoryData{
    
    if (_arrCategoryFilter == nil) {
        
        _arrCategoryFilter = [NSMutableArray new];
        
    }
    
    else{
        
        [_arrCategoryFilter removeAllObjects];
        
    }
    
    
    
    NSArray *arrService = [singleton.dbManager loadServiceCategory];
    
    
    
    for (int i = 0; i< arrService.count; i++) {
        
        NSDictionary *dict = arrService [i];
        
        
        
        FilterServicesBO *objFilter = [[FilterServicesBO alloc]init];
        
        objFilter.serviceName = [dict valueForKey:@"SERVICE_CATEGORY"];
        
        [_arrCategoryFilter addObject:objFilter];
        
    }
    
    
    
    //  [_tblFilter reloadData];
    
}







- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented

{
    
    return 3;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 44.0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 0;
    
}



- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    static NSString *CellIdentifier = @"NotificationHeaderCell";
    
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (headerView == nil){
        
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
        
    }
    
    UILabel *lblSectionTitle = (UILabel*)[headerView viewWithTag:122];
    
    UIButton *btnClear = (UIButton*)[headerView viewWithTag:123];
    
    [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateSelected];
    [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateNormal];
    
    CGRect labelFrame =  lblSectionTitle.frame;
    labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
    lblSectionTitle.frame = labelFrame;
    btnClear.hidden = NO;
    
    
    
    if (section == 0) {
        
        lblSectionTitle.text = NSLocalizedString(@"sort_by", nil);
        
        btnClear.hidden = YES;
        
    }
    
    else if (section == 1)
        
    {
        
        lblSectionTitle.text =NSLocalizedString(@"filter_by", nil);
        btnClear.hidden = YES;
        
        //NSLocalizedString(@"categories", nil);
        
        //        if ([_arrSelectedCategories count])
        //        {
        //
        //            btnClear.hidden = NO;
        //
        //        }
        //
        //        else{
        //
        //            btnClear.hidden = YES;
        //
        //        }
        //
        //
        //
        //        [btnClear addTarget:self action:@selector(btnClearFilterCategoriesClicked) forControlEvents:UIControlEventTouchUpInside];
        //
    }
    
    else{
        if (_isCategorySelected) {
            lblSectionTitle.text = NSLocalizedString(@"cat_label", nil);
        }
        
        else
        {
            lblSectionTitle.text = NSLocalizedString(@"services", nil);
        }
        
        
        
        if ([_arrSelectedServices count]|| [_arrSelectedCategories count])
        {
            
            btnClear.hidden = NO;
            
        }
        
        else{
            
            btnClear.hidden = YES;
            
        }
        
        
        if ([_arrSelectedServices count]) {
            [btnClear addTarget:self action:@selector(btnClearFilterServicesClicked) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            [btnClear addTarget:self action:@selector(btnClearFilterCategoriesClicked) forControlEvents:UIControlEventTouchUpInside];
            
            
        }
        
        
        
        
    }
    
    
    
    headerView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    return headerView;
    
}// custom view for header. will be adjusted to default or specified header height





- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return nil;
    
}// custom view for footer. will be adjusted to default or specified footer height



-(void)btnClearFilterCategoriesClicked



{
    [singleton traceEvents:@"Clear Filter Category" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

    categoryName=@"";
    [_arrSelectedCategories removeAllObjects];
    
    [_tblFilter reloadData];
    
    
    
}





-(void)btnClearFilterServicesClicked
{
    [singleton traceEvents:@"Clear Filter Service" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

    serviceName=@"";
    
    [_arrSelectedServices removeAllObjects];
    
    [_tblFilter reloadData];
    
    
    
}



- (IBAction)btnBackClicked:(id)sender

{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
    
    
    
}







- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

{
    
    if (indexPath.section == 0 || indexPath.section == 1)
        
    {
        
        return  85;
        
    }
    
    else if (indexPath.section == 2)
        
    {
        
        if (indexPath.row == 0) {
            
            return 70;
            
        }
        
        else{
            
            return [self heightForDynamicCellForSection:indexPath.section];
            
        }
        
    }
    
    else{
        
        return 70.0;
        
    }
    
}



-(CGFloat)heightForDynamicCellForSection:(NSInteger)section

{
    NSMutableArray *arrData;
    
    if (_isCategorySelected) {
        
        arrData = _arrSelectedCategories;
    }
    else{
        arrData = _arrSelectedServices;
    }
    
    
    
    CGFloat xCord = 10;
    
    CGFloat heightPadding = 42;
    
    CGFloat padding = 10;
    
    
    
    if ([arrData count] == 0) {
        
        return 0.0;
        
    }
    
    else
        
    {
        
        CGFloat height = 60.0;
        
        for (int i = 0; i<arrData.count; i++)
            
        {
            
            CGFloat screenWidth = _tblFilter.frame.size.width;
            
            
            FilterServicesBO *objService = arrData[i];
            
            if ([objService isKindOfClass:[FilterServicesBO class]]) {
                
                
                
                CGRect frame = [self rectForText:objService.serviceName usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 60.0)];
                
                frame.size.width = frame.size.width + 40;
                frame.size.height = frame.size.height + 5;

                
                
                CGFloat rightMargin = self.tblFilter.frame.size.width - xCord;
                
                if (rightMargin < frame.size.width) {
                    
                    xCord = 10;
                    
                    height+=heightPadding;
                    
                    NSLog(@"New Item, Height Increased = %lf",height);
                    
                }
                
                
                xCord+=frame.size.width + padding;
                
            }
            
            
            
            else{
                
                
                
                NotificationItemBO *objNotifi = (NotificationItemBO*)objService;
                
                
                CGRect frame = [self rectForText:objNotifi.titleService usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 30.0)];
                
                frame.size.width = frame.size.width + 40;
                
                
                
                CGFloat rightMargin = self.tblFilter.frame.size.width - xCord;
                
                if (rightMargin < frame.size.width) {
                    
                    xCord = 10;
                    
                    height+=heightPadding;
                    
                    NSLog(@"New Item, Height Increased = %lf",height);
                    
                }
                
                
                
                xCord+=frame.size.width + padding;
                
                
                
            }
            
        }
        
        
        
        return height;
        
    }
    
    
    
}



-(CGRect)rectForText:(NSString *)text

           usingFont:(UIFont *)font

       boundedBySize:(CGSize)maxSize

{
    
    
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (section == 2){
        if (_isCategorySelected) {
            return [_arrSelectedCategories count] ? 2 : 1;
            
        }else{
            return [_arrSelectedServices count] ? 2 : 1;
        }
        
    }
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

{
    
    static NSString *cellIdentifier = @"cellNotificationFilter";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
//    if (self.view.frame.size.height < 500)
//    {
//        cell = nil;
//    }
    
    
    if(cell == nil)
    {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
        tableView.separatorColor = [UIColor lightGrayColor];
        
    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [[cell viewWithTag:454545] removeFromSuperview];
    cell.textLabel.text = @"";
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if (indexPath.section == 0) {
        
        CGFloat xCord = self.tblFilter.frame.size.width/2 - 120;
        
        CGFloat yCord = 13;
        
        
        
        CGFloat width = 90;
        
        CGFloat height = 60;
        
        
        
        for (int i = 0 ;  i < arrFirstFilterItems.count ; i++) {
            
            
            
            NSMutableDictionary *dictItem = arrFirstFilterItems[i];
            
            SDCapsuleButton *btnFilter = [[SDCapsuleButton alloc] initWithFrameForFilterButtons:CGRectMake(xCord, yCord, width, height) withTitle:[dictItem objectForKey:@"Title"] withNormalImage:[dictItem objectForKey:@"NormalState"] andSelectedImage:[dictItem objectForKey:@"SelectedState"]];
            
            [cell.contentView addSubview:btnFilter];
            
            [btnFilter.btnMain addTarget:self action:@selector(btnClickedFromCellForFistSection:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            if ([selectedFirstFilter isEqualToString:[dictItem objectForKey:@"Title"]]) {
                
                [btnFilter setSelected:YES];
                
            }
            
            else{
                
                [btnFilter setSelected:NO];
                
            }
            
            
            
            xCord = self.tblFilter.frame.size.width/2 + 30;
            
            
            
        }
        
        
        
        
        
        return cell;
        
        
        
    }
    
    else if (indexPath.section == 1)
    {
        
        CGFloat xCord = self.tblFilter.frame.size.width/2 - 120;
        
        CGFloat yCord = 13;
        
        
        
        CGFloat width = 90;
        
        CGFloat height = 60;
        
        
        
        for (int i = 0 ;  i < arrSecondFilterItems.count ; i++) {
            
            
            
            NSMutableDictionary *dictItem = arrSecondFilterItems[i];
            
            SDCapsuleButton *btnFilter = [[SDCapsuleButton alloc] initWithFrameForFilterButtons:CGRectMake(xCord, yCord, width, height) withTitle:[dictItem objectForKey:@"Title"] withNormalImage:[dictItem objectForKey:@"NormalState"] andSelectedImage:[dictItem objectForKey:@"SelectedState"]];
            
            [cell.contentView addSubview:btnFilter];
            
            [btnFilter.btnMain addTarget:self action:@selector(btnClickedFromCellForSecondSection:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            if ([selectedSecondFilter isEqualToString:[dictItem objectForKey:@"Title"]]) {
                
                [btnFilter setSelected:YES];
                
            }
            
            else{
                
                [btnFilter setSelected:NO];
                
            }
            
            
            
            xCord = self.tblFilter.frame.size.width/2 + 30;
            
            
            
        }
        
        
        
        
        
        return cell;
        
        
        
    }
    
    
    else if (indexPath.section == 2){
        
        if (indexPath.row == 0) {
            
            UIView *tempVw = [[UIView alloc] initWithFrame:CGRectMake(10, 10, CGRectGetWidth(self.tblFilter.frame) - 20, 50)];
            
            tempVw.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5].CGColor;
            
            tempVw.layer.borderWidth = 1.0;
            
            tempVw.layer.cornerRadius  = 2.0;
            
            tempVw.clipsToBounds = YES;
            tempVw.tag = 454545;
            
            tempVw.backgroundColor = [UIColor clearColor];
            
            [cell.contentView addSubview:tempVw];
            cell.textLabel.backgroundColor = [UIColor clearColor];
            
            
            if (_isCategorySelected)
            {
                cell.textLabel.text = NSLocalizedString(@"select_category", nil);
            }
            else
            {
                cell.textLabel.text = NSLocalizedString(@"select_service", nil);
            }
            
            
            cell.textLabel.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            cell.textLabel.textColor = [UIColor lightGrayColor];
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        else{
            
            
            cell.textLabel.text = @"";
            
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            // Add Capsule Buttons for Selected States
            
            [self designCellContentForStateNamesOnCelView:cell.contentView andForSection:indexPath.section];
            
        }
        
    }
    cell.textLabel.font = [AppFont regularFont:14.0];
    
    return cell;
    
}







-(void)designCellContentForStateNamesOnCelView:(UIView*)contentView andForSection:(NSInteger)section{
    
    
    
    CGFloat xCord = 10;
    
    CGFloat yCord = 15;
    
    CGFloat padding = 10;
    
    CGFloat heightPadding = 42;
    
    
    
    if(_isCategorySelected)
        
    {
        
        for (int i = 0; i<_arrSelectedCategories.count; i++)
            
        {
            
            
            
            FilterServicesBO *objFilter = _arrSelectedCategories[i];
            
            
            
            CGFloat screenWidth = _tblFilter.frame.size.width;
            
            
            
            CGRect frame = [self rectForText:objFilter.serviceName usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 30.0)];
            
            frame.size.width = frame.size.width +30;
            
            
            
            CGFloat rightMargin = self.tblFilter.frame.size.width - xCord;
            
            if (rightMargin < frame.size.width) {
                
                xCord = 10;
                
                yCord+=heightPadding;
                
            }
            
            
            
            SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrame:CGRectMake(xCord, yCord, frame.size.width, 30)];
            
            NSString *btntitle=[NSString stringWithFormat:@"%@",objFilter.serviceName];
            // [btn setBtnTitle:objFilter.serviceName];
            
            [btn setBtnTitle:btntitle];
            
            NSLog(@"objFilter.serviceName=%@",objFilter.serviceName);
            
            categoryName=objFilter.serviceName;
            
            [btn addTarget:self action:@selector(btnCapsuleCategoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            btn.tag = 2300+i;
            [contentView addSubview:btn];
            
            
            xCord+=frame.size.width + padding;
            
        }
        
    }
    
    else
        
    {
        
        for (int i = 0; i<_arrSelectedServices.count; i++) {
            
            NotificationItemBO *objFilter = _arrSelectedServices[i];
            
            
            
            CGFloat screenWidth = _tblFilter.frame.size.width;
            
            CGRect frame = [self rectForText:objFilter.titleService usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 30.0)];
            
            frame.size.width = frame.size.width +30;
            
            
            
            CGFloat rightMargin = self.tblFilter.frame.size.width - xCord;
            
            if (rightMargin < frame.size.width) {
                
                xCord = 10;
                
                yCord+=heightPadding;
                
            }
            
            
            
            SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrame:CGRectMake(xCord, yCord, frame.size.width, 30)];
            
            [btn setBtnTitle:objFilter.titleService];
            
            NSLog(@"titleService=%@",objFilter.titleService);
            serviceName=objFilter.titleService;
            
            [btn addTarget:self action:@selector(btnCapsuleServiceClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            btn.tag = 2300+i;
            
            [contentView addSubview:btn];
            
            xCord+=frame.size.width + padding;
            
        }
        
    }
    
}





-(void)btnCapsuleCategoryClicked:(SDCapsuleButton*)btnState
{
    [singleton traceEvents:@"Capsule Category Button" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

    
    NSInteger row = btnState.tag - 2300;
    NSLog(@"category Clicked : %li",(long)row);
    
    if (row < _arrSelectedCategories.count) {
        [_arrSelectedCategories removeObjectAtIndex:row];
        [_tblFilter reloadData];
        
    }
    
    
}

-(void)btnCapsuleServiceClicked:(SDCapsuleButton*)btnState
{
    [singleton traceEvents:@"Capsule Service Button" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

    NSInteger row = btnState.tag - 2300;
    NSLog(@"category Clicked : %li",(long)row);
    
    if (row < _arrSelectedServices.count) {
        [_arrSelectedServices removeObjectAtIndex:row];
        [_tblFilter reloadData];
    }
    
}




-(void)btnCrossClicked:(UIButton*)btnCross{
    
    // SDCapsuleButton *btn = (SDCapsuleButton*)[btnCross superview];
    
    
    
}







-(void)btnClickedFromCellForFistSection:(UIButton*)sender{
    
    SDCapsuleButton *superVw = (SDCapsuleButton*)[sender superview];
    
    selectedFirstFilter = [superVw btnTitle];
    
    
    if ([selectedFirstFilter isEqualToString:NSLocalizedString(@"alphabetic", nil)]) {
        [singleton traceEvents:@"Alphabetic Button" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

        sortby=@"alpha";
    }
    else
    {
        [singleton traceEvents:@"DateTime Button" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

        sortby=@"date_time";
        
    }
    [_tblFilter reloadData];
    
}


-(void)btnClickedFromCellForSecondSection:(UIButton*)sender{
    
    SDCapsuleButton *superVw = (SDCapsuleButton*)[sender superview];
    
    selectedSecondFilter = [superVw btnTitle];
    
    
    if ([selectedSecondFilter isEqualToString:NSLocalizedString(@"filter_categories", nil)])
    {
        _isCategorySelected = YES;
        [_arrSelectedServices removeAllObjects];
        
    }
    else
    {
        _isCategorySelected = NO;
        [_arrSelectedCategories removeAllObjects];
        
        
    }
    [_tblFilter reloadData];
    
}






- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.section == 2) {
        
        
        [singleton traceEvents:@"Open Select Category" withAction:@"Clicked" withLabel:@"Transactional History Filter" andValue:0];

        if (_isCategorySelected)
        {
            [_arrSelectedServices removeAllObjects];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DetailService" bundle:nil];

            SelectCategoryVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SelectCategoryVC"];
            vc.callBack = self;
            vc.isForCategory = YES;
            vc.arrTableContent = _arrCategoryFilter;
            vc.arrPreselectedItem=_arrSelectedCategories;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        
        else
        {
            [_arrSelectedCategories removeAllObjects];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DetailService" bundle:nil];

            SelectCategoryVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SelectCategoryVC"];
            vc.arrTableContent = _arrServiceFilter;
            vc.arrPreselectedItem=_arrSelectedServices;
            
            vc.callBack = self;
            vc.isForCategory = NO;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
    }
    
}







-(void)updateFilterValue:(id)obj{
    
    
    
    
    
    if (_isCategorySelected) {
        
        
        
        [_arrSelectedCategories removeAllObjects];
        
        [_arrSelectedCategories addObjectsFromArray:obj];
        
    }
    
    
    
    else{
        
        [_arrSelectedServices removeAllObjects];
        
        [_arrSelectedServices addObjectsFromArray:obj];
        
    }
    
    
    
    [self.tblFilter reloadData];
    
}





- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    [_tblFilter reloadData];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 
 
 */






@end
