//
//  HomeTabVC.h
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvertisingColumn.h"
#import "NotifyPopUp.h"



#import "StateList.h"


@interface HomeTabVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITabBarDelegate,UITextFieldDelegate>
{
    
    AdvertisingColumn *_headerView;
    
    IBOutlet UITableView *tableView_home;
   __weak SharedManager *singleton;
    IBOutlet UITextField *txt_searchField;
    
    BOOL flag_complete;
    
    UIRefreshControl *refreshController;
    
    IBOutlet UIView *vw_line;
    
    
    IBOutlet UIView *vw_RetryOption;
    IBOutlet UIButton *btn_retryclick;
    IBOutlet UILabel *lbl_errorloading;
    
    
    IBOutlet UIImageView *searchIconImage;
}


-(IBAction)retryAction:(id)sender;
-(IBAction)btn_filterAction:(id)sender;//for filterview show
-(IBAction)btn_noticationAction:(id)sender;//for notification view show
-(void)fetchDatafromDB;
-(void)hitHomeAPI;


@end
