//
//  SettingNotificationVwCon.m
//  Umang
//
//  Created by admin on 19/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "SettingNotificationVwCon.h"
#import "CustomPickerVC.h"
#import "SettingNotificationCell.h"
#import "NotificationItemBO.h"
#import "NotificationSettingTitleCell.h"
#import "ServiceHeaderCell.h"
#import "NotiSettingCell.h"
#import "SettingsCell.h"
#import "AllServiceCell.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "SettingsCell.h"
#import "UIView+Toast.h"

@interface SettingNotificationVwCon ()<UITableViewDataSource,UITableViewDelegate>
{
    SharedManager *singleton;
    MBProgressHUD *hud;
    CustomPickerVC *customVC;
    
    NSMutableArray *arrServiceNotification;
    NotificationSettingTitleCell *notifictnSettingCell;
    ServiceHeaderCell *serviceCell;
    
    __weak IBOutlet UIButton *backNotification;
    __weak IBOutlet UILabel *lblAllService;
    NSMutableArray *_arrNotificationSetting;
    
    BOOL _isNotificationON;
    
    NSString *notifiTypeSelected;
    
    BOOL isFirstTimeLoad;
}
@property(retain,nonatomic)NSMutableArray *arr_table_pass;
@property(retain,nonatomic)NSString *header_title_pass;
@property(retain,nonatomic)NSString *TAG_pass;
@property(retain,nonatomic)NSString *disableStatus;

@end

@implementation SettingNotificationVwCon

@synthesize arr_table_pass;
@synthesize header_title_pass;
@synthesize TAG_pass;



-(NSString*)getNotificationStatus{
    NSString *defaultStatus = NSLocalizedString(@"enabled", nil);
    
    singleton=[SharedManager sharedSingleton];
    
    if(([singleton.shared_ntft isEqualToString:@"0"] ||[singleton.shared_ntft isEqualToString:@""]) && ([singleton.shared_ntfp isEqualToString:@"0"] || [singleton.shared_ntfp isEqualToString:@""])){
        defaultStatus = NSLocalizedString(@"disabled", nil);
    }
    
    return  defaultStatus;
}


-(NSString*)getUpdatedNotificationTypeSelectedStatus{
    
    singleton=[SharedManager sharedSingleton];
    
    if([singleton.shared_ntft isEqualToString:@"1"]&&[singleton.shared_ntfp isEqualToString:@"1"]){
        notifiTypeSelected=NSLocalizedString(@"all", nil);
    }
    else if([singleton.shared_ntft isEqualToString:@"1"]&&[singleton.shared_ntfp isEqualToString:@"0"]){
        notifiTypeSelected=NSLocalizedString(@"transactional_small", nil);
    }
    else if([singleton.shared_ntft isEqualToString:@"0"]&&[singleton.shared_ntfp isEqualToString:@"1"]){
        notifiTypeSelected=NSLocalizedString(@"promotional_small", nil);
    }
    else{
        // Means notification is disabled
        notifiTypeSelected = @"";
    }
    
    return notifiTypeSelected;
}


- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    
    isFirstTimeLoad = YES;
    
    if (fDeviceWidth <= 568)
    {
        lblAllService.font = [UIFont systemFontOfSize:12.0];
    }
    else
    {
        lblAllService.font = [UIFont systemFontOfSize:14.0];
        
    }
    singleton=[SharedManager sharedSingleton];
    singleton.notiTypeSelected = [self getUpdatedNotificationTypeSelectedStatus];
    
    
    lblAllService.textAlignment = NSTextAlignmentLeft;
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:SETTING_NOTIFICATION_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    _isNotificationON = NO;
    
    
    _lblSettingHeader.text = NSLocalizedString(@"notification_settings", nil);
    
    
    
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    
    //  [self prepareTempDataForServiceNotification];
    [self.tblNotificationSetting reloadData];
    
    [backNotification setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:backNotification.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(backNotification.frame.origin.x, backNotification.frame.origin.y, backNotification.frame.size.width, backNotification.frame.size.height);
        
        [backNotification setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        backNotification.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [self addNavigationView];
    /* NSLog(@"%@",[singleton.dbManager getNotifData]);
     NSLog(@"%@",[singleton.dbManager getNotifDatapromo]);
     NSLog(@"%@",[singleton.dbManager getNotifDatatrans]);
     */
    // Do any additional setup after loading the view.
}

-(void)updateNofificationStatusAPI{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"app" forKey:@"mod"];
    [dictBody setObject:@"" forKey:@"type"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    if ([notifiTypeSelected isEqualToString:NSLocalizedString(@"all", nil)]) {
        [dictBody setObject:@"1" forKey:@"ntft"];
        [dictBody setObject:@"1" forKey:@"ntfp"];
    }
    else if (notifiTypeSelected.length == 0){ // Means notification is disabled
        [dictBody setObject:@"0" forKey:@"ntft"];
        [dictBody setObject:@"0" forKey:@"ntfp"];
    }
    else{
        [dictBody setObject:[self getNTFTStatus] forKey:@"ntft"];
        [dictBody setObject:[self getNTFPStatus] forKey:@"ntfp"];
    }
    
    [dictBody setObject:@"" forKey:@"imsi"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UPDATE_NOTIFICATION_SETTINGS withBody:dictBody andTag:TAG_REQUEST_NOTIFICATION_SETTINGS completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1]){
                // Update the value
                
                singleton.shared_ntft = [dictBody objectForKey:@"ntft"];
                singleton.shared_ntfp = [dictBody objectForKey:@"ntfp"];
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    }];
}

-(NSString*)getNTFTStatus{
    NSString *defaultValue = @"0";
    if ([notifiTypeSelected isEqualToString:NSLocalizedString(@"transactional_small", nil)]) {
        defaultValue = @"1";
    }
    return defaultValue;
}

-(NSString*)getNTFPStatus{
    NSString *defaultValue = @"0";
    if ([notifiTypeSelected isEqualToString:NSLocalizedString(@"promotional_small", nil)]) {
        defaultValue = @"1";
    }
    return defaultValue;
}





-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}
-(void)addNotificationSettingData
{
    
    if (_arrNotificationSetting == nil) {
        _arrNotificationSetting = [[NSMutableArray alloc]init];
        
    }
    else
    {
        [_arrNotificationSetting removeAllObjects];
    }
    
    NSArray *arrSettingData  = [singleton.dbManager loadDataServiceData];
    
    for (int i = 0; i < arrSettingData.count; i++)
    {
        NSDictionary *dict = [arrSettingData objectAtIndex:i];
        NotificationItemBO *objSetting = [[NotificationItemBO alloc]init];
        objSetting.titleService = [dict valueForKey:@"SERVICE_NAME"];
        objSetting.ID = [dict valueForKey:@"SERVICE_ID"];
        objSetting.statusTypeForServices = [dict valueForKey:@"SERVICE_IS_HIDDEN"];
        objSetting.imgService=[dict valueForKey:@"SERVICE_IMAGE"];
        [_arrNotificationSetting addObject: objSetting];
        
    }
    
    [_tblNotificationSetting reloadData];
    
}




-(void)switchChanged:(UISwitch*)sender
{
    NSInteger row = sender.tag - 2300;
    NotificationItemBO *objFilter = [_arrNotificationSetting objectAtIndex:row];
    
    if (sender.isOn) {
        
        objFilter.statusTypeForServices = @"true";
        [singleton.dbManager updateServiceIsNotifEnabled :objFilter.ID notifIsEnabled:@"true"];
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:@"off"
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
        
        
        
        objFilter.statusTypeForServices = @"false";
        [singleton.dbManager updateServiceIsNotifEnabled :objFilter.ID notifIsEnabled:@"false"];
    }
    
    [_tblNotificationSetting reloadData];
}


-(void)switchChangedwithObjFilter:(NotificationItemBO*)obj and:(UISwitch*)sender
{
    if ([obj.statusTypeForServices isEqualToString:@"true"])
    {
        [obj.statusTypeForServices isEqualToString:@"false"];
        [singleton.dbManager updateServiceIsNotifEnabled :obj.ID notifIsEnabled:@"false"];
    }
    else
    {
        [obj.statusTypeForServices isEqualToString:@"true"];
        [singleton.dbManager updateServiceIsNotifEnabled :obj.ID notifIsEnabled:@"true"];
    }
    
    [_tblNotificationSetting reloadData];
    
    
}




-(IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (section == 0)
    {
        return 20;
    }
    if (section == 1)
    {
        
        return 45.0;
    }
    else{
        return 60;
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (_isNotificationON) {
        return 0.001;
    }
    else{
        return 50.0;
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section   // custom view for footer. will be adjusted to default or specified footer height
{
    if (_isNotificationON) {
        return nil;
    }
    
    static NSString *CellIdentifier = @"ServiceHeaderCell";
    ServiceHeaderCell *footerCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (footerCell == nil)
    {
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    
    
    footerCell.lblServiceNotificatn .text = @"";
    footerCell.lblServiceSubTitle.text = NSLocalizedString(@"disabled_notif_txt", nil);
    footerCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
      footerCell.lblServiceSubTitle.font = [AppFont regularFont:12];
    footerCell.lblServiceNotificatn.font = [AppFont regularFont:14];

    return footerCell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    NotificationSettingTitleCell *headerView;
    
    if (section == 0)
    {
        
        
    }
    else if (section == 1)
    {
        
        static NSString *CellIdentifier = @"NotificationSettingTitleCell";
        headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (headerView == nil)
        {
            [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
        }
        
        
        
        headerView.lblNotificatnSetting .text =  [NSLocalizedString(@"notification_settings_Large", nil) uppercaseString];
      headerView.lblNotificatnSetting.font = [AppFont regularFont:13.0];
        headerView.lblNotificatnSetting.textColor = [UIColor grayColor];
        headerView.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
        //[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
        
        
        
    }
    
    else if (section == 2)
    {
        
        static NSString *CellIdentifier = @"ServiceHeaderCell";
        headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (headerView == nil)
        {
            [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
        }
        
        
        serviceCell.lblServiceNotificatn.text = NSLocalizedString(@"notification_settings", nil);
        
        serviceCell.lblServiceSubTitle.text = NSLocalizedString(@"select_notif_txt", nil);
        serviceCell.backgroundColor = [UIColor redColor];
        //[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
        
    }
    
    
    // headerView.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    return 50.0;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    if (_isNotificationON) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (!_isNotificationON) {
        return 1;
    }
    
    switch (section)
    {
        case 0:
            return 1;
            break;
            
        case 1:
            return 1;
            break;
            
            
        default:
            break;
    }
    
    return 0;
}

-(void)notificationSwitchChanged:(UISwitch*)sender{
    
    _isNotificationON = sender.isOn;
    
    if (_isNotificationON) {
        notifiTypeSelected=NSLocalizedString(@"all", nil);
    }
    else{
        notifiTypeSelected = @"";
    }
    
    
    [[NSUserDefaults standardUserDefaults] setBool:_isNotificationON forKey:@"IS_NOTIFICATION_ENABLED"];
    
    if(sender.isOn)
    {
        singleton.notificationSelected=NSLocalizedString(@"enabled", nil);
        //  turn_off_notif_dialog_msg_txt
        [self showToast:NSLocalizedString(@"notification_enabled_txt", nil)];
        
        // singleton.notificationSelected=NSLocalizedString(@"disabled", nil);
        
        // If on then hit api
        [self updateNofificationStatusAPI];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"turn_off_notif_dialog_title_txt", nil)
                                                        message:NSLocalizedString(@"turn_off_notif_dialog_msg_txt", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                              otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        alert.tag=102;
        [alert show];
        
        
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:singleton.notificationSelected forKey:@"SELECTED_NOTIFICATION_STATUS"];
    
    [_tblNotificationSetting reloadData];
}




-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 101)
    { // handle the altdev
        if (buttonIndex==0)
        {
            
            singleton.notificationSelected=NSLocalizedString(@"enabled", nil);
            _isNotificationON =YES;
            
        }
        else
        {
            
            
            
            
            singleton.notificationSelected=NSLocalizedString(@"disabled", nil);
            
            _isNotificationON =NO;
            
            
        }
        
        [_tblNotificationSetting reloadData];
        
    }
    
    if (alertView.tag == 102)
    { // handle the altdev
        if (buttonIndex==0)
        {
            notifiTypeSelected = NSLocalizedString(@"all", nil);
            singleton.notificationSelected=NSLocalizedString(@"enabled", nil);
            _isNotificationON =YES;
            
            
        }
        else
        {
            notifiTypeSelected = @"";
            
            singleton.notificationSelected=NSLocalizedString(@"disabled", nil);
            
            self.disableStatus=@"off";
            _isNotificationON =NO;
            
            [self showToast:NSLocalizedString(@"notification_disabled_txt", nil)];
            
            [self updateNofificationStatusAPI];
            
        }
        //        [self hitAPI];
        
        [_tblNotificationSetting reloadData];
        
    }
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    if (indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"NotiSettingCell";
        NotiSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell.lblNotification.font = [AppFont regularFont:17.0];
        cell.lblNotification.text = NSLocalizedString(@"notifications", nil);
        [cell.switchOnOff addTarget:self action:@selector(notificationSwitchChanged:) forControlEvents:UIControlEventValueChanged];
        [cell.switchOnOff setOn:_isNotificationON];
        
        return cell;
        
    }
    else
    {
        
        
        static NSString *CellIdentifierType = @"SettingsCell";
        SettingsCell   *settingCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
        
        settingCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        settingCell.lblReceiveSetting.text = NSLocalizedString(@"receive_notification", nil);
        settingCell.lblAll.text =  notifiTypeSelected;
        settingCell.lblAll.textAlignment = NSTextAlignmentRight;
        
        settingCell.imgSettings.image = [UIImage imageNamed:@"icon_notifications.png"];
        settingCell.lblAll.font = [AppFont regularFont:17.0];
        settingCell.lblReceiveSetting.font = [AppFont regularFont:17.0];
        
        return settingCell;
        
    }
    
    
    
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section==1) {
        [self initSettingData];
    }
}

#pragma mark- add Navigation View to View

-(void)addNavigationView{
    backNotification.hidden = true;
    _lblSettingHeader.hidden = true;
    //btnHelp.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backBtnAction:btnBack];
    };
    nvView.lblTitle.text = _lblSettingHeader.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tbFrame = self.tblNotificationSetting.frame;
        tbFrame.origin.y = kiPhoneXNaviHeight
        self.tblNotificationSetting.frame = tbFrame;
        [self.view layoutIfNeeded];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    // [self addNotificationSettingData];
    
    // [singleton.dbManager updateServiceIsNotifEnabled :objFilter.ID notifIsEnabled:@"true"];
    
    //    if([singleton.shared_ntft isEqualToString:@"0"]&&[singleton.shared_ntfp isEqualToString:@"0"])
    //    {
    //
    //        singleton.notificationSelected=NSLocalizedString(@"disabled", nil);
    //
    //        self.disableStatus=@"off";
    //        _isNotificationON =NO;
    //    }
    //    else  if([singleton.shared_ntft isEqualToString:@"1"]&&[singleton.shared_ntfp isEqualToString:@"1"])
    //    {
    //        singleton.notificationSelected=NSLocalizedString(@"enabled", nil);
    //        _isNotificationON =YES;
    //        self.disableStatus=@"on";
    //    }
    
    
    
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    notifiTypeSelected = singleton.notiTypeSelected;
    
    if ([singleton.notificationSelected isEqualToString:NSLocalizedString(@"enabled", nil)]) {
        _isNotificationON = true;
    }
    else{
        _isNotificationON = false;
    }
    
    if (isFirstTimeLoad ) {
        isFirstTimeLoad = NO;
    }
    else{
        [self updateNofificationStatusAPI];
    }
    [_tblNotificationSetting reloadData];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:YES];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [backNotification.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _lblSettingHeader.font = [AppFont semiBoldFont:17];
    if (fDeviceWidth <= 568)
    {
        lblAllService.font = [AppFont regularFont:12];
    }
    else
    {
        lblAllService.font = [AppFont regularFont:14];
        
    }
}
-(void)initSettingData
{
    
    TAG_pass=TAG_NOTIFICATION;
    
    arr_table_pass=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"all", nil),NSLocalizedString(@"promotional_small", nil),NSLocalizedString(@"transactional_small", nil), nil];
    
    
    
    [self callCustomPicker];
    
    
    
}




-(void)callCustomPicker
{
    //CustomPickerVC
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    customVC = [storyboard instantiateViewControllerWithIdentifier:@"CustomPickerVC"];
    customVC.delegate=self;
    customVC.get_title_pass=header_title_pass;
    customVC.get_arr_element=arr_table_pass;
    customVC.get_TAG=TAG_pass;
    // [customVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:customVC animated:NO completion:nil];
    [self.navigationController pushViewController:customVC animated:YES];
    
}





- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}




#pragma mark - Table view data source



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
