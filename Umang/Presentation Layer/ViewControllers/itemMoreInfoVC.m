//
//  itemMoreInfoVC.m
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "itemMoreInfoVC.h"
#import "UIImageView+WebCache.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"

#import "HomeDetailVC.h"

@interface itemMoreInfoVC ()
{
    SharedManager *singleton;
    MBProgressHUD *hud ;
    
}
@end

@implementation itemMoreInfoVC
@synthesize dic_serviceInfo;
@synthesize imagesRatingControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    singleton = [SharedManager sharedSingleton];
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: ITEM_MORE_INFO_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    

    [self setUpViewItem];
    
    btn_edit.hidden=TRUE;
    img_btn_edit.hidden=TRUE;
    lbl_rateService.text=@"RATE SERVICE";
    [self hitFetchUserRatingAPI];//call fetch User Rating
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitFetchUserRatingAPI
{
    NSString *service_id=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_ID"]];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:service_id forKey:@"sid"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_USER_RATING withBody:dictBody andTag:TAG_REQUEST_FETCH_USER_RATING completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                NSString *rating=[[response valueForKey:@"pd"]valueForKey:@"rating"];
                [imagesRatingControl setRating:[rating intValue]];
                
                //[imagesRatingControl setRating:3];
                //----JUST FOR TEST
                
                if ([rating isEqualToString:@"0"]) {
                    
                }
                else
                {
                    FlagCheckEdit=TRUE;
                    
                    [self ratingStatusCheck];
                }
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}




- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [super viewWillAppear:NO];
}
-(void)ratingStatusCheck
{
    
    if (FlagCheckEdit==TRUE) //user rating is done can edit here
    {
        btn_edit.hidden=FALSE;
        img_btn_edit.hidden=FALSE;
        lbl_rateService.text=@"YOUR RATING";
        
    }
    else
    {
        btn_edit.hidden=TRUE;
        img_btn_edit.hidden=TRUE;
        lbl_rateService.text=@"RATE SERVICE";
    }
    
}

-(void)setUpViewItem
{
    
    NSLog(@"dic_serviceInfo=%@",dic_serviceInfo);
    
    // - and max rating
    UIImage *dot, *star;
    dot = [UIImage imageNamed:@"star_border"];
    star = [UIImage imageNamed:@"star_filled"];
    imagesRatingControl = [[AMRatingControl alloc] initWithLocation:CGPointMake(fDeviceWidth/2,fDeviceHeight/3)emptyImage:dot solidImage:star andMaxRating:5];
    
    __weak typeof(self) weakSelf = self;
    
    imagesRatingControl.editingDidEndBlock = ^(NSUInteger rating)
    {
        NSLog(@"editingDidEndBlock %@",[NSString stringWithFormat:@"%lu_star", (unsigned long)rating]);
        
        //hit API here
        NSString *rate=[NSString stringWithFormat:@"%lu",(unsigned long)rating];
        [weakSelf hitRatingserviceAPI:rate];
        
    };
    
    
    // Add the control(s) as a subview of your view
    [self.view addSubview:imagesRatingControl];
    
    
    
    self.view.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.3];
    
    [self addShadowToTheView:self.view];
    
    [self addShadowToTheView:vw_bg];
    [self addShadowToTheView:vw_withname];
    [self addShadowToTheView:vw_withrate];
    [self addShadowToTheView:vw_withdesc];
    [self addShadowToTheView:vw_withbtns];
    
    
    NSURL *url=[NSURL URLWithString:[dic_serviceInfo objectForKey:@"SERVICE_IMAGE"]];
    
    [img_service sd_setImageWithURL:url
                   placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
    
    
    /*img_service.layer.cornerRadius = img_service.frame.size.height /2;
     img_service.layer.masksToBounds = YES;
     img_service.layer.backgroundColor=(__bridge CGColorRef)([UIColor grayColor]);
     img_service.layer.borderWidth = 0.2;
     */
    
    lbl_rating.text=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_RATING"]];
    
    NSString *rating=lbl_rating.text;
    
    float fCost = [rating floatValue];
    self.starRatingView.value = fCost;

   // [self.starRatingView setScore:fCost*2 withAnimation:NO];
    
    
    
    lbl_name.text=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_NAME"]];
    
    lbl_category.text=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_CATEGORY"]];
    lbl_category.frame=CGRectMake(lbl_category.frame.origin.x, lbl_category.frame.origin.y, lbl_category.intrinsicContentSize.width+10,lbl_category.frame.size.height);
    
    txt_description.text=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_DESC"]];
    
    
}








-(void)hitRatingserviceAPI:(NSString*)rating
{
    
    NSString *service_id=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_ID"]];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:rating forKey:@"rating"];//Enter mobile number of user
    [dictBody setObject:service_id forKey:@"sid"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"st"]; //tkn number
    // [dictBody setObject:@"en" forKey:@"lcle"]; //tkn number
    
    
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_RATINGS withBody:dictBody andTag:TAG_REQUEST_API_RATINGS completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                //[imagesRatingControl setRating:3];
                //----JUST FOR TEST
                FlagCheckEdit=TRUE;
                
                [self ratingStatusCheck];
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}








-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}


- (IBAction)btn_close_Action:(id)sender {
    
    
    [UIView animateWithDuration:0.4 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}


-(IBAction)btn_editAction:(id)sender
{
    NSLog(@"EDIT ACION");
}

-(IBAction)btn_viewWeb_Action:(id)sender
{
    NSLog(@"VISIT WEBSITE=%@", [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"]]);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Visit Website"
                                                    message:@"Sure to Visit?"
                                                   delegate:self
                                          cancelButtonTitle:@"CANCEL"
                                          otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
    alert.tag=100;
    [alert show];
}
-(IBAction)btn_viewMap_Action:(id)sender
{
    NSLog(@"VISIT MAP LAT=%@", [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_LATITUDE"]]);
    NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_LONGITUDE"]]);
    
    NSString *latitude=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_LATITUDE"]];
    
    NSString *longitute=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_LONGITUDE"]];
    
    
    NSString *deptName=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_NAME"]];
    deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    //http://maps.google.com/?saddr=29.9457, 78.1642&daddr=29.9457, 78.1642
    
    
    //NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%@,%@&daddr=%@,%@",latitude, longitute, latitude,longitute];
    
    //  NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=\%@,%@",latitude, longitute];
    
   // NSString* googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@",latitude, longitute];
    
    
   // [[UIApplication sharedApplication] openURL: [NSURL URLWithString: googleMapsURLString]];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]])
        
    {
        NSLog(@"Map App Found");
        
        NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
        
        // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
        
        NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSLog(@"mapURL= %@",mapURL);
        
        [[UIApplication sharedApplication] openURL:mapURL];
        
        
    } else
    {
        NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
        
    }

    
    
}

-(IBAction)btn_contact_Action:(id)sender
{
    NSLog(@"CONTACT=%@", [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"]]);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contact"
                                                    message:@"Sure to call?"
                                                   delegate:self
                                          cancelButtonTitle:@"CANCEL"
                                          otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
    alert.tag=101;
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100 && buttonIndex == 1)
    { // handle the altdev
        
        NSString *numberString = [dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
        numberString = [numberString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",numberString];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
        
    }
    else if (alertView.tag == 101 && buttonIndex == 1)
    { // handle the donate
        NSString *url=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        
    }
}

-(IBAction)btn_VisitService_Action:(id)sender
{
    NSLog(@"VISIT SERVICE=%@", [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_URL"]]);//SERVICE_WEBSITE
    //JUMP HERE TO DETAIL VIEW LOAD PASS ALL DATA TO BE USED THERE AS WELL
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    vc.dic_serviceInfo=dic_serviceInfo ;
    vc.tagComeFrom=@"OTHERS";
    
    vc.sourceTab     = @"home_more";
    vc.sourceState   = @"";
    vc.sourceSection = [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"serviceCard"]];
    vc.sourceBanner  = @"";
    
    [self presentViewController:vc animated:NO completion:nil];
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
