//
//  ServiceDirectoryVC.h
//  Umang
//
//  Created by admin on 10/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceDirectoryVC : UIViewController


@property (weak, nonatomic) IBOutlet UITableView *tblServiceDirectory;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UILabel *screenTitle;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;
@property (strong, nonatomic) IBOutlet UIView *noRecordView;
@property (strong, nonatomic) IBOutlet UILabel *noRecordFoundLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *screenTitleTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *backButtonTopConstraint;

@end


#pragma mark - Service Directory Cell Interface
@interface ServiceDirectoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *departmentNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *deptImage;

@end
