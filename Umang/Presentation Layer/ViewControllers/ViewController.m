//
//  ViewController.m
//  Umang
//
//  Created by spice_digital on 31/08/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "ViewController.h"
//#import "LoginViewController.h"


#import "LoginAppVC.h"
#import "NewLanguageSelectVC.h"
#import "AppDelegate.h"
#import "LanguageSelectVC.h"


//-------
#import<CoreTelephony/CTCallCenter.h>
#import<CoreTelephony/CTTelephonyNetworkInfo.h>
#import<CoreTelephony/CTCarrier.h>

//-----

#import "UIImageView+WebCache.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"

#import "UserEditVC.h"
#import "MobileRegistrationVC.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#import <QuartzCore/QuartzCore.h>

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface ViewController ()
{
    __weak IBOutlet UIButton *btnSignUp;
    
    NSString * preferedLocale;
    MBProgressHUD *hud ;
    NSDictionary *jsonDic;
    SharedManager *singleton;
    NetworkStatus networkStatus;
    
    IBOutlet UIButton *btnBack;
    NSMutableDictionary *tourdic;
    
    NSDictionary *finalDic;
    MobileRegistrationVC *mobileReg;
    NavigationView *nvView ;
}
@end

@implementation ViewController

@synthesize scrollView;

@synthesize page;
@synthesize pageControl;
@synthesize numberOfLines;



//--------Code to get mobile information--------
-(void)mobileInfo
{
    CTTelephonyNetworkInfo* info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = info.subscriberCellularProvider;
    NSString *mobileCountryCode = carrier.mobileCountryCode;
    NSString *carrierName = carrier.carrierName;
    NSString *isoCountryCode = carrier.isoCountryCode;
    NSString *mobileNetworkCode = carrier.mobileNetworkCode;
    
    NSLog(@"carrier %@",carrier);
    NSLog(@"mobileCountryCode %@",mobileCountryCode);
    NSLog(@"carrierName %@",carrierName);
    NSLog(@"isoCountryCode %@",isoCountryCode);
    NSLog(@"mobileNetworkCode %@",mobileNetworkCode);
    // Try this to track CTCarrier changes
    info.subscriberCellularProviderDidUpdateNotifier = ^(CTCarrier* inCTCarrier) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"User did change SIM");
        });
    };
    
    NSString *deviceType=[NSString stringWithFormat:@"%@",[DeviceInfo deviceType]];
    NSLog(@"ios Device type %@",deviceType);
    NSLog(@"IOS Version %f",[[UIDevice currentDevice].systemVersion floatValue]);
    
    NSString *deviceTypeModel = [[UIDevice currentDevice] model];
    NSLog(@"ios Device type %@",deviceTypeModel);
    // IMEI not posible
    //
    NSString *UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSLog(@"ios UDID %@",UDID);
    
    
    
    
    
    NSString *bundleId = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    NSLog(@"bundleId %@",bundleId);//hardcode this time
    
    
    /* KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:bundleId accessGroup:nil];
     if(![keychainItem objectForKey:(__bridge id)(kSecValueData)]){
     NSString *idfa = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
     [keychainItem setObject:idfa forKey:(__bridge id)(kSecValueData)];
     NSLog(@"saving item %@", [keychainItem objectForKey:(__bridge id)(kSecValueData)]);
     }else{
     NSLog(@"saved item is %@", [keychainItem objectForKey:(__bridge id)(kSecValueData)]);
     }
     
     */
    
    
}
//--------Code to get mobile information--------

//-----------------------------------------------
//              FetchUSPScrren
//-----------------------------------------------
/*
 -(void)call_FetchUSPScreen
 {
 
 UMFetchUspImage *obj=[UMFetchUspImage  sharedSingleton];
 NSString *did=@"87de96a7be0f13e7";
 NSString *imei=@"862563036223976";
 NSString *imsi=@"404022100194578";
 NSString *hmk=@"OnePlus";
 NSString *hmd=@"ONEPLUS A3003";
 NSString *os=@"Android 6.0.1";
 NSString *pkg=@"in.spicedigital.umang";
 NSString *ver=@"1";
 NSString *tkn=@"";
 NSString *rot=@"no";
 NSString *mod=@"app";
 NSString *mcc=@"404";
 NSString *mnc=@"02";
 NSString *lac=@"2065";
 NSString *clid=@"11413";
 NSString *acc=@"";
 NSString *lat=@"";
 NSString *lon=@"";
 NSString *peml=@"singhmaninder152@gmail.com";
 NSString *lang=@"en";
 
 NSDictionary *myDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
 did,@"did",
 imei,@"imei",
 imsi,@"imsi",
 hmk,@"hmk",
 hmd,@"hmd",
 os,@"os",
 pkg,@"pkg",
 ver,@"ver",
 tkn,@"tkn",
 rot,@"rot",
 mod,@"mod",
 mcc,@"mcc",
 mnc,@"mnc",
 lac,@"lac",
 clid,@"clid",
 acc,@"acc",
 lat,@"lat",
 lon,@"lon",
 peml,@"peml",
 lang,@"lang",
 nil];
 
 NSError * err;
 NSData * jsonData = [NSJSONSerialization dataWithJSONObject:myDictionary options:0 error:&err];
 NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
 
 
 [obj methodFetchUSP:jsonString :^(NSString *strResponse)
 {
 NSString *responseString = strResponse;
 
 dispatch_async(dispatch_get_main_queue(), ^{
 
 NSLog(@" response =%@",responseString);
 //  NSLog(@" response =%@",[[obj.res_dic valueForKey:@"pd"]valueForKey:@"hi"]);
 
 
 NSError *error;
 
 NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
 NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
 options:kNilOptions
 error:&error];
 
 
 NSLog(@" jsonResponse =%@",jsonResponse);
 
 // _txt_view.text=[NSString stringWithFormat:@"%@",[[[[jsonResponse valueForKey:@"pd"]valueForKey:@"hi"]valueForKey:@"desc"] objectAtIndex:0]];
 
 [self drawTour:jsonResponse];
 });
 
 
 
 }];
 
 
 
 }
 */







//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------   NEW USP SCREEN  START  ----------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------



-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}



-(void)drawTour:(NSDictionary*)responsedic
{
 


    jsonDic=responsedic;
    
    for(UIView *subview in scrollView.subviews)
    {
        [subview removeFromSuperview];
    }
    
    
    for (UIView *view in [self.view subviews])
    {
        if ([view isKindOfClass:[UIScrollView class]])
        {
            [view removeFromSuperview];
            
        }
        if ([view isKindOfClass:[UIPageControl class]])
        {
            [view removeFromSuperview];
            
        }
        
    }
    
   
        // Bound the screen according to width size of the screen
        CGSize rectsize = [[UIScreen mainScreen] bounds].size;
        CGFloat frameX = rectsize.width;
        CGFloat frameY = rectsize.height-57; //padding for UIpageControl
    
    
  /*  if ([[UIScreen mainScreen]bounds].size.height <= 568)
    {
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0, frameX, fDeviceHeight-50)];

    }
    else
    {
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0, frameX, frameY-100)];
    }
    */
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0, frameX, fDeviceHeight-50)];
    
    if (iPhoneX())
    {
        
        if (@available(iOS 11.0, *))
        {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            CGFloat bottomPadding = window.safeAreaInsets.bottom;
            
            CGFloat scrollHeight = fDeviceHeight-50-bottomPadding;
            
            scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0, frameX, scrollHeight)];
        }
        
    }
    
    

        scrollView.pagingEnabled = YES;
        scrollView.delegate=self;
        scrollView.backgroundColor = [UIColor clearColor];

        self.automaticallyAdjustsScrollViewInsets = NO;
        [self.view addSubview: scrollView];
  
    
        // Add images and text according to data model
        for(int i = 0; i < [jsonDic count]; i++)
        {
            
            //=============== Programatically adding center UIImageview===============
            
            
            NSString *imgUrl;
            NSString *pageTitles;
            NSString *pageSubTitles;
            NSString *colorcode;

            
            imgUrl=[[jsonDic valueForKey:@"image"] objectAtIndex:i];
            pageTitles=[[jsonDic valueForKey:@"title"] objectAtIndex:i];
            
            pageSubTitles=[[jsonDic valueForKey:@"desc"] objectAtIndex:i];

            colorcode=[[jsonDic valueForKey:@"colorcode"] objectAtIndex:i];
            
            pageControl = [[UIPageControl alloc] init]; //SET a property of UIPageControl

            
            UIView*  vw_imgBg = [[UIView alloc] init];
            UIImageView*  imageView = [[UIImageView alloc] init];

           
         if ([[UIScreen mainScreen]bounds].size.height <= 568)
            {
                vw_imgBg.frame=CGRectMake(frameX*i, 0, fDeviceWidth, rectsize.height/1.8);
                imageView.frame= CGRectMake(frameX*i+(frameX/ 2 - frameY/4), 34, frameY/2, frameY/2);
                pageControl.frame = CGRectMake(rectsize.width/2-60,rectsize.height-80,120,40);

            }
            
            else
            {
                
                vw_imgBg.frame=CGRectMake(frameX*i, 0, fDeviceWidth, rectsize.height/1.5);
                // add for iphone X to set page controller frame
                if(iPhoneX())
                {
                  //  imageView.frame= CGRectMake(frameX*i+(frameX/ 2 - frameY/4), 30+84, frameY/2, frameY/2);
                    CGFloat yCod = vw_imgBg.frame.size.height/2 - frameY/4;
                    NSLog(@"yCod = %f ",yCod);
                    // align center
                    imageView.frame= CGRectMake(frameX*i+(frameX/ 2 - frameY/4),yCod , frameY/2, frameY/2);

                    pageControl.frame = CGRectMake(rectsize.width/2-60,rectsize.height-100-30,120,40);
                }
                else
                {
              //  imageView.frame= CGRectMake(frameX*i+(frameX/ 2 - frameY/4), 30+64, frameY/2, frameY/2);
                    
                    CGFloat yCod = vw_imgBg.frame.size.height/2 - frameY/4;
                    NSLog(@"yCod = %f ",yCod);
                    
                    imageView.frame= CGRectMake(frameX*i+(frameX/ 2 - frameY/4),yCod , frameY/2, frameY/2);
                pageControl.frame = CGRectMake(rectsize.width/2-60,rectsize.height-100,120,40);
                }
            }

            imageView.image= [UIImage imageNamed:imgUrl];
            
            
            if (networkStatus == NotReachable) {
                imageView.image= [UIImage imageNamed:imgUrl];
            }
            else
            {
                [imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl]
                             placeholderImage:[UIImage imageNamed:@"tour_default.png"]];
                
            }
            scrollView.backgroundColor=[UIColor clearColor];
            
            [scrollView addSubview:vw_imgBg];

            [scrollView addSubview:imageView];

            if(colorcode == (NSString *)[NSNull null] || colorcode.length==0)
            {
                vw_imgBg.backgroundColor= [UIColor clearColor];

            }
            else
            {
                vw_imgBg.backgroundColor= [self colorFromHexString:colorcode];

            }

            //=============== Programatically adding title label====================
            UILabel  * lbl_title = [[UILabel alloc] init];
            lbl_title.frame=CGRectMake(frameX*i,vw_imgBg.frame.origin.y+ vw_imgBg.frame.size.height+10, rectsize.width, 50);

 
            
            
            lbl_title.textColor= [UIColor colorWithRed:53.0/255.0 green:184.0/255.0 blue:108.0/255.0 alpha:1];
            lbl_title.numberOfLines=2;
            lbl_title.lineBreakMode=NSLineBreakByClipping;//UILineBreakModeCharacterWrap
            lbl_title.textAlignment = NSTextAlignmentCenter;
            lbl_title.text=[NSString stringWithFormat:@"%@",pageTitles];
            //[lbl_title setFont:[UIFont boldSystemFontOfSize:21]];
            lbl_title.backgroundColor=[UIColor clearColor];

            [scrollView addSubview:lbl_title];
            
            //=============== Programatically adding sub title label====================


            UITextView  * lbl_subtitle = [[UITextView alloc] init];
            
            if (IPAD)
            {
                lbl_subtitle.frame=CGRectMake(frameX*i+fDeviceWidth/4+50,lbl_title.frame.origin.y+ lbl_title.frame.size.height+2,fDeviceWidth-fDeviceWidth/2-100, 100);

            }
            else
            {
                lbl_subtitle.frame=CGRectMake(frameX*i+50,lbl_title.frame.origin.y+ lbl_title.frame.size.height+2,fDeviceWidth-100, 100);

            }
            
            lbl_subtitle.textAlignment = NSTextAlignmentCenter;
            lbl_subtitle.text=[NSString stringWithFormat:@"%@",pageSubTitles];
            lbl_subtitle.backgroundColor=[UIColor clearColor];
            
            lbl_subtitle.textColor= [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1];

            
            
            
           // lbl_title.backgroundColor=[UIColor orangeColor];
          //  lbl_subtitle.backgroundColor=[UIColor blueColor];
          /*  for (NSString* family in [UIFont familyNames])
            {
                NSLog(@"family %@", family);
                
                for (NSString* name in [UIFont fontNamesForFamilyName: family])
                {
                    NSLog(@"name  %@", name);
                }
            }
            */
            
            if (fDeviceWidth<=320)
            {
              //  [lbl_subtitle setFont:[UIFont systemFontOfSize:14]];
                lbl_title.font = [UIFont fontWithName:@"Rubrik-Medium" size:19];

                lbl_subtitle.font = [UIFont fontWithName:@"Rubrik-Regular" size:14];
            }
            else
            {
                lbl_title.font = [UIFont fontWithName:@"Rubrik-Medium" size:23];

               // [lbl_subtitle setFont:[UIFont systemFontOfSize:18]];
                lbl_subtitle.font = [UIFont fontWithName:@"Rubrik-Regular" size:18];
            }
            
            [lbl_subtitle setUserInteractionEnabled:NO];
            [scrollView addSubview:lbl_subtitle];

        }
        scrollView.contentSize = CGSizeMake(frameX*[jsonDic count],scrollView.frame.size.height);

        scrollView.showsHorizontalScrollIndicator = NO;//hide scrollbar
        scrollView.showsVerticalScrollIndicator = NO;
        
    
        //--------- Add page control in the scrollview ----------
        pageControl.numberOfPages = [jsonDic count]; //as we added 3 diff views
        pageControl.currentPage = 0;
        pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    
       //UIColor *tintColouring=[UIColor colorWithRed:53.0/255.0 green:184.0/255.0 blue:108.0/255.0 alpha:1];
        UIColor *tintColouring=[UIColor darkGrayColor];
        pageControl.currentPageIndicatorTintColor =  tintColouring;
        [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: pageControl];
    
    //btnBack.titleLabel.font = [UIFont fontWithName:@"Rubrik-Regular" size:16];
    btnSignUp.titleLabel.font = [UIFont fontWithName:@"Rubrik-Regular" size:16];
    btn_login.titleLabel.font = [UIFont fontWithName:@"Rubrik-Regular" size:16];

 //   [self.scrollView bringSubviewToFront:nvView.leftBackButton]; //use to bring button subview to top

    [self addNavigationView];
}




-(void)hitAPI
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_NEWTOUR_IMAGE withBody:dictBody andTag:TAG_REQUEST_NEWTOUR_IMAGE completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     
     {
         [hud hideAnimated:YES];
         
         if (error == nil) {
             NSLog(@"Server Response = %@",response);
          
             //Check selcted locale
             NSString *selectedLocale = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
             if (selectedLocale.length)
             {
                 selectedLocale = [[selectedLocale componentsSeparatedByString:@"-"] firstObject];
             }
             else
             {
                 selectedLocale = @"en";
             }
   
             
             finalDic = (NSDictionary *)[[[response valueForKey:@"pd"] valueForKey:selectedLocale] mutableCopy];
        
             
             [self drawTour:finalDic];
         }
         else{
             NSLog(@"Error Occured = %@",error.localizedDescription);
           
             [self loadLocalTour];
             
         }
         
     }];
    
}



 //---------------------------------------------------------------------------------------------
 //---------------------------------------------------------------------------------------------
 //---------------------   NEW USP SCREEN  START  ----------------------------------------------
 //---------------------------------------------------------------------------------------------
 //---------------------------------------------------------------------------------------------
 //---------------------------------------------------------------------------------------------


 


//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------   OLD USP SCREEN  START  ----------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

/*

 -(void)hitAPI
 {
 
 
 
 hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 
 
 UMAPIManager *objRequest = [[UMAPIManager alloc] init];
 
 NSMutableDictionary *dictBody = [NSMutableDictionary new];
 
 [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_TOUR_IMAGE withBody:dictBody andTag:TAG_REQUEST_TOUR_IMAGE completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
 
 
 {
 [hud hideAnimated:YES];
 
 if (error == nil) {
 NSLog(@"Server Response = %@",response);
 //------ save value in nsuserdefault for relanch app
 //[[NSUserDefaults standardUserDefaults] setObject:response forKey:@"TOUR_Key"];
 //[[NSUserDefaults standardUserDefaults] synchronize];
 
 
 
 //Check selcted locale
 NSString *selectedLocale = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
 if (selectedLocale.length)
 {
 selectedLocale = [[selectedLocale componentsSeparatedByString:@"-"] firstObject];
 }
 else
 {
 selectedLocale = @"en";
 }
 
 ///------- fix lint-----
 
 //  NSDictionary* finalDic=[[NSDictionary alloc]init];
 //  finalDic = [[[response valueForKey:@"pd"] valueForKey:selectedLocale] mutableCopy];
 
 
 
 finalDic = (NSDictionary *)[[[response valueForKey:@"pd"] valueForKey:selectedLocale] mutableCopy];
 //            NSDictionary *hindiDic=[[NSDictionary alloc]init];
 //            hindiDic=[[response valueForKey:@"pd"] valueForKey:@"hi"];
 //            NSLog(@" hindiDic =%@",hindiDic);
 //
 //
 //            NSDictionary *engDic=[[NSDictionary alloc]init];
 //            engDic=[[response valueForKey:@"pd"] valueForKey:@"en"];
 //            NSLog(@" engDic =%@",engDic);
 //
 
 
 //            NSDictionary* finalDic=[[NSDictionary alloc]init];
 //
 //            if ([preferedLocale isEqualToString:@"en"]) {
 //                finalDic=[engDic mutableCopy];
 //
 //            }
 //            else
 //            {
 //                finalDic=[hindiDic mutableCopy];
 //
 //            }
 
 [self drawTour:finalDic];
 }
 else{
 NSLog(@"Error Occured = %@",error.localizedDescription);
 
 
 [self loadLocalTour];
 
 }
 
 }];
 
 }
 
 
-(void)drawTour:(NSDictionary*)responsedic
{
    
    jsonDic=responsedic;
    
    for(UIView *subview in scrollView.subviews)
    {
        [subview removeFromSuperview];
    }
    
    
    for (UIView *view in [self.view subviews])
    {
        if ([view isKindOfClass:[UIScrollView class]])
        {
            [view removeFromSuperview];
            
        }
        if ([view isKindOfClass:[UIPageControl class]])
        {
            [view removeFromSuperview];
            
        }
        
    }
    if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation))
    {
        // Bound the screen according to width size of the screen
        CGSize size = [[UIScreen mainScreen] bounds].size;
        CGFloat frameX = size.width;
        CGFloat frameY = size.height-57; //padding for UIpageControl
        
        //Add scrollview programatically
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,64, frameX, frameY-80)];
        scrollView.pagingEnabled = YES;
        scrollView.delegate=self;
        scrollView.backgroundColor = [UIColor clearColor];
        self.automaticallyAdjustsScrollViewInsets = NO;
        
        [self.view addSubview: scrollView];
        
        CGFloat imgframeY = (size.height-100)/4; //padding for UIpageControl
        
        // Add images and text according to data model
        for(int i = 0; i < [jsonDic count]; i++)
        {
            
            //=============== Programatically adding center UIImageview===============
            
            
            NSString *imgUrl;
            NSString *pageTitles;
            NSString *pageSubTitles;
            
            
            imgUrl=[[jsonDic valueForKey:@"image"] objectAtIndex:i];
            pageTitles=[[jsonDic valueForKey:@"title"] objectAtIndex:i];
            pageSubTitles=[[jsonDic valueForKey:@"desc"] objectAtIndex:i];
            
            
            UIImageView*  imageView = [[UIImageView alloc] init];
            
            imageView.frame = CGRectMake(frameX*i+(frameX/ 2 - frameX/4), imgframeY-20, frameX/2, frameX/2);
            
            
            
            // imageView.image= [UIImage imageNamed:imgUrl];
            if (networkStatus == NotReachable) {
                
                imageView.image= [UIImage imageNamed:imgUrl];
                
            }
            else
            {
                [imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl]
                             placeholderImage:[UIImage imageNamed:@"tour_default.png"]];
                
            }
            scrollView.backgroundColor=[UIColor clearColor];
            [scrollView addSubview:imageView];
            
            //=============== Programatically adding title label====================
            UILabel  * lbl_title = [[UILabel alloc] initWithFrame: CGRectMake(frameX*i, imageView.frame.origin.y-70, size.width, 100)];
            lbl_title.numberOfLines=2;
            lbl_title.lineBreakMode=NSLineBreakByClipping;//UILineBreakModeCharacterWrap
            lbl_title.textAlignment = NSTextAlignmentCenter;
            lbl_title.text=[NSString stringWithFormat:@"%@",pageTitles];
            [lbl_title setFont:[UIFont boldSystemFontOfSize:21]];
            [scrollView addSubview:lbl_title];
            
            //=============== Programatically adding sub title label====================
            UITextView  * lbl_subtitle = [[UITextView alloc] initWithFrame: CGRectMake(frameX*i+50,imageView.frame.origin.y+imageView.frame.size.height+10, fDeviceWidth-100, 100)];
            lbl_subtitle.textAlignment = NSTextAlignmentCenter;
            lbl_subtitle.text=[NSString stringWithFormat:@"%@",pageSubTitles];
            lbl_subtitle.backgroundColor=[UIColor clearColor];
            if (fDeviceWidth<=320)
            {
                [lbl_subtitle setFont:[UIFont systemFontOfSize:14]];
                
            }
            else
            {
                [lbl_subtitle setFont:[UIFont systemFontOfSize:18]];
                
            }
            
            [lbl_subtitle setUserInteractionEnabled:NO];
            [scrollView addSubview:lbl_subtitle];
        }
        //scrollView.contentSize = CGSizeMake(frameX*[jsonDic count],scrollView.frame.size.height);
        
        float sizeOfContent = 0;
        UIView *lLast = [scrollView.subviews lastObject];
        NSInteger wd = lLast.frame.origin.y;
        NSInteger ht = lLast.frame.size.height;
        
        sizeOfContent = wd+ht;
        
        scrollView.contentSize = CGSizeMake(frameX*[jsonDic count], sizeOfContent);
        
        
        
        scrollView.showsHorizontalScrollIndicator = NO;//hide scrollbar
        scrollView.showsVerticalScrollIndicator = NO;
        //--------- Add page control in the scrollview ----------
        pageControl = [[UIPageControl alloc] init]; //SET a property of UIPageControl
        pageControl.frame = CGRectMake(0,size.height-90,fDeviceWidth,40);
        pageControl.numberOfPages = [jsonDic count]; //as we added 3 diff views
        pageControl.currentPage = 0;
        //pageControl.backgroundColor=[UIColor orangeColor];
        pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
        pageControl.backgroundColor = [UIColor clearColor];
        [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventTouchUpInside];
        
        // scrollView.backgroundColor=[UIColor orangeColor];
        
        [self.view addSubview: pageControl];
        
    }
    else
    {
        // Bound the screen according to width size of the screen
        CGSize size = [[UIScreen mainScreen] bounds].size;
        CGFloat frameX = size.width;
        CGFloat frameY = size.height-57; //padding for UIpageControl
        
        //Add scrollview programatically
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,64, frameX, frameY-80)];
        scrollView.pagingEnabled = YES;
        scrollView.delegate=self;
        scrollView.backgroundColor = [UIColor clearColor];
        self.automaticallyAdjustsScrollViewInsets = NO;
        
        [self.view addSubview: scrollView];
        
        CGFloat imgframeY = (size.height-100)/4; //padding for UIpageControl
        
        // Add images and text according to data model
        for(int i = 0; i < [jsonDic count]; i++)
        {
            
            //=============== Programatically adding center UIImageview===============
            
            
            NSString *imgUrl;
            NSString *pageTitles;
            NSString *pageSubTitles;
            
            
            imgUrl=[[jsonDic valueForKey:@"image"] objectAtIndex:i];
            pageTitles=[[jsonDic valueForKey:@"title"] objectAtIndex:i];
            pageSubTitles=[[jsonDic valueForKey:@"desc"] objectAtIndex:i];
            
            
            UIImageView*  imageView = [[UIImageView alloc] init];
            
            imageView.frame = CGRectMake(frameX*i+(frameX/ 2 - frameY/4), imgframeY-40, frameY/2, frameY/2);
            
            
            
            //imageView.image= [UIImage imageNamed:imgUrl];
            
            
            if (networkStatus == NotReachable) {
                
                imageView.image= [UIImage imageNamed:imgUrl];
                
            }
            else
            {
                [imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl]
                             placeholderImage:[UIImage imageNamed:@"tour_default.png"]];
                
            }
            
            [scrollView addSubview:imageView];
            
            //=============== Programatically adding title label====================
            UILabel  * lbl_title = [[UILabel alloc] initWithFrame: CGRectMake(frameX*i, imageView.frame.origin.y-90, size.width, 100)];
            lbl_title.numberOfLines=2;
            lbl_title.lineBreakMode=NSLineBreakByClipping;//UILineBreakModeCharacterWrap
            lbl_title.textAlignment = NSTextAlignmentCenter;
            lbl_title.text=[NSString stringWithFormat:@"%@",pageTitles];
            [lbl_title setFont:[UIFont boldSystemFontOfSize:21]];
            [scrollView addSubview:lbl_title];
            
            //=============== Programatically adding sub title label====================
            UITextView  * lbl_subtitle = [[UITextView alloc] initWithFrame: CGRectMake(frameX*i+50,imageView.frame.origin.y+imageView.frame.size.height+10, fDeviceWidth-100, 100)];
            lbl_subtitle.textAlignment = NSTextAlignmentCenter;
            lbl_subtitle.text=[NSString stringWithFormat:@"%@",pageSubTitles];
            lbl_subtitle.backgroundColor=[UIColor clearColor];
            if (fDeviceWidth<=320)
            {
                [lbl_subtitle setFont:[UIFont systemFontOfSize:14]];
                
            }
            else
            {
                [lbl_subtitle setFont:[UIFont systemFontOfSize:18]];
                
            }
            
            [lbl_subtitle setUserInteractionEnabled:NO];
            [scrollView addSubview:lbl_subtitle];
        }
        scrollView.contentSize = CGSizeMake(frameX*[jsonDic count],scrollView.frame.size.height);
        scrollView.showsHorizontalScrollIndicator = NO;//hide scrollbar
        scrollView.showsVerticalScrollIndicator = NO;
        
        // scrollView.backgroundColor=[UIColor greenColor];
        
        //--------- Add page control in the scrollview ----------
        pageControl = [[UIPageControl alloc] init]; //SET a property of UIPageControl
        pageControl.frame = CGRectMake(size.width/2-60,size.height-100,120,40);
        pageControl.numberOfPages = [jsonDic count]; //as we added 3 diff views
        pageControl.currentPage = 0;
        //pageControl.backgroundColor=[UIColor orangeColor];
        pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
        // pageControl.backgroundColor = [UIColor clearColor];
        [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: pageControl];
    }
    
    
}
*/
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//-----------------------   OLD USP SCREEN  END  ----------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------








- (void)viewDidLoad
{
    nvView = [[NavigationView alloc] init];//initialise navigationview for all
    
    [btn_login setTitle:NSLocalizedString(@"login_caps", nil) forState:UIControlStateNormal];
    [btnSignUp setTitle:NSLocalizedString(@"register_caps", nil) forState:UIControlStateNormal];
    
    
    
    singleton=[SharedManager sharedSingleton];
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:TAB_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    /*
     [(UIButton*)btn_skip setTitle:NSLocalizedString(@"skip", @"") forState:UIControlStateNormal];
     [(UIButton*)btn_next setTitle:NSLocalizedString(@"next", @"") forState:UIControlStateNormal];
     */
    
    
    [btn_login setTitle: NSLocalizedString(@"login_caps", nil) forState:UIControlStateNormal];
    
    [btn_signUp setTitle:NSLocalizedString(@"sign_up",nil) forState:UIControlStateNormal];
    
    
    NSString * str = [[NSUserDefaults standardUserDefaults] valueForKey:KEY_PREFERED_LOCALE];
    if(!str)
    {
        str = TXT_LANGUAGE_ENGLISH;// @"english";
        [[NSUserDefaults standardUserDefaults] setValue:TXT_LANGUAGE_ENGLISH/*@"english"*/ forKey:KEY_PREFERED_LOCALE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    preferedLocale = [[NSString alloc] initWithString:str];
    //    [self call_FetchUSPScreen]; //done
    
    
    
    
    //  AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    networkStatus = [networkReachability currentReachabilityStatus];
    /* if (networkStatus == NotReachable) {
     
     [self loadLocalTour];
     } else {
     
     
     NSLog(@"There IS internet connection");
     [self hitAPI];
     
     }
     
     */
    
    
    if (![self connected]) {
        // Not connected
        [self loadLocalTour];
        
    } else {
        // Connected. Do some Internet stuff
        [self hitAPI];
        
    }
    
    
    
    
    
    // Update Key for Language Screen
    [[NSUserDefaults standardUserDefaults] setInteger:kTutorialScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [btnBack addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self setNeedsStatusBarAppearanceUpdate];
   // self.view.backgroundColor=[UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1];
    
    self.view.backgroundColor=[UIColor whiteColor];

    [self addNavigationView];
    [super viewDidLoad];
    
    
}


- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStats = [reachability currentReachabilityStatus];
    return networkStats != NotReachable;
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    for (UIView *subView in self.view.subviews) {    // UIView.subviews
        if (subView.tag == 899) {
            NSLog(@"delete headerview");
            [subView removeFromSuperview];
        }
    }

    
    btnBack.hidden = true;
    [btnBack setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [btnBack setImage:[UIImage imageNamed:@"adv_back_arrow"] forState:UIControlStateNormal];

    [nvView.leftBackButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [nvView.leftBackButton setImage:[UIImage imageNamed:@"adv_back_arrow"] forState:UIControlStateNormal];

   // lbltitle.hidden = true;
    //NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.bgImage.hidden=true;
    nvView.backgroundColor=[UIColor clearColor];
    nvView.tag=899;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backBtnAction:btnBack];
    };
   /// nvView.lblTitle.font = [AppFont semiBoldFont:17];
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
   
    
}

-(void)btnBackNewClicked
{
    /*
     
     [self dismissViewControllerAnimated:YES
     completion:nil ];
     
     
     */
    [self backBtnAction:nil];
    
    //    BOOL isContPresent = NO;
    //
    //    UIViewController *vc = self.presentingViewController;
    //    while (vc.presentingViewController) {
    //        vc = vc.presentingViewController;
    //
    //        if ([vc isKindOfClass:[LanguageSelectVC class]])
    //        {
    //            [vc dismissViewControllerAnimated:YES completion:NULL];
    //
    //
    //        }
    //
    //    }
    //
    //    if (isContPresent == NO) {
    //        UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //        LanguageSelectVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LanguageSelectVC"];
    //        [APP_DELEGATE.window setRootViewController:vc];
    //
    //        [UIView transitionWithView:APP_DELEGATE.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
    //    }
    
}


-(IBAction)backBtnAction:(id)sender
{
    BOOL isContPresent = YES;
      [[SharedManager sharedSingleton] traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"App Tutorial Screen" andValue:0];
    UIViewController *vc = self.presentingViewController;
    //if (vc.presentingViewController) {
       // vc = vc.presentingViewController;
        
        if ([vc isKindOfClass:[NewLanguageSelectVC class]])
        {
            isContPresent = YES;
            [vc dismissViewControllerAnimated:YES completion:NULL];
        }
        
   // }
    
    if (isContPresent == NO) {
        UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
        NewLanguageSelectVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewLanguageSelectVC"];
        [APP_DELEGATE.window setRootViewController:vc];
        [UIView transitionWithView:APP_DELEGATE.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
    }
    
}
-(IBAction)loginBtnAction:(id)sender
{
    //----later add
    [[SharedManager sharedSingleton] traceEvents:@"Login Button" withAction:@"Clicked" withLabel:@"App Tutorial Screen" andValue:0];

    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------
    LoginAppVC *vc;
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
}


-(IBAction)SignUpAction:(id)sender
{
    [[SharedManager sharedSingleton] traceEvents:@"Signup Button" withAction:@"Clicked" withLabel:@"App Tutorial Screen" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
    
    /*ChooseRegistrationTypeVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChooseRegistrationTypeVC"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     [self presentViewController:vc animated:YES completion:nil];*/
    
    
    NSLog(@"Register Pressed");
    
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    /*ChooseRegistrationTypeVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChooseRegistrationTypeVC"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     [self presentViewController:vc animated:YES completion:nil];*/
    
    
    if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        mobileReg = [[MobileRegistrationVC alloc] initWithNibName:@"MobileRegistrationVC_iPad" bundle:nil];
        //loadNibNamed:@"MobileRegistrationVC_ipad" owner:self options:nil];
        // cell = (MobileRegistrationVC_ipad *)[nib objectAtIndex:0];
        [self presentViewController:mobileReg animated:YES completion:nil];
        
    }
    else
    {
        mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegistrationVC"];
        [mobileReg setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:mobileReg animated:YES completion:nil];
    }
    
    
    
    
}



-(void)loadLocalTour
{
    //NSArray *imageArray=[NSArray arrayWithObjects:@"tour1.png",@"tour2.png",@"tour4.png",@"tour3.png", nil];
    //uncomment below line imageArray and comment above line for new USP
    NSArray *imageArray=[NSArray arrayWithObjects:@"newimg_01.png",@"newimg_02.png",@"newimg_03.png",@"newimg_04.png",@"newimg_05.png", nil];

    NSArray *pagetitleArray=[NSArray arrayWithObjects:NSLocalizedString(@"tour_title1", @""),NSLocalizedString(@"tour_title2", @""),NSLocalizedString(@"tour_title3", @""),NSLocalizedString(@"tour_title4", @""),NSLocalizedString(@"tour_title5", @""), nil];
    
    
    NSArray *pageSubtitleArray=[NSArray arrayWithObjects:NSLocalizedString(@"tour_desc1", @""),NSLocalizedString(@"tour_desc2", @""),NSLocalizedString(@"tour_desc3", @""),NSLocalizedString(@"tour_desc4", @""),NSLocalizedString(@"tour_desc5", @""), nil];
    
    
     NSArray *colourCodeArray=[NSArray arrayWithObjects:@"#42a5f5",@"#a39ddb",@"#81c784",@"#4cbeb2",@"#ce93d8", nil];


    tourdic=[NSMutableDictionary new];
    
    
    
    
    NSMutableArray *tourArr=[[NSMutableArray alloc]init];
    
    
    
    for (int i=0; i<[imageArray count]; i++)
    {
        NSMutableDictionary *dic=[NSMutableDictionary new];
        [dic setObject:[imageArray objectAtIndex:i] forKey:@"image"];
        [dic setObject:[pagetitleArray objectAtIndex:i] forKey:@"title"];
        [dic setObject:[pageSubtitleArray objectAtIndex:i] forKey:@"desc"];
        [dic setObject:[colourCodeArray objectAtIndex:i] forKey:@"colorcode"];

        
        NSLog(@"dicitonary = %@",dic);
        
        [tourArr addObject:dic];
    }
    
    [tourdic setValue:tourArr forKey:@"tour"];
    
    
    [self drawTour:[tourdic valueForKey:@"tour"]];
    
    
}
#pragma Scrollview delegate function for scroll with page enable
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    CGFloat frameX = size.width;
    
    
    //CGFloat pageWidth = self.scrollView.frame.size.width;
    page = floor((self.scrollView.contentOffset.x - frameX / 2 ) / frameX) + 1; //this provide you the page number
    pageControl.currentPage = page;// this displays the white dot as current page
    
    [self changeBtnName:page];
}


// -------- Method to change page on click of page controll--------------
- (IBAction)changePage:(id)sender
{
    UIPageControl *pager=sender;
    NSInteger myInteger = pager.currentPage;
    page = (int) myInteger;
    NSLog(@"page=%d",page);
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    
    //[self changeBtnName:page];
    
}

//------- Method to change name of the UIButton---------

-(void)changeBtnName:(int)pageIndex
{
    /* if (pageIndex==[jsonDic count]-1) {
     btn_skip.hidden=TRUE;
     [(UIButton*)btn_next setTitle:NSLocalizedString(@"finish", @"") forState:UIControlStateNormal];
     }
     else
     {
     btn_skip.hidden=FALSE;
     [(UIButton*)btn_next setTitle:NSLocalizedString(@"next", @"") forState:UIControlStateNormal];
     
     }*/
}

//------ Method Finish Press action ------------------
-(IBAction)finishPressed:(id)sender
{
    NSLog(@"title of button=%@",btn_next.titleLabel.text);
     [[SharedManager sharedSingleton] traceEvents:@"Finish Button" withAction:@"Clicked" withLabel:@"App Tutorial Screen" andValue:0];
    if ([btn_next.titleLabel.text isEqualToString:NSLocalizedString(@"finish", @"")])
    {
        //call skip pressed action
        [self skipPressed:self];
    }
    else
    {
        
        // NSLog(@"page=%d",page);
        page=page+1;// get current page number globally define and add +1 to get next
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        [scrollView scrollRectToVisible:frame animated:YES];
        //[self changeBtnName:page];
        
    }
    
}

//--------------Method Skip Pressed to go to next View by skip current--------------
- (IBAction)skipPressed:(id)sender
{
    [[SharedManager sharedSingleton] traceEvents:@"Skip Button" withAction:@"Clicked" withLabel:@"App Tutorial Screen" andValue:0];

    LoginAppVC *vc;
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    /* UserEditVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserEditVC"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     [self presentViewController:vc animated:YES completion:nil];
     
     */
    
    
}

-(void)hitInitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    singleton = [SharedManager sharedSingleton];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            singleton.arr_initResponse=[[NSMutableDictionary alloc]init];
            singleton.arr_initResponse=[response valueForKey:@"pd"];
            NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
            
            [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
            
            NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
            NSLog(@"value of abbr=%@",abbr);
            NSString*  infoTab=[singleton.arr_initResponse valueForKey:@"infotab"];
[[NSUserDefaults standardUserDefaults] setObject:[infoTab capitalizedString] forKey:@"infotab"];
            if ([abbr length]==0)
            {
                abbr=@"";
            }
            
            singleton.user_StateId = [singleton.arr_initResponse valueForKey:@"ostate"];
            [singleton setStateId:singleton.user_StateId];
            
            NSString *emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
            emblemString = emblemString.length == 0 ? @"":emblemString;
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[abbr capitalizedString] forKey:@"ABBR_KEY"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            /* facebooklink
             faq
             forceupdate
             googlepluslink
             opensource
             privacypolicy
             splashScreen
             tabordering
             termsandcondition
             twitterlink
             ver
             vermsg
             */
            
            //------ save value in nsuserdefault for relanch app
            // [[NSUserDefaults standardUserDefaults] setObject:response forKey:@"TOUR_Key"];
            //[[NSUserDefaults standardUserDefaults] synchronize];
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
        }
        
    }];
    
}
-(void)viewWillAppear:(BOOL)animated
{
   //uncomment it for new USP
     
     
     [btn_login setTitleColor:[self colorFromHexString:@"6CB1F5"] forState:UIControlStateNormal];
     [btnSignUp setTitleColor:[self colorFromHexString:@"6CB1F5"] forState:UIControlStateNormal];
     btn_login.backgroundColor= [UIColor whiteColor];
     btnSignUp.backgroundColor= [UIColor whiteColor];
     
 
    [[btn_login layer] setCornerRadius:0.5f];
    [[btn_login layer] setMasksToBounds:YES];

    UIColor *grayColorMe=[UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1];

   

    [[btn_login layer] setBorderWidth:1.0f];

    [[btnSignUp layer] setCornerRadius:0.5f];
    [[btnSignUp layer] setMasksToBounds:YES];
    [[btnSignUp layer] setBorderWidth:1.0f];
    
    btn_login.layer.borderColor = grayColorMe.CGColor;
    btnSignUp.layer.borderColor = grayColorMe.CGColor;

  
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    [self performSelector:@selector(hitInitAPI) withObject:nil afterDelay:1];
    
    
    
    //[[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [super viewWillAppear:NO];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    //[[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    
}

- (void)orientationChanged:(NSNotification *)notification{
    // [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    //close for orientation change
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation
{
    
    if (finalDic==nil) {
        [self drawTour:[tourdic valueForKey:@"tour"]];
        
    }
    else
    {
        [self drawTour:finalDic];
        
    }
    
    
    
    
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

