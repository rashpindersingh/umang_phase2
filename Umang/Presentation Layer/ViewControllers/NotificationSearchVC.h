//
//  NotificationSearchVC.h
//  Umang
//
//  Created by admin on 18/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationSearchVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblSearchNotification;

- (IBAction)btnCancelClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vwTextBG;

@end
