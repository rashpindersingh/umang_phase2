//
//  LogoutSessionVC.m
//  Umang
//
//  Created by Rashpinder on 29/01/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "LogoutSessionVC.h"
#import "MBProgressHUD.h"
#import "RunOnMainThread.h"
#import "SocialAuthentication.h"
#import "LoginAppVC.h"
#import "UIImageView+WebCache.h"


@implementation CellLogoutSession

@end
@interface LogoutSessionVC ()
{
    MBProgressHUD *hud;
    SharedManager *singleton;
    NSMutableArray *arrSessions;
    NSIndexPath *selectedPath;
    BOOL isAllLogout;
    IBOutlet NSLayoutConstraint *navHeightConstraint;
}
@end

@implementation LogoutSessionVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:LOGGED_IN_SESSION_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    self.btnLogoutAllSessions.hidden = true;
    isAllLogout = false ;
    self.tblSessions.tableFooterView = [[UIView alloc] init];
    singleton = [SharedManager sharedSingleton];
    arrSessions = [[NSMutableArray alloc] init];
    [self hitAPIGetCurrentUserSessions];
 
    
    self.tblSessions.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.view.backgroundColor  = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    self.tblSessions.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    self.titleOfView.text = NSLocalizedString(@"logged_in_sessions", nil);
    [self.backButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    [self.btnLogoutAllSessions setTitle:NSLocalizedString(@"logout_all_devices", nil) forState:UIControlStateNormal];
    
    [self.backButton.titleLabel setFont:[AppFont regularFont:17.0]];
    self.titleOfView.font = [AppFont semiBoldFont:17];
    
    if (iPhoneX())
    {
        navHeightConstraint.constant = 84;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Api call to delete session
-(void)hitAPIGetCurrentUserSessions
{
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"aadhr"];
    [dictBody setObject:@"" forKey:@"peml"];
    __weak __typeof(self) weakSelf = self;

    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_SESSIONS withBody:dictBody andTag:TAG_REQUEST_INIT_REG completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         if (error == nil) {
             [weakSelf processFetchSessionResponse:response];
             return;
         }else {
             NSLog(@"Error Occured = %@",error.localizedDescription);
             [self showToast:error.localizedDescription withDuration:2.0];
         }
      
     }];
    
}
-(void)processFetchSessionResponse:(NSDictionary*)response
{
    
    [hud hideAnimated:true];
    
    NSString *rc=[response valueForKey:@"rc"];
    NSString *rs=[response valueForKey:@"rs"];
    
    if ([rc isEqualToString:API_SUCCESS_CASE]||[rs isEqualToString:API_SUCCESS_CASE1])
    {
        arrSessions = [[NSMutableArray alloc] initWithArray:[[response valueForKey:@"pd"] valueForKey:@"sessionList"]];

        if (arrSessions.count > 0)
        {
            [arrSessions enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                if ([[[arrSessions objectAtIndex:idx] valueForKey:@"tkn"] isEqualToString:singleton.user_tkn])
                {
                    NSDictionary *tempDict = (NSDictionary *) [arrSessions objectAtIndex:idx];
                    
                    [arrSessions removeObjectAtIndex:idx];
                    [arrSessions insertObject:tempDict atIndex:0];
                }
                
            }];
        }
        
        
        __weak __typeof(self) weakSelf = self;
        
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            
            [weakSelf.tblSessions reloadData];
            self.btnLogoutAllSessions.hidden = false;
            
        }];
    }
}

-(void)showToast :(NSString *)toast withDuration:(double)duration
{
    [self.view makeToast:toast duration:duration position:CSToastPositionBottom];
}

- (IBAction)didTapBackButtonAction:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)didTapLogoutFromAlldevicesAction:(UIButton *)sender
{
    isAllLogout = true;
    NSString *strSession = @"";
    for (NSDictionary *dicSession in arrSessions) {
        if (strSession.length == 0) {
            strSession = [dicSession valueForKey:@"tkn"];
        }else {
            NSString *strTkn = [dicSession valueForKey:@"tkn"];
            strSession = [NSString stringWithFormat:@"%@,%@",strSession,strTkn];
        }
    }
     [self hitAPIDelete:strSession tag:sender.tag];
}
- (void)didTapLogoutFromDeviceAction:(UIButton *)sender
{
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:NSLocalizedString(@"end_session", nil), nil];
    
    sheet.tag = sender.tag;
    
    [sheet showInView:self.view];
    
}


-(void)clearLogoutData
{
        //singleton.user_tkn=tkn;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:NO forKey:@"LOGIN_KEY"];
        //[defaults setValue:@"" forKey:@"TOKEN_KEY"];
        [defaults synchronize];
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
        [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        //------------------------- Encrypt Value------------------------
        //——Remove Sharding Value——
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"jsonStringtoSave"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //----- remove sharding--------
     singleton = [SharedManager sharedSingleton];
         singleton.profileUserAddress = @"";
        singleton.stateSelected = @"";
        singleton.imageLocalpath=@"";
        singleton.notiTypeGenderSelected=@"";
        singleton.profileNameSelected =@"";
        singleton.profilestateSelected=@"";
        singleton.notiTypeCitySelected=@"";
        singleton.notiTypDistricteSelected=@"";
        singleton.profileDOBSelected=@"";
        singleton.altermobileNumber=@"";
        singleton.user_Qualification=@"";
        singleton.user_Occupation=@"";
        singleton.user_profile_URL=@"";
        singleton.profileEmailSelected=@"";
        singleton.mobileNumber=@"";
        singleton.user_id=@"";
        singleton.user_tkn=@"";
        singleton.user_mpin=@"";
        singleton.user_aadhar_number=@"";
        singleton.objUserProfile = nil;
        
        singleton.imageLocalpath=@"";
        singleton.stateSelected = @"";
    //added infor tab
    NSString *infotab=[NSString stringWithFormat:@"%@",[singleton.arr_initResponse valueForKey:@"infotab"]];
    singleton.infoTab = infotab;
    
        singleton.arr_initResponse = nil;
        
        //[[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"AccessTokenDigi"];
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"RefreshTokenDigi"];
        //[[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"dialogueID"];
        
        [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:[NSString stringWithFormat:@"userdisconnected~~~%@~~~exit",singleton.user_tkn] toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"lastFetchV1"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"AllTabState"];
        
        [[[AppDelegate sharedDelegate] xmppHandler] teardownStream];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Enable_ServiceDir"];
        
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastDirectoryFetchDate"];
        
        //delete service directory
        [singleton.dbManager deleteServicesDirectory];
        
        @try {
            [singleton.arr_recent_service removeAllObjects];
            //Blank recent service from NSUserDefaults

            [[NSUserDefaults standardUserDefaults] setObject: [NSKeyedArchiver archivedDataWithRootObject:singleton.arr_recent_service] forKey:@"recentServicesKey"];

            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
        
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_ID"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
        
        //------------------------- Encrypt Value------------------------
        
        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSHTTPCookie *cookie;
        for (cookie in [storage cookies])
        {
            
            [storage deleteCookie:cookie];
            
        }
        NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
        [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        //——Remove Sharding Value——
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //----- remove sharding--------
        
        
        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ABBR_KEY"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"EMB_STR"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"InitAPIResponse"];
        
        // Logout from social frameworks as well.
        SocialAuthentication *objSocial = [[SocialAuthentication alloc] init];
        [objSocial logoutFromAllSocialFramewors];
        objSocial = nil;
        
        
        
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //------------------------- Encrypt Value------------------------
        
        
        
        
        //[singleton.dbManager  deleteAllNotifications];
        [singleton.dbManager deleteBannerHomeData];
        [singleton.dbManager  deleteAllServices];
        [singleton.dbManager  deleteSectionData];
        [singleton.dbManager deleteBannerStateData];
    
        //[[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SELECTED_TAB_INDEX"];
        //[[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"]
        [self deleteCache];
        [singleton setStateId:@""];
        [defaults synchronize];
        [self openNextView];
        //[self removePrefrence];
        //[self alertwithMsg:rd];
    //added new method to clear values
    [singleton.dbManager clearSingletonProfileValue];
}
-(void)deleteCache
{
    NSFileManager *filemgr;
    filemgr = [NSFileManager defaultManager];
    if ([filemgr removeItemAtPath: [NSHomeDirectory() stringByAppendingString:@"/Documents/OfflineCache"] error: NULL]  == YES)
        NSLog (@"Remove successful");
    else
        NSLog (@"Remove failed");
}

-(void)openNextView
{
    
    //singleton=[SharedManager sharedSingleton];
    
    singleton.dbManager = [[UMSqliteManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    
    [singleton.dbManager createUmangDB];
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [singleton.reach stopNotifier];
//    LoginAppVC *vc;
//    //----later add
//    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
//    [[NSUserDefaults standardUserDefaults] setBool:false forKey:kKeepMeLoggedIn];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    //------
//    
//    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
//    
//    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
//    }
//    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//    AppDelegate *appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//    appD.window.rootViewController = vc;
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate setFlagshipTabBarVC:@"NO"];
    
}

#pragma mark - UIActionSheet Delegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            NSDictionary *dicSession = [arrSessions objectAtIndex:actionSheet.tag];
            NSString *strSession = [dicSession valueForKey:@"tkn"];
            [self hitAPIDelete:strSession tag:actionSheet.tag];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrSessions count];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGSize tblFrame = self.tblSessions.frame.size;
    UITableViewCell *cell = [[ UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tblFrame.width, 40)];
    cell.backgroundColor = [UIColor clearColor];
    
    /*UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, tblFrame.width, 40)];
    
    if (arrSessions.count != 0)
    {
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.text = @"Logged in devices";
        lbl.textColor = [UIColor colorWithRed:0.0/255.0 green:89.0/255.0 blue:157.0/255.0 alpha:1.0];///[UIColor colorWithRed:0.0/255.0 green:89.0/255.0 blue:157.0/255.0 alpha:1];
        lbl.font = [AppFont regularFont:16];
        lbl.numberOfLines = 10;
        lbl.lineBreakMode = NSLineBreakByWordWrapping;
        [cell addSubview:lbl];
    }*/
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIndentfier = singleton.isArabicSelected ? @"CellLogoutSession_Arabic" : @"CellLogoutSession";
    CellLogoutSession *cell = [tableView dequeueReusableCellWithIdentifier:strIndentfier forIndexPath:indexPath];
    NSDictionary *dicSession = [arrSessions objectAtIndex:indexPath.row];
    
    cell.btnEndSession.tag = indexPath.row;
    
    [cell.btnEndSession addTarget:self action:@selector(didTapLogoutFromDeviceAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[dicSession valueForKey:@"tkn"] isEqualToString:singleton.user_tkn])
    {
        cell.timeOfSession.text = NSLocalizedString(@"current_device", nil);
        cell.timeOfSession.textColor = [UIColor colorWithRed:0.0/255.0f green:89.0/255.0f blue:157.0/255.0f alpha:1.0f];
        [cell.timeOfSession setFont:[AppFont mediumFont:13]];
    }
    else
    {
        [cell.timeOfSession setFont:[AppFont lightFont:13]];
        cell.timeOfSession.text = [self getDateFormatString:[dicSession valueForKey:@"sdate"]];
        cell.timeOfSession.textColor = [UIColor blackColor];
    }
    
    //lbl_results.text=[NSString stringWithFormat:NSLocalizedString(@"result_found", nil),[[[response valueForKey:@"pd"] valueForKey:@"response"] valueForKey:@"numFound"]];
    cell.deviceName.font = [AppFont semiBoldFont:15];
    cell.deviceName.text = [dicSession valueForKey:@"logdev"];
    cell.IPAdress.text = [NSString stringWithFormat:NSLocalizedString(@"ip_address", nil),[dicSession valueForKey:@"ip"]] ;
    cell.IPAdress.font = [AppFont regularFont:14];
    
    NSURL *url = [NSURL URLWithString:[dicSession valueForKey:@"logdevimgurl"]];
    if(![[url absoluteString] isEqualToString:@""])
    {
        [cell.imageOfDevice sd_setImageWithURL:url
                             placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
    }
    else
    {
        cell.imageOfDevice.image=[UIImage imageNamed:@"img_loadertime.png"];
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //selectedPath = indexPath;
    //[self.tblSessions reloadData];
}

-(NSString*)getDateFormatString:(NSString*)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = @"yyyy-MM-dd HH:mm:ss.SSS";
    NSDate *nsDate = [dateFormat dateFromString:date];
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MMMM dd"];
    NSString *onlyDateString = [dateFormatter1 stringFromDate:nsDate];
    
    [dateFormatter1 setDateFormat:@"h:mm a"];
    NSString *onlyTimeString = [dateFormatter1 stringFromDate:nsDate];
    
    NSString *combinedString = [NSString stringWithFormat:@"%@ at %@",onlyDateString,onlyTimeString];
    
    //NSString *strDate = [MHPrettyDate prettyDateFromDate:nsDate withFormat:MHPrettyDateShortRelativeTime withDateStyle:NSDateFormatterLongStyle withTimeStyle:dateFormat.timeStyle];
    
    
    return combinedString;
}

#pragma mark - Api call to delete session
-(void)hitAPIDelete:(NSString*)sessions tag:(NSInteger)tagSession
{
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:sessions forKey:@"token"];
    [dictBody setObject:@"" forKey:@"peml"];
    __weak __typeof(self) weakSelf = self;
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_DELETE_SESSIONS withBody:dictBody andTag:TAG_REQUEST_DELETE_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         if (error == nil) {
             [weakSelf processDeleteSessionResponse:response tag:tagSession sessionDeleted:sessions];
             return;
         }else {
             NSLog(@"Error Occured = %@",error.localizedDescription);
             [self showToast:error.localizedDescription withDuration:2.0];
         }
         
     }];
    
}
-(void)processDeleteSessionResponse:(NSDictionary*)response tag:(NSInteger)tag  sessionDeleted:(NSString*)session {
    [hud hideAnimated:true];
    NSString *rc=[response valueForKey:@"rc"];
    NSString *rs=[response valueForKey:@"rs"];
    if ([rc isEqualToString:API_SUCCESS_CASE]||[rs isEqualToString:API_SUCCESS_CASE1])
    {
        if ([session isEqualToString:singleton.user_tkn] || isAllLogout ) {
            [self clearLogoutData];
            return;
        }else  {
            [arrSessions removeObjectAtIndex:tag];
            __weak __typeof(self) weakSelf = self;
            [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                [weakSelf.tblSessions reloadData];
            }];
        }
    }
    
}
@end
