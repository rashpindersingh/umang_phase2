//
//  LogoutSessionVC.h
//  Umang
//
//  Created by Rashpinder on 29/01/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "MHPrettyDate.h"
@interface CellLogoutSession:UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageOfDevice;
@property (weak, nonatomic) IBOutlet UILabel *deviceName;
@property (weak, nonatomic) IBOutlet UILabel *timeOfSession;
@property (weak, nonatomic) IBOutlet UILabel *IPAdress;
@property (weak, nonatomic) IBOutlet UIButton *btnEndSession;

@end

@interface LogoutSessionVC : UIViewController<UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleOfView;
@property (strong, nonatomic) IBOutlet UITableView *tblSessions;
@property (weak, nonatomic) IBOutlet UIButton *btnLogoutAllSessions;
@property (strong, nonatomic) IBOutlet UIButton *backButton;


@end
