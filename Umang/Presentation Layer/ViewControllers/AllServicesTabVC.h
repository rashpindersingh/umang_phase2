//
//  AllServicesTabVC.h
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface AllServicesTabVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>

{
    
    IBOutlet UITextField *txt_searchField;
    
    NSMutableArray *table_data;
    
    SharedManager *singleton;
    IBOutlet UIView *vw_line;
    
    UIRefreshControl *refreshController;
    IBOutlet UIImageView *searchIconImage;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnStateSelected;

@property(nonatomic, retain) IBOutlet UICollectionView *allSer_collectionView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentServiceType;

@property (nonatomic,copy) NSString *comingFromNotification;


-(IBAction)btn_filterAction:(id)sender;
-(IBAction)btn_noticationAction:(id)sender;




@end

