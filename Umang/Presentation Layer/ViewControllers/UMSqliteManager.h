//
//  UMSqliteManager.h
//  Umang
//
//  Created by deepak singh rawat on 20/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UMSqliteManager : NSObject
{
    sqlite3  *umangDB;
    
}


-(void)insertNotifData:(NSString*)notifId
            notifTitle:(NSString*)notifTitle
              notifImg:(NSString*)notifImg
              notifMsg:(NSString*)notifMsg
             notifType:(NSString*)notifType
             notifDate:(NSString*)notifDate
             notifTime:(NSString*)notifTime
            notifState:(NSString*)notifState
            notifIsFav:(NSString*)notifIsFav
             serviceId:(NSString*)serviceId
      currentTimeMills:(NSString*)currentTimeMills
               subType:(NSString*)subType
                   url:(NSString*)url
            screenName:(NSString*)screenName
       receiveDateTime:(NSString*)receiveDateTime
             dialogMsg:(NSString*)dialogMsg
          webpageTitle:(NSString*)webpageTitle;



-(void)insertServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite;


-(void)insertServiceSections:(NSString*)sectionName
                  sectionImg:(NSString*)sectionImg
             sectionServices:(NSString*)sectionServices;




-(void)updateNotifIsFav:(NSString*)notifId
             notifIsFav:(NSString*)notifIsFav;

-(void)updateServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite;


-(void)updateServiceIsFav:(NSString*)serviceId
             serviceIsFav:(NSString*)serviceIsFav hitAPI:(NSString*)hitStatus;


-(void)updateServiceIsNotifEnabled:(NSString*)serviceId
                    notifIsEnabled:(NSString*)notifIsEnabled;

-(NSString*)getServiceIsNotifEnabled:(NSString*)serviceId;

-(NSArray*)getTrendingServiceData;

-(void)deleteNotification:(NSString*)notifId;
-(void)deleteAllNotifications;
-(void)deleteServiceData:(NSString*)serviceId;
-(void)deleteAllServices;
-(void)deleteSectionData;

-(void)createUmangDB;
-(NSArray*)loadDataServiceSection;
-(NSArray*)loadDataServiceData;



-(NSArray*)getServiceData:(NSString*)serviceId;
-(NSString*)getServiceFavStatus:(NSString*)serviceId;


-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;



-(void)deleteBannerHomeData;

-(void)deleteBannerStateData;


-(void)insertBannerHomeData:(NSString*)bannerImgUrl
           bannerActionType:(NSString*)bannerActionType
            bannerActionUrl:(NSString*)bannerActionUrl
                 bannerDesc:(NSString*)bannerDesc;


-(void)insertBannerStateData:(NSString*)bannerImgUrl
            bannerActionType:(NSString*)bannerActionType
             bannerActionUrl:(NSString*)bannerActionUrl
                  bannerDesc:(NSString*)bannerDesc;

-(NSArray*)getBannerStateData;
-(NSArray*)getBannerHomeData;
-(NSArray*)loadServiceCategory;




-(NSArray*)getFilteredServiceData:(NSString*)sortBy serviceType:(NSString*)serviceType stateIdAlist:(NSArray*)stateIdAlist categoryList:(NSArray*)categoryList;



-(NSArray*)getFilteredNotifData:(NSString*)sortBy notifType:(NSString*)notifType serviceIdAlist:(NSArray*)serviceIdAlist stateIdAlist:(NSArray*)stateIdAlist startDate:(NSString*)startDate  endDate:(NSString*)endDate;





@end
