//
//  AllServicesTabVC.m
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "AllServicesTabVC.h"
#import "HCSStarRatingView.h"
#import "HeaderCollectionReusableView.h"

#import "AllServiceCVCell.h"
#import "CustomImageFlowLayout.h"

#import "ScrollNotificationVC.h"
#import "HomeDetailVC.h"
#import "AdvanceSearchVC.h"

#import "UIImageView+WebCache.h"
#import "itemMoreInfoVC.h"
#import "DetailServiceNewVC.h"

#import "AddallserviceFilterVC.h"
#import "AllServiceCVCellForiPad.h"
#import "HomeTabVC.h"
#import "UserProfileVC.h"
#import "UIView+MGBadgeView.h"
#import "ShowUserProfileVC.h"

#import "CustomBadge.h"
#import "BadgeStyle.h"
#import "CustomPickerVC.h"

#import "StateList.h"
#import "RunOnMainThread.h"

#import "ServiceNotAvailableView.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"

#import "AddFavFilterVC.h"

static const CGSize progressViewSize = { 95.0f, 25.0f };
static float NV_height= 50;


@interface AllServicesTabVC ()<UIActionSheetDelegate>
{
    itemMoreInfoVC *itemMoreVC;
    
    __weak IBOutlet UIView *vwSearchBG;
    BOOL flagrotation;
    
    BOOL flagStatusBar;
    IBOutlet UIButton *btn_notification;
    
    StateList *obj;
    NSString *state_id;
    CustomPickerVC *customVC;
    
    ServiceNotAvailableView *noServiceView;
    MBProgressHUD *hud;
    HeaderCollectionReusableView *headerView;
    NSIndexPath *lastSelectedIndex;
    UIActivityIndicatorView *spinner;
    NavigationSearchView *nvSearchView ;

}

@property (nonatomic, strong) NSString *profileComplete;



@property (nonatomic, strong) HCSStarRatingView *starRatingView;
@property(nonatomic,retain)NSArray *sampleData ;
@property(nonatomic,retain)NSMutableArray *tableData ;
@property(nonatomic,retain)NSDictionary *cellData;
@property(nonatomic,retain)NSMutableDictionary * cellDataOfmore;





@property (nonatomic) CGFloat progress;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSArray *progressViews;



@property(nonatomic,retain)UIImageView *imgProfile;
@property(nonatomic,retain)UILabel *lbl_profileComplete;
@property(nonatomic,retain)UILabel *lbl_whiteLine;
@property(nonatomic,retain)UILabel *lbl_percentage;
@property(nonatomic,retain)UIButton *btn_close;
@property(nonatomic,retain)UIButton *btn_clickUpdate;
@property(nonatomic,retain)UIView *NotifycontainerView;

@end

@implementation AllServicesTabVC

@synthesize allSer_collectionView;
@synthesize cellData;
@synthesize cellDataOfmore;


@synthesize imgProfile;
@synthesize lbl_profileComplete;
@synthesize lbl_whiteLine;
@synthesize lbl_percentage;
@synthesize btn_close;
@synthesize btn_clickUpdate;
@synthesize NotifycontainerView;
@synthesize profileComplete;

-(void)fetchData
{
    
    UITabBarController *tab=self.tabBarController;
    
    if (tab){
        NSLog(@"I have a tab bar");
       // [self.tabBarController setSelectedIndex:2];
        [self.tabBarController setSelectedIndex:singleton.tabSelectedIndex];

    }
    
    else{
        NSLog(@"I don't have");
    }
    
    
    NSInteger opentabIndex=self.tabBarController.selectedIndex;
    NSLog(@"opentabIndex=%ld",(long)opentabIndex);
    
    
    
    //------------ Handling loading service if user logout -----------------
    NSInteger currentIndexOfTab=[singleton getSelectedTabIndex];
    NSLog(@"currentIndexOfTab=%ld",(long)currentIndexOfTab);
    
    if (currentIndexOfTab==opentabIndex)
    {
        [self fetchDatafromDB];
        
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FETCHHOMEDATA" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchData)
                                                 name:@"FETCHHOMEDATA" object:nil];
    
    
}

-(void)callStateAPI
{
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    
}

-(void)profileCheck
{
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [self addNotify];
    }];
    
    
}
-(void)reloadEverything
{
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [self performSelector:@selector(reloadAfterDelay) withObject:nil afterDelay:1.0];
    }];
}


- (void)viewDidLoad
{
   
    if (iPhoneX())
    {
        
        nvSearchView = [[NavigationSearchView alloc] init];
        NSLog(@"An iPhone X Load UI");
    }
    [super viewDidLoad];
    
    singleton = [SharedManager sharedSingleton];
    
    self.allSer_collectionView.alwaysBounceVertical = YES;
    //  [ self.allSer_collectionView registerClass:[headerView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderViewAllService"];
    
    flagStatusBar=FALSE;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchData)
                                                 name:@"FETCHHOMEDATA" object:nil];
    
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(profileCheck)
    //                                                 name:@"ProfileData" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileCheck)
                                                 name:@"PROFILECOMPLETE" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadEverything)
                                                 name:@"ReloadEverything" object:nil];;
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(reloadYourtable)                                                 name:@"CHECKTABLEEMPTYORNOT" object:nil];
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: ALL_SERVICE_TAB_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self adjustSearchBarView];
    }
    
    txt_searchField.placeholder = NSLocalizedString(@"search", @"");
    txt_searchField.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    [self callStateAPI];
    
    if (!refreshController)
    {
        refreshController = [[UIRefreshControl alloc] init];
        //refreshController.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
        [refreshController addTarget:self action:@selector(getEvents:) forControlEvents:UIControlEventValueChanged];
        //self.refreshController = refresh;
        [allSer_collectionView addSubview:refreshController];
        // [self getEvents:refreshController];
    }
    
    if (noServiceView == nil)
    {
        noServiceView = [[ServiceNotAvailableView alloc] initWithFrame:CGRectMake(allSer_collectionView.frame.origin.x, allSer_collectionView.frame.size.height/2, allSer_collectionView.frame.size.width, allSer_collectionView.frame.size.height)];
        [self.view addSubview:noServiceView];
    }
    [noServiceView.btnSubHeading addTarget:self action:@selector(didTapChangeStateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    noServiceView.hidden = YES;
    noServiceView.backgroundColor = allSer_collectionView.backgroundColor;
    allSer_collectionView.delegate = self;
    allSer_collectionView.dataSource = self;
    
    // [allSer_collectionView registerClass:[AllServiceCVCell class] forCellWithReuseIdentifier:@"AllServiceCVCell"];
    
    allSer_collectionView.backgroundColor = [UIColor clearColor];
    
    allSer_collectionView.collectionViewLayout = [[CustomImageFlowLayout alloc] init];
    if (@available(iOS 9, *)) {
        [allSer_collectionView setRemembersLastFocusedIndexPath:true];
    }
    //===== Start Code to add spinner at center of the view=====
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.frame = CGRectMake(fDeviceWidth/2-30, fDeviceHeight/2-30, 60, 60);
    spinner.backgroundColor=[UIColor clearColor];
    [spinner setColor:[UIColor grayColor]];
    UIViewController *topvc=[self topMostController];
    [topvc.view addSubview:spinner];
    //===== Start spinner =====
    [spinner startAnimating];
    //
    //load on first time then stop while tab loaded
    
    //=====End Code to add spinner at center of the view=====

    [self addNavigationSearchTabView];
    
}

#pragma mark- add Navigation View to View

-(void)addNavigationSearchTabView{
    
    if (iPhoneX())
    {
        NSLog(@"An iPhone X Load UI");
        
       // NavigationSearchView *nvSearchView = [[NavigationSearchView alloc] init];
        __weak typeof(self) weakSelf = self;
        vw_line.hidden=TRUE;
        
        nvSearchView.didTapFilterBarButton= ^(id btnfilter)
        {
            
            [weakSelf btn_filterAction:btnfilter];
            
        };
        
        
        nvSearchView.didTapNotificationBarButton= ^(id btnNotification)
        {
            
            [weakSelf btn_noticationAction:btnNotification];
            
        };
        
        nvSearchView.didTapSearchTextfield= ^(id txtSearchView)
        {
            
            [weakSelf openSearch];
            
            
        };
        
        [self.view addSubview:nvSearchView];
        
        CGRect table = allSer_collectionView.frame;
        table.origin.y = kiPhoneXNaviHeight4Search;
        table.size.height = fDeviceHeight - kiPhoneXNaviHeight4Search;
        allSer_collectionView.frame = table;
        [self.view layoutIfNeeded];
        
        
    }
    else
    {
        NSLog(@"Not an iPhone X use default UI");
    }
    
}

- (void)getEvents:(UIRefreshControl *)refresh
{
    static BOOL refreshInProgress = NO;
    
    if (!refreshInProgress)
    {
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            
            refreshInProgress = YES;
            [self fetchDatafromDB];
            [refresh endRefreshing];
            
            refreshInProgress = NO;
            
        }];
        
    }
}


-(void)adjustSearchBarView
{
    vwSearchBG.frame = CGRectMake(self.view.frame.size.width/2 - 150, vwSearchBG.frame.origin.y, 300, vwSearchBG.frame.size.height);
    
    searchIconImage.frame = CGRectMake(vwSearchBG.frame.origin.x + 7, searchIconImage.frame.origin.y, searchIconImage.frame.size.width, searchIconImage.frame.size.height);
    
    txt_searchField.frame = CGRectMake(searchIconImage.frame.origin.x + searchIconImage.frame.size.width + 5, txt_searchField.frame.origin.y, 265, txt_searchField.frame.size.height);
    
}
#pragma mark - UICollection View Delegates

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    lastSelectedIndex = indexPath;
     [singleton traceEvents:@"Select Department" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.dic_serviceInfo=(NSDictionary*)[table_data objectAtIndex:indexPath.row];
    vc.tagComeFrom=@"OTHERS";
    
    
    //------------ Handling loading service if user logout -----------------
    
    
    //===================================================================================
    //         START  NEW CHANGES OF ADDING STATE ID AND STATE NAME
    //===================================================================================

    NSInteger currentSegmentIndex=headerView.segmentService.selectedSegmentIndex;
    NSLog(@"currentSegmentIndex=%ld",(long)headerView.segmentService.selectedSegmentIndex);
    if (currentSegmentIndex==2)
    {
        NSString *strState = [obj getStateName:state_id];
        vc.isStateSelected=@"TRUE";// use to pass value in Homedetail
        vc.state_idtopass=state_id;
        vc.state_nametopass=strState;

        NSLog(@"state ID= %@",state_id);
        NSLog(@"state NAME= %@",strState);

    }
    else
    {
        vc.isStateSelected=@"FALSE";//
        vc.state_idtopass=@"";
        vc.state_nametopass=@"";
    }
    
    vc.sourceTab     = @"all_services";
    vc.sourceState   = @"";
    vc.sourceBanner  = @"";
    
    switch (currentSegmentIndex)
    {
        case 0:
        {
            vc.sourceSection = @"all";
        }
            break;
        case 1:
        {
            vc.sourceSection = @"central";
        }
            break;
        case 2:
        {
            if (state_id == nil || state_id.length == 0 || [state_id isEqualToString:@"9999"])
            {
                vc.sourceSection = @"state_all";
            }
            else
            {
                vc.sourceState   = [NSString stringWithFormat:@"%@",state_id];
                vc.sourceSection = @"state";
            }
            
        }
            break;
        default:
            break;
    }
    
    [self presentViewController:vc animated:NO completion:nil];
    
    
}

#pragma mark -
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    // [allSer_collectionView.collectionViewLayout invalidateLayout];
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    
}
-(void)didTapChangeStateButtonAction:(UIButton*)sender
{
    [singleton traceEvents:@"Change State Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    customVC = [storyboard instantiateViewControllerWithIdentifier:@"CustomPickerVC"];
    
    //NSArray *arrState=[obj getStateList];
    //arrState = [arrState sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSArray  * servicesArray = [singleton.dbManager getServiceStateAvailable];
    NSArray *upcomingServicesArray = [singleton.dbManager getUpcommingStates];
    
    servicesArray = [servicesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    upcomingServicesArray = [upcomingServicesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    customVC.delegate=self;
    customVC.get_title_pass = NSLocalizedString(@"select_state", nil);
    customVC.get_arr_element=[servicesArray mutableCopy];
    customVC.arrUpcomingState = [upcomingServicesArray mutableCopy];
    customVC.get_TAG=TAG_ALLSERVICE_STATE;
    
    customVC.allServiceState = [headerView.btnStateSelected titleForState:UIControlStateNormal];
    __weak __typeof(self) weakSelf = self;
    customVC.finishState = ^(NSString *stateID) {
        
        state_id = stateID;
        
        state_id = state_id.length == 0 ? @"" : state_id;
        
        [[NSUserDefaults standardUserDefaults] setObject:state_id forKey:@"AllTabState"];
        [hud hideAnimated:YES];
        
       // [weakSelf hitSetStateAPI:stateID];
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [weakSelf loadStateService];
        }];
    };
    // [customVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:customVC animated:NO completion:nil];
    [self.navigationController pushViewController:customVC animated:YES];
}
- (IBAction)segmentDidChangeValue:(UISegmentedControl *)sender
{
   
    switch (sender.selectedSegmentIndex) {
        case 0:
            [singleton traceEvents:@"All Segment" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
            [self reloadAfterDelay];
            break;
        case 1:
            [singleton traceEvents:@"Central Segment" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
            [self loadCentralServiceList];
            break;
        case 2:
            [singleton traceEvents:@"State Segment" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
            //break;
            [self loadStateService];
            break;
        default:
            // [self reloadAfterDelay];
            break;
    }
}
-(void)loadCentralServiceList
{
    
    singleton = [SharedManager sharedSingleton];
    NSArray *arrServiceData=[singleton.dbManager getCentralServiceData];
    NSLog(@"arrServiceData=%@",arrServiceData);
    table_data=[[NSMutableArray alloc]init];
    table_data=[arrServiceData mutableCopy];
    NSSortDescriptor *aphabeticDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
    table_data = [[table_data sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    allSer_collectionView.scrollEnabled = true;
    
    [self addNotify];//add later
    [self reloadCollectionView];
    //[allSer_collectionView reloadData];
}
-(NSString*)getStateName
{
    obj=[[StateList alloc]init];
    state_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"AllTabState"];
//    if (state_id.length != 0)
//    {
//        [self hitSetStateAPI:state_id];
//    }
    return [obj getStateName:state_id];
}
-(void)loadStateService
{
    [hud hideAnimated:YES];
    allSer_collectionView.scrollEnabled = true;
    
    obj=[[StateList alloc]init];
    
    state_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"AllTabState"];
    
    if (state_id.length != 0)
    {
        [self hitSetStateAPI:state_id];
    }
    
    NSString *strState = [obj getStateName:state_id];
    
    singleton = [SharedManager sharedSingleton];
    
    NSArray *arrServiceData = nil ;
    if ([state_id isEqualToString:@"9999"] || [state_id isEqualToString:@""] || [[strState uppercaseString] isEqualToString:@"ALL"] || strState == nil || strState.length == 0)
    {
        strState = NSLocalizedString(@"all", nil);
        arrServiceData = [[singleton.dbManager getAllServiceDataNotCentral] mutableCopy];
    }else {
        arrServiceData = [[singleton.dbManager getServiceStateData:state_id] mutableCopy];
    }
    
    table_data=[[NSMutableArray alloc]init];
    NSSortDescriptor *aphabeticDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
    table_data = [[arrServiceData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    NSLog(@"table_data=%@",table_data);
    
    if ([table_data count]==0) {
        allSer_collectionView.scrollEnabled = false;
    }
    [self addNotify];//add later
    [self reloadCollectionView];
    //[allSer_collectionView reloadData];
    
}
-(void)addNotify
{
    
    NSLog(@"ADD NOTIFY");
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight );
    
    if (iPhoneX())
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        float yX= kiPhoneXNaviHeight4Search;
        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
        
        float heightX = heightVar-tabBarHeight ;
        allSer_collectionView.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
        
    }
    NSString* showProfilestatus =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"SHOW_PROFILEBAR"];//[NSString stringWithFormat:@"%@", profileComplete];
    if ([showProfilestatus isEqualToString:@"YES"])
    {
        
        // tableView_home.frame=CGRectMake(0,69,377,fDeviceHeight-69-NV_height);
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-NV_height);
        NSLog(@"tableView_home =%f height=%f",allSer_collectionView.frame.size.width,allSer_collectionView.frame.size.height);
        
        if (iPhoneX())
        {
            CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
            
            float yX= kiPhoneXNaviHeight4Search;
            float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
            
            float heightX = heightVar-tabBarHeight-NV_height ;
            allSer_collectionView.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
            
        }
        for (UIView *subview in [self.view subviews])
        {
            if (subview.tag == 7) {
                [subview removeFromSuperview];
            }
        }
        
        
        
        // UIEdgeInsets inset = UIEdgeInsetsMake(-20, 0,0, 0);
        // tableView_home.contentInset = inset;
        
        
        //----------- Add later can remove it----
        //----------- Add later can remove it----
        if ([[UIScreen mainScreen]bounds].size.height == 812)
        {
            NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-tabBarHeight-NV_height,fDeviceWidth, NV_height)];
            if (flagStatusBar==TRUE)
            {
                NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
            }
        }
        else
        {
            NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-1.95*NV_height,fDeviceWidth, NV_height)];
            if (flagStatusBar==TRUE)
            {
                NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
            }
        }
//        NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-1.95*NV_height,fDeviceWidth, NV_height)];
//
//        if (flagStatusBar==TRUE)
//        {
//            NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
//
//        }
        
        
        NotifycontainerView.backgroundColor = [UIColor clearColor];
        NotifycontainerView.tag=7;
        [self.view addSubview:NotifycontainerView];
        
        
        //-------- background image view--------------
        UIImageView *bg_img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,fDeviceWidth, NV_height)];
        bg_img.image=[UIImage imageNamed:@"img_popup_notify.png"];
        //bg_img.tag=7;
        [NotifycontainerView addSubview:bg_img];
        
        
        
        //  dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", NULL);
        //  dispatch_async(queue, ^{
        //code to be executed in the background
        
        
        //------- Profile image view-------
        UIImageView *user_img=[[UIImageView alloc]initWithFrame:CGRectMake(10,5,35,35)];
        
        
        
        UIImage *tempImg;
        if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_male", nil)]) {
            tempImg=[UIImage imageNamed:@"male_avatar"];
            
        }
        else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_female", nil)]) {
            tempImg=[UIImage imageNamed:@"female_avatar"];
        }
        else
        {
            tempImg=[UIImage imageNamed:@"user_placeholder"];
            
        }
        
        
        
        user_img.image=tempImg;
        
        //dispatch_async(dispatch_queue_create("com.getImage", NULL), ^(void) {
        //NSLog(@"singleton.imageLocalpath=%@",singleton.imageLocalpath);
        UIImage *tempImg1 ;
        
        if([[NSFileManager defaultManager] fileExistsAtPath:singleton.imageLocalpath])
        {
            // ur code here
            NSLog(@"file present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            tempImg1 = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
            if (tempImg1==nil) {
                tempImg1=tempImg;
            }
            user_img.image=tempImg1;
        } else {
            // ur code here**
            NSLog(@"Not present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            tempImg1=tempImg;
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        singleton.user_profile_URL=[defaults decryptedValueForKey:@"USER_PIC"];
        NSLog(@"user_profile_URL=%@",singleton.user_profile_URL);
        [defaults synchronize];
        NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
        if(![[url absoluteString] isEqualToString:@""])
        {
           /* dispatch_async(dispatch_get_main_queue(),^{
                [user_img sd_setImageWithURL:url
                            placeholderImage:tempImg];
            });*/
            
            NSURLRequest* request = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse * response,
                                                       NSData * data,
                                                       NSError * error) {
                                       if (!error){
                                           if(data == nil)
                                           {
                                               user_img.image = tempImg1;
                                               
                                           }
                                           else
                                           {
                                               UIImage* image = [[UIImage alloc] initWithData:data];
                                               //added later to resolve if server image path is invalid image
                                               if(image == nil)
                                               {
                                                   image = tempImg1;
                                                   
                                               }
                                               //== end of  image nil
                                               
                                               user_img.image = image;
                                           }
                                           // do whatever you want with image
                                       }
                                       else
                                       {
                                           user_img.image = tempImg1;
                                           
                                       }
                                       
                                   }];
            
            
        }
        else
        {
            user_img.image = tempImg1;
        }
        
        // });
        
        user_img.layer.cornerRadius = user_img.frame.size.height /2;
        user_img.layer.masksToBounds = YES;
        user_img.layer.borderWidth = 0.1;
        //user_img.tag=7;
        [NotifycontainerView addSubview:user_img];
        
        
        //-------Label Complete text----------
        
        CGSize stringcompletesize = [NSLocalizedString(@"profile_completion", nil) sizeWithFont:[UIFont systemFontOfSize:12]];
        //or whatever font you're using
        
        
        
        
        
        
        
        
        lbl_profileComplete=[[UILabel alloc]init];
        
        [lbl_profileComplete setFrame:CGRectMake(51,6,stringcompletesize.width, stringcompletesize.height)];
        
        lbl_profileComplete.text= NSLocalizedString(@"profile_completion", nil);
        
        lbl_profileComplete.font=[UIFont systemFontOfSize:12];
        lbl_profileComplete.textColor=[UIColor whiteColor];
        lbl_profileComplete.adjustsFontSizeToFitWidth = YES;
        //lbl_profileComplete.tag=7;
        [NotifycontainerView addSubview:lbl_profileComplete];
        
        
        
        
        
        
        
        //-------Button Click Update ----------
        
        
        btn_clickUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
        
        // btn_clickUpdate.frame = CGRectMake(52,20,290,22);
        
        
        @try {
            btn_clickUpdate.titleLabel.adjustsFontSizeToFitWidth = YES;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        CGSize stringsize = [NSLocalizedString(@"update_profile_txt", nil) sizeWithFont:[UIFont systemFontOfSize:12]];
        //or whatever font you're using
        
        
        
        
        [btn_clickUpdate setFrame:CGRectMake(52,25,stringsize.width, stringsize.height)];
        
        
        [btn_clickUpdate addTarget:self
                            action:@selector(clickUpdateAction:)
                  forControlEvents:UIControlEventTouchUpInside];
        [btn_clickUpdate setTitle:NSLocalizedString(@"update_profile_txt", nil) forState:UIControlStateNormal];
        
        [btn_clickUpdate setTitle:NSLocalizedString(@"update_profile_txt", nil) forState:UIControlStateSelected];
        btn_clickUpdate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        btn_clickUpdate.titleLabel.font=[UIFont systemFontOfSize:12];
        // btn_clickUpdate.tag=7;
        
        [NotifycontainerView addSubview:btn_clickUpdate];
        
        
        //---- Show Custom Progress Profile View--------
        
        
        
        UIProgressView *progressView;
        progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        progressView.progressTintColor =[UIColor colorWithRed:12.0/255.0 green:118.0/255.0 blue:157.0/255.0 alpha:1.0];
        [[progressView layer]setFrame:CGRectMake(lbl_profileComplete.frame.origin.x+lbl_profileComplete.frame.size.width+5,9,fDeviceWidth-188-50,10)];
        progressView.trackTintColor = [UIColor whiteColor];
        [[progressView layer]setCornerRadius:2];
        progressView.layer.masksToBounds = TRUE;
        progressView.clipsToBounds = TRUE;
        
        
        
        NSString* str =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"PROFILE_COMPELTE_KEY"];//[NSString stringWithFormat:@"%@", profileComplete];
        //str=@"90";
        if (str == nil)
        {
            str=@"0";
        }
        if ([str length]==0)
        {
            str=@"0";
            
        }
        
        CGFloat value = [str floatValue]/100;
        lbl_percentage.text=  [NSString stringWithFormat:@"%@%%",str];
        // self.progress=value;
        [progressView setProgress:value];  ///15
        
        
        
        [NotifycontainerView addSubview:progressView];
        
        
        
        
        
        //-------Label Percentage text----------
        lbl_percentage=[[UILabel alloc]initWithFrame:CGRectMake(progressView.frame.origin.x+progressView.frame.size.width+5,2,30,22)];
        
        
        
        if ([str intValue]>=70)
            
        {
            
            [self closeBtnAction:self];
        }
        
        
        
        lbl_percentage.text=[NSString stringWithFormat:@"%@%%",str];
        lbl_percentage.font=[UIFont boldSystemFontOfSize:11];
        lbl_percentage.textColor=[UIColor whiteColor];
        @try {
            lbl_percentage.adjustsFontSizeToFitWidth = YES;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        //lbl_percentage.tag=7;
        [NotifycontainerView addSubview:lbl_percentage];
        
        
        //-------Button Close ----------
        
        btn_close = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn_close.frame = CGRectMake(fDeviceWidth-30,NV_height/2-8, 22, 22);
        
        [btn_close addTarget:self
                      action:@selector(closeBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        UIImage *buttonImagePressed = [UIImage imageNamed:@"btn_close_notify.png"];
        [btn_close setImage:buttonImagePressed forState:UIControlStateNormal];
        [btn_close setImage:buttonImagePressed forState:UIControlStateSelected];
        
        btn_close.tag=7;
        
        [NotifycontainerView addSubview:btn_close];
        
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [self reloadCollectionView];
          //  [allSer_collectionView reloadData];
        }];
        
    }
    else
    {
        [self closeBtnAction:self];
    }
    
    [self checkAppUpdate];
    allSer_collectionView.hidden = false ;
    noServiceView.hidden = true ;
    [spinner stopAnimating];

    if ([table_data count]==0)
    {
        allSer_collectionView.hidden = false ;
        noServiceView.hidden         = false;
        NSLog(@"arr init --%@", singleton.arr_initResponse);
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [noServiceView setBackgroundImageWithUrl:@"https:google.com"];
            [self setNoServiceFrame];
        }];
        [self.view bringSubviewToFront:noServiceView];
    }
    
    
    //    CGFloat navHeight = [showProfilestatus isEqualToString:@"YES"] ? 50 : 0.0;
    //    noServiceView.frame = CGRectMake(allSer_collectionView.frame.origin.x, allSer_collectionView.frame.origin.y + 110, allSer_collectionView.frame.size.width, allSer_collectionView.frame.size.height - CGRectGetHeight(headerView.frame) - navHeight);
    
    //    if (fDeviceHeight < 560) {
    //
    //        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
    //            CGRect allFrame = allSer_collectionView.frame;
    //            allFrame.origin.y = 74;
    //            allSer_collectionView.frame = allFrame;
    //
    //        }];
    //    }
    [self.view layoutIfNeeded];
    
}
-(void)checkAppUpdate {
    NSString *newVersion = [NSString stringWithFormat:@"%@",[[SharedManager sharedSingleton].arr_initResponse valueForKey:@"nver"]];
    NSString *showVersion = [[NSUserDefaults standardUserDefaults] valueForKey:APPUPDATE_SHOWN_VERSION];
    // BOOL isRemoved = [[NSUserDefaults standardUserDefaults] boolForKey:APPUPDATE_IS_REMOVED];
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    for (UIView *subview in [self.view subviews])
    {
        if (subview.tag == 7878) {
            [subview removeFromSuperview];
        }
    }
    BOOL isShown = false;
    if ([newVersion integerValue] > [showVersion integerValue] ) {
        allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-NV_height);
        
        if (iPhoneX())
        {
            CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
            
            float yX= kiPhoneXNaviHeight4Search;
            float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
            
            float heightX = heightVar-tabBarHeight-NV_height ;
            allSer_collectionView.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
            
        }
        AppUpdateView *updateView = nil ;
        if ([[UIScreen mainScreen]bounds].size.height == 812)
        {
            updateView  = [[AppUpdateView alloc] initWithFrame:CGRectMake(0, fDeviceHeight-tabBarHeight-NV_height,fDeviceWidth, NV_height)];
        }
        else
        {
            updateView  = [[AppUpdateView alloc] initWithFrame:CGRectMake(0, fDeviceHeight-1.95*NV_height,fDeviceWidth, NV_height)];
            
        }
        if (flagStatusBar==TRUE)
        {
            updateView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
        }
        updateView.tag = 7878;
        __block typeof(self) weakSelf = self;
        updateView.didRemoveApp = ^{
             [weakSelf addNotify];
        };
        [self.view addSubview:updateView];
        [self.view bringSubviewToFront:updateView];
        isShown = true;
    }
}

- (void)closeBtnAction:(id)sender
{
    
    [singleton traceEvents:@"Close ProfileBar Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight );
    
    
    if (iPhoneX())
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        float yX= kiPhoneXNaviHeight4Search;
        float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
        
        float heightX = heightVar-tabBarHeight ;
        allSer_collectionView.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
        
    }
    
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"SHOW_PROFILEBAR"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [NotifycontainerView removeFromSuperview];
    }];
    
    
}
-(void)setNoServiceFrame
{
    NSString* showProfilestatus =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"SHOW_PROFILEBAR"];
    CGFloat navHeight = 50;//[showProfilestatus isEqualToString:@"YES"] ? 50 : 50;
    CGFloat bannerHeight = headerView.segmentService.selectedSegmentIndex == 2 ? 40 : 20;
    if (fDeviceHeight < 560)
    {
        noServiceView.frame = CGRectMake(allSer_collectionView.frame.origin.x, allSer_collectionView.frame.origin.y + 90, allSer_collectionView.frame.size.width, allSer_collectionView.frame.size.height  - navHeight - bannerHeight);
    }
    else
    {
        if (iPhoneX())
        {
            CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
            
            float yX= kiPhoneXNaviHeight4Search;
            float heightVar = fDeviceHeight-kiPhoneXNaviHeight4Search;
            
            float heightX = heightVar-tabBarHeight;
            allSer_collectionView.frame=CGRectMake(0,yX,fDeviceWidth,heightX);
            
        }
        noServiceView.frame = CGRectMake(allSer_collectionView.frame.origin.x, allSer_collectionView.frame.origin.y + 110, allSer_collectionView.frame.size.width, allSer_collectionView.frame.size.height  - navHeight - bannerHeight - 50);
    }
    [self.view layoutIfNeeded];
}
#pragma mark -

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitSetStateAPI:(NSString*)stateId
{
    if ([stateId isEqualToString:@"9999"] || [stateId isEqualToString:@""])
    {
        return;
    }
    
    [hud hideAnimated:YES];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"status"];//This is Status of the account
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    // [dictBody setObject:stateId forKey:@"st"];  //get from mobile default email //not supported iphone
    
    [dictBody setObject:stateId forKey:@"stid"];  //get from mobile default email //not supported iphone
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_GETAMBLEM withBody:dictBody andTag:TAG_REQUEST_STATE completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         if (error == nil)
         {
             NSLog(@"Server Response = %@",response);
             //----- below value need to be forword to next view according to requirement after checking Android apk-----
             NSString *rc=[response valueForKey:@"rc"];
             NSString *rs=[response valueForKey:@"rs"];
             NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
             NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
             //----- End value need to be forword to next view according to requirement after checking Android apk-----
             NSString *rd=[response valueForKey:@"rd"];
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                 
             {
                 NSString*  stemblem=[[response valueForKey:@"pd"] valueForKey:@"stemblem"];
                 if (noServiceView != nil) {
                     [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                         [noServiceView setBackgroundImageWithUrl:stemblem];
                     }];
                     // [noServiceView setBackgroundImageWithUrl:stemblem];
                 }
                 /* {"rs":"S","rc":"00","rd":"Successful","pd":{"ostate":"20","abbr":"Haryana","stname":"Haryana","stemblem":"https://static.umang.gov.in/app/ico/emblem/haryana.png"},"gcmid":"","ntfp":"","ntft":"","plang":"","node":""}
                  
                  */
             }
             
         }
         else{
             NSLog(@"Error Occured = %@",error.localizedDescription);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                             message:error.localizedDescription
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                   otherButtonTitles:nil];
             [alert show];
             
         }
         
     }];
    
}



#pragma mark -
- (void)updateProgress
{
    // NSString* str = [NSString stringWithFormat:@"%@", profileComplete];
    NSString* str =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"PROFILE_COMPELTE_KEY"];//[NSString stringWithFormat:@"%@", profileComplete];
    
    if (str == nil)
    {
        str=@"0";
    }
    if ([str length]==0)
    {
        str=@"0";
        
    }
    
    
    CGFloat value = [str floatValue]/100;
    lbl_percentage.text=  [NSString stringWithFormat:@"%@%%",str];
    self.progress=value;
    
    
    
    [self.progressViews enumerateObjectsUsingBlock:^(THProgressView *progressView, NSUInteger idx, BOOL *stop) {
        [progressView setProgress:self.progress animated:YES];
        
    }];
    
}



-(void)clickUpdateAction:(id)sender
{
    
    // Reset here for login with registration here
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLoginWithRegistration"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    SharedManager *singleton  = [SharedManager sharedSingleton];
    singleton.shared_mpinflag = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinflag"];
    singleton.shared_mpinmand = [[NSUserDefaults standardUserDefaults] valueForKey:@"mpinmand"];
    if ([[singleton.shared_mpinflag lowercaseString] isEqualToString:@"false"])
    {
        
        NSLog(@"MPIN SHOWING STATUS IS FALSE, so  show it");
        
        dispatch_async(dispatch_get_main_queue(),^{
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate showMPIN_AlertBox];
            [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
             delegate.vw_MPIN_AlertBox.lbl_subtitle1.text = NSLocalizedString(@"setting_mpin_mandatory_this_screen", nil);
        });
        /*
         NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
         [[NSUserDefaults standardUserDefaults]synchronize];
         if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]||[lastTime isEqualToString:@"NR"])
         {
         // dont show first time
         }
         else
         {
         // ========= check to open show mpin alert box=====
         dispatch_async(dispatch_get_main_queue(),^{
         AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
         [delegate showHideSetMpinAlertBox ];
         [delegate.vw_MPIN_AlertBox showHideClose:@"false"];
         
         });
         }
         */
    }
    else
    {
        // do nothing
        NSLog(@"MPIN SHOWING STATUS IS TRUE, so dont show it");
        
    [singleton traceEvents:@"Show User Profile Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    }
    
    
}



//----- End----------

//------- End of delegate methods of update profile------------



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if (headerView.segmentService.selectedSegmentIndex == 0) {
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [self fetchDatafromDB];
        }];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    //[allSer_collectionView sendSubviewToBack:refreshController];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    //  [allSer_collectionView setScrollsToTop:YES];
    
    //------------- Network View Handle------------
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    //------------- Tab bar Handle------------
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    //appDelegate.shouldRotate = YES;
    // [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];
    //------------- Tab bar Handle------------
    
    
    
    singleton = [SharedManager sharedSingleton];
    // Do any additional setup after loading the view, typically from a nib.
    vwSearchBG.layer.cornerRadius = 5.0;
    [txt_searchField setValue:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    // allSer_collectionView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
    
    
    self.navigationController.navigationBar.hidden = YES;
    
    // UIEdgeInsets inset = UIEdgeInsetsMake(-20, 0, -50, 0);
    // allSer_collectionView.contentInset = inset;
    
    vw_line.frame=CGRectMake(0, 74, fDeviceWidth, 0.5);
    
    [self setNeedsStatusBarAppearanceUpdate];
    // [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:NSLocalizedString(@"home_small", @"")];
    
    // [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:NSLocalizedString(@"favourites_small", @"")];
    
    //[[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:NSLocalizedString(@"all_services_small", @"")];
    // [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:NSLocalizedString(@"help_live_chat", @"")];
    
    
    // [[self.tabBarController.tabBar.items objectAtIndex:4] setTitle:NSLocalizedString(@"more", @"")];
    
    
    
    
    flagrotation=TRUE;
    
    
    
    //[allSer_collectionView setContentOffset:CGPointZero animated:YES];
    
    [allSer_collectionView setContentOffset:CGPointMake(0,0) animated:YES];
    
    appDel.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"appDelegate.badgeCount=%d",appDelegate.badgeCount);
    
    //-------- Added later-------------
    [self badgeHandling];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(badgeHandling) name:@"NotificationRecievedComplete" object:nil];
    
    
    self.tabBarController.tabBar.itemPositioning = UITabBarItemPositioningCentered;
    [hud hideAnimated:YES];
    
    if (refreshController)
    {
        [refreshController endRefreshing];
        [self.allSer_collectionView sendSubviewToBack:refreshController];
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        headerView.segmentService.frame = CGRectMake(self.view.frame.size.width/2 - 300, headerView.segmentService.frame.origin.y, 600, headerView.segmentService.frame.size.height);
        
        for (int i = 0; i < 3; i++)
        {
            [headerView.segmentService setWidth:200.0 forSegmentAtIndex:i];
        }
        
    }
    else
    {
        headerView.segmentService.frame = CGRectMake(40, headerView.segmentService.frame.origin.y, self.view.frame.size.width-80, headerView.segmentService.frame.size.height);
        
        for (int i = 0; i < 3; i++)
        {
            [headerView.segmentService setWidth:(headerView.segmentService.frame.size.width)/3 forSegmentAtIndex:i];
        }
    }
    
    [self setViewFont];
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    [_btnStateSelected.titleLabel setFont:[AppFont regularFont:18.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    txt_searchField.font = [AppFont mediumFont:13.0];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
#pragma mark -


-(void)badgeHandling
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //open for testing purpose
    // NSString *badge=[NSString stringWithFormat:@"%d",1];
    //[[NSUserDefaults standardUserDefaults] setObject:badge forKey:@"BadgeValue"];
    
    app.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"appDelegate.badgeCount=%d",app.badgeCount);
    NSString*badgeValue=[NSString stringWithFormat:@"%d",app.badgeCount];
    
    if (app.badgeCount>0)
    {
        
        if (app.badgeCount>=10)      // 3 and 2 digit digit case
        {
            CustomBadge *badge = [CustomBadge customBadgeWithString:badgeValue withScale:.9];
            CGSize size = CGSizeMake(badge.frame.size.width, badge.frame.size.height);
            
            //CGPoint point = CGPointMake(btn_notification.frame.size.width+5-badge.frame.size.width, -14);
            CGPoint  point = [UIScreen mainScreen].bounds.size.height == 812.0 ? CGPointMake(nvSearchView.notificationBarButton.frame.size.width+5-badge.frame.size.width, -14) :  CGPointMake(btn_notification.frame.size.width+5-badge.frame.size.width, -14);
            
            CGRect rect = CGRectMake(point.x, point.y, size.width, size.height+8);
            [badge setFrame:rect];
            badge.userInteractionEnabled=NO;
            badge.tag=786;
            
            //  [btn_notification addSubview:badge];
            
            
            [([UIScreen mainScreen].bounds.size.height == 812.0 ? nvSearchView.notificationBarButton : btn_notification) addSubview:badge];
            
        }
        else
        {
            CustomBadge * badge = [CustomBadge customBadgeWithString:badgeValue];
            //CGPoint point = CGPointMake(btn_notification.frame.size.width/2+8-badge.frame.size.width/2, -8);
            
            CGPoint  point = [UIScreen mainScreen].bounds.size.height == 812.0 ? CGPointMake(nvSearchView.notificationBarButton.frame.size.width/2+8-badge.frame.size.width/2, -8) : CGPointMake(btn_notification.frame.size.width/2+8-badge.frame.size.width/2, -8);
            
            CGRect rect = CGRectMake(point.x, point.y, 21, 21);
            [badge setFrame:rect];
            badge.userInteractionEnabled=NO;
            badge.tag=786;
            // [btn_notification addSubview:badge];
            [([UIScreen mainScreen].bounds.size.height == 812.0 ? nvSearchView.notificationBarButton : btn_notification) addSubview:badge];
            
            
        }
        
        
        
    }
    else
    {
        // for (UIView *subview in [btn_notification subviews])
        for (UIView *subview in [([UIScreen mainScreen].bounds.size.height == 812.0 ? nvSearchView.notificationBarButton : btn_notification) subviews])
            
        {
            if (subview.tag ==786) {
                [subview removeFromSuperview];
            }
            
        }
        
        
    }
    
    
}


-(void)fetchDatafromDB
{
   
    //===== Start spinner =====
   // [spinner startAnimating];
    
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        
        [self performSelector:@selector(reloadAfterDelay) withObject:nil afterDelay:1.0];
        
    }];
    
}

-(void)reloadAfterDelay
{
    if (headerView.segmentService.selectedSegmentIndex == 0) {
        allSer_collectionView.scrollEnabled = true;
        singleton = [SharedManager sharedSingleton];
        NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
        NSLog(@"arrServiceData=%@",arrServiceData);
        table_data=[[NSMutableArray alloc]init];
        table_data=[arrServiceData mutableCopy];
        
        NSSortDescriptor *aphabeticDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
        table_data = [[table_data sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        
        [self addNotify];//add later
        [self reloadCollectionView];
       // [allSer_collectionView reloadData];
        
        //===== stop spinner =====

        [spinner stopAnimating];

    }
    
}
-(void)reloadCollectionView{
    
    
    [allSer_collectionView reloadData];
    if (lastSelectedIndex != nil ) {
       // [allSer_collectionView scrollToItemAtIndexPath:lastSelectedIndex atScrollPosition:UICollectionViewScrollPositionNone animated:true];
    }
}
/*
 -(void)setupData
 {
 self.selectedMarks = [[NSMutableArray alloc] init];
 
 SharedManager *singleton=[SharedManager sharedSingleton];
 
 NSArray *fav_Arr=[singleton.fav_Mutable_Arr mutableCopy];
 
 table_data=[[NSMutableArray alloc]init];
 
 for (int i = 1; i < [fav_Arr count]; i++)
 {
 
 
 NSDictionary *cellData1 = [fav_Arr objectAtIndex:i];
 NSArray *AllServicesData = [cellData1 objectForKey:@"AllServices"];
 
 
 NSLog(@"cellData=%@ and AllServicesData=%@",cellData1,AllServicesData);
 
 for (int i=0; i<[AllServicesData count]; i++) {
 
 
 
 
 NSLog(@"Not Selected!");
 [table_data addObject:[AllServicesData objectAtIndex:i]];
 
 
 
 }
 }
 
 NSLog(@"tableData=%@",table_data);
 [allSer_collectionView reloadData];
 
 }*/

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
                        layout:(UICollectionViewLayout *) collectionViewLayout
        insetForSectionAtIndex:(NSInteger) section {
    if (headerView.segmentService.selectedSegmentIndex == 2) {
        return UIEdgeInsetsMake(5,13,0, 16);
    }
    return UIEdgeInsetsMake(10,13,0, 16); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
                   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    return 5.0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return table_data.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
#pragma mark - header View
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader)
    {
        headerView  = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderCollectionReusableView" forIndexPath:indexPath];
        CGRect segmentFrame = headerView.segmentService.frame;
        segmentFrame.origin.x = self.view.center.x - (segmentFrame.size.width / 2);
        headerView.segmentService.frame = segmentFrame;
        // self.segmentServiceType.tintColor = [UIColor colorWithRed:12.0/255.0 green:118.0/255.0 blue:157.0/255.0 alpha:1.0];
        [headerView.segmentService setTitle:NSLocalizedString(@"all", @"") forSegmentAtIndex:0];
        [headerView.segmentService setTitle:NSLocalizedString(@"central", @"") forSegmentAtIndex:1];
        [headerView.segmentService setTitle:[NSLocalizedString(@"state_caps", @"") capitalizedString] forSegmentAtIndex:2];
        [headerView.segmentService addTarget:self action:@selector(segmentDidChangeValue:) forControlEvents:UIControlEventValueChanged];
        [headerView.btnStateSelected setImage:[UIImage imageNamed:@"dropdown_arrow_Lang"] forState:UIControlStateNormal];
        [headerView.btnStateSelected addTarget:self action:@selector(didTapChangeStateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        headerView.btnStateSelected.layer.cornerRadius = 15;
        headerView.btnStateSelected.clipsToBounds = YES;
        headerView.btnStateSelected.hidden = false;
        headerView.btnStateSelected.titleLabel.font =[AppFont regularFont:14];
        
        NSString *strState = [self getStateName] ;
        if ( [[strState uppercaseString] isEqualToString:@"ALL"] || strState == nil || strState.length == 0)
        {
            strState = NSLocalizedString(@"all", nil);
        }
        [headerView.btnStateSelected setTitle:strState forState:UIControlStateNormal];
        [noServiceView setBtnSubHeadingTitle:headerView.btnStateSelected.currentTitle];
        [headerView.btnStateSelected sizeToFit];
        CGRect btnFrame = headerView.btnStateSelected.frame;
        btnFrame.size.width = btnFrame.size.width + 25;
        btnFrame.size.height = 30;
        btnFrame.origin.x = self.view.center.x - (btnFrame.size.width / 2);
        headerView.btnStateSelected.frame = btnFrame;
        [headerView.btnStateSelected setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(headerView.btnStateSelected.frame) - 25, 0, 0)];
        [headerView.btnStateSelected setTitleEdgeInsets:UIEdgeInsetsMake(0,-40, 0, 0)];
        
        reusableview = headerView;
    }
    return reusableview;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (headerView.segmentService.selectedSegmentIndex == 2) {
        return CGSizeMake(fDeviceWidth, 78);
    }
    return CGSizeMake(fDeviceWidth, 40);
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat height = 135 ;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        height = 220;
        
    }else {
        if (fDeviceWidth == 320)
        {
            height = 135;
        }
        else if (fDeviceWidth == 375)
        {
            height = 145;
        }
        else if (fDeviceWidth > 375)
        {
            height = 155;
        }
        
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return CGSizeMake((fDeviceWidth - 75 ) / 4.0, height);
    }
    return  CGSizeMake((fDeviceWidth - 60 ) / 3.0, height);
    /*
     if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation))
     {
     
     NSLog(@"UIInterfaceOrientationIsLandscape xxxxxxx value=%f",[[ UIScreen mainScreen] bounds].size.height);
     // 375.000000
     
     if ([[ UIScreen mainScreen] bounds].size.height >= 768)
     {
     return CGSizeMake(150, 220);
     }
     else if ([[UIScreen mainScreen] bounds].size.height <= 320)
     {
     return CGSizeMake(90, 135);
     }
     else if ([[UIScreen mainScreen] bounds].size.height <= 375)
     {
     return CGSizeMake(105, 150);
     }
     else
     return CGSizeMake(105, 150);
     }
     else
     {
     //667
     NSLog(@"UIInterfaceOrientationIsLandscape NOT xxxxxxx value=%f",[[ UIScreen mainScreen] bounds].size.height);
     if ([[ UIScreen mainScreen] bounds].size.height >= 1023)
     {
     return CGSizeMake(150, 220);
     }else if ([[UIScreen mainScreen] bounds].size.height >= 667)
     {
     return CGSizeMake(150, 220);
     }
     else if ([[UIScreen mainScreen] bounds].size.height >= 568)
     {
     return CGSizeMake(90, 135);
     }
     
     else
     return CGSizeMake(105, 150);
     
     
     
     
     }
     
     return CGSizeMake(105, 150);*/
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[ UIScreen mainScreen] bounds].size.height >= 1024)
    {
        static NSString *identifier = @"AllServiceCVCellForiPad";
        AllServiceCVCellForiPad *allCellipad = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        @try
        {
            
            allCellipad.serviceTitle.text =[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row]];
            allCellipad.serviceTitle.font = [UIFont systemFontOfSize:18];
            
            
            NSURL *url=[NSURL URLWithString:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
            
            [allCellipad.serviceImage sd_setImageWithURL:url
                                        placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
            int tagvalue= (int)(indexPath.row);
            allCellipad.serviceFav.tag=tagvalue;
            
            
            //            UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
            //            UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
            //
            //            [allCellipad.serviceFav setImage:btnImage1 forState:UIControlStateNormal];
            //            [allCellipad.serviceFav setImage:btnImage2 forState:UIControlStateSelected];
            
            
            
            NSString *serviceid=[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
            
            NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceid]];//get service status from db
            
            if ([serviceFav isEqualToString:@"true"])
            {
                allCellipad.serviceFav .selected=YES;
            }else{
                allCellipad.serviceFav .selected=NO;
            }
            
            
            cellData = (NSDictionary*)[table_data objectAtIndex:[indexPath row]];
            
            allCellipad.serviceInfo.celldata=[cellData mutableCopy];
            
            
            [allCellipad.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            allCellipad.serviceFav.usdata=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_NAME"]];
            [allCellipad.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *rating=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_RATING"]];
            
            float fCost = [rating floatValue];
            
            allCellipad.starRatingView.value = fCost;
            //[allCellipad.starRatingView setScore:fCost*2 withAnimation:NO];
            
            allCellipad.serviceTitle.font = [AppFont regularFont:14];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        //[allCellipad layoutIfNeeded];
        return allCellipad;
        
        
    }
    else
    {
        static NSString *identifier = @"AllServiceCVCell";
        AllServiceCVCell *allCell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        @try {
            allCell.serviceTitle.text =[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row]];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                allCell.serviceTitle.font = [UIFont systemFontOfSize:18];
            }
            NSURL *url=[NSURL URLWithString:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
            
            [allCell.serviceImage sd_setImageWithURL:url
                                    placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
            int tagvalue= (int)(indexPath.row);
            allCell.serviceFav.tag=tagvalue;
            
            
            //            UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
            //            UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
            //
            //            [allCell.serviceFav setImage:btnImage1 forState:UIControlStateNormal];
            //            [allCell.serviceFav setImage:btnImage2 forState:UIControlStateSelected];
            
            
            
            NSString *serviceid=[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
            
            NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceid]];//get service status from db
            
            if ([serviceFav isEqualToString:@"true"])
            {
                allCell.serviceFav .selected=YES;
            }else{
                allCell.serviceFav .selected=NO;
            }
            
            
            cellData = (NSDictionary*)[table_data objectAtIndex:[indexPath row]];
            
            allCell.serviceInfo.celldata=[cellData mutableCopy];
            
            
            [allCell.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            allCell.serviceFav.usdata=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_NAME"]];
            [allCell.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *rating=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_RATING"]];
            
            float fCost = [rating floatValue];
            
            allCell.starRatingView.value = fCost;
            //[allCell.starRatingView setScore:fCost*2 withAnimation:NO];
            
            allCell.serviceTitle.font = [AppFont regularFont:14];
        }
        @catch (NSException *exception)
        {
            
        }
        @finally
        {
            
        }
        
        //[allCell layoutIfNeeded];
        
        return allCell;
        
        
    }
    
    
    
    
}



//==================================
//       CUSTOM PICKER STARTS
//==================================
- (void)btnOpenSheet

{
    NSString *information=NSLocalizedString(@"information", nil);
    NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self callDetailServiceVC];
        }
            break;
        case 1:
        {
            NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
            [singleton traceEvents:@"Visit Map Option" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];

            NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
            
            NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
            
            NSString *deptName=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_NAME"]];
            deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]])
                
            {
                NSLog(@"Map App Found");
                
                NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
                
                // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
                
                NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSLog(@"mapURL= %@",mapURL);
                
                [[UIApplication sharedApplication] openURL:mapURL];
                
            } else
            {
                NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
                
            }
            
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

//==================================
//          CUSTOM PICKER ENDS
//==================================

//------------ Show more info-------------
-(IBAction)moreInfo:(MyButton*)sender
{
    NSLog(@"Data 1 = %@",sender.celldata);
    [singleton traceEvents:@"More Department Option Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    self.cellDataOfmore=[[NSMutableDictionary alloc]init];
    cellDataOfmore=(NSMutableDictionary*)sender.celldata;
    [self btnOpenSheet];
    
    /*
     NSString *titlename=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_NAME"]];
     
     //  NSString *titleDesc=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_DESC"]];
     
     //  NSString *imageName=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_IMAGE"]];
     
     //  NSString *Type=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_CATEGORY"]];
     
     
     // [self displayContentController:[self getHomeDetailLayerLeftController]];
     
     
     UIAlertController * alert=   [UIAlertController
     alertControllerWithTitle:titlename
     message:nil
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* info = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"information", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     // [self displayContentController:[self getHomeDetailLayerLeftController]];
     
     [self callDetailServiceVC];
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* map = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"view_on_map", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     
     NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
     
     NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
     
     NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
     
     
     //  NSString* googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@",latitude, longitute];
     
     
     
     // [[UIApplication sharedApplication] openURL: [NSURL URLWithString: googleMapsURLString]];
     
     if ([[UIApplication sharedApplication] canOpenURL:
     [NSURL URLWithString:@"comgooglemaps://"]])
     
     {
     NSLog(@"Map App Found");
     
     NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f&center=%.6f,%.6f&zoom=15&views=traffic", [latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     
     
     
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
     
     
     } else
     {
     NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
     
     }
     
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* cancel = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"cancel", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     
     
     [alert addAction:info];
     [alert addAction:map];
     
     [alert addAction:cancel];
     
     
     UIViewController *vc=[self topMostController];
     [vc presentViewController:alert animated:NO completion:nil];
     */
}

-(void)callDetailServiceVC
{
    [singleton traceEvents:@"Department Information Option" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    vc.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    //UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [self presentViewController:vc animated:NO completion:nil];
    
}



-(itemMoreInfoVC*)getHomeDetailLayerLeftController
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    itemMoreVC = [storyboard instantiateViewControllerWithIdentifier:@"itemMoreInfoVC"];
    
    itemMoreVC.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    
    return itemMoreVC;
}


- (void) displayContentController: (UIViewController*) content;
{
    UIViewController *vc=[self topMostController];
    
    [vc addChildViewController:content];
    
    
    content.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    
    [vc.view addSubview:content.view];
    [content didMoveToParentViewController:vc];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil)
    //{
    //itemMoreVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        itemMoreVC.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil) {
    itemMoreVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        itemMoreVC.view.frame = CGRectMake(vc.view.frame.size.width, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}


//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}



-(IBAction)fav_action:(MyFavButton*)sender
{
     [singleton traceEvents:@"Favourite Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];
    MyFavButton *button = (MyFavButton *)sender; //instance of UIButton
    int indexOfTheRow=(int)button.tag;   //tag of the button
    // NSLog(@"tabledata=%@",[table_data objectAtIndex:indexOfTheRow]);
    
    // Add image to button for pressed state
    //    UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
    //    UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
    //
    //    [button setImage:btnImage1 forState:UIControlStateNormal];
    //    [button setImage:btnImage2 forState:UIControlStateSelected];
    
    NSString *serviceId=[NSString stringWithFormat:@"%@",[[table_data objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_ID"]];
    
    NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceId]];
    
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
        button.selected=FALSE;
    }
    else
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
        button.selected=true;
    }
    
}





//-----------for filterview show----------
-(IBAction)btn_filterAction:(id)sender
{
    //code to open filter view here
    [singleton traceEvents:@"Filter Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];

    NSLog(@"Filter Pressed");
    
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    AddallserviceFilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddallserviceFilterVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    // [self presentViewController:vc animated:NO completion:nil];
    vc.hidesBottomBarWhenPushed = YES;
    vc.serviceType = headerView.segmentService.selectedSegmentIndex;
    vc.stateSelected = headerView.btnStateSelected.currentTitle;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}





//-----------for notification view show---
-(IBAction)btn_noticationAction:(id)sender
{
    //code to open notification view here
    [singleton traceEvents:@"Notification Button" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];

    NSLog(@"Notification Pressed");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
    
    ScrollNotificationVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ScrollNotificationVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:vc animated:NO completion:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
    
}
/*
 - (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
*/
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == txt_searchField)
    {
        [self openSearch];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)openSearch
{
    
    [singleton traceEvents:@"Advanced Search Bar" withAction:@"Clicked" withLabel:@"All Service Tab" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    AdvanceSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AdvanceSearchVC"];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:vc animated:NO completion:nil];
    
    
    
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
@end

