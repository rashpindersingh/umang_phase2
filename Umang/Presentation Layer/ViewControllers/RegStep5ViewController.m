//
//  RegStep5ViewController.m
//  Umang
//
//  Created by deepak singh rawat on 02/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "RegStep5ViewController.h"
#import "EnterRegFieldVC.h"

#define kOFFSET_FOR_KEYBOARD 80.0

#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "StateList.h"


@interface RegStep5ViewController ()<UIScrollViewDelegate,UITextFieldDelegate>
{
    EnterRegFieldVC *customVC;
    MBProgressHUD *hud ;
    
    UIDatePicker *datepicker;
    UIButton*  btnDone;
    UIButton*  btncancel;
    UIView*  vw_dateToolbar;
    StateList *obj;
    
    BOOL flag_apiHitStatus;
    NSDate *pickdate;

}
@property(retain,nonatomic)NSString *header_title_pass;
@property(retain,nonatomic)NSMutableArray *arr_table_pass;
@property(retain,nonatomic)NSString *TAG_pass;


@property(nonatomic,retain)NSMutableArray *citiesList;
@property(nonatomic,retain)NSMutableArray *districtList;
@property (nonatomic, strong)NSString *state_id;

@property (nonatomic, strong)NSString *city_id;
@property (nonatomic, strong)NSString *district_id;

@property (weak, nonatomic) IBOutlet UITextField *txt_name;
@property (weak, nonatomic) IBOutlet UITextField *txt_emailAddress;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Gender;

@property (weak, nonatomic) IBOutlet UILabel *lbl_state;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview_profile;

@property (weak, nonatomic) IBOutlet UILabel *lbl_dob;
@property (weak, nonatomic) IBOutlet UILabel *lbl_district;



@property (weak, nonatomic) IBOutlet UILabel *lbl_city;


@property (weak, nonatomic) IBOutlet UITextField *txt_alterMobile;

@property (weak, nonatomic) IBOutlet UIButton *btn_gender;
@property (weak, nonatomic) IBOutlet UIButton *btn_dob;

@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet UIButton *btn_skip;
- (IBAction)btnNextAction:(id)sender;

- (IBAction)btnSkipAction:(id)sender;

- (IBAction)btn_stateAction:(id)sender;
- (IBAction)btn_districtAction:(id)sender;
- (IBAction)btn_cityAction:(id)sender;

- (IBAction)btnDobAction:(id)sender;
- (IBAction)btnGenderAction:(id)sender;


- (IBAction)bgTouch:(id)sender;

@end

@implementation RegStep5ViewController
@synthesize arr_table_pass;
@synthesize header_title_pass;
@synthesize TAG_pass;

@synthesize citiesList,districtList;
@synthesize state_id,city_id,district_id;
@synthesize dic_info;
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    singleton=[SharedManager sharedSingleton];
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:REGISTRATION_PROFILE_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    NSLog(@"dic_info=%@",dic_info);
    flag_apiHitStatus=FALSE;
    self.btn_next.enabled=false;
    [self enableBtnNext:NO];

    self.txt_alterMobile.delegate=self;
    self.txt_name.delegate=self;
    self.txt_emailAddress.delegate=self;
    
    obj=[[StateList alloc]init];
    state_id=@"";
    
    

    self.scrollview_profile.delegate=self;
    
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollview_profile.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollview_profile.contentSize = contentRect.size;
    
    [self setNeedsStatusBarAppearanceUpdate];

    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    
}


-(void)hideKeyboard
{
       
    [self.txt_alterMobile resignFirstResponder];
    [self.txt_name resignFirstResponder];
    [self.txt_emailAddress resignFirstResponder];

}




- (IBAction)btnBackClicked:(id)sender
{
    
   [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)checkValidation
{
    if ([_txt_name.text isEqualToString:@""]) {
        self.btn_next.enabled=false;
        [self enableBtnNext:NO];
 
    }
 else   if ([_txt_emailAddress.text isEqualToString:@""]) {
        self.btn_next.enabled=false;
        [self enableBtnNext:NO];
 
    }
  else  if ([_lbl_Gender.text isEqualToString:@""]||[_lbl_Gender.text isEqualToString:@"Gender"]) {
        self.btn_next.enabled=false;
        [self enableBtnNext:NO];
  
    }
  else  if ([_lbl_dob.text isEqualToString:@""]||[_lbl_dob.text isEqualToString:@"Date of Birth"]) {
        self.btn_next.enabled=false;
        [self enableBtnNext:NO];
  
    }
  else  if ([_lbl_state.text isEqualToString:@""]||[_lbl_state.text isEqualToString:@"State"]) {
        self.btn_next.enabled=false;
        [self enableBtnNext:NO];
  
    }
 else   if ([_lbl_district.text isEqualToString:@""]||[_lbl_district.text isEqualToString:@"District"]) {
        self.btn_next.enabled=false;
        [self enableBtnNext:NO];
 
    }
   else if ([_txt_alterMobile.text isEqualToString:@""]) {
        //self.btn_next.enabled=false;
       //[self enableBtnNext:NO];
       self.btn_next.enabled=TRUE;
       [self enableBtnNext:TRUE];
    }
    else
    {
        
        
        self.btn_next.enabled=TRUE;
        [self enableBtnNext:TRUE];
  
    }

}


//------- Start of validation-------

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
    NSString *phoneRegex =@"[6789][0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
}


- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}



-(BOOL)validateNameWithString:(NSString*)nametopass
{
    NSString *nameRegex =@"^[a-zA-Z\\s]*$";
    
    NSPredicate *testRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    if(![testRegex evaluateWithObject:nametopass])
        return NO;
    else
        return YES;
    
}

//------- End of validation-------
- (IBAction)btnNextAction:(id)sender
{
    
   
    if ([self validateNameWithString:_txt_name.text]!=TRUE) {
        
        NSLog(@"Wrong Name");
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:@"Invalid Name" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
    }
    
    
   else if ([self validateEmailWithString:_txt_emailAddress.text]!=TRUE) {
        
        NSLog(@"Wrong email");
       UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:@"Invalid email-id" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
       [alert show];
    }
    
    
  else  if ([self validatePhone:_txt_alterMobile.text]!=TRUE) {
        
        NSLog(@"Wrong Mobile");
      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:@"Invalid Mobile Number" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
      [alert show];
    }
    
    else
    {
        [self hitAPI];
  
    }
    
     // }

 


}




//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
  

    //(dd-mm-yyyy)
    
    
    
    //  NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
    //NSString *mpinStrEncrypted=[singleton.user_mpin sha256HashFor:singleton.user_mpin];
    
    
    
    NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",singleton.user_mpin,strSaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    //NOTE mpinStrEncrypted value will be pass everywher
   // [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];
    
    
    
    NSString *userGender=@"";
    
    if([_lbl_Gender.text isEqualToString:@"Male"]||[_lbl_Gender.text isEqualToString:@"male"]||[_lbl_Gender.text isEqualToString:@"MALE"])
    {
        userGender=@"m";
    }
    if([_lbl_Gender.text isEqualToString:@"Female"]||[_lbl_Gender.text isEqualToString:@"female"]||[_lbl_Gender.text isEqualToString:@"FEMALE"])
    {
        userGender=@"f";
        
    }
    else
    {
        //  userGender=@"t";
        
    }
    
     state_id=[obj getStateCode:_lbl_state.text];
    
    
    NSLog(@"state_id=%@",state_id);
    
    
    if ([_txt_alterMobile.text length]==0) {
        _txt_alterMobile.text=@"";
    }
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:_txt_name.text forKey:@"nam"]; //Enter user name
    [dictBody setObject:userGender forKey:@"gndr"];//Enter gender of user(m/f/t)
    [dictBody setObject:_lbl_dob.text forKey:@"dob"]; //Enter DOB of User
    [dictBody setObject:city_id forKey:@"cty"]; //Enter District of User
    [dictBody setObject:state_id forKey:@"st"]; //Enter State Of user
    [dictBody setObject:district_id forKey:@"dist"]; //Enter District of User

    
    [dictBody setObject:_txt_alterMobile.text forKey:@"amno"];//Enter alternate mobile number of user
    [dictBody setObject:_txt_emailAddress.text forKey:@"email"];//Enter email address by user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];  //MPIN
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_UPDATE_PROFILE withBody:dictBody andTag:TAG_REQUEST_UPDATE_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];

        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                [self alertwithMsg:rd];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}


-(void)alertwithMsg:(NSString*)msg
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"profile_label", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                      // singleton.user_mpin=@"";
                                       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                       UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                                       // tbc.selectedIndex=[SharedManager getSelectedTabIndex];
                                       tbc.selectedIndex=0;

                                       [self presentViewController:tbc animated:NO completion:nil];                                   }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}






- (IBAction)btnSkipAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
     //tbc.selectedIndex=[SharedManager getSelectedTabIndex];
    tbc.selectedIndex=0;

    [self presentViewController:tbc animated:NO completion:nil];
}

- (IBAction)btn_stateAction:(id)sender
{

    
    [self loadDataState];
    flag_apiHitStatus=FALSE;
}

- (IBAction)btn_districtAction:(id)sender
{
    if ([state_id length]!=0) {
        header_title_pass=@"District";
        [self loadDataDistrict];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:@"Please select state!"                                                           delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    

}

- (IBAction)btn_cityAction:(id)sender
{
   
    
    if ([state_id length]!=0)
    {
        header_title_pass=@"City";
        
        [self loadDataCity];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:@"Please select state!"                                                           delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    

    
 }

- (IBAction)btnGenderAction:(id)sender
{
    header_title_pass=@"Gender";

    [self loadDataGender];

}
- (IBAction)btnDobAction:(id)sender
{
    [self openDOBpicker];
}


-(void)loadDataCity
{
    
    TAG_pass=TAG_CITY;
    
    
   
    arr_table_pass=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[citiesList count]; i++) {
        [arr_table_pass addObject:[[citiesList objectAtIndex:i]valueForKey:@"cnam"]];
    }
    
   // notiTypeCitySelected
    
    [self callCustomPicker];
}




-(void)openDOBpicker
{
    
    
    for(UIView *subview in [self.view subviews]) {
        
        if ([subview isKindOfClass:[UIButton class]]) {
            [subview removeFromSuperview];
        }
        if ([subview isKindOfClass:[UIDatePicker class]]) {
            [subview removeFromSuperview];
        }
        if ([subview isKindOfClass:[UIView class]]) {
            [vw_dateToolbar removeFromSuperview];
        }
    }
    [self.view endEditing:YES];
    
    vw_dateToolbar =[[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-270.0, fDeviceWidth, 100)];
    
    vw_dateToolbar.backgroundColor=[UIColor grayColor];
    [self.view addSubview:vw_dateToolbar];
    
    btncancel = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btncancel setTitle:@"Cancel" forState:UIControlStateNormal];
    btncancel.titleLabel.textColor=[UIColor blueColor];
    
    btncancel.frame = CGRectMake(10.0,5.0,100.0, 40.0);
    [btncancel addTarget:self
                  action:@selector(cancelPicker:)
        forControlEvents:UIControlEventTouchUpInside];
    [vw_dateToolbar addSubview:btncancel];
    
    
    
    
    
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDone.titleLabel.textColor=[UIColor blueColor];
    
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    btnDone.frame = CGRectMake(fDeviceWidth-110,5,100.0, 40.0);
    [btnDone addTarget:self
                action:@selector(HidePicker:)
      forControlEvents:UIControlEventTouchUpInside];
    [vw_dateToolbar addSubview:btnDone];
    
    
    datepicker= [[UIDatePicker alloc] initWithFrame:CGRectMake(0, fDeviceHeight-220, fDeviceWidth,220 )];
    datepicker.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    datepicker.datePickerMode = UIDatePickerModeDate;
    datepicker.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:datepicker];
}

-(IBAction)cancelPicker:(id)sender
{
    [self resignFirstResponder];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         datepicker.frame = CGRectMake(0, -250, fDeviceWidth, 50);
                     } completion:^(BOOL finished) {
                         [datepicker removeFromSuperview];
                         [btnDone removeFromSuperview];
                         [btncancel removeFromSuperview];
                         [vw_dateToolbar removeFromSuperview];
                         
                         
                     }];
    
    [datepicker removeFromSuperview];
}



-(IBAction)HidePicker:(id)sender{
    [self resignFirstResponder];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         datepicker.frame = CGRectMake(0, -250, fDeviceWidth, 50);
                     } completion:^(BOOL finished) {
                         [datepicker removeFromSuperview];
                         [btnDone removeFromSuperview];
                         [btncancel removeFromSuperview];
                         [vw_dateToolbar removeFromSuperview];
                         
                         
                     }];
    
    [datepicker removeFromSuperview];
    
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-yyyy"];
    NSString *formattedDate = [df stringFromDate:[datepicker date]];
    pickdate=[datepicker date];
    NSLog(@"formattedDate String : %@",formattedDate);
    
   
    
    NSTimeInterval secondsBetween = [ [NSDate date] timeIntervalSinceDate:pickdate];
    
    int numberOfDays = secondsBetween / 86400;
    
    NSLog(@"There are %d days in between the two dates.", numberOfDays);
    
    
    if (numberOfDays <365)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:@"Age should not be less then one year"                                                           delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        singleton.profileDOBSelected= formattedDate;
        
        self.lbl_dob.text=formattedDate;
    }

    // 30-11-2001
}


-(void)loadDataGender
{
    TAG_pass=TAG_GENDER;
    header_title_pass=@"Gender";
    arr_table_pass=[[NSMutableArray alloc]initWithObjects:@"Male",@"Female",@"Other", nil];
    
    [self callCustomPicker];
}

-(void)loadDataState
{
    TAG_pass=TAG_STATE_PROFILE;
    header_title_pass=@"State";

    NSArray*arry=[obj getStateList];
    arry = [arry sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    header_title_pass=@"State";

    arr_table_pass=[[NSMutableArray alloc]init];
    
    arr_table_pass=[arry mutableCopy];
    
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=@"";
    
    [self callCustomPicker];
    
}




-(void)loadDataDistrict
{
    TAG_pass=TAG_DISTRICT;
    
    header_title_pass=@"District";

    
    arr_table_pass=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[districtList count]; i++) {
        [arr_table_pass addObject:[[districtList objectAtIndex:i] valueForKey:@"dnam"]];
    }
    
    
    
    
    
    
    [self callCustomPicker];
}





-(void)callCustomPicker
{
    //CustomPickerVC
    
  //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    customVC = [storyboard instantiateViewControllerWithIdentifier:@"EnterRegFieldVC"];
    
    
   // customVC.delegate=self;
    customVC.get_title_pass=header_title_pass;
    customVC.get_arr_element=arr_table_pass;
    customVC.get_TAG=TAG_pass;
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:customVC animated:NO completion:nil];
    
    
    

    
    
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitCityAPI
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:state_id forKey:@"stid"];  //get from mobile default email //not supported iphone
    
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_CITY withBody:dictBody andTag:TAG_REQUEST_FETCH_CITY completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
          //  NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                flag_apiHitStatus=TRUE;
                //singleton.user_tkn=tkn;
                citiesList=[[NSMutableArray alloc]init];
                citiesList=[[response valueForKey:@"pd"]valueForKey:@"cities"];
                
                districtList=[[NSMutableArray alloc]init];
                districtList=[[response valueForKey:@"pd"]valueForKey:@"district"];
                
                
                
                
                /*    cid = 9878;
                 cnam = Zira;
                 did = 295;
                 dnam = Amritsar;
                 
                 */
                [self refreshdata];
                
            }
            
        }
        else{
            flag_apiHitStatus=FALSE;
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}

-(void)refreshdata
{
  
    
    
    if ([citiesList count]!=0) {
        NSPredicate *cityfilter = [NSPredicate predicateWithFormat:@"cnam =%@",singleton.notiTypeCitySelected];
        NSArray *cityIdFilter = [citiesList filteredArrayUsingPredicate:cityfilter];
        
        NSPredicate *distfilter = [NSPredicate predicateWithFormat:@"dnam =%@",singleton.notiTypDistricteSelected];
        NSArray *distIdFilter = [districtList filteredArrayUsingPredicate:distfilter];
        
        
      
        if ([cityIdFilter count]!=0) {
            city_id=[[cityIdFilter objectAtIndex:0]valueForKey:@"cid"];
 
        }
        if ([distIdFilter count]!=0)
        {

        district_id=[[distIdFilter objectAtIndex:0]valueForKey:@"did"];
        }
        
        NSLog(@"cityIdFilter=%@",cityIdFilter);
        NSLog(@"distIdFilter=%@",distIdFilter);
    }
    
    
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——

    
    if (![singleton.profilestateSelected isEqualToString:@""]) {
        state_id= [obj getStateCode:singleton.profilestateSelected];
        
       // flag_apiHitStatus=TRUE;
        if (flag_apiHitStatus ==FALSE)
        {
            [self hitCityAPI];
 
        }
    }

    
  //  _txt_name.text=[NSString stringWithFormat:@"%@",singleton.profileNameSelected];

    
    if ([singleton.notiTypeGenderSelected length]!=0) {
        _lbl_Gender.text=[NSString stringWithFormat:@"%@",singleton.notiTypeGenderSelected];

    }
    
    if ([singleton.profilestateSelected length]!=0) {
        _lbl_state.text=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
 
    }
    if ([singleton.notiTypDistricteSelected length]!=0) {
        _lbl_district.text=[NSString stringWithFormat:@"%@",singleton.notiTypDistricteSelected];
 
    }
    if ([singleton.profileDOBSelected length]!=0) {
        _lbl_dob.text=[NSString stringWithFormat:@"%@",singleton.profileDOBSelected];
 
    }
    
    if ([singleton.notiTypeCitySelected length]!=0) {
        _lbl_city.text=[NSString stringWithFormat:@"%@",singleton.notiTypeCitySelected];
        
    }
    
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgTouch:)];
    [recognizer setNumberOfTapsRequired:1];
    [recognizer setNumberOfTouchesRequired:1];
    [self.scrollview_profile addGestureRecognizer:recognizer];
   
    [self refreshdata];

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

//----- Code to hide keyboard on background touch-------
- (IBAction)bgTouch:(id)sender
{
    
    [self.txt_alterMobile resignFirstResponder];
    [self.txt_name resignFirstResponder];
    [self.txt_emailAddress resignFirstResponder];
    [self checkValidation];
}




-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
    [self checkValidation];
}
//
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    UITouch * touch = [touches anyObject];
//    if(touch.phase == UITouchPhaseBegan) {
//        [self.txt_name resignFirstResponder];
//        [self.txt_alterMobile resignFirstResponder];
//        [self.txt_emailAddress resignFirstResponder];
//        
//    }
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==_txt_alterMobile)
    {
    
     if (_txt_alterMobile.text.length >= 10 && range.length == 0)
     {
        return NO; // return NO to not change text
     }
        
    }
    
           return YES;
        
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}
*/


@end
