//
//  ProfileFromMobileRegistration.m
//  Umang
//
//  Created by admin on 05/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ProfileFromMobileRegistration.h"

@interface ProfileFromMobileRegistration () <UITableViewDataSource,UITableViewDelegate>
{
    
    NSMutableArray *arrGeneralInformation;
    NSMutableArray *arrAccountInformation;
}

@property (weak, nonatomic) IBOutlet UITableView *tblFromMobileRegistration;

@end

@implementation ProfileFromMobileRegistration
@synthesize tblFromMobileRegistration;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: PROFILE_FROM_MOBILE_REGISTRATION];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];

    
    [tblFromMobileRegistration reloadData];
    
    
    
    
    
    arrGeneralInformation = [[NSMutableArray alloc]initWithObjects:@"NAME",@"GENDER",@"DATE OF BIRTH",@"STATE/UT", nil];
    
    arrAccountInformation = [[NSMutableArray alloc]initWithObjects:@"EMAIL ADDRESS",@"ALTERNATE MOBILE NUMBER", nil];
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
