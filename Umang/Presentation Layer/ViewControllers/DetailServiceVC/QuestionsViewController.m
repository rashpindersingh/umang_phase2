//
//  QuestionsViewController.m
//  Umang
//
//  Created by Lokesh Jain on 01/12/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "QuestionsViewController.h"
#import "RunOnMainThread.h"
@interface QuestionsViewController ()

@end

@implementation QuestionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [backButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    screenTitleLabel.text = self.screenTitleString;
    [self addNavigationView];
    
}
- (IBAction)backButtonPressed:(UIButton *)sender
{
    if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([self.tagComingFrom isEqualToString:@"Set MPIN"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

#pragma mark - UITableView DataSource and Delegate Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.questionsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"questionCell"];
    
    cell.textLabel.text = [[self.questionsArray objectAtIndex:indexPath.row] valueForKey:@"question"];
    
    if (fDeviceWidth == 320)
    {
        cell.textLabel.font = [AppFont regularFont:15.0];
    }
    else
    {
        cell.textLabel.font = [AppFont regularFont:16.0];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    
   __block NSString *questionString = [[self.questionsArray objectAtIndex:indexPath.row] valueForKey:@"question"];
    __weak typeof(self) weakSelf = self;

    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [weakSelf pushBack:questionString];
    }];
    
}
-(void)pushBack:(NSString *)ans{
    
    if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
    {
        [self dismissViewControllerAnimated:false completion:nil];
    }
    else if ([self.tagComingFrom isEqualToString:@"Set MPIN"])
    {
        [self dismissViewControllerAnimated:false completion:nil];
    }
    else if ([self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"])
    {
        [self dismissViewControllerAnimated:false completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    if (ans != nil && ans.length != 0)
    {
        self.finishState(ans);
    }
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    backButton.hidden = true;
    screenTitleLabel.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backButtonPressed:btnBack];
    };
    nvView.lblTitle.text = screenTitleLabel.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tbFrame = self.questionsTableView.frame;
  self.questionsTableView.translatesAutoresizingMaskIntoConstraints = true ;
        tbFrame.origin.y = 84.0;
        self.questionsTableView.frame = tbFrame;
        [self.view layoutIfNeeded];
        [self.view updateConstraintsIfNeeded];
    }
}
#pragma mark -
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
