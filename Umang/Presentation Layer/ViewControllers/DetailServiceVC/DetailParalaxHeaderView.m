//
//  DetailParalaxHeaderView.m
//  Umang
//
//  Created by admin on 17/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DetailParalaxHeaderView.h"
#import "StateList.h"
@implementation DetailParalaxHeaderView
@synthesize scrollView_nouse;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.serviceName.titleLabel.font = [AppFont regularFont:18];
        self.lblServiceRating.font = [AppFont lightFont:18];
        self.lblCat_Service.font = [AppFont lightFont:18];
        self.stateName.font = [AppFont lightFont:18];
        [[self.logoSuperView viewWithTag:8899]setHidden:YES];
    }
    return self;
}
-(void)updateViewServiceDirecoryForRightAlignLanguage {
    
    CGRect frameSuperLogo = self.logoSuperView.frame;
    CGRect frameServiceName = self.serviceName.frame;
    frameSuperLogo.origin.x = (fDeviceWidth - 100);
    
    frameServiceName.size.width = (fDeviceWidth - 130);
    frameServiceName.origin.x = (fDeviceWidth - 100) - (frameServiceName.size.width + 10);
    self.logoSuperView.translatesAutoresizingMaskIntoConstraints = YES;
    self.logoSuperView.frame = frameSuperLogo;
    self.serviceName.translatesAutoresizingMaskIntoConstraints = YES;
    self.serviceName.frame = frameServiceName;
    
    [self layoutSubviews];
    [self updateConstraints];
    
    
}
-(void)updateViewForRightAlignLanguage {
    
    CGRect frameSuperLogo = self.logoSuperView.frame;
    CGRect frameRate = self.lblServiceRating.frame;
    CGRect frameViewRating = self.ratingView.frame;
    CGRect frameCat = self.lblCat_Service.frame;
    CGRect frameStateName = self.stateName.frame;
    CGRect frameServiceName = self.serviceName.frame;
    frameSuperLogo.origin.x = (fDeviceWidth - 100);
    
    frameServiceName.size.width = (fDeviceWidth - 130);
    frameServiceName.origin.x = (fDeviceWidth - 100) - (frameServiceName.size.width + 10);
    
    frameRate.origin.x = (fDeviceWidth - 100) - (frameRate.size.width + 10);
    frameViewRating.origin.x = (fDeviceWidth - 100) - (frameViewRating.size.width + frameRate.size.width + 13);
    frameCat.origin.x = (fDeviceWidth - 100) - (frameCat.size.width + 10);
    frameStateName.origin.x = (fDeviceWidth - (100 + CGRectGetWidth(frameCat) + 5)) - (frameStateName.size.width + 10);
    
    self.logoSuperView.translatesAutoresizingMaskIntoConstraints = YES;
    self.logoSuperView.frame = frameSuperLogo;
    
    self.lblServiceRating.translatesAutoresizingMaskIntoConstraints = YES;
    self.lblServiceRating.frame = frameRate;
    
    self.ratingView.translatesAutoresizingMaskIntoConstraints = YES;
    self.ratingView.frame = frameViewRating;
    
    self.lblCat_Service.translatesAutoresizingMaskIntoConstraints = YES;
    self.lblCat_Service.frame = frameCat;
    
    self.stateName.translatesAutoresizingMaskIntoConstraints = YES;
    self.stateName.frame = frameStateName;
    
    self.serviceName.translatesAutoresizingMaskIntoConstraints = YES;
    self.serviceName.frame = frameServiceName;
    //  self.allViews.translatesAutoresizingMaskIntoConstraints = YES;
    // self.allViews.frame = frameLogoSuper;
    [self layoutSubviews];
    [self updateConstraints];
    
    
}

- (void)didChangeStretchFactor:(CGFloat)stretchFactor {
    NSLog(@"stretchFactor--%f", stretchFactor);
    CGFloat alpha = CGFloatTranslateRange(stretchFactor, 0.2, 0.8, 0, 1);
    alpha = MAX(0, MIN(1, alpha));
    
    self.allViews.translatesAutoresizingMaskIntoConstraints = YES;
    if (stretchFactor < 1) {
        self.allViews.top = CGFloatInterpolate(stretchFactor, -100, 0);
    }
    else {
        self.allViews.top = 0;
    }
    
//    self.logoSuperView.alpha = alpha;
//    self.allViews.alpha = alpha;
//    self.backGroundImage.alpha = alpha;
    
}
-(void)addCategoryStateItemsToScrollView:(NSArray*)arrCat state:(NSArray*)arrStates withFont:(CGFloat)fontSize cell:(DetailParalaxHeaderView*)cell
{
    
    if (scrollView_nouse != nil)
    {
        [scrollView_nouse removeFromSuperview];
        scrollView_nouse = nil;
        //return;
    }
    
    CGFloat scrollWidth = fDeviceWidth - 100;
    scrollView_nouse = [[UIScrollView alloc]
                        initWithFrame:CGRectMake(CGRectGetMinX(self.lblServiceRating.frame), CGRectGetMaxY(_lblServiceRating.frame) + 6, scrollWidth, 26)];
    
    SharedManager *singlton = [SharedManager sharedSingleton];
    if (singlton.isArabicSelected)
    {
        scrollView_nouse = nil;
        scrollWidth = fDeviceWidth - 120;
        
        scrollView_nouse = [[UIScrollView alloc]
                             initWithFrame:CGRectMake(10, CGRectGetMaxY(_lblServiceRating.frame)+6, scrollWidth, 26)];
        //self.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
    }
    
    [cell.allViews addSubview:scrollView_nouse];
    scrollView_nouse.showsHorizontalScrollIndicator = false;
    scrollView_nouse.showsHorizontalScrollIndicator = false;
    
    cell.lblCat_Service.hidden = YES;
    cell.stateName.hidden = YES;
    CGFloat xCord = singlton.isArabicSelected ? scrollWidth : 4;
    CGFloat yCord = 3;
    CGFloat padding = 3;
    CGFloat itemHeight = 20;
    CGFloat arabicContentSize = 4;
    for (int i = 0; i < arrCat.count; i++)
    {
        NSString *stateName = arrCat[i ];
        if (stateName.length == 0) {
            continue;
        }
        CGRect frame = [self rectForText:stateName usingFont:[AppFont regularFont:fontSize] boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 40;
        UILabel *lbl = [self labelWithBorderColor:[UIColor colorWithRed:247/255.0 green:161/255.0 blue:41/255.0 alpha:1.0]];//[self labelWithBorderColor:[UIColor colorWithRed:228/255.0 green:100.0/255.0 blue:52.0/255.0 alpha:1.0]];//[[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        
        lbl.font = [AppFont lightFont:fontSize];
        lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
        
        if (singlton.isArabicSelected) {
            xCord -= (frame.size.width + padding);
            lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
            arabicContentSize += frame.size.width + padding;
        } else {
            xCord+=frame.size.width + padding;
        }
        lbl.text = [stateName uppercaseString];;
        lbl.userInteractionEnabled = YES;
      //  lbl.tag = indexpath.row;
        [scrollView_nouse addSubview:lbl];
    }
    // *** custom logic for user state selected ***
    StateList *obj = [[StateList alloc]init];
    singlton.user_StateId = [singlton getStateId];
    NSString *stateTab = @"";
    NSString *userState = @"";
    if (singlton.profilestateSelected.length != 0)
    {
        userState =singlton.profilestateSelected; //[obj getStateName:singlton.profilestateSelected];
    }
    if (singlton.user_StateId.length != 0)
    {
        stateTab = [obj getStateName:singlton.user_StateId];
    }
    NSMutableArray *arrFinal = [[NSMutableArray alloc] init];
    NSMutableArray *arrStatesAll = [[NSMutableArray alloc] initWithArray:arrStates];
    if (userState.length != 0 || stateTab.length != 0) {
        if ([arrStatesAll containsObject:userState]) {
            [arrFinal insertObject:userState atIndex:0];
            [arrStatesAll removeObject:userState];
        }
        else if ([arrStatesAll containsObject:stateTab]) {
            [arrFinal addObject:stateTab];
            [arrStatesAll removeObject:stateTab];
        }
    }
    NSArray *sortedArrayOfString = [arrStatesAll sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [(NSString *)obj1 compare:(NSString *)obj2 options:NSNumericSearch];
    }];
    [arrFinal addObjectsFromArray:sortedArrayOfString];
    
    for (int i = 0; i < arrFinal.count; i++)
    {
        
        NSString *stateName = arrFinal[i ];
        if (stateName.length == 0) {
            continue;
        }
        CGRect frame = [self rectForText:stateName usingFont:[AppFont regularFont:fontSize] boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 40;
        UILabel *lbl = [self labelWithBorderColor:[UIColor colorWithRed:57.0/255.0 green:236.0/255.0 blue:105.0/255.0 alpha:1.0]];//[self labelWithBorderColor:[UIColor colorWithRed:86.0/255.0 green:190.0/255.0 blue:113.0/255.0 alpha:1.0]];//[[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        lbl.font = [AppFont lightFont:fontSize];
        lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
        if (singlton.isArabicSelected) {
            xCord -= (frame.size.width + padding);
            lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
            arabicContentSize += frame.size.width + padding;
        } else {
            xCord+=frame.size.width + padding;
        }
        lbl.text = [stateName uppercaseString];
        lbl.userInteractionEnabled = YES;
        //lbl.tag = indexpath.row;
        [scrollView_nouse addSubview:lbl];
    }
    xCord = singlton.isArabicSelected ? arabicContentSize : xCord;
    [scrollView_nouse setContentSize:CGSizeMake(xCord, scrollView_nouse.frame.size.height)];
    if (singlton.isArabicSelected) {
        [scrollView_nouse setContentInset:UIEdgeInsetsMake(0, arabicContentSize/2, 0,0)];
    }
    
    [cell.allViews bringSubviewToFront:scrollView_nouse];
}

-(void)addCategoryStateItemsToScrollViewServiceDetails:(NSArray*)arrCat state:(NSArray*)arrStates withFont:(CGFloat)fontSize cell:(DetailParalaxHeaderView*)cell {
    if (scrollView_nouse != nil)
    {
        [scrollView_nouse removeFromSuperview];
        scrollView_nouse = nil;
        //return;
    }
    CGFloat scrollWidth = fDeviceWidth -  (CGRectGetWidth(self.logoSuperView.frame) + 16.0);
    scrollView_nouse = [[UIScrollView alloc]
                        initWithFrame:CGRectMake(CGRectGetMinX(_serviceName.frame), CGRectGetMaxY(_serviceName.frame), scrollWidth, 26)];
    
    SharedManager *singlton = [SharedManager sharedSingleton];
    if (singlton.isArabicSelected)
    {
        scrollView_nouse = nil;
        scrollWidth = fDeviceWidth - (CGRectGetWidth(self.logoSuperView.frame) + 20.0);
        
        scrollView_nouse = [[UIScrollView alloc]
                            initWithFrame:CGRectMake(10, CGRectGetMaxY(_serviceName.frame), scrollWidth, 26)];
        //self.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
    }
    
    [cell.allViews addSubview:scrollView_nouse];
    scrollView_nouse.showsHorizontalScrollIndicator = false;
    scrollView_nouse.showsHorizontalScrollIndicator = false;
    CGFloat xCord = singlton.isArabicSelected ? scrollWidth : 0;
    CGFloat yCord = 3;
    CGFloat padding = 3;
    CGFloat itemHeight = 20;
    CGFloat arabicContentSize = 4;
    for (int i = 0; i < arrCat.count; i++)
    {
        NSString *stateName = arrCat[i ];
        if (stateName.length == 0) {
            continue;
        }
        CGRect frame = [self rectForText:stateName usingFont:[AppFont regularFont:fontSize] boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 40;
        UILabel *lbl = [self labelWithBorderColor:[UIColor colorWithRed:247/255.0 green:161/255.0 blue:41/255.0 alpha:1.0]];//[self labelWithBorderColor:[UIColor colorWithRed:228/255.0 green:100.0/255.0 blue:52.0/255.0 alpha:1.0]];//[[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        
        lbl.font = [AppFont lightFont:fontSize];
        lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
        
        if (singlton.isArabicSelected) {
            xCord -= (frame.size.width + padding);
            lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
            arabicContentSize += frame.size.width + padding;
        } else {
            xCord+=frame.size.width + padding;
        }
        lbl.text = [stateName uppercaseString];;
        lbl.userInteractionEnabled = YES;
        //  lbl.tag = indexpath.row;
        [scrollView_nouse addSubview:lbl];
    }
    // *** custom logic for user state selected ***
    StateList *obj = [[StateList alloc]init];
    singlton.user_StateId = [singlton getStateId];
    NSString *stateTab = @"";
    NSString *userState = @"";
    if (singlton.profilestateSelected.length != 0)
    {
        userState =singlton.profilestateSelected; //[obj getStateName:singlton.profilestateSelected];
    }
    if (singlton.user_StateId.length != 0)
    {
        stateTab = [obj getStateName:singlton.user_StateId];
    }
    NSMutableArray *arrFinal = [[NSMutableArray alloc] init];
    NSMutableArray *arrStatesAll = [[NSMutableArray alloc] initWithArray:arrStates];
    if (userState.length != 0 || stateTab.length != 0) {
        if ([arrStatesAll containsObject:userState]) {
            [arrFinal insertObject:userState atIndex:0];
            [arrStatesAll removeObject:userState];
        }
        else if ([arrStatesAll containsObject:stateTab]) {
            [arrFinal addObject:stateTab];
            [arrStatesAll removeObject:stateTab];
        }
    }
    NSArray *sortedArrayOfString = [arrStatesAll sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [(NSString *)obj1 compare:(NSString *)obj2 options:NSNumericSearch];
    }];
    [arrFinal addObjectsFromArray:sortedArrayOfString];
    
    for (int i = 0; i < arrFinal.count; i++)
    {
        
        NSString *stateName = arrFinal[i ];
        if (stateName.length == 0) {
            continue;
        }
        CGRect frame = [self rectForText:stateName usingFont:[AppFont regularFont:fontSize] boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 40;
        UILabel *lbl = [self labelWithBorderColor:[UIColor colorWithRed:57.0/255.0 green:236.0/255.0 blue:105.0/255.0 alpha:1.0]];//[self labelWithBorderColor:[UIColor colorWithRed:86.0/255.0 green:190.0/255.0 blue:113.0/255.0 alpha:1.0]];//[[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        lbl.font = [AppFont lightFont:fontSize];
        lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
        if (singlton.isArabicSelected) {
            xCord -= (frame.size.width + padding);
            lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
            arabicContentSize += frame.size.width + padding;
        } else {
            xCord+=frame.size.width + padding;
        }
        lbl.text = [stateName uppercaseString];
        lbl.userInteractionEnabled = YES;
        //lbl.tag = indexpath.row;
        [scrollView_nouse addSubview:lbl];
    }
    xCord = singlton.isArabicSelected ? arabicContentSize : xCord;
    [scrollView_nouse setContentSize:CGSizeMake(xCord, scrollView_nouse.frame.size.height)];
    if (singlton.isArabicSelected) {
        [scrollView_nouse setContentInset:UIEdgeInsetsMake(0, arabicContentSize/2, 0,0)];
    }
    [cell.allViews bringSubviewToFront:scrollView_nouse];
}


-(UILabel*)labelWithBorderColor:(UIColor*)borderColor {
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textColor = [UIColor whiteColor];
    lbl.highlightedTextColor = [UIColor whiteColor];
    lbl.backgroundColor = borderColor;
    lbl.layer.cornerRadius = 10;//12.5;
    lbl.layer.masksToBounds = YES;
    lbl.clipsToBounds = YES;
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.layer.borderColor = [borderColor CGColor];
    lbl.layer.borderWidth = 1.0;
    return lbl;
}
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}
@end

