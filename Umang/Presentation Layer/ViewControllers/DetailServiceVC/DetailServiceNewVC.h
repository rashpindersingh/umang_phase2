//
//  DetailServiceNewVC.h
//  Umang
//
//  Created by admin on 17/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CellLinksWebsite:UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLinksLabelConstraint;



@end
@interface DetailServiceNewVC : UIViewController
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *naviHeightConstraint;
@property(nonatomic,retain)NSDictionary *dic_serviceInfo;
@property(nonatomic,retain)NSString *isFrom;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;

@end
