//
//  CallBoxTVC.h
//  Umang
//
//  Created by admin on 12/09/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>


@interface CallBoxTVC : UIViewController<MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblContacts;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightOfTable;

@property (strong)NSString *call;
@property (nonatomic,strong) NSString *comingFrom;

@end
