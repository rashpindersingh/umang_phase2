//
//  CallBoxTVC.m
//  Umang
//
//  Created by admin on 12/09/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "CallBoxTVC.h"

@interface CallBoxTVC ()
{
    NSMutableDictionary *dataSource;
    NSMutableArray *arrHeadings;
    CGFloat fontApplied;
    
}
@end

@implementation CallBoxTVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    fontApplied = 19.0;
    SharedManager *Singlton = [SharedManager sharedSingleton];
    if ([Singlton.fontSizeSelected isEqualToString:NSLocalizedString(@"large", nil)])
    {
        fontApplied=21.0;
        
    }
    //fontApplied += 2;
    dataSource = [[NSMutableDictionary alloc] init];
    arrHeadings = [[NSMutableArray alloc] init];

    BOOL shouldTap = false;
    
    if ([self.call containsString:@"|"])
    {
       NSArray *arrHead = [self.call componentsSeparatedByString:@"|"];
        for (NSString *str in arrHead) {
            NSArray *head = [str componentsSeparatedByString:@"#"];
            NSArray *numbers = [[head objectAtIndex:1] componentsSeparatedByString:@","];
            [self addHeadingWithNumber:head.firstObject with:numbers];
        }
    }
    else if ([self.call containsString:@"#"]) {
        NSArray *head = [self.call componentsSeparatedByString:@"#"];
        NSArray *numbers = [[head objectAtIndex:1] componentsSeparatedByString:@","];
        [self addHeadingWithNumber:head.firstObject with:numbers];
    }
    else {
        [self addTapToCall];
    }
    NSInteger countTable = arrHeadings.count - 1;
    CGFloat height = (countTable *75 ) + 60;
    countTable = 0;
    for (NSString *strKey in arrHeadings) {
        countTable += [[dataSource valueForKey:strKey] count];
    }
    CGFloat tblHeight = (40 * countTable) + height + 15;
    if (tblHeight <= (fDeviceHeight - 200) ) {
        self.heightOfTable.constant = tblHeight;
        self.tblContacts.scrollEnabled = false;
    }else {
        self.heightOfTable.constant = fDeviceHeight - 250;
        self.tblContacts.scrollEnabled = true;
    }
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
    [self.tblContacts reloadData];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideView)];
    [[self.view viewWithTag:2323] addGestureRecognizer:tap];
}
- (IBAction)didTapCrossButtonAction:(UIButton *)sender {
    [self hideView];
}
-(void)hideView {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
-(void)addHeadingWithNumber:(NSString*)heading with:(NSArray*)numbers
{
    [arrHeadings addObject:heading];
    [dataSource setValue:numbers forKey:heading];
}
-(void)addTapToCall
{
    if ([self.comingFrom isEqualToString:@"call"])
    {
        NSArray *arrCalls = [self.call componentsSeparatedByString:@","];
        [arrHeadings addObject:NSLocalizedString(@"tap_a_number_to_call", nil)];
        [dataSource setValue:arrCalls forKey:arrHeadings.firstObject];
    }
    else
    {
        NSArray *arrCalls = [self.call componentsSeparatedByString:@","];
        [arrHeadings addObject:NSLocalizedString(@"select_email", nil)];
        [dataSource setValue:arrCalls forKey:arrHeadings.firstObject];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrHeadings.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[dataSource valueForKey:[arrHeadings objectAtIndex:section]] count];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    CGSize tblFrame = self.tblContacts.frame.size;
    UITableViewCell *cell = [[ UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tblFrame.width, 60)];
    cell.backgroundColor = [UIColor whiteColor];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tblFrame.width, 60)];
    if (arrHeadings.count != 0) {
        if (section != 0 )  {
            UIView *viewLine = [[UIView alloc] initWithFrame:CGRectMake(0, 20, fDeviceWidth, 2)];
                viewLine.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [cell addSubview:viewLine];
            [cell bringSubviewToFront:viewLine];
            cell.frame = CGRectMake(0, 0, tblFrame.width, 75);
            lbl.frame = CGRectMake(0, 25, tblFrame.width, 50);
        }
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = [arrHeadings objectAtIndex:section];
        lbl.textColor = [UIColor colorWithRed:0.0/255.0 green:89.0/255.0 blue:157.0/255.0 alpha:1.0];///[UIColor colorWithRed:0.0/255.0 green:89.0/255.0 blue:157.0/255.0 alpha:1];
        lbl.font = [UIFont systemFontOfSize:fontApplied + 1];
        lbl.numberOfLines = 10;
        lbl.lineBreakMode = NSLineBreakByWordWrapping;
        [cell addSubview:lbl];
    }
    // Configure the cell...
    return cell;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellCallBox" forIndexPath:indexPath];
    NSArray *arrCals = [dataSource valueForKey:[arrHeadings objectAtIndex:indexPath.section]];
    if (arrCals.count != 0) {
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.text = [arrCals objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [UIColor colorWithRed:60.0/255.0 green:172.0/255.0 blue:110.0/255.0 alpha:1];
        cell.textLabel.font = [UIFont systemFontOfSize:fontApplied];
    }
    // Configure the cell...
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

     if (section != 0 )  {
         return 75;
     }
    return 60;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (arrHeadings.count == 1 )  {
        return 0.0;
    }
    return 0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.comingFrom isEqualToString:@"call"])
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSString*  callNumber=[NSString stringWithFormat:@"%@",cell.textLabel.text];
        callNumber = [callNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",callNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
    }
    else
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSString*  emailAddress = [NSString stringWithFormat:@"%@",cell.textLabel.text];
        emailAddress = [emailAddress stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        if ([MFMailComposeViewController canSendMail]) {
            
            
            NSString *receipt = emailAddress;
            // Email Subject
            NSString *emailTitle = @"Umang Services Email";
            // Email Content
            NSString *messageBody = @"Sent by Umang App!";
            // To address
            // To address
            NSArray *toRecipents = [NSArray arrayWithObject:receipt];
            
            picker.mailComposeDelegate = self;
            [picker setSubject:emailTitle];
            [picker setMessageBody:messageBody isHTML:YES];
            [picker setToRecipients:toRecipents];
            
            // Present mail view controller on screen
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }
    
    
   // [self hideView];
    
}

#pragma mark - Mail Composer Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
