//
//  ShowUserProfileVC.m
//  Umang
//
//  Created by admin on 30/10/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ShowUserProfileVC.h"
#import "ProfileCell.h"
#import "UserEditVC.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "StateList.h"
#import "SharedManager.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
@implementation CellShowUserProfile
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

@interface ShowUserProfileVC ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *arrGeneralInformation;
    NSMutableArray *arrImagePlaceholder;
    NSMutableArray *arrImageAccountPlaceholder;
    
    NSMutableArray *arrAccountInformation;
    UIView *vwHeader;
    
    SharedManager *singleton;
    MBProgressHUD *hud ;
    
    __weak IBOutlet UILabel *lblUmangHeader;
    
    //----- local values  to be save and show from API-----------
    NSString *str_name;
    NSString *str_gender;
    NSString *str_dob;
    NSString *str_qualification;
    NSString *str_occupation;
    NSString *str_state;
    NSString *str_district;
    NSString *str_registerMb;
    NSString *str_emailAddress;
    NSString *str_alternateMb;
    NSString *str_emailVerifyStatus;
    NSString *str_amnosVerifyStatus;
    NSString *str_address;
    
    __weak IBOutlet UIButton *btnBack;
    NSString *str_Url_pic;
    
    NSMutableArray *socialpd;
    
    
    
    
    //-----------------------------------------
    
    
    
    NSMutableArray *arrDataGeneralInformation;
    NSMutableArray *arrDataAccountInformation;
    
    
    StateList *obj;
    
    NSString *stringGender;
}
@property (strong,nonatomic) NSMutableArray *arrDataSource;
@property (retain, nonatomic)NSString *goimg;

@property (retain, nonatomic)NSString *fbid;

@property (retain, nonatomic)NSString *facebookId;

@property (retain, nonatomic)NSString *twitimg;

@end

@implementation ShowUserProfileVC
@synthesize  goimg;

@synthesize  fbid;

@synthesize  facebookId;

@synthesize  twitimg;


- (void)viewDidLoad
{
    [super viewDidLoad];
    _tblProfileInfo.dataSource = self;
    _tblProfileInfo.delegate = self;
    // Do any additional setup after loading the view.
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    stringGender=@"";
    singleton = [SharedManager sharedSingleton];
    // Do any additional setup after loading the view.
    //self.navigationController.navigationBarHidden=true;
    lblUmangHeader.text = NSLocalizedString(@"general", nil);
    
    
    self.navigationItem.title = @"";
    
    self.navigationController.navigationBarHidden=true;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    arrGeneralInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"name_caps", nil),NSLocalizedString(@"gender_caps", nil),NSLocalizedString(@"date_of_birth_caps", nil),NSLocalizedString(@"qualication_caps", nil),NSLocalizedString(@"occupation_caps", nil),NSLocalizedString(@"state_txt_caps", nil),NSLocalizedString(@"district_caps", nil),NSLocalizedString(@"address_caps", nil), nil];
    arrImagePlaceholder = [[NSMutableArray alloc] initWithObjects:@"icon_name",@"icon_gender",@"icon_dob",@"icon_qualification",@"icon_occupation",@"icon_state_ut",@"icon_district",@"icon_profile_address", nil];
    arrImageAccountPlaceholder = [[NSMutableArray alloc] initWithObjects:@"icon_profile_email",@"icon_alt_mobile", nil];
    arrAccountInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"email_address", nil),NSLocalizedString(@"alt_mob_num", nil), nil];
    [self setEditButtonLayer:15];
    [self setProfileData];
}
-(void)setEditButtonLayer:(CGFloat)cornerRadius{
    self.btnEditUserProfile.layer.cornerRadius = cornerRadius;
    self.btnEditUserProfile.layer.borderWidth = 1.5;
    self.btnEditUserProfile.layer.borderColor = [[UIColor grayColor] CGColor];
    self.btnEditUserProfile.clipsToBounds = YES;
    self.btnEditUserProfile.layer.masksToBounds = YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    @try {
        singleton = [SharedManager sharedSingleton];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (![self connected]) {
                // Not connected
                [_tblProfileInfo reloadData];
            } else {
                // Connected. Do some Internet stuff
                dispatch_queue_t queue = dispatch_queue_create("com.spice.umang", NULL);
                dispatch_async(queue, ^{
                    obj=[[StateList alloc]init];
                    //NSLog(@"Device is connected to the Internet");
                    [obj hitStateQualifiAPI];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self hitFetchProfileAPI];
                    });
                });
            }
        });
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
   
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    [self setViewFont];
    [self.view bringSubviewToFront:self.btnBack];
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    self.btnEditUserProfile.titleLabel.font = [AppFont regularFont:18.0];
    lblUmangHeader.font = [AppFont mediumFont:18.0];
    _lblName.font = [AppFont mediumFont:18.0];
    _lblMobile.font = [AppFont regularFont:13.0];
    
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
-(void)fillDataInString
{
    SharedManager *sharedObject = [SharedManager sharedSingleton];
    str_name = sharedObject.objUserProfile.objAadhar.name;
    str_gender = sharedObject.objUserProfile.objAadhar.gender;
    str_dob = sharedObject.objUserProfile.objAadhar.dob;
    str_state= sharedObject.objUserProfile.objAadhar.state;
    str_district = sharedObject.objUserProfile.objAadhar.district;
    str_address = sharedObject.objUserProfile.objAadhar.street;
}

- (IBAction)btneditAction:(id)sender
{
    @try {
        
        
        
        if([str_name length]==0)
        {
            str_name=@"";
        }
        
        if([str_gender length]==0)
        {
            str_gender=@"";
        }
        
        if([str_dob length]==0)
        {
            str_dob=@"";
        }
        
        if([str_qualification length]==0)
        {
            str_qualification=@"";
        }
        
        if([str_occupation length]==0)
        {
            str_occupation=@"";
        }
        
        if([str_state length]==0)
        {
            str_state=@"";
        }
        
        if([str_district length]==0)
        {
            str_district=@"";
        }
        
        
        if([str_registerMb length]==0)
        {
            str_registerMb=@"";
        }
        
        if([str_emailAddress length]==0)
        {
            str_emailAddress=@"";
        }
        
        if([str_alternateMb length]==0)
        {
            str_alternateMb=@"";
        }
        
        if([str_emailVerifyStatus length]==0)
        {
            str_emailVerifyStatus=@"";
        }
        
        if([str_amnosVerifyStatus length]==0)
        {
            str_amnosVerifyStatus=@"";
        }
        if([str_address length]==0)
        {
            str_address=@"";
        }
        if([str_Url_pic length]==0)
        {
            str_Url_pic=@"";
        }
        
        /* if([socialpd count]==0)
         {
         socialpd=@"";
         }*/
        
        if ([socialpd count]>0)
        {
            if ([socialpd count]>0)
            {
                
                goimg=[socialpd valueForKey:@"goimg"];
                twitimg=[socialpd valueForKey:@"twitimg"];
                fbid=[socialpd valueForKey:@"fbid"];
                
                facebookId=@"";
                //facebookId=[socialpd valueForKey:@"fbid"];
                if (![fbid isEqualToString:@""])
                {
                    facebookId=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookId];
                }
                
                
                
                
                
            }
        }
        
        //singleton.notiTypeGenderSelected
        singleton.profileNameSelected =str_name;
        singleton.profilestateSelected=str_state;
        singleton.notiTypeCitySelected=@"";
        singleton.notiTypDistricteSelected=str_district;
        singleton.profileDOBSelected=str_dob;
        singleton.altermobileNumber=str_alternateMb;
        singleton.user_Qualification=str_qualification;
        singleton.user_Occupation=str_occupation;
        singleton.profileEmailSelected=str_emailAddress;
        
        
        
        NSMutableDictionary *userInfo=[NSMutableDictionary new];
        [userInfo setObject:str_name forKey:@"str_name"];
        [userInfo setObject:str_gender forKey:@"str_gender"];
        [userInfo setObject:str_dob forKey:@"str_dob"];
        [userInfo setObject:str_qualification forKey:@"str_qualification"];
        [userInfo setObject:str_occupation forKey:@"str_occupation"];
        [userInfo setObject:str_state forKey:@"str_state"];
        [userInfo setObject:str_district forKey:@"str_district"];
        [userInfo setObject:str_registerMb forKey:@"str_registerMb"];
        [userInfo setObject:str_emailAddress forKey:@"str_emailAddress"];
        [userInfo setObject:str_alternateMb forKey:@"str_alternateMb"];
        [userInfo setObject:str_emailVerifyStatus forKey:@"str_emailVerifyStatus"];
        [userInfo setObject:str_address forKey:@"str_address"];
        [userInfo setObject:str_amnosVerifyStatus forKey:@"str_amnosVerifyStatus"];
        [userInfo setObject:str_Url_pic forKey:@"str_Url_pic"];
        
        
        if([goimg length]==0)
        {
            goimg=@"";
        }
        if([twitimg length]==0)
        {
            twitimg=@"";
        }
        if([facebookId length]==0)
        {
            facebookId=@"";
        }
        
        
        
        
        [userInfo setObject:goimg forKey:@"goimg"];
        [userInfo setObject:twitimg forKey:@"twitimg"];
        [userInfo setObject:facebookId forKey:@"facebookId"];
        
        
        
        
        
        NSLog(@"str_Url_pic  ====> %@",str_Url_pic);
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    /*
     EditUserInfoVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditUserInfoVC"];
     vc.dic_info=userInfo;
     vc.tagFrom=@"ISFROMPROFILEUPDATE";
     //vc.userInfo=[]
     vc.hidesBottomBarWhenPushed = YES;
     [self.navigationController pushViewController:vc animated:YES];*/
    
}


- (IBAction)btnBackAction:(id)sender {
    
    [singleton traceEvents:@"Back" withAction:@"Clicked" withLabel:@"UserProfile" andValue:0];

    ////dispatch_async(dispatch_get_main_queue(), ^{
    [self.navigationController popViewControllerAnimated:YES];
    //});
    
}
-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(-10.0, 1.0);
    vwItem.layer.shadowColor = [UIColor grayColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}
- (IBAction)btnEditClicked:(id)sender
{
    @try {
        
        [singleton traceEvents:@"Edit Button" withAction:@"Clicked" withLabel:@"UserProfile" andValue:0];

        
        if([str_name length]==0)
        {
            str_name=@"";
        }
        
        if([str_gender length]==0)
        {
            str_gender=@"";
        }
        
        if([str_dob length]==0)
        {
            str_dob=@"";
        }
        
        if([str_qualification length]==0)
        {
            str_qualification=@"";
        }
        
        if([str_occupation length]==0)
        {
            str_occupation=@"";
        }
        
        if([str_state length]==0)
        {
            str_state=@"";
        }
        
        if([str_district length]==0)
        {
            str_district=@"";
        }
        
        
        if([str_registerMb length]==0)
        {
            str_registerMb=@"";
        }
        
        if([str_emailAddress length]==0)
        {
            str_emailAddress=@"";
        }
        
        if([str_alternateMb length]==0)
        {
            str_alternateMb=@"";
        }
        
        if([str_emailVerifyStatus length]==0)
        {
            str_emailVerifyStatus=@"";
        }
        
        if([str_amnosVerifyStatus length]==0)
        {
            str_amnosVerifyStatus=@"";
        }
        if([str_address length]==0)
        {
            str_address=@"";
        }
        if([str_Url_pic length]==0)
        {
            str_Url_pic=@"";
        }
        if ([socialpd count]>0)
        {
            
            goimg=[socialpd valueForKey:@"goimg"];
            twitimg=[socialpd valueForKey:@"twitimg"];
            fbid=[socialpd valueForKey:@"fbid"];
            
            facebookId=@"";
            //facebookId=[socialpd valueForKey:@"fbid"];
            if (![fbid isEqualToString:@""])
            {
                facebookId=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",fbid];
            }
            
            
            
            
            
            
        }
        
        
        
        
        //singleton.notiTypeGenderSelected
        singleton.profileNameSelected =str_name;
        singleton.profilestateSelected=str_state;
        singleton.notiTypeCitySelected=@"";
        singleton.notiTypDistricteSelected=str_district;
        singleton.profileDOBSelected=str_dob;
        singleton.altermobileNumber=str_alternateMb;
        singleton.user_Qualification=str_qualification;
        singleton.user_Occupation=str_occupation;
        singleton.profileEmailSelected=str_emailAddress;
        
        
        
        NSMutableDictionary *userInfo=[NSMutableDictionary new];
        [userInfo setObject:str_name forKey:@"str_name"];
        [userInfo setObject:str_gender forKey:@"str_gender"];
        [userInfo setObject:str_dob forKey:@"str_dob"];
        [userInfo setObject:str_qualification forKey:@"str_qualification"];
        [userInfo setObject:str_occupation forKey:@"str_occupation"];
        [userInfo setObject:str_state forKey:@"str_state"];
        [userInfo setObject:str_district forKey:@"str_district"];
        [userInfo setObject:str_registerMb forKey:@"str_registerMb"];
        [userInfo setObject:str_emailAddress forKey:@"str_emailAddress"];
        [userInfo setObject:str_alternateMb forKey:@"str_alternateMb"];
        [userInfo setObject:str_emailVerifyStatus forKey:@"str_emailVerifyStatus"];
        [userInfo setObject:str_address forKey:@"str_address"];
        [userInfo setObject:str_amnosVerifyStatus forKey:@"str_amnosVerifyStatus"];
        [userInfo setObject:str_Url_pic forKey:@"str_Url_pic"];
        
        if([goimg length]==0)
        {
            goimg=@"";
        }
        if([twitimg length]==0)
        {
            twitimg=@"";
        }
        if([facebookId length]==0)
        {
            facebookId=@"";
        }
        
        
        
        
        [userInfo setObject:goimg forKey:@"goimg"];
        [userInfo setObject:twitimg forKey:@"twitimg"];
        [userInfo setObject:facebookId forKey:@"facebookId"];
        
        
        
        
        
        //[userInfo setObject:socialpd forKey:@"socialpd"];
        
        NSLog(@"str_Url_pic  ====> %@",str_Url_pic);
        
        
        // if ([str_Url_pic length]!=0 ) {
        
        singleton.user_profile_URL=str_Url_pic;
        //}
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UserEditVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserEditVC"];
        //vc.userInfo=[]
        vc.dic_info=userInfo;
        vc.tagFrom=@"ISFROMPROFILEUPDATE";
        vc.imgtopass = UIImagePNGRepresentation(_userProfileImage.image);
        vc.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:vc animated:YES];
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}
-(NSString *)UpperFirstWord:(NSString*)inputString
{
    
    if ([inputString length]!=0) {
        inputString = [inputString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[inputString substringToIndex:1] uppercaseString]];
    }
    return inputString;
}

-(void)setProfileData
{
    
    //----- local values  to be save and show from API-----------
    
    
    str_name=[self UpperFirstWord:str_name];
    // str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    
    
    
    
    //if ([str_Url_pic length]!=0 ) {
    
    singleton.user_profile_URL=str_Url_pic;
    //}
    
    
    
    
    
    
    if ([str_gender length]!=0)
    {
        str_gender=[str_gender uppercaseString];
        
        if ([str_gender isEqualToString:@"M"]) {
            stringGender= NSLocalizedString(@"gender_male", nil);
            singleton.notiTypeGenderSelected= NSLocalizedString(@"gender_male", nil);
            
        }
        else  if ([str_gender isEqualToString:@"F"]) {
            singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_female", nil);
            stringGender=NSLocalizedString(@"gender_female", nil);
            
        }
        else  if ([str_gender isEqualToString:@"T"]||[str_gender isEqualToString:@"t"]) {
            singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_transgender", nil);
            stringGender=NSLocalizedString(@"gender_transgender", nil);
            
        }
        
        else
        {
            singleton.notiTypeGenderSelected=@"";
            stringGender=@"";
        }
        
        
    }
    
    else
    {
        singleton.notiTypeGenderSelected=@"";
        stringGender=@"";
    }
    
    
    
    str_name=[self UpperFirstWord:str_name];
    //str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    
    
    
    //singleton.notiTypeGenderSelected
    singleton.profileNameSelected =str_name;
    singleton.profilestateSelected=str_state;
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=str_district;
    singleton.profileDOBSelected=str_dob;
    singleton.altermobileNumber=str_alternateMb;
    singleton.user_Qualification=str_qualification;
    singleton.user_Occupation=str_occupation;
    singleton.profileEmailSelected=str_emailAddress;
    singleton.profileUserAddress=str_address;
    arrDataGeneralInformation = [[NSMutableArray alloc]initWithObjects:singleton.profileNameSelected,stringGender,singleton.profileDOBSelected,singleton.user_Qualification,singleton.user_Occupation,singleton.profilestateSelected,singleton.notiTypDistricteSelected,singleton.profileUserAddress, nil];
    arrDataAccountInformation = [[NSMutableArray alloc]initWithObjects:singleton.profileEmailSelected,singleton.altermobileNumber, nil];
    NSLog(@"str_gender=%@",str_gender);
    UIImage *tempImg;
    NSLog(@"str_gender=%@",str_gender);
    if ([str_gender length] >0)
    {
        if ([str_gender isEqualToString:@"M"])
        {
            stringGender=NSLocalizedString(@"gender_male", nil);
            tempImg=[UIImage imageNamed:@"male_avatar"];
        }
        else if ([str_gender isEqualToString:@"F"]) {
            stringGender=NSLocalizedString(@"gender_female", nil);
            tempImg=[UIImage imageNamed:@"female_avatar"];
        }
        else if ([str_gender isEqualToString:@"T"])
        {
            stringGender=NSLocalizedString(@"gender_transgender", nil);
            tempImg=[UIImage imageNamed:@"user_placeholder"];
        }
        else {
            stringGender=@"";
            tempImg=[UIImage imageNamed:@"user_placeholder"];
        }
        if([[NSFileManager defaultManager] fileExistsAtPath:singleton.imageLocalpath])
        {
            NSLog(@"file present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            tempImg = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
            _userProfileImage.image=tempImg;
        } else {
            NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
            if(![[url absoluteString] isEqualToString:@""])
            {
                [_userProfileImage sd_setImageWithURL:url
                                     placeholderImage:tempImg];
            }else {
                _userProfileImage.image=tempImg;
            }
        }
    }
    else
    {
        NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
        if(![[url absoluteString] isEqualToString:@""])
        {
            [_userProfileImage sd_setImageWithURL:url
                                 placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
        }
        else
        {
            _userProfileImage.image=[UIImage imageNamed:@"user_placeholder"];
        }
    }
    _userProfileImage.clipsToBounds=YES;
    _userProfileImage.layer.cornerRadius = _userProfileImage.frame.size.width/2.0;
    _userProfileImage.layer.masksToBounds = YES;
    _userProfileImage.layer.borderColor = [[UIColor whiteColor] CGColor];
    _userProfileImage.layer.borderWidth = 0.8;
    [_tblProfileInfo reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */





//----- hitAPI for IVR OTP call Type registration ------
-(void)hitFetchProfileAPI
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            
            NSString *responseStr=[NSString stringWithFormat:@"%@",response];
            if ([responseStr length]>0)
            {
                
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    
                {
                    
                    @try {
                        
                        
                        //singleton.user_tkn=tkn;
                        NSMutableArray *aadharpd=[[NSMutableArray alloc]init];
                        aadharpd=[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];
                        
                        
                        NSMutableArray *generalpd=[[NSMutableArray alloc]init];
                        generalpd=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                        
                        
                        
                        socialpd=[[NSMutableArray alloc]init];
                        
                        singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                        
                        NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                        
                        if ([abbreviation length]==0) {
                            abbreviation=@"";
                        }
                        
                        NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                        
                        emblemString = emblemString.length == 0 ? @"":emblemString;
                        [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"TabKey"] isEqualToString:@"YES"])
                        {
                            if (abbreviation.length != 0)
                            {
                                //NSString * tordc = [NSString stringWithFormat:@"%@",[singleton.arr_initResponse  valueForKey:@"tord"]];
                                //NSArray *array =[tordc componentsSeparatedByString:@"|,"];
                                NSArray *array =[NSArray arrayWithArray:singleton.AppTabOrders];

                                NSMutableArray *tempTabOrder =[NSMutableArray new];
                                NSString *home = [NSLocalizedString(@"home_small", @"") capitalizedString];
                                NSString *flagship = [NSLocalizedString(@"flagship", @"") capitalizedString];
                                NSString *fav = [NSLocalizedString(@"favourites_small", @"") capitalizedString];
                                NSString *allservices = [NSLocalizedString(@"all_services_small", @"") capitalizedString];
                                NSString *state = NSLocalizedString(@"state_txt", @"") ;
                                for (int i =0; i<[array count]; i++)
                                {
                                    NSString *tempItem = [array objectAtIndex:i];
                                    if ([tempItem containsString:@"home"])
                                    {
                                        NSLog(@"string contains home!");
                                        [tempTabOrder addObject: home];
                                    }
                                    else if ([tempItem containsString:@"HomeWithFav"]) {
                                        NSLog(@"string contains flagship!");
                                        [tempTabOrder addObject: home];
                                    }
                                    else if ([tempItem containsString:@"flagship"]) {
                                        NSLog(@"string contains flagship!");
                                        [tempTabOrder addObject: flagship];
                                    }
                                    else if ([tempItem containsString:@"fav"]) {
                                        NSLog(@"string contains fav!");
                                        [tempTabOrder addObject: fav];
                                    }
                                    else if ([tempItem containsString:@"allservices"]) {
                                        NSLog(@"string contains allservices!");
                                        [tempTabOrder addObject: allservices];
                                    }
                                    else if ([tempItem containsString:@"state"]) {
                                        NSLog(@"string contains state!");
                                        [tempTabOrder addObject: state];
                                    }
                                }
                                NSLog(@" tempTabOrder=%@",tempTabOrder);
                                if ([tempTabOrder count]>0) {
                                    NSUInteger i = [tempTabOrder indexOfObject: state];
                                   
                                    [[self.tabBarController.tabBar.items objectAtIndex:i] setTitle:abbreviation];
                                    
                                }
                                
                                
                            }
                        }
                        
                        
                        singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                        [singleton setStateId:singleton.user_StateId];
                        
                        //-------- Add later----------
                        singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                        singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                        //-------- Add later----------
                        
                        NSString *profileComplete=[generalpd valueForKey:@"pc"];
                        
                        if ([profileComplete length]>0)
                        {
                            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                            // Encrypt
                            [[NSUserDefaults standardUserDefaults] encryptValue:profileComplete withKey:@"PROFILE_COMPELTE_KEY"];
                            // [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        
                        
                        if ([singleton.user_id length]==0) {
                            singleton.user_id=@"";
                        }
                        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        
                        // [defaults setObject:singleton.user_id forKey:@"USER_ID"];
                        
                        // [defaults synchronize];
                        
                        
                        NSString *imageUrl=[generalpd valueForKey:@"pic"];
                        singleton.user_profile_URL=imageUrl;
                        if ([singleton.user_profile_URL length]==0)
                        {
                            singleton.user_profile_URL=@"";
                        }
                        //------------------------- Encrypt Value------------------------
                        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                        // Encrypt
                        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        //------------------------- Encrypt Value------------------------
                        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                        // Encrypt
                        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        //------------------------- Encrypt Value------------------------
                        
                        
                        
                        
                        //NSString *identifier =( [[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        socialpd=(NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        
                        
                        
                        //----- local values  to be save and show from API-----------
                        
                        
                        
                        //NSString * sid=[generalpd valueForKey:@"sid"];
                        
                        str_name=[generalpd valueForKey:@"nam"];
                        str_registerMb =[generalpd valueForKey:@"mno"];
                        str_alternateMb=[generalpd valueForKey:@"amno"];
                        //str_city=[generalpd valueForKey:@"cty"];
                        str_state=[generalpd valueForKey:@"st"];
                        
                        // state_id=[obj getStateCode:str_state];
                        
                        
                        str_district=[generalpd valueForKey:@"dist"];
                        str_dob=[generalpd valueForKey:@"dob"];
                        str_gender=[generalpd valueForKey:@"gndr"];
                        str_emailAddress=[generalpd valueForKey:@"email"];
                        str_emailVerifyStatus=[generalpd valueForKey:@"emails"];
                        str_amnosVerifyStatus=[generalpd valueForKey:@"amnos"];
                        str_Url_pic=[generalpd valueForKey:@"pic"];
                        str_address=[generalpd valueForKey:@"addr"];
                        
                        NSLog(@"str_gender=%@",str_gender);
                        
                        /* if (str_gender.length>0) {
                         _tblUserProfile.tableHeaderView =  [self designHeaderView];
                         }
                         */
                        NSString *quali_id=[generalpd valueForKey:@"qual"];
                        NSString *Occu_id=[generalpd valueForKey:@"occup"];
                        
                        
                        str_qualification=[obj getqualName:quali_id];
                        str_occupation=[obj getOccuptname:Occu_id];
                        
                        NSLog(@"str_Url_pic  =>>> %@",str_Url_pic);
                        // singleton.notiTypeGenderSelected=str_gender;
                        
                        /*
                         "gcmid": "",
                         "opid": "",
                         "refid": "",
                         "tkn": "",
                         */
                        
                        
                        [self setProfileData];
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                }
            }
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];
             */
            //  [self setProfileData];//closed
            
        }
        
    }];
    
}


- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

#pragma mark - Resend Email Methods

-(void)didTapCheckEmailVerifiesAlert
{
    UIAlertView *alert = [[UIAlertView alloc]
                          
                          initWithTitle:NSLocalizedString(@"resend_email_heading", nil)
                          message:NSLocalizedString(@"resend_email_heading_desp", nil)
                          delegate:self
                          cancelButtonTitle:nil
                          otherButtonTitles:NSLocalizedString(@"cancel_caps", nil),NSLocalizedString(@"yes", nil), nil];
    alert.tag = 1000;
    [alert show];
}






-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1000 && buttonIndex == 1)
    { // handle the altdev
        
        [self hitResendEmail];
    }
}





//----- hitAPI for IVR OTP call Type registration ------
-(void)hitResendEmail
{
    
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.profileEmailSelected forKey:@"email"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_RESENDEMAILVERIFY withBody:dictBody andTag:TAG_REQUEST_RESENDEMAILVERIFY completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"success", nil)
                                                                message:rd
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil];
                [alert show];
                
                
            }
            
        }
        else{
            
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}


#pragma mark - ---- Table view data Source -----
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            return [arrGeneralInformation count];
            break;
        case 2 :
            return arrAccountInformation.count;
            break;
        default:
            break;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = singleton.isArabicSelected ? @"CellShowUserProfile_Arabic" : @"CellShowUserProfile";
    
    CellShowUserProfile *profileCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    @try {
        profileCell.widthOfRightDetailsImageConstraint.constant = 0.0;
        profileCell.imageRightDetail.hidden=TRUE;
        profileCell.imageRightDetail.userInteractionEnabled=FALSE;
        
        if (indexPath.section == 1) {
            profileCell.lblPlaceholder.text = [arrGeneralInformation objectAtIndex:indexPath.row];
            profileCell.lblValue.text= [arrDataGeneralInformation objectAtIndex:indexPath.row];
            
            //[profileCell.lblValue sizeToFit];
            profileCell.imagePlaceholder.image = [UIImage imageNamed:[arrImagePlaceholder objectAtIndex:indexPath.row]];
        }
        else if (indexPath.section == 2){
            profileCell.lblPlaceholder.text = [arrAccountInformation objectAtIndex:indexPath.row];
            if (indexPath.row == 0) {
                profileCell.widthOfRightDetailsImageConstraint.constant = 20.0;
                [self checkEmailVerified:profileCell];
            }
            profileCell.lblValue.text= [arrDataAccountInformation objectAtIndex:indexPath.row];
            profileCell.imagePlaceholder.image = [UIImage imageNamed:[arrImageAccountPlaceholder objectAtIndex:indexPath.row]];
        }
        profileCell.lblPlaceholder.textAlignment = NSTextAlignmentLeft;
        profileCell.lblValue.textAlignment = NSTextAlignmentLeft;
        if (singleton.isArabicSelected) {
            profileCell.lblPlaceholder.textAlignment = NSTextAlignmentRight;
            profileCell.lblValue.textAlignment = NSTextAlignmentRight;
           // profileCell.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    profileCell.lblPlaceholder.font = [AppFont regularFont:15];
    profileCell.lblValue.font = [AppFont regularFont:14];
    
    
    
    return profileCell;
}
-(void)checkEmailVerified:(CellShowUserProfile*)profileCell {
    if ([str_emailVerifyStatus isEqualToString:@"0"])
    {
        profileCell.imageRightDetail.hidden=TRUE;
        profileCell.imageRightDetail.userInteractionEnabled=FALSE;
        if ([str_emailAddress length]!=0)
        {
            profileCell.imageRightDetail.hidden=FALSE;
            profileCell.imageRightDetail.userInteractionEnabled=TRUE;
            [profileCell.imageRightDetail setImage:[UIImage imageNamed:@"img_error_mpin"]];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapCheckEmailVerifiesAlert)];
            [profileCell.imageRightDetail addGestureRecognizer:tap];
        }
       
    }
    else  if ([str_emailVerifyStatus isEqualToString:@"1"])
    {
        profileCell.imageRightDetail.userInteractionEnabled=FALSE;
        profileCell.imageRightDetail.hidden=TRUE;

        if ([str_emailAddress length]!=0)
        {
            profileCell.imageRightDetail.hidden=FALSE;
            [profileCell.imageRightDetail setImage:[UIImage imageNamed:@"dialog_check"]];
        }
        
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section != 0)
    {
        NSString *strAddress = [arrGeneralInformation objectAtIndex:indexPath.row];
        if ([strAddress isEqualToString:NSLocalizedString(@"address_caps", nil)])
        {
            @try {
                
                CGFloat retVal = 40.0;
                NSString *txtService =  [arrDataGeneralInformation objectAtIndex:indexPath.row];
                CGRect dynamicHeight = [self rectForText:txtService usingFont:[AppFont regularFont:14] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
                return retVal+dynamicHeight.size.height;
            }  @catch (NSException *exception) {
                
            } @finally {
                
            }
            
        }
        
    }
    if ([AppFont isIPad]) {
        return 65.0;
    }
    return 52.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        self.lblName.textAlignment = NSTextAlignmentLeft;
        self.lblMobile.textAlignment = NSTextAlignmentLeft;
        
        //
       // self.headerViewProfile.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
        self.lblName.text = str_name;
        self.lblMobile.text = str_registerMb;
        [_btnEditUserProfile setTitle:NSLocalizedString(@"edit", nil) forState:UIControlStateNormal];
        CGRect dynamicHeight = [self rectForText:NSLocalizedString(@"edit", nil) usingFont:[AppFont regularFont:18.0] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 26.0)];
        CGRect frameEditBtn = _btnEditUserProfile.frame;
    _btnEditUserProfile.translatesAutoresizingMaskIntoConstraints = YES;
        frameEditBtn.size.width = dynamicHeight.size.width + 50;
        frameEditBtn.origin.x = fDeviceWidth - (frameEditBtn.size.width + 30);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            frameEditBtn.size.width = dynamicHeight.size.width + 60;
            frameEditBtn.size.height = 35.0;
            frameEditBtn.origin.x = fDeviceWidth - (frameEditBtn.size.width + 30);
            [self setEditButtonLayer:17.5];

        }
        if (singleton.isArabicSelected) {
            self.lblName.textAlignment = NSTextAlignmentRight;
            self.lblMobile.textAlignment = NSTextAlignmentRight;
            frameEditBtn.origin.x = 30;
            self.leadingUserProConstraint.constant = self.view.frame.size.width - 120;
        }
        _btnEditUserProfile.frame = frameEditBtn;
        [self.headerViewProfile layoutIfNeeded];
        [self.headerViewProfile updateConstraintsIfNeeded];
        return self.headerViewProfile;
        
    }
    else {
        UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, fDeviceWidth, 30)];
        header.backgroundColor = [UIColor whiteColor];
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, fDeviceWidth - 30, 30)];
        if (section == 1) {
            lbl.text =  [NSLocalizedString(@"general_information", nil) capitalizedString];
        }else {
            lbl.text =  [NSLocalizedString(@"account_information", nil) capitalizedString];
        }
        
        lbl.font = [AppFont regularFont:15.0];
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.textColor = [UIColor colorWithRed:16.0/255.0 green:94.0/255.0 blue:160.0/255.0 alpha:1.0];
        if (singleton.isArabicSelected) {
            lbl.textAlignment = NSTextAlignmentRight;
        }
        [header addSubview:lbl];

        return header;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 210;
    }
    return 30;
}
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

@end

