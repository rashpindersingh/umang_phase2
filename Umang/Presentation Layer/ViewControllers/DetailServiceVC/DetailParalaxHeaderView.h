//
//  DetailParalaxHeaderView.h
//  Umang
//
//  Created by admin on 17/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GSKStretchyHeaderView/GSKStretchyHeaderView.h>
#import "UIView+GSKLayoutHelper.h"
#import "HCSStarRatingView.h"
#import "AMRatingControl.h"
@interface DetailParalaxHeaderView : GSKStretchyHeaderView
@property (weak, nonatomic) IBOutlet UIView *allViews;
@property (weak, nonatomic) IBOutlet UIView *logoSuperView;

@property (weak, nonatomic) IBOutlet UIImageView *backGroundImage;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIButton *serviceName;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImageLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceRating;
@property (weak, nonatomic) IBOutlet UILabel *lblCat_Service;
@property (weak, nonatomic) IBOutlet UILabel *stateName;
@property(retain, nonatomic)UIScrollView *scrollView_nouse;
@property(nonatomic)CGFloat lastStrech;

-(void)updateViewForRightAlignLanguage;
-(void)updateViewServiceDirecoryForRightAlignLanguage;
-(void)addCategoryStateItemsToScrollView:(NSArray*)arrCat state:(NSArray*)arrStates withFont:(CGFloat)fontSize cell:(DetailParalaxHeaderView*)cell;
-(void)addCategoryStateItemsToScrollViewServiceDetails:(NSArray*)arrCat state:(NSArray*)arrStates withFont:(CGFloat)fontSize cell:(DetailParalaxHeaderView*)cell;
@end

