//
//  TransactionalSearchVC.m
//  Umang
//
//  Created by admin on 12/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "TransactionalSearchVC.h"
#import "TransactionCell.h"
#import "MBProgressHUD.h"
#import "UMAPIManager.h"
#import "UIImageView+WebCache.h"
#import "TxnDetailVC.h"
#import "TxnHistoryFilterVC.h"
#import "RunOnMainThread.h"

@interface TransactionalSearchVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    MBProgressHUD *hud;
    SharedManager* singleton;
    NSArray *searchResults;
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblHeaderTransSearch;
    
    __weak IBOutlet UISearchBar *searchBar;
}
@property(nonatomic,retain)NSString *page;

@property (weak, nonatomic) IBOutlet UITableView *tblTransactionSeacrh;

@end

@implementation TransactionalSearchVC
@synthesize arrTransactionSearch;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _page=@"1";
    //  btnFilterSort.hidden = YES;
    
    [arrTransactionSearch removeAllObjects];
    
    NSLog(@"arrTransactionSearch --%@",arrTransactionSearch);
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: TRANSACTIONAL_SEARCH_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    
    [self.view bringSubviewToFront:searchBar];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    lblHeaderTransSearch.text = NSLocalizedString(@"trans_search_screen", "");
    //  searchBar.placeholder = NSLocalizedString(@"search",@"");
    
    
    [btnFilterSort setTitle:NSLocalizedString(@"filter_sort", nil) forState:UIControlStateNormal];
    //aditi
    
    UITextField *searchTextField = [((UITextField *)[searchBar.subviews objectAtIndex:0]).subviews lastObject];
    searchTextField.layer.cornerRadius = 15.0f;
    searchTextField.textAlignment = NSTextAlignmentLeft;
    
    UIImage *image = [UIImage imageNamed:@"search"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    
    searchTextField.leftView = nil;
    
    searchTextField.placeholder = NSLocalizedString(@"search", nil);
    
    searchTextField.rightView = imageView;
    searchTextField.rightViewMode = UITextFieldViewModeAlways;
    
    
    //aditi
    
    //searchBar.text
    [searchBar setValue:NSLocalizedString(@"cancel", @"") forKey:@"_cancelButtonText"];
    lbl_noresult.text =NSLocalizedString(@"no_transaction_found", "");
    singleton=[SharedManager sharedSingleton];
    [self prepareTempData];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    searchBar.placeholder = NSLocalizedString(@"search", @"");
    [self addNavigationView];
    
    // Do any additional setup after loading the view.
}
- (IBAction)btnFilterSortAction:(id)sender
{
    
    [singleton traceEvents:@"FilterSort Button" withAction:@"Clicked" withLabel:@"Transactional Search" andValue:0];

    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    TxnHistoryFilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"TxnHistoryFilterVC"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
    
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    UIButton *cancelButton;
    UIView *topView = searchBar.subviews[0];
    for (UIView *subView in topView.subviews) {
        if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
            cancelButton = (UIButton*)subView;
        }
    }
    if (cancelButton) {
        [cancelButton setTitle:NSLocalizedString(@"cancel", @"") forState:UIControlStateNormal];
    }
    
}
-(void)hitSearchApi:(NSString*)searchText {
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    
    // Set the label text.
    
    
    
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    
    
    
    
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:searchText forKey:@"keywd"];
    [dictBody setObject:_page forKey:@"page"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:@"" forKey:@"lang"];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    
    
    
    //"page": null,
    
    
    
    NSLog(@"Dict body is :%@",dictBody);
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_TRANSACTION_FILTER withBody:dictBody andTag:TAG_REQUEST_TRANSACTION_FILTER completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        
        [hud hideAnimated:YES];
        
        if (error == nil) {
            
            
            NSLog(@"Server Response = %@",response);
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                NSString *pages=[[response valueForKey:@"pd"] valueForKey:@"page"];
                
                NSArray *pageArray = [pages componentsSeparatedByString:@"|"];
                
                NSString*  temppage =[pageArray objectAtIndex:0];
                if ([temppage isEqualToString:@"0"])
                {
                    
                }
                else
                {
                    _page=[pageArray objectAtIndex:1];
                }
                
                NSLog(@"page = %@",_page);
                if (arrTransactionSearch == nil) {
                    arrTransactionSearch=[[NSMutableArray alloc]init];
                }else{
                    [arrTransactionSearch removeAllObjects];
                }
                [arrTransactionSearch addObjectsFromArray:[[response valueForKey:@"pd"] valueForKey:@"fetchUserService"]];
                searchResults = arrTransactionSearch;
                
                [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                    [self prepareTempData];
                }];
                
                
            }
            else{
                
                NSString *serverError = [response objectForKey:@"rd"];
                if ([serverError isKindOfClass:[NSString class]]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                    message:serverError
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                }
                NSLog(@"Error Occured = %@",error.localizedDescription);
                
            }
        }else{
            
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            [arrTransactionSearch removeAllObjects];

            [self prepareTempData];

            //added new -----
            //[self openResultsView];
            
        }
        
    }];
    
    
    
}

-(void)prepareTempData
{
    
   // self.searchDisplayController.searchResultsTableView.hidden=YES;

    NSLog(@"transaction histroy data=%@",arrTransactionSearch);
    NSLog(@"searchResults histroy data=%@",searchResults);

    if ([arrTransactionSearch count]>0) {
        vw_searchResults.hidden=TRUE;
        [self.view bringSubviewToFront:self.tblTransactionSeacrh];
        if (btnFilterSort.hidden) {
            CGRect frameTbl = self.tblTransactionSeacrh.frame;
            frameTbl.size.height = frameTbl.size.height - btnFilterSort.frame.size.height;
            self.tblTransactionSeacrh.frame = frameTbl;
        }
        btnFilterSort.hidden = false;
        self.tblTransactionSeacrh.hidden=FALSE;
        [btnFilterSort setBackgroundColor:[UIColor whiteColor]];
        // [self.view bringSubviewToFront:self.tblTransactionSeacrh];
        [self.view insertSubview:searchBar aboveSubview:self.tblTransactionSeacrh];
        [self.view insertSubview:btnFilterSort aboveSubview:self.tblTransactionSeacrh];
        
    }
    else
    {
        vw_searchResults.hidden=FALSE;
        btnFilterSort.hidden = true;
        //[self.view bringSubviewToFront:vw_searchResults];
        CGRect frameTbl = self.tblTransactionSeacrh.frame;
        frameTbl.size.height = frameTbl.size.height + btnFilterSort.frame.size.height;
        self.tblTransactionSeacrh.frame = frameTbl;
        self.tblTransactionSeacrh.hidden=TRUE;

        
    }
    self.searchDisplayController.searchResultsTableView.hidden=YES;
    img_no_result.hidden = vw_searchResults.hidden;
    [self.tblTransactionSeacrh reloadData];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            // [self.searchController setActive:YES];
            [self.searchDisplayController.searchBar becomeFirstResponder];
        });
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    //vw_searchResults.hidden=TRUE;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    //236
    
    self.view.backgroundColor= [UIColor colorWithRed:236.0/255.0f green:236.0/255.0f blue:236.0/255.0f alpha:1.0f];
    [super viewWillAppear:NO];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btnBack.hidden = true;
   // lblHeaderTransSearch.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btnCancelClicked:btnBack];
    };
    nvView.lblTitle.text = NSLocalizedString(@"trans_search_screen", "");
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tbFrame = searchBar.frame;
        tbFrame.origin.y = kiPhoneXNaviHeight
        searchBar.frame = tbFrame;
        CGRect frame = self.tblTransactionSeacrh.frame;
        frame.origin.y = CGRectGetMaxY(tbFrame);
        frame.size.height = fDeviceHeight - CGRectGetMaxY(tbFrame);
        self.tblTransactionSeacrh.frame = frame;
        [self.view layoutIfNeeded];
    }
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeaderTransSearch.font = [AppFont semiBoldFont:17.0];
    lbl_noresult.font = [AppFont regularFont:14.0];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [singleton traceEvents:@"Search Cancel Button" withAction:@"Clicked" withLabel:@"Transactional Search" andValue:0];

    [arrTransactionSearch removeAllObjects];
    [self prepareTempData];
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [singleton traceEvents:@"Search Button" withAction:@"Clicked" withLabel:@"Transactional Search" andValue:0];

    [self hitSearchApi:searchBar.text];
    
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    
    //    //[self filterContentForSearchText:searchString
    //                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
    //                                      objectAtIndex:[self.searchDisplayController.searchBar
    //                                                     selectedScopeButtonIndex]]];
    
    self.searchDisplayController.searchResultsTableView.hidden=YES;
    return YES;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //--fix lint0---
    // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSMutableDictionary *notifyData;//=[NSMutableDictionary new];
    
    if (self.searchDisplayController.active)
    {
        indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
        // notifyData=[searchResults objectAtIndex:indexPath.row];
        
        notifyData = (NSMutableDictionary *)[searchResults objectAtIndex:indexPath.row];
        
        
    } else {
        indexPath = [self.tblTransactionSeacrh indexPathForSelectedRow];
        //  notifyData=[arrTransactionSearch objectAtIndex:indexPath.row];
        notifyData = (NSMutableDictionary *)[arrTransactionSearch objectAtIndex:indexPath.row];
    }
    
    
    
    
    
    [self selectedIndexNotify:notifyData];
    
    
}



- (void)selectedIndexNotify:(NSDictionary*)celldata
{
    
    
    if (celldata)
    {
        [singleton traceEvents:@"Open Transactional Detail" withAction:@"Clicked" withLabel:@"Transactional Search" andValue:0];

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TxnDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"TxnDetailVC"];
        vc.srid=[celldata valueForKey:@"srid"];
        vc.sdltid=[celldata valueForKey:@"sdltid"];
        vc.dic_txnInfo=celldata;
        //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}





- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    
    if (searchText.length > 0) {
        //  NSPredicate *title = [NSPredicate predicateWithFormat:@"(NOTIF_TITLE contains[c] %@) || (NOTIF_MSG contains[c] %@)", searchText];
        NSPredicate *p1 = [NSPredicate predicateWithFormat:@"servicename contains[c] %@", searchText];
        NSPredicate *p2 = [NSPredicate predicateWithFormat:@"event contains[c] %@", searchText];
        NSPredicate *p3 = [NSPredicate predicateWithFormat:@"dept_resp contains[c] %@", searchText];
        
        
        
        NSPredicate *placesPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[p1, p2, p3]];
        //    NSPredicate *msg = [NSPredicate predicateWithFormat:@"(NOTIF_TITLE contains[c] %@) OR (NOTIF_MSG contains[c] %@)", searchText];
        //    NSPredicate *key = [NSPredicate predicateWithFormat:@"(NOTIF_TITLE contains[c] %@) OR (NOTIF_MSG contains[c] %@)", searchText];
        
        //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@", searchText];
        searchResults = [arrTransactionSearch filteredArrayUsingPredicate:placesPredicate];
    }
    [self.tblTransactionSeacrh reloadData];
    
    
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"servicename contains[c] %@", searchText];
    // searchResults = [arrTransactionSearch filteredArrayUsingPredicate:resultPredicate];
    // [self.tblTransactionSeacrh reloadData];
    
}


- (IBAction)btnCancelClicked:(id)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"Transactional Search" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
    
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001f;
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        //Overloading default no results with multilingual string
        if (searchResults.count == 0) {
            for (UIView *view in tableView.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    ((UILabel *)view).text =NSLocalizedString(@"no_result_found", nil);
                }
            }
        }
        
        return [searchResults count];
        
    } else {
        return [arrTransactionSearch count];
    }
    

}



- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 94;
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    
    
    static NSString *simpleTableIdentifier = @"TransactionCell";
    
    TransactionCell *cell =(TransactionCell *) [self.tblTransactionSeacrh dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[TransactionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    
    
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {

        if([searchResults count]>0)
        {
            cell.headerTransaction.text = [[searchResults objectAtIndex:indexPath.row] valueForKey:@"servicename"];
            cell.descriptnTransaction.text =  [[searchResults objectAtIndex:indexPath.row] valueForKey:@"dept_resp"];
            
            
            cell.lblCategory.text= [[searchResults objectAtIndex:indexPath.row] valueForKey:@"event"];
            cell.lblCategory.font = [UIFont systemFontOfSize:15.0];
            
            NSString *timedate=[[searchResults objectAtIndex:indexPath.row] valueForKey:@"ldate"];
            
            if ([timedate length]>16) {
                timedate = [timedate substringToIndex:16];
                
            }
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd HH:mm"];
            NSDate *d1 = [df dateFromString:timedate];
            NSString *newDate = [df stringFromDate:d1];
            NSDate *date = [df dateFromString: newDate]; // here you can fetch date from string with define format
            df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"dd-MM-yyyy HH:mm"];
            NSString *convertedString = [df stringFromDate:date];
            cell.lblDateNtime.text=convertedString;
            
            cell.lblDateNtime.font = [UIFont systemFontOfSize:13.0];
            
            NSURL *url=[NSURL URLWithString:[[searchResults objectAtIndex:indexPath.row] valueForKey:@"image"]];
            [cell.imgTransaction sd_setImageWithURL:url
                                   placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
        }
        else
        {
            
        }
        
        
        return cell;
        
    }
    
    
    else {
        
        if([arrTransactionSearch count]>0)
        {
            cell.headerTransaction.text = [[arrTransactionSearch objectAtIndex:indexPath.row] valueForKey:@"servicename"];
            cell.descriptnTransaction.text =  [[arrTransactionSearch objectAtIndex:indexPath.row] valueForKey:@"dept_resp"];
            cell.lblCategory.text= [[arrTransactionSearch objectAtIndex:indexPath.row] valueForKey:@"event"];
            cell.lblCategory.font = [UIFont systemFontOfSize:15.0];
            
            
            
            NSString *timedate=[[arrTransactionSearch objectAtIndex:indexPath.row] valueForKey:@"ldate"];
            
            if ([timedate length]>16) {
                timedate = [timedate substringToIndex:16];
                
            }
            
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd HH:mm"];
            NSDate *d1 = [df dateFromString:timedate];
            NSString *newDate = [df stringFromDate:d1];
            NSDate *date = [df dateFromString: newDate]; // here you can fetch date from string with define format
            df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"dd-MM-yyyy HH:mm"];
            NSString *convertedString = [df stringFromDate:date];
            cell.lblDateNtime.text=convertedString;
            // cell.lblDateNtime.text=timedate;
            cell.lblDateNtime.font = [UIFont systemFontOfSize:13.0];
            
            ////cell.lblDateNtime.text=[[arrTransactionSearch objectAtIndex:indexPath.row] valueForKey:@"ldate"];
            
            NSURL *url=[NSURL URLWithString:[[arrTransactionSearch objectAtIndex:indexPath.row] valueForKey:@"image"]];
            [cell.imgTransaction sd_setImageWithURL:url
                                   placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
            
        }
        else
        {
            
        }
        
        return cell;
        
        
    }
    
  
    
    return cell;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 
 
 
 - (void)didReceiveMemoryWarning {
 [super didReceiveMemoryWarning];
 // Dispose of any resources that can be recreated.
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little aaration before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
