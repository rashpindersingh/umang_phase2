//
//  FacebookUtility.m
//  SocialAuthentication
//
// Created by aditi on 06/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.



#import "FacebookUtility.h"
#import <Social/Social.h>

@implementation FacebookUtility
@synthesize delegate;

-(void)doFacebookAuthentication
{
    self.accountStore = [[ACAccountStore alloc] init];
    ACAccountType *fbAccountType= [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];

    NSString *APP_ID = @"277245622672804";
    
    NSDictionary *dictFB = [NSDictionary dictionaryWithObjectsAndKeys:APP_ID,ACFacebookAppIdKey,@[@"email",@"basic_info",@"user_interests",@"user_location"],ACFacebookPermissionsKey, nil];
 
    [self.accountStore requestAccessToAccountsWithType:fbAccountType options:dictFB completion:
     ^(BOOL granted, NSError *error) {
         if (granted) {
             NSArray *accounts = [self.accountStore accountsWithAccountType:fbAccountType];
             //it will always be the last object with single sign on
             self.facebookAccount = [accounts lastObject];
             [self getFBUserInfo];
         } else {
             //Fail gracefully...
             NSLog(@"error getting permission %@",error);
             if ([error code] == 6) {
                 NSLog(@"fb account not configured");
                 [self performSelectorOnMainThread:@selector(showAlertNoFBAccountConfigured) withObject:nil waitUntilDone:NO];
             }
             else {
                 if ([error code] == -1200) {
                     [self performSelectorOnMainThread:@selector(showAlertFBConnectionNotAvailable) withObject:nil waitUntilDone:NO];
                 }
                 else  if ([error code] == 7) {
                     [self performSelectorOnMainThread:@selector(showAlertOperationCouldNotBeCompleted) withObject:nil waitUntilDone:NO];
                 }
                 NSLog(@"User denied permission");
             }
         }
     }];


}


-(void)getFBUserInfo
{
    NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                            requestMethod:SLRequestMethodGET
                                                      URL:requestURL
                                               parameters:nil];
    request.account = self.facebookAccount;
    [request performRequestWithHandler:^(NSData *data,
                                         NSHTTPURLResponse *response,
                                         NSError *error) {
        if(!error) {
            NSMutableDictionary *dictFBUserInfo =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            NSLog(@"User's FB Info: %@",dictFBUserInfo);
            
            if([dictFBUserInfo objectForKey:@"error"] != nil) {
                [self attemptRenewCredentials];
            }
            dispatch_async(dispatch_get_main_queue(),^{
                [self populateUserDetailsFromDict:dictFBUserInfo];
            });
        }
        else {
            //handle error gracefully
            NSLog(@"error from get%@",error);
            [self performSelectorOnMainThread:@selector(showAlertOperationCouldNotBeCompleted) withObject:nil waitUntilDone:NO];
            //attempt to revalidate credentials
        }
    }];
    
}
- (void)populateUserDetailsFromDict:(NSMutableDictionary*)dictData
{
    NSLog(@"First Name = %@",[dictData objectForKey:@"first_name"]);
    NSLog(@"username = %@",[dictData objectForKey:@"username"]);
    NSLog(@"birthday = %@",[dictData objectForKey:@"birthday"]);
    
    NSLog(@"Link = %@",[dictData objectForKey:@"link"]);
    NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", [dictData objectForKey:@"id"]]];
    NSLog(@"Image URL = %@",pictureURL);
    
//    UserProfileBO *objUser = [[UserProfileBO alloc] init];
//    objUser.strFirstName = [dictData objectForKey:@"first_name"];
//    objUser.strEmailID = [dictData objectForKey:@"email"];
//    objUser.strUserID = [dictData objectForKey:@"username"];
//    objUser.strUserName = [dictData objectForKey:@"username"];
//    objUser.strUserName = [dictData objectForKey:@"id"];
//    objUser.strLastName = [dictData objectForKey:@"last_name"];
//    objUser.strDOB = [dictData objectForKey:@"birthday"]?[dictData objectForKey:@"birthday"]:@"";
//    objUser.strUserImgURL = [pictureURL absoluteString];
//    objUser.imgUser  = [UIImage imageWithData:[NSData dataWithContentsOfURL:pictureURL]];
    
    NSMutableDictionary *dictBackground = [NSMutableDictionary dictionary];
    
    NSMutableArray *arrTemp = [NSMutableArray array];
    for(int i = 0 ; i < [[dictData valueForKey:@"languages"] count];i++){
        [arrTemp addObject:[[[dictData valueForKey:@"languages"] objectAtIndex:i]valueForKey:@"name"]];
    }
    
    if([[dictData objectForKey:@"location"] objectForKey:@"name"]){
        [dictBackground setObject:[[dictData objectForKey:@"location"] objectForKey:@"name"] forKey:@"Country"];
    }
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(facebookLoginSuccessful:)]){
        [self.delegate performSelector:@selector(facebookLoginSuccessful:) withObject:dictData];
    }
    
//    objUser.dictUserBackground = dictBackground;
//    dictBackground = nil;
//    [self facebookAuthenticationSuccesfulWithRequiredUserInfo:objUser];
//    objUser = nil;
}

-(void)attemptRenewCredentials {
    [self.accountStore renewCredentialsForAccount:(ACAccount *)self.facebookAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error) {
            switch (renewResult) {
                case ACAccountCredentialRenewResultRenewed:
                    NSLog(@"Good to go");
                    [self getFBUserInfo];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    NSLog(@"User declined permission");
                    break;
                case ACAccountCredentialRenewResultFailed:
                    NSLog(@"non-user-initiated cancel, you may attempt to retry");
                    break;
                default:
                    break;
            }
        }
        else {
            //handle error gracefully
            NSLog(@"error from renew credentials%@",error);
        }
    }];
}

- (void)showAlertNoFBAccountConfigured {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Facebook account has not been configured in your device settings." delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
    [alert show];
}

- (void)showAlertFBConnectionNotAvailable {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Could not connect to facebook. Please try after some time." delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
    [alert show];
}

- (void)showAlertOperationCouldNotBeCompleted {
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Operation could not be completed." delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
    [alert show];
}



@end
