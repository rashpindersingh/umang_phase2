//
//  APIConstant.h
//  UmangApiDemo
//
//  Created by spice on 11/11/16.
//  Copyright (c) 2016 Spice. All rights reserved.
//



#ifndef UmangApiDemo_APIConstant_h
#define UmangApiDemo_APIConstant_h

#define SaltSHA1MAC @"$P!(3UM4NG"
//UMFetchUspImage
#import "NSString+AES256Crypt.h"
#import "NSString+MD5.h"
#define X_REQUEST_UV    @"X-REQUEST-UV"

#import "DeviceInfo.h"
//#define  UM_API_APPCATSRCH     @"/umang/coreapi/ws2/appcatgrysrch"
#define  UM_API_APPCATSRCH @"/umang/coreapi/ws2/applangsrch"
#define  UM_API_FETCH_FEED_SERVICELIST  @"/umang/coreapi/ws2/fthds"

#define  UM_API_TUK     @"/umang/coreapi/ws2/tuk" //umang/coreapi/ws1/tuk

#define  UM_API_TUK_NEW     @"/umang/modsapi/refreshtkn/ws2/tuk"

//https://app.umang.gov.in/umang/coreapi/ws2/applangsrch
#define  UM_API_TRANSACTION_FILTER   @"/umang/coreapi/ws2/fustl"

#define  UM_API_VMPIN           @"/umang/coreapi/ws2/vmpin" //validate mpin

#define  UM_API_UMOBILE         @"/umang/coreapi/ws2/umobile" //update register mobile number

#define  UM_API_UPDATEQUESTION   @"/umang/coreapi/ws2/usecques"
#define  UM_API_FTAL            @"/umang/coreapi/ws2/ftal" //fetchTrending

#define  UM_API_FSTQU           @"/umang/coreapi/ws2/fstqu" //SetUnsetFavourite

#define  UM_API_APPKEYSEARCH    @"/umang/coreapi/ws2/appkeysearch"


#define  UM_API_APPSRSRCH       @"/umang/coreapi/ws2/appsrsrch" //useful link

#define  UM_API_LOGIN                @"/umang/coreapi/ws2/lg"  //Login
#define  UM_API_APPSEARCH            @"/umang/coreapi/ws2/appsearch"   //AppSearch
#define  UM_API_CHANGE_MPIN          @"/umang/coreapi/ws2/changempin" //ChangeMpin
#define  UM_API_REGISTRATION         @"/umang/coreapi/ws2/rgt"  //Registration"
#define  UM_API_DELETE_PROFILE       @"/umang/coreapi/ws2/delete"  // Delete Profile
#define UM_API_GENERATE_AADHAR_OTP   @"/umang/coreapi/ws2/initaadh"   //to generate OTP
#define UM_API_LINK_AADHAAR          @"/umang/coreapi/ws2/linkaadh"

#define UM_API_VALID_AADHAAR         @"/umang/coreapi/ws2/valaadh"
#define  UM_API_FEEDBACK             @"/umang/coreapi/ws2/feedback"  //Feedback
#define  UM_API_FETCH_CITY           @"/umang/coreapi/ws2/fcity"  // Fetch Cities

#define  UM_API_FETCH_DPT_MSG        @"/umang/coreapi/ws2/fdptmsg"  // Fetch Department Message

//#define UM_API_FEEDBACK_SUBMIT       @"/umangApi/ws2/feedback"   //FeedbackSubmit

#define  UM_API_HERO_SPACE           @"/umang/coreapi/ws2/fhs" // Fetch HeroSpace

// NEW API USED FOR UMANG DEVELOPMENT MODE FOR HOME AND SERVICE DIRECTORY
//#define  UM_API_HOME_SCREEN_DATA     @"/umang/coreapi/ws2/fhomescreenv3"  // Fetch Home Screen

#define  UM_API_HOME_SCREEN_DATA     @"/umang/coreapi/ws2/fhomescreenv4"  // Fetch Home Screen

#define  UM_API_GET_SERVICE_DIRECTORY     @"/umang/coreapi/ws2/fdirserv1"
// NEW API USED FOR UMANG DEVELOPMENT MODE FOR HOME AND SERVICE DIRECTORY




//#define  UM_API_HOME_SCREEN_DATA     @"/umang/coreapi/ws2/fhomescreen"

#define  UM_API_VIEW_PROFILE         @"/umang/coreapi/ws2/fp"  // Fetch Profile

#define  UM_API_FETCH_PROFILESTATUS  @"/umang/coreapi/ws2/fps"  // Fetch Profile Status Controller

#define  UM_API_FETCH_USER_RATING    @"/umang/coreapi/ws2/fur"  // Fetch User Ratings

#define  UM_API_FETCH_TRANSACTION_LOGS    @"/umang/coreapi/ws2/fustl"  // Fetch User Service Transaction Logs
#define  UM_API_NEWTOUR_IMAGE           @"/umang/coreapi/ws2/fuiv1" // Fetch USP Images

#define  UM_API_TOUR_IMAGE           @"/umang/coreapi/ws2/fui" // Fetch USP Images

#define  UM_API_FORGOT_MPIN          @"/umang/coreapi/ws2/fmpin"  // Forget Mpin

#define  UM_API_INITAADHAR_OTP       @"/umang/coreapi/ws2/initaadh"  //Init Aadhar Otp
#define  UM_API_INIT                 @"/umang/coreapi/ws2/initv2"   // init Api

#define  UM_API_LOGIN_WITH_OTP       @"/umang/coreapi/ws2/initotp"  // Inti OTP
#define  UM_API_RESEND_OTP           @"/umang/coreapi/ws2/initotp" //Init OTP / resend OTP

#define  UM_API_IVR_OTP              @"/umang/coreapi/ws2/ivrotp"   // IVR OTP

#define  UM_API_LINKAADHAR           @"/umang/coreapi/ws2/linkaadh"  // Link Aadhaar
#define UM_API_UN_LINK_WITH_SOCIAL    @"/umang/coreapi/ws2/uadhr"     //Unlink Aadhar

#define  UM_API_LINK_WITH_SOCIAL     @"/umang/coreapi/ws2/linksa"  // Link Social Account

#define  UM_API_VALIDATE_OTP              @"/umang/coreapi/ws2/valotp"  //ValidateOtp
#define  UM_API_VALIDATE_OTP2             @"/umang/coreapi/ws2/valotpu2"
#define  UM_API_VALIDATE_MPIN             @"/umang/coreapi/ws2/vmpin"  //ValidateMpin
#define  UM_API_VALIDATE_AADHAR_OTP       @"/umang/coreapi/ws2/valaadh" //ValidateAadhaarOtp
#define  UM_API_UPDATE_PROFILE            @"/umang/coreapi/ws2/updatep" //UpdateProfile
#define  UM_API_UPDATE_NOTIFICATION_SETTINGS       @"/umang/coreapi/ws2/uns" //UpdateNotificationSettings
#define  UM_API_UPDATE_MPIN            @"/umang/coreapi/ws2/umpin" //UpdateMpin
#define  UM_API_UPDATE_MOBILE          @"/umang/coreapi/ws2/umobile"  //UpdateMobile
#define  UM_API_UPDATE_GCM_TOKEN       @"/umang/coreapi/ws2/updategcm" //UpdateGcm

#define  UM_API_STATEWISE_SERVICE      @"/umang/coreapi/ws2/stservice"  // StatewiseService

#define  UM_API_UNSET_FAVORITE         @"/umang/coreapi/ws2/setunsetfav" //SetUnsetFavourite
#define  UM_API_SET_MPIN               @"/umang/coreapi/ws2/smpin" // SetMpin

#define UM_API_TRANSACTION_HISTORY      @"/umang/coreapi/ws2/ftld"   //TransactionHistory

#define  UM_API_RESENDEMAILVERIFY      @"/umang/coreapi/ws2/reever"   // ResendEmailVerify

#define  UM_API_RATINGS                @"/umang/coreapi/ws2/ratedpt" //RateDepartment
#define  UM_API_LOGOUT                 @"/umang/coreapi/ws2/logout" //Logout

#define UM_API_LOGOUT_NPS              @"/umang/depttapi/npsApi/ws1/flogout"
#define UM_API_LOGOUT_EPFO             @"/umang/depttapi/epfoApi/ws1/flogout"


#define  UM_API_VALIDATE_SESSION          @"/umang/coreapi/ws2/vsess"  // ValidateSession
#define  UM_API_USERSERVICETANSLOGS       @"/umang/coreapi/ws2/ustl"   //UserServiceTransLogs
#define  UM_API_UPDATEALTERNATEMOBILE     @"/umang/coreapi/ws2/uam"  // UpdateAlternateMobile
#define  UM_API_PULLDOCUMENT              @"/umang/coreapi/ws2/pull"  //PullDocument
#define  UM_API_SMSDELIVERYREPORT         @"/umang/coreapi/ws2/smsdlry" // SmsRealTimeDeliveryReport
#define  UM_API_EMAILVERIFY               @"/umang/coreapi/ws2/ever"   //Email Verify

#define  UM_API_VALIDATEANSWER            @"/umang/coreapi/ws2/vsecques"

#define  UM_API_CHAT                      @"/AgentCallDistribution/customer/chat"
//#define  UM_API_CHAT_HISTORY              @"/AgentCallDistribution/user/history/v2"
#define  UM_API_CHAT_HISTORY              @"/AgentCallDistribution/user/history"
#define  UM_API_CHAT_IMAGE                @"/AgentCallDistribution/file/upload"


#define  UM_API_SETSTATE                  @"/umang/coreapi/ws2/uostate"
#define  UM_API_GETAMBLEM                 @"/umang/coreapi/ws2/fstdata"

// Digilocker API's
#define  UM_API_DIGILOCKER_AADHAR_VERIFICATION       @"/umang/modsapi/digilocker/ws1/sotp"   //Email Verify
#define UM_API_REFRESH_TOKEN                         @"/umang/modsapi/digilocker/ws1/rtkn" //for refresh token
#define  UM_API_VALIDATE_DIGILOCKER_OTP              @"/umang/modsapi/digilocker/ws1/votp" //ValidateAadhaarOtp

#define UM_API_REFRESH_BEARER_DIGI                   @"/umang/modsapi/digilocker/ws1/wstkn"

#define UM_API_REFRESH_DEPT                          @"/umang/modsapi/refreshtkn/ws2/tukdept"
#define UM_API_GETDIGI_TOKEN                         @"/umang/modsapi/digilocker/ws1/gtkn"
#define UM_API_GETDIGI_AUTHURL                         @"/umang/modsapi/digilockerNew/ws1/initauth"


//
#define UM_API_GETISSUED_DOC                         @"/umang/modsapi/digilocker/ws1/gil"

#define UM_API_GETISSUED_DOC_NEW                         @"/umang/modsapi/digilockerNew/ws1/getlistissueddoc"
//
#define UM_API_GETUPLOADED_DOC                       @"/umang/modsapi/digilocker/ws1/gul"

#define UM_API_GETUPLOADED_DOC_NEW                       @"/umang/modsapi/digilockerNew/ws1/getlistuploaddocs"


#define UM_API_UPLOAD_DOC                            @"/umang/modsapi/digilocker/ws1/upf"

#define UM_API_UPLOAD_DOC_NEW                           @"/umang/modsapi/digilockerNew/ws1/uploadfile"

#define UM_API_DOWNLOAD_DOC                          @"/umang/modsapi/digilocker/ws1/doc"

#define UM_API_GETDIGILOCKER_ISSUERLIST                         @"/umang/modsapi/digilockerNew/ws1/getlistofissuers"


#define UM_API_GETDOCUMENTLIST_FROMISSUER                        @"/umang/modsapi/digilockerNew/ws1/getlistdocprovidedissuer"


#define UM_API_PULL_DIGILOCKER__DOCUMENT                       @"/umang/modsapi/digilockerNew/ws1/pulldocument"

#define UM_API_UNLINK_DIGILOCKER                       @"/umang/modsapi/digilockerNew/ws1/unlink"

//

#define UM_API_GET_SEARCH_PARAMETER_DOCUMENT                       @"/umang/modsapi/digilockerNew/ws1/getsearchparamdoc"
//
//
#define UM_API_DOWNLOAD_DOC_NEW                          @"/umang/modsapi/digilockerNew/ws1/doc"

#define UM_API_LOGOUT_DIGI                           @"/umang/modsapi/digilocker/ws1/lgt"

#define UM_API_REFRESH_MODULE_DIGILOCKER             @"/umang/modsapi/refreshtkn/ws2/tukmods"
#define UM_API_INSTANCE_ID             @"/umang/coreapi/opn/ws2/instancekey"
#define UM_API_SERVICES_INFO            @"/umang/coreapi/ws2/fdirdeptser"
#define UM_API_FETCH_SESSIONS            @"/umang/coreapi/ws2/fcurrentsess"
#define UM_API_DELETE_SESSIONS            @"/umang/coreapi/ws2/delsess"


#define  UM_API_INFOSCREEN         @"/umang/coreapi/ws2/finfoscreen" //SetUnsetFavourite
#define  UM_API_SET_MPINU2               @"/umang/coreapi/ws2/smpinu2" // SetMpin

#define  UM_API_NEW_OTP            @"/umang/coreapi/ws2/initotpamem"
#define  UM_API_NEW_VALIDATEOTP    @"/umang/coreapi/ws2/valotpamem"
#define  UM_API_GET_RECOVERY       @"/umang/coreapi/ws2/frecopt"
#define  UM_API_UMPINU2            @"/umang/coreapi/ws2/umpinu2"  // Forget Mpin

//// Nslog 

/*
 URL : https://stagingapp.umang.gov.in
 Bearer : 9f43b495-8c2d-3b18-b4be-a27badfc0cd3
 Client Secret : TkRNUHNIcmJMbFVTSnRLUkpCbEVUY1lYZmo4YTpqQm5ETUJyZmozNUlHbEpVMks1OEw5QTVnZElh
*/

//for IPV6

//#define WSO2PathBASE @"https://stgios.umang.gov.in"
//#define UMAuthorization @"Bearer  9f43b495-8c2d-3b18-b4be-a27badfc0cd3"
//#define PACKAGE_NAME @"in.gov.umang.negd.g2c.staging"
//#define ChatBaseUrl  @"https://stgcht.umang.gov.in"
//#define RefreshTokenBearer @"Bearer f6854d92-da1b-3562-9066-4238aa4f69e7"

// For Staging

//#define WSO2PathBASE @"https://stagingapp.umang.gov.in"
//#define UMAuthorization @"Bearer  9f43b495-8c2d-3b18-b4be-a27badfc0cd3"
//#define PACKAGE_NAME @"in.gov.umang.negd.g2c.staging"
//#define ChatBaseUrl  @"https://stgreporting.umang.gov.in"
//#define RefreshTokenBearer @"Bearer f6854d92-da1b-3562-9066-4238aa4f69e7"
//#define DigiLockerBearer   @"Bearer fcb0bef2-5464-315f-a339-e0bae67dec74"



// For Development
//#define WSO2PathBASE @"https://devapp.umang.gov.in"
//#define UMAuthorization @"Bearer 7a6368ab-2259-3360-8ee0-99019eb3587e"
//#define PACKAGE_NAME @"in.gov.umang.negd.g2c.development"
//#define RefreshTokenBearer @"Bearer b0961437-05ff-33ca-aee9-1a0126b86bd7"
//#define ChatBaseUrl  @"https://devreporting.umang.gov.in"
//#define DigiLockerBearer   @"Bearer 33ed7109-9c12-3c84-a02e-e7b26f47a04d"


// For Production
/*
 #define WSO2PathBASE @"https://app.umang.gov.in"
#define UMAuthorization @"Bearer  fdd7e307-a1ed-3337-82a3-48e65eb33bbe"
#define PACKAGE_NAME @"in.gov.umang.negd.g2c"
#define ChatBaseUrl  @"https://reporting.umang.gov.in"
#define RefreshTokenBearer @"Bearer 49b08c8b-5ebc-31a0-8cab-b1d211ff4a0c"
#define DigiLockerBearer   @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e42c"
*/

//no use
//#define UMAuthorization @"Bearer  415137cd-1b91-36a8-8a96-8a0542c6946b"

//9f43b495-8c2d-3b18-b4be-a27badfc0cd3
#define WSO2_Port @"8580"


#define SaltAES @"@#digitalspice*&"
#define SaltAESDigilocker @"@#localedigitalumang*&"
#define SaltRequestControl @"$f%GY#JX^9H@"
#define SaltRequestVaue @"R%d&Wst676#(Na"
#define SaltRequestVaueDigiLocker @"D@GUM4NG$#4PP"
#define SaltMPIN @"56$f@D8H2x^"

#define KEYCHAIN_ACCOUNT_KEY @"UmangUnique"
#define KEYCHAIN_SaltAES @"KEYCHAIN_SaltAES"
#define KEYCHAIN_SaltAESDigilocker @"KEYCHAIN_SaltAESDigilocker"
#define KEYCHAIN_SaltRequestControl @"KEYCHAIN_SaltRequestControl"
#define KEYCHAIN_SaltRequestDigiLockerControl @"KEYCHAIN_SaltRequestDigiLockerControl"

#define KEYCHAIN_SaltRequestVaue @"KEYCHAIN_SaltRequestVaue"
#define KEYCHAIN_SaltRequestVaueDigiLocker @"KEYCHAIN_SaltRequestVaueDigiLocker"
#define KEYCHAIN_SaltMPIN @"KEYCHAIN_SaltMPIN"
#define KEYCHAIN_SaltCHAT @"KEYCHAIN_SaltCHAT"


#define KEYCHAIN_KEY_JSON @"KEYCHAIN_KEY_JSON"
#define KEYCHAIN_KEY_CHAT @"KEYCHAIN_KEY_CHAT"
#define KEYCHAIN_KEY_DIGILOCKER @"KEYCHAIN_KEY_DIGILOCKER"

#define SaltCHAT @"this"



/*
 For chat
 protected static final String saltChat = "this";
 protected static final byte[] keyValueChat = new byte[]{'u', 'M', 'a', 'N', 'g', 'c', 'H', 'a', 't', '@', 'S', 'p', 'i', 'C', 'e', 'y'};
 */



/*
 ****************** API REQUEST HEADER TAGS
 */
#define API_K_TYPE @"K-TYPE"
#define API_CONTENT_TYPE @"Content-Type"
#define API_REQUEST_CONTROL @"X-REQUEST-CONTROL"
#define API_REQUEST_VALUE @"X-REQUEST-VALUE"
#define API_REQUEST_TIME_STAMP @"X-REQUEST-TSTAMP"
#define API_REQUEST_AUTHORIZATION @"Authorization"
#define TIME_STAMP_DATE_FORMAT @"yyyyMMddHHmmss"


#define API_SUCCESS_CASE @"SU"
#define API_FAILURE_CASE @"FL"
#define API_SUCCESS_CASE1 @"S"

#define APP_VERSION @"13"

#endif

#define Banner_State_Changed   @"BANNERSTATECAHNGED"


/*
 
 
 //url : http://umang.spicesafar.com:8580/fui/1.0.0/fui
 //https://192.168.64.28/umangApi/ws2/fui/1.0.0/fui
 
 //NEW
 //http://192.168.64.28:8580/fui/1.0.0/fui
 //https://192.168.64.28:8543/fui/1.0.0/fui  (In future)
 
 
 //https://drive.google.com/drive/u/0/folders/0B8ju-1TCpEtQaDJQc2t3enQzNk0 API DOC
 
 
 //#define WSO2PathBASE @"http://umang.spicesafar.com"
 
 #define WSO2PathBASE @"https://app.umang.gov.in"
 
 
 #define WSO2_Port @"8580"
 //#define UMAuthorization @"Bearer e7b88f82-eb59-3f53-9335-450fce362e99"
 
 #define UMAuthorization @"Bearer  415137cd-1b91-36a8-8a96-8a0542c6946b"
 
 
 
 #define  UM_API_TOUR_IMAGE                  @"/fui/1.0.0/fui" //Fetch Usp Images
 #define  UM_API_REGISTRATION                @"/rgt/1.0.0/rgt" //Init Registration (create User)
 //old url @"/initreg/1.0.0/rgt"
 #define  UM_API_VALIDATE_OTP                @"/valotp/1.0.0/valotp" //Validate OTP
 #define  UM_API_UPDATE_PROFILE              @"/updatep/1.0.0/updatep" //Update Profile
 #define UM_API_LOGIN_WITH_OTP                 @"/initotp/1.0.0/initotp"   //LoginWithOTP
 
 
 #define UM_API_UPDATE_MPIN                 @"/umpin/1.0.0/umpin"
 #define UM_API_FORGOT_MPIN                   @"/fmpin/1.0.0/fmpin"
 
 #define  UM_API_LOGIN                  @"/umang/core/lg/ws2/lg"//     @"/lg/1.0.0/lg" //Login
 #define  UM_API_DELETE_PROFILE              @"/delete/1.0.0/delete" //Delete Profile
 
 
 #define  UM_API_UNSET_FAVORITE   @"/setunsetfav/1.0.0/setunsetfav" //SetUnset Favourite
 #define  UM_API_LOGOUT        @"/logout/1.0.0/logout" //Logout
 #define  UM_API_VIEW_PROFILE            @"/fp/1.0.0/fp" //view profile
 
 
 #define  UM_API_FETCH_CITY  @"/fcity/1.0.0/fcity" //fetch city
 
 
 #define  UM_API_SET_MPIN                       @"/smpin/1.0.0/smpin" //Set Mpin
 
 #define  UM_API_CHANGE_MPIN                  @"/changempin/1.0.0/changempin" //Change Mpin
 
 #define  UM_API_FORGOT_MPIN                       @"/fmpin/1.0.0/fmpin" //Forgot Mpin
 
 #define  UM_API_UPDATE_MPIN                       @"/umpin/1.0.0/umpin" //Update Mpin
 
 #define  UM_API_VALIDATE_MPIN                       @"/vmpin/1.0.0/vmpin" //Validate Mpin
 //  not found
 
 
 
 #define  UM_API_RESEND_OTP                     @"/initotp/1.0.0/initotp" //Init OTP / resend OTP
 
 #define  UM_API_LINK_WITH_SOCIAL        @"/linksa/1.0.0/linksa" //Link Social
 
 #define  UM_API_UPDATE_MOBILE       @"/umobile/1.0.0/umobile" //Update Mobile
 
 
 #define  UM_API_FEEDBACK      @"/feedback/1.0.0/feedback" //Feedback
 #define  UM_API_UPDATE_NOTIFICATION_SETTINGS           @"/uns/1.0.0/uns" //Update Notifications Settings
 
 #define  UM_API_UPDATE_GCM_TOKEN     @"/updategcm/1.0.0/updategcm" //Update GCM Id
 
 #define  UM_API_RATINGS       @"/ratedpt/1.0.0/ratedpt" //Ratings
 
 
 #define  UM_API_STATEWISE_SERVICE     @"/stservice/1.0.0/stservice" //Statewise Service
 
 
 #define  UM_API_IVR_OTP        @"/ivrotp/1.0.0/ivrotp" //IVR- otp - request
 
 
 #define  UM_API_HOME_SCREEN_DATA   @"/fhomescreen/1.0.0/fhomescreen" //FETCH home Screen
 #define  UM_API_FETCH_USER_RATING   @"/fur/1.0.0/fur" //FETCH USER RATING
 
 #define  UM_API_HERO_SPACE           @"/fhs/1.0.0/fhs" //FETCH HEROSPACE
 
 #define  UM_API_VALIDATE_SESSION         @"/vsess/1.0.0/vsess" //Validate Session
 */
