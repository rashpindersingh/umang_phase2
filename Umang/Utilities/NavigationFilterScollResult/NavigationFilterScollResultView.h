//
//  NavigationFilterScollResultView.h
//  Umang
//
//  Created by admin on 01/05/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationFilterScollResultView : UIView

@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIButton *leftBackButton;

@property (strong, nonatomic) IBOutlet UIButton *rightFilterBarButton;


@property(copy) void(^didTapBackButton)(id btnBack);
@property(copy) void(^didTapRightFilterBarButton)(id btnFilter);

@property (strong, nonatomic) IBOutlet UIScrollView *scroll;


@end
