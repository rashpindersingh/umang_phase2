//
//  NavigationFilterScollResultView.m
//  Umang
//
//  Created by admin on 01/05/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "NavigationFilterScollResultView.h"

@implementation NavigationFilterScollResultView


- (id)init{
    self = [self initWithFrame:[self getRect]];
    
    
    return self;
    
}


-(CGRect)getRect {
 
    return [UIScreen mainScreen].bounds.size.height == 812.0 ? CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 84.0) : CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 64.0);
    
}

- (id)initWithFrame:(CGRect)frame
{
   // SharedManager *singleton = [SharedManager sharedSingleton];
    
    self = [super initWithFrame:frame];
    
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"NavigationFilterScollResultXIB" owner:self options:nil];
    self = (NavigationFilterScollResultView *)[objects objectAtIndex:0];
    self.frame = frame;
    
    
    

    
    
    return self;
}
- (IBAction)didTapRightFilterBarButton:(UIButton *)sender {
    
    if (self.didTapRightFilterBarButton != nil) {
        self.didTapRightFilterBarButton(sender);
    }
}
- (IBAction)didTapBackButton:(UIButton *)sender {
    if (self.didTapBackButton != nil) {
        self.didTapBackButton(sender);
    }
}




/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end


