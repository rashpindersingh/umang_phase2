//
//  AppFont.m
//  Umang
//
//  Created by admin on 24/10/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "AppFont.h"

static CGFloat iPadScale = 1.2;
@implementation AppFont


+ (UIFont *)regularFont:(CGFloat)size {
    
    CGFloat scale =  [AppFont getScaleOfSizeSelected];
    if ([AppFont checkDevice]) {
        return [UIFont systemFontOfSize:(size * iPadScale) + scale];
    }
    return [UIFont systemFontOfSize:size  + scale];
}
+ (UIFont *)boldFont:(CGFloat)size {
    CGFloat scale =  [AppFont getScaleOfSizeSelected];
    if ([AppFont checkDevice]) {
        return [UIFont boldSystemFontOfSize:(size * iPadScale) + scale];
    }
    return [UIFont boldSystemFontOfSize:size + scale];
}
+ (UIFont *)lightFont:(CGFloat)size {
    CGFloat scale =  [AppFont getScaleOfSizeSelected];
    if ([AppFont checkDevice]) {
        return [UIFont systemFontOfSize:(size * iPadScale) + scale weight:UIFontWeightLight];
    }
    return  [UIFont systemFontOfSize:size + scale weight:UIFontWeightLight];
}
+ (UIFont *)heavyFont:(CGFloat)size {
    CGFloat scale =  [AppFont getScaleOfSizeSelected];

    if ([AppFont checkDevice]) {
        return [UIFont systemFontOfSize:(size * iPadScale) + scale weight:UIFontWeightHeavy];
    }
    return  [UIFont systemFontOfSize:size + scale weight:UIFontWeightHeavy];
}
+ (UIFont *)semiBoldFont:(CGFloat)size {
    CGFloat scale =  [AppFont getScaleOfSizeSelected];

    if ([AppFont checkDevice]) {
        return [UIFont systemFontOfSize:(size * iPadScale) + scale weight:UIFontWeightSemibold];
    }
    return  [UIFont systemFontOfSize:size + scale weight:UIFontWeightSemibold];
}
+ (UIFont *)ultraLightFont:(CGFloat)size {
    CGFloat scale =  [AppFont getScaleOfSizeSelected];
    if ([AppFont checkDevice]) {
        return [UIFont systemFontOfSize:(size * iPadScale) + scale weight:UIFontWeightUltraLight];
    }
    return  [UIFont systemFontOfSize:size + scale weight:UIFontWeightUltraLight];
}
+ (UIFont *)mediumFont:(CGFloat)size {
    CGFloat scale =  [AppFont getScaleOfSizeSelected];
    if ([AppFont checkDevice]) {
        return [UIFont systemFontOfSize:(size * iPadScale) + scale weight:UIFontWeightMedium];
    }
    return  [UIFont systemFontOfSize:size + scale weight:UIFontWeightMedium];
}
+(BOOL)checkDevice{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ;
}
+(BOOL)isIPad{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ;
}
+(CGFloat)getScaleOfSizeSelected{
    SharedManager *singleton = [SharedManager sharedSingleton];
    CGFloat scale =  (singleton.fontSizeSelectedIndex - 1 ) * 2;
    return scale;
}
@end
