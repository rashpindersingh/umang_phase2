//
//  UIColor.h
//  Umang
//
//  Created by deepak singh rawat on 08/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CALayer(UIColor)
@property(nonatomic, assign) UIColor* borderUIColor;

@end
