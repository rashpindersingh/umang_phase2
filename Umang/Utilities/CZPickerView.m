//
//  CZPickerView.h
//
//  Created by chenzeyu on 9/6/15.
//  Copyright (c) 2015 chenzeyu. All rights reserved.
//

#import "CZPickerView.h"
#import "LanguageSelectVC.h"

#define CZP_FOOTER_HEIGHT 44.0
#define CZP_HEADER_HEIGHT 44.0
#define CZP_HEADER_HEIGHT_NEW 88.0

#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_7_1
#define CZP_BACKGROUND_ALPHA 0.8
#else
#define CZP_BACKGROUND_ALPHA 0.3
#endif


#define BORDER_COLOR [UIColor colorWithRed:46.0/255.0 green:156.0/255.0 blue:78.0/255.0 alpha:1.0]


typedef void (^CZDismissCompletionCallback)(void);

@interface CZPickerView ()

{
    UIButton *btnRadio;
    UIButton *btnClear;
    LanguageSelectVC *languageSeclectVC;
    SharedManager *sharedObject;
}
@property NSString *headerTitle;
@property NSString *cancelButtonTitle;
@property NSString *confirmButtonTitle;
@property UIView *backgroundDimmingView;
@property UIView *containerView;
@property UIView *headerView;
@property UIView *footerview;
@property UITableView *tableView;
@property NSMutableArray *selectedIndexPaths;
@property CGRect previousBounds;
@end

@implementation CZPickerView

- (id)initWithHeaderTitle:(NSString *)headerTitle
        cancelButtonTitle:(NSString *)cancelButtonTitle
       confirmButtonTitle:(NSString *)confirmButtonTitle{
    self = [super init];
    
    if(self){
        if([self needHandleOrientation])
        {
            [[NSNotificationCenter defaultCenter] addObserver: self
                                                     selector:@selector(deviceOrientationDidChange:)
                                                         name:UIDeviceOrientationDidChangeNotification
                                                       object: nil];
        }
        self.tapBackgroundToDismiss = YES;
        self.needFooterView = YES;
        self.allowMultipleSelection = NO;
        self.animationDuration = 0.5f;
        sharedObject = [SharedManager sharedSingleton];
        
        self.confirmButtonTitle = confirmButtonTitle;
        self.cancelButtonTitle = cancelButtonTitle;
        
        if ([headerTitle isEqualToString:NSLocalizedString(@"profile_state", nil)]) {
            self.headerTitle =NSLocalizedString(@"all", nil);
        }else{
            self.headerTitle = headerTitle ? headerTitle : @"";
        }
        
        
        self.headerTitleColor = [UIColor blackColor];
        
        self.headerBackgroundColor = [UIColor whiteColor];
        
        //        UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, 43,40, 10)];
        //        [_containerView addSubview:vwLine];
        //        vwLine.backgroundColor = [UIColor redColor];
        
        self.cancelButtonNormalColor = [UIColor colorWithRed:59.0/255 green:72/255.0 blue:5.0/255 alpha:1];
        self.cancelButtonHighlightedColor = [UIColor grayColor];
        self.cancelButtonBackgroundColor = [UIColor colorWithRed:236.0/255 green:240/255.0 blue:241.0/255 alpha:1];
        
        self.confirmButtonNormalColor = [UIColor whiteColor];
        self.confirmButtonHighlightedColor = [UIColor colorWithRed:236.0/255 green:240/255.0 blue:241.0/255 alpha:1];
        self.confirmButtonBackgroundColor = BORDER_COLOR;
        
        _previousBounds = [UIScreen mainScreen].bounds;
        self.frame = _previousBounds;
        
        
        _arrPreviousItemSelected = [NSMutableArray new];
    }
    return self;
}

-(void)setArrPreviousItemSelected:(NSMutableArray *)arrPreviousItemSelected
{
    _arrPreviousItemSelected = arrPreviousItemSelected;
    if (self.selectedIndexPaths == nil) {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    else{
        [self.selectedIndexPaths removeAllObjects];
    }
    
    if ([_arrPreviousItemSelected containsObject:NSLocalizedString(@"all", nil)])
    {
        [self allCheckBoxSelected:btnClear];
    }
    else
    {
        [self.selectedIndexPaths removeAllObjects];
        [self.selectedIndexPaths addObjectsFromArray:arrPreviousItemSelected];
    }
    [self reloadData];
}


- (void)setupSubviews
{
    if(!self.backgroundDimmingView)
    {
        self.backgroundDimmingView = [self buildBackgroundDimmingView];
        [self addSubview:self.backgroundDimmingView];
    }
    
    self.containerView = [self buildContainerView];
    [self addSubview:self.containerView];
    
    self.tableView = [self buildTableView];
    [self.containerView addSubview:self.tableView];
    
    self.headerView = [self buildHeaderView];
    [self.containerView addSubview:self.headerView];
    
    self.footerview = [self buildFooterView];
    [self.containerView addSubview:self.footerview];
    
    CGRect frame = self.containerView.frame;
    
    self.containerView.frame = CGRectMake(frame.origin.x,
                                          frame.origin.y,
                                          frame.size.width,
                                          self.headerView.frame.size.height + self.tableView.frame.size.height + self.footerview.frame.size.height);
    self.containerView.center = CGPointMake(self.center.x, self.center.y + self.frame.size.height);
    
}

- (void)performContainerAnimation
{
    self.containerView.center = self.center;
    
    if([self.delegate respondsToSelector:@selector(czpickerViewDidDisplay:)]){
        [self.delegate czpickerViewDidDisplay:self];
        
        
        //    [UIView animateWithDuration:self.animationDuration delay:0 usingSpringWithDamping:0.7f initialSpringVelocity:3.0f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        //
        //    } completion:^(BOOL finished) {
        //               }
        //    }];
    }
}

- (void)show {
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
    self.frame = mainWindow.frame;
    [self showInContainer:mainWindow];
}

- (void)showInContainer:(id)container {
    
    if([self.delegate respondsToSelector:@selector(czpickerViewWillDisplay:)]){
        [self.delegate czpickerViewWillDisplay:self];
    }
    if (self.allowMultipleSelection && !self.needFooterView) {
        self.needFooterView = self.allowMultipleSelection;
    }
    
    if ([container respondsToSelector:@selector(addSubview:)]) {
        [container addSubview:self];
        
        [self setupSubviews];
        [self performContainerAnimation];
        
        [UIView animateWithDuration:self.animationDuration animations:^{
            self.backgroundDimmingView.alpha = CZP_BACKGROUND_ALPHA;
        }];
    }
}

- (void)reloadData{
    [self.tableView reloadData];
}

- (void)dismissPicker:(CZDismissCompletionCallback)completion{
    
    if([self.delegate respondsToSelector:@selector(czpickerViewWillDismiss:)]){
        [self.delegate czpickerViewWillDismiss:self];
    }
    self.containerView.center = CGPointMake(self.center.x, self.center.y + self.frame.size.height);
    
    //    [UIView animateWithDuration:self.animationDuration delay:0 usingSpringWithDamping:0.7f initialSpringVelocity:3.0f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
    //           }completion:^(BOOL finished) {
    //    }];
    
    [UIView animateWithDuration:self.animationDuration animations:^{
        self.backgroundDimmingView.alpha = 0.0;
    } completion:^(BOOL finished) {
        if(finished){
            if([self.delegate respondsToSelector:@selector(czpickerViewDidDismiss:)]){
                [self.delegate czpickerViewDidDismiss:self];
            }
            if(completion){
                completion();
            }
            [self removeFromSuperview];
        }
    }];
}

- (UIView *)buildContainerView
{
    CGFloat widthRatio = _pickerWidth ? _pickerWidth / [UIScreen mainScreen].bounds.size.width : 0.8;
    CGAffineTransform transform = CGAffineTransformMake(widthRatio, 0, 0, 0.8, 0, 0);
    CGRect newRect = CGRectApplyAffineTransform(self.frame, transform);
    UIView *cv = [[UIView alloc] initWithFrame:newRect];
    cv.layer.cornerRadius = 2.0f;
    cv.clipsToBounds = YES;
    cv.center = CGPointMake(self.center.x, self.center.y + self.frame.size.height);
    return cv;
}

- (UITableView *)buildTableView
{
    CGFloat widthRatio = _pickerWidth ? _pickerWidth / [UIScreen mainScreen].bounds.size.width : 0.8;
    CGAffineTransform transform = CGAffineTransformMake(widthRatio, 0, 0, 0.8, 0, 0);
    CGRect newRect = CGRectApplyAffineTransform(self.frame, transform);
    NSInteger n = [self.dataSource numberOfRowsInPickerView:self];
    CGRect tableRect;
    float heightOffset = self.isClearOptionRequired? CZP_HEADER_HEIGHT_NEW : CZP_HEADER_HEIGHT + CZP_FOOTER_HEIGHT;
    if(n > 0){
        float height = n * 44.0;
        height = height > newRect.size.height - heightOffset ? newRect.size.height -heightOffset : height;
        tableRect = CGRectMake(0, self.isClearOptionRequired ?88.0:44.0, newRect.size.width, height);
    } else {
        tableRect = CGRectMake(0, self.isClearOptionRequired ?88.0:44.0, newRect.size.width, newRect.size.height - heightOffset);
    }
    UITableView *tableView = [[UITableView alloc] initWithFrame:tableRect style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return tableView;
}

- (UIView *)buildBackgroundDimmingView
{
    
    UIView *bgView;
    //blur effect for iOS8
    CGFloat frameHeight = self.frame.size.height;
    CGFloat frameWidth = self.frame.size.width;
    CGFloat sideLength = frameHeight > frameWidth ? frameHeight : frameWidth;
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1) {
        bgView = [[UIView alloc] initWithFrame:self.frame];
        bgView.backgroundColor = [UIColor grayColor];
        //        UIBlurEffect *eff = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        //        bgView = [[UIVisualEffectView alloc] initWithEffect:eff];
        bgView.frame = CGRectMake(0, 0, sideLength, sideLength);
    }
    else {
        
    }
    bgView.alpha = 0.0;
    if(self.tapBackgroundToDismiss){
        [bgView addGestureRecognizer:
         [[UITapGestureRecognizer alloc] initWithTarget:self
                                                 action:@selector(cancelButtonPressed:)]];
    }
    return bgView;
}

- (UIView *)buildFooterView{
    if (!self.needFooterView){
        return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    }
    
    CGRect rect = self.tableView.frame;
    CGRect newRect = CGRectMake(0,
                                rect.origin.y + rect.size.height,
                                rect.size.width,
                                CZP_FOOTER_HEIGHT);
    //    CGRect leftRect = CGRectMake(0,0, newRect.size.width /2, CZP_FOOTER_HEIGHT);
    //    CGRect rightRect = CGRectMake(newRect.size.width /2,0, newRect.size.width /2, CZP_FOOTER_HEIGHT);
    
    UIView *view = [[UIView alloc] initWithFrame:newRect];
    
    // Show only apply button
    // if (self.allowRadioButtons)
    {
        CGRect fullRect = CGRectMake(0,0, newRect.size.width, CZP_FOOTER_HEIGHT);
        
        UIButton *confirmButton = [[UIButton alloc] initWithFrame:fullRect];
        // UIButton *confirmButton = [[UIButton alloc] initWithFrame:newRect];
        [confirmButton setTitle:self.confirmButtonTitle forState:UIControlStateNormal];
        [confirmButton setTitleColor:self.confirmButtonNormalColor forState:UIControlStateNormal];
        [confirmButton setTitleColor:self.confirmButtonHighlightedColor forState:UIControlStateHighlighted];
        confirmButton.titleLabel.font = [UIFont systemFontOfSize:16];
        confirmButton.backgroundColor = self.confirmButtonBackgroundColor;
        [confirmButton addTarget:self action:@selector(confirmButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:confirmButton];
        
        return view;
    }
    
    //    UIButton *cancelButton = [[UIButton alloc] initWithFrame:leftRect];
    //    [cancelButton setTitle:self.cancelButtonTitle forState:UIControlStateNormal];
    //    [cancelButton setTitleColor: self.cancelButtonNormalColor forState:UIControlStateNormal];
    //    [cancelButton setTitleColor:self.cancelButtonHighlightedColor forState:UIControlStateHighlighted];
    //    cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    //    cancelButton.backgroundColor = self.cancelButtonBackgroundColor;
    //    [cancelButton addTarget:self action:@selector(cancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    //    [view addSubview:cancelButton];
    //
    //    UIButton *confirmButton = [[UIButton alloc] initWithFrame:rightRect];
    //    [confirmButton setTitle:self.confirmButtonTitle forState:UIControlStateNormal];
    //    [confirmButton setTitleColor:self.confirmButtonNormalColor forState:UIControlStateNormal];
    //    [confirmButton setTitleColor:self.confirmButtonHighlightedColor forState:UIControlStateHighlighted];
    //    confirmButton.titleLabel.font = [UIFont systemFontOfSize:16];
    //    confirmButton.backgroundColor = self.confirmButtonBackgroundColor;
    //    [confirmButton addTarget:self action:@selector(confirmButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    //    [view addSubview:confirmButton];
    //    return view;
    //}
}

-(void)clearButtonClicked{
    [self.selectedIndexPaths removeAllObjects];
    [self.tableView reloadData];
}
- (UIView *)buildHeaderView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.isClearOptionRequired? CZP_HEADER_HEIGHT_NEW : CZP_HEADER_HEIGHT)];
    view.userInteractionEnabled = YES;
    view.backgroundColor = self.headerBackgroundColor;
    
    
    CGFloat yCordinate = 44;
    if (self.isLanguageScreenSelected)
    {
        UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, yCordinate,_tableView.frame.size.width, 0.5)];
        [_containerView addSubview:vwLine];
        vwLine.backgroundColor = [UIColor lightGrayColor];
        
        
        UIFont *headerFont = self.headerTitleFont == nil ? [UIFont systemFontOfSize:18.0] : self.headerTitleFont;
        
        NSDictionary *dict = @{
                               NSForegroundColorAttributeName: self.headerTitleColor,
                               NSFontAttributeName:headerFont
                               };
        NSAttributedString *at = [[NSAttributedString alloc] initWithString:self.headerTitle attributes:dict];
        
        CGRect frame = view.frame;
        frame.size.height = CZP_HEADER_HEIGHT;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 44)];
        label.attributedText = at;
        label.textAlignment = sharedObject.isArabicSelected? NSTextAlignmentRight :NSTextAlignmentLeft;
        [view addSubview:label];
        
        
        
    }
    
    else if (self.isClearOptionRequired)
    {
        UILabel *lblCategories = [[UILabel alloc]initWithFrame:CGRectMake(10, yCordinate, 200, 44)];
        [view addSubview:lblCategories];
        
        lblCategories.font = [UIFont systemFontOfSize:12.0];
        lblCategories.textColor = [UIColor grayColor];
        lblCategories.text = NSLocalizedString(@"cat_label", nil);
        
        btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnClear setTitleColor:[UIColor colorWithRed:0.0/255.0 green:78.0/255.0 blue:152.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        btnClear.frame = CGRectMake(view.frame.size.width - 70, yCordinate, 60, 44);
        // [btnClear setTitle:NSLocalizedString(@"clear", nil) forState:UIControlStateNormal];
        [self allCheckBoxSelected:btnClear];
        [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateSelected];
        [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateNormal];
        
        btnClear.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [btnClear addTarget:self action:@selector(clearButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btnClear];
        btnClear.hidden = YES;
        
        
        
        if (sharedObject.isArabicSelected) {
            lblCategories.frame = CGRectMake(view.frame.size.width - 210, yCordinate, 200, 44);
            lblCategories.textAlignment = NSTextAlignmentRight;
            btnClear.frame = CGRectMake( 10, yCordinate, 60, 44);
            
        }else{
            lblCategories.frame = CGRectMake(10, yCordinate, 200, 44);
            lblCategories.textAlignment = NSTextAlignmentLeft;
            
            btnClear.frame = CGRectMake(view.frame.size.width - 70, yCordinate, 60, 44);
            
        }
        //yCordinate+=44;
    }
    
    
    
    
    else{
        
        UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, yCordinate,_tableView.frame.size.width, 0.5)];
        [_containerView addSubview:vwLine];
        vwLine.backgroundColor = [UIColor lightGrayColor];
        
        
        UIFont *headerFont = self.headerTitleFont == nil ? [UIFont systemFontOfSize:18.0] : self.headerTitleFont;
        
        NSDictionary *dict = @{
                               NSForegroundColorAttributeName: self.headerTitleColor,
                               NSFontAttributeName:headerFont
                               };
        NSAttributedString *at = [[NSAttributedString alloc] initWithString:self.headerTitle attributes:dict];
        
        CGRect frame = view.frame;
        frame.size.height = CZP_HEADER_HEIGHT;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 44)];
        label.attributedText = at;
        [view addSubview:label];
        
        
        
        btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnClear setFrame:CGRectMake(view.frame.size.width - 40, 10, 20, 20)];
        [btnClear setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
        [btnClear setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateSelected];
        [view addSubview:btnClear];
        btnClear.selected = NO;
        [btnClear addTarget:self action:@selector(allCheckBoxSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        if (sharedObject.isArabicSelected) {
            label.frame = CGRectMake(view.frame.size.width - 210, 0, 200, 44);
            label.textAlignment = NSTextAlignmentRight;
            btnClear.frame = CGRectMake( 20, 10, 20, 20);
            
        }else{
            label.frame = CGRectMake(10, 0, 200, 44);
            label.textAlignment = NSTextAlignmentLeft;
            
            btnClear.frame = CGRectMake(view.frame.size.width - 40, 10, 20, 20);
            
        }
        
        if (self.arrPreviousItemSelected != nil && self.arrPreviousItemSelected.count) {
            [self.selectedIndexPaths removeAllObjects];
            
            if ([self.dataSource respondsToSelector:@selector(numberOfRowsInPickerView:)]) {
                NSInteger count = [self.dataSource numberOfRowsInPickerView:self];
                
                if (self.arrPreviousItemSelected.count == count) {
                    [self allCheckBoxSelected:btnClear];
                }
            }
            for (NSInteger i = 0; i<self.arrPreviousItemSelected.count; i++) {
                
                NSNumber *itemRow = [self.arrPreviousItemSelected objectAtIndex:i];
                NSIndexPath *ip = [NSIndexPath indexPathForRow:[itemRow integerValue] inSection: 0];
                [self.selectedIndexPaths addObject:ip];
            }
        }else{
            [self allCheckBoxSelected:btnClear];
        }
        
        //        btnClear = [[UIButton alloc]initWithFrame:CGRectMake(view.frame.size.width - 70, 2, 60,44)];
        //        [btnClear setTitleColor:[UIColor colorWithRed:0.0/255.0 green:78.0/255.0 blue:152.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        //        //  btnClear.frame = CGRectMake(view.frame.size.width - 70, yCordinate, 60, 44);
        //        [btnClear setTitle:@"CLEAR" forState:UIControlStateNormal];
        //        btnClear.titleLabel.textAlignment = NSTextAlignmentRight;
        //        btnClear.titleLabel.font = [UIFont systemFontOfSize:14.0];
        //        [btnClear addTarget:self action:@selector(clearButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        //        btnClear.hidden = YES;
        //        [view addSubview:btnClear];
        
        
        
        
    }
    
    
    
    return view;
}

-(void)allCheckBoxSelected:(UIButton*)btnSender{
    
    if (self.selectedIndexPaths == nil) {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    else{
        [self.selectedIndexPaths removeAllObjects];
    }
    
    btnSender.selected = !btnSender.selected;
    
    
    if (btnSender.selected) {
        
        if ([self.dataSource respondsToSelector:@selector(numberOfRowsInPickerView:)]) {
            NSInteger count = [self.dataSource numberOfRowsInPickerView:self];
            
            for (NSInteger i = 0; i<count; i++) {
                NSIndexPath *ip = [NSIndexPath indexPathForRow:i inSection: 0];
                [self.selectedIndexPaths addObject:ip];
            }
        }
        
        [self.tableView reloadData];
    }
    else{
        [self.selectedIndexPaths removeAllObjects];
        [self.tableView reloadData];
    }
}


- (IBAction)cancelButtonPressed:(id)sender{
    [self dismissPicker:^{
        if([self.delegate respondsToSelector:@selector(czpickerViewDidClickCancelButton:)]){
            [self.delegate czpickerViewDidClickCancelButton:self];
        }
    }];
}

- (IBAction)confirmButtonPressed:(id)sender{
    [self dismissPicker:^{
        if(self.allowMultipleSelection && [self.delegate respondsToSelector:@selector(czpickerView:didConfirmWithItemsAtRows:)]){
            [self.delegate czpickerView:self didConfirmWithItemsAtRows:[self selectedRows]];
        }
        
        else if(!self.allowMultipleSelection && [self.delegate respondsToSelector:@selector(czpickerView:didConfirmWithItemAtRow:)]){
            if (self.selectedIndexPaths.count > 0){
                NSInteger row = ((NSIndexPath *)self.selectedIndexPaths[0]).row;
                [self.delegate czpickerView:self didConfirmWithItemAtRow:row];
            }
        }
    }];
}

- (NSArray *)selectedRows {
    
    
    NSMutableArray *rows = [NSMutableArray new];
    
    // Means all options are selected
    if (btnClear.selected) {
        if ([self.dataSource respondsToSelector:@selector(numberOfRowsInPickerView:)]) {
            NSInteger count = [self.dataSource numberOfRowsInPickerView:self];
            
            for (NSInteger i = 0; i<count; i++) {
                NSIndexPath *ip = [NSIndexPath indexPathForRow:i inSection: 0];
                [rows addObject:@(ip.row)];
            }
        }
        
    }else{
        
        for (NSIndexPath *ip in self.selectedIndexPaths) {
            [rows addObject:@(ip.row)];
        }
    }
    return rows;
}

- (void)setSelectedRows:(NSArray *)rows{
    if (![rows isKindOfClass: NSArray.class]) {
        return;
    }
    self.selectedIndexPaths = [NSMutableArray new];
    for (NSNumber *n in rows){
        NSIndexPath *ip = [NSIndexPath indexPathForRow:[n integerValue] inSection: 0];
        [self.selectedIndexPaths addObject:ip];
    }
    if ([self.dataSource respondsToSelector:@selector(numberOfRowsInPickerView:)]) {
        NSInteger count = [self.dataSource numberOfRowsInPickerView:self];
        if ([self.selectedIndexPaths count] ==count || [self.selectedIndexPaths count] == 0) {
            [self allCheckBoxSelected:btnClear];
        }else{
            btnClear.selected = NO;
        }
    }
    [self reloadData];
}

- (void)unselectAll {
    self.selectedIndexPaths = [NSMutableArray new];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([self.dataSource respondsToSelector:@selector(numberOfRowsInPickerView:)]) {
        return [self.dataSource numberOfRowsInPickerView:self];
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"czpicker_view_identifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell = nil;
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellIdentifier];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    for (UIView *vw in cell.contentView.subviews)
    {
        if ([vw isKindOfClass:[UIButton class]])
        {
            [vw removeFromSuperview];
        }
    }
    
    btnRadio = nil;
    if (self.allowRadioButtons)
    {
        btnRadio = [UIButton buttonWithType:UIButtonTypeCustom];
        btnRadio.tag = 222+indexPath.row;
        [btnRadio setFrame:sharedObject.isArabicSelected? CGRectMake(20, 10, 20, 20) : CGRectMake(tableView.frame.size.width - 40, 10, 20, 20)];
        [btnRadio setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
        [btnRadio setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateSelected];
        [btnRadio addTarget:self action:@selector(btnCheckBoxClickedFromCell:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnRadio];
        btnRadio.selected = NO;
    }
    
    for(NSIndexPath *ip in self.selectedIndexPaths)
    {
        if(ip.row == indexPath.row){
            // Add Radio Buttons
            if (self.allowRadioButtons) {
                btnRadio.selected = YES;
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
    }
    if([self.dataSource respondsToSelector:@selector(czpickerView:titleForRow:)] && [self.dataSource respondsToSelector:@selector(czpickerView:imageForRow:)])
    {
        cell.textLabel.text = [self.dataSource czpickerView:self titleForRow:indexPath.row];
        cell.imageView.image = [self.dataSource czpickerView:self imageForRow:indexPath.row];
    }
    else if ([self.dataSource respondsToSelector:@selector(czpickerView:attributedTitleForRow:)] && [self.dataSource respondsToSelector:@selector(czpickerView:imageForRow:)])
    {
        cell.textLabel.attributedText = [self.dataSource czpickerView:self attributedTitleForRow:indexPath.row];
        cell.imageView.image = [self.dataSource czpickerView:self imageForRow:indexPath.row];
    }
    else if ([self.dataSource respondsToSelector:@selector(czpickerView:attributedTitleForRow:)])
    {
        cell.textLabel.attributedText = [self.dataSource czpickerView:self attributedTitleForRow:indexPath.row];
    }
    else if([self.dataSource respondsToSelector:@selector(czpickerView:titleForRow:)])
    {
        cell.textLabel.text = [self.dataSource czpickerView:self titleForRow:indexPath.row];
    }
    
    
    cell.textLabel.textAlignment =  sharedObject.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    if(self.checkmarkColor){
        cell.tintColor = self.checkmarkColor;
    }
    
    if ([[UIScreen mainScreen]bounds].size.height <= 568) {
        cell.textLabel.font = [UIFont systemFontOfSize:12.0];
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        
    }
    else
    {
        cell.textLabel.font = [UIFont systemFontOfSize:14.5];
        cell.textLabel.adjustsFontSizeToFitWidth = NO;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(!self.selectedIndexPaths){
        self.selectedIndexPaths = [NSMutableArray new];
    }
    // the row has already been selected
    
    if (self.allowMultipleSelection)
    {
        btnClear.hidden = NO;
        
        if([self.selectedIndexPaths containsObject:indexPath])
        {
            [self.selectedIndexPaths removeObject:indexPath];
            btnClear.selected = NO;
            cell.accessoryType = UITableViewCellAccessoryNone;
            [tableView reloadData];
            
        } else {
            [self.selectedIndexPaths addObject:indexPath];
            btnRadio.selected = YES;
            if ([self.dataSource respondsToSelector:@selector(numberOfRowsInPickerView:)]) {
                NSInteger count = [self.dataSource numberOfRowsInPickerView:self];
                if ([self.selectedIndexPaths count] == count) {
                    btnClear.selected = YES;
                }
                else{
                    btnClear.selected = NO;
                }
            }
            [tableView reloadData];
            // cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        
    } else { //single selection mode
        
        if (self.allowRadioButtons) {
            if (self.selectedIndexPaths == nil) {
                self.selectedIndexPaths = [NSMutableArray new];
            }
            
            [self.selectedIndexPaths removeAllObjects];
            if (![self.selectedIndexPaths containsObject:indexPath]) {
                [self.selectedIndexPaths addObject:indexPath];
            }
            [tableView reloadData];
            return;
        }
        
        if (self.selectedIndexPaths.count > 0){// has selection
            
            btnClear.hidden = NO;
            NSIndexPath *prevIp = (NSIndexPath *)self.selectedIndexPaths[0];
            UITableViewCell *prevCell = [tableView cellForRowAtIndexPath:prevIp];
            if(indexPath.row != prevIp.row){ //different cell
                prevCell.accessoryType = UITableViewCellAccessoryNone;
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                [self.selectedIndexPaths removeObject:prevIp];
                [self.selectedIndexPaths addObject:indexPath];
            } else {//same cell
                cell.accessoryType = UITableViewCellAccessoryNone;
                self.selectedIndexPaths = [NSMutableArray new];
            }
        } else {//no selection
            [self.selectedIndexPaths addObject:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        
        if(!self.needFooterView && [self.delegate respondsToSelector:@selector(czpickerView:didConfirmWithItemAtRow:)])
        {
            [self dismissPicker:^{
                [self.delegate czpickerView:self didConfirmWithItemAtRow:indexPath.row];
            }];
        }
        else
        {
            
            [self dismissPicker:^{
                [self.delegate czpickerView:self didConfirmWithItemAtRow:indexPath.row];
            }];
            
            
        }
    }
    
}

#pragma mark - Notification Handler

-(void)btnCheckBoxClickedFromCell:(UIButton*)btnSender{
    
    
    NSInteger row = btnSender.tag - 222;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    if([self.selectedIndexPaths containsObject:indexPath])
    {
        [self.selectedIndexPaths removeObject:indexPath];
        btnClear.selected = NO;
        [self.tableView reloadData];
        
    } else {
        [self.selectedIndexPaths addObject:indexPath];
        if ([self.dataSource respondsToSelector:@selector(numberOfRowsInPickerView:)]) {
            NSInteger count = [self.dataSource numberOfRowsInPickerView:self];
            if ([self.selectedIndexPaths count] == count) {
                btnClear.selected = YES;
            }
            else{
                btnClear.selected = NO;
            }
        }
        [self.tableView reloadData];
        // cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}


- (BOOL)needHandleOrientation{
    NSArray *supportedOrientations = [[[NSBundle mainBundle] infoDictionary]
                                      objectForKey:@"UISupportedInterfaceOrientations"];
    NSMutableSet *set = [NSMutableSet set];
    for(NSString *o in supportedOrientations){
        NSRange range = [o rangeOfString:@"Portrait"];
        if (range.location != NSNotFound) {
            [set addObject:@"Portrait"];
        }
        
        range = [o rangeOfString:@"Landscape"];
        if (range.location != NSNotFound) {
            [set addObject:@"Landscape"];
        }
    }
    return set.count == 2;
}

- (void)deviceOrientationDidChange:(NSNotification *)notification
{
    CGRect rect = [UIScreen mainScreen].bounds;
    if (CGRectEqualToRect(rect, _previousBounds))
    {
        return;
    }
    _previousBounds = rect;
    self.frame = rect;
    for(UIView *v in self.subviews){
        if([v isEqual:self.backgroundDimmingView]) continue;
        
        [UIView animateWithDuration:0.2f animations:
         ^{
             v.alpha = 0.0;
         } completion:^(BOOL finished) {
             [v removeFromSuperview];
             //as backgroundDimmingView will not be removed
             if(self.subviews.count == 1)
             {
                 [self setupSubviews];
                 [self performContainerAnimation];
             }
         }];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
