//
//  DeviceInfo.h
//  UmangApiDemo
//
//  Created by deepak singh rawat on 13/11/16.
//  Copyright © 2016 Spice. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceInfo : NSObject
+ (NSString *)deviceType;

@end
