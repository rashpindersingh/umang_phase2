//
//  RatingGraphView.m
//  RatingGraph
//
//  Created by Kuldeep Saini on 12/29/16.
//  Copyright © 2016 Kuldeep. All rights reserved.
//

#import "RatingGraphView.h"
#import "HCSStarRatingView.h"

#define DEFAULT_BLUE_NEW [UIColor colorWithRed:15.0/255.0 green:69.0/255.0 blue:139.0/255.0 alpha:1.0]

@interface RatingGraphView()<SimpleBarChartDelegate,SimpleBarChartDataSource>

@end

@implementation RatingGraphView

-(UIView*)designRatingViewWithFrame:(CGRect)frame withRating:(NSString*)rating withdownload:(NSString*)downloads
{
    
    // Design Rating View
    
    // Design Rating View
    
    
    
    
    
    
    UIView *vwRating = [[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)];
    vwRating.backgroundColor = [UIColor clearColor];
    
    CGFloat yPos = 0;
    
    UILabel *lblRatingPoints = [[UILabel alloc] initWithFrame:CGRectMake(0, yPos, 60, frame.size.height)];
    lblRatingPoints.text = rating;
    lblRatingPoints.textAlignment = NSTextAlignmentCenter;
    lblRatingPoints.font = [UIFont systemFontOfSize:38];
    lblRatingPoints.textColor=[UIColor grayColor];
    
    [vwRating addSubview:lblRatingPoints];
    
    //UIImage *dot, *star;
    //dot = [UIImage imageNamed:@"star_border"];
    //star = [UIImage imageNamed:@"star_filled"];
    
    // CGFloat starHeight = frame.size.height/2 - 10;
    
    HCSStarRatingView *starRating = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(60,yPos+8, frame.size.width - 30, 20)];
    starRating.spacing = 8.0;
    starRating.maximumValue = 5.0;
    starRating.minimumValue = 0.0;
    [starRating setTintColor:[UIColor grayColor]];
    starRating.emptyStarColor = [UIColor clearColor];
    
    // starRating.backgroundColor=[UIColor greenColor];
    
    float fCost = [rating floatValue];
    starRating.userInteractionEnabled=NO;
    [starRating setValue:fCost];
    
    // vwRating.backgroundColor=[UIColor redColor];
    
    [vwRating addSubview:starRating];
    
    //yPos+=15;
    UILabel *lblUserCount = [[UILabel alloc] initWithFrame:CGRectMake(60, starRating.frame.origin.y+starRating.frame.size.height,100, 12)];
    lblUserCount.text = downloads;
    lblUserCount.textAlignment = NSTextAlignmentCenter;
    lblUserCount.font = [UIFont systemFontOfSize:12.0];
    
    [lblUserCount sizeToFit];
    [vwRating addSubview:lblUserCount];
    
    
    UIImageView *imgUser = [[UIImageView alloc] initWithFrame:CGRectMake(62+lblUserCount.frame.size.width, starRating.frame.origin.y+starRating.frame.size.height, 12, 12)];
    imgUser.image = [UIImage imageNamed:@"user_picRating.png"];
    imgUser.contentMode = UIViewContentModeScaleAspectFit;
    
    [vwRating addSubview:imgUser];
    
    
    return vwRating;
}



- (instancetype)initWithFrame:(CGRect)frame withXValues:(NSArray*)xValues  withRating:(NSString *)rating withTotalDownload:(NSString*)withTotalDownload
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        _values = xValues;
        // Draw Star Images:
        
        CGFloat xPosition = CGRectGetWidth(frame)/2 - 150;
        CGFloat xCord = xPosition;
        CGFloat width = 15;
        CGFloat padding = 5;
        
        // Design Rating View
        UIView *removeView;
        while((removeView = [self viewWithTag:1001]) != nil) {
            [removeView removeFromSuperview];
        }
        
        
        UIView *vwRating = [self designRatingViewWithFrame:CGRectMake(frame.size.width/2-60,5,120, 50) withRating:rating withdownload:withTotalDownload];
        
        vwRating.tag=1001;
        
        [self addSubview:vwRating];
        
        CGFloat yCord = 50;
        
        for (int lineCounter = 0; lineCounter < 5; lineCounter ++)
        {
            for (int starCounter = 0; starCounter<lineCounter; starCounter++)
            {
                xCord+= width + padding ;
            }
            
            for (int starCounter = lineCounter; starCounter<5; starCounter++) {
                UIImageView *imgStars = [[UIImageView alloc] initWithFrame:CGRectMake(xCord, yCord, width, width)];
                imgStars.backgroundColor = [UIColor clearColor];
                imgStars.image = [UIImage imageNamed:@"icon_star"];
                [self addSubview:imgStars];
                xCord+= width + padding ;
                
                
                
            }
            if (lineCounter == 4)
            {
                xCord = xPosition;
                yCord+=width;
            }
            else
            {
                xCord = xPosition;
                yCord+=width+padding;
            }
        }
        
        //xCord = CGRectGetWidth(self.frame) - 160;
        //xCord
        CGRect chartFrame				= CGRectMake(fDeviceWidth/2,-12,110.0,220.0);
        _chart = [[SimpleBarChart alloc] initWithFrame:chartFrame];
        _chart.backgroundColor = [UIColor clearColor];
        
        _chart.delegate					= self;
        _chart.dataSource				= self;
        _chart.barWidth					= 10.0;
        _chart.xLabelType				= SimpleBarChartXLabelTypeAngled;
        _chart.incrementValue			= 10;
        _chart.barTextType				= SimpleBarChartBarTextTypeRoof;
        _chart.barTextColor				= [UIColor blackColor];
        _chart.transform = CGAffineTransformMakeRotation(M_PI_2);
        [self addSubview:_chart];
        
        
        
        [_chart reloadData];
        
    }
    return self;
}

#pragma mark SimpleBarChartDataSource

- (NSUInteger)numberOfBarsInBarChart:(SimpleBarChart *)barChart
{
    return _values.count;
}

- (CGFloat)barChart:(SimpleBarChart *)barChart valueForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] floatValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart textForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] stringValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart xLabelForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] stringValue];
}

- (UIColor *)barChart:(SimpleBarChart *)barChart colorForBarAtIndex:(NSUInteger)index
{
    if ([[_values objectAtIndex:index]floatValue] == 0 ) {
        return  [UIColor whiteColor];
    }
    else
    {
        return DEFAULT_BLUE_NEW;
    }
}


@end
