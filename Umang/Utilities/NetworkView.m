//
//  NetworkView.m
//  NetworkTab
//
//  Created by deepak singh rawat on 26/11/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import "NetworkView.h"

@implementation NetworkView

@synthesize lbl_network,lbl_networkMsg;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
              self.userInteractionEnabled = YES;
       
    }
    return self;
}


-(IBAction)closeNwView:(id)sender
{
    NSLog(@"remove");
    
    [self removeFromSuperview];
}

@end
