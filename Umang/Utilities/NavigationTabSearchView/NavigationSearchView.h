//
//  NavigationSearchView.h
//  Umang
//
//  Created by admin on 30/04/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationSearchView : UIView
@property (strong, nonatomic) IBOutlet UIButton *filterBarButton;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIImageView *iconsearchImage;

@property (strong, nonatomic) IBOutlet UIButton *notificationBarButton;
@property (strong, nonatomic) IBOutlet UITextField *txtSearchView;

@property (strong, nonatomic) IBOutlet UIView *vw_bgSearch;
@property (strong, nonatomic) IBOutlet UIView *vw_bottomLine;



@property(copy) void(^didTapFilterBarButton)(id btnfilter);
@property(copy) void(^didTapNotificationBarButton)(id btnNotification);
@property(copy) void(^didTapSearchTextfield)(id txtSearchView);

@end

