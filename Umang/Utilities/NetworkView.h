//
//  NetworkView.h
//  NetworkTab
//
//  Created by deepak singh rawat on 26/11/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetworkView : UIView
-(IBAction)closeNwView:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_network;

@property (weak, nonatomic) IBOutlet UILabel *lbl_networkMsg;

@end
