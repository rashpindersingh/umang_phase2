//
//  NavigationView.m
//  Umang
//
//  Created by Rashpinder on 30/04/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "NavigationView.h"

@implementation NavigationView

- (id)initRightBarButton{
    self = [self initWithFrame:[self getRect]];
    self.rightBarButton.hidden = false;
    self.viewMoreRightItems.hidden = true;
    return self;
}
- (id)initSecondBarItemsView{
    self = [self initWithFrame:[self getRect]];
    self.rightBarButton.hidden = true;
    self.leftBackButton.hidden = true;
    self.lblTitle.hidden = true;
    self.viewMoreRightItems.hidden = false;
    return self;
}
- (id)init{
    self = [self initWithFrame:[self getRect]];
    self.rightBarButton.hidden = true;
    self.widthRightBarConstraint.constant = 80.0;
    self.viewMoreRightItems.hidden = true;
    return self;

}
-(CGRect)getRect {
    return [UIScreen mainScreen].bounds.size.height == 812.0 ? CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 84.0) : CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 64.0);
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"NavigationViewNib" owner:self options:nil];
    self = (NavigationView *)[objects objectAtIndex:0];
    self.frame = frame;
    self.rightBarButton.hidden = true;
    [self.rightBarButton.titleLabel setFont:[AppFont regularFont:17.0]];
    [self.leftBackButton.titleLabel setFont:[AppFont regularFont:17.0]];
    [self.leftBackButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [self.secondBackButton.titleLabel setFont:[AppFont regularFont:17.0]];
    [self.secondBackButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    self.lblTitle.font = [AppFont semiBoldFont:17];
    self.lblTitle.text = @"";
    self.lblTitle.textColor = [UIColor blackColor];
    
    self.secondLblTitle.font = [AppFont semiBoldFont:17];
    self.secondLblTitle.text = @"";
    self.secondLblTitle.textColor = [UIColor blackColor];
   
    return self;
}
- (IBAction)didTapRightBarButtonAction:(UIButton *)sender {
    
    if (self.didTapRightBarButton != nil) {
        self.didTapRightBarButton(sender);
    }
}
- (IBAction)didTapSecondRightBarButtonAction:(UIButton *)sender {
    
    if (self.didTapSecondRightBarButton != nil) {
        self.didTapSecondRightBarButton(sender);
    }
}
- (IBAction)didTapBackButtonAction:(UIButton *)sender {
    if (self.didTapBackButton != nil) {
        self.didTapBackButton(sender);
    }
}

@end
