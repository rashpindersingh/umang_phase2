//
//  ServiceNotAvailableView.m
//  Umang
//
//  Created by admin on 27/09/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "FilterNoResultView.h"
#import "UIImageView+WebCache.h"
@implementation FilterNoResultView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"FilterNoResultView" owner:self options:nil];
    self = (FilterNoResultView *)[objects objectAtIndex:0];
    self.frame = frame;
    self.btnEditFilter.titleLabel.numberOfLines = 2.0;
    self.btnEditFilter.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setFilterBtnTitle {
    self.lblHeading.text = NSLocalizedString(@"no_result_found", nil);
    self.lblSubHeading.text = NSLocalizedString(@"retry_filter_desc", nil);
     NSString *msg = NSLocalizedString(@"edit_filters", nil);
    [self.btnEditFilter setTitle:msg forState:UIControlStateNormal];
    self.btnEditFilter.titleLabel.font = [AppFont mediumFont:15.0f];
    [self.btnEditFilter sizeToFit];
    CGRect frameBtn = self.btnEditFilter.frame;
    frameBtn.size.width = frameBtn.size.width + 10;
    frameBtn.size.height = frameBtn.size.height + 10;
    frameBtn.origin.x = self.center.x - frameBtn.size.width /2.0;
    self.btnEditFilter.translatesAutoresizingMaskIntoConstraints = true;
    self.btnEditFilter.frame = frameBtn;
    //self.btnEditFilter.center.x = ;
    [self layoutIfNeeded];
    [self updateConstraintsIfNeeded];
}

@end
