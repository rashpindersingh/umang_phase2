//
//  RatingGraphView.h
//  RatingGraph
//
//  Created by Kuldeep Saini on 12/29/16.
//  Copyright © 2016 Kuldeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleBarChart.h"

@interface RatingGraphView : UIView
{
  SimpleBarChart *_chart;
  NSArray *_values;
}

- (instancetype)initWithFrame:(CGRect)frame withXValues:(NSArray*)xValues  withRating:(NSString *)rating withTotalDownload:(NSString*)withTotalDownload;

@end
