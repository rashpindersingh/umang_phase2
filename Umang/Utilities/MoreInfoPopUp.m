//
//  MoreInfoPopUp.m
//  Umang
//
//  Created by spice on 05/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "MoreInfoPopUp.h"

@implementation MoreInfoPopUp

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        //UIView *contentView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 100)];
        UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0,0, 320, 480)];

        UIImageView *icon_info=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
        icon_info.image=[UIImage imageNamed:@"icon_information.png"];
        [contentView addSubview:icon_info];
        
        UILabel *lbl_info=[[UILabel alloc]initWithFrame:CGRectMake(50, 10, 100, 30)];
        lbl_info.text=@"Information";
        lbl_info.font=[UIFont boldSystemFontOfSize:16];
        lbl_info.textAlignment=NSTextAlignmentLeft;
        [contentView addSubview:lbl_info];
        
        
        UIImageView *icon_hideinfo=[[UIImageView alloc]initWithFrame:CGRectMake(10, 50, 30, 30)];
        icon_hideinfo.image=[UIImage imageNamed:@"icon_hideInfo.png"];
        [contentView addSubview:icon_hideinfo];
        
        UILabel *lbl_Hideinfo=[[UILabel alloc]initWithFrame:CGRectMake(50, 50, 100, 30)];
        lbl_Hideinfo.text=@"Hide Service";
        lbl_Hideinfo.font=[UIFont boldSystemFontOfSize:16];
        lbl_Hideinfo.textAlignment=NSTextAlignmentLeft;
        [contentView addSubview:lbl_Hideinfo];
        
        
        
    }
    return self;
}

 /*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
