//
//  NSString+AES256Crypt.m
//  AES256EnDeCrypt
//
//  Created by deepak singh rawat on 09/11/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import "NSString+AES256Crypt.h"
#import "NSData+AES256Crypt.h"

#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonHMAC.h>

@implementation NSString (AES256Crypt)

//------- Method for passing encrytpion data with salt key pass and embeded our own results----
-(NSString*)EncryptData:(NSString*)value saltkey:(NSString*)salt
{
    int ITERATIONS=2;
    NSString * eValue = value;
    NSString * valueToEnc;
    //    "kreq": "@#umANG#$%\u0026etKey",--Key JSON
    
    unsigned char keyPtr[kCCKeySizeAES128] = {'@', '#', 'u', 'm', 'A', 'N', 'G', '#', '$', '%', '&', 'e', 't', 'K', 'e', 'y'};
    NSData *theDataKey = [NSData dataWithBytes:keyPtr length:sizeof(keyPtr)];
    NSString* encryptedKey = [[NSString alloc] initWithData:theDataKey encoding:NSUTF8StringEncoding];
    for (int i = 0; i < ITERATIONS; i++) {
        valueToEnc=[NSString stringWithFormat:@"%@%@",salt,eValue];
        eValue = [valueToEnc AES128EncryptWithKey:encryptedKey];
        //NSLog(@"%@",eValue);
    }
    
    
    return eValue;
}
#pragma mark:- Instance ID Encrption
-(NSString*)EncryptData:(NSString*)value saltkey:(NSString*)salt withKey:(NSString*)Key
{
    int ITERATIONS=2;
    NSString * eValue = value;
    NSString * valueToEnc;
    //    unsigned char keyPtr[kCCKeySizeAES128] = {'@', '#', 'u', 'm', 'A', 'N', 'G', '#', '$', '%', '&', 'e', 't', 'K', 'e', 'y'};
    //    NSData *theDataKey = [NSData dataWithBytes:keyPtr length:sizeof(keyPtr)];
    //    NSString* encryptedKey = [[NSString alloc] initWithData:theDataKey encoding:NSUTF8StringEncoding];
    for (int i = 0; i < ITERATIONS; i++) {
        valueToEnc=[NSString stringWithFormat:@"%@%@",salt,eValue];
        eValue = [valueToEnc AES128EncryptWithKey:Key];
        //NSLog(@"%@",eValue);
    }
    
    
    return eValue;
}
#pragma mark:- Instance ID DecryptData

-(NSString*)DecryptData:(NSString*)value saltkey:(NSString*)salt withKey:(NSString*)Key

{
    NSString *decordedValue ;
    NSString *dValue ;
    NSString *valueToDecrypt ;
    valueToDecrypt=value;
    //    unsigned char keyPtr[kCCKeySizeAES128] = {'@', '#', 'u', 'm', 'A', 'N', 'G', '#', '$', '%', '&', 'e', 't', 'K', 'e', 'y'};
    //    NSData *theDataKey = [NSData dataWithBytes:keyPtr length:sizeof(keyPtr)];
    //    NSString* encryptedKey = [[NSString alloc] initWithData:theDataKey encoding:NSUTF8StringEncoding];
    int ITERATIONS=2;
    for (int i = 0; i < ITERATIONS; i++) {
        decordedValue= [valueToDecrypt AES128DecryptWithKey:Key];
        @try {
            if ([decordedValue length]>0)
            {
                
                
             //   NSLog(@"1befor decode decordedValue=%@",decordedValue);
                dValue = [decordedValue  substringToIndex:salt.length];
                decordedValue = [decordedValue stringByReplacingOccurrencesOfString:dValue withString:@""];
              //  NSLog(@"after decode decordedValue=%@",decordedValue);
                valueToDecrypt=decordedValue;
            }
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
    }
    return valueToDecrypt;
    
}
//------ Code to encrypt data using our keys and default AES crypto-------------
-(NSString *)AES128EncryptWithKey:(NSString *)key
{
    NSData *plainData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptedData = [plainData AES128EncryptWithKey:key];
    NSString *encryptedString = [encryptedData base64Encoding];
    return encryptedString;
}





//-----------------------------------------------
//----- new encrption added for chat Start
//-----------------------------------------------

-(NSString*)EncryptChatData:(NSString*)value saltkey:(NSString*)salt
{
    int ITERATIONS=2;
    NSString * eValue = value;
    //    "kchat": "uMaNgcHat@SpiCey",-- Key Chat ---
    
    NSString * valueToEnc;
    unsigned char keyPtr[kCCKeySizeAES128] = {'u', 'M', 'a', 'N', 'g', 'c', 'H', 'a', 't', '@', 'S', 'p', 'i', 'C', 'e', 'y'};
    NSData *theDataKey = [NSData dataWithBytes:keyPtr length:sizeof(keyPtr)];
    NSString* encryptedKey = [[NSString alloc] initWithData:theDataKey encoding:NSUTF8StringEncoding];
    for (int i = 0; i < ITERATIONS; i++) {
        valueToEnc=[NSString stringWithFormat:@"%@%@",salt,eValue];
        eValue = [valueToEnc AES128EncryptWithKey:encryptedKey];
    }
    return eValue;
}



-(NSString*)DecryptChatData:(NSString*)value saltkey:(NSString*)salt

{
    NSString *decordedValue ;
    NSString *dValue ;
    NSString *valueToDecrypt ;
    valueToDecrypt=value;
    unsigned char keyPtr[kCCKeySizeAES128] = {'u', 'M', 'a', 'N', 'g', 'c', 'H', 'a', 't', '@', 'S', 'p', 'i', 'C', 'e', 'y'};
    NSData *theDataKey = [NSData dataWithBytes:keyPtr length:sizeof(keyPtr)];
    NSString* encryptedKey = [[NSString alloc] initWithData:theDataKey encoding:NSUTF8StringEncoding];
    int ITERATIONS=2;
    for (int i = 0; i < ITERATIONS; i++) {
        decordedValue= [valueToDecrypt AES128DecryptWithKey:encryptedKey];
        @try {
            if ([decordedValue length]>0)
            {
                
                NSLog(@"1befor decode");
                dValue = [decordedValue  substringToIndex:salt.length];
                NSLog(@"1after decode");
                decordedValue = [decordedValue stringByReplacingOccurrencesOfString:dValue withString:@""];
                // NSLog(@"decordedValue: %@", decordedValue);
                valueToDecrypt=decordedValue;
                
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
    return valueToDecrypt;
    
}


//-----------------------------------------------
//----- New encrption added for chat End
//-----------------------------------------------


//protected static final String saltChat = "this";
///protected static final byte[] keyValueChat = new byte[]{'u', 'M', 'a', 'N', 'g', 'c', 'H', 'a', 't', '@', 'S', 'p', 'i', 'C', 'e', 'y'};


-(NSString*)DecryptData:(NSString*)value saltkey:(NSString*)salt

{
    NSString *decordedValue ;
    NSString *dValue ;
    NSString *valueToDecrypt ;
    valueToDecrypt=value;
    unsigned char keyPtr[kCCKeySizeAES128] = {'@', '#', 'u', 'm', 'A', 'N', 'G', '#', '$', '%', '&', 'e', 't', 'K', 'e', 'y'};
    NSData *theDataKey = [NSData dataWithBytes:keyPtr length:sizeof(keyPtr)];
    NSString* encryptedKey = [[NSString alloc] initWithData:theDataKey encoding:NSUTF8StringEncoding];
    int ITERATIONS=2;
    for (int i = 0; i < ITERATIONS; i++) {
        decordedValue= [valueToDecrypt AES128DecryptWithKey:encryptedKey];
        @try {
            if ([decordedValue length]>0)
            {
                
                
             //   NSLog(@"1befor decode decordedValue=%@",decordedValue);
                dValue = [decordedValue  substringToIndex:salt.length];
                decordedValue = [decordedValue stringByReplacingOccurrencesOfString:dValue withString:@""];
              //  NSLog(@"after decode decordedValue=%@",decordedValue);
                valueToDecrypt=decordedValue;
            }
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
    }
    return valueToDecrypt;
    
}


//----- Code to decrypt data using our keys and default AES crypto-----------
-(NSString *)AES128DecryptWithKey:(NSString *)key
{
    NSData *encryptedData = [NSData dataWithBase64EncodedString:self];
    NSData *plainData = [encryptedData AES128DecryptWithKey:key];
    NSString* plainString = [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
    
    return plainString;
    
    
    
}

//------ Digilocker Encryption

-(NSString*)EncryptDataDigilocker:(NSString*)value saltkey:(NSString*)salt
{
    int ITERATIONS=2;
    NSString * eValue = value;
    //"kdigi": "$%loVHG@\u0026%*eaSey"--- Key Digilocker---
    NSString * valueToEnc;
    unsigned char keyPtr[kCCKeySizeAES128] = {'$', '%', 'l', 'o', 'V', 'H', 'G', '@', '&', '%', '*', 'e', 'a', 'S', 'e', 'y'};
    NSData *theDataKey = [NSData dataWithBytes:keyPtr length:sizeof(keyPtr)];
    NSString* encryptedKey = [[NSString alloc] initWithData:theDataKey encoding:NSUTF8StringEncoding];
    for (int i = 0; i < ITERATIONS; i++) {
        valueToEnc=[NSString stringWithFormat:@"%@%@",salt,eValue];
        eValue = [valueToEnc AES128EncryptWithKey:encryptedKey];
        //NSLog(@"%@",eValue);
    }
    
    
    return eValue;
}


-(NSString*)DecryptDataDigiLocker:(NSString*)value saltkey:(NSString*)salt

{
    NSString *decordedValue ;
    NSString *dValue ;
    NSString *valueToDecrypt ;
    valueToDecrypt=value;
    unsigned char keyPtr[kCCKeySizeAES128] = {'$', '%', 'l', 'o', 'V', 'H', 'G', '@', '&', '%', '*', 'e', 'a', 'S', 'e', 'y'};
    NSData *theDataKey = [NSData dataWithBytes:keyPtr length:sizeof(keyPtr)];
    NSString* encryptedKey = [[NSString alloc] initWithData:theDataKey encoding:NSUTF8StringEncoding];
    int ITERATIONS=2;
    for (int i = 0; i < ITERATIONS; i++) {
        decordedValue= [valueToDecrypt AES128DecryptWithKey:encryptedKey];
        @try {
            if ([decordedValue length]>0)
            {
                
                
                //NSLog(@"1befor decode decordedValue=%@",decordedValue);
                dValue = [decordedValue  substringToIndex:salt.length];
                decordedValue = [decordedValue stringByReplacingOccurrencesOfString:dValue withString:@""];
               // NSLog(@"after decode decordedValue=%@",decordedValue);
                valueToDecrypt=decordedValue;
            }
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
    }
    return valueToDecrypt;
    
}



//----To get a hash for plain text input SHA256----------------
-(NSString*)sha256HashFor:(NSString*)input
{
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

//------------To get hash for NSData as input-------------
- (NSString *)SHA256_HASH {
    //if (!self) return nil;
    
    unsigned char hash[CC_SHA256_DIGEST_LENGTH];
    if ( CC_SHA256([(NSData*)self bytes], [(NSData*)self length], hash) ) {
        NSData *sha2 = [NSData dataWithBytes:hash length:CC_SHA256_DIGEST_LENGTH];
        
        // description converts to hex but puts <> around it and spaces every 4 bytes
        NSString *hash = [sha2 description];
        hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
        hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
        hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
        // hash is now a string with just the 40char hash value in it
        //NSLog(@"hash = %@",hash);
        
        // Format SHA256 fingerprint like
        // 00:00:00:00:00:00:00:00:00
        int keyLength=[hash length];
        NSString *formattedKey = @"";
        for (int i=0; i<keyLength; i+=2) {
            NSString *substr=[hash substringWithRange:NSMakeRange(i, 2)];
            if (i!=keyLength-2)
                substr=[substr stringByAppendingString:@":"];
            formattedKey = [formattedKey stringByAppendingString:substr];
        }
        
        return formattedKey;
    }
    return nil;
}


@end

