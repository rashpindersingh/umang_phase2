//
//  NSString+AES256Crypt.h
//  AES256EnDeCrypt
//
//  Created by deepak singh rawat on 09/11/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AESCrypt)
-(NSString*)EncryptData:(NSString*)value saltkey:(NSString*)salt;
-(NSString *)AES128EncryptWithKey:(NSString *)key;


-(NSString*)DecryptData:(NSString*)value saltkey:(NSString*)salt;
-(NSString *)AES128DecryptWithKey:(NSString *)key;



- (NSString*)sha256HashFor:(NSString*)input;
- (NSString *)SHA256_HASH;



-(NSString*)DecryptChatData:(NSString*)value saltkey:(NSString*)salt;
-(NSString*)EncryptChatData:(NSString*)value saltkey:(NSString*)salt;


-(NSString*)EncryptDataDigilocker:(NSString*)value saltkey:(NSString*)salt;
-(NSString*)DecryptDataDigiLocker:(NSString*)value saltkey:(NSString*)salt;

-(NSString*)EncryptData:(NSString*)value saltkey:(NSString*)salt withKey:(NSString*)Key;
-(NSString*)DecryptData:(NSString*)value saltkey:(NSString*)salt withKey:(NSString*)Key;

@end

