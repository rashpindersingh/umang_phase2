//
//  NavigationAdvanceSearchXIB.h
//  Umang
//
//  Created by admin on 30/04/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationAdvanceSearchXIB : UIView<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIImageView *iconSearchImage;

@property (strong, nonatomic) IBOutlet UIButton *leftBackButton;
@property (strong, nonatomic) IBOutlet UIButton *rightFilterButton;
@property (strong, nonatomic) IBOutlet UITextField *txtSearchView;

@property (strong, nonatomic) IBOutlet UIView *vw_bgSearch;
@property (strong, nonatomic) IBOutlet UIView *vw_bottomLine;


@property(copy) void(^didTapFilterBarButton)(id btnfilter);
@property(copy) void(^didTapBackButton)(id btnBack);

@end
