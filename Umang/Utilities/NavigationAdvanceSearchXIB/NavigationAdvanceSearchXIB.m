//
//  NavigationAdvanceSearchXIB.m
//  Umang
//
//  Created by admin on 30/04/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "NavigationAdvanceSearchXIB.h"

@implementation NavigationAdvanceSearchXIB



- (id)init{
    self = [self initWithFrame:[self getRect]];
    
    
    return self;
    
}


-(CGRect)getRect {
    // return [UIScreen mainScreen].bounds.size.height == 812.0 ? CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 84.0) : CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 64.0);
    return [UIScreen mainScreen].bounds.size.height == 812.0 ? CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 94.0) : CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 64.0);
    
}

- (id)initWithFrame:(CGRect)frame
{
    SharedManager *singleton = [SharedManager sharedSingleton];
    
    self = [super initWithFrame:frame];
    
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"NavigationAdvanceSearchXIB" owner:self options:nil];
    self = (NavigationAdvanceSearchXIB *)[objects objectAtIndex:0];
    self.frame = frame;
    
    
    
    // Set textfield value as Search with localisation
    [self.txtSearchView setValue:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    self.txtSearchView.placeholder = NSLocalizedString(@"type_to_search", @"");
    self.txtSearchView.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    self.txtSearchView.font = [AppFont mediumFont:13.0];
    self.vw_bgSearch.layer.cornerRadius = 5.0;
    
    
  
    
    return self;
}
- (IBAction)didTapFilterBarButton:(UIButton *)sender {
    
    if (self.didTapFilterBarButton != nil) {
        self.didTapFilterBarButton(sender);
    }
}
- (IBAction)didTapBackButton:(UIButton *)sender {
    if (self.didTapBackButton != nil) {
        self.didTapBackButton(sender);
    }
}




/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

