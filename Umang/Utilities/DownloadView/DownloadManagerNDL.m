//
//  DownloadManagerNDL.m
//  Umang
//
//  Created by Rashpinder on 15/06/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "DownloadManagerNDL.h"
#import "RunOnMainThread.h"
static DownloadManagerNDL *downloadSinglton = nil;
@interface DownloadManagerNDL ()<NSURLSessionDelegate>
{
    
    
}
@end
@implementation DownloadManagerNDL
{
    NSURLSessionDownloadTask *downtask;
}

+ (DownloadManagerNDL*)sharedSingleton
{
    @synchronized(self) {
        if (downloadSinglton == nil)
        {
            downloadSinglton = [[super allocWithZone:NULL] init];
        }
        return downloadSinglton;
    }
}

-(void)networkNotReachable {
    if (downtask != nil) {
        downtask.cancel;
        SharedManager *singleton = [SharedManager sharedSingleton];
        NSString *taskID = downtask.taskDescription;
        __block typeof(self) weakSelf = self;
        if (singleton.arrDocumentDownloading.count != 0 ) {
            [singleton.arrDocumentDownloading removeAllObjects];
            if (weakSelf.didErrorDownload != nil ) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    weakSelf.didErrorDownload(taskID);
                });
            }
        }
    }
}
-(void)startDocumentDownloadWithJson:(NSDictionary *)json   contentUrl:(NSString*)contentUrl

{
    NSURL *URL = [NSURL URLWithString:contentUrl];
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    // NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithURL:URL];
    NSString *doc_id = [json valueForKey:@"doc_id"];
     downtask = [session downloadTaskWithURL:URL];
     downtask.taskDescription = doc_id;
     [downtask resume];
}


-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error != nil ) {
        SharedManager *singleton = [SharedManager sharedSingleton];
        NSString *taskID = task.taskDescription;
        __block typeof(self) weakSelf = self;
        if (singleton.arrDocumentDownloading.count != 0 ) {
            if (weakSelf.didErrorDownload != nil ) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    weakSelf.didErrorDownload(taskID);
                });
            }else {
                [singleton.arrDocumentDownloading removeAllObjects];
            }
        }
    }
}
-(void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    
    float progress = (double)totalBytesWritten/(double)totalBytesExpectedToWrite;
    __block typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf.didUpdateProgress != nil ) {
            weakSelf.didUpdateProgress(progress);
        }
    });
}

-(void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    
}
-(void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    SharedManager *singleton = [SharedManager sharedSingleton];
    NSString *task = downloadTask.taskDescription;
    //@try {
        for(NSDictionary *json in singleton.arrDocumentDownloading) {
            NSString *doc = [json valueForKey:@"doc_id"];
            if ([doc isEqualToString:task]) {
                [self saveNDLFileToLocalPath:json withLocation:location];
                break;
            }
        }
   // } @catch (NSException *exception) {
   // } @finally {
    //}
    __block typeof(self) weakSelf = self;
    if (weakSelf.didFinishDownload != nil ) {
        dispatch_async(dispatch_get_main_queue(), ^{
       if (singleton.arrDocumentDownloading.count != 0 ) {
        if (weakSelf.didFinishDownload != nil ) { weakSelf.didFinishDownload([singleton.arrDocumentDownloading objectAtIndex:0]);
            }
          }
        });
    }else {
        if (singleton.arrDocumentDownloading.count != 0 ) {
            [singleton.arrDocumentDownloading removeObjectAtIndex:0];
        }
        if (singleton.arrDocumentDownloading.count != 0 ) {
            NSInteger countDown = [self.currentDownloading integerValue]+1;
            self.currentDownloading = [NSString stringWithFormat:@"%lu",countDown];
            [self startDocumentDownloadWithJson: [singleton.arrDocumentDownloading objectAtIndex:0]];
            
        }
    }
    
}
-(void)saveNDLFileToLocalPath:(NSDictionary*)json withLocation:(NSURL *)location
{
   
    SharedManager *singleton = [SharedManager sharedSingleton];
    NSString* doc_id = [json valueForKey:@"doc_id"];
    NSString* contentUrl =[json valueForKey:@"contentUrl"];
    contentUrl = [contentUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSString* fileName =[json valueForKey:@"fileName"];;
    
    NSString* type =[[json valueForKey:@"type"] lowercaseString];
    NSString* fileFormat =[[json valueForKey:@"fileFormat"] lowercaseString];
    
    NSString* thumbnailUrl =[json valueForKey:@"thumbnailUrl"];;
    NSString* author =[json valueForKey:@"author"];;
    NSString* contentSize =[json valueForKey:@"contentSize"];;
    NSString* date_time=[json valueForKey:@"dateTime"];//N_DATE_TIME
    NSString* filetitle =[json valueForKey:@"fileTitle"];//N_FILE_TITLE
    
    if (singleton.ndliGlobalUserId == nil) {
        singleton.ndliGlobalUserId = @"12345bdfjkewbf";
    }
    NSString *userDicPath= [NSString stringWithFormat:@"%@",singleton.ndliGlobalUserId];
    
    
    NSString *filePath=[self createDirectory:userDicPath];
    
    //NSString* filenamewithBook=[NSString stringWithFormat:@"%@/%@.%@",filePath,doc_id,fileFormat];
    
    fileName = [fileName stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString* filenamewithBook=[NSString stringWithFormat:@"%@/%@",filePath,fileName];
    
    
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *fileManagerError;
    if ([fileManager fileExistsAtPath:filenamewithBook])
    {
        success = [fileManager removeItemAtPath:filenamewithBook error:&fileManagerError];
    }
    
    success = [fileManager moveItemAtURL:location toURL:[NSURL fileURLWithPath:filenamewithBook] error:&fileManagerError];
    NSLog(@"save NDL File at URL %@ with Status ---",location);
    NSString * ndlDocPath= [NSString stringWithFormat:@"%@",filenamewithBook];
    
    [singleton.dbManager insertData_NDL:doc_id ndlDocPath:ndlDocPath thumbnailUrl:thumbnailUrl author:author fileFormat:fileFormat filetitle:filetitle filename:fileName type:type contentSize:contentSize contentUrl:contentUrl date_time:date_time user_id:singleton.ndliGlobalUserId];
//    NSDictionary *jsontoPass = @{@"status":@"S",@"message": @"File downloaded successfully",@"doc_id" : doc_id};
//    NSError *error;
//    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:jsontoPass options:NSJSONWritingPrettyPrinted error:&error];
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
//    NSLog(@"jsonString==>%@",jsonString);
//
//    jsonString=[self JSONString:jsonString];
//
//
//    NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",singleton.ndliGlobalCallBack,jsonString];
//    UIWebView *webview = [[UIWebView alloc] init];
//    [webview performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
//
}
-(NSString *)JSONString:(NSString *)aString {
    NSMutableString *s = [NSMutableString stringWithString:aString];
    [s replaceOccurrencesOfString:@"\"" withString:@"\\\"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"/" withString:@"\\/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\n" withString:@"\\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\b" withString:@"\\b" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\f" withString:@"\\f" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\r" withString:@"\\r" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\t" withString:@"\\t" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    return [NSString stringWithString:s];
}
-(NSString*) createDirectory : (NSString *) dirName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Fetch path for document directory
    NSString* filePath = (NSMutableString *)[documentsDirectory stringByAppendingPathComponent:dirName];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:NO attributes:nil error:&error]) {
        NSLog(@"Couldn't create directory error: %@", error);
    }
    else {
        NSLog(@"directory created!");
    }
    
    NSLog(@"filePath : %@ ",filePath); // Path of folder created
    
    return filePath;
}

-(void)startDocumentDownloadWithJson:(NSDictionary *)json
{
    
    SharedManager *singleton = [SharedManager sharedSingleton];
   // NSString* doc_id = [json valueForKey:@"doc_id"];
    NSString* contentUrl =[json valueForKey:@"contentUrl"];
    
    contentUrl = [contentUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    //NSString* name =[json valueForKey:@"fileName"];;
    
    NSString* type =[[json valueForKey:@"type"] lowercaseString];
    NSString* fileFormat =[[json valueForKey:@"fileFormat"] lowercaseString];
    bool videDownload = false;
    if ([type isEqualToString:@"video"])
    {
        
        if ([fileFormat isEqualToString:@"mov"] || [fileFormat isEqualToString:@"m4v"] || [fileFormat isEqualToString:@"mp4"] || [fileFormat isEqualToString:@"3gp"] )
        {
            videDownload = true;
        }
        
    }
    else
    {
        videDownload = true;
    }
    
    if ( videDownload == false )
    {
        [singleton.arrDocumentDownloading removeObjectAtIndex:0];
  
        
        return;
    }
    if (contentUrl.length == 0)
    {
        [singleton.arrDocumentDownloading removeObjectAtIndex:0];
        return;
    }
    // [vw_download downloadFileWith:(NSMutableDictionary*)json];
    [self startDocumentDownloadWithJson:json contentUrl:contentUrl];
  
  
    //[downtask resume];
}
@end
