//
//  NavigationView.h
//  Umang
//
//  Created by Rashpinder on 30/04/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadView : UIView
    @property (weak, nonatomic) IBOutlet UILabel *lblDownloading;
    @property (weak, nonatomic) IBOutlet UILabel *lblDocNameDownloading;
    @property (weak, nonatomic) IBOutlet UILabel *lblNumberOfDownloads;
    
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UILabel *lblProgress;


@property(copy) void(^didTapBackButton)(id btnBack);


@end
