//
//  NavigationView.m
//  Umang
//
//  Created by Rashpinder on 30/04/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "DownloadView.h"

@implementation DownloadView


- (id)init{
    self = [self initWithFrame:[self getRect]];
    
    return self;

}
-(CGRect)getRect {
    return [UIScreen mainScreen].bounds.size.height == 812.0 ? CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 84.0) : CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width , 64.0);
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"DownloadViewNIB" owner:self options:nil];
    self = (DownloadView *)[objects objectAtIndex:0];
    self.frame = frame;
    self.lblDownloading.font = [AppFont regularFont:12.0];
    self.lblNumberOfDownloads.font = [AppFont regularFont:12.0];
    self.lblDocNameDownloading.font = [AppFont regularFont:12.0];
     self.lblProgress.font = [AppFont regularFont:11.0];
  self.lblDownloading.text=NSLocalizedString(@"downloading_ebook", nil);//""
    [self.btnCancel setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];

    return self;
}

- (IBAction)didTapBackButtonAction:(UIButton *)sender {
    if (self.didTapBackButton != nil) {
        self.didTapBackButton(sender);
    }
}
- (IBAction)didTapCancelButtonAction:(UIButton *)sender {
    if (self.didTapBackButton != nil) {
        self.didTapBackButton(sender);
    }
}


@end
