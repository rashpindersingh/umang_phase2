//
//  DownloadManagerNDL.h
//  Umang
//
//  Created by Rashpinder on 15/06/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DownloadManagerNDL : NSObject

@property(nonatomic,strong) void(^ _Nullable didUpdateProgress)(float progress);
@property(nonatomic,strong) void(^ _Nullable didFinishDownload)(NSDictionary * _Nullable json);
@property(nonatomic,strong) void(^ _Nullable didErrorDownload)(NSString * _Nullable docID);


-(void)networkNotReachable;
+ (DownloadManagerNDL*_Nonnull)sharedSingleton;
-(void)startDocumentDownloadWithJson:(NSDictionary *_Nullable)json   contentUrl:(NSString*_Nullable)contentUrl;

@property(strong)NSString *downloadCount;
@property(strong)NSString *currentDownloading;

@end
