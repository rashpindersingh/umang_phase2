//
//  NavigationView.h
//  Umang
//
//  Created by Rashpinder on 30/04/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDetailNaviVW : UIView
@property (strong, nonatomic) IBOutlet UIButton *rightBarButton;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIButton *leftBackButton;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIView *viewMoreRightItems;
@property (strong, nonatomic) IBOutlet UIButton *rightRightBarButton;
@property (strong, nonatomic) IBOutlet UILabel *secondLblTitle;
@property (strong, nonatomic) IBOutlet UIButton *secondRightBarButtobn;

@property (strong, nonatomic) IBOutlet UIButton *secondBackButton;


@property(copy) void(^didTapBackButton)(id btnBack);

@property(copy) void(^didTapRightBarButton)(id btnReset);
@property(copy) void(^didTapSecondRightBarButton)(id btnReset);
@property(copy) void(^didTapThirdRightBarButton)(id btnReset);

- (id)initRightBarButton;
- (id)initSecondBarItemsView;
@end
