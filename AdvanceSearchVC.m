//
//  SearchViewController.m
//  Umang
//
//  Created by spice_digital on 06/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "AdvanceSearchVC.h"
#import "RunOnMainThread.h"
#import "SearchTrendCell.h"
//#import "UIImageView+WebCache.h"

#import "StateList.h"
#import "advanceSearchCell.h"

#import "UMAPIManager.h"
#import "SharedManager.h"
#define fDeviceWidth ([UIScreen mainScreen].bounds.size.width)
#define fDeviceHeight ([UIScreen mainScreen].bounds.size.height)

#import "AppDelegate.h"

#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication]delegate])

#define TAGFILTERSTBL 100
#define TAGSEARCHRESULTTBL 101
#define TAGAUTOCOMPTBL 102
#define TAGAUTORECENT 105
#import "MBProgressHUD.h"

#import "UIImageView+WebCache.h"
#import "HomeDetailVC.h"

#import "recentTrendCell.h"
#import "FavouriteCell.h"

#import "LoginAppVC.h"

#import "UMANG-Swift.h"




@interface AdvanceSearchVC ()<UITextFieldDelegate>
{
    SharedManager *singleton;
    UITableView *autocompleteTableView;
    StateList *obj;
    BOOL flagShowfilter;
    int filterTag;
    CGFloat xCord;
    CGFloat yCord;
    CGFloat width;
    MBProgressHUD *hud ;
    
    __weak IBOutlet UILabel *lblCategoryType;
    __weak IBOutlet UILabel *lblStateType;
    __weak IBOutlet UILabel *lblServiceType;
    BOOL onFilter;
    
    int tagRecentTrend;
    __weak IBOutlet UIView *vwTextBg;
    
    NavigationAdvanceSearchXIB *nvSearchView ;
}
@property (weak, nonatomic) IBOutlet UIView *vw_filter;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_servicetype;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_state;


@property (nonatomic, strong) NSMutableArray *autocompleteSearch;


@property (weak, nonatomic) IBOutlet UILabel *lbl_title_category;
@property (weak, nonatomic) IBOutlet UILabel *lbl_servicetype;
@property (weak, nonatomic) IBOutlet UILabel *lbl_category;

@property (weak, nonatomic) IBOutlet UILabel *lbl_state;
@property (weak, nonatomic) IBOutlet UIButton *btn_servicetype;
@property (weak, nonatomic) IBOutlet UIButton *btn_state;
@property (weak, nonatomic) IBOutlet UIButton *btn_category;



@property(retain,nonatomic)NSString *header_title_pass;
@property(retain,nonatomic)NSMutableArray *arr_table_pass;
@property(retain,nonatomic)NSString *TAG_pass;


@property (weak, nonatomic) IBOutlet UIView *vw_tbladvance;
@property (nonatomic, strong) NSMutableArray *arr_filter;
@property (weak, nonatomic) IBOutlet UITableView *tbl_filter;



@property(retain,nonatomic)NSMutableArray *arr_results;
@property(weak,nonatomic)IBOutlet UITableView *tbl_recentrend;


@property (nonatomic, strong) NSMutableArray *recentArray;
@property(nonatomic,retain)NSMutableArray *trendingarray;


@end

@implementation AdvanceSearchVC

@synthesize autocompleteSearch;
@synthesize header_title_pass,arr_table_pass,TAG_pass;

@synthesize arr_results;



-(IBAction)btnSearchClose:(id)sender
{
    
}

-(IBAction)btn_filter:(id)sender
{
    
    
    CGRect frame=lbl_results.frame;
    
    if (flagShowfilter==false) {
        flagShowfilter=true;
        _vw_filter.hidden=false;
     
        if (iPhoneX())
        {
            CGRect vwFrame =_vw_filter.frame;
            vwFrame.origin.y=kiPhoneXNaviHeight4Search;
            _vw_filter.frame=vwFrame;
            
        }
        
       

        
        // lbl_results.frame=CGRectMake(0, frame.origin.y, frame.size.width, frame.size.height);
    }
    else
    {
        flagShowfilter=false;
        _vw_filter.hidden=TRUE;
        _vw_tbladvance.hidden=TRUE;//hide tableview
        
        // lbl_results.frame=CGRectMake(0, frame.origin.y-48, frame.size.width, frame.size.height);
        
    }
    
    float yAxis=161.0f;
    
    if (flagShowfilter==TRUE)
    {
        //yAxis=74+48;
        
        //add condition for iphoneX
        yAxis = [UIScreen mainScreen].bounds.size.height == 812.0 ? (94+48) : (74+48);

    }
    else
    {
       // yAxis=74.0f;
        yAxis = [UIScreen mainScreen].bounds.size.height == 812.0 ? 94 : 74;

    }
    //autocompleteTableView.translatesAutoresizingMaskIntoConstraints = YES;
    
    
    
    float height=fDeviceHeight-yAxis; //49 tab
    NSLog(@"height after=%f",height);
    autocompleteTableView.frame = CGRectMake(0, yAxis, fDeviceWidth, height);
    tbl_search.frame= CGRectMake(0, yAxis+25, fDeviceWidth, fDeviceHeight-yAxis-25);
    
    lbl_results.frame=CGRectMake(0,yAxis, frame.size.width, frame.size.height);
    
    _tbl_recentrend.frame=CGRectMake(0, yAxis, fDeviceWidth, height);
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        height = fDeviceWidth-160 - yAxis;
        
        autocompleteTableView.frame = CGRectMake(80, yAxis + 40, fDeviceWidth-160, height);
        self.tbl_recentrend.frame   = autocompleteTableView.frame;
        tbl_search.frame= CGRectMake(80, yAxis+25 + 20, fDeviceWidth -160, fDeviceHeight-yAxis-45);
    }
    
    
    
    
}










-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    //———— Add to handle network bar of offline——
    
    
    
    tbl_search.tag=TAGSEARCHRESULTTBL;
    
    
    // tbl_search.allowsSelection = NO;
    
    //flagShowfilter=true;
    
    onFilter=TRUE;
    
    flagShowfilter=false;
    _vw_filter.hidden=TRUE;
    _vw_tbladvance.hidden=TRUE;//hide tableview
    
    _btn_state.enabled=FALSE;
    _lbl_state.textColor=[UIColor lightGrayColor];
    _lbl_title_state.textColor=[UIColor lightGrayColor];
    
    
    
    
    float yAxis=161.0f;
    
    if (flagShowfilter==TRUE) {
       // yAxis=74+48;
        yAxis = [UIScreen mainScreen].bounds.size.height == 812.0 ? (94+48) :(74+48) ;

    }
    else
    {
       // yAxis=74.0f;
        yAxis = [UIScreen mainScreen].bounds.size.height == 812.0 ? 94 : 74;

    }
    
    
    float height=fDeviceHeight-yAxis; //49 tab
    NSLog(@"height after=%f",height);
    autocompleteTableView.frame = CGRectMake(0, yAxis, fDeviceWidth, height);
    _tbl_recentrend.frame=CGRectMake(0, yAxis, fDeviceWidth, height);
    
    
    //autocompleteTableView.translatesAutoresizingMaskIntoConstraints = YES;
    tbl_search.frame= CGRectMake(0, yAxis+25, fDeviceWidth, fDeviceHeight-yAxis-25);
    
    CGRect frame=lbl_results.frame;
    
    lbl_results.frame=CGRectMake(0,yAxis, frame.size.width, frame.size.height);
    
    [self refreshData];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        height = fDeviceWidth-160 - yAxis;
        
        autocompleteTableView.frame = CGRectMake(80, yAxis + 40, fDeviceWidth-160, height);
        self.tbl_recentrend.frame   = autocompleteTableView.frame;
        
        tbl_search.frame= CGRectMake(80, yAxis+25 + 20, fDeviceWidth -160, fDeviceHeight-yAxis-45);
        
    }
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    [self setViewFont];
    
    [super viewWillAppear:YES];
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    // [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    if(iPhoneX())
    {
        nvSearchView.txtSearchView.font = [AppFont mediumFont:15.0];

    }
    else
    {
    txt_search.font = [AppFont mediumFont:15.0];
    }
    lblServiceType.font = [AppFont regularFont:11.0];
    lblStateType.font = [AppFont regularFont:11.0];
    lblCategoryType.font = [AppFont regularFont:11.0];
    
    _lbl_servicetype.font = [AppFont mediumFont:12];
    _lbl_state.font = [AppFont mediumFont:12];
    _lbl_category.font = [AppFont mediumFont:12];
    
    lbl_results.font = [AppFont lightFont:15];
    //lblHeaderDescriptiom.font = [AppFont semiBoldFont:17.0];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    // [txt_search resignFirstResponder];
    [self.view endEditing:YES];
    onFilter =FALSE;
    // [self callCustomPicker];
}



-(void)refreshData
{
    self.recentArray=[[NSMutableArray alloc]init];
    self.trendingarray=[[NSMutableArray alloc]init];
    
    self.trendingarray=singleton.arr_trend_Searchservice;//set trendings
    
    
    self.arr_filter=[[NSMutableArray alloc]init];
    
    
    self.recentArray=[[self fetchRecentDB] mutableCopy];
    
    
    NSLog(@" recentArray =%@", self.recentArray );
    
    
    
    
    /*self.recentArray=[NSMutableArray arrayWithObjects:@"one",@"two",@"three",@"four", nil];
     self.trendingarray=[NSMutableArray arrayWithObjects:@"Trendone",@"Trendtwo",@"Trendthree",@"Trendfour", nil];
     
     */
    
    [_tbl_recentrend reloadData];
    [autocompleteTableView reloadData];
    [self reloadTableContents];
}

-(void)clearPressed:(id)sender
{
    
    [singleton.arr_recent_Searchservice removeAllObjects];
    [self refreshData];
    
}
- (IBAction)goPressed:(NSArray*)addService
{
    // [txt_search resignFirstResponder];
    [self.view endEditing:YES];
    autocompleteTableView.hidden = YES;
    _tbl_recentrend.hidden=TRUE;
    tbl_search.hidden=FALSE;
   // [self hitNewappsearchAPI:txt_search.text];
    
    if(iPhoneX())
    {
        [self hitNewappsearchAPI:nvSearchView.txtSearchView.text];

    }
    else
    {
        [self hitNewappsearchAPI:txt_search.text];
    }
    
    
    
    tbl_search.hidden=NO;
    
    if (tagRecentTrend==101)
    {
        // Clean up UI
        NSString *addServiceName ;
        if (tagCheck==100) {
            addServiceName   = [NSString stringWithFormat:@"%@",addService];
        }
        else
        {
            //======================== Start Offline code 6 april 2018 ======================

            AppDelegate *appD = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if (appD.networkStatus==false)
            {
                addServiceName   = [NSString stringWithFormat:@"%@",[addService valueForKey:@"SERVICE_NAME"]];

            }
            else
            {
              addServiceName   = [NSString stringWithFormat:@"%@",[addService valueForKey:@"SERVICE_ALISES"]];
            }
            
            //======================== End Offline code 6 april 2018 ======================

            //uncomment below line for old and comment above lines 
           // addServiceName   = [NSString stringWithFormat:@"%@",[addService valueForKey:@"SERVICE_ALISES"]];

        }
        
        if (![singleton.arr_recent_Searchservice containsObject:addServiceName])
        {
            [singleton.arr_recent_Searchservice addObject:addServiceName];
        }
    }
    
    
    
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _vw_tbladvance.hidden=TRUE;//add later
    
    
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *combinedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    
    
    
    
    if ([combinedText length]!=0)
    {
        
        autocompleteTableView.tag=TAGAUTOCOMPTBL;
        autocompleteTableView.hidden = NO;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            lbl_results.hidden = TRUE;
            tbl_search.hidden=TRUE;
        }
        
        
        self.tbl_recentrend.hidden=TRUE;
        
        
    }
    else
    {
        autocompleteTableView.hidden = YES;
        
        self.tbl_recentrend.hidden=FALSE;
        //[autocompleteTableView reloadData];
        [self refreshData];
        
    }
    
    
    
    
    
    //autocompleteTableView.tag=TAGAUTOCOMPTBL;
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    [self searchAutocompleteEntriesWithSubstring:substring];
    return YES;
}


-(NSArray *)fetchRecentDB
{
    
    NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:singleton.arr_recent_Searchservice];
    NSArray*  myArray = [[NSMutableArray alloc] initWithArray:[mySet array]];
    
    //NSMutableArray *reversedArray = [NSMutableArray arrayWithCapacity:4]; // will grow if needed.
    NSMutableArray *reversedArray = [[[myArray reverseObjectEnumerator] allObjects] mutableCopy];
    
    
    if ([reversedArray count]>5) {
        NSArray *tempArray = [reversedArray subarrayWithRange:NSMakeRange(0, 5)];
        return  tempArray;
    }
    
    return reversedArray ;
}

-(void)loadView
{
    
    singleton = [SharedManager sharedSingleton];
    //[self fetchDatafromDB];
    //  txt_search.text=txt_Searchingfor;
    
    static NSString *CellIdentifier2 = @"SearchTrendCell";
    
    UINib *nib = [UINib nibWithNibName:@"SearchTrendCell" bundle:nil];
    [tbl_search registerNib:nib forCellReuseIdentifier:CellIdentifier2];
    [super loadView];
    
    
}
-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor grayColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}



#pragma mark - UIView Methods

- (void)viewDidLoad
{
    if (iPhoneX())
    {
        NSLog(@"An iPhone X Load UI");
        
        nvSearchView = [[NavigationAdvanceSearchXIB alloc] init];
    }
    
    [super viewDidLoad];
    
    tagCheck=0;
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:ADVANCE_SEARCH_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    [txt_search setValue:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    txt_search.placeholder = NSLocalizedString(@"type_to_search", @"");
    
    singleton = [SharedManager sharedSingleton];
    
    // now add textview as per iPhoneX
    if(iPhoneX())
    {
        nvSearchView.txtSearchView.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;

    }
    else
    {
        txt_search.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    }
    
    // recent_tbl
    
    lblStateType.text=NSLocalizedString(@"state_label", nil);
    lblServiceType.text=NSLocalizedString(@"alll_label", nil);
    lblCategoryType.text=NSLocalizedString(@"cat_label", nil);
    
    _lbl_state.text=NSLocalizedString(@"all", nil);
    
    
    _lbl_servicetype.text=NSLocalizedString(@"all", nil);
    
    
    _lbl_category.text=NSLocalizedString(@"all", nil);
    
    
    self.tbl_recentrend.hidden=false;
    self.tbl_recentrend.tag=TAGAUTORECENT;
    
    vwTextBg.layer.cornerRadius = 5.0;
    [txt_search setValue:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    //txt_search.placeholder = NSLocalizedString(@"search", @"");
    
    lbl_results.hidden=TRUE;
    // pastServices = [[NSMutableArray alloc] initWithObjects:@"www.google.com",@"www.facebook.com",@"www.twitter.com", nil];
    pastServices = [[NSMutableArray alloc]init];
    arr_results=[[NSMutableArray alloc]init];//for results of api key pass in appsearch api
    

     
     
     AppDelegate *appD = (AppDelegate*)[[UIApplication sharedApplication] delegate];
   
    //======================== Start Offline code 6 april 2018 ======================
    //Comment below  to revert Old
     if (appD.networkStatus==false)
     {
     
         NSMutableArray *temparrServiceData = [NSMutableArray new];
         
         if(singleton.user_tkn.length==0)
         {
             temparrServiceData =[[singleton.dbManager getInfoFlagServiceDataOnly] mutableCopy];

         }
         else
         {
             temparrServiceData =[[singleton.dbManager loadDataServiceData] mutableCopy];
         }
    // NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
         NSArray *arrServiceData= [[NSArray alloc] initWithArray:temparrServiceData];
         

     for (int i=0; i<[arrServiceData count]; i++)
     {
     [pastServices addObject:[arrServiceData objectAtIndex:i]];
     }
     }
     
    //======================== End Offline code ======================

    
    autocompleteService = [[NSMutableArray alloc] init];
    
    float yAxis=78.0f;
    
    if (flagShowfilter==TRUE) {
        yAxis=78+50;
    }
    else
    {
        yAxis=78.0f;
    }
    autocompleteTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, yAxis, fDeviceWidth, 120) style:UITableViewStylePlain];
    autocompleteTableView.delegate = self;
    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    autocompleteTableView.hidden = YES;
    [self addShadowToTheView:autocompleteTableView];
    
    
    
    
    [self addShadowToTheView:_vw_tbladvance];//adding shadow to popupview
    
    
    tbl_search.hidden = YES;
    
    autocompleteTableView.tableFooterView = [UIView new];//remove empty table rows
    
    [self.view addSubview:autocompleteTableView];
    
    if (iPhoneX()) {
      //do nothing
        
    }
    else
    {
    
    txt_search.delegate=self;
    txt_search.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_search.keyboardAppearance = UIKeyboardAppearanceDefault;
    txt_search.returnKeyType=UIReturnKeySearch;
    [txt_search becomeFirstResponder];

    }
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    vw_line.frame=CGRectMake(0, 74, fDeviceWidth, 0.5);
    self.view.backgroundColor=[[UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0] colorWithAlphaComponent:1];
    obj=[[StateList alloc]init];
    
    tbl_search.backgroundColor=[[UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0] colorWithAlphaComponent:1];
    self.tbl_filter.backgroundColor = tbl_search.backgroundColor;
    self.tbl_recentrend.backgroundColor  = tbl_search.backgroundColor;
    autocompleteTableView.backgroundColor= tbl_search.backgroundColor;
    
    
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self adjustSearchBarView];
    }
    [self addNavigationSearchTabView];
    
}

#pragma mark- add Navigation View to View

-(void)addNavigationSearchTabView{
    
    if (iPhoneX())
    {
        NSLog(@"An iPhone X Load UI");
        
        __weak typeof(self) weakSelf = self;
        vw_line.hidden=TRUE;
        
        
        nvSearchView.txtSearchView.delegate=self;
        nvSearchView.txtSearchView.clearButtonMode = UITextFieldViewModeWhileEditing;
        nvSearchView.txtSearchView.keyboardAppearance = UIKeyboardAppearanceDefault;
        nvSearchView.txtSearchView.returnKeyType=UIReturnKeySearch;
        [nvSearchView.txtSearchView becomeFirstResponder];


        nvSearchView.didTapFilterBarButton= ^(id btnfilter)
        {
            
            [weakSelf btn_filter:btnfilter];
            
        };
        
        
        nvSearchView.didTapBackButton= ^(id btnBack)
        {
            
            [weakSelf cancelBtnAction:btnBack];
            
        };
        
      
        [self.view addSubview:nvSearchView];
        
        CGRect table = tbl_search.frame;
        table.origin.y = kiPhoneXNaviHeight4Search;
        table.size.height = fDeviceHeight - kiPhoneXNaviHeight4Search;
        tbl_search.frame = table;
        [self.view layoutIfNeeded];
        
        
    }
    else
    {
        NSLog(@"Not an iPhone X use default UI");
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Search View Methods

-(void)adjustSearchBarView
{
    vwTextBg.frame = CGRectMake(self.view.frame.size.width/2 - 150, vwTextBg.frame.origin.y, 300, vwTextBg.frame.size.height);
    
    searchIconImage.frame = CGRectMake(vwTextBg.frame.origin.x + 7, searchIconImage.frame.origin.y, searchIconImage.frame.size.width, searchIconImage.frame.size.height);
    
    txt_search.frame = CGRectMake(searchIconImage.frame.origin.x + searchIconImage.frame.size.width + 5, txt_search.frame.origin.y, 265, txt_search.frame.size.height);
    
}

#pragma mark -
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource Methods



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (tableView.tag==TAGAUTORECENT)
    {
        return 2;
    }
    return 1 ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag==TAGAUTOCOMPTBL)
    {
        
        NSLog(@"XXXXX autocompleteService.count=%lu",(unsigned long)autocompleteService.count);
        return autocompleteService.count;
        // return 10;
        
    }
    
    if (tableView.tag==TAGFILTERSTBL)
    {
        return _arr_filter.count;
        // return 10;
        
    }
    if (tableView.tag==TAGSEARCHRESULTTBL)
    {
        // return 1;
        
        return arr_results.count;
    }
    if (tableView.tag==TAGAUTORECENT)
    {
        if (section==0)
        {
            return self.recentArray.count;
            
        }
        if (section==1)
        {
            return self.trendingarray.count;
            
            
        }
        
        
        
    }
    
    return 0;
}


//---------------


#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView.tag==TAGAUTOCOMPTBL)
    {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
      //  txt_search.text = selectedCell.textLabel.text;
        
        
        
        if(iPhoneX())
        {
            nvSearchView.txtSearchView.text = selectedCell.textLabel.text;

        }
        else
        {
            txt_search.text = selectedCell.textLabel.text;
        }
        
        
        
        tagRecentTrend=101;
        
        tagCheck=0;
        
        [self goPressed:[autocompleteService objectAtIndex:indexPath.row]];//used to open
        
        
    }
    if (tableView.tag==TAGFILTERSTBL)
    {
        
        tagRecentTrend=102;
        tagCheck=0;
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        //txt_search.text = selectedCell.textLabel.text;
        
        if (filterTag==1) {
            self.lbl_servicetype.text=selectedCell.textLabel.text;
            
            
            
            
        }
        if (filterTag==2) {
            self.lbl_state.text=selectedCell.textLabel.text;
            
            
            tagCheck=0;
            
            
            
        }
        if (filterTag==3) {
            self.lbl_category.text=selectedCell.textLabel.text;
            
            
            tagCheck=0;
        }
        
        if ([self.lbl_servicetype.text isEqualToString:NSLocalizedString(@"regional", nil)]) {
            _btn_state.enabled=TRUE;
            _lbl_state.textColor=[UIColor blackColor];
            _lbl_title_state.textColor=[UIColor blackColor];
            
            tagCheck=0;
        }
        else
        {
            _lbl_state.text=NSLocalizedString(@"all", nil);
            _btn_state.enabled=FALSE;
            _lbl_state.textColor=[UIColor lightGrayColor];
            _lbl_title_state.textColor=[UIColor lightGrayColor];
            tagCheck=0;
            //arr_results
        }
        
        _vw_tbladvance.hidden=TRUE;//hide tableview
        onFilter=TRUE;
        
       

        
        if(iPhoneX())
        {
            if([ nvSearchView.txtSearchView.text length]>0)
            {
                [self hitNewappsearchAPI:nvSearchView.txtSearchView.text];
                
            }
            
            
        }
        else
        {
            if([txt_search.text length]>0)
            {
                [self hitNewappsearchAPI:txt_search.text];
                
            }
            
        }
        
        
        
        
       // nvSearchView.txtSearchView
    }
    if (tableView.tag==TAGSEARCHRESULTTBL)
    {
        tagCheck=0;
        tbl_search.hidden = NO;
        NSLog(@"====>%@",[arr_results objectAtIndex:indexPath.row]);
        
        
        NSDictionary *cellData = (NSDictionary*)[arr_results objectAtIndex:indexPath.row];
        [self didSelectItem:cellData];
    }
    
    if (tableView.tag==TAGAUTORECENT)
    {
        
        
        if (indexPath.section==0) {
            
            //txt_search.text = [[self.recentArray objectAtIndex:indexPath.row] valueForKey:@"SERVICE_ALISES"];
            
            tagCheck=100;
            
            
            
            if(iPhoneX())
            {
                
                nvSearchView.txtSearchView.text = [self.recentArray objectAtIndex:indexPath.row];

            }
            else
            {
                txt_search.text = [self.recentArray objectAtIndex:indexPath.row];

                
            }
            
            
            [self goPressed:[self.recentArray objectAtIndex:indexPath.row]];//used to open
            
            
        }
        if (indexPath.section==1) {
            
            
            tagCheck=0;
            
            
            
            if(iPhoneX())
            {
                
                nvSearchView.txtSearchView.text= [[self.trendingarray objectAtIndex:indexPath.row] valueForKey:@"alises"];

            }
            else
            {
                txt_search.text= [[self.trendingarray objectAtIndex:indexPath.row] valueForKey:@"alises"];

                
            }
            
            
            
            [self goPressed:[self.trendingarray objectAtIndex:indexPath.row]];//used to open
            
        }
        
        
    }
    
    
    
}
-(void)openHomeDetailVC:(NSDictionary*)cellData
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.dic_serviceInfo=cellData;
    vc.tagComeFrom=@"ADVANCESEARCH";
    
    vc.sourceTab     = @"search";
    vc.sourceState   = @"";
    vc.sourceSection = @"";
    vc.sourceBanner  = @"";
    
    [self presentViewController:vc animated:NO completion:nil];
}

- (void) didSelectItem:(NSDictionary*)cellData
{
   
    SharedManager  *singleton = [SharedManager sharedSingleton];

    if(singleton.user_tkn.length==0)
    {
        NSString *serviceId = [cellData valueForKey:@"SERVICE_ID"];
        NSString *service_type =[singleton.dbManager getInfoFlagServiceCardType:serviceId];
      // not login
        if (![[service_type uppercaseString] isEqualToString:@"I"])
        {
            
            [self showLoginSingupActionSheet];
            
        }
        else
        {
            [self openHomeDetailVC:cellData];
        }
            
    }
    else
    {
        // user is login
        [self openHomeDetailVC:cellData];

    }
    
   
  /*  if (cellData)
    {
        //SERVICE_CARD_TYPE
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        vc.dic_serviceInfo=cellData;
        vc.tagComeFrom=@"ADVANCESEARCH";

        vc.sourceTab     = @"search";
        vc.sourceState   = @"";
        vc.sourceSection = @"";
        vc.sourceBanner  = @"";
        
        [self presentViewController:vc animated:NO completion:nil];
        
    }
   */
}


//=====================================
//=====================================
//=====================================
//=====================================

-(void)showLoginSingupActionSheet{
    //"You need to be logged in to use this service"
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[NSLocalizedString(@"need_login", nil) capitalizedString] preferredStyle:UIAlertControllerStyleActionSheet];
    
    //"cancel"
    __block typeof(self) weakself = self;
    UIAlertAction *alertActionCancel = [UIAlertAction actionWithTitle:[NSLocalizedString(@"cancel", nil) capitalizedString] style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *alertActionLogin = [UIAlertAction actionWithTitle:[NSLocalizedString(@"login_caps", nil) capitalizedString] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                       
                                       {
                                           //
                                           [weakself loginBtnAction];
                                           
                                       }];
    UIAlertAction *alertActionRegister = [UIAlertAction actionWithTitle:[NSLocalizedString(@"register_caps", nil) capitalizedString] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                          
                                          {
                                              //
                                              [weakself registerBtnAction];
                                              
                                          }];
    
    [alertController addAction:alertActionLogin];
    [alertController addAction:alertActionRegister];
    [alertController addAction:alertActionCancel];
    
    
    // UIViewController *vc1=[self topMostController];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:alertController];
        
        [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)inView:self.view permittedArrowDirections:0 animated:NO];
        
    }
    else
    {
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    
    
}



#pragma mark - Button Action Methods
- (void)loginBtnAction
{
    LoginAppVC *vc;
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    vc.hidesBottomBarWhenPushed = YES;
    vc.sender = @"information";
    UINavigationController *NaviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    NaviVC.hidesBottomBarWhenPushed = YES;
    [NaviVC setNavigationBarHidden:true];
    [self presentViewController:NaviVC animated:false completion:nil];
    //[self.navigationController pushViewController:vc animated:YES];
}

- (void)registerBtnAction
{
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
     
     MobileRegistrationVC *mobileReg;
     
     if ([[UIScreen mainScreen]bounds].size.height == 1024)
     {
     mobileReg = [[MobileRegistrationVC alloc] initWithNibName:@"MobileRegistrationVC_iPad" bundle:nil];
     }
     else
     {
     mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegistrationVC"];
     }
     */
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Flagship" bundle:nil];
    MobileRegister_FlagVC *mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegister_FlagVC"];
    
    mobileReg.hidesBottomBarWhenPushed = YES;
    //[self.navigationController pushViewController:mobileReg animated:YES];
    UINavigationController *NaviVC = [[UINavigationController alloc] initWithRootViewController:mobileReg];
    NaviVC.hidesBottomBarWhenPushed = YES;
    [NaviVC setNavigationBarHidden:true];
    [self presentViewController:NaviVC animated:false completion:nil];
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

//=====================================
//=====================================
//=====================================
//=====================================











-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //--------fix lint------------------
    static NSString *identifier = @"AutoCompleteRowIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    //--------fix lint------------------
    
    
    
    //-------- Handling of table that shows drop down results and recent history 5 results
    if(tableView.tag==TAGAUTOCOMPTBL) //autocomplete server key
    {
        UITableViewCell *cell = nil;
        static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
        }
        
        
        //cell.textLabel.text = @"Autocomplete Case Default";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //======================== Start Offline code 6 april 2018 ======================
        AppDelegate *appD = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        NSString*suggestions=@"";
        if (appD.networkStatus==false)
        {
            suggestions=[NSString stringWithFormat:@"%@", [[autocompleteService objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"]];

            
        }
        else
            
        {
         suggestions=[NSString stringWithFormat:@"%@", [[autocompleteService objectAtIndex:indexPath.row] valueForKey:@"SERVICE_ALISES"]];
        }
        //======================== End Offline code 6 april 2018 ======================

        //uncomment below line for old code and comment above
       // suggestions=[NSString stringWithFormat:@"%@", [[autocompleteService objectAtIndex:indexPath.row] valueForKey:@"SERVICE_ALISES"]];

        
        
        @try {
            NSString *trimmedString = [suggestions stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]];
            cell.textLabel.text =trimmedString;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        cell.textLabel.font = [AppFont regularFont:14.0];
        
        cell.textLabel.textColor=[UIColor blackColor];
        
        cell.textLabel.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        UIImage *imgSearchResult = [UIImage imageNamed:@"icon_search_result"];
        
        cell.imageView.image = nil;
        cell.accessoryView = nil;
        
        if (singleton.isArabicSelected) {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:imgSearchResult];
            cell.accessoryView = imageView;
        }
        else{
            cell.imageView.image= imgSearchResult;
        }
        
        
        
        
        return cell;
        
    }
    
    //-------- Handling of table that shows results with results found count-------------
    
    else if(tableView.tag==TAGSEARCHRESULTTBL)
    {
        static NSString *CellIdentifier2 = @"FavouriteCellSearch";
        
        static NSString *cellIdent = @"newSearchCell";
        
        NSString *serviceTYPE = [[arr_results objectAtIndex:indexPath.row] valueForKey:@"CATELOG_TYPE"];
        
        
        if ([[serviceTYPE uppercaseString] isEqualToString:@"S"])
        {
            NewSearchCell *cell = (NewSearchCell *)[tableView dequeueReusableCellWithIdentifier:cellIdent];
            if (!cell)
            {
                cell = [[NewSearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdent];
            }
            
            
            
            cell.lbl_servicetitle.text=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"];
            
            cell.lbl_servicedesc.text=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"dept_description"];
            
            
            cell.lbl_servicedesc.lineBreakMode = NSLineBreakByWordWrapping;
            cell.lbl_servicedesc.numberOfLines =0;
            
            // cell.lbl_category.text=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"CATEGORY_NAME"];
            
            
            
            // Code to show complete category label text Start
            
            cell.lbl_category.backgroundColor = [UIColor clearColor];
            cell.lbl_category.textColor=[UIColor whiteColor];
            cell.lbl_category.layer.cornerRadius = 10.0;
            
           // NSString *multi_Category = [[arr_results objectAtIndex:indexPath.row] valueForKey:@"CATEGORY_NAME"];
            NSString *multi_Category = [[arr_results  objectAtIndex:indexPath.row] valueForKey:@"MULTI_CATEGORY_NAME"];
            
            
            // MULTI_CATEGORY_NAME
            NSMutableArray *arrCat = [[NSMutableArray alloc] init];
            /*NSArray *arr = [category componentsSeparatedByString:@","];
             for (NSString *cat in arr) {
             [arrCat addObject:cat];
             }*/
            //[arrCat addObject:category];
            if ([multi_Category containsString:@","]) {
                NSArray *arrMulti = [multi_Category componentsSeparatedByString:@","];
                [arrCat addObjectsFromArray:arrMulti];
            }else {
                if (multi_Category.length!=0) {
                    [arrCat addObject:multi_Category];

                }
            }
            
            
            
            
            /*NSArray *arr = [category componentsSeparatedByString:@","];
             for (NSString *cat in arr) {
             [arrCat addObject:cat];
             }*/
            //[arrCat addObject:category];
            NSString *stateId = [[arr_results  objectAtIndex:indexPath.row] valueForKey:@"STATE"];
            NSString *stateName = [obj getStateName:stateId];
            if (stateId.length == 0 || [stateId isEqualToString:@"99"])
            {
                stateName =  NSLocalizedString(@"central", @"");
            }
            NSMutableArray *arrState = [[NSMutableArray alloc] initWithObjects:stateName, nil];
            NSMutableArray *otherStatesNames = [[NSMutableArray alloc] init] ;
            if (stateId.length != 0 && ![stateId isEqualToString:@"99"])
            {
                NSString *otherStateId = [[arr_results  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_OTHER_STATE"];
              /*  if (otherStateId.length != 0) {
                    NSString *newString = [otherStateId substringToIndex:[otherStateId length]-1];
                    NSString *finalString = [newString substringFromIndex:1];
                    if (finalString.length != 0)
                    {
                        NSArray *otherStates = [finalString componentsSeparatedByString:@"|"];
                        NSLog(@"%@",otherStates);
                        for (NSString *stateIDS in otherStates) {
                            [otherStatesNames addObject: [obj getStateName:stateIDS]];
                        }
                    }
                }*/
                
                
                if (otherStateId != nil && otherStateId.length != 0) {
                    
                    //if else is added to parse value without | seprated
                    if ([otherStateId containsString:@"|"])
                    {
                        NSString *newString = [otherStateId substringToIndex:[otherStateId length]-1];
                        NSString *finalString = [newString substringFromIndex:1];
                        if (finalString.length != 0)
                        {
                            NSArray *otherStates = [finalString componentsSeparatedByString:@"|"];
                            NSLog(@"%@",otherStates);
                            for (NSString *stateIDS in otherStates) {
                                [otherStatesNames addObject: [obj getStateName:stateIDS]];
                            }
                        }
                    }
                    else
                    {
                        // else is added to parse value  , comma seprated string of other states
                        if (otherStateId.length != 0)
                        {
                            NSArray *otherStates = [otherStateId componentsSeparatedByString:@","];
                            NSLog(@"%@",otherStates);
                            for (NSString *stateIDS in otherStates) {
                                [otherStatesNames addObject: [obj getStateName:stateIDS]];
                            }
                        }
                        //  [otherStatesNames addObject: [obj getStateName:otherStateId]];
                    }
                    
                    NSLog(@"otherStatesNames======>%@",otherStatesNames);
                }
                
            }
            if ([otherStatesNames containsObject:stateName]) {
                [otherStatesNames removeObject:stateName];
            }
            if (otherStatesNames.count != 0) {
                [arrState addObjectsFromArray:otherStatesNames];
            }
            
            CGFloat fontSize = 12.0;
            
            NSString *deptName = [[arr_results objectAtIndex:indexPath.row] valueForKey:@"DEPT_NAME"];
            cell.lbl_deptName.text = deptName;
            cell.lbl_deptName.numberOfLines = 0;
            cell.lbl_deptName.lineBreakMode = NSLineBreakByWordWrapping;
            
            [cell addCategoryStateItemsToScrollView:arrCat state:arrState withFont:fontSize cell:cell andTagIndex:indexPath];
            
            CGRect frameScroll = cell.scrollView_nouse.frame;
            frameScroll.size.width = tableView.frame.size.width - 115;
            
            cell.scrollView_nouse.frame = frameScroll;
            cell.lbl_rating.text=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"RATING"];
            
            NSURL *url=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"IMAGE"];
            
            [cell.img_service sd_setImageWithURL:url
                                placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.lbl_servicetitle.font = [AppFont semiBoldFont:16.0];
            cell.lbl_servicedesc.font = [AppFont lightFont:14.0];
            cell.lbl_deptName.font = [AppFont lightFont:14.0];
            cell.lbl_rating.font = [AppFont regularFont:13.0];
            
            return cell;
        }
        else
        {
            FavouriteCell *cell = (FavouriteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
            if (!cell)
            {
                cell = [[FavouriteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2];
            }
            
            cell.btn_serviceFav.hidden = YES;
            cell.btn_moreInfo.hidden = YES;
            
            cell.lbl_serviceName.text=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"];
            
            cell.lbl_serviceDesc.text=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"dept_description"];
            
            
            cell.lbl_serviceDesc.lineBreakMode = NSLineBreakByWordWrapping;
            cell.lbl_serviceDesc.numberOfLines =0;
            
            
            //cell.lbl_serviceCateg.backgroundColor = [UIColor clearColor];
            //cell.lbl_serviceCateg.textColor=[UIColor whiteColor];
           // cell.lbl_serviceCateg.layer.cornerRadius = 10.0;
          /*  NSString *category = [[arr_results objectAtIndex:indexPath.row] valueForKey:@"CATEGORY_NAME"];
            NSMutableArray *arrCat = [[NSMutableArray alloc] init];
            NSArray *arr = [category componentsSeparatedByString:@","];
             for (NSString *cat in arr) {
             [arrCat addObject:cat];
             }
            [arrCat addObject:category];
            
            NSString *stateId = [[arr_results  objectAtIndex:indexPath.row] valueForKey:@"STATE"];
            NSString *stateName = [obj getStateName:stateId];
            if (stateId.length == 0 || [stateId isEqualToString:@"99"])
            {
                stateName =  NSLocalizedString(@"central", @"");
            }
            NSMutableArray *arrState = [[NSMutableArray alloc] initWithObjects:stateName, nil];
            NSMutableArray *otherStatesNames = [[NSMutableArray alloc] init] ;
            if (stateId.length != 0 && ![stateId isEqualToString:@"99"])
            {
                NSString *otherStateId = [[arr_results  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_OTHER_STATE"];
                if (otherStateId.length != 0) {
                    NSString *newString = [otherStateId substringToIndex:[otherStateId length]-1];
                    NSString *finalString = [newString substringFromIndex:1];
                    if (finalString.length != 0)
                    {
                        NSArray *otherStates = [finalString componentsSeparatedByString:@"|"];
                        NSLog(@"%@",otherStates);
                        for (NSString *stateIDS in otherStates) {
                            [otherStatesNames addObject: [obj getStateName:stateIDS]];
                        }
                    }
                }
                
            }
            if ([otherStatesNames containsObject:stateName]) {
                [otherStatesNames removeObject:stateName];
            }
            if (otherStatesNames.count != 0) {
                [arrState addObjectsFromArray:otherStatesNames];
            }
            CGFloat fontSize = 14.0;
            [cell addCategoryStateItemsToScrollView:arrCat state:arrState withFont:fontSize cell:cell andTagIndex:indexPath];
           */
           // CGRect frameScroll = cell.scrollView_nouse.frame;
            //frameScroll.size.width = tableView.frame.size.width - 115;
           // cell.scrollView_nouse.frame = frameScroll;//CGRectMake(CGRectGetMaxX(lbl_rating.frame) + 8, CGRectGetMinY(lbl_serviceCateg.frame), fDeviceWidth - 150, 30)];
        
            [cell stateNameWithCategory:[arr_results objectAtIndex:indexPath.row] ofCell:cell andTagIndex:indexPath];
            cell.lbl_rating.text=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"RATING"];
            
            NSURL *url=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"IMAGE"];
            
            [cell.img_serviceCateg sd_setImageWithURL:url
                                     placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            cell.lbl_serviceName.font = [AppFont semiBoldFont:16.0];
            cell.lbl_serviceDesc.font = [AppFont lightFont:14.0];
            cell.lbl_rating.font = [AppFont regularFont:13.0];
            
            return cell;
        }
        
    }
    
    
    /*NSString *deptName = [[arr_results objectAtIndex:indexPath.row] valueForKey:@"DEPT_NAME"];
     
     NSString *deptDesc = [[arr_results objectAtIndex:indexPath.row] valueForKey:@"dept_description"];
     
     NSString *combinedString = [NSString stringWithFormat:@"%@\n%@",deptName,deptDesc];
     
     cell.lbl_serviceDesc.text = combinedString;
     
     CGRect serviceDescFrame = cell.lbl_serviceDesc.frame;
     serviceDescFrame.size.height = 70;
     
     cell.lbl_serviceDesc.frame = serviceDescFrame;
     
     
     CGRect ratingFrame = cell.lbl_rating.frame;
     
     ratingFrame.origin.y = serviceDescFrame.origin.y + serviceDescFrame.size.height + 5;
     
     cell.lbl_rating.frame = ratingFrame;
     
     
     
     CGRect starFrame = cell.img_star.frame;
     
     starFrame.origin.y = serviceDescFrame.origin.y + serviceDescFrame.size.height + 5;
     
     cell.img_star.frame = starFrame;
     
     
     CGRect categoryFrame = cell.lbl_serviceCateg.frame;
     
     categoryFrame.origin.y = serviceDescFrame.origin.y + serviceDescFrame.size.height + 5;
     
     cell.lbl_serviceCateg.frame = categoryFrame;*/
    
    
    //-------- Handling of table that shows number of filters-------------
    
    else if (tableView.tag==TAGFILTERSTBL) //autocomplete server key
    {
        UITableViewCell *cell = nil;
        static NSString *ApplyFilterRowIdentifier = @"ApplyFilterRowIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:ApplyFilterRowIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ApplyFilterRowIdentifier];
        }
        
        
        // cell.textLabel.text = @"Filter Case Default";
        
        cell.textLabel.text = [_arr_filter objectAtIndex:indexPath.row];
        cell.textLabel.font=[UIFont systemFontOfSize:13];
        
        cell.textLabel.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        cell.textLabel.numberOfLines = 0;
        //cell.textLabel.minimumFontSize = 10;
        //cell.textLabel.adjustsFontSizeToFitWidth = YES;
        // cell.textLabel.textColor=[UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font=[AppFont regularFont:11];
        
        return cell;
        
    }
    
    else if(tableView.tag==TAGAUTORECENT)
        //show history if no type selected
    {
        static NSString *CellIdentifier = @"recentTrendCell";
        recentTrendCell *cell = (recentTrendCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        // Configure cell
        if (!cell)
        {
            cell = [[recentTrendCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        if (indexPath.section==0) {
            
            
            //cell.lbl_text.text = [[self.recentArray objectAtIndex:indexPath.row] valueForKey:@"SERVICE_ALISES"];
            
            
            cell.lbl_text.text = [self.recentArray objectAtIndex:indexPath.row];
            
            cell.lbl_text.textColor=[UIColor blackColor];
            
            cell.img_right_icon.image = nil;
            cell.img_icon.image = nil;
            
            if( singleton.isArabicSelected){
                cell.img_right_icon.image=[UIImage imageNamed:@"icon_search_recent"];
            }
            else{
                cell.img_icon.image=[UIImage imageNamed:@"icon_search_recent"];
            }
            
        }
        else
        {
            
            
            
            // cell.lbl_text.text=@"trend";
            cell.lbl_text.text = [[self.trendingarray objectAtIndex:indexPath.row] valueForKey:@"alises"];
            cell.lbl_text.textColor=[UIColor blackColor];
            //            cell.img_icon.image=[UIImage imageNamed:@"icon_trending.png"];
            
            cell.img_right_icon.image = nil;
            cell.img_icon.image = nil;
            
            if( singleton.isArabicSelected){
                cell.img_right_icon.image=[UIImage imageNamed:@"icon_trending.png"];
            }
            else{
                cell.img_icon.image=[UIImage imageNamed:@"icon_trending.png"];
            }
            
            
        }
        
        //cell.textLabel.text = @"Autocomplete Case Default";
        
        
        
        if (singleton.isArabicSelected) {
            cell.lbl_text.textAlignment = NSTextAlignmentRight;
            
            
        }
        else{
            cell.lbl_text.textAlignment = NSTextAlignmentLeft;
        }
        
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lbl_text.font = [AppFont regularFont:17.0];
        
        return cell;
        
        
        
        
    }
    //  return nil;
    
    
    return cell;
    //--------fix lint------------------
    
    
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell1 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell1 isKindOfClass:[FavouriteCell class]])
    {
        FavouriteCell *favCell = (FavouriteCell*)cell1;
        
        
      //  dispatch_async(dispatch_get_main_queue(), ^{
            
            //            favCell.lblName.font = [UIFont systemFontOfSize:16.0];
            //            favCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            //            [favCell setNeedsDisplay];
            //        });
            
            //  }
            
            
            CGRect buttonImageAdvanceFrame =  favCell.img_serviceCateg.frame;
            buttonImageAdvanceFrame.origin.x = singleton.isArabicSelected ?CGRectGetWidth(tableView.frame) - 62 : 12;
            buttonImageAdvanceFrame.size.width = 55;
            buttonImageAdvanceFrame.size.height = 55;
            buttonImageAdvanceFrame.origin.y = favCell.frame.size.height/2 - buttonImageAdvanceFrame.size.height/2;
            favCell.img_serviceCateg.frame = buttonImageAdvanceFrame;
            
            
            CGRect lblServiceTitle =  favCell.lbl_serviceName.frame;
            lblServiceTitle.origin.x = singleton.isArabicSelected ? 10  : 80;
            lblServiceTitle.size.width = tableView.frame.size.width - 100;
            favCell.lbl_serviceName.frame = lblServiceTitle;
            favCell.lbl_serviceName.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            
            CGRect lbl_servicedescFrame =  favCell.lbl_serviceDesc.frame;
            lbl_servicedescFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
            lbl_servicedescFrame.size.width = tableView.frame.size.width - 100;
            favCell.lbl_serviceDesc.frame = lbl_servicedescFrame;
            favCell.lbl_serviceDesc.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            CGRect lblRatingFrame =  favCell.lbl_rating.frame;
            lblRatingFrame.origin.x = singleton.isArabicSelected ? tableView.frame.size.width - 95  : 80;
            favCell.lbl_rating.frame = lblRatingFrame;
            //        cell.lbl_rating.backgroundColor = [UIColor redColor];
            
            CGRect starImageFrame =  favCell.img_star.frame;
            starImageFrame.origin.x = singleton.isArabicSelected ? lblRatingFrame.origin.x -20  : lblRatingFrame.origin.x +lblRatingFrame.size.width -8;
            favCell.img_star.frame = starImageFrame;
            
            
            
            
            CGRect categoryLabelFrame =  favCell.lbl_serviceCateg.frame;
            categoryLabelFrame.origin.x = singleton.isArabicSelected ? starImageFrame.origin.x - 90  : starImageFrame.origin.x + 30 ;
            favCell.lbl_serviceCateg.frame = categoryLabelFrame;
            
            
            CGRect btnFavFrame =  favCell.btn_serviceFav.frame;
            btnFavFrame.origin.x = singleton.isArabicSelected ? 10  : tableView.frame.size.width - 30 ;
            favCell.btn_serviceFav.frame = btnFavFrame;
            
            CGRect btnMoreFrame =  favCell.btn_moreInfo.frame;
            btnMoreFrame.origin.x = singleton.isArabicSelected ? 5  : tableView.frame.size.width - 36 ;
            favCell.btn_moreInfo.frame = btnMoreFrame;
       // });
    }
    
    
    if ([cell1 isKindOfClass:[NewSearchCell class]])
    {
        NewSearchCell *favCell = (NewSearchCell*)cell1;
        
        
      //  dispatch_async(dispatch_get_main_queue(), ^{
            
            //            favCell.lblName.font = [UIFont systemFontOfSize:16.0];
            //            favCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            //            [favCell setNeedsDisplay];
            //        });
            
            //  }
            
            
            CGRect buttonImageAdvanceFrame =  favCell.img_service.frame;
            buttonImageAdvanceFrame.origin.x = singleton.isArabicSelected ?CGRectGetWidth(tableView.frame) - 62 : 12;
            buttonImageAdvanceFrame.size.width = 55;
            buttonImageAdvanceFrame.size.height = 55;
        buttonImageAdvanceFrame.origin.y = favCell.frame.size.height/2 - buttonImageAdvanceFrame.size.height/2;
            favCell.img_service.frame = buttonImageAdvanceFrame;
            
            
            CGRect lblServiceTitle =  favCell.lbl_servicetitle.frame;
            lblServiceTitle.origin.x = singleton.isArabicSelected ? 10  : 80;
            lblServiceTitle.size.width = tableView.frame.size.width - 100;
            favCell.lbl_servicetitle.frame = lblServiceTitle;
            favCell.lbl_servicetitle.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            
            CGRect lbl_serviceDeptName =  favCell.lbl_deptName.frame;
            lbl_serviceDeptName.origin.x = singleton.isArabicSelected ? 10  : 80;
            lbl_serviceDeptName.size.width = tableView.frame.size.width - 100;
            lbl_serviceDeptName.origin.y = lblServiceTitle.origin.y + lblServiceTitle.size.height + 2;
            
            //lbl_serviceDeptName.size.height = favCell.lbl_servicedesc.frame.size.height-10;
            
            favCell.lbl_deptName.frame = lbl_serviceDeptName;
            favCell.lbl_deptName.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            
            CGRect lbl_servicedescFrame =  favCell.lbl_servicedesc.frame;
            lbl_servicedescFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
            lbl_servicedescFrame.size.width = tableView.frame.size.width - 100;
            lbl_servicedescFrame.origin.y = lbl_serviceDeptName.origin.y + lbl_serviceDeptName.size.height + 2;
            favCell.lbl_servicedesc.frame = lbl_servicedescFrame;
            favCell.lbl_servicedesc.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            CGRect lblRatingFrame =  favCell.lbl_rating.frame;
            lblRatingFrame.origin.x = singleton.isArabicSelected ? tableView.frame.size.width - 95  : 80;
            
            lblRatingFrame.origin.y = lbl_servicedescFrame.origin.y + lbl_servicedescFrame.size.height + 5;
            
            favCell.lbl_rating.frame = lblRatingFrame;
            //        cell.lbl_rating.backgroundColor = [UIColor redColor];
            
            CGRect starImageFrame =  favCell.img_star.frame;
        starImageFrame.origin.x = singleton.isArabicSelected ? lblRatingFrame.origin.x -20  : lblRatingFrame.origin.x+ 20; // +lblRatingFrame.size.width -12;
            
            starImageFrame.origin.y = lblRatingFrame.origin.y + 4;
            favCell.img_star.frame = starImageFrame;
            
            
            
            
            CGRect categoryLabelFrame =  favCell.lbl_category.frame;
            categoryLabelFrame.origin.x = singleton.isArabicSelected ? starImageFrame.origin.x - 90  : starImageFrame.origin.x + 20 ;
            
            categoryLabelFrame.origin.y = lblRatingFrame.origin.y;
            
            favCell.lbl_category.frame = categoryLabelFrame;
            
            CGRect scrollFrame =  favCell.scrollView_nouse.frame;
            scrollFrame.origin.y = favCell.lbl_category.frame.origin.y-2;
            scrollFrame.origin.x = scrollFrame.origin.x - 5;
            favCell.scrollView_nouse.frame = scrollFrame;
        //});
    }
}


-(void)reloadTableContents
{
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [tbl_search reloadData];
    }];
//    [tbl_search setNeedsLayout ];
//    [tbl_search layoutIfNeeded ];
//    [tbl_search reloadData];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag==TAGSEARCHRESULTTBL)
    {
        /*NSString *serviceTYPE=[[arr_results objectAtIndex:indexPath.row] valueForKey:@"CATELOG_TYPE"];
         
         if ([[serviceTYPE uppercaseString] isEqualToString:@"S"])
         {
         
         NSString *deptName = [[arr_results objectAtIndex:indexPath.row] valueForKey:@"DEPT_NAME"];
         
         NSString *deptDesc = [[arr_results objectAtIndex:indexPath.row] valueForKey:@"dept_description"];
         
         NSString *combinedString = [NSString stringWithFormat:@"%@\n%@",deptName,deptDesc];
         
         
         CGSize constraintSize = {tableView.frame.size.width, 20000}; //230 is cell width & 20000 is max height for cell
         CGSize neededSize = [[NSString stringWithFormat:@"%@",combinedString] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:constraintSize  lineBreakMode:NSLineBreakByWordWrapping];
         
         
         return MAX(130, neededSize.height +33);
         
         }
         else
         {
         return 105.0;
         }*/
        
        UITableViewCell* cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
        
        if ([cell.reuseIdentifier isEqualToString:@"newSearchCell"])
        {
            return 130;
        }
        
        return 105.0;
    }
    
    if(tableView.tag==TAGAUTORECENT)
    {
        return 44.0f;
    }
    else
    {
        
        return 35.0f;
        
    }
    
}





-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    //[textField resignFirstResponder];
    
    [self.view endEditing:YES];
    autocompleteTableView.hidden=TRUE;
    tbl_search.hidden=TRUE;
    lbl_results.hidden = TRUE;
    
    //-------- Recent Trend service----------
    self.tbl_recentrend.hidden=FALSE;
    self.tbl_recentrend.tag=TAGAUTORECENT;
    //-------- Recent Trend service----------
    
    [self refreshData];
    
    
    return NO;
}



-(IBAction)cancelBtnAction:(id)sender
{
    
    
    if(iPhoneX())
    {
        
        nvSearchView.txtSearchView.text=@"";

    }
    else
    {
        txt_search.text=@"";

        
    }
    
    
   // [txt_search resignFirstResponder];
    [self.view endEditing:YES];

    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
    
    
}




- (IBAction)btn_category:(UIButton *)sender
{
    
    filterTag=3;
    
    // _vw_tbladvance.frame=CGRectMake(fDeviceWidth-130, 110, 130, 185);
    
    xCord=fDeviceWidth-160;
    yCord=110;
    width=160;
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        xCord = fDeviceWidth - sender.frame.size.width;
        yCord = 110;
        width = sender.frame.size.width;
    }
    
    
    
    header_title_pass=@"Service Category";
    arr_table_pass=[[NSMutableArray alloc]init];
    
    //NSArray *arrService = [singleton.dbManager loadServiceCategory];
    
    NSMutableArray *tempServiceArr = [NSMutableArray new];
    
    if(singleton.user_tkn.length==0)
    {
        SharedManager  *singleton = [SharedManager sharedSingleton];
        
        tempServiceArr = [[singleton.dbManager loadServiceCategoryFlagShip] mutableCopy];

     
        
    }
    else
    {
        tempServiceArr = [[singleton.dbManager loadServiceCategory] mutableCopy];
       // NSArray *arrService = [singleton.dbManager loadServiceCategory];

    }
    
    NSArray *arrService = [NSArray arrayWithArray:tempServiceArr];
   // loadServiceCategoryFlagShip
    for (int i = 0; i< arrService.count; i++) {
        NSDictionary *dict = arrService [i];
        NSLog(@"SERVICE CATEOGRY = %@",[dict valueForKey:@"SERVICE_CATEGORY"]);
        [arr_table_pass addObject:[dict valueForKey:@"SERVICE_CATEGORY"]];
    }
    
    
    [_arr_filter removeAllObjects];
    
    
    _arr_filter=arr_table_pass;
    
    // [_arr_filter addObject:NSLocalizedString(@"all", nil)];
    
    [_arr_filter insertObject:NSLocalizedString(@"all", nil) atIndex:0];
    
    
    [self callCustomPicker];
    
}

- (IBAction)btn_state:(UIButton *)sender
{
    
    filterTag=2;
    //_vw_tbladvance.frame=CGRectMake(fDeviceWidth/2-100, 110, 175, 185);
    
    xCord=fDeviceWidth/2-88;
    yCord=110;
    width=176;
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        xCord = fDeviceWidth/2 - sender.frame.size.width/2;
        yCord = 110;
        width = sender.frame.size.width;
    }
    
    header_title_pass=NSLocalizedString(@"states", nil);
    
    NSArray*arry=[obj getStateList];
    arry = [arry sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    arr_table_pass=[[NSMutableArray alloc]init];
    
    arr_table_pass=[arry mutableCopy];
    
    
    
    [_arr_filter removeAllObjects];
    
    _arr_filter=arr_table_pass;
    //  [_arr_filter addObject:NSLocalizedString(@"all", nil)];
    [_arr_filter insertObject:NSLocalizedString(@"all", nil) atIndex:0];
    
    [self callCustomPicker];
    
}


- (IBAction)btn_servicetype:(UIButton *)sender
{
    
    // [txt_search resignFirstResponder];
    [self.view endEditing:YES];
    filterTag=1;
    
    
    xCord=0;
    yCord=110;
    width=130;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        width = sender.frame.size.width;
    }
    
    //_vw_tbladvance.frame=CGRectMake(10, 110, 107, 115);
    
    header_title_pass=@"Service Type";
    
    arr_table_pass=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"all", nil),NSLocalizedString(@"central", nil),NSLocalizedString(@"regional", nil), nil];
    
    
    [_arr_filter removeAllObjects];
    
    _arr_filter=arr_table_pass;
    
    [self callCustomPicker];
    
}

-(void)callCustomPicker
{
    
    
    if (onFilter ==TRUE) {
        
        _vw_tbladvance.hidden=FALSE;//hide tableview
        
        
        onFilter=FALSE;
        
    }
    else
    {
        
        
        _vw_tbladvance.hidden=TRUE;//hide tableview
        
        onFilter=TRUE;
        
    }
    // [txt_search resignFirstResponder];
    [self.view endEditing:YES];
    autocompleteTableView.hidden = YES;
    
    _tbl_filter.tag=TAGFILTERSTBL;
    CGFloat height= 35 *[_arr_filter count];
    NSLog(@"height=%f",height);
    if (height>=fDeviceHeight)
    {
        height=fDeviceHeight-200;
    }
    NSLog(@"height after=%f",height);
    
    _vw_tbladvance.frame=CGRectMake(xCord, yCord, width, height+10);
    //_tbl_filter.frame=CGRectMake(xCord, yCord, width, height);
    
    
    [autocompleteTableView bringSubviewToFront:_vw_tbladvance];
    
    
    [_tbl_filter reloadData];
    
}








-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField.returnKeyType==UIReturnKeyDefault)
    {
        //Your Return Key code
        autocompleteTableView.hidden = YES;
        tbl_search.hidden = NO;
        
        // [textField resignFirstResponder];
        [self.view endEditing:YES]; //end editing to hide keybaord

    }
    else if(textField.returnKeyType==UIReturnKeySearch)
    {
        //Your search key code
        
        NSLog(@"Hit api here for other=%@",textField.text);
        
        tbl_search.hidden = NO;
        
        NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if([text length]>0)
            
        {
            [self hitNewappsearchAPI:textField.text];
            
            
            /*
             {
             "SERVICE_ALISES" = "cbse center";
             "SERVICE_ID" = 10;
             }
             
             */
            // Clean up U
            if (![singleton.arr_recent_Searchservice containsObject:textField.text])
            {
                [singleton.arr_recent_Searchservice addObject:textField.text];
            }
            
            
        }
    }
    
    
    return YES;
}


/*{
 fl = "";
 fq = "";
 mno = 7055241833;
 peml = "";
 q = "case* AND an*";
 tkn = "2f2c34fe-0060-4e70-b66f-0d6c755a3c8a";
 wt = json;
 }
 */



-(void)hitOldappsearchAPI:(NSString*)substring
{
    
    // [txt_search resignFirstResponder];
    [self.view endEditing:YES];
    autocompleteTableView.tag=TAGAUTOCOMPTBL;
    tbl_search.hidden=NO;
    
    if ([substring length]>0)
    {
        autocompleteTableView.hidden=TRUE;
        NSString *SELECTED_STATE=[obj getStateCode:_lbl_state.text];
        if ([SELECTED_STATE isEqualToString:@"9999"]) {
            SELECTED_STATE=@"";
        }
        NSString *SELECTED_CAT=_lbl_category.text;
        if ([SELECTED_CAT isEqualToString:NSLocalizedString(@"all", nil)]) {
            SELECTED_CAT=@"";
        }
        
        
        /*     ALL *
         Service Type : Regional State ALL   ( AND -state:99)
         Central  with All  99
         */
        
        
        if ([_lbl_servicetype.text isEqualToString:NSLocalizedString(@"all", nil)] &&[_lbl_state.text isEqualToString:NSLocalizedString(@"all", nil)])
        {
            SELECTED_STATE=@"";
        }
        if ([_lbl_servicetype.text isEqualToString:NSLocalizedString(@"central", nil)] &&[_lbl_state.text isEqualToString:NSLocalizedString(@"all", nil)])
        {
            SELECTED_STATE=@"99";
        }
        
        
        
        
        
        // NSString *tempQueryStr=[substring stringByReplacingOccurrencesOfString:@" " withString:@"* AND "];
        //  NSString *q=[NSString stringWithFormat:@"%@*",tempQueryStr];
        
        NSString *trimmedString = [substring stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        NSString *q=[NSString stringWithFormat:@"%@",trimmedString];
        
        NSString *fq=@"";
        if ([_lbl_servicetype.text isEqualToString:NSLocalizedString(@"regional", nil)] &&[_lbl_state.text isEqualToString:NSLocalizedString(@"all", nil)])
        {
            SELECTED_STATE=@"99";
            
            fq=[NSString stringWithFormat:@"(category_name:%@* AND -state:%@*)",SELECTED_CAT,SELECTED_STATE];
            
        }
        else
        {
            fq=[NSString stringWithFormat:@"(category_name:%@* AND state:%@*)",SELECTED_CAT,SELECTED_STATE];
        }
        
        
        
        
        NSLog(@"%@",q);
        
        NSLog(@"%@",fq);
        
        UMAPIManager *objRequest = [[UMAPIManager alloc] init];
        NSMutableDictionary *dictBody = [NSMutableDictionary new];
        [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
        [dictBody setObject:q forKey:@"q"];
        [dictBody setObject:@"json" forKey:@"wt"];
        [dictBody setObject:fq forKey:@"fq"];
        [dictBody setObject:@"" forKey:@"fl"];
        [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
        [dictBody setObject:@"" forKey:@"peml"];
        
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        // Set the label text.
        hud.label.text = NSLocalizedString(@"loading",nil);
        
        
        
        
        // https://app.umang.gov.in/umang/coreapi/ws1/appsrsrch
        // TAG_REQUEST_APPSEARCH
        
        [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_APPSRSRCH withBody:dictBody andTag:TAG_REQUEST_APPSEARCH completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
            [hud hideAnimated:YES];
            
            if (error == nil) {
                NSLog(@"Server Response = %@",response);
                
                
                
                //----fix lint----
                // NSMutableArray *pdRes=[[NSMutableArray alloc]init];
                // pdRes=[[[response valueForKey:@"pd"] valueForKey:@"response"] valueForKey:@"docs"];
                
                
                
                NSMutableArray *pdRes = (NSMutableArray *)[[[response valueForKey:@"pd"] valueForKey:@"response"] valueForKey:@"docs"];
                
                NSLog(@"pdRes Search Result= %@",pdRes);
                // if ([pdRes count]>0) {
                
                
                lbl_results.hidden=FALSE;
                
                lbl_results.text=[NSString stringWithFormat:@"%@ results found",[[[response valueForKey:@"pd"] valueForKey:@"response"] valueForKey:@"numFound"]];
                
                
                [self loadtableviewResultsfromserver:pdRes];
                
                
                // }
            }
            else{
                NSLog(@"Error Occured = %@",error.localizedDescription);
            }
        }];
    }
    else
    {
        //do nothing
    }
}





-(void)loadtableviewResultsfromserver:(NSMutableArray*)resultsArr
{
    
    [arr_results removeAllObjects];
    
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",substring]; // if you need case sensitive search avoid '[c]' in the predicate
    NSMutableArray *arr_tempWebSearch=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[resultsArr count]; i++) {
        
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        
        NSString *service_id=[[resultsArr valueForKey:@"srid"] objectAtIndex:i];
        [dic setObject:service_id forKey:@"SERVICE_ID"];
        
        NSString *category_name=[[resultsArr valueForKey:@"category_name"] objectAtIndex:i];
        [dic setObject:category_name forKey:@"CATEGORY_NAME"];
        
        
        NSString *catlg_type=[[resultsArr valueForKey:@"catlg_type"] objectAtIndex:i];
        [dic setObject:catlg_type forKey:@"CATELOG_TYPE"];
        
        //CATELOG_TYPE
        
        NSString *dept_id=[[resultsArr valueForKey:@"dept_id"] objectAtIndex:i];
        [dic setObject:dept_id forKey:@"DEPT_ID"];
        
        
        
        
        NSString *dept_name=[[resultsArr valueForKey:@"dept_name"] objectAtIndex:i];
        [dic setObject:dept_name forKey:@"DEPT_NAME"];
        
        
        
        NSString *description=[[resultsArr valueForKey:@"dept_description"] objectAtIndex:i];
        [dic setObject:description forKey:@"dept_description"];
        
        
        
        
        NSString *image=[[resultsArr valueForKey:@"image"] objectAtIndex:i];
        [dic setObject:image forKey:@"IMAGE"];
        
        
        
        NSString *lang=[[resultsArr valueForKey:@"lang"] objectAtIndex:i];
        [dic setObject:lang forKey:@"LANG"];
        
        
        
        NSString *lat=[[resultsArr valueForKey:@"lat"] objectAtIndex:i];
        [dic setObject:lat forKey:@"LAT"];
        
        NSString *lon=[[resultsArr valueForKey:@"lon"] objectAtIndex:i];
        [dic setObject:lon forKey:@"LON"];
        
        
        NSString *rating=[[resultsArr valueForKey:@"rating"] objectAtIndex:i];
        [dic setObject:rating forKey:@"RATING"];
        
        
        
        
        NSString *service_name=[[resultsArr valueForKey:@"service_name"] objectAtIndex:i];
        [dic setObject:service_name forKey:@"SERVICE_NAME"];
        
        
        
        NSString *state=[[resultsArr valueForKey:@"state"] objectAtIndex:i];
        [dic setObject:state forKey:@"STATE"];
        
        
        
        NSString *sub_category_name=[[resultsArr valueForKey:@"sub_category_name"] objectAtIndex:i];
        [dic setObject:sub_category_name forKey:@"SUB_CATEGORY_NAME"];
        
        // Added new condition for other states show
        NSString *otherState=[[resultsArr valueForKey:@"other_states"] objectAtIndex:i];
        if(otherState.length==0||otherState==nil)
        {
            otherState=@"";
        }
        [dic setObject:otherState forKey:@"SERVICE_OTHER_STATE"];
        //SERVICE_OTHER_STATE END
        
        NSString *url=[[resultsArr valueForKey:@"url"] objectAtIndex:i];
        [dic setObject:url forKey:@"URL"];
        
        // need to add for search result case handling 
       // dept_type as SERVICE_CARD_TYPE
        NSString *dept_type=[[resultsArr valueForKey:@"dept_type"] objectAtIndex:i];
        [dic setObject:dept_type forKey:@"SERVICE_CARD_TYPE"];
        
        
        
        NSString *mcgid=[[resultsArr valueForKey:@"mcgid"] objectAtIndex:i];
        [dic setObject:mcgid forKey:@"MULTI_CATEGORY_ID"];
        
        
        NSString *mcgname=[[resultsArr valueForKey:@"mcgname"] objectAtIndex:i];
        [dic setObject:mcgname forKey:@"MULTI_CATEGORY_NAME"];
        
        
        
        
        [arr_tempWebSearch addObject:dic];//to add
        
    }
    
    
    
    //autocompleteService=[results mutableCopy];
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arr_tempWebSearch];
    arr_results = [[orderedSet array] mutableCopy];
    //[tbl_search reloadData];
    [self reloadTableContents];
}




-(void)hitNewappsearchAPI:(NSString*)substring
{
    // [txt_search resignFirstResponder];
    [self.view endEditing:YES];
    autocompleteTableView.tag=TAGAUTOCOMPTBL;
    tbl_search.hidden=NO;
    
    if ([substring length]>0)
    {
        autocompleteTableView.hidden=TRUE;
        NSString *SELECTED_STATE=[obj getStateCode:_lbl_state.text];
        if ([SELECTED_STATE isEqualToString:@"9999"]) {
            SELECTED_STATE=@"";
        }
        //NSString *SELECTED_CAT=_lbl_category.text;
        
        NSString *SELECTED_CAT = @"";
        if(singleton.user_tkn.length==0)
        {
         // SELECTED_CAT=[singleton.dbManager getFlagServiceCategoryId:_lbl_category.text];

            SELECTED_CAT=[singleton.dbManager getFlagServiceMultiCategoryID:_lbl_category.text];

        }
        else
        {
    //  SELECTED_CAT=[singleton.dbManager getServiceCategoryId:_lbl_category.text];
           SELECTED_CAT=[singleton.dbManager getMultiCategoryID:_lbl_category.text];

        }
        SELECTED_CAT=[NSString stringWithFormat:@"%@",SELECTED_CAT];
        
        //getServiceCategoryId
        
        
        if ([SELECTED_CAT isEqualToString:NSLocalizedString(@"all", nil)]) {
            SELECTED_CAT=@"";
        }
        
        
        
        
        
        if ([_lbl_category.text isEqualToString:NSLocalizedString(@"all", nil)]) {
            SELECTED_CAT=@"*";
        }
        
        
        /*     ALL *
         Service Type : Regional State ALL   ( AND -state:99)
         Central  with All  99
         */
        
        
        if ([_lbl_servicetype.text isEqualToString:NSLocalizedString(@"all", nil)] &&[_lbl_state.text isEqualToString:NSLocalizedString(@"all", nil)])
        {
            SELECTED_STATE=@"*";
        }
        if ([_lbl_servicetype.text isEqualToString:NSLocalizedString(@"central", nil)] &&[_lbl_state.text isEqualToString:NSLocalizedString(@"all", nil)])
        {
            SELECTED_STATE=@"99";
        }
        
        
        
        
        
        // NSString *tempQueryStr=[substring stringByReplacingOccurrencesOfString:@" " withString:@"* AND "];
        //  NSString *q=[NSString stringWithFormat:@"%@*",tempQueryStr];
        
        NSString *trimmedString = [substring stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        NSString *q=[NSString stringWithFormat:@"%@",trimmedString];
        
        NSString *fq=@"";
        
        
        NSString *selectedStateId;
        
        if ([_lbl_servicetype.text isEqualToString:NSLocalizedString(@"regional", nil)] &&[_lbl_state.text isEqualToString:NSLocalizedString(@"all", nil)])
        {
            SELECTED_STATE=@"99";
            
            fq=[NSString stringWithFormat:@"(cgid:%@ AND -state:%@)",SELECTED_CAT,SELECTED_STATE];
            
            selectedStateId = @"-99";
        }
        else
        {
            fq=[NSString stringWithFormat:@"(cgid:%@ AND state:%@)",SELECTED_CAT,SELECTED_STATE];
            
            selectedStateId = SELECTED_STATE;
        }
        
        // fq = "(cgid:4* AND state:*)";
        
        
        
        NSLog(@"%@",q);
        
        NSLog(@"%@",fq);
        
        UMAPIManager *objRequest = [[UMAPIManager alloc] init];
        NSMutableDictionary *dictBody = [NSMutableDictionary new];
        [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
        [dictBody setObject:q forKey:@"q"];
        [dictBody setObject:@"json" forKey:@"wt"];
        [dictBody setObject:fq forKey:@"fq"];
        //[dictBody setObject:@"" forKey:@"fl"];
        [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
        [dictBody setObject:@"" forKey:@"peml"];
        [dictBody setObject:selectedStateId forKey:@"st"];
        [dictBody setObject:SELECTED_CAT forKey:@"catid"];
        
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        // Set the label text.
        hud.label.text = NSLocalizedString(@"loading",nil);
        
        
        // https://app.umang.gov.in/umang/coreapi/ws1/appsrsrch
        // TAG_REQUEST_APPSEARCH
        
        [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_APPCATSRCH withBody:dictBody andTag:TAG_REQUEST_APPSEARCH completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
            [hud hideAnimated:YES];
            
            if (error == nil) {
                NSLog(@"Server Response = %@",response);
                
                //-- fix lint----
                // NSMutableArray *pdRes=[[NSMutableArray alloc]init];
                // pdRes=[[[response valueForKey:@"pd"] valueForKey:@"response"] valueForKey:@"docs"];
                
                
                NSMutableArray *pdRes = (NSMutableArray *)[[[response valueForKey:@"pd"] valueForKey:@"response"] valueForKey:@"docs"];
                NSLog(@"pdRes Search Result= %@",pdRes);
                // if ([pdRes count]>0) {
                
                
                lbl_results.hidden=FALSE;
                
                lbl_results.text=[NSString stringWithFormat:@"%@ %@",[[[response valueForKey:@"pd"] valueForKey:@"response"] valueForKey:@"numFound"],NSLocalizedString(@"result_found", nil)];
                
                
                [self loadtableviewResultsfromserver:pdRes];
                
                
                // }
            }
            else{
                NSLog(@"Error Occured = %@",error.localizedDescription);
            }
        }];
    }
    else
    {
        //do nothing
    }
}







-(void)hitKeySearchAPI:(NSString*)substring
{
    
    // NSString *tempQueryStr=[substring stringByReplacingOccurrencesOfString:@" " withString:@"* AND "];
    //  NSString *q=[NSString stringWithFormat:@"%@*",tempQueryStr];
    
    NSString *trimmedString = [substring stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    NSString *q=[NSString stringWithFormat:@"%@",trimmedString];
    
    
    NSLog(@"%@",q);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:q forKey:@"q"];  //KEYWORD to be searched required in response
    [dictBody setObject:@"json" forKey:@"wt"];  //Josn required in response
    [dictBody setObject:@"" forKey:@"fq"];  //filter query based in city or state etc e.g. (fq=(state:KL OR state: AM))
    [dictBody setObject:@"" forKey:@"fl"];  //fields required in response
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //token
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_APPKEYSEARCH withBody:dictBody andTag:TAG_REQUEST_APPKEYSEARCH completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            
            //---fix lint----
            //  NSMutableArray *pdRes=[[NSMutableArray alloc]init];
            //  pdRes=[[[response valueForKey:@"pd"] valueForKey:@"response"] valueForKey:@"docs"];
            
            
            NSMutableArray *pdRes = (NSMutableArray *)[[[response valueForKey:@"pd"] valueForKey:@"response"] valueForKey:@"docs"];
            
            NSLog(@"pdRes Search Result= %@",pdRes);
            /* if ([pdRes count]>0) {
             [self loadtableviewfromserver:pdRes];
             // lbl_results.hidden=FALSE;
             
             }
             */
            [self loadtableviewfromserver:pdRes];
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
        }
    }];
    
}




-(void)loadtableviewfromserver:(NSMutableArray*)resultsArr
{
    
    [autocompleteService removeAllObjects];
    
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",substring]; // if you need case sensitive search avoid '[c]' in the predicate
    NSMutableArray *arr_tempWebSearch=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[resultsArr count]; i++) {
        
        NSString *service_name=[NSString stringWithFormat:@"%@",[[resultsArr valueForKey:@"alises"] objectAtIndex:i]];
        
        if ([service_name length]>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            
            NSString *service_id=[NSString stringWithFormat:@"%@",[[resultsArr valueForKey:@"srid"] objectAtIndex:i]];
            
            [dic setObject:service_id forKey:@"SERVICE_ID"];
            
            [dic setObject:service_name forKey:@"SERVICE_ALISES"];//change to service name so can show it
            
            
            [arr_tempWebSearch addObject:dic];//to add
        }
        
        
        
    }
    
    //autocompleteService=[results mutableCopy];
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arr_tempWebSearch];
    autocompleteService = [[orderedSet array] mutableCopy];
    [autocompleteTableView reloadData];
    
    [self reloadTableContents];
    
}



- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    
    AppDelegate *appD = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    if (appD.networkStatus==false)
    {
        NSLog(@"NO Network");
        
        [autocompleteService removeAllObjects];
        
        // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",substring]; // if you need case sensitive search avoid '[c]' in the predicate
        
        
        ///NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SERVICE_NAME contains[c] %@",substring]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SERVICE_NAME beginswith[c] %@",substring]; // if you need case sensitive search avoid '[c]' in the predicate

        
        NSArray *results = [pastServices filteredArrayUsingPredicate:predicate];
        
        
        // NSString *webStringSearch=[NSString stringWithFormat:@"See results for %@",substring];
        //  [autocompleteService addObject:webStringSearch];//to add
        
        for (int i=0; i<[results count]; i++) {
            [autocompleteService addObject:[results objectAtIndex:i]];//to add
            
        }
        //autocompleteService=[results mutableCopy];
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:autocompleteService];
        autocompleteService = [[orderedSet array] mutableCopy];
        [autocompleteTableView reloadData];
    }
    
    else
    {
        NSLog(@"Network");
        
        NSUInteger length=[substring length];
        
        if (length>0) {
            
            NSLog(@"value of length=%lu",(unsigned long)length);
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // if(length==1||length%2==0)
                //{
                [ self hitKeySearchAPI:substring];
                // }
            });
            
            
        }
    }
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //For each section, you must return here it's label
    if(tableView.tag==TAGAUTORECENT)
    {
        if(section == 0)
        {
            if ([self.recentArray count]!=0)
            {
                
                return [NSLocalizedString(@"recent_search", nil) capitalizedString];
                
                
            }
            return @"";
            
        }
        else
        {
            if ([self.trendingarray count]!=0)
            {
                
                return [NSLocalizedString(@"trending_small", nil) capitalizedString];
                
                
            }
            return @"";
        }
    }
    else
        return @"";
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if(tableView.tag==TAGAUTORECENT)
    {
        if(section == 0)
        {
            if ([self.recentArray count]!=0) {
                return 44;
                
            }
            return 0.00001;
            
        }
        else
        {
            if ([self.trendingarray  count]!=0) {
                return 44;
                
            }
            return 0.00001;
        }
        
    }
    else
        return 0.00001;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if(tableView.tag==TAGAUTORECENT)
    {
        if(section == 0)
        {
            if ([self.recentArray count]!=0)
            {
                
                return 40;
                
            }
            return 0.00001;
            
        }
        else
        {
            return 0.00001;
        }
        
    }
    else
        return 0.00001;
    
    
}



-(void)clearRecent
{
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"clear_history", nil) message:NSLocalizedString(@"delete_recent_searches_msg", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"cancel", nil),NSLocalizedString(@"ok", nil), nil];
    alert.tag=100;
    [alert show];
    
    
}

-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100 && buttonIndex == 1)
    { // handle the altdev
        
        [singleton.arr_recent_Searchservice removeAllObjects];
        [self.recentArray removeAllObjects];
        [self.tbl_recentrend reloadData];
        
    }
    
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if(tableView.tag==TAGAUTORECENT)
    {
        
        if (section == 0){
            
            //check header height is valid
            if ([self.recentArray count]!=0)
            {
                
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
                headerView.backgroundColor=[UIColor clearColor];
                
                
                
                UILabel *label = [[UILabel alloc] init];
                label.frame = CGRectMake(15,5,fDeviceWidth - 30, 40);
                label.backgroundColor = [UIColor clearColor];
                label.textColor = [UIColor colorWithRed:0.298 green:0.337 blue:0.424 alpha:1.0];
                
                label.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
                // label.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
                
                label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
                
                label.text=NSLocalizedString(@"recent_search", nil);
                label.font = [AppFont regularFont:16.0];
                
                [headerView addSubview:label];
                
                
                
                
                return headerView;
            }
            
        }
        if (section == 1){
            
            //check header height is valid
            if ([self.trendingarray count]!=0)
            {
                
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
                headerView.backgroundColor=[UIColor clearColor];
                
                
                
                UILabel *label = [[UILabel alloc] init];
                label.frame = CGRectMake(15,5,fDeviceWidth - 30, 40);
                label.backgroundColor = [UIColor clearColor];
                label.textColor = [UIColor colorWithRed:0.298 green:0.337 blue:0.424 alpha:1.0];
                
                label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
                
                //label.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
                
                label.text= [NSLocalizedString(@"trending_small", nil) capitalizedString];
                
                label.font = [AppFont mediumFont:16.0];
                [headerView addSubview:label];
                
                
                label.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
                
                
                return headerView;
            }
            
        }
    }
    return  nil;
    
}




- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if(tableView.tag==TAGAUTORECENT)
    {
        
        if (section == 0){
            
            //check header height is valid
            if ([self.recentArray count]!=0)
            {
                
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
                headerView.backgroundColor=[UIColor whiteColor];
                
                //--------- Adding UIButton------------------------------------------
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(8, 8, 24.0, 24.0)];
                button.tag = section;
                
                [button setImage:[UIImage imageNamed:@"adv_close"] forState:UIControlStateNormal];
                
                // button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
                button.tag=section;
                //  [button setTitleColor:[[UIColor colorWithRed:6.0/255.0 green:84.0/255.0 blue:141.0/255.0 alpha:1.0] colorWithAlphaComponent:1.0] forState:UIControlStateNormal];
                [button addTarget:self action:@selector(clearRecent) forControlEvents:UIControlEventTouchDown];
                [headerView addSubview:button];
                
                
                
                UILabel *label = [[UILabel alloc] init];
                label.frame = CGRectMake(42,5,290, 30);
                label.backgroundColor = [UIColor clearColor];
                label.textColor = [UIColor colorWithRed:0.0/255.0 green:89.0/255.0 blue:157.0/255.0 alpha:1.0];
                
                label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
                
                label.text= NSLocalizedString(@"clear_history", nil);
                label.font = [AppFont regularFont:16.0];
                [headerView addSubview:label];
                
                
                UITapGestureRecognizer *singleFingerTap =
                [[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(clearRecent)];
                [headerView addGestureRecognizer:singleFingerTap];
                
                
                return headerView;
            }
            
        }else{
            
            
            return nil;
        }
    }
    return  nil;
    
}



@end

#pragma mark - NewSearchCell Cell Interface and Implementation

@interface NewSearchCell()

@end

@implementation NewSearchCell

-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}
-(void)addCategoryStateItemsToScrollView:(NSArray*)arrCat state:(NSArray*)arrStates withFont:(CGFloat)fontSize cell:(NewSearchCell*)cell andTagIndex:(NSIndexPath *)indexpath
{
    
    if (_scrollView_nouse != nil)
    {
        [_scrollView_nouse removeFromSuperview];
        _scrollView_nouse = nil ;

        //return;
    }
    _scrollView_nouse.translatesAutoresizingMaskIntoConstraints = true;
    CGFloat scrollWidth = self.frame.size.width - 180;
    _scrollView_nouse = [[UIScrollView alloc]
                         initWithFrame:CGRectMake(CGRectGetMaxX(_lbl_rating.frame) + 8, CGRectGetMinY(_lbl_rating.frame), scrollWidth, 26)];
    
    SharedManager *singlton = [SharedManager sharedSingleton];
    if (singlton.isArabicSelected)
    {
       _scrollView_nouse = nil;
        scrollWidth = self.frame.size.width - 200;
       _scrollView_nouse = [[UIScrollView alloc]
                             initWithFrame:CGRectMake(CGRectGetMinX(cell.frame) + 8, CGRectGetMinY(_lbl_rating.frame), scrollWidth, 26)];
        //self.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
    }
    
    [cell addSubview:_scrollView_nouse];
    _scrollView_nouse.showsHorizontalScrollIndicator = false;
    _scrollView_nouse.showsHorizontalScrollIndicator = false;
    
    cell.lbl_category.hidden = YES;
    CGFloat xCord = singlton.isArabicSelected ? scrollWidth : 4;
    CGFloat yCord = 3;
    CGFloat padding = 3;
    CGFloat itemHeight = 20;
    CGFloat arabicContentSize = 4;
    for (int i = 0; i < arrCat.count; i++)
    {
        NSString *stateName = arrCat[i ];
        if (stateName.length == 0) {
            continue;
        }
        CGRect frame = [self rectForText:stateName usingFont:[AppFont regularFont:fontSize] boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 20;
        UILabel *lbl = [self labelWithBorderColor:[UIColor colorWithRed:228/255.0 green:100.0/255.0 blue:52.0/255.0 alpha:1.0]];//[[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        
        lbl.font = [AppFont regularFont:fontSize];
        lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
        
        if (singlton.isArabicSelected) {
            xCord -= (frame.size.width + padding);
            lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
            arabicContentSize += frame.size.width + padding;
        } else {
            xCord+=frame.size.width + padding;
        }
        lbl.text = stateName;
        lbl.userInteractionEnabled = YES;
        lbl.tag = indexpath.row;
        [_scrollView_nouse addSubview:lbl];
    }
    // *** custom logic for user state selected ***
    StateList *obj = [[StateList alloc]init];
    singlton.user_StateId = [singlton getStateId];
    NSString *stateTab = @"";
    NSString *userState = @"";
    if (singlton.profilestateSelected.length != 0)
    {
        userState =singlton.profilestateSelected; //[obj getStateName:singlton.profilestateSelected];
    }
    if (singlton.user_StateId.length != 0)
    {
        stateTab = [obj getStateName:singlton.user_StateId];
    }
    NSMutableArray *arrFinal = [[NSMutableArray alloc] init];
    NSMutableArray *arrStatesAll = [[NSMutableArray alloc] initWithArray:arrStates];
    if (userState.length != 0 || stateTab.length != 0) {
        if ([arrStatesAll containsObject:userState]) {
            [arrFinal insertObject:userState atIndex:0];
            [arrStatesAll removeObject:userState];
        }
        else if ([arrStatesAll containsObject:stateTab]) {
            [arrFinal addObject:stateTab];
            [arrStatesAll removeObject:stateTab];
        }
    }
    NSArray *sortedArrayOfString = [arrStatesAll sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [(NSString *)obj1 compare:(NSString *)obj2 options:NSNumericSearch];
    }];
    [arrFinal addObjectsFromArray:sortedArrayOfString];
    
    for (int i = 0; i < arrFinal.count; i++)
    {
        
        NSString *stateName = arrFinal[i ];
        if (stateName.length == 0) {
            continue;
        }
        CGRect frame = [self rectForText:stateName usingFont:[AppFont regularFont:fontSize] boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        UILabel *lbl = [self labelWithBorderColor:[UIColor colorWithRed:86.0/255.0 green:190.0/255.0 blue:113.0/255.0 alpha:1.0]];//[[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        lbl.font = [AppFont regularFont:fontSize];
        lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
        if (singlton.isArabicSelected) {
            xCord -= (frame.size.width + padding);
            lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
            arabicContentSize += frame.size.width + padding;
        } else {
            xCord+=frame.size.width + padding;
        }
        lbl.text = stateName;
        lbl.userInteractionEnabled = YES;
        lbl.tag = indexpath.row;
        [_scrollView_nouse addSubview:lbl];
    }
    xCord = singlton.isArabicSelected ? arabicContentSize : xCord;
    [_scrollView_nouse setContentSize:CGSizeMake(xCord, _scrollView_nouse.frame.size.height)];
    if (singlton.isArabicSelected) {
        [_scrollView_nouse setContentInset:UIEdgeInsetsMake(0, arabicContentSize/2, 0,0)];
    }
  // _scrollView_nouse.backgroundColor = [UIColor redColor];
    [cell bringSubviewToFront:_scrollView_nouse];
}

-(UILabel*)labelWithBorderColor:(UIColor*)borderColor {
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textColor = borderColor;
    lbl.layer.cornerRadius = 10;//12.5;
    lbl.layer.masksToBounds = YES;
    lbl.clipsToBounds = YES;
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.layer.borderColor = [borderColor CGColor];
    lbl.layer.borderWidth = 1.0;
    return lbl;
}
@end
