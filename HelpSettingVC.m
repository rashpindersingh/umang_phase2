//
//  HelpSettingVC.m
//  Umang
//
//  Created by deepak singh rawat on 15/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "HelpSettingVC.h"
#import "HelpScreenCell.h"
#import "SocialHelpIconCell.h"
#import "FAQWebVC.h"
#import "SubmitQueryVC.h"
#import "MBProgressHUD.h"
#import "UMAPIManager.h"
#import "HelpViewController.h"


@interface HelpSettingVC ()<UITableViewDelegate,UITableViewDataSource>
{
    
    __weak IBOutlet UIButton *btnBack;
    SharedManager *singleton;
    NSMutableArray *arrAadharData;
    NSMutableArray *arrAadharHelpName;
    NSMutableArray *arrAadharHelpImage;
    __weak IBOutlet UILabel *titleHeader;
    
}


@end

@implementation HelpSettingVC
@synthesize tbl_helpSetting;

- (void)viewDidLoad
{
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    //Google Tracking
    titleHeader.text = NSLocalizedString(@"help", nil);
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: HELP_SETTING_SCREEN];
    arrAadharHelpName = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"help_faq", nil),NSLocalizedString(@"user_manual", nil), nil];
    
    arrAadharHelpImage = [[NSMutableArray alloc]initWithObjects:@"more_help_support",@"faq_Updated",@"_SocialHelp", nil];
    
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    singleton = [SharedManager sharedSingleton];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor  = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    tbl_helpSetting.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    [self addNavigationView];
}

-(IBAction)backbtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    
    // [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btnBack.hidden = true;
    titleHeader.hidden = true;
    //btnHelp.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backbtnAction:btnBack];
    };
    nvView.lblTitle.text = titleHeader.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tbFrame = self.tbl_helpSetting.frame;
        tbFrame.origin.y = kiPhoneXNaviHeight
        self.tbl_helpSetting.frame = tbFrame;
        [self.view layoutIfNeeded];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    [self setViewFont];
    [tbl_helpSetting reloadData];
    [super viewWillAppear:YES];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    titleHeader.font = [AppFont semiBoldFont:17];
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
/*- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
 {
 
 
 
 if (section == 0)
 {
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text = NSLocalizedString(@"default_tab_help_txt", nil);
 
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 
 
 
 
 }
 
 else if (section == 1)
 {
 
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text =  NSLocalizedString(@"show_banner_help_txt", nil);
 
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 
 
 }
 else if (section == 2)
 
 
 {
 
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text = NSLocalizedString(@"notifications_help_txt", nil);
 
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 }
 else if (section == 3)
 
 
 {
 
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text =  NSLocalizedString(@"change_language_help_txt", nil);
 
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 }
 
 else if (section == 4)
 
 
 {
 
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text = NSLocalizedString(@"font_size_help_txt", nil);
 
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 }
 
 else
 
 {
 
 
 
 static NSString *CellIdentifier = @"ServiceHeaderCell";
 serviceCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (serviceCell == nil)
 {
 [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
 }
 
 //Change MPIN
 serviceCell.lblServiceSubTitle.text =  NSLocalizedString(@"account_setting_help_txt", nil);
 
 
 
 serviceCell.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
 serviceCell.lblServiceNotificatn.font=[UIFont systemFontOfSize:15];
 
 return serviceCell;
 
 }
 
 
 return nil;
 }*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section

{
    
    if (section == 0) {
        return 0;
    }
    else
    {
        return  44;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.section == 0) {
        return 90;
    }
    else
    {
        return 50;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 6;
    }
    else
    {
        return 3;
    }
    
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    //check header height is valid
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,50)];
    headerView.backgroundColor=[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(15,15,300,20);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:0.298 green:0.337 blue:0.424 alpha:1.0];
    
    
    /*  CGRect labelFrame =  label.frame;
     labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
     label.frame = labelFrame;
     */
    if (singleton.isArabicSelected==TRUE)
    {
        label.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        label.textAlignment=NSTextAlignmentLeft;
        
    }
    label.font = [UIFont boldSystemFontOfSize:14.0];
    
    label.text = NSLocalizedString(@"more_help_lbl", nil);
    
    
    [headerView addSubview:label];
    
    return headerView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    static NSString *CellIdentifierType = @"HelpScreenCell";
    HelpScreenCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
    
    if (indexPath.section == 0)
    {
        
        if (indexPath.row == 0)
        {
            cell.lblServiceTitle.text =NSLocalizedString(@"default_tab", nil);
            cell.imgService.image = [UIImage imageNamed:@"default_tab_HS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text = NSLocalizedString(@"default_tab_help_txt", nil);
            cell.lblServiceDescription.font = [AppFont regularFont:15];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        else if (indexPath.row == 1)
        {
            cell.lblServiceTitle.text =NSLocalizedString(@"show_banner", nil);
            cell.imgService.image = [UIImage imageNamed:@"show_banner_HS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text =  NSLocalizedString(@"show_banner_help_txt", nil);
            cell.lblServiceDescription.font = [UIFont systemFontOfSize:15];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        
        else if (indexPath.row == 2)
        {
            cell.lblServiceTitle.text =NSLocalizedString(@"notifications", nil);
            cell.imgService.image = [UIImage imageNamed:@"notifications_HS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text = NSLocalizedString(@"notifications_help_txt", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        
        else if (indexPath.row == 3)
        {
            cell.lblServiceTitle.text = NSLocalizedString(@"language", nil);
            cell.imgService.image = [UIImage imageNamed:@"language_HS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.lblServiceDescription.text =  NSLocalizedString(@"change_language_help_txt", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            
        }
        
        else if (indexPath.row == 4)        {
            cell.lblServiceTitle.text = NSLocalizedString(@"font_size", nil);
            cell.imgService.image = [UIImage imageNamed:@"font_size_HS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.lblServiceDescription.text = NSLocalizedString(@"font_size_help_txt", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            
        }
        
        else if (indexPath.row == 5)
        {
            cell.lblServiceTitle.text =NSLocalizedString(@"account", nil);
            cell.imgService.image = [UIImage imageNamed:@"icon_account_settings_HS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.lblServiceDescription.text =  NSLocalizedString(@"account_setting_help_txt", nil);
            cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        /*   if (singleton.isArabicSelected==TRUE)
         {
         cell.transform = CGAffineTransformMakeRotation(180*0.0174532925);
         
         cell.lblServiceTitle.transform = CGAffineTransformMakeRotation(-M_PI);
         cell.lblServiceDescription.transform = CGAffineTransformMakeRotation(-M_PI);
         
         cell.imgService.transform= CGAffineTransformMakeRotation(-M_PI);
         cell.lblServiceTitle.textAlignment=NSTextAlignmentRight;
         cell.btnSwitch.transform=  CGAffineTransformMakeRotation(-M_PI);
         
         cell.lblServiceTitle.textAlignment=NSTextAlignmentRight;
         cell.lblServiceDescription.textAlignment=NSTextAlignmentRight;
         // cell.transform = CGAffineTransformMakeRotation(180*0.0174532925);
         
         }
         else
         {
         cell.lblServiceTitle.textAlignment=NSTextAlignmentLeft;
         cell.lblServiceDescription.textAlignment=NSTextAlignmentLeft;
         
         }
         */
        if (singleton.isArabicSelected==TRUE)
        {
            cell.transform = CGAffineTransformMakeScale(-1.0, 1.0);
            cell.lblServiceTitle.transform= CGAffineTransformMakeScale(-1.0, 1.0);
            cell.lblServiceDescription.transform= CGAffineTransformMakeScale(-1.0, 1.0);
            cell.lblServiceTitle.textAlignment=NSTextAlignmentRight;
            cell.lblServiceDescription.textAlignment=NSTextAlignmentRight;
        }
        
        else
        {
            cell.lblServiceTitle.textAlignment=NSTextAlignmentLeft;
            cell.lblServiceDescription.textAlignment=NSTextAlignmentLeft;
        }
        
        cell.lblServiceDescription.font = [AppFont regularFont:15];
        cell.lblServiceTitle.font = [AppFont regularFont:17];
        return cell;
        
    }
    
    else
    {
        
        static NSString *simpleTableIdentifier = @"HelpCell";
        
        SocialHelpIconCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (cell == nil)
        {
            cell = [[SocialHelpIconCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.textLabel.text = [arrAadharHelpName objectAtIndex:indexPath.row];
        cell.textLabel.font = [AppFont regularFont:17.0];
        cell.imageView.image = [UIImage imageNamed:[arrAadharHelpImage objectAtIndex:indexPath.row]];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.backgroundColor = [UIColor whiteColor];
        
        
        if (singleton.isArabicSelected==TRUE)
        {
            cell.transform = CGAffineTransformMakeRotation(180*0.0174532925);
            
            cell.textLabel.transform = CGAffineTransformMakeRotation(-M_PI);
            cell.imageView.transform = CGAffineTransformMakeRotation(-M_PI);
            
            cell.textLabel.textAlignment=NSTextAlignmentRight;
            
        }
        else
            
        {
            cell.textLabel.textAlignment=NSTextAlignmentLeft;
            
        }
        
        
        return cell;
    }
    
    
}






#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0)
    {
    }
    
    else
    {
        
        if (indexPath.row==0)//tab picker
        {
            
            [self myHelp_Action];
            
        }
        
        if (indexPath.row==1)//language picker
        {
            
            
            [self openFAQWebVC];
            
        }
        
        if (indexPath.row==2)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

            FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
            if ([singleton.arr_initResponse  count]>0) {
                vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"usrman"];
                
            }
            
            vc.titleOpen= NSLocalizedString(@"user_manual", nil);
            // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
    
}

-(void)myHelp_Action
{
    NSLog(@"My Help Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)openFAQWebVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    if ([singleton.arr_initResponse  count]>0) {
        vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"faq"];
        
    }
    vc.titleOpen= NSLocalizedString(@"help_faq", nil);
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
