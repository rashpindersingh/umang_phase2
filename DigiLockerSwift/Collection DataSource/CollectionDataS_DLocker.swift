//
//  CollectionDataS_DLocker.swift
//  UMANG
//
//  Created by Rashpinder on 06/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import Foundation
import UIKit

class CollectionDataS_DLocker<Cell:CellDocumentList,Model:LockerDocument>:NSObject, UICollectionViewDataSource {
    typealias configureCellModel = ((_ cell:Cell,_ model:Model,_ index:IndexPath)-> Void)
    
    var cellIndentify:String! = CellDocumentList.id
    var configureCell:configureCellModel!
    var segmentType:SegmentType! = SegmentType.issued
    var arrDataSource = [LockerDocument]()
    init( arrData:[LockerDocument],configureCell: @escaping configureCellModel) {
        self.arrDataSource = arrData
        self.configureCell = configureCell
        //self.segmentType = segment
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellDoc = collectionView.dequeueReusableCell(withReuseIdentifier: CellDocumentList.id, for: indexPath) as! Cell
        let model :Model = arrDataSource[indexPath.row] as! Model
        self.configureCell(cellDoc, model, indexPath)
        return cellDoc
    }
    
    
    
    
}
