//
//  CollectionDelegate_DLocker.swift
//  UMANG
//
//  Created by Rashpinder on 06/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import Foundation
import UIKit
import DisplaySwitcher

class CollectionDelegate_DLocker<Cell:CellDocumentList>:NSObject, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    typealias configureCellModel = ((_ cell:Cell?,_ index:IndexPath)-> Void)
    var configureCell:configureCellModel!
    init( configureCell: @escaping configureCellModel) {
        self.configureCell = configureCell
        //self.segmentType = segment
    }
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    func collectionView(_ collectionView: UICollectionView, transitionLayoutForOldLayout fromLayout: UICollectionViewLayout, newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
        let customTransitionLayout = TransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
        return customTransitionLayout
    }
    func collectionView(_ collectionView: UICollectionView,didSelectItemAt indexPath: IndexPath) {
        // let docuMent = arrDataSource[indexPath.row]
       let cellDoc = collectionView.cellForItem(at: indexPath) as? Cell
       self.configureCell(cellDoc,indexPath)
        
    }
    
}
