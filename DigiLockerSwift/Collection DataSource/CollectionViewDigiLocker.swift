//
//  CollectionViewDigiLocker.swift
//  UMANG
//
//  Created by Rashpinder on 06/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import Photos
import DisplaySwitcher
import Foundation

class CollectionViewDigiLocker: UICollectionView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    public var refreshController = UIRefreshControl()
    
    public var isTransitionAvailable = true
    fileprivate lazy var listLayout = DisplaySwitchLayout(staticCellHeight: listLayoutStaticCellHeight, nextLayoutStaticCellHeight: gridLayoutStaticCellHeight, layoutState: .list)
    fileprivate lazy var gridLayout = DisplaySwitchLayout(staticCellHeight: gridLayoutStaticCellHeight, nextLayoutStaticCellHeight: listLayoutStaticCellHeight, layoutState: .grid)
    public var layoutState: LayoutState = .list
    
     var singleton = SharedManager.sharedSingleton() as! SharedManager
    override func awakeFromNib() {
        super.awakeFromNib()
        let layout =  UserDefaults.standard.value(forKey: kIssueLayoutKey) as? Int ?? LayoutState.list.rawValue
        layoutState = LayoutState.init(rawValue: layout) ?? LayoutState.list
        self.collectionViewLayout = layoutState == .list ? listLayout : gridLayout
        self.register(CellDocumentList.cellNib, forCellWithReuseIdentifier:CellDocumentList.id)
        self.backgroundColor = .white
        self.isScrollEnabled = true
        self.alwaysBounceVertical = true;
        refreshController = UIRefreshControl()
        self.addSubview(refreshController)
    }
    
    
   
    
}
