//
//  DigiLockerHomeVC.swift
//  Umang
//
//  Created by Rashpinder on 05/03/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import Photos
import DisplaySwitcher
import Foundation
import MobileCoreServices

class DigiLockerHomeVC: DigiLockerBaseNewVC
{
    // MARK:- **** Property Decalarations *****
    

    @IBOutlet weak var btnGridView: UIButton!
    @IBOutlet weak var btnListView: UIButton!
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnUploadDoc: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var bottomCollectionConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var noRecordView: UIView!
    @IBOutlet weak var collDocuments: CollectionViewDigiLocker!
    @IBOutlet weak var naviView: UIView!
    @IBOutlet weak var viewIssuedDocMsg: UIView!
    @IBOutlet weak var lblIssuedDocMSG: UILabel!
    
    @IBOutlet weak var btnCrossIssuedMSG: UIButton!
    
    @IBOutlet var topBottomConstraintIssedDocMSG: [NSLayoutConstraint]!
    
    
    @IBOutlet weak var widthMenuButtonConstraint: NSLayoutConstraint!
    //fileprivate var isTransitionAvailable = true
    //fileprivate lazy var listLayout = DisplaySwitchLayout(staticCellHeight: listLayoutStaticCellHeight, nextLayoutStaticCellHeight: gridLayoutStaticCellHeight, layoutState: .list)
   // fileprivate lazy var gridLayout = DisplaySwitchLayout(staticCellHeight: gridLayoutStaticCellHeight, nextLayoutStaticCellHeight: listLayoutStaticCellHeight, layoutState: .grid)
   // fileprivate var layoutState: LayoutState = .list
     var arrIssued = [LockerDocument]()
     var arrUploaded = [LockerDocument]()
      var segmentType = SegmentType.issued
      var singleton = SharedManager.sharedSingleton() as SharedManager
     var hud :MBProgressHUD!
     var serviceGroup: DispatchGroup?
    var refreshController = UIRefreshControl()
    
    var dataSourceCollection :CollectionDataS_DLocker<CellDocumentList,LockerDocument>!
     var delegateColl :CollectionDelegate_DLocker<CellDocumentList>!
    
    var fileDelegate = DigiLockerFileModel()
    var isSelectDepartment = false
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    // MARK:- **** View Life Cycle *****

    override func viewDidLoad() {
        super.viewDidLoad()
        let issueMsgShow = UserDefaults.standard.bool(forKey: kIssuedMSGKey)
        self.changeIssuedDocHeaderMSG(show: !issueMsgShow)
        UIApplication.shared.statusBarStyle = .default
        DigiSessionManager.instance
        self.navigationController?.isNavigationBarHidden = true
        self.setupInitialView()
        setupCollectionView()
        self.btnMenu.isHidden = false;
        if  self.isSelectDepartment  {
            self.btnMenu.isHidden = true;
            self.widthMenuButtonConstraint.constant = 0
            self.view.updateConstraintsIfNeeded()
        }
        let accessStr = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String ?? ""
    
        let  dlink = singleton.objUserProfile?.objGeneral?.dlink ?? ""
        if accessStr.lowercased() == "y" || dlink.lowercased() == "y" {
            self.callDispatchApi()
        }else if self.isSelectDepartment{
            self.pushToMenuScreen()
        }
      
        
        // Do any additional setup after loading the view.
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func pushToMenuScreen()  {
        let storyboard = UIStoryboard(name: "DetailService", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DigilockerMenuVC") as! DigilockerMenuVC
        vc.hidesBottomBarWhenPushed = true
        ///vc = "yes"
        vc.isSelectDepartment = true;
        //vc.fileDelegate = self;
        vc.didDepartmentLogin = {[unowned self] (isBack) in
            vc.dismiss(animated: false, completion: nil)
            if isBack {
                self.didTapBackButtonAction(UIButton())
                return;
            }
            self.callDispatchApi()
        }
        self.present(vc, animated: false, completion: nil)
        // self.navigationController!.pushViewController(vc, animated: false)
    }
    func callDispatchApi()  {
        if serviceGroup == nil  {
           serviceGroup = DispatchGroup()
        }
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        serviceGroup?.enter()
        self.fetchIssuedDocumentsApi()
        serviceGroup?.enter()
        self.fetchUploadedDocumentsApi("")
        let issueSortKey =  UserDefaults.standard.value(forKey: kIssueSortKey) as? Int ?? SortType.date.rawValue
        let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
        let uploadSortKey =  UserDefaults.standard.integer(forKey: kUploadSortKey)
        let sortUpload = SortType.init(rawValue: uploadSortKey) ?? SortType.date
        if serviceGroup != nil  {
            serviceGroup?.notify(queue:.main, execute: {[weak self] in
                self?.updateViewWithSegmentChanges()
               // self?.reloadCollectionOnMain()
                self?.serviceGroup = nil
            })
        }
    }
    func setupInitialView(){
        self.segment.setTitle("issued_documents".localized, forSegmentAt: 0)
        self.segment.setTitle("uploaded_documents".localized, forSegmentAt: 1)
        self.lblTitle.text = "digi_locker".localized
        self.btnBack.setTitle("back".localized, for: .normal)
        self.segment.selectedSegmentIndex = segmentType.rawValue
        self.lblTitle.font = AppFont.semiBoldFont(17)
        self.btnBack.titleLabel?.font = AppFont.regularFont(17)
        self.btnSort.titleLabel?.font = AppFont.regularFont(14)
    self.segment.setTitleTextAttributes([kCTFontAttributeName:AppFont.regularFont(14)], for: .normal)
        
        self.lblIssuedDocMSG.font = AppFont.lightFont(13)
        
        //self.naviView.backgroundColor = UIColor(red: 247.0/255.0, green:  247.0/255.0, blue:  249.0/255.0, alpha: 1)
        self.view.backgroundColor = UIColor(red: 239.0/255.0, green:  239.0/255.0, blue:  244.0/255.0, alpha: 1)
        self.viewIssuedDocMsg.backgroundColor = self.view.backgroundColor
        self.lblIssuedDocMSG.backgroundColor = self.view.backgroundColor
        //navi view-- 247 247 249
        // sort 239 239 244
        
    }

    // MARK:-   *** Collection View Setup ***
    fileprivate func setupCollectionView() {
        self.initilizeCollectionDelegate()
        self.initializeDataSource()
        collDocuments.reloadData()
        self.refreshController = self.collDocuments.refreshController
        refreshController.addTarget(self, action: #selector(self.refreshDocumentView), for: .valueChanged)
        collDocuments.addSubview(refreshController)

    }
    @objc func refreshDocumentView()  {
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        switch segmentType {
        case .issued:
            self.fetchIssuedDocumentsApi()
        case .uploaded:
            self.fetchUploadedDocumentsApi()
        }
        if refreshController.isRefreshing {
            refreshController.endRefreshing()
        }
    }
   
// MARK:- ****   ALL Button Actions Here   ****

    
    // MARK: ****   BACK   Button Action  ****

    @IBAction func didTapBtnCrossIssuedDocMSgAction(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: kIssuedMSGKey)
        self.updateViewWithSegmentChanges()
    }
    
    @IBAction func didTapBackButtonAction(_ sender: UIButton) {
        
        if self.isSelectDepartment {
             self.fileDelegate.requestData(dataDict: self.getFailureJSONFromLockerDocument(reason:"User Cancel"))
             self.dismiss(animated: false, completion: nil)
            return;
        }
        if let Navi = self.navigationController {
            for vc in Navi.viewControllers {
                if vc.isKind(of:MoreTabVC.self) {
                   self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    // MARK: ****   SORT ITEMS  Button Action  ****

    @IBAction func didTapSortByButtonAction(_ sender: UIButton) {
        
        let alertController = self.actionSheet()
        let nameAction = UIAlertAction(title: "name_alphabetic".localized, style: .default) { [weak self](alert) in
           self?.sortDocumentsList(.name)
        }
        let dateAction = UIAlertAction(title: "date_newest_oldest".localized, style: .default) {[weak self] (alert) in
            self?.sortDocumentsList(.date)
        }
        alertController.addAction(nameAction)
        alertController.addAction(dateAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender
            if #available(iOS 9.0, *) {
                popoverController.canOverlapSourceViewRect = true
            } else {
                // Fallback on earlier versions
            }
            popoverController.sourceRect.origin.y = 25
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let cancelAction = UIAlertAction(title:"cancel".localized , style: .cancel, handler: nil) //
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: ****   LIST VIEW  Button Action  ****

    @IBAction func didTapListViewButtonAction(_ sender: UIButton) {
        let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded

        if !self.collDocuments.isTransitionAvailable ||  self.collDocuments.layoutState == .list || arrData.count == 0 {
            return
        }
        let key = segmentType == .issued ? kIssueLayoutKey : kIssueLayoutKey
        self.collDocuments.layoutState = .list
    UserDefaults.standard.setValue(self.collDocuments.layoutState.rawValue, forKey: key)

        self.updateTrasitionManager()
    }
    
    // MARK: ****   GRID  Button Action  ****

    @IBAction func didTapGridViewButtonAction(_ sender: UIButton) {
         let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded
        if !self.collDocuments.isTransitionAvailable ||  self.collDocuments.layoutState == .grid || arrData.count == 0{
            return
        }
       self.collDocuments.layoutState = .grid
        let key = segmentType == .issued ? kIssueLayoutKey : kIssueLayoutKey
    UserDefaults.standard.setValue(self.collDocuments.layoutState.rawValue, forKey: key)
        self.updateTrasitionManager()
    }
    
    func updateTrasitionManager() {
        self.updateListGridButtons()
        let layout = self.collDocuments.layoutState == .grid ? gridLayout : listLayout
          let transitionManager = TransitionManager(duration: animationDuration, collectionView: self.collDocuments!, destinationLayout: layout, layoutState: self.collDocuments.layoutState)
        DispatchQueue.main.async {[weak self] in
            self?.checkCollectionViewHeight()
            transitionManager.startInteractiveTransition()
        }
    }
    func updateListGridButtons()  {
        switch self.collDocuments.layoutState {
        case .list:
            btnListView.isSelected = true
            btnGridView.isSelected = false
        case .grid:
            btnListView.isSelected = false
            btnGridView.isSelected = true
        }
    }
    
    // MARK: ****   MENU or MORE Button Action  ****

    @IBAction func didTapMenuButtonAction(_ sender: UIButton) {
        
        let alertController = self.actionSheet()
        let nameAction = UIAlertAction(title: "logout".localized, style: .default) { [weak self](alert) in
            self?.unlinkDigiLocker()
        }
//        let dateAction = UIAlertAction(title: "Account Info", style: .default) {[weak self] (alert) in
//
//        }
        alertController.addAction(nameAction)
        //alertController.addAction(dateAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender
            if #available(iOS 9.0, *) {
                popoverController.canOverlapSourceViewRect = true
            } else {
                // Fallback on earlier versions
            }
            popoverController.sourceRect.origin.y = 25
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let cancelAction = UIAlertAction(title:"cancel".localized , style: .cancel, handler: nil) //
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    // MARK: ****   SEARCH Button Action  ****

    @IBAction func didTapSearchButtonAction(_ sender: UIButton) {
       
        let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded
        if arrData.count != 0  {
            self.pushToSearchVC(self.segmentType, layout: self.collDocuments.layoutState, arrData: arrData)
        }
    }
    // MARK: ****   UPLOAD Button Action  ****
    @IBAction func didTapUploadDocumentButtonAction(_ sender: UIButton){
        if self.segmentType == .issued {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchDLockerDepartmentDocVC") as! SearchDLockerDepartmentDocVC
            vc.isSelectDepartment = self.isSelectDepartment
            vc.fileDelegate = self.fileDelegate
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        let alertController = self.actionSheet()
        let cameraAction = UIAlertAction(title: "camera".localized, style: .default) { [weak self](alert) in
            self?.openImagePicker(with: .camera)
        }
        
        let galaryAction = UIAlertAction(title: "gallery".localized, style: .default) { [weak self](alert) in
            self?.openImagePicker(with: .photoLibrary)
        }
        let fileAppAction = UIAlertAction(title: "browse_text".localized, style: .default) {[weak self] (alert) in
            self?.openFileBrowser()
        }
        alertController.addAction(cameraAction)
        alertController.addAction(galaryAction)
        alertController.addAction(fileAppAction)

        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender
            if #available(iOS 9.0, *) {
                popoverController.canOverlapSourceViewRect = true
            } else {
                // Fallback on earlier versions
            }
            popoverController.sourceRect.origin.y = 25
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let cancelAction = UIAlertAction(title:"cancel".localized , style: .cancel, handler: nil) //
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    func openFileBrowser()   {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeImage)], in: .import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overCurrentContext
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = false
        }
        present(documentPicker, animated: true, completion: nil)
        
        
    }
    func openImagePicker(with type:UIImagePickerControllerSourceType)  {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = type
        DispatchQueue.main.async {[weak self] in
            self?.navigationController?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    // MARK:- *** Segement  Value Changes ****
    @IBAction func didChangeValueSegment(_ sender: UISegmentedControl) {
        self.segmentType = SegmentType(rawValue: sender.selectedSegmentIndex)!
        self.updateViewWithSegmentChanges()
    }
    
    func updateViewWithSegmentChanges() {
        self.btnUploadDoc.isHidden = false

        switch segmentType {
        case .issued:
            self.btnUploadDoc.setImage(UIImage(named: "plusIconDigi"), for: .normal)
            let issueMsgShow = UserDefaults.standard.bool(forKey: kIssuedMSGKey)
            self.changeIssuedDocHeaderMSG(show: !issueMsgShow)
            break
        case .uploaded:
            self.btnUploadDoc.setImage(UIImage(named: "icon_upload"), for: .normal)
            if self.isSelectDepartment {
                self.btnUploadDoc.isHidden = true
            }
            self.changeIssuedDocHeaderMSG(show: false)
            break
        }
        
        let sort = self.getSortyKeyWith(segmentType)
        self.sortDocumentsList(sort)
        //self.sortedButtonTitleChanges()
        self.updateListGridButtons()
        self.reloadCollectionOnMain()
    }
    func changeIssuedDocHeaderMSG(show:Bool)  {
        self.lblIssuedDocMSG.text = show ? "issued_document_text".localized : ""
        for cons in self.topBottomConstraintIssedDocMSG {
            cons.constant = show ? 10 : 0
        }
        self.viewIssuedDocMsg.isHidden = !show
    }
    func reloadAfterDelay()  {
        self.updateTrasitionManager()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK:- UNLINK DIGILOCKER
    
    func unlinkDigiLocker()  {
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        // var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
        // accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
        //dictBody["ort"] = "rgtadhr"
        //dictBody["rc"] = "Y"
        // dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
        //  dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        let objRequest = UMAPIManager()
        objRequest.hitAPIForDigiLockerAuthentication(withPost: true, isAccessTokenRequired: true, webServiceURL: UM_API_UNLINK_DIGILOCKER, withBody: dictBody, andTag: TAG_REQUEST_DIGILOCKER_GETTOKEN, completionHandler: {[weak self](_ response: Any, _ error: Error?, _ tag: REQUEST_TAG) -> Void in
            guard let er = error else {
                guard let responseJson = response as? [String:Any] else {
                    self?.hud.hide(animated: true)
                    return
                }
                self?.processUnlinkDigiLocker(dic: responseJson)
                return
            }
            
            self?.hud.hide(animated: true)
            self?.showErrorAlert(error: er)
        })
    }
    func processUnlinkDigiLocker(dic:[String:Any])  {
        self.hud.hide(animated: true)
        let dicFormat = dic.formatDictionaryForNullValues(dic)
        if self.checkSuccessResponse(dicResponse: dicFormat!) {
            UserDefaults.standard.set(false, forKey: kIssuedMSGKey)

            singleton.objUserProfile.objGeneral.dlink = ""
            UserDefaults.standard.setValue("", forKey: "AccessTokenDigi")
            self.didTapBackButtonAction(UIButton())
        }
    }
    

}
//MARK:- ---====Action Sheet Methods =====

extension DigiLockerHomeVC {
    func actionSheet(title:String? = nil, message:String? = nil) -> UIAlertController  {
        let alertController  = UIAlertController(title: title, message:message , preferredStyle: .actionSheet)
        return alertController
    }
    func sortDocumentsList(_ type:SortType, segment:SegmentType? = nil ){
        let vSegment = segment == nil ? self.segmentType :segment!
        switch vSegment {
        case .issued:
            UserDefaults.standard.set(type.rawValue, forKey: kIssueSortKey)
            let arrSorted = type == .name ? self.arrIssued.sortWithName(self.arrIssued) : self.arrIssued.sortWithDate(self.arrIssued)
              self.arrIssued.removeAll()
            self.arrIssued = arrSorted.removeExistingDownloadUpload()
        case .uploaded:
            UserDefaults.standard.set(type.rawValue, forKey: kUploadSortKey)
            let arrSorted = type == .name ? self.arrUploaded.sortWithName(self.arrUploaded) : self.arrUploaded.sortWithDate(self.arrUploaded)
             self.arrUploaded.removeAll()
            self.arrUploaded = arrSorted.removeExistingDownloadUpload()
            break
        }
        UserDefaults.standard.synchronize()
        self.reloadCollectionOnMain()
    }
    
    func sortedButtonTitleChanges()  {
        switch segmentType {
        case .issued:
          let issueSortKey =  UserDefaults.standard.value(forKey: kIssueSortKey) as? Int ?? SortType.date.rawValue
          let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
          let title = sortIssue == .name ? "Sort_by_name_digi".localized : "sorted_by_date".localized
            self.btnSort.setTitle(title, for: .normal)
        case .uploaded:
            let issueSortKey =  UserDefaults.standard.value(forKey: kUploadSortKey) as? Int ?? SortType.date.rawValue
            let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
            let title = sortIssue == .name ? "Sort_by_name_digi".localized : "sorted_by_date".localized
            self.btnSort.setTitle(title, for: .normal)
        }
    }
}
// MARK:- ***  Web api Call Methods Here ****

extension DigiLockerHomeVC
{
    // MARK:- *** Get Issued Document list ****
    func fetchIssuedDocumentsApi()
    {
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
       // var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
       // accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
        //dictBody["ort"] = "rgtadhr"
        //dictBody["rc"] = "Y"
       // dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
      //  dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        self.callApi(with: dictBody, url: UM_API_GETISSUED_DOC_NEW, type: .issued)
    }
    func callApi(with dicBody:NSMutableDictionary, url:String, type:SegmentType) {
        let objRequest = UMAPIManager()
        objRequest.hitAPIForDigiLockerAuthentication(withPost: true, isAccessTokenRequired: true, webServiceURL: url, withBody: dicBody, andTag: TAG_REQUEST_DIGILOCKER_GETTOKEN, completionHandler: {[unowned self](_ response: Any, _ error: Error?, _ tag: REQUEST_TAG) -> Void in
            guard let er = error as NSError? else {
                guard let responseJson = response as? [String:Any] else {
                    self.hud.hide(animated: true)
                    return
                }
                self.checkResponse(responseJson, type: type)
                return
            }
            if er.code == 403 {
                self.sessionExpirePushBackToMoreTab(sender: self)
            }
            self.hud.hide(animated: true)
            self.showErrorAlert(error: er)
        })
    }
    func checkResponse(_ response:[String:Any],type:SegmentType)  {
        self.hud.hide(animated: true)
        let dicFormat = response.formatDictionaryForNullValues(response)
        if self.checkSuccessResponse(dicResponse: dicFormat!) {
            self.processIssuedApi(dicFormat!, type: type)
        }
    }
    
    // MARK:- *** Get Uploaded Document list ****
    func fetchUploadedDocumentsApi(_ idString:String = "")
    {
        
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        //let accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String ?? ""
        dictBody["id"] = idString
        dictBody["trkr"] = timeInMS
      //  dictBody["ort"] = "rgtadhr"
        //dictBody["rc"] = "Y"
        //dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
       // dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        self.callApi(with: dictBody, url: UM_API_GETUPLOADED_DOC_NEW,type: .uploaded)
    }
    func processIssuedApi(_ dicJson:[String:Any], type:SegmentType)
    {
        self.hud.hide(animated: true)
        
        print("API Success")
        guard let pdJson = dicJson["pd"] as? [String:Any] else {
            return
        }
        guard let itemsJson = pdJson["items"] as? [[String:String]] else {
            return
        }
        var arrDocuments = [LockerDocument]()
        let activeTask = DigiSessionManager.instance.dicActiveTaks
        for dicItems in itemsJson {
            var lockDoc = LockerDocument(dicLocker: dicItems)
            if activeTask?.count != 0
            {
                if let task = activeTask![lockDoc.uri] {
                    lockDoc = task
                }
            }
            arrDocuments.append(lockDoc)
        }
        let uploadTasks = DigiSessionManager.instance.dicUploadTaks;

        switch type {
        case .issued:
            self.arrIssued = arrDocuments
            
        case .uploaded:
            self.arrUploaded = arrDocuments
            if uploadTasks != nil && uploadTasks!.count != 0 {
                let arrDocs = uploadTasks!.map { (key,item) -> LockerDocument in
                    return item
                }
                self.arrUploaded.append(contentsOf: arrDocs)
//                for (_,item) in uploadTasks!.enumerated() {
//                    sef
//                }
            }
        }
        
        if serviceGroup != nil {
            serviceGroup?.leave()
            // serviceGroup = nil ;
        }else {
            self.updateViewWithSegmentChanges()
        }
    }
    
    // MARK:- Collection View DELEGATE

    func initilizeCollectionDelegate() {
        self.delegateColl = CollectionDelegate_DLocker(configureCell: { [weak self](cell, indexPath) in
            self?.cellDidSelected(cell: cell, indexPath: indexPath)
        })
        self.collDocuments.delegate = self.delegateColl
    }
    func cellDidSelected(cell:CellDocumentList?,indexPath:IndexPath)  {
        let docuMent = self.segmentType == .issued ? self.arrIssued[indexPath.row] : self.arrUploaded[indexPath.row]
         docuMent.index = indexPath;
         docuMent.segment = segmentType
         docuMent.cell = cell
        if self.isSelectDepartment {
            if docuMent.type == DocType.file.rawValue {
                self.downloadFile(docuMent)
            }
            return;
        }
        if docuMent.type == DocType.folder.rawValue {
            self.pushToSubHomeVC(docuMent, segment: self.segmentType, layout: self.collDocuments.layoutState, sortType: self.getSortyKeyWith(self.segmentType),isHomeDepartment: self.isSelectDepartment, fileModel: self.fileDelegate)
            return
        }
        if docuMent.downloaded  {
            self.openDownloadedDocument(doc: docuMent)

        }else {
            self.downloadFile(docuMent)
        }
        
        
        //self.showDocumentSelectionOptions(doc: docuMent, cell: cell!)
    }
    func didTapCellRowAtIndexPath(cell:CellDocumentList?,indexPath:IndexPath)  {
        
        let alertController = self.actionSheet()
        let nameAction = UIAlertAction(title: "download_digi".localized, style: .default) { [weak self](alert) in
            self?.unlinkDigiLocker()
        }
        let dateAction = UIAlertAction(title: "open_text".localized, style: .default) {[weak self] (alert) in
            
        }
        alertController.addAction(nameAction)
        alertController.addAction(dateAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = cell
            if #available(iOS 9.0, *) {
                popoverController.canOverlapSourceViewRect = true
            } else {
                // Fallback on earlier versions
            }
            popoverController.sourceRect.origin.y = 25
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let cancelAction = UIAlertAction(title:"cancel".localized , style: .cancel, handler: nil) //
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Collection View DATA Source
    func initializeDataSource(){
        var dataSource = [LockerDocument]()
        if self.segmentType == .issued {
            dataSource = self.arrIssued
        }else {
            dataSource = self.arrUploaded
        }
        self.dataSourceCollection = CollectionDataS_DLocker(arrData: dataSource, configureCell: {[weak self] (cell, model, indexPath) in
            cell.btnMenuGrid.addTarget(self, action: #selector(self?.didTapCellMenuButtonAction(_:)), for: .touchUpInside)
            cell.btnMenu.addTarget(self, action: #selector(self?.didTapCellMenuButtonAction(_:)), for: .touchUpInside)
            cell.configureCell(cell: cell, doc: model, indexPath: indexPath, segment: self?.segmentType)
            if model.downTask != nil && model.downloading {
                cell.updateProgressOnMainQueue(model.downTask!.progress)
                model.cell = cell
                model.index = indexPath
                model.segment = self!.segmentType
                self?.trackDownloadTask(model)
            }
            if model.uploadTask != nil && model.uploading {
                cell.updateProgressOnMainQueue(model.uploadTask!.progress)
                model.cell = cell
                model.index = indexPath
                model.segment = self!.segmentType
                self?.trackUploadTask(model)
            }
            if self!.isSelectDepartment && model.type == DocType.file.rawValue{
                cell.btnMenuGrid.isHidden = true
                cell.btnMenu.isHidden = true
            }
//            self?.setButtonActioneMethods(cell)
        })
        self.collDocuments.dataSource = self.dataSourceCollection
    }
    
    func reloadCollectionOnMain() {
        self.reloadCollectionOnly()
    }
    func reloadCollectionOnly() {
        self.initializeDataSource()
        DispatchQueue.main.async {[weak self] in
            self?.checkCollectionViewHeight()
            self?.sortedButtonTitleChanges()
            self?.setNoRecordHidden()
            self?.collDocuments.reloadData()
        }
    }
    func checkCollectionViewHeight()  {
        let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded
        var height :CGFloat = 0
        switch self.collDocuments.layoutState {
        case .grid:
            height = gridLayoutStaticCellHeight
            if arrData.count.remainderReportingOverflow(dividingBy: 3).partialValue != 0 {
                height = (CGFloat((arrData.count / 3)) * gridLayoutStaticCellHeight) + gridLayoutStaticCellHeight
            }else {
                height = (CGFloat((arrData.count / 3)) * gridLayoutStaticCellHeight)
            }
        case .list:
            height = CGFloat(arrData.count) * listLayoutStaticCellHeight
        }
        
        let issuedHeaderHeight = self.viewIssuedDocMsg.frame.height
        
        if  height >= (self.view.frame.size.height - (120.0 + 43.0 + issuedHeaderHeight)) && arrData.count != 0{
            self.bottomCollectionConstraint.constant = 0.0
        }else if arrData.count != 0{
            self.bottomCollectionConstraint.constant = self.view.frame.size.height - ((120.0 + 43.0 + issuedHeaderHeight) + height)
        }
        //self.bottomCollectionConstraint.constant = 0.0
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
    }
    func setNoRecordHidden() {
        let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded
        if arrData.count == 0 {
            self.noRecordView.isHidden = false
            self.collDocuments.isHidden = true
            self.view.bringSubview(toFront: self.noRecordView)
        }
        else {
            self.noRecordView.isHidden = true
            self.collDocuments.isHidden = false
            self.view.bringSubview(toFront: self.collDocuments)
        }
    }
    func setButtonActioneMethods(_ cell:CellDocumentList)
    {
       
    }
    
}
// MARK:- *** UICollectionViewDataSource UICollectionViewDelegate ****
//
//extension DigiLockerHomeVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
//    // MARK: - UICollectionViewDataSource
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        switch self.segmentType {
//        case .issued:
//            return arrIssued.count
//        case .uploaded:
//            return arrUploaded.count
//        }
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
//    {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellDocumentList.id, for: indexPath) as! CellDocumentList
//        if layoutState == .grid {
//            cell.setupGridLayoutConstraints(1, cellWidth: cell.frame.width)
//        } else {
//            cell.setupListLayoutConstraints(1, cellWidth: cell.frame.width)
//        }
//
//        let doc = self.segmentType == .issued ? self.arrIssued[indexPath.row] : self.arrUploaded[indexPath.row]//arrDataSource[indexPath.row]
//        // doc.index = indexPath
//        cell.setDataSource(doc, segment: self.segmentType)
//        cell.btnMenu.tag = indexPath.row + 1000
//        cell.btnMenuGrid.tag = indexPath.row
//        if doc.downTask != nil && doc.downloading {
//        cell.updateProgressOnMainQueue(doc.downTask!.progress)
//            doc.cell = cell
//            doc.index = indexPath
//            doc.segment = segmentType
//        }
//        if doc.uploadTask != nil && doc.uploading {
//       cell.updateProgressOnMainQueue(doc.uploadTask!.progress)
//           // doc.cell = cell
//           // doc.index = indexPath
//           // doc.segment = segmentType
//        }
//        self.setButtonActioneMethods(cell)
//        return cell
//    }

//    // MARK: - UICollectionViewDelegate
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsetsMake(0, 0, 0, 0)
//    }
//    func collectionView(_ collectionView: UICollectionView, transitionLayoutForOldLayout fromLayout: UICollectionViewLayout, newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
//        let customTransitionLayout = TransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
//        return customTransitionLayout
//    }
//
//    func collectionView(_ collectionView: UICollectionView,didSelectItemAt indexPath: IndexPath) {
//       // let docuMent = arrDataSource[indexPath.row]
//        let docuMent = self.segmentType == .issued ? self.arrIssued[indexPath.row] : self.arrUploaded[indexPath.row]
//        if docuMent.type == DocType.folder.rawValue {
//            self.pushToSubHomeVC(docuMent, segment: self.segmentType, layout: self.layoutState, sortType: self.getSortyKeyWith(self.segmentType))
//        }
//        else if docuMent.type == DocType.file.rawValue {
//            docuMent.index = indexPath
//            let cell = collectionView.cellForItem(at: indexPath) as? CellDocumentList
//            docuMent.cell = cell
//            docuMent.segment = segmentType
//            self.downloadFile(docuMent)
//        }
//
//    }
//
//}

// MARK:- ***** Downlod Document Methods  ****

extension DigiLockerHomeVC {
    
    func downloadFile(_ doc:LockerDocument)
    {
        doc.segment = self.segmentType
        let task = self.downloadDocumentFile(doc) ?? doc
        task.downloading = true
        // self.segmentType == .issued ? self.arrIssued[indexPath.row]  : self.arrUploaded[indexPath.row]
        self.saveTaskToArray(doc)
        //  self.arrDataSource[doc.index.row] = task
        task.cell?.updateProgressOnMainQueue(0)
        if self.isSelectDepartment {
            hud = MBProgressHUD.showAdded(to: view, animated: true)
            hud.label.text = "loading".localized
        }
        task.downTask?.completionHandler = { [weak self] in
            switch $0 {
            case .failure(let error):
                print(error)
              self?.downloadFile(doc)
            case .success(let document):
                self?.downloadSuccess(document)
            }
        }
        task.downTask?.progressHandler = { [weak self] in
            self?.updateProgress($0)
        }
    }
    func trackDownloadTask(_ doc:LockerDocument)  {
        let task = doc.downTask
        task?.completionHandler = nil
        task?.progressHandler = nil
        if task?.completionHandler == nil  {
            task?.completionHandler = { [weak self] in
                switch $0 {
                case .failure(let error):
                    print(error)
                    self?.downloadFile(doc)
                case .success(let path):
                    self?.downloadSuccess(path)//downloadSuccess(doc, data: data)
                }
            }
        }
        if task?.progressHandler == nil  {
            task?.progressHandler = {[weak self] in
                self?.updateProgress($0)
            }
        }
        
    }
    func saveTaskToArray(_ doc:LockerDocument)  {
        if doc.segment != nil {
            switch doc.segment! {
            case .issued:
                self.arrIssued[doc.index.row] = doc
                let docNew = self.arrIssued[doc.index.row]
                print("New Updated Doc Added Into Issued Array --\(docNew.localPath) -- with name --\(docNew.name)")
            case .uploaded:
                self.arrUploaded[doc.index.row] = doc
            }
        }
    }
    
    func updateProgress(_ doc:LockerDocument)
    {
        self.saveTaskToArray(doc)
        if let docSegment = doc.segment {
            if docSegment == segmentType
            {
                if let cell = self.collDocuments.cellForItem(at: doc.index) as? CellDocumentList
                {
                    cell.updateProgressOnMainQueue(doc.downTask!.progress)
                }
            }
        }
    }
    
    func downloadSuccess(_ doc:LockerDocument)  {
        self.saveTaskToArray(doc)
        if self.isSelectDepartment  {
            let json = self.getJsonFileFromLockerDocument(doc: doc)
            self.fileDelegate.requestData(dataDict: json)
            return
        }
        doc.downTask = nil
        doc.downloaded = true
        doc.cell = nil
         if let docSegment = doc.segment {
            if docSegment == segmentType {
                //  self.arrDataSource[doc.index.row] = doc
                DispatchQueue.main.async {[weak self] in
                    // cell.updateProgressOnMainQueue(1.0)
                    self?.collDocuments.reloadItems(at: [doc.index])
                }
            }
        }
    }
    
    
}
    
// MARK:- ***** Cell Button Actione Here ****
extension DigiLockerHomeVC{
    @objc  func didTapCellMenuButtonAction(_ sender:UIButton)  {
        let tag = sender.tag >= 1000 ? sender.tag - 1000 : sender.tag
        let doc = self.segmentType == .issued ? self.arrIssued[tag] : self.arrUploaded[tag]//self.arrDataSource[tag]
         doc.segment = self.segmentType
        doc.index = IndexPath(item: tag, section: 0)
        let cell = self.collDocuments.cellForItem(at:  doc.index) as? CellDocumentList
        doc.cell = cell
        self.showDocumentSelectionOptions(doc: doc, cell: sender)

    }
    func showDocumentSelectionOptions(doc:LockerDocument,cell:UIView)  {
        if doc.type == DocType.folder.rawValue {
            self.pushToSubHomeVC(doc, segment: self.segmentType, layout: self.collDocuments.layoutState, sortType: self.getSortyKeyWith(self.segmentType),isHomeDepartment: self.isSelectDepartment, fileModel: self.fileDelegate)
            return
        }
        let alertController = self.actionSheet()
        let nameAction = UIAlertAction(title: "open_text".localized, style: .default) { [weak self](alert) in
            self?.openDownloadedDocument(doc: doc)
        }
        var strDownload = "download_digi".localized
        if doc.downloaded  {
            alertController.addAction(nameAction)
            strDownload = "refresh_digi".localized
        }
        let downloadAction = UIAlertAction(title: strDownload, style: .default) { [weak self](alert) in
            self?.downloadFile(doc)
        }
        alertController.addAction(downloadAction)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = cell
            if #available(iOS 9.0, *) {
                popoverController.canOverlapSourceViewRect = true
            } else {
                // Fallback on earlier versions
            }
            popoverController.sourceRect.origin.y = 25
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let cancelAction = UIAlertAction(title:"cancel".localized , style: .cancel, handler: nil) //
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func openDownloadedDocument(doc:LockerDocument) {
        
        if doc.localPath.contains(DocType.jpg.rawValue) || doc.localPath.contains(DocType.pdf.rawValue) || doc.localPath.contains(DocType.png.rawValue) || doc.localPath.contains(DocType.jpeg.rawValue) {
        }
        else {
            doc.localPath = "\(doc.localPath).\(doc.mime.components(separatedBy: "/")[1])"
        }
        let documentVC = UIDocumentInteractionController(url: URL(fileURLWithPath: doc.localPath))
        documentVC.name = doc.name
        documentVC.delegate = self
        documentVC.presentPreview(animated: true)
    }
    
}

// MARK:- ***** Scroll Delegates  ****


extension DigiLockerHomeVC :UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.collDocuments.isTransitionAvailable = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
         self.collDocuments.isTransitionAvailable = true
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    
}
//MARK:-  UIDocumentInteractionController delegates


extension DigiLockerHomeVC: UIDocumentInteractionControllerDelegate,UINavigationControllerDelegate
{
   func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
    UINavigationBar.appearance().backgroundColor = UIColor.white
    UINavigationBar.appearance().barTintColor = UIColor.white
    UINavigationBar.appearance().isOpaque = false
    UINavigationBar.appearance().tintColor = UIColor.white
    
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.black, NSFontAttributeName: AppFont.regularFont(16)]
    return self
  }
}
//MARK:- **** UIImage Picker Controller Delegate Upload Files ***

extension DigiLockerHomeVC : UIImagePickerControllerDelegate
{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
       var fileName = ""
      if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset  = result.firstObject
           fileName = asset?.value(forKey: "filename") as? String ?? ""
        }
     if let beforeCrop = info[UIImagePickerControllerOriginalImage] as? UIImage {
         let imageData = UIImageJPEGRepresentation(beforeCrop, 1)
       
        fileName  = fileName.isEmpty ? "Image" + Date().getString()! : fileName
        if imageData!.getSize() > 10.0 {
            return
        }
        let task = self.uploadFileWith(imageData!, path: fileName)
           // task.localPath = tuple!.path
            task.segment = self.segmentType
            task.uploading = true
            task.uploadTask!.progress = 0.0
            self.uploadTask(task)
        }
        self.dismiss(animated: true, completion: nil)
     }
    
   
    
}
// MARK:- **** FIle App Picker Document Delegate ****
extension DigiLockerHomeVC:  UIDocumentPickerDelegate {
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print("didPickDocumentAt-- URL --\(url)")
        let urlStr = url.absoluteString.components(separatedBy: "Inbox/")
        var fileName = ""
        if let lastObject = urlStr.last {
            fileName = lastObject.components(separatedBy: ".").first ?? ""
        }
        
        do {
             let docData = try?  Data(contentsOf: url)
            
            if docData!.getSize() > 10.0 {
                return
            }
            
            let task = self.uploadFileWith(docData!, path: fileName)
            // task.localPath = tuple!.path
            task.segment = self.segmentType
            task.uploading = true
            task.uploadTask!.progress = 0.0
            self.uploadTask(task)
        }catch {
            
        
        }
        
        /// Handle your document
    }
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overCurrentContext
        present(documentPicker, animated: true, completion: nil)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        /// Picker was cancelled! Duh  🤷🏻‍♀️
    }
}
// MARK:- ** Document Upload API , METHODS ****
extension DigiLockerHomeVC {
    func uploadTask(_ doc:LockerDocument){
        var task = doc
        task.uploading = true
        task = self.saveUploadTaskToArray(doc)
        let sort = self.getSortyKeyWith(task.segment!)
        self.sortDocumentsList(sort, segment: task.segment!)
        let index = self.checkTaskinArrayUpload(doc: doc)
        self.reloadCollectionOnMain()
        task.index = index
        DigiSessionManager.instance.dicUploadTaks[task.taskDescription] = task
        task.uploadTask?.completionHandler = nil;
        if  task.uploadTask?.completionHandler == nil  {
            task.uploadTask?.completionHandler = { [weak self] in
                switch $0 {
                case .failure(let error):
                    print(error)
                //self?.
                case .success(let doc):
                    self?.uploadSuccess(doc)
                }
            }
        }
        task.uploadTask?.progressHandler = nil;
        if  task.uploadTask?.progressHandler == nil  {
            task.uploadTask?.progressHandler = { [weak self] in
                self?.uploadProgress($0)
            }
        }
        
        //self.uploadProgress(task)
    }
    func trackUploadTask(_ doc:LockerDocument)  {
        var task = doc
        task.uploadTask?.completionHandler = nil
        if  task.uploadTask?.completionHandler == nil  {
            task.uploadTask?.completionHandler = { [weak self] in
                switch $0 {
                case .failure(let error):
                    print(error)
                //self?.
                case .success(let doc):
                    self?.uploadSuccess(doc)
                }
            }
        }
        
        task.uploadTask?.progressHandler = nil
        if  task.uploadTask?.progressHandler == nil
        {
            task.uploadTask?.progressHandler = { [weak self] in
                self?.uploadProgress($0)
                print("doc progressHandler upload task --\($0.taskDescription)")
            }
        }
    }
    func uploadProgress(_ doc:LockerDocument)  {
        if doc.segment! == segmentType
        {
            // self.saveTaskToArray(doc)
            let index = self.checkTaskinArrayUpload(doc: doc)
            if let cell = self.collDocuments.cellForItem(at:index) as? CellDocumentList
            {
                if doc.uploadTask!.progress >= 100.0 {
                    self.uploadSuccess(doc)
                }
                cell.updateProgressOnMainQueue(doc.uploadTask!.progress)
            }
        }
    }
    func checkTaskinArrayUpload( doc:LockerDocument) -> IndexPath {
        var index : Int = 11110
        if doc.segment != nil {
            switch doc.segment! {
            case .issued:
                index = self.arrIssued.indexOf(object: doc)
                if  index != 11110 {
                    self.arrIssued[index] = doc
                }
            case .uploaded:
                index = self.arrUploaded.indexOf(object: doc)
                if index != 11110 {
                    self.arrUploaded[index] = doc
                }
            }
        }
        return IndexPath(item: index, section: 0)
    }
    func uploadSuccess(_ doc:LockerDocument) {
        if DigiSessionManager.instance.dicUploadTaks.count != 0 {
            return
        }
        // DispatchQueue.main.async { [weak self] in
        self.refreshDocumentView()
        //}
    }
    func saveUploadTaskToArray(_ doc:LockerDocument) -> LockerDocument {
        var docR = doc
        if docR.segment != nil {
            switch docR.segment! {
            case .issued:
                self.arrIssued.insert(docR, at: 0)
                //  self.arrIssued.append(docR)
                self.arrIssued.first?.index = IndexPath(item: 0, section: 0)
                docR = self.arrIssued.first ?? docR
            case .uploaded:
                self.arrUploaded.insert(docR, at: 0)
                //  self.arrIssued.append(docR)
                self.arrUploaded.first?.index = IndexPath(item: 0, section: 0)
                docR = self.arrUploaded.first ?? docR
                //                self.arrUploaded.append(docR)
                //             self.arrUploaded.last?.index = IndexPath(item: self.arrUploaded.count - 1, section: 0)
                //                docR = self.arrUploaded.last ?? docR
            }
        }
        return docR
    }
    
}
// MARK:- *****   Common Methods For View Controllers ****

extension UIViewController {
    func getJsonFileFromLockerDocument(doc:LockerDocument) -> NSMutableDictionary {
        guard let downTask  = doc.downTask else {
            return NSMutableDictionary();
        }
        let dataDoc = downTask.buffer
        let base64 = dataDoc.base64EncodedString()
        let mimeType = dataDoc.mimeType
        
        let dicBody = NSMutableDictionary()
        dicBody.setValue("t", forKey: "status")
        dicBody.setValue(base64, forKey: "base64")
        dicBody.setValue(mimeType, forKey: "mimeType")
        dicBody.setValue(doc.uri, forKey: "uri")
        dicBody.setValue("Success", forKey: "msg")
       return dicBody
    }
    func getFailureJSONFromLockerDocument(reason:String) -> NSMutableDictionary {
        
        let dicBody = NSMutableDictionary()
        dicBody.setValue("f", forKey: "status")
        dicBody.setValue("", forKey: "base64")
        dicBody.setValue("", forKey: "mimeType")
        dicBody.setValue("", forKey: "uri")
        dicBody.setValue(reason, forKey: "msg")
        return dicBody
    }
    func showErrorAlert(error:Error)  {
       
        let alert = UIAlertView(title: "error".localized, message: error.localizedDescription, delegate: nil, cancelButtonTitle: "ok".localized)//UIAlertView(title: "error".localized, message: error.localizedDescription, delegate: nil, cancelButtonTitle:"ok".localized, otherButtonTitles: "")
        alert.show()
    }
    func pushToSubHomeVC(_ doc:LockerDocument, segment:SegmentType, layout:LayoutState, sortType:SortType, isHomeDepartment:Bool = false,fileModel:DigiLockerFileModel)  {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DigiLockerSubHomeVC") as! DigiLockerSubHomeVC
        vc.sortType = sortType//self.getSortyKeyWith(self.segmentType)
        //vc.layoutState = layout//self.layoutState
        vc.folderId = doc.id
        vc.strTitle = doc.name
        vc.isSelectDepartment = isHomeDepartment
        vc.segmentType = segment//self.segmentType
        vc.fileDelegate = fileModel
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func pushToSearchVC(_ segment:SegmentType,layout:LayoutState,arrData:[LockerDocument])  {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DigilockerSearchVC") as! DigilockerSearchVC
        //vc.sortType = sortType//self.getSortyKeyWith(self.segmentType)
        //vc.layoutState = layout//self.layoutState
        vc.arrDocuments = arrData
        vc.segmentType = segment//self.segmentType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func downloadDocumentFile(_ doc:LockerDocument) -> LockerDocument?
    {
         let singleton = SharedManager.sharedSingleton() as! SharedManager
        var dictBody = [AnyHashable:Any]()//NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
//        var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
//        accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
//        dictBody["ort"] = "rgtadhr"
//        dictBody["rc"] = "Y"
//        dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
      //  dictBody["utkn"] = accessTokenString
        dictBody["uri"] = doc.uri
        dictBody["did"] = singleton.appUniqueID()
        var selectedLanguage = UserDefaults.standard.object(forKey: "PreferedLocale") as? String
        if selectedLanguage == nil {
            selectedLanguage = "en"
        }
        dictBody["lang"] = selectedLanguage
        dictBody["mod"] = "app"
        print("Dictionary is \(dictBody)")

        let task =  DigiSessionManager.instance.downloadFileWith(requestBody: dictBody, doc: doc)//digiDownloadFile(tokenRequired: true, apiURL: UM_API_DOWNLOAD_DOC, isPost: true, requestBody: dictBody, successBlock: nil, failerBlock: nil, doc: doc)
        return task
    }
    
func openDocumentfile(_ pdfDocumentData: Data?,doc:LockerDocument)
{
    ( SharedManager.sharedSingleton() as! SharedManager).traceEvents("Open FAQWeb", withAction: "Clicked", withLabel: "DigiLocker Upload", andValue: 0)
        //NSLog(@"OpenWebViewFeedback");
    print("doc Opened--\(doc)")
//    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//    let vc = storyboard.instantiateViewController(withIdentifier: "FAQWebVC") as! FAQWebVC
//    vc.pdfData = pdfDocumentData
//    vc.titleOpen = doc.name
//    vc.urltoOpen = ""
//    vc.isfrom = "WEBVIEWHANDLEPDFOPEN"
//    vc.file_name = doc.localPath
//   // let type = self.checkFileTypes(mime: doc.mime)
//    vc.file_type = doc.mime
//    print("file type --\(vc.file_type)")
//    vc.file_shareType = "ShareYes"
//    vc.modalTransitionStyle = .crossDissolve
//    self.navigationController?.present(vc, animated: false, completion: nil)
  }
    func getUploadFilePath(_ image:UIImage, name:String) -> (path:String, data:Data)? {
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        var logsPath = ""
        if let documentDirectoryPath = documentDirectoryPath
        {
        // create the custom folder path
        logsPath = documentDirectoryPath.appending("/DigiUploadFolder")
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: logsPath)
        {
        do {
            try fileManager.createDirectory(atPath: logsPath,withIntermediateDirectories: false,attributes: nil)
        } catch {
        print("Error creating images folder in documents dir: \(error)")
           }
          }
        }
        // Get documents folder
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateString: String = dateFormatter.string(from: Date())
        let fileName = "\(logsPath)/Image-\(dateString).jpeg"
        let imageData = UIImageJPEGRepresentation(image, 1)
        let base64String: String = imageData!.base64EncodedString()
        let base64data = Data.init(base64Encoded: base64String)
        do {
        try base64data!.write(to: URL(fileURLWithPath: fileName), options: .atomic)
        return (fileName,base64data!)
        } catch {
        print("base64data save digiFolder--=\(error)")
        }
        return nil
    }
    
    func uploadFileWith(_ data:Data, path:String) -> LockerDocument
    {
        
        var dicLocker = [String:String]()
        var dictBody = [AnyHashable: Any]()
        let singleton = SharedManager.sharedSingleton() as SharedManager
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
      //  var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
      //  accessTokenString = accessTokenString?.count == 0 ? "" : //accessTokenString
       //// let fileName: String? = path.components(separatedBy: "/").last
        //let path = "/" + fileName!
        dictBody["trkr"] = timeInMS
        dictBody["tkn"] = singleton.user_tkn
        let mimeType = data.mimeType
       // dictBody["utkn"] = accessTokenString
        dictBody["clength"] = "\(data.count)"
        var varPath = path
        if varPath.contains(DocType.jpg.rawValue) || varPath.contains(DocType.pdf.rawValue) || varPath.contains(DocType.png.rawValue) || varPath.contains(DocType.jpeg.rawValue) {
        }
        else {
           varPath = "\(varPath).\(mimeType.components(separatedBy: "/").last!)"
        }
        dictBody["path"] = varPath
        dictBody["ctype"] = mimeType
        dictBody["file"] = data
        dictBody["did"] = singleton.appUniqueID()
        var selectedLanguage = UserDefaults.standard.object(forKey: "PreferedLocale") as? String
        if selectedLanguage == nil {
            selectedLanguage = "en"
        }
        dictBody["lang"] = selectedLanguage
        dictBody["mod"] = "app"
        
        
        dicLocker["size"] = "\(data.count)"
        dicLocker["type"] = DocType.file.rawValue
        let n = Int(arc4random_uniform(42))
        dicLocker["uri"]  = path + "\(n)"
        dicLocker["mime"] = mimeType
        var nameFile = varPath
        if path.contains("/") {
            nameFile = path.components(separatedBy: "/").last!
        }
        dicLocker["name"] = nameFile
        let doc = LockerDocument(dicLocker: dicLocker)
        let task =  DigiSessionManager.instance.uploadFileWith(requestBody: dictBody, doc: doc)
        task!.date = Date()
        return task!
    }
    func getSortyKeyWith(_ type:SegmentType) -> SortType {
        let key = type == .issued ? kIssueSortKey : kUploadSortKey
        let issueSortKey =  UserDefaults.standard.value(forKey:key ) as? Int ?? SortType.date.rawValue
        return  SortType.init(rawValue: issueSortKey) ?? SortType.date
    }
    func sessionExpirePushBackToMoreTab(sender:UIViewController) {
        guard let Navi = sender.navigationController else {
            sender.dismiss(animated: true, completion: nil)
            return
         }
          var isMoreTab = true
            for vc in Navi.viewControllers {
                if vc.isKind(of:MoreTabVC.self) {
                    self.navigationController?.popToViewController(vc, animated: true)
                    isMoreTab = false
                    return;
                }
            }
        if isMoreTab {
            Navi.popViewController(animated: true)

        }
       
    }
}

