//
//  LoginDigiLockerVC.swift
//  Umang
//
//  Created by Rashpinder on 05/03/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

class LoginDigiLockerVC: UIViewController
{
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var screenTitleLabel: UILabel!
    @IBOutlet var digilockerDescriptionLabel: UILabel!
    
    @IBOutlet var signUpButton: UIButton!
    @IBOutlet var loginTableView: UITableView!
    
    @IBOutlet var loginHeaderView: UIView!
    
    var isAadhaaarLinked = false
    
    var contentArray = [[String:String]]()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        contentArray = [["Heading" : "LINK YOUR AADHAAR XXXX-XXXX-1234","SubHeading":"If your DigiLocker account is linked to this Aadhaar","Image":"adhr_logo_aadhaar.png"],["HeadingText" : "LINK ANOTHER ACCOUNT","SubHeading":"If you wish to link a different Digilocker account","Image":"digi_locker_new"]]
        
    }

    @IBAction func backButtonPressed(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpDigiAccountButtonPressed(_ sender: UIButton)
    {
        
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton)
    {
        
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton)
    {
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }


}
extension LoginDigiLockerVC: UITableViewDataSource, UITableViewDelegate
{
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if isAadhaaarLinked
        {
            return nil
        }
        
        return loginHeaderView
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isAadhaaarLinked
        {
            return contentArray.count
        }
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DigiLockerCell", for: indexPath) as! DigiLockerCell
        cell.headingLabelView.text = contentArray[indexPath.row]["Heading"]
        cell.descriptionLabel.text = contentArray[indexPath.row]["SubHeading"]
        
        let imageName = contentArray[indexPath.row]["Image"]
        
        
        cell.cellImageView.image = UIImage.init(named: imageName!)
        
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        
        return cell
        
    }
    
}

class DigiLockerCell : UITableViewCell
{
    
    @IBOutlet var cellImageView: UIImageView!
    @IBOutlet var headingLabelView: UILabel!
    
    @IBOutlet var descriptionLabel: UILabel!
    
    
    
}
