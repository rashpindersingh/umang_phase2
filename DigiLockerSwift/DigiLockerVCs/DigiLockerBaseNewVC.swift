//
//  DigiLockerBaseNewVC.swift
//  UMANG
//
//  Created by Rashpinder on 06/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import Photos
import DisplaySwitcher
import Foundation

let animationDuration: TimeInterval = 0.0
let listLayoutStaticCellHeight: CGFloat = 83
var gridLayoutStaticCellHeight: CGFloat = 100

var kIssueSortKey = "issueSortKey"
var kUploadSortKey = "kUploadSortKey"
var kIssueLayoutKey = "kIssueLayoutKey"
var kUploadLayoutKey = "kUploadLayoutKey"
var kIssuedMSGKey = "kIssuedMSGKey"

enum SegmentType:Int {
    case issued = 0
    case uploaded = 1
}
enum DocType:String {
    case file = "file"
    case folder = "dir"
    case pdf = "pdf"
    case image = "image"
    case jpg = "jpeg"
    case jpeg = "jpg"
    case png = "png"
    case pdfApplication = "application/pdf"
    case other = "other"
}
enum SortType:Int {
    case name = 100
    case date = 111
    
}
enum DocTypeName:String {
    case file = "file_folder_digi"
   // case folder = "dir"
    case pdf = "pdf_document_digi"
    case image = "image_document_digi"
    case jpg = "jpg_document_digi"
    case jpeg = "Jpeg_document_digi"
    case png = "png_document_digi"
    //case pdfApplication = "PDF Document"
    case other = "other_document_digi"
}
 



class DigiLockerBaseNewVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
