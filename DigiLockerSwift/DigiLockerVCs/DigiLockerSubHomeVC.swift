//
//  DigiLockerHomeVC.swift
//  Umang
//
//  Created by Rashpinder on 05/03/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import DisplaySwitcher
import Photos
import MobileCoreServices
class DigiLockerSubHomeVC: UIViewController {
    // MARK:- **** Property Decalarations *****
    
    @IBOutlet weak var btnGridView: UIButton!
    @IBOutlet weak var btnListView: UIButton!
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnUploadDoc: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var bottomCollectionConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var noRecordView: UIView!
    @IBOutlet weak var collDocuments: CollectionViewDigiLocker!
    @IBOutlet weak var naviView: UIView!
    fileprivate var isTransitionAvailable = true
    fileprivate lazy var listLayout = DisplaySwitchLayout(staticCellHeight: listLayoutStaticCellHeight, nextLayoutStaticCellHeight: gridLayoutStaticCellHeight, layoutState: .list)
    fileprivate lazy var gridLayout = DisplaySwitchLayout(staticCellHeight: gridLayoutStaticCellHeight, nextLayoutStaticCellHeight: listLayoutStaticCellHeight, layoutState: .grid)
     var layoutState: LayoutState = .list
//     var arrIssued = [LockerDocument]()
//     var arrUploaded = [LockerDocument]()
      var arrDataSource = [LockerDocument]()
       var sortType = SortType.date
      var segmentType = SegmentType.issued
      var singleton = SharedManager.sharedSingleton() as! SharedManager
     var hud :MBProgressHUD!
     var serviceGroup: DispatchGroup?
    var folderId = ""
    var strTitle = ""
    var refreshController = UIRefreshControl()
     var dataSourceCollection :CollectionDataS_DLocker<CellDocumentList,LockerDocument>!
     var delegateColl :CollectionDelegate_DLocker<CellDocumentList>!
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    var fileDelegate = DigiLockerFileModel()
    var isSelectDepartment = false
    
    // MARK:- **** View Life Cycle *****

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.setupInitialView()
        setupCollectionView()
        self.layoutState = self.collDocuments.layoutState
        self.refreshDocumentView()
        //self.callDispatchApi()
       // self.view.backgroundColor = UIColor.init(patternImage: UIImage(named:"img_notification_header.png")!)
        if self.isSelectDepartment {
            self.btnUploadDoc.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func callDispatchApi()  {
        if serviceGroup == nil  {
           serviceGroup = DispatchGroup()
        }
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        serviceGroup?.enter()
        self.fetchIssuedDocumentsApi()
        serviceGroup?.enter()
        self.fetchUploadedDocumentsApi("")
        //let issueSortKey =  UserDefaults.standard.value(forKey: kIssueSortKey) as? Int ?? SortType.date.rawValue
//        let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
//        let uploadSortKey =  UserDefaults.standard.integer(forKey: kUploadSortKey)
//        let sortUpload = SortType.init(rawValue: uploadSortKey) ?? SortType.date
        if serviceGroup != nil  {
            serviceGroup?.notify(queue:.main, execute: {[weak self] in
                self?.updateViewWithSegmentChanges()
               // self?.reloadCollectionOnMain()
                self?.serviceGroup = nil
            })
        }
    }
    
    func setupInitialView(){
      //  self.segment.setTitle("Issued", forSegmentAt: 0)
      //  self.segment.setTitle("Uploaded", forSegmentAt: 1)
        self.lblTitle.text = strTitle
        self.btnBack.setTitle("back".localized, for: .normal)
      //  self.segment.selectedSegmentIndex = segmentType.rawValue
        self.lblTitle.font = AppFont.semiBoldFont(17)
        self.btnBack.titleLabel?.font = AppFont.regularFont(17)
        //self.btnSort.titleLabel?.font = AppFont.regularFont(14)
        //self.segment.setTitleTextAttributes([NSFontAttributeName:AppFont.regularFont(14)], for: .normal)
       // self.naviView.backgroundColor = UIColor(red: 247.0/255.0, green:  247.0/255.0, blue:  249.0/255.0, alpha: 1)
        self.view.backgroundColor = UIColor(red: 239.0/255.0, green:  239.0/255.0, blue:  244.0/255.0, alpha: 1)
    }
    // MARK:- *** Collection View Setup ***
    fileprivate func setupCollectionView() {
        self.initilizeCollectionDelegate()
        self.initializeDataSource()
        collDocuments.reloadData()
        self.refreshController = self.collDocuments.refreshController
        refreshController.addTarget(self, action: #selector(self.refreshDocumentView), for: .valueChanged)
        collDocuments.addSubview(refreshController)
        
    }
    @objc func refreshDocumentView()  {
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        switch segmentType {
        case .issued:
            self.fetchIssuedDocumentsApi()
        case .uploaded:
            self.fetchUploadedDocumentsApi(folderId)
        }
        if refreshController.isRefreshing {
            refreshController.endRefreshing()
        }
    }
// MARK: - ***** Custom Button Action Methods *****
    
    @IBAction func didTapBackButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func didTapSortByButtonAction(_ sender: UIButton) {
        
        let alertController = self.actionSheet()
        let nameAction = UIAlertAction(title: "name_alphabetic".localized, style: .default) { [weak self](alert) in
           self?.sortDocumentsList(.name)
        }
        let dateAction = UIAlertAction(title: "date_newest_oldest".localized, style: .default) {[weak self] (alert) in
            self?.sortDocumentsList(.date)
        }
        alertController.addAction(nameAction)
        alertController.addAction(dateAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender
            if #available(iOS 9.0, *) {
                popoverController.canOverlapSourceViewRect = true
            } else {
                // Fallback on earlier versions
            }
            popoverController.sourceRect.origin.y = 25
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let cancelAction = UIAlertAction(title:"cancel".localized , style: .cancel, handler: nil) //
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func didTapListViewButtonAction(_ sender: UIButton) {
        if !isTransitionAvailable ||  layoutState == .list  {
            return
        }
        let key = segmentType == .issued ? kIssueLayoutKey : kIssueLayoutKey
        layoutState = .list
        UserDefaults.standard.setValue(layoutState.rawValue, forKey: key)

        self.updateTrasitionManager()
    }
    
    @IBAction func didTapGridViewButtonAction(_ sender: UIButton) {
        if !isTransitionAvailable ||  layoutState == .grid {
            return
        }
        layoutState = .grid
        let key = segmentType == .issued ? kIssueLayoutKey : kIssueLayoutKey
        UserDefaults.standard.setValue(layoutState.rawValue, forKey: key)
        self.updateTrasitionManager()
    }
    
    func updateTrasitionManager() {
       // self.updateListGridButtons()
        let layout = layoutState == .grid ? gridLayout : listLayout
        self.checkCollectionViewHeight()
       let transitionManager = TransitionManager(duration: animationDuration, collectionView: collDocuments!, destinationLayout: layout, layoutState: layoutState)
        transitionManager.startInteractiveTransition()
    }
    func updateListGridButtons()  {
        switch layoutState {
        case .list:
            btnListView.isSelected = true
            btnGridView.isSelected = false
        case .grid:
            btnListView.isSelected = false
            btnGridView.isSelected = true
        }
    }
    @IBAction func didTapMenuButtonAction(_ sender: UIButton) {
        
    }
    @IBAction func didTapSearchButtonAction(_ sender: UIButton) {
        
    }
    @IBAction func didTapUploadDocumentButtonAction(_ sender: UIButton) {
        
        if self.segmentType == .issued {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchDLockerDepartmentDocVC") as! SearchDLockerDepartmentDocVC
            vc.isSelectDepartment = self.isSelectDepartment
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        let alertController = self.actionSheet()
        let cameraAction = UIAlertAction(title: "camera".localized, style: .default) { [weak self](alert) in
            self?.openImagePicker(with: .camera)
        }
        
        let galaryAction = UIAlertAction(title: "gallery".localized, style: .default) { [weak self](alert) in
            self?.openImagePicker(with: .photoLibrary)
        }
        let fileAppAction = UIAlertAction(title: "browse_text".localized, style: .default) {[weak self] (alert) in
            self?.openFileBrowser()
        }
        alertController.addAction(cameraAction)
        alertController.addAction(galaryAction)
        alertController.addAction(fileAppAction)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender
            if #available(iOS 9.0, *) {
                popoverController.canOverlapSourceViewRect = true
            } else {
                // Fallback on earlier versions
            }
            popoverController.sourceRect.origin.y = 25
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let cancelAction = UIAlertAction(title:"cancel".localized , style: .cancel, handler: nil) //
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    func openFileBrowser()   {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeImage)], in: .import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overCurrentContext
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = false
        }
        present(documentPicker, animated: true, completion: nil)
        
        
    }
    func openImagePicker(with type:UIImagePickerControllerSourceType)  {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = type
        DispatchQueue.main.async {[weak self] in
            self?.navigationController?.present(myPickerController, animated: true, completion: nil)
        }
    }
    @IBAction func didChangeValueSegment(_ sender: UISegmentedControl) {
        self.segmentType = SegmentType(rawValue: sender.selectedSegmentIndex)!
        self.updateViewWithSegmentChanges()
    }
    
    
    
    func updateViewWithSegmentChanges() {
//        switch segmentType {
//        case .issued:
//            //self.btnUploadDoc.isHidden = true
//           // let issueSortKey =  UserDefaults.standard.value(forKey: kIssueSortKey) as? Int ?? SortType.date.rawValue
//            //let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
//            self.sortDocumentsList(sortType)
////            let layout =  UserDefaults.standard.value(forKey: kIssueLayoutKey) as? Int ?? LayoutState.list.rawValue
////            layoutState = LayoutState.init(rawValue: layout) ?? LayoutState.list
//            break
//        case .uploaded:
//           // self.btnUploadDoc.isHidden = false
//            //let uploadSortKey =  UserDefaults.standard.integer(forKey: kUploadSortKey)
//            //let sortUpload = SortType.init(rawValue: uploadSortKey) ?? SortType.date
////            let layout =  UserDefaults.standard.value(forKey: kUploadLayoutKey) as? Int ?? LayoutState.list.rawValue
////            layoutState = LayoutState.init(rawValue: layout) ?? LayoutState.list
//            break
//        }
        self.sortDocumentsList(sortType)
        //self.sortedButtonTitleChanges()
       // self.updateListGridButtons()
        self.reloadCollectionOnMain()
    }
    func reloadAfterDelay()  {
        self.updateTrasitionManager()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DigiLockerSubHomeVC {
    //MARK:----====Action Sheet Methods =====
    func actionSheet(title:String? = nil, message:String? = nil) -> UIAlertController  {
        let alertController  = UIAlertController(title: title, message:message , preferredStyle: .actionSheet)
        return alertController
    }
    func sortDocumentsList(_ type:SortType){
        
        switch segmentType {
        case .issued:
           // UserDefaults.standard.set(type.rawValue, forKey: kIssueSortKey)
            self.arrDataSource = type == .name ? self.sortWithName(self.arrDataSource) : self.sortWithDate(self.arrDataSource)
        case .uploaded:
            //UserDefaults.standard.set(type.rawValue, forKey: kUploadSortKey)
             self.arrDataSource = type == .name ? self.sortWithName(self.arrDataSource) : self.sortWithDate(self.arrDataSource)
            break
       
        }
        UserDefaults.standard.synchronize()
        self.reloadCollectionOnMain()
    }
    func sortWithName(_ arr:[LockerDocument]) -> [LockerDocument] {
        let arrSorted = arr.sorted{$0.name.compare($1.name, options: .caseInsensitive) == .orderedAscending }
        let arrFolder = arrSorted.filter({ (doc) -> Bool in
            return doc.type == DocType.folder.rawValue
        })
        let arrFile = arrSorted.filter({ (doc) -> Bool in
            return doc.type == DocType.file.rawValue
        })
        return arrFolder + arrFile
    }
    func sortWithDate(_ arr:[LockerDocument]) -> [LockerDocument] {
        let arrSorted = arr.sorted() {$0.date!.timeIntervalSince1970 > $1.date!.timeIntervalSince1970}
        let arrFolder = arrSorted.filter({ (doc) -> Bool in
            return doc.type == DocType.folder.rawValue
        })
        let arrFile = arrSorted.filter({ (doc) -> Bool in
            return doc.type == DocType.file.rawValue
        })
        return arrFolder + arrFile
    }
    func sortedButtonTitleChanges()  {
        switch segmentType {
        case .issued:
          let issueSortKey =  UserDefaults.standard.value(forKey: kIssueSortKey) as? Int ?? SortType.date.rawValue
         // let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
//let title = sortIssue == .name ? "Sort By Name" : "Sort By Date"
           // self.btnSort.setTitle(title, for: .normal)
        case .uploaded:
            break
//            let issueSortKey =  UserDefaults.standard.value(forKey: kUploadSortKey) as? Int ?? SortType.date.rawValue
//            let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
//            let title = sortIssue == .name ? "Sort By Name" : "Sort By Date"
//            self.btnSort.setTitle(title, for: .normal)
        }
    }
}
// MARK:- ***  Web api Call Methods Here ****

extension DigiLockerSubHomeVC
{
    // MARK:- *** Get Issued Document list ****
    func fetchIssuedDocumentsApi()
    {
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        // var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
        // accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
        //dictBody["ort"] = "rgtadhr"
        //dictBody["rc"] = "Y"
        // dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
        //  dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        self.callApi(with: dictBody, url: UM_API_GETISSUED_DOC_NEW, type: .issued)
    }
    func callApi(with dicBody:NSMutableDictionary, url:String, type:SegmentType) {
        let objRequest = UMAPIManager()
        objRequest.hitAPIForDigiLockerAuthentication(withPost: true, isAccessTokenRequired: true, webServiceURL: url, withBody: dicBody, andTag: TAG_REQUEST_DIGILOCKER_GETTOKEN, completionHandler: {[weak self](_ response: Any, _ error: Error?, _ tag: REQUEST_TAG) -> Void in
            guard let er = error else {
                guard let responseJson = response as? [String:Any] else {
                    self?.hud.hide(animated: true)
                    return
                }
                self?.checkResponse(responseJson, type: type)
                return
            }
            self?.showErrorAlert(error: er)
        })
    }
    func checkResponse(_ response:[String:Any],type:SegmentType)  {
        let dicFormat = response.formatDictionaryForNullValues(response)
        if self.checkSuccessResponse(dicResponse: dicFormat!) {
            self.processIssuedApi(dicFormat!, type: type)
        }
    }
    
    // MARK:- *** Get Uploaded Document list ****
    func fetchUploadedDocumentsApi(_ idString:String = "")
    {
//
//        let objRequest = UMAPIManager()
//        let dictBody = NSMutableDictionary()
//        let CurrentTime: Double = CACurrentMediaTime()
//        var timeInMS = "\(CurrentTime)"
//        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
//        let accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String ?? ""
//        dictBody["id"] = idString
//        dictBody["trkr"] = timeInMS
//        dictBody["ort"] = "rgtadhr"
//        dictBody["rc"] = "Y"
//        dictBody["mec"] = "Y"
//        dictBody["tkn"] = singleton.user_tkn
//        dictBody["utkn"] = accessTokenString
//        print("Dictionary is \(dictBody)")
//        self.callApi(with: dictBody, url: UM_API_GETUPLOADED_DOC,type: .uploaded)
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        //let accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String ?? ""
        dictBody["id"] = idString
        dictBody["trkr"] = timeInMS
        //  dictBody["ort"] = "rgtadhr"
        //dictBody["rc"] = "Y"
        //dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
        // dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        self.callApi(with: dictBody, url: UM_API_GETUPLOADED_DOC_NEW,type: .uploaded)
    }
    func processIssuedApi(_ dicJson:[String:Any], type:SegmentType)
    {

        print("API Success")
        guard let pdJson = dicJson["pd"] as? [String:Any] else {
            self.hud.hide(animated: true)

            return
        }
        guard let itemsJson = pdJson["items"] as? [[String:String]] else {
            self.hud.hide(animated: true)

            return
        }
        var arrDocuments = [LockerDocument]()
        let activeTask = DigiSessionManager.instance.dicActiveTaks
        
        for dicItems in itemsJson {
            var lockDoc = LockerDocument(dicLocker: dicItems)
            if activeTask?.count != 0
            {
                if let task = activeTask![lockDoc.uri] {
                    lockDoc = task
                }
            }
            arrDocuments.append(lockDoc)
        }
        switch type {
        case .issued:
            self.arrDataSource = arrDocuments
        case .uploaded:
            self.arrDataSource = arrDocuments
        }
        
        if serviceGroup != nil {
            serviceGroup?.leave()
            // serviceGroup = nil ;
        }else {
            self.updateViewWithSegmentChanges()
        }
    }
    func initializeDataSource(){
        let dataSource = self.arrDataSource
//        if self.segmentType == .issued {
//            dataSource = self.arrIssued
//        }else {
//            dataSource = self.arrUploaded
//        }
        self.dataSourceCollection = CollectionDataS_DLocker(arrData: dataSource, configureCell: {[weak self] (cell, model, indexPath) in
            cell.configureCell(cell: cell, doc: model, indexPath: indexPath, segment: self?.segmentType)
            
            self?.setButtonActioneMethods(cell)
            if model.downTask != nil && model.downloading {
               self?.trackDownloadTask(model)
            }
            if model.uploadTask != nil && model.uploading {
                self?.trackDownloadTask(model)
            }
            if self!.isSelectDepartment && model.type == DocType.file.rawValue
            {
                cell.btnMenuGrid.isHidden = true
                cell.btnMenu.isHidden = true
            }
        })
        self.collDocuments.dataSource = self.dataSourceCollection
    }
    func initilizeCollectionDelegate() {
        self.delegateColl = CollectionDelegate_DLocker(configureCell: { [weak self](cell, indexPath) in
            self?.cellDidSelected(cell: cell, indexPath: indexPath)
        })
        self.collDocuments.delegate = self.delegateColl
    }
    func cellDidSelected(cell:CellDocumentList?,indexPath:IndexPath)  {
        let docuMent =  self.arrDataSource[indexPath.row] // //self.arrUploaded[indexPath.row]
        docuMent.index = indexPath;
        docuMent.segment = segmentType
        docuMent.cell = cell
        if self.isSelectDepartment {
            if docuMent.type == DocType.file.rawValue {
                self.downloadFile(docuMent)
            }
            return;
        }
       // self.showDocumentSelectionOptions(doc: docuMent, cell: cell!)
        if docuMent.type == DocType.folder.rawValue {
            self.pushToSubHomeVC(docuMent, segment: self.segmentType, layout: self.collDocuments.layoutState, sortType: self.getSortyKeyWith(self.segmentType),isHomeDepartment: self.isSelectDepartment, fileModel: self.fileDelegate)
            return
        }
        if docuMent.downloaded  {
            self.openDownloadedDocument(doc: docuMent)
            
        }else {
            self.downloadFile(docuMent)
        }
    }
    func reloadCollectionOnMain() {
        self.initializeDataSource()
        DispatchQueue.main.async {[weak self] in
            self?.hud.hide(animated: true)
            self?.checkCollectionViewHeight()
            self?.setNoRecordHidden()
            self?.collDocuments.reloadData()
        }
    }
    func checkCollectionViewHeight()  {
        var height :CGFloat = listLayoutStaticCellHeight
        switch layoutState {
        case .grid:
            height = gridLayoutStaticCellHeight
            if arrDataSource.count.remainderReportingOverflow(dividingBy: 3).partialValue != 0 {
                height = (CGFloat((self.arrDataSource.count / 3)) * gridLayoutStaticCellHeight) + gridLayoutStaticCellHeight
            }else {
                 height = (CGFloat((self.arrDataSource.count / 3)) * gridLayoutStaticCellHeight)
            }
            
        case .list:
            height = (CGFloat(arrDataSource.count) * listLayoutStaticCellHeight)
        }
        
        if  height >= (self.view.frame.size.height - 87.0) {
            self.bottomCollectionConstraint.constant = 0.0
        }else {
            self.bottomCollectionConstraint.constant = self.view.frame.size.height - (87.0 + height )
        }
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
    }
    func setNoRecordHidden() {
        if self.arrDataSource.count == 0 {
            self.noRecordView.isHidden = false
            self.collDocuments.isHidden = true
            self.view.bringSubview(toFront: self.noRecordView)
        }
        else {
            self.noRecordView.isHidden = true
            self.collDocuments.isHidden = false
            self.view.bringSubview(toFront: self.collDocuments)
        }
    }
    func setButtonActioneMethods(_ cell:CellDocumentList)
    {
        cell.btnMenuGrid.addTarget(self, action: #selector(self.didTapCellMenuButtonAction(_:)), for: .touchUpInside)
        cell.btnMenu.addTarget(self, action: #selector(self.didTapCellMenuButtonAction(_:)), for: .touchUpInside)
        
    }
}
// MARK:- *** UICollectionViewDataSource UICollectionViewDelegate ****
//
//extension DigiLockerSubHomeVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
//    // MARK: - UICollectionViewDataSource
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
////        switch layoutState {
////        case .grid:
////        let reminder = arrDataSource.count.remainderReportingOverflow(dividingBy: 3)
////         if reminder.partialValue != 0 {
////                return arrDataSource.count + reminder.partialValue
////            }
////        case .list:
////            return arrDataSource.count
////
////        }
//        return arrDataSource.count
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellDocumentList.id, for: indexPath) as! CellDocumentList
//        if layoutState == .grid {
//            cell.setupGridLayoutConstraints(1, cellWidth: cell.frame.width)
//        } else {
//            cell.setupListLayoutConstraints(1, cellWidth: cell.frame.width)
//        }
//       // cell.bind(searchUsers[(indexPath as NSIndexPath).row])
//        let doc = arrDataSource[indexPath.row]
//        cell.btnMenu.tag = indexPath.row + 1000
//        cell.btnMenuGrid.tag = indexPath.row
//        cell.setDataSource(doc, segment: self.segmentType)
//        if doc.downTask != nil && doc.downloading {
//        cell.updateProgressOnMainQueue(doc.downTask!.progress)
//            doc.cell = cell
//            doc.index = indexPath
//            doc.segment = segmentType
//           // self.trackDownloadTask(doc)
//        }
//        if doc.uploadTask != nil && doc.uploading {
//        cell.updateProgressOnMainQueue(doc.uploadTask!.progress)
//            // doc.cell = cell
//            // doc.index = indexPath
//            // doc.segment = segmentType
//        }
//        self.setButtonActioneMethods(cell)
//
//        return cell
//    }
//
//
//    // MARK: - UICollectionViewDelegate
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsetsMake(0, 0, 0, 0)
//    }
//    func collectionView(_ collectionView: UICollectionView, transitionLayoutForOldLayout fromLayout: UICollectionViewLayout, newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
//        let customTransitionLayout = TransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
//        return customTransitionLayout
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView,didSelectItemAt indexPath: IndexPath) {
//        print("Hi \((indexPath as NSIndexPath).row)")
//        let docuMent = arrDataSource[indexPath.row]
//        if docuMent.type == DocType.folder.rawValue {
//             self.pushToSubHomeVC(docuMent, segment: self.segmentType, layout: self.layoutState, sortType: self.sortType)
//        }
//        else if docuMent.type == DocType.file.rawValue {
//            docuMent.index = indexPath
//            let cell = collectionView.cellForItem(at: indexPath) as? CellDocumentList
//            docuMent.cell = cell
//            docuMent.segment = segmentType
//            self.downloadFile(docuMent)
//        }
//    }
//
//}
extension DigiLockerSubHomeVC {
    func downloadFile(_ doc:LockerDocument)  {
        doc.segment = self.segmentType
       
        let task = self.downloadDocumentFile(doc) ?? doc
        task.downloading = true
       
        self.arrDataSource[doc.index.row] = task
        task.cell?.updateProgressOnMainQueue(0)
        if self.isSelectDepartment {
            hud = MBProgressHUD.showAdded(to: view, animated: true)
            hud.label.text = "loading".localized
        }
        task.downTask?.completionHandler = { [weak self] in
            switch $0 {
            case .failure(let error):
                print(error)
                
                self?.downloadFile(doc)
            case .success(let document):
                self?.downloadSuccess(document)
            }
        }
        task.downTask?.progressHandler = { [weak self] in
            self?.updateProgress($0)
        }
        
    }
    func trackDownloadTask(_ doc:LockerDocument)  {
        let task = doc.downTask
        task?.completionHandler = nil
        task?.progressHandler = nil
        if task?.completionHandler == nil  {
                task?.completionHandler = { [weak self] in
                    switch $0 {
                    case .failure(let error):
                        print(error)
                        self?.downloadFile(doc)
                    case .success(let path):
                        self?.downloadSuccess(path)//downloadSuccess(doc, data: data)
                    }
                }
            }
            if task?.progressHandler == nil  {
                task?.progressHandler = {[weak self] in
                    self?.updateProgress($0)
                }
            }
    
        }
    
    func updateProgress(_ doc:LockerDocument)  {
        self.arrDataSource[doc.index.row] = doc
        if let cell = self.collDocuments.cellForItem(at: doc.index) as? CellDocumentList {
            cell.updateProgressOnMainQueue(doc.downTask!.progress)
        }
    }
    func downloadSuccess(_ doc:LockerDocument)  {
        if self.isSelectDepartment  {
            let json = self.getJsonFileFromLockerDocument(doc: doc)
            self.fileDelegate.requestData(dataDict: json)
            return
        }
        guard doc.cell != nil else {
            return
        }
        doc.downTask = nil
        doc.downloaded = true
        doc.cell = nil
        self.arrDataSource[doc.index.row] = doc
        DispatchQueue.main.async {[weak self] in
            // cell.updateProgressOnMainQueue(1.0)
            self?.collDocuments.reloadItems(at: [doc.index])
        }
    }
}
extension DigiLockerSubHomeVC :UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isTransitionAvailable = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isTransitionAvailable = true
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    
}
//MARK:- **** UIImagePickerControllerDelegate Upload Files ***

extension DigiLockerSubHomeVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        var fileName = ""
        if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset  = result.firstObject
            fileName = asset?.value(forKey: "filename") as? String ?? ""
        }
        if let beforeCrop = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let imageData = UIImageJPEGRepresentation(beforeCrop, 1)
           // let tuple = self.getUploadFilePath(beforeCrop, name: fileName)
            if imageData!.getSize() > 10.0 {
                return
            }
            fileName  = fileName.isEmpty ? "Image" + Date().getString()! : fileName
            fileName = strTitle + "/" + fileName
            
            let task = self.uploadFileWith(imageData!, path: fileName)
           // task.localPath = tuple!.path
            task.segment = self.segmentType
            task.uploading = true
            task.uploadTask!.progress = 0.0
            self.uploadTask(task)
        }
        self.dismiss(animated: true, completion: nil)
    }
    func uploadTask(_ doc:LockerDocument){
        var task = doc
        task.uploading = true
       // task = self.saveUploadTaskToArray(doc)
        self.arrDataSource.append(task)
        //  self.arrIssued.append(docR)
        self.arrDataSource.last?.index = IndexPath(item: self.arrDataSource.count - 1, section: 0)
        task = self.arrDataSource.last ?? task
        let type = self.getSortyKeyWith(self.segmentType)
        self.arrDataSource = type == .name ? self.arrDataSource.sortWithName(self.arrDataSource) : self.arrDataSource.sortWithDate(self.arrDataSource)
       // self.arrDataSource = self.arrDataSource.
        let index = self.checkTaskinArrayUpload(doc: task)
        self.reloadCollectionOnMain()
        task.index = index
    DigiSessionManager.instance.dicUploadTaks[task.taskDescription] = task
        if  task.uploadTask?.completionHandler == nil  {
            task.uploadTask?.completionHandler = { [weak self] in
                switch $0 {
                case .failure(let error):
                    print(error)
                //self?.
                case .success(let doc):
                    self?.uploadSuccess(doc)
                }
            }
        }
        if  task.uploadTask?.progressHandler == nil
        {
            task.uploadTask?.progressHandler = { [weak self] in
                self?.uploadProgress($0)
                print("doc progressHandler upload task --\($0.taskDescription)")

            }
        }
        //self.uploadProgress(task)
    }
    func trackUploadTask(_ doc:LockerDocument)  {
        var task = doc
        task.uploadTask?.completionHandler = nil
        if  task.uploadTask?.completionHandler == nil  {
            task.uploadTask?.completionHandler = { [weak self] in
                switch $0 {
                case .failure(let error):
                    print(error)
                //self?.
                case .success(let doc):
                    self?.uploadSuccess(doc)
                }
            }
        }
        
        task.uploadTask?.progressHandler = nil
        if  task.uploadTask?.progressHandler == nil
        {
            task.uploadTask?.progressHandler = { [weak self] in
                self?.uploadProgress($0)
                print("doc progressHandler upload task --\($0.taskDescription)")
            }
        }
    }
    func uploadProgress(_ doc:LockerDocument)  {
        if doc.segment! == segmentType
        {
           //self.saveTaskToArray(doc)
           // self.arrDataSource[doc.index.row] = doc
            let index = self.checkTaskinArrayUpload(doc: doc)
            if let cell = self.collDocuments.cellForItem(at:index) as? CellDocumentList
            {
        cell.updateProgressOnMainQueue(doc.uploadTask!.progress)
            }
        }
    }
    func checkTaskinArrayUpload( doc:LockerDocument) -> IndexPath {
        var index : Int = 11110
        if doc.segment != nil {
            switch doc.segment! {
            case .issued:
                index = self.arrDataSource.indexOf(object: doc)
                if  index != 11110 {
                    self.arrDataSource[index] = doc
                }
            case .uploaded:
                index = self.arrDataSource.indexOf(object: doc)
                if index != 11110 {
                    self.arrDataSource[index] = doc
                }
            }
        }
        return IndexPath(item: index, section: 0)
    }
    func uploadSuccess(_ doc:LockerDocument) {
        if DigiSessionManager.instance.dicUploadTaks.count != 0 {
            return
        }
        // DispatchQueue.main.async { [weak self] in
        self.refreshDocumentView()
        //}
    }
}
// MARK:- ***** Cell Button Actione Here ****
extension DigiLockerSubHomeVC
{
    @objc  func didTapCellMenuButtonAction(_ sender:UIButton)  {
        let tag = sender.tag >= 1000 ? sender.tag - 1000 : sender.tag
        let doc = self.segmentType == .issued ? self.arrDataSource[tag] : self.arrDataSource[tag]//self.arrDataSource[tag]
        doc.segment = self.segmentType
        doc.index = IndexPath(item: tag, section: 0)
        let cell = self.collDocuments.cellForItem(at:  doc.index) as? CellDocumentList
        doc.cell = cell
        self.showDocumentSelectionOptions(doc: doc, cell: sender)
     }
    func showDocumentSelectionOptions(doc:LockerDocument,cell:UIView)  {
        if doc.type == DocType.folder.rawValue {
            self.pushToSubHomeVC(doc, segment: self.segmentType, layout: self.collDocuments.layoutState, sortType: self.getSortyKeyWith(self.segmentType),isHomeDepartment: self.isSelectDepartment, fileModel: self.fileDelegate)
            return
        }
        let alertController = self.actionSheet()
        let nameAction = UIAlertAction(title: "open_text".localized, style: .default) { [weak self](alert) in
            self?.openDownloadedDocument(doc: doc)
        }
        var strDownload = "download_digi".localized
        if doc.downloaded  {
            alertController.addAction(nameAction)
            strDownload = "refresh_digi".localized
        }
        let downloadAction = UIAlertAction(title: strDownload, style: .default) { [weak self](alert) in
            self?.downloadFile(doc)
        }
        alertController.addAction(downloadAction)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = cell
            if #available(iOS 9.0, *) {
                popoverController.canOverlapSourceViewRect = true
            } else {
                // Fallback on earlier versions
            }
            popoverController.sourceRect.origin.y = 25
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let cancelAction = UIAlertAction(title:"cancel".localized , style: .cancel, handler: nil) //
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func openDownloadedDocument(doc:LockerDocument) {
        
        if doc.localPath.contains(DocType.jpg.rawValue) || doc.localPath.contains(DocType.pdf.rawValue) || doc.localPath.contains(DocType.png.rawValue) || doc.localPath.contains(DocType.jpeg.rawValue) {
        }
        else {
            doc.localPath = "\(doc.localPath).\(doc.mime.components(separatedBy: "/")[1])"
        }
        let documentVC = UIDocumentInteractionController(url: URL(fileURLWithPath: doc.localPath))
        documentVC.name = doc.name
        documentVC.delegate = self
        //documentVC.
        documentVC.presentPreview(animated: true)
    }
}
extension DigiLockerSubHomeVC: UIDocumentInteractionControllerDelegate
{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
    
        UINavigationBar.appearance().backgroundColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().isOpaque = false
        UINavigationBar.appearance().tintColor = UIColor.white
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.black, NSFontAttributeName: AppFont.regularFont(16)]
        return self
    }
    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
        
    }

}
@available(iOS 11.0, *)
extension DigiLockerSubHomeVC :UIDocumentBrowserViewControllerDelegate {
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentsAt documentURLs: [URL]) {
        print("didPickDocumentsAt -- \(documentURLs)")
    }
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentURLs documentURLs: [URL]) {
        print("didPickDocumentURLs -- \(documentURLs)")

    }

}
extension DigiLockerSubHomeVC: UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let urlStr = url.absoluteString.components(separatedBy: "Inbox/")
        var fileName = ""
        if let lastObject = urlStr.last {
            fileName = lastObject.components(separatedBy: ".").first ?? ""
        }
        
        do {
            let docData = try?  Data(contentsOf: url)
            
            if docData!.getSize() > 10.0 {
                return
            }
            
            let task = self.uploadFileWith(docData!, path: fileName)
            // task.localPath = tuple!.path
            task.segment = self.segmentType
            task.uploading = true
            task.uploadTask!.progress = 0.0
            self.uploadTask(task)
        }catch {
            
            
        }
        
        /// Handle your document
    }
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overCurrentContext
        present(documentPicker, animated: true, completion: nil)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        /// Picker was cancelled! Duh  🤷🏻‍♀️
    }
}
