//
//  CellDocumentList.swift
//  Umang
//
//  Created by Rashpinder on 05/03/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import DisplaySwitcher

protocol CellInterface {
    
    static var id: String { get }
    static var cellNib: UINib { get }
    
}

extension CellInterface {
    
    static var id: String {
        return String(describing: Self.self)
    }
    
    static var cellNib: UINib {
        return UINib(nibName: id, bundle: nil)
    }
    
}
private let avatarListLayoutSize: CGFloat = 45.0
let colorDocName = UIColor(red: 71.0/255.0, green: 71.0/255.0, blue: 71.0/255.0, alpha: 1)
let dateColor = UIColor(red: 124.0/255.0, green: 124.0/255.0, blue: 124.0/255.0, alpha: 1)

class CellDocumentList: UICollectionViewCell,CellInterface
{
    
    @IBOutlet fileprivate weak var leftImageView: UIImageView!
    @IBOutlet fileprivate weak var nameListLabel: UILabel!
    @IBOutlet fileprivate weak var nameGridLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var size: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnMenuGrid: UIButton!
    @IBOutlet weak var lblDateNewOne: UILabel!
    @IBOutlet weak var btnDownloaded: UIButton!
    
    @IBOutlet weak var widthSizeLabelConstraint: NSLayoutConstraint!
    

    @IBOutlet weak var imageGridView: UIImageView!
    @IBOutlet weak var lblBottomLine: UILabel!
    @IBOutlet weak var progressList: UIProgressView!
    @IBOutlet weak var imageDownloaded: UIImageView!
    
    @IBOutlet weak var imageGridDownloaded: UIImageView!
    @IBOutlet weak var vwPrgressList: UIView!
    
    @IBOutlet weak var lblProgressGrid: UILabel!
    @IBOutlet weak var vwProgressGrid: UIView!
    @IBOutlet weak var imageProgressGrid: UIImageView!
    @IBOutlet weak var nameOfProgressLabel: UILabel!
    
    @IBOutlet weak var progressGrid: UIProgressView!
    @IBOutlet weak var lblProgressList: UILabel!
    //    var progressGrid :UIProgressView?
//    var progressList :UIProgressView?
//    var lblProgressList :UILabel?
//    var lblProgressGrid :UILabel?
    
    @IBOutlet weak var leadingListContentConstraint: NSLayoutConstraint!{
        didSet {
            initialLabelsLeadingConstraintValue = leadingListContentConstraint.constant
        }
    }
    @IBOutlet weak var listContentView: UIView!
    @IBOutlet weak var gridContentView: UIView!
    // avatarImageView constraints
    @IBOutlet fileprivate weak var avatarImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var avatarImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var gridImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var gridImageHeightConstraint: NSLayoutConstraint!
  
   
    
    fileprivate var avatarGridLayoutSize: CGFloat = 0.0
    fileprivate var initialLabelsLeadingConstraintValue: CGFloat = 0.0
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
    }
    func setFont() {
        self.date.font = AppFont.lightFont(15)
        self.nameListLabel.font = AppFont.regularFont(16)
        self.nameGridLabel.font = AppFont.regularFont(13)
        self.lblProgressList.font = AppFont.regularFont(12)
        self.lblProgressGrid.font = AppFont.regularFont(12)
        self.size.font = AppFont.regularFont(13)
        self.nameListLabel.numberOfLines = 1
        self.nameListLabel.lineBreakMode = .byTruncatingTail
        self.nameOfProgressLabel.font = AppFont.regularFont(10)
        self.nameOfProgressLabel.numberOfLines = 1
        self.nameOfProgressLabel.lineBreakMode = .byTruncatingTail
        self.btnDownloaded.titleLabel?.font = AppFont.boldFont(12)
        self.lblDateNewOne.font = AppFont.regularFont(13)
        
        self.nameListLabel.textColor = colorDocName
        self.nameGridLabel.textColor = colorDocName
        self.nameOfProgressLabel.textColor = colorDocName
        self.lblProgressGrid.textColor = colorDocName
        self.lblProgressList.textColor = colorDocName
        
        self.date.textColor = colorDocName
        
        self.lblDateNewOne.textColor = dateColor
        self.size.textColor = dateColor

    }
    func configureCell(cell:CellDocumentList,doc:LockerDocument,indexPath:IndexPath,segment:SegmentType?)  {
        guard let segmentType = segment else {
            return;
        }
        let layout =  UserDefaults.standard.value(forKey: kIssueLayoutKey) as? Int ?? LayoutState.list.rawValue
        let layoutState = LayoutState.init(rawValue: layout) ?? LayoutState.list
        if layoutState == .grid {
            cell.setupGridLayoutConstraints(1, cellWidth: cell.frame.width)
        } else {
            cell.setupListLayoutConstraints(1, cellWidth: cell.frame.width)
        }
        
        cell.setDataSource(doc, segment: segmentType)
        cell.btnMenu.tag = indexPath.row + 1000
        cell.btnMenuGrid.tag = indexPath.row
        
    }
    func setDataSource(_ document: LockerDocument, segment:SegmentType) {
        self.btnDownloaded.isHidden = true
        self.imageDownloaded.isHidden = true
        self.imageGridDownloaded.isHidden = true
        
        self.progressHiddenUpdate(hide: false)
        let typeTupple = self.checkFileTypes(mime: document.mime)
        nameListLabel.text = document.name
        nameGridLabel.text = nameListLabel.text
        nameOfProgressLabel.text = document.name
        
        
        let dateStr = document.date?.getString() ?? ""
        date.text = dateStr//document.date?.getString() ?? ""
        size.text = ""
        self.checkFileExist(document, arrNames: DigiSessionManager.instance.arrFileName)

//        if  document.type == DocType.file.rawValue {
//            date.text = date.text! + "   " + document.size.getSize()
//            lblDateNewOne.text = dateStr//document.date?.getString() ?? ""
//        }
        self.widthSizeLabelConstraint.constant = 40.0
        if segment == .issued {
            date.text = document.issuer
            lblDateNewOne.text = dateStr
            self.widthSizeLabelConstraint.constant = 0.0
        }else {
            lblDateNewOne.text =  dateStr
            size.text = document.size.getSize()
            date.text = self.getFileName(docType: typeTupple.0).rawValue.localized
            if  document.type != DocType.file.rawValue {
                size.text = ""
                self.widthSizeLabelConstraint.constant = 0.0
            }
        }
        leftImageView.contentMode = .scaleAspectFit
        leftImageView.image = UIImage(named:typeTupple.image)
        imageGridView.image =  leftImageView.image
        imageProgressGrid.image = leftImageView.image
        let imageMenu = self.checkFileOrDocument(type: document.type)
        btnMenu.setImage(UIImage(named:imageMenu.image), for: .normal)
        
    }
    
    func checkFileOrDocument(type:String) -> (DocType, image:String) {
        if type.contains(DocType.file.rawValue) {
            return (DocType.file,"menu_AadharLink.png")
        }
        else if type.contains(DocType.folder.rawValue) {
            return (DocType.folder,"digiRightArrow")
        }
        return (DocType.file,"menu_AadharLink.png")
    }

    func setupGridLayoutConstraints(_ transitionProgress: CGFloat, cellWidth: CGFloat) {
//        avatarImageViewHeightConstraint.constant = ceil((cellWidth - avatarListLayoutSize) * transitionProgress + avatarListLayoutSize)
//        avatarImageViewWidthConstraint.constant = ceil(avatarImageViewHeightConstraint.constant)
        leadingListContentConstraint.constant = -avatarImageViewWidthConstraint.constant * transitionProgress + initialLabelsLeadingConstraintValue
        self.alpha = transitionProgress <= 0.5 ? 1 - transitionProgress : transitionProgress
        listContentView.alpha = 1 - transitionProgress
        leftImageView.alpha = listContentView.alpha
        gridContentView.alpha = 1
        imageGridDownloaded.alpha = imageGridView.alpha
        lblBottomLine.alpha = listContentView.alpha

    }
    func setupListLayoutConstraints(_ transitionProgress: CGFloat, cellWidth: CGFloat) {
        avatarImageViewHeightConstraint.constant = ceil(avatarGridLayoutSize - (avatarGridLayoutSize - avatarListLayoutSize) * transitionProgress)
        avatarImageViewWidthConstraint.constant = avatarImageViewHeightConstraint.constant
        leadingListContentConstraint.constant = avatarImageViewWidthConstraint.constant * transitionProgress + (initialLabelsLeadingConstraintValue - avatarImageViewHeightConstraint.constant)
        
        self.alpha = transitionProgress <= 0.5 ? 1 - transitionProgress : transitionProgress
        listContentView.alpha = transitionProgress
        gridContentView.alpha = 0
        leftImageView.alpha = listContentView.alpha
        imageDownloaded.alpha = leftImageView.alpha
        lblBottomLine.alpha = listContentView.alpha
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DisplaySwitchLayoutAttributes {
            if attributes.transitionProgress > 0 {
                if attributes.layoutState == .grid {
                    setupGridLayoutConstraints(attributes.transitionProgress, cellWidth: attributes.nextLayoutCellFrame.width)
                    avatarGridLayoutSize = attributes.nextLayoutCellFrame.width
                } else {
                    setupListLayoutConstraints(attributes.transitionProgress, cellWidth: attributes.nextLayoutCellFrame.width)
                }
            }
        }
    }
}
// MARK:--  **Progress Methods Here ***
extension CellDocumentList {
    func updateProgressOnMainQueue(_ progress:Float)  {
        if self.vwPrgressList.isHidden == true || self.vwProgressGrid.isHidden == true  {
           // DispatchQueue.main.async {[weak self] in
                self.showProgress()
                self.updateProgress(progress)
           // }
            return
        }else {
           // DispatchQueue.main.async {[weak self] in
                self.updateProgress(progress)
            //}
        }
    }
    func showProgress(){
        self.gridContentView.bringSubview(toFront: self.vwProgressGrid)
        self.listContentView.bringSubview(toFront: self.vwPrgressList)
        self.vwProgressGrid.layer.borderWidth = 0.8
        self.vwProgressGrid.layer.borderColor = UIColor.lightGray.cgColor
        self.progressHiddenUpdate(hide: true)
    }
    
    
    func updateProgress(_ progress:Float){
        if progress == 1.0 {
            self.progressCompleted()
            return
        }
        let progressPercent = Int(progress*100)
        if  self.progressGrid != nil {
            self.isUserInteractionEnabled = false
            self.progressGrid!.progress = Float(progress)
            self.lblProgressGrid.text = "\(progressPercent)%"
        }
        if  self.progressList != nil {
            self.isUserInteractionEnabled = false
            self.progressList!.progress = Float(progress)
            self.lblProgressList.text = "\(progressPercent)%"
        }
     }
    func progressCompleted() {
        DispatchQueue.main.async {[weak self] in
            self?.progressHiddenUpdate(hide: false)
        }
    }
    func progressHiddenUpdate(hide:Bool) {
        let alpha:CGFloat = hide ? 0.60 : 1.0
        self.nameListLabel.alpha = alpha
        self.nameGridLabel.alpha = alpha
        self.imageGridView.alpha = alpha
        self.leftImageView.alpha = alpha
        self.vwPrgressList.isHidden = !hide
        self.vwProgressGrid.isHidden = !hide
        self.progressList.isHidden = !hide
        self.progressGrid.isHidden = !hide
        self.lblProgressList.isHidden = !hide
        self.lblProgressGrid.isHidden = !hide
        self.date.isHidden = hide
        self.size.isHidden = hide
        self.btnMenu.isHidden = hide
        self.btnMenuGrid.isHidden = hide
        self.isUserInteractionEnabled = !hide
        
        //let height :CGFloat = hide ? 40.0 : 60.0
        //self.gridImageWidthConstraint.constant = height
       // self.gridImageHeightConstraint.constant = height
        self.layoutIfNeeded()
        self.updateConstraintsIfNeeded()
    }
    
}

extension CellDocumentList {
    func checkFileExist(_ doc:LockerDocument, arrNames:[String])
    {
        if arrNames.count != 0  && arrNames.contains(doc.uri){
            let logsPath = self.getDigiFolderPath()
            var fileName =  doc.uri + "(UMANG)" + doc.name
            fileName = "\(logsPath)/\(fileName)"
            fileName = fileName.removeBlankSpace()
            doc.localPath = fileName
            doc.downloaded = true
            self.imageDownloaded.isHidden = true
            self.imageGridDownloaded.isHidden = true
            self.btnDownloaded.isHidden = false
            self.btnDownloaded.setTitle("Downloaded_downtime".localized, for: .normal)
            self.btnDownloaded.setImage(self.imageDownloaded.image, for: .normal)
            return
        }
        self.btnDownloaded.isHidden = true
        self.imageDownloaded.isHidden = true
        self.imageGridDownloaded.isHidden = true

    }
    func getDigiFolderPath() -> String {
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        guard let documentPath = documentDirectoryPath  else {
            return ""
        }
        // create the custom folder path
        return  documentPath.appending("/DigiLocker")
    }
}

