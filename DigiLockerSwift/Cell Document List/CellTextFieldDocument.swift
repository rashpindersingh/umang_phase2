//
//  CellTextFieldDocument.swift
//  UMANG
//
//  Created by Rashpinder on 14/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

class CellTextFieldDocument: UITableViewCell {

    
    @IBOutlet weak var lblExample: UILabel!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var txtInputUser: UMTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

@IBDesignable class UMTextField: UITextField {

    var indexPath :IndexPath!
    @IBInspectable var lineWidth: CGFloat = 1.5 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var lineColor: UIColor = UIColor.lightGray {
        didSet {
            self.setNeedsDisplay()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        //setting left image
      // self.draw(self.frame)
    }
    override func draw(_ rect: CGRect)
    {
        let height = self.bounds.height
        // get the current drawing context
        let context = UIGraphicsGetCurrentContext()
        // set the line color and width
        
        context?.setStrokeColor(lineColor.cgColor)
        context?.setLineWidth(lineWidth)
        
        // start a new Path
        context?.beginPath()
        
        context!.move(to: CGPoint(x: 0, y: (height + lineWidth) - lineWidth))
        context!.addLine(to: CGPoint(x: self.bounds.size.width, y: (height + lineWidth) - lineWidth))
        // close and stroke (draw) it
        context?.closePath()
        context?.strokePath()
    }
}
