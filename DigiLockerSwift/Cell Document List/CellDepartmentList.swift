//
//  CellDepartmentList.swift
//  UMANG
//
//  Created by Rashpinder on 13/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

class CellDepartmentList: UITableViewCell {

    @IBOutlet weak var lblCompleteName: UILabel!
    @IBOutlet weak var btnName: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnName.layer.cornerRadius = self.btnName.frame.width / 2.0
        self.btnName.layer.masksToBounds = true;
        self.btnName.backgroundColor = self.getRandomColor()
        self.btnName.setTitleColor(UIColor.white, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func getRandomColor() -> UIColor{
        
//        var randomRed:CGFloat = CGFloat(drand48())
//
//        var randomGreen:CGFloat = CGFloat(drand48())
//
//        var randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: 1.0)
    }

}
