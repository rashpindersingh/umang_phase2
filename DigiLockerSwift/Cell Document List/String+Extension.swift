//
//  string+Extension.swift
//  PassengerApp
//
//  Created by Netquall on 12/16/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import Foundation
import UIKit


extension NSAttributedString {
	
	func append (_ left: NSAttributedString, right: NSAttributedString) -> NSAttributedString
	{
		let result = NSMutableAttributedString()
		result.append(left)
		result.append(right)
		return result
	}
}
extension String {
	
	func firstCharacterUpperCase() -> String {
		let lowercaseString = self.lowercased()
		
		return lowercaseString.firstCharacterUpperCase()
		//lowercaseString.replacingCharacters(in: lowercaseString.startIndex...lowercaseString.startIndex, with: String(lowercaseString[lowercaseString.startIndex]).uppercased())
	}
	func urlencode() -> String {
		let urlEncoded = self.replacingOccurrences(of: " ", with: "+")
		return urlEncoded.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
	}
	
	//func hmacsha1(key: String) -> String {
	//
	//    let encoding = GTMStringEncoding.rfc4648Base64WebsafeStringEncoding()
	//
	//    let dataToDigest = self.dataUsingEncoding(NSASCIIStringEncoding)
	//    let secretKey = encoding.decode(key)
	//    let digestLength = Int(CC_SHA1_DIGEST_LENGTH)
	//    let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLength)
	//    CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA1), secretKey!.bytes, secretKey!.length, dataToDigest!.bytes, dataToDigest!.length, result)
	//    let binarySignData = NSData(bytes: result, length: digestLength)
	//    let strSignature = encoding.encode(binarySignData)
	//    return strSignature
	//    }
	///===== convert string to bool =====
	func toBool() -> Bool? {
		switch self {
		case "True", "true", "yes", "1":
			return true
		case "False", "false", "no", "0":
			return false
		default:
			return nil
		}
	}
	
	
	func NSRangeFromRange(_ range : Range<String.Index>) -> NSRange {
		let utf16view = self.utf16
		let from = String.UTF16View.Index(range.lowerBound, within: utf16view)
		let to = String.UTF16View.Index(range.upperBound, within: utf16view)
        return NSMakeRange(utf16view.startIndex.encodedOffset, utf16view.count)//NSMakeRange(utf16view.startIndex.distance(to:from), from!.distance(to: to))
		
	}
 func compareTwoString(_ strAnother:String?)-> Bool {
	
	if strAnother != nil {
		if self.caseInsensitiveCompare(strAnother!) == ComparisonResult.orderedSame {
			return true
		}
	}
	return false
	}
	//New Changes
	func toDouble() -> Double? {
		if !self.isEmpty{
			return Double(self);
		}
		else{
			return 0.0
		}
	}
	
	func toInteger() -> Int {
		if !self.isEmpty{
			return Int(self) ?? 0
		}
		else{
			return 0
		}
	}
	
	func toFloat() -> Float {
		if !self.isEmpty{
			return Float(self)!
		}
		else{
			return 0.0
		}
	}

    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    //Validate Email
    
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    //validate Password
    var isValidPassword: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
            if(regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil){
                
                if(self.characters.count>=6 && self.characters.count<=20){
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        } catch {
            return false
        }
    }
    var isPhoneNumber : Bool {
        let phoneRegex = "[789][0-9]{3}([0-9]{6})?"
        let test = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return test.evaluate(with: self)
    }
    
    var validatePhoneNumber : Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
    // MARK:-=== Localize String ===
    var localized: String {
        let strLocal = NSLocalizedString(self, comment: "")
        //🖕Fuck the translators team, they don’t deserve comments
        return strLocal
    }
    
    func removeBlankSpace() -> String {
          return self.replacingOccurrences(of: " ", with: "")
    }
    func removeNewLineWithBlankSpace() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    func getArray(WithComponet componet:String ) -> [String] {
        return self.components(separatedBy: componet)
    }
    func getDate() -> Date? {
        var dateFormat :DateFormatter? = DateFormatter()
        dateFormat?.dateFormat = "yyyy-MM-dd"
        var date = dateFormat!.date(from: self)
        if  date == nil {
            dateFormat?.dateFormat = "dd-MM-yyyy"
            date = dateFormat!.date(from: self)
        }
        dateFormat = nil
        return date
    }
    func getSize() -> String
    {
    var convBytes: Double = self.toDouble() ?? 0
        switch convBytes {
        case 0...1024.0 :
            return "\(Int(convBytes))" + " bytes"
        case 1024...1048576 :
            convBytes = convBytes / 1024
            //kb
            let size = convBytes.rounded(.toNearestOrEven)
            return "\(Int(size))" + " kb"
        case 1048576...1073741824 :
            convBytes = convBytes / 1024 / 1024
            //kb
            let size = convBytes.rounded(.toNearestOrEven)
            return "\(Int(size))" + " mb"
        default:
            convBytes = convBytes / 1024 / 1024 / 1024
            //kb
            let size = convBytes.rounded(.toNearestOrEven)
            return "\(Int(size))" + " gb"
        }
    }
    
}
extension Date {
    func getString() -> String? {
        var dateFormat :DateFormatter? = DateFormatter()
        dateFormat?.dateFormat = "dd MMM, yyyy"//"//"dd-MM-yyyy"
        let strDate = dateFormat!.string(from: self)
//        if  strDate == nil {
//            dateFormat?.dateFormat = "dd-MM-yyyy"
//            strDate = dateFormat!.string(from: self)
//        }
        dateFormat = nil
        return strDate
    }
   
}
extension Array where Element == LockerDocument{
    func indexOf(object: LockerDocument) -> Int {
        for (idx, objectToCompare) in self.enumerated() {
            if let to = objectToCompare as? LockerDocument {
                if object.uri == to.uri {
                    return idx
                }
            }
        }
        return 10000
    }
    func sortWithName(_ arr:[LockerDocument]) -> [LockerDocument] {
        let arrSorted = arr.sorted{$0.name.compare($1.name, options: .caseInsensitive) == .orderedAscending }
        let arrFolder = arrSorted.filter({ (doc) -> Bool in
            return doc.type == DocType.folder.rawValue
        })
        let arrFile = arrSorted.filter({ (doc) -> Bool in
            return doc.type == DocType.file.rawValue
        })
        return arrFolder + arrFile
    }
    func sortWithDate(_ arr:[LockerDocument]) -> [LockerDocument] {
        let arrSorted = arr.sorted() {$0.date!.timeIntervalSince1970 > $1.date!.timeIntervalSince1970}
        let arrFolder = arrSorted.filter({ (doc) -> Bool in
            return doc.type == DocType.folder.rawValue
        })
        let arrFile = arrSorted.filter({ (doc) -> Bool in
            return doc.type == DocType.file.rawValue
        })
        return arrFolder + arrFile
    }
    func removeExistingDownloadUpload() -> [LockerDocument] {
        typealias Element = LockerDocument
        let arrSorted = self.flatMap { (doc:LockerDocument) -> LockerDocument in
            if doc.downloading  {
                doc.index = nil
                doc.cell = nil
                doc.downTask?.completionHandler = nil
                doc.downTask?.progressHandler = nil
            }
            if doc.uploading  {
                doc.index = nil
                doc.cell = nil
                doc.uploadTask?.completionHandler = nil
                doc.uploadTask?.progressHandler = nil
            }
            return doc
        }
        return arrSorted;
    }
}
extension Data {
    private static let mimeTypeSignatures: [UInt8 : String] = [
        0xFF : "image/jpeg",
        0x89 : "image/png",
        0x47 : "image/gif",
        0x49 : "image/tiff",
        0x4D : "image/tiff",
        0x25 : "application/pdf",
        0xD0 : "application/vnd",
        0x46 : "text/plain",
        ]
    
    var mimeType: String {
        var c: UInt8 = 0
        copyBytes(to: &c, count: 1)
        return Data.mimeTypeSignatures[c] ?? "application/pdf"
    }
    func getSize() -> Float {
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
        bcf.countStyle = .file
        let string = bcf.string(fromByteCount:Int64(self.count))
        if let countString = string.components(separatedBy: " MB").first {
            print("countString count --\(countString.toFloat())")
            return countString.toFloat()
        }
        return 0

    }
}
extension NSObject {
    
    //MARK:----==== Check Server Response Success =====
    func checkSuccessResponse(dicResponse:[AnyHashable:Any]) -> Bool {
        if  let rs1 = dicResponse[ResponseKeys.API_SUCCESS_CASE1] as? String {
            if rs1 == ResponseResult.API_SUCCESS_CASE1 || rs1 == ResponseResult.API_SUCCESS_CASE1{
                return true
            }
        }
        if let rs = dicResponse[ResponseKeys.SUCCESS] as? String  {
            if rs == ResponseResult.API_SUCCESS_CASE1 || rs == ResponseResult.API_SUCCESS_CASE1 {
                return true
            }
        }
        return false
    }
    func saveFileToLocalPath(_ doc:LockerDocument, data:Data)-> String  {
        let pdfDocumentData = data
        let mimeType: String = data.mimeType
        var logsPath = ""
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        if let documentDirectoryPath = documentDirectoryPath {
            // create the custom folder path
            logsPath = documentDirectoryPath.appending("/DigiLocker")
            print(logsPath)
            let fileManager = FileManager.default
            if !fileManager.fileExists(atPath: logsPath)
            {
                do {
                    try fileManager.createDirectory(atPath: logsPath,
                                                    withIntermediateDirectories: false,
                                                    attributes: nil)
                } catch {
                    print("Error creating images folder in documents dir: \(error)")
                }
            }
        }
        
        var selectedFileName = ""
        if doc.name.contains(DocType.jpg.rawValue) || doc.name.contains(DocType.pdf.rawValue) || doc.name.contains(DocType.png.rawValue) || doc.name.contains(DocType.jpeg.rawValue) {
            selectedFileName = doc.name
        }
        else {
            selectedFileName = "\(doc.name).\(mimeType.components(separatedBy: "/")[1])"
        }
        let fileName = doc.uri + "(UMANG)" + selectedFileName
        
        var strDocumentPath = "\(logsPath)/\(fileName)"
        strDocumentPath = strDocumentPath.removeBlankSpace()
        print("documentPath --\(strDocumentPath)")
        let base64String: String = pdfDocumentData.base64EncodedString()
        let base64data = Data.init(base64Encoded: base64String)
        do {
            try base64data!.write(to: URL(fileURLWithPath: strDocumentPath), options: .atomic)
        } catch {
            print("base64data save digiFolder--=\(error)")
        }
        DigiSessionManager.instance.arrFileName =  DigiSessionManager.instance.getFileNames()
        return strDocumentPath
    }
    func saveUploadedFileToLocalPath(_ doc:LockerDocument, data:Data)-> String  {
        let pdfDocumentData = data
        let mimeType: String = data.mimeType
        var logsPath = ""
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        if let documentDirectoryPath = documentDirectoryPath {
            // create the custom folder path
            logsPath = documentDirectoryPath.appending("/DigiLocker")
            print(logsPath)
            let fileManager = FileManager.default
            if !fileManager.fileExists(atPath: logsPath)
            {
                do {
                    try fileManager.createDirectory(atPath: logsPath,
                                                    withIntermediateDirectories: false,
                                                    attributes: nil)
                } catch {
                    print("Error creating images folder in documents dir: \(error)")
                }
            }
        }
        
        var selectedFileName = ""
        if doc.name.contains(DocType.jpg.rawValue) || doc.name.contains(DocType.pdf.rawValue) || doc.name.contains(DocType.png.rawValue) || doc.name.contains(DocType.jpeg.rawValue) {
            selectedFileName = doc.name
        }
        else {
            selectedFileName = "\(doc.name).\(mimeType.components(separatedBy: "/")[1])"
        }
        let fileName = selectedFileName
        var strDocumentPath = "\(logsPath)/\(fileName)"
        strDocumentPath = strDocumentPath.removeBlankSpace()
        print("documentPath --\(strDocumentPath)")
        let base64String: String = pdfDocumentData.base64EncodedString()
        let base64data = Data.init(base64Encoded: base64String)
        do {
            try base64data!.write(to: URL(fileURLWithPath: strDocumentPath), options: .atomic)
        } catch {
            print("base64data save digiFolder--=\(error)")
        }
        DigiSessionManager.instance.arrFileName =  DigiSessionManager.instance.getFileNames()
        return strDocumentPath
    }
    func checkFileTypes(mime:String) -> (DocType, image:String) {
        if mime.contains(DocType.jpg.rawValue) {
            return (DocType.jpg,"jpgIcon")
        }
        else if mime.contains(DocType.png.rawValue) {
            return (DocType.png,"pngIcon")
        }
        else if mime.contains(DocType.pdf.rawValue) {
            return (DocType.pdf,"pdfIcon")
        }
        else if mime.contains(DocType.folder.rawValue) {
            return (DocType.folder,"folderIcon")
        }
        else if mime.contains(DocType.image.rawValue) {
            return (DocType.image,"jpgIcon")
        }
        
        return (DocType.other, "folderIcon")
    }
    func getFileName(docType:DocType) -> DocTypeName {
        switch docType {
        case .file,.folder:
            return DocTypeName.file
        case .image:
            return DocTypeName.image
        case .jpeg:
            return DocTypeName.jpeg
        case .jpg:
            return DocTypeName.jpg
        case .png:
            return DocTypeName.png
        case .pdf,.pdfApplication:
            return DocTypeName.pdf
        default:
            return DocTypeName.file
        }
    }
    func loadCockieArray() -> [Any]? {
        let cookieStorage = HTTPCookieStorage.shared
        cookieStorage.cookieAcceptPolicy = .always
        return HTTPCookieStorage.shared.cookies
    }
    
}
extension URLRequest {
    mutating func addCookies(_ cookies: [Any]) {
        if  cookies.count > 0 {
            var cookieHeader: String? = nil
            for cookie in cookies as! [HTTPCookie]{
                if cookieHeader == nil {
                    cookieHeader = "\(cookie.name)=\(cookie.value)"
                }
                else {
                    cookieHeader = "\(String(describing: cookieHeader)); \(cookie.name)=\(cookie.value)"
                }
            }
            if cookieHeader != nil {
                self.setValue(cookieHeader, forHTTPHeaderField: "Cookie")//setValue(cookieHeader, forHTTPHeaderField: "Cookie")
            }
        }
    }
}
struct ResponseResult {
    static let API_SUCCESS_CASE = "SU"
    static let API_FAILURE_CASE = "FL"
    static let API_FAILURE_CASE1 = "F"
    static let API_SUCCESS_CASE1 = "S"
    static let HEADER_X_VALUE = "X-REQUEST-VALUE"
}
//MARK:- ==== Success  Response Keys  ====

struct ResponseKeys {
    static let SUCCESS = "rs"
    static let FAILURE = "rd"
    static let API_SUCCESS_CASE1 = "S"
    static let PdData = "pd"
    static let token = "tkn"
}
