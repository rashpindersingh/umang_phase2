//
//  DigiLockerFileModel.swift
//  UMANG
//
//  Created by Lokesh Jain on 15/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

@objc protocol DataModelDelegate: class
{
    func didRecieveDataUpdate(data: NSMutableDictionary)
}

@objc class DigiLockerFileModel: NSObject {

    public weak var delegate: DataModelDelegate?
    var callBack = ""
    var type = ""
    var digiHome : DigiLockerHomeVC!
    var singleton = SharedManager.sharedSingleton() as SharedManager
    var delegateReference :UIViewController!
    func requestData(dataDict : NSMutableDictionary)
    {
        delegate?.didRecieveDataUpdate(data: dataDict)
        self.dissmissDigiLockerHome()
    }
    func dissmissDigiLockerHome()  {
        if self.digiHome != nil {
            self.digiHome.dismiss(animated: true, completion: nil)
        }
    }
    func passReferenceToDigilocker(delegateReference :UIViewController)
    {
        self.delegateReference = delegateReference
        let storyboard = UIStoryboard(name: "DigiLocker", bundle: nil)
        self.digiHome = storyboard.instantiateViewController(withIdentifier: "DigiLockerHomeVC") as? DigiLockerHomeVC
        self.digiHome.hidesBottomBarWhenPushed = true;
        self.digiHome.fileDelegate = self
        self.digiHome.isSelectDepartment = true;
        let navi = UINavigationController(rootViewController: self.digiHome)
        navi.hideNavigation()
        
        var accessStr = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String ?? ""
        
         let  dlink = singleton.objUserProfile.objGeneral.dlink ?? ""
        
        if accessStr.lowercased() == "y" || dlink.lowercased() == "y" {
            delegateReference.present(navi, animated: true, completion: nil)
        }else {
            //self.pushToMenuScreen()
            delegateReference.present(navi, animated: false, completion: nil)
        }
       
    }
    
    func dissmissAfterLoginView(sender:UIViewController)  {
        
    }
}
