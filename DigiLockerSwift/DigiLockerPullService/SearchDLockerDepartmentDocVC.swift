//
//  SearchDLockerDepartmentDocVC.swift
//  UMANG
//
//  Created by Rashpinder on 13/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

let lightGrayColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)


class SearchDLockerDepartmentDocVC: UIViewController {
    // MARK:- **** Property  ****

    @IBOutlet weak var naviView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet var headerView: UIView!
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    var singleton = SharedManager.sharedSingleton() as! SharedManager
    var hud :MBProgressHUD!
    var arrIssuerList : [[String:String]] = []
    var arrSearchResult : [[String:String]] = []
    var fileDelegate = DigiLockerFileModel()
    var isSelectDepartment = false
    var searchActive : Bool = false

    // MARK:- ****   View Life Cycle  ****
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblList.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tblList.estimatedSectionHeaderHeight = 50
        self.fetchIssuerList()
        self.tblList.layoutIfNeeded()
        self.tblList.reloadData()
        self.tblList.tableFooterView = UIView(frame: CGRect.zero)
        self.setViewFont()
        self.searchBar.placeholder = "search".localized
        searchBar.setValue("cancel".localized, forKey:"_cancelButtonText")
        self.searchBar.showsCancelButton = false
        //self.searchBar.textFieldSearchBar.show
        // Do any additional setup after loading the view.
    }
    
    func setViewFont()   {
        self.lblTitle.font = AppFont.semiBoldFont(17)
        self.btnBack.titleLabel?.font = AppFont.regularFont(17)
        self.lblHeaderTitle.font = AppFont.lightFont(15)
        self.lblHeaderTitle.textColor = lightGrayColor
          self.btnBack.setTitle("back".localized, for: .normal)
        self.lblTitle.text = "list_issuers_digi".localized
        self.lblHeaderTitle.text = "searc_doc_partnertxt_digi".localized
    }
    @IBAction func didTapBackButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        searchActive = false
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- Call API's
extension SearchDLockerDepartmentDocVC {
    func fetchIssuerList()
    {
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        // var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
        // accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
        //dictBody["ort"] = "rgtadhr"
        //dictBody["rc"] = "Y"
        // dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
        //  dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        self.callApi(with: dictBody, url: UM_API_GETDIGILOCKER_ISSUERLIST)
    }
    func callApi(with dicBody:NSMutableDictionary, url:String) {
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        objRequest.hitAPIForDigiLockerAuthentication(withPost: true, isAccessTokenRequired: true, webServiceURL: url, withBody: dicBody, andTag: TAG_REQUEST_DIGILOCKER_GETTOKEN, completionHandler: {[weak self](_ response: Any, _ error: Error?, _ tag: REQUEST_TAG) -> Void in
            guard let er = error else {
                guard let responseJson = response as? [String:Any] else {
                    self?.hud.hide(animated: true)
                    return
                }
                self?.checkResponse(responseJson)
                return
            }
            self?.hud.hide(animated: true)
            self?.showErrorAlert(error: er)
        })
    }
    func checkResponse(_ response:[String:Any])  {
        self.hud.hide(animated: true)
        let dicFormat = response.formatDictionaryForNullValues(response)
        if self.checkSuccessResponse(dicResponse: dicFormat!) {
            self.processResponse(dicFormat!)
        }
    }
    func processResponse(_ dicJson:[String:Any])  {
        print("API Success")
        guard let pdJson = dicJson["pd"] as? [String:Any] else {
            return
        }
        guard let itemsJson = pdJson["issuers"] as? [[String:String]] else {
            return
        }
        self.arrIssuerList = itemsJson
        DispatchQueue.main.async {[weak self] in
            self?.tblList.reloadData()
        }
        
    }
    func reloadCollectionOnMain() {
        self.tblList.reloadData()

//        DispatchQueue.main.async {[weak self] in
//        }
    }
}
extension SearchDLockerDepartmentDocVC :UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        
        return self.searchActive ? arrSearchResult.count : arrIssuerList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellDepartmentList", for: indexPath) as! CellDepartmentList
        let item = self.searchActive ? arrSearchResult[indexPath.row] : arrIssuerList[indexPath.row]
        let name = item.string(key: "name")
        let firstLetter = "\(name.first!)"
        cell.lblCompleteName.font = AppFont.regularFont(16)
        cell.lblCompleteName.textColor = lightGrayColor
        cell.lblCompleteName.text = name
        cell.lblCompleteName.sizeToFit()
        cell.btnName.setTitle(firstLetter, for: .normal)
        return cell
    }
    
}
extension SearchDLockerDepartmentDocVC :UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let item = arrIssuerList[indexPath.row]
         let item = self.searchActive ? arrSearchResult[indexPath.row] : arrIssuerList[indexPath.row]
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectDLockerDepartmentDocVC") as! SelectDLockerDepartmentDocVC
        vc.itemSelected = item
        vc.isSelectDepartment = self.isSelectDepartment
        vc.fileDelegate = self.fileDelegate
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return self.headerView
        }else {
            return self.searchBar
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
extension SearchDLockerDepartmentDocVC :UISearchBarDelegate {
    //MARK: Search Bar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = ""
        searchActive = false
        searchBar.resignFirstResponder()
        self.reloadCollectionOnMain()
        
        //self.dismiss(animated: true, completion: nil)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchActive = false
        searchBar.setShowsCancelButton(false, animated: true)
        self.reloadCollectionOnMain()
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
       // searchActive = true
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
      //  searchActive = true
       // self.reloadCollectionOnMain()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.setShowsCancelButton(false, animated: true)

        //dismissKeyboard()
        self.reloadCollectionOnMain()
    }
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if !searchActive {
            searchActive = true
            self.reloadCollectionOnMain()
        }
        //dismissKeyboard()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
          searchActive = true

        let searchString = searchText
        
        if  searchString.isEmpty == false  {
            self.arrSearchResult = self.arrIssuerList.filter({ (json) -> Bool in
                return json.string(key: "name").lowercased().contains(searchString.lowercased())
            })
//            self.arrSearchResult = self.arrDocuments.filter({ (locker) -> Bool in
//                return locker.name.lowercased().contains(searchString.lowercased())
//            })
        }else {
            searchActive = false
            self.arrSearchResult.removeAll()
        }
        self.reloadCollectionOnMain()

    }
}
