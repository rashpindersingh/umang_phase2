//
//  SelectDLockerDepartmentDocVC.swift
//  UMANG
//
//  Created by Rashpinder on 13/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit

class SelectDLockerDepartmentDocVC: UIViewController {

    
    @IBOutlet weak var naviView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblList: UITableView!
    
    @IBOutlet var headerView: UIView!
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    var singleton = SharedManager.sharedSingleton() as SharedManager
    var hud :MBProgressHUD!
    var itemSelected = [String:String]()
    var arrIssuerList : [[String:String]] = []
    var fileDelegate = DigiLockerFileModel()
    var isSelectDepartment = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblList.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tblList.estimatedSectionHeaderHeight = 50
        self.tblList.layoutIfNeeded()

        
        self.fetchDocumentsList()
       ///  self.lblHeaderTitle.text = "Search the document using the dummy text o show the header of Search the document using the dummy text o show the header of "
        self.tblList.reloadData()
        self.tblList.tableFooterView = UIView(frame: CGRect.zero)
        self.setViewFont()
        
        // Do any additional setup after loading the view.
    }
    
    func setViewFont()   {
        self.lblTitle.font = AppFont.semiBoldFont(17)
        self.btnBack.titleLabel?.font = AppFont.regularFont(17)
           self.btnBack.setTitle("back".localized, for: .normal)
        self.lblTitle.text = "list_of_doc_digi".localized

    }
    @IBAction func didTapBackButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    func setAttributedTitle()
    {
        
        let name = itemSelected.string(key: "name")
        

        let namewithbold = "<b>"+name+"</b>"

        var htmlString = String(format: "list_doc_digi_arg".localized,namewithbold)

        defer {
        }
        
        htmlString =   htmlString + (String(format: "<style>body{font-size:%fpx;}</style>",  AppFont.boldFont(17.5).pointSize))
        
        
        
        
        
        var attrStr: NSMutableAttributedString? = nil
        if let anEncoding = htmlString.data(using: String.Encoding(rawValue: String.Encoding.unicode.rawValue)) {
            do {
                     attrStr = try NSMutableAttributedString(data: anEncoding, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
            }catch {
                print("error html document digilocker \(error)")
            }
     
        }
        
        let combineString = String(format: "\n\n%@", "Select_doc_list_digi".localized)
        
        let lastAttributes = NSAttributedString(string: combineString, attributes: [NSForegroundColorAttributeName: UIColor.gray,  NSFontAttributeName:AppFont.lightFont(14)] as [String : Any])
        
        
        attrStr?.append(lastAttributes)
            
        self.lblHeaderTitle.attributedText = attrStr
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- Call API's
extension SelectDLockerDepartmentDocVC {
    func fetchDocumentsList()
    {
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        // var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
        // accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
        //dictBody["ort"] = "rgtadhr"
        //dictBody["rc"] = "Y"
        // dictBody["mec"] = "Y"
       // &quot;orgid&quot;:&quot;002024&quot;
        dictBody["tkn"] = singleton.user_tkn
        dictBody["orgid"] = itemSelected.string(key: "orgid")

        //  dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        self.callApi(with: dictBody, url: UM_API_GETDOCUMENTLIST_FROMISSUER)
    }
    func callApi(with dicBody:NSMutableDictionary, url:String) {
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        objRequest.hitAPIForDigiLockerAuthentication(withPost: true, isAccessTokenRequired: true, webServiceURL: url, withBody: dicBody, andTag: TAG_REQUEST_DIGILOCKER_GETTOKEN, completionHandler: {[weak self](_ response: Any, _ error: Error?, _ tag: REQUEST_TAG) -> Void in
            guard let er = error else {
                guard let responseJson = response as? [String:Any] else {
                    self?.hud.hide(animated: true)
                    return
                }
                self?.checkResponse(responseJson)
                return
            }
            self?.hud.hide(animated: true)
            self?.showErrorAlert(error: er)
        })
    }
    func checkResponse(_ response:[String:Any])  {
        self.hud.hide(animated: true)
        let dicFormat = response.formatDictionaryForNullValues(response)
        if self.checkSuccessResponse(dicResponse: dicFormat!) {
            self.processResponse(dicFormat!)
        }
    }
    func processResponse(_ dicJson:[String:Any])  {
        print("API Success")
        guard let pdJson = dicJson["pd"] as? [String:Any] else {
            return
        }
        guard let itemsJson = pdJson["documents"] as? [[String:String]] else {
            return
        }
        self.arrIssuerList = itemsJson
        DispatchQueue.main.async {[weak self] in
            self?.setAttributedTitle()

            self?.tblList.reloadData()
        }
        
    }
}
extension SelectDLockerDepartmentDocVC :UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrIssuerList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellDepartmentList", for: indexPath) as! CellDepartmentList
        
        let item = arrIssuerList[indexPath.row]
        let name = item.string(key: "description")
        let firstLetter = "\(name.first!)"
        cell.lblCompleteName.text = name
        cell.lblCompleteName.font = AppFont.regularFont(16)
        cell.lblCompleteName.textColor = lightGrayColor
        cell.btnName.setTitle(firstLetter, for: .normal)
        return cell
    }
    
}
extension SelectDLockerDepartmentDocVC :UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let json = arrIssuerList[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DownloadDLokcerDocVC") as! DownloadDLokcerDocVC
        vc.itemSelected = self.itemSelected
        vc.docSelected = json
        vc.isSelectDepartment = self.isSelectDepartment
        vc.fileDelegate = self.fileDelegate
        self.navigationController?.pushViewController(vc, animated: true);
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerView
    }
    
}
