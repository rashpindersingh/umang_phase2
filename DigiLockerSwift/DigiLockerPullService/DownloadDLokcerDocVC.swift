//
//  DownloadDLokcerDocVC.swift
//  UMANG
//
//  Created by Rashpinder on 13/12/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import IQKeyboardManager

enum TextFieldType {
    case picker
    case normal
}
class PickerTextField: UIPickerView {
    
    var txtField :UITextField!
   
}
//&quot;label&quot;: &quot;Roll No.&quot;,
//&quot;paramname&quot;: &quot;RROLL&quot;,
//&quot;valuelist&quot;: null,
//&quot;example&quot;: &quot;M20XXXX1&quot;

class TextInputDoc {
    
    var type = TextFieldType.normal
    var lblValue = ""
    var arrPickerValues = [String]()
    var example = ""
    var inputValue = ""
    var paramName = ""
    var indexPath :IndexPath?
    init(dicLocker:[String:String]){
       self.lblValue = dicLocker.string(key: "label")
        self.example = dicLocker.string(key: "example")
        let valueList = dicLocker.string(key: "valuelist")
        if  valueList.count != 0 {
            self.arrPickerValues = valueList.components(separatedBy: ",")
        }
        self.paramName = dicLocker.string(key: "paramname")
        if self.arrPickerValues.count != 0 {
            self.type = .picker
        }
        self.indexPath = nil
    }
    
}


class DownloadDLokcerDocVC: UIViewController {

    @IBOutlet weak var naviView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblList: UITableView!
    
    @IBOutlet weak var pickerView: PickerTextField!
    @IBOutlet var viewPicker: UIView!
    @IBOutlet var headerView: UIView!
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblAgreeConsent: UILabel!
    @IBOutlet weak var btnGetDoc: UIButton!
    
    @IBOutlet weak var btnConsent: UIButton!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var btnCancelDownload: UIButton!
    
    @IBOutlet weak var progressDownload: UIProgressView!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var viewDownloadDoc: UIView!
    
    @IBOutlet weak var lblFileName: UILabel!
    var singleton = SharedManager.sharedSingleton() as SharedManager
    var hud :MBProgressHUD!
    var itemSelected = [String:String]()
    var docSelected = [String:String]()
    var arrPickerDataSource = [String]()
    var arrIssuerList : [TextInputDoc] = []
    var agreeConsent = "N"
    var fileDelegate = DigiLockerFileModel()
    var isSelectDepartment = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblFileName.isHidden = true 
        self.fetchDocumentsList()
        self.tblList.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tblList.estimatedSectionHeaderHeight = 50
        
        self.tblList.sectionFooterHeight = UITableViewAutomaticDimension
        self.tblList.estimatedSectionFooterHeight = 130
       
        
        //self.tblList.layoutIfNeeded()
      

      //  self.tblList.reloadData()
       
        //self.tblList.tableFooterView = UIView(frame: CGRect.zero)
        self.setViewFont()
        self.showHideDownloadView()
        self.tblList.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: .leastNormalMagnitude))

    }
    @IBAction func didTapBackButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func setViewFont()   {
        self.lblTitle.font = AppFont.semiBoldFont(17)
        self.btnBack.titleLabel?.font = AppFont.regularFont(17)
        self.lblHeaderTitle.textColor = UIColor.black
        self.lblHeaderTitle.font = AppFont.regularFont(15)
        self.lblAgreeConsent.font = AppFont.lightFont(13.5)
        self.lblAgreeConsent.textColor = lightGrayColor
        self.btnGetDoc.titleLabel?.font = AppFont.mediumFont(18)
        self.layoutCornerRadius(view: self.btnGetDoc, radius: 5)
        self.lblTitle.text = "search_doc_digi".localized
        self.lblAgreeConsent.text = "i_providetxt_digi".localized
        self.lblHeaderTitle.text = "search_doctxt_digi".localized
        self.btnBack.setTitle("back".localized, for: .normal)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func didTapAgreeConsentBtnAction(_ sender: UIButton) {
        if agreeConsent == "N" {
            sender.isSelected = true
            agreeConsent = "Y"
        }else {
            sender.isSelected = false
            agreeConsent = "N"
        }
        
    }
    @IBAction func didTapPickerDone_CancelButtonAction(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        
    }
    @IBAction func didTapGetDocumentBtnAction(_ sender: UIButton) {
        if agreeConsent == "N" {
            self.showAlert(msg: "provide_consent".localized)
            return
        }
        let isNotValid = self.arrIssuerList.allSatisfy { (doc) -> Bool in
            if  doc.inputValue.count != 0 {
                return false
            }
            return true
        }
        
        if isNotValid  {
            self.showAlert(msg: "fill_all_fields".localized)
            return
        }
        
        self.getDocument()
        
    }
    
    @IBAction func didTapCancelDownloadBtnAction(_ sender: UIButton) {
        
        DigiSessionManager.instance.downloadTask.cancel()
         self.fileDelegate.requestData(dataDict: self.getFailureJSONFromLockerDocument(reason:"network failure"))
        
    }
    
}
// MARK:- Call API's
extension DownloadDLokcerDocVC {
    func fetchDocumentsList()
    {
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        // var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
        // accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
        //dictBody["ort"] = "rgtadhr"
        //dictBody["rc"] = "Y"
        // dictBody["mec"] = "Y"
        // &quot;orgid&quot;:&quot;002024&quot;
        dictBody["tkn"] = singleton.user_tkn
        dictBody["orgid"] = itemSelected.string(key: "orgid")
        dictBody["doctype"] = docSelected.string(key: "doctype")

        //&quot;doctype&quot;:&quot;DGCER&quot;
        //  dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        self.callApi(with: dictBody, url: UM_API_GET_SEARCH_PARAMETER_DOCUMENT)
    }
    func callApi(with dicBody:NSMutableDictionary, url:String) {
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        objRequest.hitAPIForDigiLockerAuthentication(withPost: true, isAccessTokenRequired: true, webServiceURL: url, withBody: dicBody, andTag: TAG_REQUEST_DIGILOCKER_GETTOKEN, completionHandler: {[weak self](_ response: Any, _ error: Error?, _ tag: REQUEST_TAG) -> Void in
            guard let er = error else {
                guard let responseJson = response as? [String:Any] else {
                    self?.hud.hide(animated: true)
                    return
                }
                self?.checkResponse(responseJson)
                return
            }
            self?.hud.hide(animated: true)
            self?.showErrorAlert(error: er)
        })
    }
    func checkResponse(_ response:[String:Any])  {
        self.hud.hide(animated: true)
        let dicFormat = response.formatDictionaryForNullValues(response)
        if self.checkSuccessResponse(dicResponse: dicFormat!) {
            self.processResponse(dicFormat!)
        }
    }
    func processResponse(_ dicJson:[String:Any])  {
        print("API Success")
        guard let pdJson = dicJson["pd"] as? [[String:String]] else {
            return
        }
//        guard let itemsJson = pdJson["documents"] as? [[String:String]] else {
//            return
//        }
        self.arrIssuerList = [TextInputDoc]()
        for json in pdJson {
            let doc = TextInputDoc(dicLocker: json)
            self.arrIssuerList.append(doc)
        }
        DispatchQueue.main.async {[weak self] in
            self?.tblList.reloadData()
        }
        
    }
    func getDocument()  {
        
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        // var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
        // accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
        //dictBody["ort"] = "rgtadhr"
        //dictBody["rc"] = "Y"
        // dictBody["mec"] = "Y"
        // &quot;orgid&quot;:&quot;002024&quot;
        dictBody["tkn"] = singleton.user_tkn
        dictBody["orgid"] = itemSelected.string(key: "orgid")
        dictBody["doctype"] = docSelected.string(key: "doctype")
        
        dictBody["consent"] = agreeConsent
        
        var paramJson = [String:String]()
        for doc in self.arrIssuerList {
            paramJson[doc.paramName] = doc.inputValue
        }
         dictBody["dyparam"] = paramJson
       
        print("Dictionary is \(dictBody)")
        let objRequest = UMAPIManager()
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        objRequest.hitAPIForDigiLockerAuthentication(withPost: true, isAccessTokenRequired: true, webServiceURL: UM_API_PULL_DIGILOCKER__DOCUMENT, withBody: dictBody, andTag: TAG_REQUEST_DIGILOCKER_GETTOKEN, completionHandler: {[weak self](_ response: Any, _ error: Error?, _ tag: REQUEST_TAG) -> Void in
            self?.hud.hide(animated: true)
             //self?.downloadFileForDepartment(uri: "in.gov.digilocker-OTHER-b6811d0aac80f058ae5830a8897d018f")
            guard let er = error else {
                guard let responseJson = response as? [String:Any] else {
                    self?.hud.hide(animated: true)
                    return
                }
                self?.processPullDocumentResponse(responseJson)
                return
            }
            
            self?.showErrorAlert(error: er)
        })
    }
    func processPullDocumentResponse(_ response:[String:Any])  {
        self.hud.hide(animated: true)
        let dicFormat = response.formatDictionaryForNullValues(response)
        if dicFormat!.keys.contains("uri") {
            if let uri = dicFormat?.string(key: "uri") {
                if self.isSelectDepartment {
                    self.downloadFileForDepartment(uri: uri)
                }
            }
        }
        else {
            for vc  in self.navigationController!.viewControllers {
                if vc.isKind(of: DigiLockerHomeVC.self) {
                    self.navigationController?.popToViewController(vc, animated: true)
                    break
                }
            }
        }
    }
    func downloadFileForDepartment(uri:String)  {
        var dicLocker = [String:String]()
        dicLocker["size"] = "\(59446)"
        dicLocker["type"] = DocType.file.rawValue
        dicLocker["uri"]  = uri
        dicLocker["mime"] = ""
        let doc = LockerDocument(dicLocker: dicLocker)
        let task = self.downloadDocumentFile(doc) ?? doc
        task.downloading = true
        //  self.arrDataSource[doc.index.row] = task
       // task.cell?.updateProgressOnMainQueue(0)
         DispatchQueue.main.async {[weak self] in
        
            self?.updateProgressOnMainQueue(0)
         }
//        if self.isSelectDepartment {
//            hud = MBProgressHUD.showAdded(to: view, animated: true)
//            hud.label.text = "loading".localized
//        }
        task.downTask?.completionHandler = { [weak self] in
            switch $0 {
            case .failure(let error):
                self?.fileDelegate.requestData(dataDict: self!.getFailureJSONFromLockerDocument(reason:"network failure"))
                print(error)
            case .success(let document):
                self?.downloadSuccess(document)
                break
                
            }
        }
        task.downTask?.progressHandler = { [weak self] in
            self?.updateProgress($0)
        }
    }
    func updateProgress(_ doc:LockerDocument)
    {
        self.updateProgress(doc.downTask!.progress)
        
    }
    
    func downloadSuccess(_ doc:LockerDocument)  {
        self.showHideDownloadView()
        if self.isSelectDepartment  {
            let json = self.getJsonFileFromLockerDocument(doc: doc)
            self.fileDelegate.requestData(dataDict: json)
            return
        }
    }
    func updateProgressOnMainQueue(_ progress:Float)  {
        if self.viewDownloadDoc.isHidden == true  {
            // DispatchQueue.main.async {[weak self] in
            self.showProgress()
            self.updateProgress(progress)
            // }
            return
        }else {
            // DispatchQueue.main.async {[weak self] in
            self.updateProgress(progress)
            //}
        }
    }
    func showProgress(){
        self.showHideDownloadView()
        self.view.bringSubview(toFront: self.viewDownloadDoc)
    }
    func showHideDownloadView()  {
         self.viewDownloadDoc.isHidden =  !self.viewDownloadDoc.isHidden
    }
    func updateProgress(_ progress:Float){
        if progress == 1.0 {
            return
        }
        let progressPercent = Int(progress*100)
        if  self.viewDownloadDoc != nil {
            self.progressDownload!.progress = Float(progress)
            self.lblProgress.text = "\(progressPercent)%"
        }
    }
}
extension DownloadDLokcerDocVC :UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
           return arrIssuerList.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTextFieldDocument", for: indexPath) as! CellTextFieldDocument
         let doc = arrIssuerList[indexPath.row]
          doc.indexPath = indexPath
          cell.txtInputUser.placeholder = doc.lblValue
          cell.txtInputUser.text = doc.inputValue
          cell.txtInputUser.delegate = self
         cell.txtInputUser.font = AppFont.lightFont(19)
        cell.txtInputUser.textColor = lightGrayColor
        cell.txtInputUser.indexPath = indexPath

        cell.lblPlaceholder.font = AppFont.lightFont(15)
        cell.lblPlaceholder.textColor = lightGrayColor
        cell.lblPlaceholder.text = doc.lblValue
        cell.lblPlaceholder.isHidden = true
        if doc.inputValue != "" {
            cell.lblPlaceholder.isHidden = false
        }
        cell.lblExample.textColor = lightGrayColor
        cell.lblExample.font = AppFont.lightFont(13)
        cell.lblExample.text = "Example:(\(doc.example))"
        cell.lblExample.lineBreakMode = .byWordWrapping
        cell.lblExample.numberOfLines = 0
          cell.txtInputUser.tag = indexPath.row
         if doc.type == .picker {
            cell.txtInputUser.inputView = self.viewPicker
         }
         arrIssuerList[indexPath.row] = doc
        return cell
    }
    
}
extension DownloadDLokcerDocVC :UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
                 return UITableViewAutomaticDimension

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
         return self.headerView
    }
    
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
//    {
//
//        return 0
//    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        
//        let cellFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 130))
//        cellFooterView.addSubview(footerView)
        
        
        return footerView
    }
    
    
}
extension DownloadDLokcerDocVC :UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let tag = textField.tag
        let doc = arrIssuerList[tag]
        if doc.type == .picker {
            IQKeyboardManager.shared().isEnableAutoToolbar = false
            self.arrPickerDataSource = doc.arrPickerValues
            self.pickerView.txtField = textField
            self.pickerView.reloadAllComponents()
        }
        if let umTxt =  textField as? UMTextField{
            umTxt.lineColor = UIColor.black
            if let cell = tblList.cellForRow(at:  umTxt.indexPath) as? CellTextFieldDocument {
                self.showWithAnimation(cell:cell)
            }
         }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let tag = textField.tag
        arrIssuerList[tag].inputValue = textField.text ?? ""
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        if let umTxt =  textField as? UMTextField{
            umTxt.lineColor = UIColor.lightGray
            if umTxt.text == nil || umTxt.text == "" {
                if let cell = tblList.cellForRow(at:  umTxt.indexPath) as? CellTextFieldDocument {
                    self.hideWithAnimation(cell: cell)
                }
            }
        }
    }
    func showWithAnimation(cell:CellTextFieldDocument)  {
       // weak var  wekCell = cell
        cell.lblPlaceholder.alpha = 0.2
        UIView.animateKeyframes(withDuration: 0.50, delay: 0.20, options: .overrideInheritedDuration, animations: {
            cell.lblPlaceholder.isHidden = false
            cell.lblPlaceholder.alpha = 0.2
        }) { (boo) in
            UIView.animate(withDuration: 0.20, animations: {
                cell.lblPlaceholder.alpha = 1
            })
        }
    }
    func hideWithAnimation(cell:CellTextFieldDocument)  {
        // weak var  wekCell = cell
        UIView.animateKeyframes(withDuration: 0.20, delay: 0.10, options: .overrideInheritedDuration, animations: {
            cell.lblPlaceholder.alpha = 0.5
        }) { (boo) in
            cell.lblPlaceholder.alpha = 0.0
            cell.lblPlaceholder.isHidden = true
        }
    }
}
extension DownloadDLokcerDocVC :UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPickerDataSource.count
    }
    
}
extension DownloadDLokcerDocVC :UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrPickerDataSource[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let value = arrPickerDataSource[row]
        if let txt = self.pickerView.txtField   {
            txt.text = value
        }
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }

    
}
