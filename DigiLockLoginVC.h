//
//  DigiLockLoginVC.h
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigiLockLoginVC : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *vw_webDigiLock;
}

@property (nonatomic,strong) NSString *tagComingFrom;
@property (nonatomic,strong) NSString *authURL;
@property (nonatomic)id fileDelegate;
@property (assign)BOOL isSelectDepartment;
@property(copy) void(^didDepartmentLogin)(void);

@end
