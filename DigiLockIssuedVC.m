//
//  DigiLockIssuedVC.m
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigiLockIssuedVC.h"
#import "MBProgressHUD.h"
#import "UMAPIManager.h"
#import "SharedManager.h"
#import "FAQWebVC.h"
#import "Base64.h"

#define kNoRecordImageHeight  85

@interface DigiLockIssuedVC ()
{
    MBProgressHUD *hud;
    SharedManager *singleton;
    
}

@end

@implementation DigiLockIssuedVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    singleton = [SharedManager sharedSingleton];
    [self.backButton setTitle:NSLocalizedString(@"back",nil) forState:UIControlStateNormal];
    
    self.titleLabel.text = NSLocalizedString(@"issued_documents", nil);
    
    self.issuedDocArray = [NSMutableArray new];
    [self hitAPIToGetIssuedDocuments];
    
    
    self.issuedDocTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.noRecordsView.hidden = YES;
    self.noRecordsLabel.text  = NSLocalizedString(@"no_result_found", nil);
    [self addNavigationView];
    //self.issuedDocTableView.hidden = NO;
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    _backButton.hidden = true;
    self.titleLabel.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backButtonAction:btnBack];
    };
    nvView.lblTitle.text = self.titleLabel.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect frame = self.issuedDocTableView.frame;
        frame.origin.y = kiPhoneXNaviHeight;
        frame.size.height = fDeviceHeight - kiPhoneXNaviHeight;
        self.issuedDocTableView.frame = frame;
        self.noRecordsView.frame = frame;
        [self.view layoutIfNeeded];
    }
    
    self.noFileImageView.frame = CGRectMake(self.noRecordsView.frame.size.width/2 - kNoRecordImageHeight/2, self.noRecordsView.frame.size.height/2 - (kNoRecordImageHeight+kNoRecordImageHeight/2), kNoRecordImageHeight, kNoRecordImageHeight);
    self.noRecordsLabel.frame = CGRectMake(0, self.noFileImageView.frame.origin.y + self.noFileImageView.frame.size.height+8, self.view.frame.size.width, 30);
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [self.backButton.titleLabel setFont:[AppFont regularFont:17.0]];
    self.titleLabel.font = [AppFont semiBoldFont:17.0];
    _noRecordsLabel.font = [AppFont regularFont:16.0];
    
    
}
- (IBAction)backButtonAction:(UIButton *)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"DigiLocker Issue" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)hitAPIToGetIssuedDocuments
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    
    
    NSString *accessTokenString = [[NSUserDefaults standardUserDefaults]valueForKey:@"AccessTokenDigi"];
    
    accessTokenString = accessTokenString.length == 0 ? @"" : accessTokenString;
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:@"Y" forKey:@"rc"];
    [dictBody setObject:@"Y" forKey:@"mec"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:accessTokenString forKey:@"utkn"];
    
    
    NSLog(@"Dictionary is %@",dictBody);
    
    
    [objRequest hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_GETISSUED_DOC withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_GETTOKEN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //NSString *rc=[response valueForKey:@"rc"];
            // NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                
                NSLog(@"API Success");
                
                
                self.issuedDocArray = [[response valueForKey:@"pd"] valueForKey:@"items"];
                
                if (self.issuedDocArray.count == 0)
                {
                    self.noRecordsView.hidden = NO;
                    [self.view bringSubviewToFront:self.noRecordsView];
                    //self.issuedDocTableView.hidden = YES;
                    
                }
                else
                {
                    self.noRecordsView.hidden = YES;
                    //self.issuedDocTableView.hidden = NO;
                }
                
                [self.issuedDocTableView reloadData];
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else
        {
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}

# pragma mark - UITableView Delegate and Datasource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.issuedDocArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IssuedDocCell *cell = (IssuedDocCell *)[tableView dequeueReusableCellWithIdentifier:@"IssuedDocDigiCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    cell.issuedDocNameLabel.text = [[self.issuedDocArray objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.issuedDocDateLabel.text = [NSString stringWithFormat:@"%@",[[self.issuedDocArray objectAtIndex:indexPath.row] valueForKey:@"date"]];
    
    
    if ([[[[[self.issuedDocArray objectAtIndex:indexPath.row] valueForKey:@"mime"] componentsSeparatedByString:@"/"] objectAtIndex:1] isEqualToString:@"jpeg"])
    {
        cell.issuedDocImageView.image = [UIImage imageNamed:@"adhr_icon_jpg.png"];
    }
    else if ([[[[[self.issuedDocArray objectAtIndex:indexPath.row] valueForKey:@"mime"] componentsSeparatedByString:@"/"] objectAtIndex:1] isEqualToString:@"png"])
    {
        cell.issuedDocImageView.image = [UIImage imageNamed:@"adhr_icon_png.png"];
    }
    else if ([[[[[self.issuedDocArray objectAtIndex:indexPath.row] valueForKey:@"mime"] componentsSeparatedByString:@"/"] objectAtIndex:1] isEqualToString:@"pdf"])
    {
        cell.issuedDocImageView.image = [UIImage imageNamed:@"adhr_icon_pdf.png"];
        
    }
    
    UIButton *customAccessoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    customAccessoryButton.frame = CGRectMake(0, 0, 24, 24);
    [customAccessoryButton setImage:[UIImage imageNamed:@"download_icon_digi"] forState:UIControlStateNormal];
    
    
    cell.accessoryView = customAccessoryButton;
    cell.issuedDocDateLabel.font = [AppFont regularFont:14.0];
    cell.issuedDocNameLabel.font = [AppFont regularFont:16.0];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *url = [NSString stringWithFormat:@"%@",[[self.issuedDocArray objectAtIndex:indexPath.row] valueForKey:@"uri"]];
    
    NSString *fileName = [NSString stringWithFormat:@"%@",[[self.issuedDocArray objectAtIndex:indexPath.row] valueForKey:@"name"]];
    
    
    [self hitAPIToDownloadDocumentWithURL:url withfileName:fileName];
    
}


#pragma mark - API To Download File

-(void)hitAPIToDownloadDocumentWithURL:(NSString *)url withfileName:(NSString*)fileName
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    
    NSString *accessTokenString = [[NSUserDefaults standardUserDefaults]valueForKey:@"AccessTokenDigi"];
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:accessTokenString forKey:@"utkn"];
    [dictBody setObject:url forKey:@"uri"];
    
    [dictBody setObject:[singleton appUniqueID] forKey:@"did"];
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else
    {
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    
    [dictBody setObject:selectedLanguage forKey:@"lang"];
    
    
    [dictBody setObject:@"app" forKey:@"mod"];
    
    NSLog(@"Dictionary is %@",dictBody);
    
    
    [objRequest hitAPIForDigiLockerDownloadFileWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_DOWNLOAD_DOC withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_DOWNLOADDOC completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         
         
         [hud hideAnimated:YES];
         // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
         
         
         if (error == nil)
         {
             
             
             NSData *pdfDocumentData=(NSData*)response;
             //
             NSString *mimeType = [self mimeTypeForData:pdfDocumentData]; // how would I implement getMimeType
             
             NSError *error;
             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
             NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
             NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/DigiFolder"];
             
             if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                 [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
             
             
             
             
             NSString *selectedFileName;
             
             if ([fileName containsString:@"jpeg"] || [fileName containsString:@"pdf"] || [fileName containsString:@"png"])
             {
                 selectedFileName = fileName;
             }
             else
             {
                 
                 selectedFileName = [NSString stringWithFormat:@"%@.%@",fileName,[[mimeType componentsSeparatedByString:@"/"] objectAtIndex:1]];
             }
             
             
             NSString *documentPath = [NSString stringWithFormat:@"%@/%@", dataPath, selectedFileName];
             
             //NSData *anImageData = UIImagePNGRepresentation(anImage);
             
             
             
             
             NSString *base64String = [pdfDocumentData base64Encoding]; //my base64Encoding function
             
             NSData *base64data = [[NSData alloc] initWithData:[NSData dataWithBase64EncodedString:base64String]];
             
             [self openPDFfile:base64data withfilename:fileName withType:mimeType];
             
             
             [base64data writeToFile:documentPath atomically:YES];
             
             
             NSLog(@"mimeType=%@",mimeType);
             //NSLog(@"pdfDocumentData=%@",pdfDocumentData);
             
             
         }
         else
         {
             
             
             UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                         {
                                             [self.navigationController popViewControllerAnimated:YES];
                                         }];
             
             
             [alert addAction:yesButton];
             
             [self presentViewController:alert animated:YES completion:nil];
             
             
         }
         
     }];
    
}


-(void)openPDFfile:(NSData*)pdfDocumentData withfilename:(NSString*)name withType:(NSString*)type

{
    [singleton traceEvents:@"Open FAQ" withAction:@"Clicked" withLabel:@"DigiLocker Issue" andValue:0];

    //NSLog(@"OpenWebViewFeedback");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.pdfData=pdfDocumentData;
    vc.titleOpen=@"PDF View";
    vc.urltoOpen=@"";
    vc.isfrom=@"WEBVIEWHANDLEPDF";
    vc.file_name=name;
    vc.file_type=type;
    vc.file_shareType=@"ShareYes";
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    // [self performSelector:@selector(openPDFfile:) withObject:pdfDocumentData afterDelay:1];
    
    
}

-(NSString *)mimeTypeForData:(NSData *)data
{
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
            break;
        case 0x89:
            return @"image/png";
            break;
        case 0x47:
            return @"image/gif";
            break;
        case 0x49:
        case 0x4D:
            return @"image/tiff";
            break;
        case 0x25:
            return @"application/pdf";
            break;
        case 0xD0:
            return @"application/vnd";
            break;
        case 0x46:
            return @"text/plain";
            break;
        default:
            return @"application/octet-stream";
    }
    return nil;
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
        
        NSLog(@"topController name = %@",topController);
        
    }
    
    return topController;
}

@end

#pragma mark - Issued Doc Cell Interface and Implementation

@interface IssuedDocCell()
@end

@implementation IssuedDocCell

@end

