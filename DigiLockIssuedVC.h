//
//  DigiLockIssuedVC.h
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigiLockIssuedVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UITableView *issuedDocTableView;

@property (weak, nonatomic) IBOutlet UIView *noRecordsView;
@property (strong, nonatomic) NSMutableArray *issuedDocArray;
@property (weak, nonatomic) IBOutlet UILabel *noRecordsLabel;
@property (strong, nonatomic) IBOutlet UIImageView *noFileImageView;




@end

#pragma mark - Issued Doc Cell Interface
@interface IssuedDocCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *issuedDocNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *issuedDocDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *issuedDocSizeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *issuedDocImageView;

@end
