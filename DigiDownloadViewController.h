//
//  DownloadedDocumentsViewController.h
//  Umang
//
//  Created by Lokesh Jain on 17/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ViewController.h"

@interface DigiDownloadViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *screenTitle;
@property (weak, nonatomic) IBOutlet UITableView *downloadDocTableView;
@property (weak, nonatomic) IBOutlet UIView *noRecordsView;
@property (weak, nonatomic) IBOutlet UILabel *noRecordsLabel;
@property (strong, nonatomic) IBOutlet UIImageView *noFileImageView;

@end

#pragma mark - Downloaded Doc Cell Interface
@interface DownloadDocCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *docNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *docDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *docSizeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *docImageView;

@end
