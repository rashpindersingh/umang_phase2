//
//  EmailSupportViewController.h
//  Umang
//
//  Created by Lokesh Jain on 12/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailSupportViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *supportLabel;
@property (weak, nonatomic) IBOutlet UITableView *supportTableView;

@property (weak, nonatomic) IBOutlet UIView *deptView;
@property (weak, nonatomic) IBOutlet UIButton *bottomButton;
@property (weak, nonatomic) IBOutlet UIButton *chooseDeptButton;
@property (weak, nonatomic) IBOutlet UIImageView *chooseDeptImageView;
@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *screentTitle;


@property (nonatomic,copy)   NSString *comingFrom;
@property (nonatomic,strong) NSDictionary *deptDict;
@end

#pragma mark - Email Cell Interface
@interface EmailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailLabel;
@property (weak, nonatomic) IBOutlet UIImageView *optionImageView;

@end
