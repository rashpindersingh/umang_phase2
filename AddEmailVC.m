//
//  AddEmailVC.m
//  Umang
//
//  Created by deepak singh rawat on 25/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "AddEmailVC.h"
#import "UpdMpinVC.h"
#define kOFFSET_FOR_KEYBOARD 80.0

@interface AddEmailVC ()<UITextFieldDelegate>
{
    SharedManager *singleton;
    __weak IBOutlet UILabel *lblVerification;
}
@property (weak, nonatomic) IBOutlet UIButton *btn_back;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subtitle;
@property (weak, nonatomic) IBOutlet UITextField *txt_email;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;

@end

@implementation AddEmailVC
@synthesize tagtopass,titletopass;

- (IBAction)btnbackAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    if ([tagtopass isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
    if ([tagtopass isEqualToString:@"ISFROMREGISTRATION"]) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        [self startNotifierIsFromNotify];
        
    }
    if ([tagtopass isEqualToString:@"UPDATEEMAILFROMSECURITY"]) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    if ([tagtopass isEqualToString:@"UPDATEFROMSECURITY_MPINBOX"]) {
         [self dismissViewControllerAnimated:NO completion:nil];
        
    }
}

- (void) startNotifierIsFromNotify
{
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STARTNOTIFIERREGIS" object:nil];
    
    
    
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
- (IBAction)btnSubmitAction:(id)sender {
    
    
    
    //jump to next step here
    if ([self validateEmailWithString:_txt_email.text]!=TRUE) {
        
        NSLog(@"Wrong Mobile");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_correct_email", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([tagtopass isEqualToString:@"UPDATEEMAILFROMSECURITY"])
    {
        [self openEnterMPIN:@"UPDATEEMAILFROMSECURITY"];
    }
    else if ([tagtopass isEqualToString:@"UPDATEFROMSECURITY_MPINBOX"])
    {
        [self openEnterMPIN:@"UPDATEFROMSECURITY_MPINBOX"];
    }
    else
    {
        
        
        singleton.profileEmailSelected= _txt_email.text;
        
        [self btnbackAction:self];
    }
    
    
}
-(void)openEnterMPIN:(NSString*)tag
{
    
    NSMutableDictionary *user_dic = [NSMutableDictionary new];
    
    
    [user_dic setValue:tag forKey:@"Tag"];
    [user_dic setValue:_txt_email.text forKey:@"email"];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UpdMpinVC *updMPinVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdMpinVC"];
    
    updMPinVC.dic_info=user_dic;//change it to URL on demand
    
    if ([tagtopass isEqualToString:@"UPDATEFROMSECURITY_MPINBOX"]) {
        __block typeof(self) weakSelf = self;
        updMPinVC.didShowHomeVC = ^{
            [weakSelf dismissViewControllerAnimated:false completion:nil];
        };
    }
    [self.navigationController pushViewController:updMPinVC animated:YES];
    
}
- (void)viewDidLoad
{
    
   [super viewDidLoad];
    
     self.btn_next.enabled=NO;
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    [self enableBtnNext:false];
    [_txt_email becomeFirstResponder];
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:ADD_EMAIL_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    [_btn_back setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_btn_back.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(_btn_back.frame.origin.x, _btn_back.frame.origin.y, _btn_back.frame.size.width, _btn_back.frame.size.height);
        
        [_btn_back setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _btn_back.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    singleton = [SharedManager sharedSingleton];
    
    lblVerification.text=NSLocalizedString(@"email_intro_sub_heading_hint", @"");

    _txt_email.delegate=self;
    
    _lbl_title.text = NSLocalizedString(@"add_email_address", @"");
    _lbl_subtitle.text  = NSLocalizedString(@"enter_new_email_address", @"");
    [self addNavigationView];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
   // if (textField == _txt_email)
   // {
     //   NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];

     //   return  [self validateEmailWithString:searchStr];
        if (textField.text.length >= 50)
        {
            return NO; // return NO to not change text
        }
    
    
        return YES;
        
    }
-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——

    
    
    
    
    
    if ([tagtopass isEqualToString:@"ISFROMPROFILEUPDATE"]) {

    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    //———— Add to handle network bar of offline——
        
    }
    
   
    
    self.lbl_title.text=titletopass;
    
   
    [_txt_email addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
    
    [self setViewFont];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    _btn_back.hidden = true;
    _lbl_title.hidden = true;
    //btnHelp.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton  = ^(id btnReset) {
        [weakSelf btnbackAction:btnReset];
    };
    nvView.lblTitle.text = _lbl_title.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX())
    {
//        CGRect tbFrame = .frame;
//        tbFrame.origin.y = kiPhoneXNaviHeight
//        tbFrame.size.height = fDeviceHeight - kiPhoneXNaviHeight;
//        tblProfileEdit.frame = tbFrame;
//        [self.view layoutIfNeeded];
    }
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [_btn_back.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _lbl_title.font = [AppFont semiBoldFont:21];
    _lbl_subtitle.font = [AppFont semiBoldFont:16];
    _txt_email.font = [AppFont regularFont:14.0];
    lblVerification.font = [AppFont regularFont:13.0];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


-(void)hideKeyboard
{
    [self.view endEditing:YES];
    
    // [self.txt_mobileNo resignFirstResponder];
}

/*
 - (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
*/
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.text.length > 0)
    {
        
        self.btn_next.enabled=YES;
        [self enableBtnNext:YES];
        
    }
    else
    {
        self.btn_next.enabled=NO;
        [self enableBtnNext:NO];
    }
}

-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];

    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.txt_email) {
        [theTextField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setFontforView:self.view andSubViews:YES];
}

/*
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (fDeviceHeight<=568) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
        
    }
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{    if (fDeviceHeight<=568) {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [self.txt_email resignFirstResponder];
            
        }
    }
}


*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
