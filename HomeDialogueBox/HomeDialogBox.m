//
//  HomeDialogBox.m
//  homedialogueBox
//
//  Created by admin on 11/08/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import "HomeDialogBox.h"
#import "UIImageView+WebCache.h"
#import "ShowUserProfileVC.h"


#import "SecuritySettingVC.h"

#import "HelpViewController.h"
#import "SettingsViewController.h"
#import "UserProfileVC.h"
#import "SocialMediaViewController.h"
#import "FeedbackVC.h"
#import "AadharCardViewCon.h"
#import "NotLinkedAadharVC.h"
#import "RateUsVCViewController.h"
#import "HomeDetailVC.h"

#import "FAQWebVC.h"

@interface HomeDialogBox ()
{
    __weak IBOutlet UIButton *btn_close;
    
    __weak IBOutlet UIButton *btn_viewservice;
    __weak IBOutlet UITextView *dialog_txtvw;
    __weak IBOutlet UIImageView *dialog_imgvw;
    __weak IBOutlet UIView *vw_dialog;
    IBOutlet NSLayoutConstraint *bannerHeightConstant;
    
    IBOutlet UILabel *dialogueTitle;
    IBOutlet NSLayoutConstraint *titleHeightConstriant;
    
    IBOutlet NSLayoutConstraint *deleteButtonHeight;
    IBOutlet NSLayoutConstraint *textViewHeightConstraint;
    
    
    IBOutlet NSLayoutConstraint *visitServiceHgtConstraint;
    
}
@end

@implementation HomeDialogBox


- (IBAction)actionVisitService:(id)sender
{
    [self DialogBoxAction:self.dialogueDict];
}
- (IBAction)actionCloseDialogbox:(id)sender
{
    [self closeBox];
}


-(void)closeBox
{
    [UIView animateWithDuration:0.4 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

- (CGFloat)getTextViewHeight:(UITextView*)textView
{
    CGSize constraint = CGSizeMake(textView.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSDictionary *attrDict = @{NSFontAttributeName : [UIFont systemFontOfSize:15.0] };
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [textView.text boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:attrDict
                                                     context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

-(void)loadContentDialogBox
{
    
    NSString *descString = [self.dialogueDict valueForKey:@"des"] == nil || [[self.dialogueDict valueForKey:@"des"] isEqualToString:@""] ? @"" : [self.dialogueDict valueForKey:@"des"];
    
    NSString *htmlString = [NSString stringWithFormat:@"%@",descString];
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                               NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attributedString length])];
    
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, [attributedString length])];
    
    dialog_txtvw.attributedText = attributedString;
    
    dialog_txtvw.font = [UIFont systemFontOfSize:14.0];
    
    
    NSString *btnTitle = [self.dialogueDict valueForKey:@"bTxt"] == nil || [[self.dialogueDict valueForKey:@"bTxt"] isEqualToString:@""] ? @"" : [self.dialogueDict valueForKey:@"bTxt"];
    
    
    NSString *bannerTitle = [self.dialogueDict valueForKey:@"title"] == nil || [[self.dialogueDict valueForKey:@"title"] isEqualToString:@""] ? @"" : [self.dialogueDict valueForKey:@"title"];
    titleHeightConstriant.constant = bannerTitle.length > 0 ? 40 : 0;
    
    
    [btn_close setTitle:@"CLOSE" forState:UIControlStateNormal];
    [btn_viewservice setTitle:btnTitle forState:UIControlStateNormal];
    
    dialogueTitle.text = bannerTitle;
    
    
    
    if ([self.dialogueDict valueForKey:@"bimg"] == nil || [[self.dialogueDict valueForKey:@"bimg"] isEqualToString:@""])
    {
        bannerHeightConstant.constant = 0;
    }
    else
    {
        NSString *imagePath = [self.dialogueDict valueForKey:@"bimg"];
        
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
            NSURL *url=[NSURL URLWithString:imagePath];
            
            
            dispatch_async(dispatch_get_main_queue(),^{
                [dialog_imgvw sd_setImageWithURL:url
                       placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            });
        });
    }
    
    //dialog_imgvw.image=[UIImage imageNamed:@"bannerImage"];
    
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];

    
    vw_dialog.backgroundColor=[UIColor whiteColor];
    
    vw_dialog.layer.cornerRadius = 4.0;

    dialog_imgvw.layer.borderColor = [UIColor whiteColor].CGColor;
    dialog_imgvw.layer.borderWidth = 2.0;

    
    [self.view bringSubviewToFront:vw_dialog];
    
    
    CGFloat height = [self getTextViewHeight:dialog_txtvw];
    
    if (height < 130)
    {
        textViewHeightConstraint.constant = height+10;
    }
    else
    {
        textViewHeightConstraint.constant = 130;
    }
    
    
    
    if ([[self.dialogueDict valueForKey:@"actionType"] isEqualToString:@""])
    {
        deleteButtonHeight.constant = 0;
        visitServiceHgtConstraint.constant = 0;
        [btn_viewservice setTitle:@"" forState:UIControlStateNormal];
        [btn_close setTitle:@"" forState:UIControlStateNormal];
    }
    else
    {
        deleteButtonHeight.constant = 0;
        visitServiceHgtConstraint.constant = 40.0;
        [btn_close setTitle:@"" forState:UIControlStateNormal];
    }
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadContentDialogBox];

}

#pragma mark - Button Action Methods

-(void)DialogBoxAction:(NSDictionary*)dialogBoxData
{
    
    SharedManager *singleton=[SharedManager sharedSingleton];
    
    //----------Handle case for subType---------------
    
    NSString *subType =[dialogBoxData valueForKey:@"actionType"];
    
    if([subType isEqualToString:@"openApp"])
    {
        //handle case in delegate for it do nothing
    }
    else if([subType isEqualToString:@"playstore"])
    {
        
        NSString *url=[NSString stringWithFormat:@"%@",[dialogBoxData valueForKey:@"actionUrl"]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
    }
    
    // Case to open app  inside webview with custom webview title
    else if([subType isEqualToString:@"webview"])
    {
        
        NSString *title =[dialogBoxData valueForKey:@"title"];
        
        NSString *url=[NSString stringWithFormat:@"%@",[dialogBoxData valueForKey:@"actionUrl"]];
        
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        [self openFAQWebVC:url withTitle:title];
        
    }
    
    // Case to open app  in mobile browser
    
    // Case to open  playstore url in external
    else if([subType isEqualToString:@"browser"]||[subType isEqualToString:@"youtube"]||[subType isEqualToString:@"url"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[dialogBoxData valueForKey:@"actionUrl"]];
        
        NSString* webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL* urltoOpen = [NSURL URLWithString:webStringURL];
        
        [[UIApplication sharedApplication] openURL:urltoOpen];
        
    }
    
    // Case to open app  with Screen Name like profile/settings etc
    else if([subType isEqualToString:@"openAppWithScreen"])
    {
        
        NSString *screenName=[NSString stringWithFormat:@"%@",[dialogBoxData valueForKey:@"actionUrl"]];
        
        screenName=[screenName lowercaseString];
        
        if ([screenName isEqualToString:@"settings"])
        {
            [self mySetting_Action];
            
        }
        
        if ([screenName isEqualToString:@"help"])
        {
            
            [self myHelp_Action];
            
        }
        
        if ([screenName isEqualToString:@"social"])
        {
            
            [self socialMediaAccount];
            
        }
        
        if ([screenName isEqualToString:@"aadhaar"])
        {
            
            if (singleton.objUserProfile.objAadhar.aadhar_number.length)
            {
            
                [self AadharCardViewCon];
                
            }
            else
            {
                [self NotLinkedAadharVC];
            }
            
        }
        
        if ([screenName isEqualToString:@"feedback"])
        {
            [self FeedbackVC];
        }
        
        if ([screenName isEqualToString:@"accountsettings"])
        {
            [self accountSettingAction];
        }
        
        if ([screenName isEqualToString:@"myprofile"])
        {
            [self myProfile_Action];
        }
        else
        {
            //main
        }
        
    }
    
                                                               // Case to open app  with tab name

    else if([subType isEqualToString:@"openAppWithTab"])
    {
        
        NSString *screenName=[NSString stringWithFormat:@"%@",[dialogBoxData valueForKey:@"actionUrl"]]; //need to confirm KEY  for it
        
        screenName=[screenName lowercaseString];
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        
        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        if ([screenName isEqualToString:NSLocalizedString(@"home_small", nil)])
        {
            tbc.selectedIndex=0;
        }
        
        if ([screenName isEqualToString:NSLocalizedString(@"favourites_small", nil)])
        {
            tbc.selectedIndex=1;
        }
        
        if ([screenName isEqualToString:NSLocalizedString(@"states", nil)])
        {
            
            //tbc.selectedIndex=0;ignore case
            
        }
        
        if ([screenName isEqualToString:  NSLocalizedString(@"all_services_small", nil)])
        {
            tbc.selectedIndex=2;
        }
        
        UIViewController *topvc=[self topMostController];
        
        [topvc presentViewController:tbc animated:NO completion:nil];
        
    }
    
    // Case to open app with service
    else  if([subType isEqualToString:NSLocalizedString(@"services", nil)]||[subType isEqualToString:@"service"])
    {
        
        if (dialogBoxData)
        {
            
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            // BANNER_ACTION_URL
            
            
            
            // SERVICE_ID_to_Pass=[dic_serviceInfo valueForKey:@"SERVICE_ID"];
            
            //titleStr=[dic_serviceInfo valueForKey:@"SERVICE_NAME"];
            
            // _lbltitle.text = titleStr;
            
            //urlString=[dic_serviceInfo valueForKey:@"SERVICE_URL"];
            
            
            NSString *stringdata=[dialogBoxData valueForKey:@"actionUrl"];
            NSArray *stringArray = [stringdata componentsSeparatedByString: @"|"];
    
            NSMutableDictionary *dialogActionData=[NSMutableDictionary new];
            
            
            @try
            {
                
                NSString *service_url=[stringArray objectAtIndex:0];
                
                NSString *service_title=[stringArray objectAtIndex:1];
                
                NSString *service_id=[stringArray objectAtIndex:2];
                
                
                [dialogActionData setObject:service_id forKey:@"SERVICE_ID"];
                
                [dialogActionData setObject:service_title forKey:@"SERVICE_NAME"];
                
                [dialogActionData setObject:service_url forKey:@"SERVICE_URL"];
                
            } @catch (NSException *exception)
            {
                
                
                
            } @finally
            {
                
                
                
            }
            
            HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
            
            [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            
            vc.dic_serviceInfo= dialogActionData;
            
            vc.tagComeFrom=@"OTHERS";
            
            vc.sourceTab     = @"attention_screen";
            vc.sourceState   = @"";
            vc.sourceSection = @"";
            vc.sourceBanner  = @"";
            
            UIViewController *topvc=[self topMostController];
            
            [topvc presentViewController:vc animated:NO completion:nil];
            
        }
        
    }
    
    // Case to open app  for rating view
    else  if([subType isEqualToString:NSLocalizedString(@"Rating", nil)] || [subType isEqualToString:@"rating"])
    {
        
        [self rateUsClicked];
    }
    
    // Case to open app  for share [sharing message will recieve inside api)
    
    else  if([subType isEqualToString:@"share"] || [subType isEqualToString:NSLocalizedString(@"share", nil)])
        
    {
        
        [self shareContent];
        
    }
    
    //Default case
    
    else if ([subType isEqualToString:@""])
    {
        [self closeBox];
    }
    else
    {
        [self closeBox];
    }
    
    [self closeBox];
    
}



#pragma mark - Dialogue Actions

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)shareContent
{
    SharedManager *singleton=[SharedManager sharedSingleton];
    NSString *textToShare =singleton.shareText;
    
    
    NSArray *objectsToShare = @[textToShare];
    
    [[UIPasteboard generalPasteboard] setString:textToShare];
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    
    UIViewController *topvc=[self topMostController];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [topvc presentViewController:controller animated:YES completion:nil];
        
    }
    
    //if iPad
    
    else {
        
        // Change Rect to position Popover
        
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        [popup presentPopoverFromRect:CGRectMake(topvc.view.frame.size.width/2, topvc.view.frame.size.height/4, 0, 0)inView:topvc.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}

-(void)rateUsClicked
{
    NSLog(@"My Help Action");
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    RateUsVCViewController  *vc = [storyboard instantiateViewControllerWithIdentifier:@"RateUsVCViewController"];
    //  vc.hidesBottomBarWhenPushed = YES;
    UIViewController *topvc=[self topMostController];
    
    //[vc bringSubviewToFront:topvc.view];
    
    [topvc presentViewController:vc animated:NO completion:nil ];
    // [topvc.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)NotLinkedAadharVC
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    // SettingsViewController
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotLinkedAadharVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NotLinkedAadharVC"];
    vc.hidesBottomBarWhenPushed = YES;
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
}

-(void)accountSettingAction
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSLog(@"My account setting Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SecuritySettingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecuritySettingVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)openFAQWebVC:(NSString *)url withTitle:(NSString*)title
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)myProfile_Action
{
    NSLog(@"My Profile Action");
    
    
    
    
    // SettingsViewController
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
    vc.hidesBottomBarWhenPushed = YES;
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
}

-(void)mySetting_Action
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSLog(@"My Setting Action");
    
    // SettingsViewController
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SettingsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    
    
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
}
-(void)myHelp_Action
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSLog(@"My Help Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
}
-(void)AadharCardViewCon
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    // SettingsViewController
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AadharCardViewCon *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharCardViewCon"];
    vc.hidesBottomBarWhenPushed = YES;
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
}
-(void)FeedbackVC
{
    
    // SettingsViewController
     UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FeedbackVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)socialMediaAccount
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    // SettingsViewController
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SocialMediaViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SocialMediaViewController"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    UIViewController *topvc=[self topMostController];
    [topvc.navigationController pushViewController:vc animated:YES];
    
    
    
}

#pragma mark -
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
