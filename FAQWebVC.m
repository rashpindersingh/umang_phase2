//
//  FAQWebVC.m
//  Umang
//
//  Created by deepak singh rawat on 15/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "FAQWebVC.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"

@interface FAQWebVC ()<UIGestureRecognizerDelegate>
{
    SharedManager *singleton;
    NSString *urlString;
    MBProgressHUD *hud;
    UIImage *downloadImage;
    UIActivityIndicatorView *activityView;
}
@end

@implementation FAQWebVC
@synthesize urltoOpen;
@synthesize titleOpen;
@synthesize file_shareType;

- (void)viewDidUnload
{
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: FAQ_WEB_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    webview = nil;
    /* self.toolbar = nil;
     self.back = nil;
     self.forward = nil;
     self.refresh = nil;
     self.stop = nil;*/
    [super viewDidUnload];
}

- (void)updateButtons
{
    /*self.forward.enabled = webview.canGoForward;
     self.back.enabled = webview.canGoBack;
     self.stop.enabled = webview.loading;*/
    
    if ([webview canGoBack])
    {
        
        [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
        
    }
    else
    {
        [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
        
    }
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
    }
}

-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}


-(IBAction)saveAction:(id)sender
{
    
    
    
    if ([[btn_save titleForState:UIControlStateNormal] isEqualToString:NSLocalizedString(@"save", nil)])
    {
        [singleton traceEvents:@"Save Button" withAction:@"Clicked" withLabel:@"FAQWeb" andValue:0];
        
        dispatch_queue_t queue = dispatch_queue_create("com.spice.umang", NULL);
        dispatch_async(queue, ^{
            // UIImage *downloadImage=[self imageFromWebView:webview];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //code to be executed on the main thread when background task is finished
                //update UI with new database inserted means reload uicollectionview
                if (downloadImage==nil) {
                    
                }
                else
                {
                    UIImageWriteToSavedPhotosAlbum(downloadImage,nil,nil,nil);
                    
                    NSString *msg=@"Save in photo library";
                    [self showToast:msg];
                }
            });
            
        });
    }
    
    else
    {
        if ([file_shareType isEqualToString:@"ShareYes"])
        {
            [singleton traceEvents:@"Share Button" withAction:@"Clicked" withLabel:@"FAQWeb" andValue:0];
            
            @try {
                NSData * data = self.pdfData;
                
                NSError *error;
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/EPFO"];
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])//Check
                    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Will Create folder
                
                NSArray* foo = [self.file_type componentsSeparatedByString:@"/"];
                if ([foo count]>0)
                {
                    
                    NSString* mimetype = [foo objectAtIndex: 1];
                    
                    //NSString *localImgpath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",@"Passbook",mimetype]];
                    
                    if([titleOpen length]==0)
                    {
                        titleOpen=@"Passbook";
                    }
                    NSString *localImgpath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",titleOpen,mimetype]];
                    
                    [data writeToFile:localImgpath atomically:YES];
                    
                    NSString *path = localImgpath;
                    NSURL *targetURL = [NSURL fileURLWithPath:path];
                    
                    NSArray *activityItems = [NSArray arrayWithObjects:targetURL, nil];
                    
                    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
                    
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                        
                    {
                        
                        [self presentViewController:activityViewController animated:YES completion:^{
                            // executes after the user selects something
                        }];
                        
                    }
                    
                    else
                    {
                        // Change Rect to position Popover
                        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
                        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                        
                    }
                    
                }
                
                
                
                
                
                
                
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            
            //        // grab an item we want to share
            //        UIImage *image = downloadImage;
            //        NSArray *items = @[image];
            //
            //        // build an activity view controller
            //        UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
            //
            //        // and present it
            //
            //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            //        {
            //            [self presentViewController:controller animated:YES completion:^{
            //                // executes after the user selects something
            //            }];
            //        }
            //        else {
            //            // Change Rect to position Popover
            //            UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
            //            [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            //        }
            
            
            
        }
        else if ([file_shareType isEqualToString:@"ShareYesPDF"])
        {
            [singleton traceEvents:@"Share Button" withAction:@"Clicked" withLabel:@"FAQWeb" andValue:0];
            
            NSData *data = self.pdfData;
            
            NSURL *targetURL = [NSURL fileURLWithPath:self.localPathString];
            
            NSArray *activityItems = [NSArray arrayWithObjects:targetURL, nil];
            
            UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                
            {
                
                [self presentViewController:activityViewController animated:YES completion:^{
                    // executes after the user selects something
                }];
                
            }
            
            else
            {
                // Change Rect to position Popover
                UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
                [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                
            }
            
        }
        
        else
        {
            
            @try {
                NSData * data = self.pdfData;
                
                NSError *error;
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Digilocker"];
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])//Check
                    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Will Create folder
                
                NSArray* foo = [self.file_type componentsSeparatedByString:@"/"];
                if ([foo count]>0) {
                    NSString* mimetype = [foo objectAtIndex: 1];
                    
                    NSString *localImgpath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",self.file_name,mimetype]];
                    
                    [data writeToFile:localImgpath atomically:YES];
                }
                
                
                
                
                
                NSString *msg=NSLocalizedString(@"download_success", nil);
                [self showToast:msg];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
-(UIImage *) imageFromWebView:(UIWebView *)view
{
    
    
    @try {
        CGRect tmpFrame         = view.frame;
        
        // set new Frame
        CGRect aFrame               = view.frame;
        aFrame.size.height  = [view sizeThatFits:[[UIScreen mainScreen] bounds].size].height;
        view.frame              = aFrame;
        
        // do image magic
        UIGraphicsBeginImageContext([view sizeThatFits:[[UIScreen mainScreen] bounds].size]);
        
        CGContextRef resizedContext = UIGraphicsGetCurrentContext();
        [view.layer renderInContext:resizedContext];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        // reset Frame of view to origin
        view.frame = tmpFrame;
        return image;
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    // tempframe to reset view size after image was created
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btnBack.hidden = true;
    lbltitle.hidden = true;
    btn_save.hidden = YES;
    
    NavigationView *nvView = [[NavigationView alloc] initRightBarButton];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backbtnAction:btnBack];
    };
    
    nvView.didTapRightBarButton = ^(id btnReset) {
        
        [weakSelf saveAction:btnReset];
    };
    
    [nvView.rightBarButton setTitle:[btn_save titleForState:UIControlStateNormal] forState:UIControlStateNormal];
    [nvView.rightBarButton setImage:[btn_save imageForState:UIControlStateNormal] forState:UIControlStateNormal];
    
    nvView.lblTitle.text = titleOpen;
    
    [self.view addSubview:nvView];
    webview.frame = CGRectMake(0, CGRectGetMaxY(nvView.frame), fDeviceWidth, fDeviceHeight - CGRectGetMaxY(nvView.frame));
    [self.view layoutIfNeeded];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    btn_save.hidden=TRUE;
    webview.delegate=self;
    if ([self.isfrom isEqualToString:@"WEBVIEWHANDLEPDF"]||[self.isfrom isEqualToString:@"WEBVIEWHANDLEPDFOPENLOCALPATH"])
    {
        NSString *savetilte;
        
        if ([file_shareType isEqualToString:@"ShareYes"] || [file_shareType isEqualToString:@"ShareYesPDF"])
        {
            //savetilte=NSLocalizedString(@"share", nil);
            
            savetilte = @"";
            [btn_save setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
        }
        else
        {
            savetilte=NSLocalizedString(@"save", nil);
            [btn_save setTitle:savetilte forState:UIControlStateNormal];
            
        }
        btn_save.hidden=FALSE;
        
        
    }
    else
    {
        btn_save.hidden=TRUE;
        
    }
    webview.delegate = self;
    webview.scalesPageToFit = YES;
    
    
    lbltitle.text=titleOpen;
    singleton = [SharedManager sharedSingleton];
    
    // urlString=[singleton.arr_initResponse valueForKey:@"faq"];
    
    urlString=urltoOpen;
    lbltitle.text=titleOpen;
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    
    [self addNavigationView];
    
    // Do any additional setup after loading the view.
}

-(void)showIndicator:(BOOL)show {
    if (show) {
        activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.view.center.x-10, self.view.center.y-10, 20, 20)];
        activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        activityView.color = [UIColor grayColor];
        activityView.hidesWhenStopped = true;
        [self.view addSubview:activityView];
        [self.view bringSubviewToFront:activityView];
        [activityView startAnimating];
    }else {
        if((activityView != nil) || (activityView.isAnimating)){
            [activityView  stopAnimating];
            [activityView removeFromSuperview];
            activityView = nil;
        }
    }
}
-(void)loadHtml
{
    
    if (self.isfrom == nil && [self.isfrom length] == 0) {
        self.isfrom = @"";
    }
    if ([self.isfrom isEqualToString:@"WEBVIEWHANDLEPDF"])
    {
        //@property(nonatomic,retain)NSString *file_name;
        // @property(nonatomic,retain)NSString *file_type;
        
        if ([self.file_type isEqualToString:@"application/pdf"])
        {
            [webview loadData:self.pdfData MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
        }
        else
        {
            [webview loadData:self.pdfData MIMEType:self.file_type textEncodingName:nil baseURL:nil];
        }
        
    }
    else  if ([self.isfrom isEqualToString:@"WEBVIEWHANDLEPDFOPEN"])
    {
        //@property(nonatomic,retain)NSString *file_name;
        // @property(nonatomic,retain)NSString *file_type;
        
        if ([self.file_type isEqualToString:@"application/pdf"]) {
            //[webview loadData:self.pdfData MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
            [webview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:self.file_name]]];
            
        }
        else
        {
            NSString * html = [[NSString alloc] initWithFormat:@"<img src=\"file://%@\"/>", self.file_name];
            
            [webview loadHTMLString:html  baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]]];
        }
        
    }
    else  if ([self.isfrom isEqualToString:@"WEBVIEWHANDLEPDFOPENLOCALPATH"])
    {
        //@property(nonatomic,retain)NSString *file_name;
        // @property(nonatomic,retain)NSString *file_type;
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.file_name]];
        [webview loadRequest:request];
        
        
    }
    else
    {
        NSString *urlAddress =urlString;//<null>
        [self showIndicator:true];

        if ([urlAddress isEqualToString:@"<null>"]) {
            urlAddress=@"";
        }
        
        NSURL *postURI=[NSURL URLWithString:urlAddress];
        //  NSString *body = [NSString stringWithFormat: @"arg1=%@&arg2=%@", @"val1",@"val2"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:postURI];
        
        [webview loadRequest:request];
        
    }
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    [self loadHtml];
    
    // [self setNeedsStatusBarAppearanceUpdate];
    //  [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:.1];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gobackWebview)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    // [self.navigationController.view addGestureRecognizer:gestureRecognizer];
    [webview addGestureRecognizer:gestureRecognizer];
    
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


-(void)gobackWebview
{
    
    if ([self.isfrom isEqualToString:@"WEBVIEWHANDLEFEEDBACK"])
    {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        
    }
    
    if ([self.isfrom isEqualToString:@"WEBVIEWHANDLEPDFOPENLOCALPATH"])
    {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        
    }
    
    
    else if ([webview canGoBack])
    {
        
        // [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
        [webview goBack];
        
    }
    else
    {
        [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
        
        NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
        if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
        {
            
            CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                           @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
            
            CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
            
            [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
            btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
            
        }
        //dismiss
        
    }
    
    
}

-(IBAction)backbtnAction:(id)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"FAQWeb" andValue:0];
    
    if ([self.isfrom isEqualToString:@"WEBVIEWHANDLEFEEDBACK"])
    {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        
    }
    
    if ([self.isfrom isEqualToString:@"WEBVIEWHANDLEPDFOPEN"]||[self.isfrom isEqualToString:@"WEBVIEWHANDLEPDFOPENLOCALPATH"])
    {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        
    }
    else if ([webview canGoBack])
    {
        
        if ([self.isfrom isEqualToString:@"Settings"])
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [webview goBack];
        }
        // [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
        
    }
    
    else
    {
        if ([self.isfrom isEqualToString:@"WEBVIEWHANDLE"]||[self.isfrom isEqualToString:@"WEBVIEWHANDLEPDF"])
        {
            [self dismissViewControllerAnimated:NO completion:nil];
            
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

//-------- WEB CACHE CODE START------------
- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self updateButtons];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self updateButtons];
    
    [self showIndicator:false];

}

-(BOOL)webView:(UIWebView *)mainWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    // hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSLog(@"url -->%@",[[request URL] absoluteString]);
    
    NSString *requestString = [[request URL] absoluteString];
    NSLog(@"requestString -->%@",requestString);
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::closeChatWindow::Yes"])
    {
        /*NSArray *components = [requestString componentsSeparatedByString:@"::"];
         NSString *functionName = (NSString*)[components objectAtIndex:1];
         //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
         NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
         NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
         
         NSData *data = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
         
         NSArray *arr_JsonArray = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:data options:0 error:nil],nil];*/
        
        if ([self.isfrom isEqualToString:@"WEBVIEWHANDLE"])
        {
            [self dismissViewControllerAnimated:NO completion:nil];
            
        }
    }
    
    //[webview loadRequest:request];
    
    return YES;
    
}

//WEBVIEWHANDLE

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    @try {
        // downloadImage=[self imageFromWebView:webview];
        [self performSelector:@selector(downloadImageGet) withObject:nil afterDelay:2];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    
    [self updateButtons];
    [self showIndicator:false];

    
}

-(void)downloadImageGet
{
    downloadImage=[self imageFromWebView:webview];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
