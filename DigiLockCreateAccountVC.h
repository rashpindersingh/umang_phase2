//
//  DigiLockCreateAccountVC.h
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTextField.h"
@interface DigiLockCreateAccountVC : UIViewController
- (IBAction)btnBackTapped:(id)sender;



@property (weak, nonatomic) IBOutlet UIButton *btnGoNext;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckboxConsent;

@property (weak, nonatomic) IBOutlet MyTextField *txtFldOne;
@property (weak, nonatomic) IBOutlet MyTextField *txtFldTwo;
@property (weak, nonatomic) IBOutlet MyTextField *txtFldThree;

@property (weak, nonatomic) IBOutlet UILabel *createAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *enterAdhaarLabel;
@property (weak, nonatomic) IBOutlet UILabel *sendOTPLabel;
@property (weak, nonatomic) IBOutlet UILabel *consentLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end
