//
//  ProfilePickerVC.h
//  Umang
//
//  Created by admin on 18/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilePickerVC : UIViewController
{
    IBOutlet UITableView *table_State;
    IBOutlet UIButton *btn_back;
    
    IBOutlet UILabel *lbl_header;
    SharedManager *singleton;
    IBOutlet UIView *vw_line;
    
}
@property(weak,nonatomic)NSString *get_TAG;
@property(weak,nonatomic)NSString *get_title_pass;
@property(weak,nonatomic)NSMutableArray *get_arr_element;
@property(weak,nonatomic)NSString *TAG_FROM;


- (IBAction)btn_backClicked:(id)sender;
@end

