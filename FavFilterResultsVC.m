//
//  FavFilterResultsVC.m
//  Umang
//
//  Created by admin on 21/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "FavFilterResultsVC.h"
#import "SearchCell.h"
#import "CZPickerView.h"
#import "SDCapsuleButton.h"
#import "FilterServicesBO.h"
#import "HomeCardBO.h"
#import "NotificationItemBO.h"
#import "UIImageView+WebCache.h"
#import "HomeDetailVC.h"
#import "FavouriteTabVC.h"
#import "FavouriteCell.h"
#import "DetailServiceNewVC.h"
#import "AllServicesTabVC.h"
#import "NearMeTabVC.h"
#import "StateList.h"
#import "ShowMoreServiceVC.h"
#import "HomeTabVC.h"
#import "FlagHomeTabVC.h"
#import "HomeWithFav_TabVC.h"

@interface FavFilterResultsVC () <CZPickerViewDelegate,CZPickerViewDataSource,UIActionSheetDelegate>
{
    NSMutableArray *arrMainData;
    NSMutableArray *arrTableData;
    
    NSArray *arrSortOptions;
    NSArray *arrFilterOptions;
    StateList *obj;
    
    SharedManager *singleton ;
    
    NavigationFilterScollResultView *navFilterScrollView;
    
}
@property(nonatomic,retain)NSDictionary *cellDataOfmore;

@end

@implementation FavFilterResultsVC
@synthesize btnSort,btnFilter;

- (void)viewDidLoad
{
    
    if (iPhoneX())
    {
        
        navFilterScrollView = [[NavigationFilterScollResultView alloc] init];
        NSLog(@"An iPhone X Load UI");
    }
    
    
    [super viewDidLoad];
    
    vw_noresults.hidden=TRUE;
    
    noResultsVW = [[FilterNoResultView alloc] initWithFrame:CGRectMake(0, 65, fDeviceWidth, fDeviceHeight - 65)];
    [noResultsVW setFilterBtnTitle];
    [self.view addSubview:noResultsVW];
    [noResultsVW.btnEditFilter addTarget:self action:@selector(btnSettingAgainClicked:) forControlEvents:UIControlEventTouchUpInside];
    noResultsVW.hidden = true;
    [vw_noresults removeFromSuperview];
    
    //lb_noresults.text = NSLocalizedString(@"no_result_found", nil);
    
    singleton = [SharedManager sharedSingleton];
    obj = [[StateList alloc] init];
    self.automaticallyAdjustsScrollViewInsets = NO;
    //    [self prepareTempData];
    
    self.tblSearchFilter.layoutMargins = UIEdgeInsetsZero;
    UIView *vwLineHide = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.5)];
    [self.tblSearchFilter addSubview:vwLineHide];
    vwLineHide.backgroundColor = [UIColor whiteColor];
    
    
    // Add Filter Capsules Button
    [self addFilterOptionsAtNavigationBar];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    // Do any additional setup after loading the view.
    
    [self addNavigationFilterResultScrollView];
    
}


#pragma mark- add Navigation View to View

-(void)addNavigationFilterResultScrollView
{
    
    if (iPhoneX())
    {
        
        // nvSearchView = [[NavigationSearchView alloc] init];
        // NavigationSearchView *nvSearchView = [[NavigationSearchView alloc] init];
        
        __weak typeof(self) weakSelf = self;
        
        navFilterScrollView.didTapRightFilterBarButton = ^(id btnfilter)
        {
            
            [weakSelf btnSettingAgainClicked:btnfilter];
            
        };
        
        
        navFilterScrollView.didTapBackButton = ^(id btnBack)
        {
            
            [weakSelf btnBackClicked:btnBack];
            
        };
        navFilterScrollView.scroll.delegate=self;
        
        
        [self.view addSubview:navFilterScrollView];
        
        CGRect table = _tblSearchFilter.frame;
        table.origin.y = kiPhoneXNaviHeight;
        table.size.height = fDeviceHeight - kiPhoneXNaviHeight;
        _tblSearchFilter.frame = table;
        [self.view layoutIfNeeded];
        
        
    }
    else
    {
        NSLog(@"Not an iPhone X use default UI");
    }
    
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

-(void)addFilterOptionsAtNavigationBar
{
    
    //Remove existing button
    
    [([UIScreen mainScreen].bounds.size.height == 812.0 ?[navFilterScrollView.scroll subviews] : [self.scrollView subviews]) makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    // [[self.scrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // First fetch State Name
    
    
    NSMutableArray *arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    
    CGFloat xCord = 10;
    CGFloat yCord = 0;
    CGFloat padding = 5;
    
    CGFloat itemHeight = 30.0;
    
    NSString *_selectedNotificationType = [self.dictFilterParams objectForKey:@"service_type"];
    if (_selectedNotificationType.length) {
        
        
        CGRect frame = [self rectForText:_selectedNotificationType usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        
        [btn setBtnTitle:_selectedNotificationType];
        [btn addTarget:self action:@selector(btnCapsuleServiceTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([_selectedNotificationType isEqualToString:NSLocalizedString(@"all", nil)])
        {
            btn.btnCross.hidden = YES;
            btn.userInteractionEnabled = NO;
        }
        else
        {
            btn.btnCross.hidden = NO;
        }
        
        
        [([UIScreen mainScreen].bounds.size.height == 812.0 ?navFilterScrollView.scroll : self.scrollView)  addSubview:btn];
        
        //[self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
    }
    
    
    for (int i = 0; i < arrStates.count; i++)
    {
        
        NSString *stateName = arrStates[i];
        
        
        CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:stateName];
        // [btn set]
        [btn addTarget:self action:@selector(btnCapsuleStateClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        if ([stateName isEqualToString:NSLocalizedString(@"all", nil)])
        {
            btn.btnCross.hidden = YES;
            btn.userInteractionEnabled = NO;
        }
        else
        {
            btn.btnCross.hidden = NO;
        }
        
        
        //  btn.btnCross.
        btn.tag = 2300+i;
        // [self.scrollView addSubview:btn];
        [([UIScreen mainScreen].bounds.size.height == 812.0 ?navFilterScrollView.scroll : self.scrollView)  addSubview:btn];
        
        xCord+=frame.size.width + padding;
    }
    
    
    // Now fetch Categories
    
    
    NSMutableArray *arrCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    for (int i = 0; i<arrCategories.count; i++) {
        NSString *serviceName = arrCategories[i];
        
        
        CGRect frame = [self rectForText:serviceName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:serviceName];
        [btn addTarget:self action:@selector(btnCapsuleCategoryClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        btn.tag = 2300+i;
        // [self.scrollView addSubview:btn];
        [([UIScreen mainScreen].bounds.size.height == 812.0 ?navFilterScrollView.scroll : self.scrollView)  addSubview:btn];
        
        xCord+=frame.size.width + padding;
    }
    
    
    CGFloat height =([UIScreen mainScreen].bounds.size.height == 812.0 ?navFilterScrollView.scroll.frame.size.height : self.scrollView.frame.size.height);
    
    [([UIScreen mainScreen].bounds.size.height == 812.0 ?navFilterScrollView.scroll : self.scrollView) setContentSize:CGSizeMake(xCord, height)];
    
    
    //  [self.scrollView setContentSize:CGSizeMake(xCord, self.scrollView.frame.size.height)];
    
    if (self.isFromHomeFilter)
    {
        [self getFilterDataForHomeScreen];
    }
    
    
    
}

-(void)btnCapsuleServiceTypeClicked:(SDCapsuleButton*)btnSender{
    //added here
    //added here
    //added here
    if ([[btnSender btnTitle] isEqualToString:NSLocalizedString(@"regional", nil)])
    {
        
        [self.dictFilterParams removeObjectForKey:@"service_type"];
        [self.dictFilterParams removeObjectForKey:@"state_name"];
        
    }
    else
    {
        [self.dictFilterParams removeObjectForKey:@"service_type"];
        
    }
    //added here
    //added here
    //added here
    
    [self addFilterOptionsAtNavigationBar];
}


-(void)btnCapsuleStateClicked:(SDCapsuleButton*)btnSender{
    
    NSLog(@"self.dictFilterParams=%@",self.dictFilterParams);
    NSMutableArray *arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    [arrStates removeObject:[btnSender btnTitle]];
    //added here
    //added here
    //added here
    if ([arrStates count]==0) {
        [self.dictFilterParams removeObjectForKey:@"service_type"];
    }
    //added here
    //added here
    //added here
    
    [self.dictFilterParams setObject:arrStates forKey:@"state_name"];
    [self addFilterOptionsAtNavigationBar];
    
}


-(void)btnCapsuleCategoryClicked:(SDCapsuleButton*)btnSender
{
    
    NSMutableArray *arrCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    [arrCategories removeObject:[btnSender btnTitle]];
    
    [self.dictFilterParams setObject:arrCategories forKey:@"category_type"];
    [self addFilterOptionsAtNavigationBar];
    
}

-(void)getFilterDataForHomeScreen{
    if (arrTableData == nil) {
        arrTableData = [[NSMutableArray alloc] init];
    }
    else{
        [arrTableData removeAllObjects];
    }
    NSString *sortBy = [self.dictFilterParams objectForKey:@"sort_by"];
    NSString *serviceType = [self.dictFilterParams objectForKey:@"service_type"];
    NSArray *_arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    
    //added here
    //added here
    //added here
    /*  if ([self.comingFromFilter isEqualToString:@"fromAllServices"])
     {
     
     }
     else
     {
     if ([_arrStates count]==0)
     {
     serviceType=@"";
     }
     }*/
    //added here
    //added here
    //added here
    NSMutableArray *arrSelectedCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    
    
    if ([self.comingFromFilter isEqualToString:@"fromAllServices"])
    {
        NSArray *arrFilterResponse =  [singleton.dbManager getFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
        [arrTableData addObjectsFromArray:arrFilterResponse];
    }
    
   else if ([self.comingFromFilter isEqualToString:@"FlagHomeTabVC"])
    {
        NSArray *arrFilterResponse =  [singleton.dbManager getInfoFlagFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
        [arrTableData addObjectsFromArray:arrFilterResponse];
    }
    else if ([self.comingFromFilter isEqualToString:@"HomeTabService"])
    {
        NSArray *arrFilterResponse =  [singleton.dbManager getFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
        [arrTableData addObjectsFromArray:arrFilterResponse];
    }
    else if ([self.comingFromFilter isEqualToString:@"fromStateTab"])
    {
        NSMutableArray *arrSelectedCategories = [self.dictFilterParams objectForKey:@"category_type"];
        NSArray *arrFilterResponse =  [singleton.dbManager getFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
        [arrTableData addObjectsFromArray:arrFilterResponse];
    }
    else if ([self.comingFromFilter isEqualToString:@"showMore"])
    {
        NSMutableArray *arrSelectedCategories = [self.dictFilterParams objectForKey:@"category_type"];
        NSArray *arrFilterResponse =  [singleton.dbManager getFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
        
        NSMutableArray *arr_filterCategory=[[NSMutableArray alloc]init];
        
        arr_filterCategory=[arrFilterResponse valueForKey:@"SERVICE_ID"];
        
      //  NSArray *tempArrServiceSection=[singleton.dbManager loadDataServiceSection];
        
        NSInteger indexpass=0;
        if (self.isFromIndexFilter ==0) {
            self.isFromIndexFilter=1;
        }
        else
        {
            indexpass =self.isFromIndexFilter;// [tempArrServiceSection count]-1;
            
            //tempArrServiceSection
            indexpass = self.isFromIndexFilter -1;
        }
        
        NSArray *arrServiceSection=[[singleton.dbManager loadDataServiceSection]objectAtIndex: indexpass];//indexpass


        NSArray *itemsService = [[arrServiceSection valueForKey:@"SECTION_SERVICES"] componentsSeparatedByString:@","];
        NSLog(@"itemsService=%@",itemsService);
        
        // NSMutableArray *section_service=[[NSMutableArray alloc]init];
        
        for (int j=0; j<[itemsService count]; j++)
        {
            
            NSString *checkId=[NSString stringWithFormat:@"%@",[itemsService objectAtIndex:j]];
            NSLog(@"checkId=%@",checkId);
            
            if ([arr_filterCategory containsObject:checkId]) // YES
            {
                // Do something
                NSArray *sectionserviceitem=[singleton.dbManager getServiceData:[itemsService objectAtIndex:j]];
                NSLog(@"sectionserviceitem=%@",sectionserviceitem);
                if ([sectionserviceitem count]!=0) {
                    [arrTableData addObject:[sectionserviceitem objectAtIndex:0]];
                    
                }
                
            }
            
            else
            {
                //do nothing
            }
        }
    }
    else
    {
        NSArray *arrFilterResponse =  [singleton.dbManager getFilteredFavouriteServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
        [arrTableData addObjectsFromArray:arrFilterResponse];
    }
    
    
    
    
    if ([arrTableData count]!=0) {
        
        
        if ([sortBy isEqualToString:NSLocalizedString(@"alphabetic", nil)])
        {
            NSSortDescriptor *aphabeticDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
            NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
            arrTableData = [[arrTableData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
            
            
        }
        
        if ([sortBy isEqualToString:NSLocalizedString(@"most_popular", nil)])
        {
            NSSortDescriptor *aphabeticDescriptor = [[NSSortDescriptor alloc] initWithKey:@"SERVICE_POPULARITY" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
            arrTableData = [[arrTableData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        }
        
        
        if ([sortBy isEqualToString:NSLocalizedString(@"top_rated", nil)])
        {
            NSSortDescriptor *aphabeticDescriptor = [[NSSortDescriptor alloc] initWithKey:@"SERVICE_RATING" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
            arrTableData = [[arrTableData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        }
        
    }
    
    //-------
    
    
    if ([arrTableData count]>0) {
        
        
        /*   NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
         
         NSArray * sortDescriptors = [NSArray arrayWithObjects: sort, nil];
         NSArray * sortedArray = [arrTableData sortedArrayUsingDescriptors:sortDescriptors];
         NSLog(@"sortedArray %@",sortedArray);
         [arrTableData removeAllObjects];
         arrTableData = [sortedArray mutableCopy];*/
        _tblSearchFilter.hidden=FALSE;
        noResultsVW.hidden = true;
        
    }
    else
    {
        _tblSearchFilter.hidden=TRUE;
        //vw_noresults.hidden=TRUE;
        noResultsVW.hidden = false;
        
        
    }
    [_tblSearchFilter reloadData];
}


/*
 
 Handle condition to jump here from
 call method name getFilteredFavouriteServiceData for favourite filters
 
 typedef enum{
 FILTER_HOME =108,
 FILTER_FAVOURITE,
 FILTER_ALLSERVICE,
 FILTER_NEARME,
 }FILTER_TYPE;
 
 
 */

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.001;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return nil;
}



-(void)btnCrossClicked:(SDCapsuleButton*)btnSender
{
    
}

-(void)prepareTempData
{
    arrSortOptions = @[@"Alphabetic",@"Near By",@"Top Rated"];
    
    arrFilterOptions = @[@"Banking & Finance",@"Identification",@"Electricity",@"Property & Real Estate",@"Health care",@"Education",@"Traffic"];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 105.0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return arrTableData.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *simpleTableIdentifier = @"FavouriteCell";
    
    FavouriteCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[FavouriteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    
    //dispatch_async(dispatch_get_main_queue(), ^{
    NSDictionary *dict = [arrTableData objectAtIndex: indexPath.row];
    
    
    cell.lbl_serviceName.text=[dict valueForKey:@"SERVICE_NAME"];
    cell.lbl_serviceDesc.text=[dict valueForKey:@"SERVICE_DEPTDESCRIPTION"];
    
    NSString *imageURLString = [dict objectForKey:@"SERVICE_IMAGE"];
    
    
    
    NSURL *url=[NSURL URLWithString:imageURLString];
    
    [cell.img_serviceCateg sd_setImageWithURL:url
                             placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
    
    
    
    
    cell.lbl_nouse.text = @"";
    
    
    //---------- Set business Unit color here---------------------------------
    cell.lbl_serviceCateg.backgroundColor = [UIColor clearColor];
    //NSLog(@"get_BUcolorcode=%@",get_BUcolorcode);
    cell.lbl_serviceCateg.textColor=[UIColor whiteColor];
    
    
    //        NSString *category = [dict valueForKey:@"SERVICE_CATEGORY"];
    //
    //        NSMutableArray *arrCat = [[NSMutableArray alloc] init];
    //        /*NSArray *arr = [category componentsSeparatedByString:@","];
    //         for (NSString *cat in arr)
    //         {
    //         [arrCat addObject:cat];
    //         }*/
    //        [arrCat addObject:category];
    //
    //
    //        NSString *stateId = [dict valueForKey:@"SERVICE_STATE"];
    //        NSString *stateName = [obj getStateName:stateId];
    //        if (stateId.length == 0 || [stateId isEqualToString:@"99"]) {
    //            stateName =  NSLocalizedString(@"central", @"");
    //        }
    //        NSMutableArray *arrState = [[NSMutableArray alloc] initWithObjects:stateName, nil];
    //
    //        /*NSString *otherStateID = [dict valueForKey:@"SERVICE_OTHER_STATE"];
    //         if (otherStateID.length != 0 && ![otherStateID isEqualToString:@"99"]) {
    //         NSArray *arr = [otherStateID componentsSeparatedByString:@","];
    //         for (NSString *stateID in arr) {
    //         [arrState addObject:[obj getStateName:stateID]];
    //         }
    //         }
    //         */
    //
    //        //  NSString *stateName = [obj getStateName:stateId];
    //
    //        //cell.lbl_serviceCateg.text= category;
    //
    //        CGFloat fontSize = 12.0;
    //        if (singleton.fontSizeSelectedIndex == 0) {
    //            fontSize = 12.0f;
    //        }
    //        else if (singleton.fontSizeSelectedIndex == 1) {
    //            fontSize = 14.0f;
    //        }
    //        else if (singleton.fontSizeSelectedIndex == 2) {
    //            fontSize = 16.0f;
    //        }
    //        [cell addCategoryStateItemsToScrollView:arrCat state:arrState withFont:fontSize cell:cell andTagIndex:indexPath];
    [cell stateNameWithCategory:dict ofCell:cell andTagIndex:indexPath];
    [cell.btn_serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btn_serviceFav.tag=indexPath.row ;
    
    NSString *serviceid=[NSString stringWithFormat:@"%@",[[arrTableData valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
    
    NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceid]];//get service status from db
    
    if ([serviceFav isEqualToString:@"true"])
    {
        cell.btn_serviceFav .selected=YES;
    }else{
        cell.btn_serviceFav .selected=NO;
    }
    
    NSDictionary *cellData = (NSDictionary*)[arrTableData objectAtIndex:[indexPath row]];
    cell.btn_serviceFav.celldata = [arrTableData objectAtIndex:[indexPath row]];
    
    cell.btn_serviceFav.usdata=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_NAME"]];
    
    
    //cell.btn_serviceFav.selected = [[[arrTableData objectAtIndex:indexPath.row] valueForKey:@"SERVICE_IS_FAV"] boolValue] ;
    
    cell.btn_moreInfo.tag=indexPath.row;
    
    
    [cell.btn_moreInfo  addTarget:self action:@selector(btnMoreInfoClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString *rating=[dict valueForKey:@"SERVICE_RATING"];
    //[cellData objectForKey:@"SERVICE_RATING"];
    
    cell.lbl_rating.text=rating;
    
    
    cell.lbl_serviceName.font = [AppFont semiBoldFont:16];
    cell.lbl_serviceDesc.font = [AppFont lightFont:14.0];
    cell.lbl_rating.font = [AppFont regularFont:13.0];
    
    //});
    
    return cell;
    
    /* static NSString *CellIdentifier = @"searchFilterCell";
     SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
     
     
     if (self.isFromHomeFilter) {
     NSDictionary *dict = [arrTableData objectAtIndex: indexPath.row];
     
     NSString *imageURLString = [dict objectForKey:@"SERVICE_IMAGE"];
     if (imageURLString) {
     NSURL *imgURL = [NSURL URLWithString:imageURLString];
     [cell.imgSearchCell sd_setImageWithURL:imgURL
     placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
     }
     
     
     cell.lblHeader.text = [dict objectForKey:@"SERVICE_NAME"] ;
     cell.lblDescription.text = [dict objectForKey:@"SERVICE_DEPTDESCRIPTION"] ;
     NSString *category = [dict objectForKey:@"SERVICE_CATEGORY"] ;
     cell.lblEducation.text= category;
     
     CGFloat fontSize = 12.0;
     if (singleton.fontSizeSelectedIndex == 0)
     {
     fontSize = 12.0f;
     }
     else if (singleton.fontSizeSelectedIndex == 1) {
     fontSize = 14.0f;
     }
     else if (singleton.fontSizeSelectedIndex == 2) {
     fontSize = 16.0f;
     }
     
     CGRect frame = [self rectForText:category usingFont:[UIFont systemFontOfSize:fontSize] boundedBySize:CGSizeMake(self.view.frame.size.width, cell.lblEducation.frame.size.height)];
     
     CGRect categoryLabelFrame =  CGRectMake(cell.lblEducation.frame.origin.x+5, cell.lblEducation.frame.origin.y, frame.size.width+10,cell.lblEducation.frame.size.height);
     cell.lblEducation.layer.backgroundColor=[UIColor colorWithRed:235/255.0 green:86/255.0 blue:2/255.0 alpha:1.0].CGColor;
     cell.lblEducation.layer.cornerRadius = 10.0;
     [cell.lblEducation setClipsToBounds:YES];
     cell.lblEducation.frame = categoryLabelFrame;
     
     // cell.lblEducation.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
     
     
     
     
     
     
     }
     else{
     NotificationItemBO *object = [arrTableData objectAtIndex: indexPath.row];
     
     cell.imgSearchCell.image = [UIImage imageNamed:object.imgName];
     cell.lblHeader.text = NSLocalizedString(object.headerTitle, nil) ;
     cell.lblDescription.text = NSLocalizedString(object.headerDesc, nil) ;
     
     CGRect buttonImageAdvanceFrame =  cell.imgSearchCell.frame;
     buttonImageAdvanceFrame.origin.x = singleton.isArabicSelected ?CGRectGetWidth(tableView.frame) - 80 : 12;
     cell.imgSearchCell.frame = buttonImageAdvanceFrame;
     
     
     CGRect lblHeaderFrame =  cell.lblHeader.frame;
     lblHeaderFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
     lblHeaderFrame.size.width = fDeviceWidth - 100;
     cell.lblHeader.frame = lblHeaderFrame;
     cell.lblHeader.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
     
     
     CGRect lbl_servicedescFrame =  cell.lblDescription.frame;
     lbl_servicedescFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
     lbl_servicedescFrame.size.width = fDeviceWidth - 100;
     cell.lblDescription.frame = lbl_servicedescFrame;
     cell.lblDescription.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
     
     
     }
     cell.layoutMargins = UIEdgeInsetsZero;
     return cell;
     */
}
//==================================
//       CUSTOM PICKER STARTS
//==================================
- (void)btnOpenSheet

{
    NSString *information=NSLocalizedString(@"information", nil);
    NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self callDetailServiceVC:self.cellDataOfmore];
        }
            break;
        case 1:
        {
            NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
            
            NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
            
            NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
            
            NSString *deptName=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_NAME"]];
            deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]])
                
            {
                NSLog(@"Map App Found");
                
                NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
                
                // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
                
                NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSLog(@"mapURL= %@",mapURL);
                
                [[UIApplication sharedApplication] openURL:mapURL];
            } else
            {
                NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
                
            }
            
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

//==================================
//          CUSTOM PICKER ENDS
//==================================


- (IBAction)btnMoreInfoClicked:(UIButton *)sender
{
    
    
    NSDictionary *cellData = (NSDictionary*)[arrTableData objectAtIndex:[sender tag]];
    
    
    self.cellDataOfmore=(NSMutableDictionary*)cellData;
    
    [self btnOpenSheet];
    
    
    /*  NSString *titlename=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_NAME"]];
     
     
     UIAlertController * alert=   [UIAlertController
     alertControllerWithTitle:titlename
     message:nil
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* info = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"information", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     //[self displayContentController:[self getHomeDetailLayerLeftController]];
     [self callDetailServiceVC:cellData];
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* map = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"view_on_map", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     
     NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[ cellData  valueForKey:@"SERVICE_LONGITUDE"]]);
     
     NSString *latitude=[NSString stringWithFormat:@"%@",[cellData valueForKey:@"SERVICE_LATITUDE"]];
     
     NSString *longitute=[NSString stringWithFormat:@"%@",[ cellData  valueForKey:@"SERVICE_LONGITUDE"]];
     
     
     // NSString* googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@",latitude, longitute];
     
     
     
     // [[UIApplication sharedApplication] openURL: [NSURL URLWithString: googleMapsURLString]];
     
     
     if ([[UIApplication sharedApplication] canOpenURL:
     [NSURL URLWithString:@"comgooglemaps://"]])
     
     {
     NSLog(@"Map App Found");
     
     NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f&center=%.6f,%.6f&zoom=15&views=traffic", [latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     
     
     
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
     
     
     } else
     {
     NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
     
     }
     
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     
     // [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* cancel = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"cancel", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     
     
     [alert addAction:info];
     [alert addAction:map];
     
     [alert addAction:cancel];
     
     
     UIViewController *vc=[self topMostController];
     [vc presentViewController:alert animated:NO completion:nil];
     */
    
    
}
//----------- END OF MORE INFO POP UP VIEW---------------
-(void)callDetailServiceVC:(NSDictionary*)celldatapass
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    vc.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [topvc presentViewController:vc animated:NO completion:nil];
    
}


- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (IBAction)fav_action:(MyFavButton *)sender  {
    
    MyFavButton *button = (MyFavButton *)sender;
    // Add image to button for normal state
    //int indexOfTheRow=(int)button.tag;
    
    
    // NSString *text = [NSString stringWithFormat:@"%@",indexOfTheRow];
    
    /*NSString *serviceFav=[NSString stringWithFormat:@"%@",[[arrTableData  objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_IS_FAV"]];
     
     NSString *serviceId=[[arrTableData objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_ID"];
     
     if ([serviceFav isEqualToString:@"true"])// Is selected?
     {
     [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
     button.selected=FALSE;
     [arrTableData removeObjectAtIndex:indexOfTheRow];
     
     
     }
     else
     {
     [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
     button.selected=true;
     }*/
    
    [button.celldata  valueForKey:@"SERVICE_ID"];
    
    // Add image to button for pressed state
    UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
    UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
    
    [button setImage:btnImage1 forState:UIControlStateNormal];
    [button setImage:btnImage2 forState:UIControlStateSelected];
    
    NSString *serviceId = [button.celldata  valueForKey:@"SERVICE_ID"];
    
    NSString *serviceFav=[singleton.dbManager getServiceFavStatus:serviceId];
    
    
    
    // NSString *serviceFav=[cellData valueForKey:@"SERVICE_IS_FAV"];
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        button.selected=FALSE;
        
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
        
    }
    else
    {
        button.selected=true;
        
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
        
    }
    
    singleton.loadServiceStatus =@"FALSE";//if added load service status
    
    [self checkServiceStatus];
    
    [self performSelector:@selector(reloadTableAfterDelay) withObject:nil afterDelay:0.3];
    
}
-(void)reloadTableAfterDelay
{
    [_tblSearchFilter reloadData];
}
-(void)checkServiceStatus
{
    if ([arrTableData count]>0) {
        // self.lbl_favourite.hidden=true;
        //vw_noServiceFound.alpha=0;
        
        // vw_noServiceFound.hidden=TRUE;
        // table_favView.hidden=false;
    }
    else
    {
        
        //vw_noServiceFound.hidden=FALSE;
        
        // self.lbl_favourite.hidden=false;
        //table_favView.hidden=true;
        if ([singleton.loadServiceStatus isEqualToString:@"TRUE"])
        {
            //  vw_noServiceFound.hidden=TRUE;
            
            
            
        }
        if ([singleton.loadServiceStatus isEqualToString:@"FALSE"])
        {
            //  vw_noServiceFound.hidden=FALSE;
            
        }
        
        
        
    }
    
    
    
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    if ([cell isKindOfClass:[FavouriteCell class]])
    {
        FavouriteCell *favCell = (FavouriteCell*)cell;
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //            favCell.lblName.font = [UIFont systemFontOfSize:16.0];
            //            favCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            //            [favCell setNeedsDisplay];
            //        });
            
            //  }
            
            
            CGRect buttonImageAdvanceFrame =  favCell.img_serviceCateg.frame;
            buttonImageAdvanceFrame.origin.x = singleton.isArabicSelected ?CGRectGetWidth(tableView.frame) - 80 : 12;
            favCell.img_serviceCateg.frame = buttonImageAdvanceFrame;
            
            
            CGRect lblServiceTitle =  favCell.lbl_serviceName.frame;
            lblServiceTitle.origin.x = singleton.isArabicSelected ? 10  : 80;
            lblServiceTitle.size.width = fDeviceWidth - 100;
            favCell.lbl_serviceName.frame = lblServiceTitle;
            favCell.lbl_serviceName.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            
            CGRect lbl_servicedescFrame =  favCell.lbl_serviceDesc.frame;
            lbl_servicedescFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
            lbl_servicedescFrame.size.width = fDeviceWidth - 100;
            favCell.lbl_serviceDesc.frame = lbl_servicedescFrame;
            favCell.lbl_serviceDesc.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            CGRect lblRatingFrame =  favCell.lbl_rating.frame;
            lblRatingFrame.origin.x = singleton.isArabicSelected ? fDeviceWidth - 110  : 80;
            favCell.lbl_rating.frame = lblRatingFrame;
            //        cell.lbl_rating.backgroundColor = [UIColor redColor];
            
            CGRect starImageFrame =  favCell.img_star.frame;
            starImageFrame.origin.x = singleton.isArabicSelected ? lblRatingFrame.origin.x -20  : lblRatingFrame.origin.x +lblRatingFrame.size.width -5;
            favCell.img_star.frame = starImageFrame;
            
            
            
            
            CGRect categoryLabelFrame =  favCell.lbl_serviceCateg.frame;
            categoryLabelFrame.origin.x = singleton.isArabicSelected ? starImageFrame.origin.x - 90  : starImageFrame.origin.x + 40 ;
            favCell.lbl_serviceCateg.frame = categoryLabelFrame;
            
            
            CGRect btnFavFrame =  favCell.btn_serviceFav.frame;
            btnFavFrame.origin.x = singleton.isArabicSelected ? 10  : fDeviceWidth - 30 ;
            favCell.btn_serviceFav.frame = btnFavFrame;
            
            CGRect btnMoreFrame =  favCell.btn_moreInfo.frame;
            btnMoreFrame.origin.x = singleton.isArabicSelected ? 5  : fDeviceWidth - 38 ;
            favCell.btn_moreInfo.frame = btnMoreFrame;
        });
    }
    
    
    /* if ([cell isKindOfClass:[SearchCell class]])
     {
     SearchCell *searchCell = (SearchCell*)cell;
     
     
     dispatch_async(dispatch_get_main_queue(), ^{
     
     CGRect buttonImageAdvanceFrame =  searchCell.imgSearchCell.frame;
     buttonImageAdvanceFrame.origin.x = singleton.isArabicSelected ?CGRectGetWidth(tableView.frame) - 80 : 12;
     searchCell.imgSearchCell.frame = buttonImageAdvanceFrame;
     
     
     CGRect lblHeaderFrame =  searchCell.lblHeader.frame;
     lblHeaderFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
     lblHeaderFrame.size.width = fDeviceWidth - 100;
     searchCell.lblHeader.frame = lblHeaderFrame;
     searchCell.lblHeader.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
     
     
     CGRect lbl_servicedescFrame =  searchCell.lblDescription.frame;
     lbl_servicedescFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
     lbl_servicedescFrame.size.width = fDeviceWidth - 100;
     searchCell.lblDescription.frame = lbl_servicedescFrame;
     searchCell.lblDescription.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
     
     
     
     
     });
     }*/
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *cellData = [arrTableData objectAtIndex:indexPath.row];
    if (cellData)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        vc.dic_serviceInfo=cellData;
        vc.tagComeFrom=@"OTHERS";
                
        vc.sourceTab     = @"filter";
        vc.sourceState   = @"";
        vc.sourceSection = @"";
        vc.sourceBanner  = @"";
        
        [self presentViewController:vc animated:NO completion:nil];
        
        
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    NSLog(@"inside back");
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    
    
    if ([self.comingFromFilter isEqualToString:@"fromAllServices"])
    {
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass:[AllServicesTabVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
            }
        }
    }
    
    
  // add later to pop to FlagHomeTabVC
  else  if ([self.comingFromFilter isEqualToString:@"FlagHomeTabVC"])
    {
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass:[FlagHomeTabVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
            }
        }
    }
    
    
    else if ([self.comingFromFilter isEqualToString:@"HomeTabService"])
    {
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass:[HomeTabVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
            }
            
            if ([aViewController isKindOfClass:[HomeWithFav_TabVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
            }
            
        }
    }
    
    else if ([self.comingFromFilter isEqualToString:@"FavouriteTab"])
    {
        for (UIViewController *aViewController in allViewControllers)
        {
            
            if ([aViewController isKindOfClass:[HomeWithFav_TabVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
            }
        }
    }
    
    
    
    
    else if ([self.comingFromFilter isEqualToString:@"fromStateTab"])
    {
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass:[NearMeTabVC class]])
            {
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
            }
          
        }
    }
    else if([self.comingFromFilter isEqualToString:@"showMore"])
    {
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        
        
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass:[ShowMoreServiceVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
                
            }
            
            
        }
    }
    
    else
    {
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass:[FavouriteTabVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
                
            }
        }
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    
    [_tblSearchFilter reloadData];
    // [_tblSearchFilter setNeedsLayout ];
    // [_tblSearchFilter layoutIfNeeded ];
    // [_tblSearchFilter reloadData];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    lb_noresults.font = [AppFont regularFont:14.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnSortActionClicked:(UIButton *)sender
{
    btnFilter.selected = NO;
    btnSort.selected = YES;
    [self showSortOptions];
}

- (IBAction)btnFilterClicked:(UIButton*)sender
{
    btnFilter.selected = YES;
    btnSort.selected = NO;
    
    [self showFilterOptions];
}

#pragma mark- Sort Picker

-(void)showSortOptions
{
    
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"sort", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"Apply", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = YES;
    picker.allowRadioButtons = YES;
    picker.isClearOptionRequired = NO;
    [picker show];
    
}

-(void)showFilterOptions
{
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"filter_caps", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"Apply", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = YES;
    picker.isClearOptionRequired = NO;
    picker.allowRadioButtons = YES;
    [picker show];
    
}




- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row
{
    NSAttributedString *att;
    
    if ([btnSort isSelected])
    {
        att = [[NSAttributedString alloc]
               initWithString:NSLocalizedString(arrSortOptions[row], nil)
               attributes:@{
                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                            }];
        
        
    }
    else
    {
        att = [[NSAttributedString alloc]
               initWithString:NSLocalizedString(arrFilterOptions[row], nil)
               attributes:@{
                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                            }];
    }
    
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row
{
    if ([btnFilter isSelected])
    {
        return arrFilterOptions[row];
    }
    else
        
        return arrSortOptions[row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    if ([btnFilter isSelected])
        return arrFilterOptions.count;
    
    
    else
    {
        return arrSortOptions.count;
    }
}


/* picker item image for each row */
//- (UIImage *)czpickerView:(CZPickerView *)pickerView imageForRow:(NSInteger)row
//{
//    if ([btnFilter isSelected])
//    {
//        UIImage *imageIcon = nil;
//        switch (row) {
//            case 0:
//                imageIcon = [UIImage imageNamed:@"icon_alphabatic-1"];
//                break;
//            case 1:
//                imageIcon = [UIImage imageNamed:@"icon_nearby"];
//
//                break;
//            case 2:
//                imageIcon = [UIImage imageNamed:@"icon_top_rated"];
//
//                break;
//
//            default:
//                break;
//
//    }
//    }
//    else
//    {
//
//    }
//


//  return imageIcon;
//}


- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows
{
    
    
    
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    NSLog(@"Canceled.");
}


- (IBAction)btnSettingAgainClicked:(id)sender
{
    
    if ([self.delegate respondsToSelector:@selector(filterChanged:andCategoryDict:)])
    {
        
        for (int i = 0; i< self.filterBOArray.count; i++)
        {
            FilterServicesBO *objBo = [self.filterBOArray objectAtIndex:i];
            
            if ([[self.dictFilterParams objectForKey:@"category_type"] containsObject:objBo.serviceName])
            {
                objBo.isServiceSelected = YES;
            }
            else
            {
                objBo.isServiceSelected = NO;
            }
        }
        
        
        NSMutableDictionary *categoryDict = [NSMutableDictionary new];
        
        if ([self.dictFilterParams valueForKey:@"state_name"] != nil)
        {
            [categoryDict setValue:[self.dictFilterParams valueForKey:@"state_name"] forKey:@"selectedStates"];
            
        }
        
        if ([self.dictFilterParams valueForKey:@"service_type"] != nil)
        {
            [categoryDict setValue:[self.dictFilterParams valueForKey:@"service_type"] forKey:@"selectedCategory"];
        }
        
        [self.delegate filterChanged:self.filterBOArray andCategoryDict:categoryDict];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    [_tblSearchFilter reloadData];
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}



/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/


@end

