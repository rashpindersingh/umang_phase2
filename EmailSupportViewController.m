//
//  EmailSupportViewController.m
//  Umang
//
//  Created by Lokesh Jain on 12/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "EmailSupportViewController.h"
#import "DepartmentListingViewController.h"

#import "SDWebImageManager.h"
#import <MessageUI/MessageUI.h>
#import "SharedManager.h"
#import "CallBoxTVC.h"

@interface EmailSupportViewController ()<MFMailComposeViewControllerDelegate,UIActionSheetDelegate>
{
    NSMutableArray *cellArray;
    NSString *emailString;
    NSString *phoneString;
    
    SharedManager *singleton;
    NSMutableArray *phoneContactsArray;
    __weak IBOutlet UIButton *backBtn;
    
    BOOL isUmangSupportSelected;
}

@end

@implementation EmailSupportViewController





#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


-(void)checkNetworkView:(BOOL)hide {
    AppDelegate *app = [AppDelegate sharedDelegate];
    UIViewController *topvc= [app topMostController];
    
    if ([topvc.view viewWithTag:1779]) {
        [topvc.view viewWithTag:1779].hidden = hide;
    }
}
-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self checkNetworkView:false];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    backBtn.hidden = true;
    self.screentTitle.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backButtonClicked:(UIButton*)btnBack];
    };
    nvView.lblTitle.text =  self.screentTitle.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX()) {
        CGRect tbFrame = self.supportLabel.frame;
        tbFrame.origin.y = 25 + kiPhoneXNaviHeight
        self.supportLabel.frame = tbFrame;
        CGRect tblFrame = self.supportTableView.frame;
        tblFrame.origin.y = CGRectGetMaxY(tbFrame) + 2.0;
        self.supportTableView.frame = tblFrame;
        [self.view layoutIfNeeded];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    /*
     
     NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
     [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
     
     */
    //———— Add to handle network bar of offline——
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    [super viewWillAppear:YES];
    
    
    
    
    self.bottomButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.bottomButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.chooseDeptButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.chooseDeptButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
    NSLog(@"%@",self.deptDict);
    phoneContactsArray = [NSMutableArray new];
    
    self.errorView.hidden = YES;
    
    if (self.deptDict != nil)
    {
        
        self.bottomButton.hidden = NO;
        
        
        
        
        [self.bottomButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"contact_department", nil),[[self.deptDict valueForKey:@"SERVICE_NAME"] uppercaseString]] forState:UIControlStateNormal];
        
        NSString *deptText = [self.deptDict valueForKey:@"SERVICE_NAME"] != nil ? [self.deptDict valueForKey:@"SERVICE_NAME"] : @"";
        
        
        [self.chooseDeptButton setTitle:deptText forState:UIControlStateNormal];
        
        if ([self.deptDict valueForKey:@"SERVICE_IMAGE"] != nil)
        {
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:[NSURL URLWithString:[self.deptDict valueForKey:@"SERVICE_IMAGE"]]
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        self.chooseDeptImageView.image = image;
                                        // do something with image
                                    }
                                    else
                                    {
                                        self.chooseDeptImageView.image = nil;
                                    }
                                }] ;
        }
        else
        {
            self.chooseDeptImageView.image = nil;
        }
        
        
        
        emailString = [self.deptDict valueForKey:@"SERVICE_EMAIL"];
        
        
        if ([[self.deptDict valueForKey:@"SERVICE_PHONE_NUMBER"] isEqualToString:@""])
        {
            phoneString = @"";
        }
        else
        {
            
            phoneContactsArray = [[[self.deptDict valueForKey:@"SERVICE_PHONE_NUMBER"] componentsSeparatedByString:@","] mutableCopy] ;
        }
        
        
        if ([self.comingFrom isEqualToString:@"EmailSupport"])
        {
            
            if (emailString.length == 0)
            {
                emailString = singleton.emailSupport;
                phoneString = singleton.phoneSupport;
                
                self.errorView.hidden = NO;
                
                [self.bottomButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"contact_umang_support", nil)] forState:UIControlStateNormal];
                
                
                self.errorMessageLabel.text = [NSString stringWithFormat:NSLocalizedString(@"email_support_not_available", nil),[[self.chooseDeptButton titleForState:UIControlStateNormal] uppercaseString]];
                
            }
            else
            {
                self.errorView.hidden = YES;
            }
            
        }
        else
        {
            
            if (phoneContactsArray.count == 1)
            {
                phoneString = [phoneContactsArray objectAtIndex:0];
            }
            
            if (phoneString.length == 0)
            {
                emailString = singleton.emailSupport;
                phoneString = singleton.phoneSupport;
                
                self.errorView.hidden = NO;
                
                [self.bottomButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"contact_umang_support", nil)] forState:UIControlStateNormal];
                
                self.errorMessageLabel.text = [NSString stringWithFormat:NSLocalizedString(@"phone_support_not_available", nil),[[self.chooseDeptButton titleForState:UIControlStateNormal] uppercaseString]];
                
            }
            else
            {
                self.errorView.hidden = YES;
            }
        }
        
    }
    else
    {
        emailString = singleton.emailSupport;
        phoneString = singleton.phoneSupport;
        self.errorView.hidden = YES;
    }
    _supportTableView.scrollEnabled = false;
    [self updateViewLayout];
    
    [self checkNetworkView:true];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isUmangSupportSelected = YES;
    
    [backBtn setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:backBtn.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(backBtn.frame.origin.x, backBtn.frame.origin.y, backBtn.frame.size.width, backBtn.frame.size.height);
        
        [backBtn setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        backBtn.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    
    [self.chooseDeptButton setTitle:NSLocalizedString(@"choose_department", nil) forState:UIControlStateNormal];
    
    
    if ([self.comingFrom isEqualToString:@"EmailSupport"])
    {
        //"help_phone_help" = "Phone Support";
        //"help_email_help" = "Email Support";
        
        self.screentTitle.text = NSLocalizedString(@"help_email_help", nil);
        self.supportLabel.text = NSLocalizedString(@"email_help_header", nil);
        [self.bottomButton setImage:[UIImage imageNamed:@"emailhelp"] forState:UIControlStateNormal];
        
        id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:EMAIL_SUPPORT_SCREEN];
        [[GAI sharedInstance].defaultTracker send:
         [[GAIDictionaryBuilder createScreenView] build]];
    }
    else
    {
        self.screentTitle.text = NSLocalizedString(@"help_phone_help", nil);
        self.supportLabel.text = NSLocalizedString(@"phone_help_header", nil);
        
        [self.bottomButton setImage:[UIImage imageNamed:@"call_support"] forState:UIControlStateNormal];
        id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:PHONE_SUPPORT_SCREEN];
        [[GAI sharedInstance].defaultTracker send:
         [[GAIDictionaryBuilder createScreenView] build]];
    }
    
    
    self.deptView.hidden = YES;
    
    
    cellArray = [[NSMutableArray alloc] initWithObjects:@{@"cellImage":@"feed_app",@"cellMainText":NSLocalizedString(@"umang_support_team", nil),@"cellDetailText":NSLocalizedString(@"contact_umang_app_support", nil)},@{@"cellImage":@"feed_department",@"cellMainText":NSLocalizedString(@"department_support_team", nil),@"cellDetailText":NSLocalizedString(@"contact_department_support_txt", nil)},nil];
    singleton = [SharedManager sharedSingleton];
    
    if (singleton.isArabicSelected==TRUE)
    {
        self.deptView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.chooseDeptButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.chooseDeptButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        self.chooseDeptImageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.supportLabel.textAlignment= NSTextAlignmentRight;
    }
    [self addNavigationView];
    self.supportTableView.scrollEnabled = NO;
    CGRect tblFrame = self.supportTableView.frame;
    tblFrame.size.height = 110;
    self.supportTableView.frame = tblFrame;
    
    CGRect deptFrame = self.deptView.frame;
    deptFrame.origin.y = tblFrame.origin.y + tblFrame.size.height + 15;
    deptFrame.size.height = 40;
    self.deptView.frame = deptFrame;
    [self layoutBorderWidthView:self.supportTableView];
    [self layoutBorderWidthView:self.deptView];
    
    [self updateViewLayout];
    
    
}
-(void)updateViewLayout
{
    CGRect btnFrame = self.chooseDeptButton.frame;
    if ([[self.chooseDeptButton titleForState:UIControlStateNormal] isEqualToString:NSLocalizedString(@"choose_department", nil)])
    {
        btnFrame.origin.x = 13;
        [self.deptView bringSubviewToFront:self.chooseDeptButton];
    }else {
        btnFrame.origin.x = 53;
        [self.deptView bringSubviewToFront:self.chooseDeptImageView];
    }
    self.chooseDeptButton.frame = btnFrame;
    [self.view layoutIfNeeded];
    
}
-(void)layoutBorderWidthView:(UIView*)view{
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 0.6;
    view.layer.masksToBounds = true;
    view.clipsToBounds = true ;
    
}
- (IBAction)chooseDeptClicked:(UIButton *)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DepartmentListingViewController *deptVC = [storyboard instantiateViewControllerWithIdentifier:@"DepartmentListing"];
    
    deptVC.viewComingFrom = self.comingFrom;
    [self.navigationController pushViewController:deptVC animated:YES];
    
}
- (IBAction)backButtonClicked:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)emailButonClicked:(id)sender
{
    
    if ([self.comingFrom isEqualToString:@"EmailSupport"])
    {
        
        NSString *emailIdString = emailString;
        
        if (isUmangSupportSelected == false)
        {
            emailIdString = [self.deptDict valueForKey:@"SERVICE_EMAIL"];
            if (self.deptDict == nil || (emailIdString == nil ||  emailIdString.length == 0 ))
            {
                emailIdString = emailString;
            }
            else
            {
                emailIdString = [self.deptDict valueForKey:@"SERVICE_EMAIL"];
            }
        }
        
        if (emailIdString.length>0)
        {
            if ([emailIdString containsString:@"|"] || [emailIdString containsString:@"#"] || [emailIdString containsString:@","])
            {
                UIStoryboard *story = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
                CallBoxTVC *callVC = [story instantiateViewControllerWithIdentifier:@"CallBoxTVC"];
                callVC.call = emailIdString;
                callVC.comingFrom = @"email";
                [self addChildViewController:callVC];
                [self.view addSubview:callVC.view];
                [callVC didMoveToParentViewController:self];
            }
            else
            {
                MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
                if ([MFMailComposeViewController canSendMail]) {
                    
                    
                    NSString *receipt=emailIdString;
                    // Email Subject
                    NSString *emailTitle = @"Umang Services Email";
                    // Email Content
                    NSString *messageBody = @"Sent by Umang App!";
                    // To address
                    // To address
                    NSArray *toRecipents = [NSArray arrayWithObject:receipt];
                    
                    picker.mailComposeDelegate = self;
                    [picker setSubject:emailTitle];
                    [picker setMessageBody:messageBody isHTML:YES];
                    [picker setToRecipients:toRecipents];
                    
                    // Present mail view controller on screen
                    [self presentViewController:picker animated:YES completion:NULL];
                }
            }
        }
        
    }
    else
    {
        NSString *contacts = phoneString;
        if (isUmangSupportSelected == false) {
           contacts=[self.deptDict valueForKey:@"SERVICE_PHONE_NUMBER"];
            if (self.deptDict == nil || (contacts == nil ||  contacts.length == 0 ))
            {
                contacts=phoneString;
            }
            else
            {
                contacts=[self.deptDict valueForKey:@"SERVICE_PHONE_NUMBER"];
            }
        }
        
        
        if (contacts.length>0)
        {
            if ([contacts containsString:@"|"] || [contacts containsString:@"#"] || [contacts containsString:@","]) {
                UIStoryboard *story = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
                CallBoxTVC *callVC = [story instantiateViewControllerWithIdentifier:@"CallBoxTVC"];
                callVC.comingFrom = @"call";
                callVC.call = contacts;
                [self addChildViewController:callVC];
                [self.view addSubview:callVC.view];
                [callVC didMoveToParentViewController:self];
             }
            else {
                NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",contacts];
                
                mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
            }
        }
        
        //        else
        //        {
        //            //do nothing
        //            // contactCell.btnCall.hidden = YES;
        //
        //
        //            UIAlertView *alert = [[UIAlertView alloc]
        //                                  initWithTitle:NSLocalizedString(@"phone_support", nil)                         message:NSLocalizedString(@"no_result_found", nil)
        //                                  delegate:self
        //                                  cancelButtonTitle:nil
        //                                  otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        //            [alert show];
        //        }
        
    }
    
    
}

- (void)openContactList
{
    // Create the sheet with only cancel button
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:NSLocalizedString(@"call", nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:nil];
    
    // Add buttons one by one (e.g. in a loop from array etc...)
    for (int i=0; i<[phoneContactsArray count]; i++)
    {
        NSString *contact=[NSString stringWithFormat:@"%@",[phoneContactsArray objectAtIndex:i]];
        [actionSheet addButtonWithTitle:contact];
        
    }
    
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if  (actionSheet.cancelButtonIndex == buttonIndex)
    {
        return;
    }
    else
    {
        if(buttonIndex>0)
        {
            buttonIndex=buttonIndex-1;
        }
        NSString*  callNumber=[NSString stringWithFormat:@"%@",[phoneContactsArray objectAtIndex:buttonIndex]];
        callNumber = [callNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",callNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}
#pragma mark - TableView Delegate and Datasource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return 50;
            break;
        case 1:
            return 65;
            break;
            
    }
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EmailCell *cell = (EmailCell *)[tableView dequeueReusableCellWithIdentifier:@"emailCellIdentifier"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.mainLabel.tag = indexPath.row;
    
    cell.mainLabel.textColor = [UIColor colorWithRed:117.0/255.0f green:117.0/255.0f blue:117.0/255.0f alpha:117.0/255.0f];
    
    if (indexPath.row == 0)
    {
        //cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        UIImageView *checkImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
        checkImage.image = [UIImage imageNamed:@"check_email"];
        
        if (singleton.isArabicSelected==TRUE)
        {
            checkImage.transform= CGAffineTransformMakeScale(-1.0, 1.0);
        }
        cell.accessoryView = checkImage;
        
        cell.mainLabel.textColor = [UIColor blackColor];
        
        
        [self.bottomButton setTitle:NSLocalizedString(@"contact_umang_support", nil) forState:UIControlStateNormal];
    }
    
    
    cell.optionImageView.image = [UIImage imageNamed:[[cellArray objectAtIndex:indexPath.row] valueForKey:@"cellImage"]];
    cell.mainLabel.text = [NSString stringWithFormat:@"%@",[[cellArray objectAtIndex:indexPath.row] valueForKey:@"cellMainText"]];
    NSString *text = [NSString stringWithFormat:@"%@",[[cellArray objectAtIndex:indexPath.row] valueForKey:@"cellDetailText"]];
    [cell.detailLabel setTitle:text forState:UIControlStateNormal];
    
    
    if (singleton.isArabicSelected==TRUE)
    {
        cell.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        cell.mainLabel.transform= CGAffineTransformMakeScale(-1.0, 1.0);
        cell.mainLabel.textAlignment=NSTextAlignmentRight;
        cell.optionImageView.transform= CGAffineTransformMakeScale(-1.0, 1.0);
        cell.detailLabel.transform= CGAffineTransformMakeScale(-1.0, 1.0);
        cell.detailLabel.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EmailCell *cell = (EmailCell *)[tableView cellForRowAtIndexPath:indexPath];
    UIImageView *checkImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    checkImage.image = [UIImage imageNamed:@"check_email"];
    [tableView cellForRowAtIndexPath:indexPath].accessoryView = checkImage;
    if (singleton.isArabicSelected==TRUE)
    {
        cell.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        checkImage.transform= CGAffineTransformMakeScale(-1.0, 1.0);
    }
    
    
    if (indexPath.row == 1)
    {
        NSIndexPath *indexPathForRow0 = [NSIndexPath indexPathForRow:0 inSection:0];
        [self tableView:tableView didDeselectRowAtIndexPath:indexPathForRow0];
        
    }
    
    cell.mainLabel.textColor = [UIColor blackColor];
    
        
    if (indexPath.row == 1)
    {
        
        isUmangSupportSelected = NO;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.deptView.hidden = NO;
        }];
        
        if ([[self.chooseDeptButton titleForState:UIControlStateNormal] isEqualToString:NSLocalizedString(@"choose_department", nil)])
        {
            self.bottomButton.hidden = YES;
        }
        else
        {
            [self.bottomButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"contact_department", nil),[[self.chooseDeptButton titleForState:UIControlStateNormal] uppercaseString]] forState:UIControlStateNormal];
            
            emailString = [self.deptDict valueForKey:@"SERVICE_EMAIL"];
            
            NSString *contacts=[self.deptDict valueForKey:@"SERVICE_PHONE_NUMBER"];
            
//            if (self.deptDict == nil || (contacts == nil ||  contacts.length == 0 ))
//            {
//                contacts=phoneString;
//            }
//            else
//            {
//                contacts=[self.deptDict valueForKey:@"SERVICE_PHONE_NUMBER"];
//            }
            
            
            if ([self.comingFrom isEqualToString:@"PhoneSupport"])
            {
                
                if (contacts.length == 0)
                {
                    //report_issue
                    emailString = singleton.emailSupport;
                    phoneString = singleton.phoneSupport;
                    
                    [self.bottomButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"contact_umang_support", nil)] forState:UIControlStateNormal];
                    
                    self.errorMessageLabel.text = [NSString stringWithFormat:NSLocalizedString(@"phone_support_not_available", nil),[[self.chooseDeptButton titleForState:UIControlStateNormal] uppercaseString]];
                    
                    self.errorView.hidden = NO;
                    
                }
                else
                {
                    self.errorView.hidden = YES;
                }
            }
            else
            {
                if (emailString.length == 0)
                {
                    //report_issue
                    emailString = singleton.emailSupport;
                    phoneString = singleton.phoneSupport;
                    
                    [self.bottomButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"contact_umang_support", nil)] forState:UIControlStateNormal];
                    
                    self.errorMessageLabel.text = [NSString stringWithFormat:NSLocalizedString(@"email_support_not_available", nil),[[self.chooseDeptButton titleForState:UIControlStateNormal] uppercaseString]];
                    
                    self.errorView.hidden = NO;
                    
                }
                else
                {
                    self.errorView.hidden = YES;
                }
            }
            
            
            
            
            self.bottomButton.hidden = NO;
        }
        
        
        [self updateViewLayout];
        
    }
    else
    {
        [self.bottomButton setTitle:NSLocalizedString(@"contact_umang_support", nil) forState:UIControlStateNormal];
        emailString = singleton.emailSupport;
        phoneString = singleton.phoneSupport;
        
        self.bottomButton.hidden = NO;
        self.errorView.hidden = YES;
        self.deptView.hidden = YES;
        
        isUmangSupportSelected = YES;
       // self.deptDict = nil;
    }
    
    
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EmailCell *cell = (EmailCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView = [[UIView alloc] initWithFrame:CGRectZero];
    
    cell.mainLabel.textColor = [UIColor colorWithRed:117.0/255.0f green:117.0/255.0f blue:117.0/255.0f alpha:117.0/255.0f];
    
}
@end

#pragma mark - Email Cell Interface and Implementation

@interface EmailCell()
@end

@implementation EmailCell

@end


