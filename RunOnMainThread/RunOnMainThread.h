//
//  RunOnMainThread.h
//  HiCast
//
//  Created by Sukhpreet Singh on 01/04/16.
//  Copyright © 2015 Seasia. All rights reserved.

#import <Foundation/Foundation.h>

@interface RunOnMainThread : NSObject

/**
  *  Create Block to check the Thread.
  **/
+ (void)runBlockInMainQueueIfNecessary:(void (^)(void))block;
+ (void)runBlockInGlobalQueueIfNecessary:(void (^)(void))block ;
    + (void)runBlockInBackgroundQueueIfNecessary:(void (^)(void))block ;

@end
