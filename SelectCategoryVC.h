//
//  SelectCategoryVC.h
//  Umang
//
//  Created by admin on 19/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCategoryVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblSelectCategory;
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;

@property(nonatomic,strong)NSMutableArray *arrTableContent;
@property(nonatomic,strong)NSMutableArray *arrPreselectedItem;

@property(nonatomic,assign)BOOL isForCategory;

@property(nonatomic,weak)id callBack;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

@end
