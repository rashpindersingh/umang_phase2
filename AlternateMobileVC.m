//
//  AlternateMobileVC.m
//  Umang
//
//  Created by deepak singh rawat on 05/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "AlternateMobileVC.h"

#import "UMAPIManager.h"

#import "MBProgressHUD.h"

#define MAX_LENGTH 10
#define kOFFSET_FOR_KEYBOARD 60.0
#import "VerifyAlternateMbVC.h"



@interface AlternateMobileVC ()<UITextFieldDelegate>
{
    MBProgressHUD *hud;
    NSString *man;
    NSString *rtry;
    NSString *tmsg;
    int tout;
    NSString *wmsg;
    CGRect origFrame;
}

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_msg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_submsg;
@property (weak, nonatomic) IBOutlet UITextField *txt_mobileNo;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) IBOutlet UILabel *lbl_mobPrefix;






- (IBAction)btn_nextAction:(id)sender;


@end

@implementation AlternateMobileVC
@synthesize TYPE_ALMNO_CHOOSE;
@synthesize tagtopass;
@synthesize titletopass;
- (void)viewDidLoad
{
    
   [super viewDidLoad];

    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:ALTERNATE_MOBILE_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    [_btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(_btnBack.frame.origin.x, _btnBack.frame.origin.y, _btnBack.frame.size.width, _btnBack.frame.size.height);
        
        [_btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    singleton = [SharedManager sharedSingleton];
    
    _lbl_title.text=titletopass;
    
    
    if (TYPE_ALMNO_CHOOSE == ISFROMPROFILEUPDATE)
    {
        tagtopass=@"ISFROMPROFILEUPDATE";
    }
    if (TYPE_ALMNO_CHOOSE == ISFROMREGISTRATION)
        
    {
        tagtopass=@"ISFROMREGISTRATION";
        
    }
    
    
    else if (TYPE_ALMNO_CHOOSE == ISFROMSETTINGS)
    {
        tagtopass=@"ISFROMSETTINGS";
        
    }
    
    
    _txt_mobileNo.delegate = self;
    self.btn_next.enabled=NO;
    self.view.userInteractionEnabled = YES;
    
    [_txt_mobileNo addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.vwTextBG.layer.borderColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0].CGColor;
    self.vwTextBG.layer.borderWidth = 2.0;
    self.vwTextBG.layer.cornerRadius = 4.0;
   _lbl_title.text = NSLocalizedString(@"add_alt_mob_num", nil);
    _lbl_title_submsg.text = NSLocalizedString(@"otp_on_this_number", nil);
    _lbl_title_msg.text = NSLocalizedString(@"enter_mobile_num", nil);
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    origFrame = scrollView.frame;
    

    //------ Add dismiss keyboard while background touch-------
  /*  UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [scrollView addGestureRecognizer:tapGesture];
    */
       // Do any additional setup after loading the view.
    [self addNavigationView];
}



    


//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    UITouch * touch = [touches anyObject];
//    if(touch.phase == UITouchPhaseBegan) {
//        [self.txt_mobileNo resignFirstResponder];
//    }
//}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


-(void)hideKeyboard
{

    [self.view endEditing:YES];
    
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.text.length >= MAX_LENGTH)
    {
        textField.text = [textField.text substringToIndex:MAX_LENGTH];
        
        self.btn_next.enabled=YES;
        [self enableBtnNext:YES];
        
        // NSLog(@"got it");
    }
    else
    {
        self.btn_next.enabled=NO;
        [self enableBtnNext:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
*/
/*
 - (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
 {
 NSString* fullString = [textField.text stringByReplacingCharactersInRange:range withString:string];
 NSLog(@"textField.text.length =%lu",textField.text.length );
 NSLog(@"fullString =%@",fullString );
 if (fullString.length >= MAX_LENGTH)
 {
 _txt_mobileNo.text = [fullString substringToIndex:MAX_LENGTH];
 self.btn_next.enabled=YES;
 [self enableBtnNext:YES];
 return NO; // return NO to not change text
 }
 else
 {
 self.btn_next.enabled=NO;
 [self enableBtnNext:NO];
 }
 return YES;
 }
 
 
 */
-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        
        [self hideKeyboard];
        
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}
- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
    NSString *phoneRegex =@"[6789][0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    

    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;

   /* dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollView.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height;
                       scrollView.contentSize = contentRect.size;
                   });*/

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    [super viewWillAppear:NO];
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    _btnBack.hidden = true;
    _lbl_title.hidden = true;
    //btnHelp.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton  = ^(id btnReset) {
        [weakSelf backbtnAction:btnReset];
    };
    nvView.lblTitle.text = _lbl_title.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX())
    {
        //        CGRect tbFrame = .frame;
        //        tbFrame.origin.y = kiPhoneXNaviHeight
        //        tbFrame.size.height = fDeviceHeight - kiPhoneXNaviHeight;
        //        tblProfileEdit.frame = tbFrame;
        //        [self.view layoutIfNeeded];
    }
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [_btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
    _lbl_title.font = [AppFont semiBoldFont:23.0];
    _lbl_title_msg.font = [AppFont semiBoldFont:16.0];
    _lbl_title_submsg.font = [AppFont mediumFont:12.0];
    self.lbl_mobPrefix.font = [AppFont mediumFont:22];
    _txt_mobileNo.font = [AppFont mediumFont:22];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    [self registerForKeyboardNotifications];

    [_txt_mobileNo becomeFirstResponder];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(IBAction)backbtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    if ([tagtopass isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    if ([tagtopass isEqualToString:@"ISFROMSETTINGS"]) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    if ([tagtopass isEqualToString:@"ISFROMREGISTRATION"]) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    if ([tagtopass isEqualToString:@"UPDATEFROMSECURITY_MPINBOX"]) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
}

- (IBAction)btn_nextAction:(id)sender
{
    //jump to next step here
    if ([self validatePhone:_txt_mobileNo.text]!=TRUE) {
        
        NSLog(@"Wrong Mobile");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:NSLocalizedString(@"enter_correct_phone_number", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [self hitAPI];
        
        
        // [self openNextView];
    }
}

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:_txt_mobileNo.text forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"sms" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile contact //not supported iphone
    [dictBody setObject:@"altmno" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin //valtmno
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //Type for which OTP to be intiate eg register,login,forgot mpin //valtmno
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);    
    //singleton.mobileNumber=_txt_mobileNo.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LOGIN_WITH_OTP withBody:dictBody andTag:TAG_REQUEST_LOGIN_WITH_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //singleton.altermobileNumber=_txt_mobileNo.text;
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            man=[[response valueForKey:@"pd"] valueForKey:@"man"];
            rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
            tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
            tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
            wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
            
//            NSString *rc=[response valueForKey:@"rc"];
//            NSString *rd=[response valueForKey:@"rd"];
//            NSString *rs=[response valueForKey:@"rs"];
            
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            


            
            //------ Sharding Logic parsing---------------
            

            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                [self openNextView];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            _txt_mobileNo.text = @"";
            [self enableBtnNext:NO];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}



-(void)openNextView
{
    
    
    //--------------------------
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VerifyAlternateMbVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyAlternateMbVC"];
    vc.TAGFROM=tagtopass;
    vc.tout=tout;
    vc.rtry=rtry;
    vc.altmobile=_txt_mobileNo.text;
    
    
    
    //--------------------------
    
    
    
    
    
    
    
    vc.lblScreenTitleName.text = @"Verify Alternate Mobile Number";
    
    if ([tagtopass isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    if ([tagtopass isEqualToString:@"ISFROMSETTINGS"]) {
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    if ([tagtopass isEqualToString:@"UPDATEFROMSECURITY_MPINBOX"]) {
        __block typeof(self) weakSelf = self;
        vc.didShowHomeVC = ^{
            [weakSelf dismissViewControllerAnimated:false completion:nil];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    //UPDATEFROMSECURITY_MPINBOX
    
    if ([tagtopass isEqualToString:@"ISFROMREGISTRATION"]) {
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:vc animated:YES completion:nil];
    }
    
    
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+100)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];
    [self setFontforView:self.view andSubViews:YES];
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
   /* UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;*/
    
    [scrollView setContentSize:CGSizeMake(origFrame.size.width, origFrame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];


}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(origFrame.size.width, origFrame.size.height+100)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }

  /*  NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
   UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+40, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;

    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.txt_mobileNo.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.txt_mobileNo.frame.origin.y-kbSize.height);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }*/
}
@end
