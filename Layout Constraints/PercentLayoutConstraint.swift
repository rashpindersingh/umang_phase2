//
//  PercentLayoutConstraint.swift
//  UmangSwift
//
//  Created by Rashpinder on 13/12/17.
//  Copyright © 2017 Umang. All rights reserved.
//

import UIKit

class PercentLayoutConstraint: NSLayoutConstraint {
    @IBInspectable var marginPercent: CGFloat = 0
    
    var screenSize: (width: CGFloat, height: CGFloat) {
        return (UIScreen.main.bounds.width, UIScreen.main.bounds.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        guard marginPercent > 0 else { return }
    
        self.layoutDidChange()
    }
    
    /**
     Re-calculate constant based on orientation and percentage.
     */
    @objc func layoutDidChange() {
        guard marginPercent > 0 else { return }
        switch firstAttribute {
        case .height :
            constant = screenSize.height * marginPercent
        case .width:
            if self.identifier == ConstraintConstant.loginTable  {
                constant = screenSize.width
            }
            break
        case .bottom, .bottomMargin :
            break
        case .top, .topMargin:
            constant = screenSize.height * marginPercent
       case .leading, .leadingMargin, .trailing, .trailingMargin:
            constant = screenSize.width * marginPercent
       
        default: break
        }
        
    }
    
    @objc func layoutDidChange_iPad() {
        guard marginPercent > 0 else { return }
        switch firstAttribute {
        case .height :
            constant = screenSize.height * marginPercent
        case .width:
            if self.identifier == ConstraintConstant.loginTable  {
                constant = 500.0
            }
            break
        case .bottom, .bottomMargin :
            break
        case .top, .topMargin:
            constant = screenSize.height * marginPercent
        case .leading, .leadingMargin, .trailing, .trailingMargin:
            constant = screenSize.width * marginPercent
            
        default: break
        }
    }
    deinit {
        guard marginPercent > 0 else { return }
        NotificationCenter.default.removeObserver(self)
    }
}
