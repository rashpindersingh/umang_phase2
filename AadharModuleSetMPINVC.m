//
//  AadharModuleSetMPINVC.m
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import "AadharModuleSetMPINVC.h"
#import "UIView+Toast.h"


#define kOFFSET_FOR_KEYBOARD 80.0

#import "MyTextField.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "AadharRegModuleVC.h"
#import "ProfileAfterAdharVC.h"
#import "UpdateQuestionsViewController.h"

#define MAX_LENGTH  4

@interface AadharModuleSetMPINVC ()<UITextFieldDelegate>

{
    __weak IBOutlet UILabel *lblTips;
    __weak IBOutlet UIButton *btnBack;
    NSString *mpinStr;
    NSString *CmpinStr;
    MBProgressHUD *hud ;
}


@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleMsg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_enterMpin;
@property (weak, nonatomic) IBOutlet UILabel *lbl_cMpin;


@property (weak, nonatomic) IBOutlet UITextField *txt_mpin;
@property (weak, nonatomic) IBOutlet UITextField *txt_Cpin;

@property (weak, nonatomic) IBOutlet UILabel *lbl_errorMsg;
@property (weak, nonatomic) IBOutlet UIImageView *img_error;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet UILabel *vw_line1;
@property (weak, nonatomic) IBOutlet UILabel *vw_line2;

@end

@implementation AadharModuleSetMPINVC
@synthesize txt_mpin,txt_Cpin,vw_line1,vw_line2;

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
- (id)initWithNibName:(NSString* )nibNameOrNil bundle:(NSBundle* )nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        self =[super initWithNibName:@"AadharModuleSetMPINVC_iPad" bundle:nil];
    }
    return self;
}


-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
    _lbl_title.font = [AppFont semiBoldFont:23.0];
    _lbl_titleMsg.font = [AppFont semiBoldFont:16.0];
    _lbl_enterMpin.font = [AppFont mediumFont:16.0];
    _lbl_cMpin.font = [AppFont mediumFont:16.0];
    lblTips.font = [AppFont mediumFont:16.0];
    _lbl_errorMsg.font = [AppFont regularFont:14.0];
    txt_mpin.font = [AppFont regularFont:21];
    txt_Cpin.font = [AppFont regularFont:21];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    // [self setFontforView:self.view andSubViews:YES];
    [self setViewFont];
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
    [txt_mpin becomeFirstResponder];
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        //or whatever font you're using
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    
    // NSString *str = NSLocalizedString(@"tip", nil);
    lblTips.text = NSLocalizedString(@"set_mpin_sub_heading", nil);
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:AADHAR_REGISTRATION_SET_MPIN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    singleton = [SharedManager sharedSingleton];
    
    [self textfieldInit];
    
    self.lbl_title.text = NSLocalizedString(@"setmpin_label", nil);
    self.lbl_titleMsg.text = NSLocalizedString(@"set_mpin_heading", nil);
    self.lbl_enterMpin.text = NSLocalizedString(@"enter_mpin_verify", nil);
    self.lbl_cMpin.text = NSLocalizedString(@"confirm_mpin", nil);
    self.lbl_errorMsg.text = NSLocalizedString(@"mpin_donot_match_txt", nil);
    
    //----- Setting delegate for Custom textfield so back space operation work smooth
    
    [self textfieldInit];
    self.lbl_errorMsg.hidden=TRUE;
    self.img_error.hidden=TRUE;
    [self enableBtnNext:NO];
    self.btn_next.enabled=NO;
    
    
    
    self.view.userInteractionEnabled = YES;
    
    
    
    // Do any additional setup after loading the view.
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    //--------- Code for handling -------------------
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollView.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height+100;
                       scrollView.contentSize = contentRect.size;
                   });
    
}


-(void)hideKeyboard
{
    [scrollView setContentOffset:
     CGPointMake(0, -scrollView.contentInset.top) animated:YES];
    
    [txt_Cpin resignFirstResponder];
    [txt_mpin resignFirstResponder];
    
    
}


- (IBAction)btnBackClicked:(id)sender {
    
    UIViewController *vc = self.presentingViewController;
    while (vc.presentingViewController) {
        vc = vc.presentingViewController;
        
        if ([vc isKindOfClass:[AadharRegModuleVC class]])
        {
            [vc dismissViewControllerAnimated:YES completion:NULL];
            
            
        }
        
    }
    
    
    
}

-(void)textfieldInit
{
    txt_mpin.delegate=self;
    txt_Cpin.delegate=self;
    //----close dsrawat4u
    // [txt_mpin addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //[txt_Cpin addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //----end close dsrawat4u
    
    
    
    
}

//---Code for hangle jump from one textfield to another while filling values
- (void)textFieldDidChange:(UITextField *)textField
{
    self.lbl_errorMsg.hidden=TRUE;
    self.img_error.hidden=TRUE;
    
    [self enableBtnNext:NO];
    self.btn_next.enabled=NO;
    
    
    if (textField.text.length >= MAX_LENGTH)
    {
        
        if ([textField isEqual:self.txt_mpin]) {
            textField.text = [textField.text substringToIndex:MAX_LENGTH];
            
            [self.txt_Cpin becomeFirstResponder];
            [self checkValidation];
            
        }
        else if([textField isEqual:self.txt_Cpin]) {
            textField.text = [textField.text substringToIndex:MAX_LENGTH];
            
            [textField resignFirstResponder];
            [self checkValidation];
        }
        
    }
    /// ---------add dsrawat4u-----
    
    else
    {
        if (textField.text.length == 0)
        {
            
            if ([textField isEqual:self.txt_Cpin])
            {
                textField.text = [textField.text substringToIndex:0];
                
                [txt_mpin becomeFirstResponder];
            }
            if ([textField isEqual:self.txt_mpin])
            {
                textField.text = [textField.text substringToIndex:0];
                
                [textField resignFirstResponder];
            }
            
            
        }
        
    }
    
    
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //Replace the string manually in the textbox
    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //perform any logic here now that you are sure the textbox text has changed
    [self textFieldDidChange:textField];
    return NO; //this make iOS not to perform any action
}


/// ---------end dsrawat4u-----

/*
 -(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
 self.lbl_errorMsg.hidden=TRUE;
 self.img_error.hidden=TRUE;
 [self enableBtnNext:NO];
 self.btn_next.enabled=NO;
 textField.text  =   string;
 if ([string length] > 0) {
 if ([textField isEqual:self.txt_mpin1]) {
 [self.txt_mpin2 becomeFirstResponder];
 }else if([textField isEqual:self.txt_mpin2]) {
 [self.txt_mpin3 becomeFirstResponder];
 }else if([textField isEqual:self.txt_mpin3]) {
 [self.txt_mpin4 becomeFirstResponder];
 }else if([textField isEqual:self.txt_mpin4]) {
 
 [self.txt_Cmpin1 becomeFirstResponder];
 
 }
 else if([textField isEqual:self.txt_Cmpin1]) {
 
 [self.txt_Cmpin2 becomeFirstResponder];
 
 }
 else if([textField isEqual:self.txt_Cmpin2]) {
 
 [self.txt_Cmpin3 becomeFirstResponder];
 
 }
 else if([textField isEqual:self.txt_Cmpin3]) {
 
 [self.txt_Cmpin4 becomeFirstResponder];
 
 }
 
 else if([textField isEqual:self.txt_Cmpin4]) {
 
 [textField resignFirstResponder];
 [self checkValidation];
 }
 
 }
 
 
 else if ([string length] == 0) {
 if ([textField isEqual:self.txt_mpin4]) {
 [self.txt_mpin3 becomeFirstResponder];
 
 }else if([textField isEqual:self.txt_mpin3]) {
 
 [self.txt_mpin2 becomeFirstResponder];
 
 }else if([textField isEqual:self.txt_mpin2]) {
 
 [self.txt_mpin1 becomeFirstResponder];
 
 }else if([textField isEqual:self.txt_mpin1]) {
 
 //[self.txt_Cmpin1 becomeFirstResponder];
 
 }
 
 
 else if([textField isEqual:self.txt_Cmpin4]) {
 
 [self.txt_Cmpin3 becomeFirstResponder];
 
 }
 else if([textField isEqual:self.txt_Cmpin3]) {
 
 [self.txt_Cmpin2 becomeFirstResponder];
 
 }
 else if([textField isEqual:self.txt_Cmpin2]) {
 
 [self.txt_Cmpin1 becomeFirstResponder];
 
 }
 
 else if([textField isEqual:self.txt_Cmpin1]) {
 
 [self.txt_mpin4 becomeFirstResponder];
 }
 
 }
 
 return FALSE;
 }
 
 */


-(void)checkValidation

{
    self.lbl_errorMsg.hidden=TRUE;
    self.img_error.hidden=TRUE;
    
    CmpinStr=[NSString stringWithFormat:@"%@",self.txt_Cpin.text];
    mpinStr=[NSString stringWithFormat:@"%@",self.txt_mpin.text];
    
    if (CmpinStr.length == 4 && mpinStr.length == 4 )
    {
        
        self.btn_next.enabled=YES;
        [self enableBtnNext:YES];
    }
    else
    {
        self.btn_next.enabled=NO;
        [self enableBtnNext: NO];
    }
    
}




-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        [self hideKeyboard];
        
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

-(IBAction)jumptoNextView:(id)sender
{
    if (mpinStr.length < 4)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_mpin_to_verify", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
    }
    
    
    
    if (CmpinStr.length < 4)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"confirm_mpin", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
    }
    if (![CmpinStr isEqualToString:mpinStr])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil)message:NSLocalizedString(@"mpin_donot_match_txt", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];    }
    
    
    
    
    else
    {
        
        
        //-------Added later---------------
        //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kKeepMeLoggedIn];
        [[NSUserDefaults standardUserDefaults] setInteger:kDashboardScreenCase forKey:kInitiateScreenKey];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        //-------Added later---------------
        
        
        UpdateQuestionsViewController *updateQuesView;
        if ([[UIScreen mainScreen]bounds].size.height == 1024)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DetailService" bundle:nil];
            
            updateQuesView = [storyboard instantiateViewControllerWithIdentifier:@"UpdateQuestionController"];
            //profileEdit.tagFrom=@"ISFROMREGISTRATION";
            
            updateQuesView.tagComingFrom = @"Set MPIN Adhaar";
            updateQuesView.mpinStr       = mpinStr;
            
            [updateQuesView setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:updateQuesView animated:NO completion:nil];
            
        }
        
        else
            
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DetailService" bundle:nil];

            updateQuesView = [storyboard instantiateViewControllerWithIdentifier:@"UpdateQuestionController"];
            //profileEdit.tagFrom=@"ISFROMREGISTRATION";
            
            updateQuesView.tagComingFrom = @"Set MPIN Adhaar";
            updateQuesView.mpinStr       = mpinStr;
            
            [updateQuesView setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:updateQuesView animated:NO completion:nil];
        }
        
        //dasdasdas
        //[self hitAPI];
        
        
    }
    
    
    
    
}

//----- hitAPI for IVR OTP call Type registration ------

-(void)hitAPI
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",mpinStr,strSaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    
    
    
    //682647029382
    //    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
    //    NSString *mpinStrEncrypted=[mpinStr sha256HashFor:encrytSHA256];
    //
    singleton.user_mpin=mpinStr; //save value in local db
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"mno"];//Enter mobile number of user//singleton.mobileNumber closed
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];
    
    if (singleton.user_aadhar_number.length) {
        [dictBody setObject:singleton.user_aadhar_number forKey:@"aadhr"];
    }
    
    //Type for which OTP to be intiate eg register,login,forgot mpin
    NSLog(@"Dict body is :%@",dictBody);
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_SET_MPIN withBody:dictBody andTag:TAG_REQUEST_SET_MPIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                singleton.user_tkn=tkn;
                //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                //[[NSUserDefaults standardUserDefaults]synchronize];
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
                //------NR SET NEW REGISTRATION Start CASE--------------
                singleton.lastFetchDate=@"NR";
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.lastFetchDate withKey:@"lastFetchDate"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                
                
                
                
                // Parse user profile data
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                [self alertwithMsg:rd];
            }
            
        }
        else
        {
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}

-(void)alertwithMsg:(NSString*)msg
{
    
    //-------Added later---------------
    [[NSUserDefaults standardUserDefaults] setInteger:kDashboardScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //-------Added later---------------
    
    [self openNextView];
    
}


-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}


-(void)openNextView
{
    //869441
    NSString *errormsg=[NSString stringWithFormat:@"%@",NSLocalizedString(@"aadhaar_linked_popup_msg", nil)];
    [self showToast:errormsg];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileAfterAdharVC *vc = [sb instantiateViewControllerWithIdentifier:@"ProfileAfterAdharVC"];
    // vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //  [self presentViewController:vc animated:YES completion:NULL];
    
    
    // profileEdit.Edit_Profile_Choose =  IsFromAadharScreen;
    vc.tagFrom=@"ISFROMREGISTRATION";
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:NO completion:nil];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+150)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];
    
    [self setFontforView:self.view andSubViews:YES];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [self.txt_mpin resignFirstResponder];
            [self.txt_Cpin resignFirstResponder];
        }
    }
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
//#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
//- (NSUInteger)supportedInterfaceOrientations
//#else
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations
//#endif
//{
//    // Return a bitmask of supported orientations. If you need more,
//    // use bitwise or (see the commented return).
//    return UIInterfaceOrientationMaskPortrait;
//    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
//}
//
//- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
//    // Return the orientation you'd prefer - this is what it launches to. The
//    // user can still rotate. You don't have to implement this method, in which
//    // case it launches in the current orientation
//    return UIInterfaceOrientationPortrait;
//}
//

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
