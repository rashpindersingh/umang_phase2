//
//  FAQWebVC.h
//  Umang
//
//  Created by deepak singh rawat on 15/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQWebVC : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *webview;
    IBOutlet UILabel *lbltitle;
    IBOutlet UIButton *btnBack;
    
    IBOutlet UIButton * btn_save;
}


- (void)updateButtons;
@property(nonatomic,retain)NSString *urltoOpen;
@property(nonatomic,retain)NSString *titleOpen;
@property(nonatomic,retain)NSString *isfrom;
@property(nonatomic,retain)NSData *pdfData;
@property(nonatomic,retain)NSString *file_name;
@property(nonatomic,retain)NSString *file_type;
@property(nonatomic,retain)NSString *file_shareType;
@property(nonatomic,retain)NSString *localPathString;

-(IBAction)saveAction:(id)sender;
@end
