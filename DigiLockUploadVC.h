//
//  DigiLockUploadVC.h
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigiLockUploadVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) NSMutableArray *itemsArray;
@property (weak, nonatomic) IBOutlet UITableView *docTableView;
@property (weak, nonatomic) IBOutlet UIView *noRecordsView;
@property (weak, nonatomic) IBOutlet UIButton *btn_upload;
@property (weak, nonatomic) IBOutlet UILabel *noRecordsLabel;
@property (strong, nonatomic) IBOutlet UIImageView *noFileImageView;

@end


#pragma mark - Uploaded Doc Cell Interface
@interface UploadedDocCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *docNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *docDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *docSizeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *docImageView;

@end
