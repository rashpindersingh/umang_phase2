//
//  DigilockerMenuVC.m
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigilockerMenuVC.h"
#import "DigiLockSinghUpVC.h"
#import "DigiLockLoginVC.h"
#import "DigiLockCreateAccountVC.h"
#import "UMANG-Swift.h"
#import "RunOnMainThread/RunOnMainThread.h"


@interface DigilockerMenuVC ()
{
    SharedManager *singleton;
    
    NSString *digi_login;
    NSString *login_message;
    NSString *digi_create_account;
    NSString *create_account_message;
    
//    DigiLockerFileModel *fileDelegate;
//
//    NSString *isSelectDepartment;

}


@end

@implementation DigilockerMenuVC

-(void)viewWillAppear:(BOOL)animated
{
    //------------- Network View Handle------------
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont
{
    [btn_back.titleLabel setFont:[AppFont regularFont:17.0]];
    [btn_help.titleLabel setFont:[AppFont regularFont:17.0]];
    //[btn_login.titleLabel setFont:[AppFont mediumFont:21.0]];
    //[btn_createAcct.titleLabel setFont:[AppFont mediumFont:21.0]];
    
    [btn_login.titleLabel setFont:[AppFont mediumFont:18.0]];
    [btn_createAcct.titleLabel setFont:[AppFont mediumFont:18.0]];
    
    titleLabel.font = [AppFont semiBoldFont:16.0];
    digilockerDescLabel.font = [AppFont mediumFont:14.0];
    
}
- (void)viewDidLoad
{
    
    singleton=[SharedManager sharedSingleton];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[btn_login setTitle:NSLocalizedString(@"login_caps", nil) forState:UIControlStateNormal];
    //[btn_createAcct setTitle:NSLocalizedString(@"create_account", nil) forState:UIControlStateNormal];
    
    [btn_login setTitle:NSLocalizedString(@"Signin_digi", nil) forState:UIControlStateNormal];
    [btn_createAcct setTitle:NSLocalizedString(@"SignUp_digi", nil) forState:UIControlStateNormal];
    
    [btn_back setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [btn_help setTitle:@"" forState:UIControlStateNormal];
    
    btn_createAcct.layer.cornerRadius = 20;
    btn_createAcct.layer.borderWidth = 1;
    btn_createAcct.layer.borderColor = [UIColor colorWithRed:72.0f/255.0f green:161.0f/255.0f blue:92.0f/255.0f alpha:1.0].CGColor;
    
    
    digilockerDescLabel.text = NSLocalizedString(@"secure_cloudtxt_digi", nil);
    titleLabel.text = NSLocalizedString(@"digi_locker", nil);
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btn_back.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btn_back.frame.origin.x, btn_back.frame.origin.y, btn_back.frame.size.width, btn_back.frame.size.height);
        
        [btn_back setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btn_back.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    //[self addNavigationView];
    
    NSString * digien =[NSString stringWithFormat:@"%@",[singleton.arr_initResponse valueForKey:@"digien"]];
    
    NSArray* digienArray = [digien componentsSeparatedByString:@"|"];
    
    if (digienArray.count == 4)
    {
        digi_login = [digienArray objectAtIndex:0];
        login_message =[digienArray objectAtIndex:1];
        digi_create_account = [digienArray objectAtIndex:2];
        create_account_message =[digienArray objectAtIndex:3];
    }
    
    [self.navigationController setNavigationBarHidden:true];
    
}
#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btn_back.hidden = true;
    // self.title.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf backbtnAction:btnBack];
    };
    //nvView.lblTitle.text = NSLocalizedString(@"help_and_support", "");
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)backbtnAction:(id)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"Digilocker Menu" andValue:0];
        if (self.isSelectDepartment) {
            self.didDepartmentLogin(true);
            //[self.navigationController popViewControllerAnimated:false];
            return;
        }
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)helpAction:(id)sender
{
    
}
-(IBAction)loginAction:(id)sender
{
    [singleton traceEvents:@"Login" withAction:@"Clicked" withLabel:@"Digilocker Menu" andValue:0];
    
    [self hitGetAuthURLTokenApiWithCode:false];
    return;
    
//    if (digi_login.length > 0 && [[digi_login lowercaseString] isEqualToString:@"true"])
//    {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//
//        DigiLockLoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockLoginVC"];
//        vc.tagComingFrom  = @"login";
//        [self.navigationController pushViewController:vc animated:YES];
//    }
//    else
//    {
//        login_message = login_message.length > 0 ? login_message : @"";
//
//        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:login_message preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//
//        [alert addAction:defaultAction];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
    
}
-(IBAction)createAccountAction:(id)sender
{
    [singleton traceEvents:@"Create Account" withAction:@"Clicked" withLabel:@"Digilocker Menu" andValue:0];
    
    
    [self hitGetAuthURLTokenApiWithCode:true];
    return;
    create_account_message = create_account_message.length > 0 ? create_account_message : @"";
    
    if (digi_create_account.length > 0 && [[digi_create_account lowercaseString] isEqualToString:@"true"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        DigiLockLoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockLoginVC"];
        vc.tagComingFrom  = @"signup";
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:create_account_message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
    
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     
     DigiLockCreateAccountVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockCreateAccountVC"];
     [self.navigationController pushViewController:vc animated:YES];*/
}
-(void)pushToDigiLockerWebview:(NSString*)url {
    
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DigiLockLoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockLoginVC"];
    
    vc.tagComingFrom  = @"login";
    vc.authURL = url;
    vc.isSelectDepartment = self.isSelectDepartment;
    vc.fileDelegate  = self.fileDelegate;
    if (self.isSelectDepartment) {
        vc.didDepartmentLogin = ^{
            [vc dismissViewControllerAnimated:false completion:nil];
            [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                [self jumpToNextView];
              //  [self.navigationController popViewControllerAnimated:false];

            }];
        };
    }
    if (self.isSelectDepartment) {
        [self presentViewController:vc animated:true completion:nil];
    }else {
        [self.navigationController pushViewController:vc animated:YES];

    }
}
-(void)hitGetAuthURLTokenApiWithCode:(BOOL)isSignup
{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    //[dictBody setObject:timeInMS forKey:@"trkr"];
    // [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    // [dictBody setObject:@"Y" forKey:@"rc"];
    // [dictBody setObject:@"Y" forKey:@"mec"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    if (isSignup) {
        [dictBody setObject:@"signup" forKey:@"flow"];
        
    }
    
    NSLog(@"Dictionary is %@",dictBody);
    
    
    [objRequest hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_GETDIGI_AUTHURL withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_GETTOKEN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //NSString *rc=[response valueForKey:@"rc"];
            // NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                if ([[response objectForKey:@"rc"] isEqualToString:@"DGL0017"]) {
                    [self jumpToNextView];
                    return ;
                }
                NSString *redirectURL = [[response valueForKey:@"pd"] valueForKey:@"url"];
                
                [self pushToDigiLockerWebview:redirectURL];
                
                
                //                [[NSUserDefaults standardUserDefaults] setValue:accessToken forKey:@"AccessTokenDigi"];
                //                [[NSUserDefaults standardUserDefaults] setValue:refreshToken forKey:@"RefreshTokenDigi"];
                //
                //
                //                [[NSUserDefaults standardUserDefaults] synchronize];
                //
                //                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                //                                                                              message:NSLocalizedString(@"logged_in_success", nil) preferredStyle:UIAlertControllerStyleAlert];
                //
                //                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                //                                            {
                //                                                [self jumpToNextView];
                //
                //                                            }];
                //
                //
                //                [alert addAction:yesButton];
                //
                //                [self presentViewController:alert animated:YES completion:nil];
                
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else
        {
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}
-(void)jumpToNextView
{
    [singleton traceEvents:@"Open DigiLocker Home" withAction:@"Clicked" withLabel:@"DigiLocker Login" andValue:0];
     [[NSUserDefaults standardUserDefaults] setValue:@"y" forKey:@"AccessTokenDigi"];
    if (self.isSelectDepartment) {
        self.didDepartmentLogin(false);
       /// [self dismissViewControllerAnimated:true completion:nil];
        return;
    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDigiLockerStoryBoard bundle:nil];
    
    DigiLockerHomeVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockerHomeVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    DigiLockHomeVC *digiView = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockHomeVC"];
    //    digiView.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:digiView animated:YES];
    
}
@end
