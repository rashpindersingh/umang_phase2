//
//  HomeFilterResultsVC.h
//  Umang
//
//  Created by admin on 22/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterNoResultView.h"
@protocol filterChangeDelegate <NSObject>

- (void)filterChanged:(NSMutableArray*)parameters andCategoryDict:(NSMutableDictionary *)dict;

@end

@interface HomeFilterResultsVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
    
    IBOutlet UITableView *tbl_homeresult;
    IBOutlet UIView *vw_noServiceFound;
    FilterNoResultView *noResultsVW;
    IBOutlet UILabel *lbl_noservicefound;
}
-(IBAction)btn_filterAction:(id)sender;//for filterview show
@property(nonatomic,strong)NSMutableDictionary *dictFilterParams;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(nonatomic,assign) BOOL isFromHomeFilter;
@property(nonatomic,assign) BOOL isFromStateFilter;


@property (nonatomic, weak) id<filterChangeDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *filterBOArray;

//--------- Code for Left Menu Open-----------



@end
