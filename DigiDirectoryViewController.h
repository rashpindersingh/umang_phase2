//
//  DigiDirectoryViewController.h
//  Umang
//
//  Created by admin on 04/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigiDirectoryViewController : UIViewController

@property (nonatomic,copy) NSString *folderIdString;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UITableView *folderTableView;
@property (weak, nonatomic) IBOutlet UIView *noRecordsView;
@property (weak, nonatomic) IBOutlet UIButton *btn_upload;

@property (strong, nonatomic) IBOutlet UIImageView *noFileImageView;
@property (weak, nonatomic) IBOutlet UILabel *noRecordsLabel;

@property (nonatomic,copy) NSString *folderNameString;

@end


#pragma mark - Uploaded Doc Cell Interface
@interface FolderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *folderNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *folderDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *folderSizeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *folderImageView;

@end
