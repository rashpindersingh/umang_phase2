//
//  XMPPvCardTemp+UserInfo.h
//  PanX_iPod
//
//  Created by Sanjay Chauhan on 2/20/14.
//  Copyright (c) 2014 lokesh. All rights reserved.
//

#import "XMPPvCardTemp.h"

@interface XMPPvCardTemp (UserInfo)

- (NSArray *)getUserDetails;
@end
