//
//  XMPPMessageCoreDataObject.m
//  SimpleChat
//
//  Created by Sanjay Chauhan on 18/12/13.
//  Copyright (c) 2013 Sanjay Chauhan. All rights reserved.
//

#import "XMPPMessageCoreDataObject.h"
#import "XMPPMessageUserCoreDataObject.h"


@implementation XMPPMessageCoreDataObject

@dynamic actualData;
@dynamic body;
@dynamic messageReceipant;
@dynamic selfReplied;
@dynamic sendDate;
@dynamic thumbnail;
@dynamic type;
@dynamic whoOwns;

@end
