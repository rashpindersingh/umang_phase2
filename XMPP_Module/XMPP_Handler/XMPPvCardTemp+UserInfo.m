//
//  XMPPvCardTemp+UserInfo.m
//  PanX_iPod
//
//  Created by Sanjay Chauhan on 2/20/14.
//  Copyright (c) 2014 lokesh. All rights reserved.
//

#import "XMPPvCardTemp+UserInfo.h"

@implementation XMPPvCardTemp (UserInfo)

- (NSArray *)getUserDetails {
	NSMutableArray *result = [[NSMutableArray alloc]init];
	NSXMLElement *categories = [self elementForName:@"N"];
	NSLog(@"dict is %@",[categories attributesAsDictionary]);
	if (categories != nil) {
		NSArray *elems = [categories elementsForName:@"GIVEN"];
        NSLog(@"main array  is %@",elems);
        NSDictionary *userDict;
        NSString *str=[NSString stringWithFormat:@"%@",[elems objectAtIndex:0]];
        NSArray *array=[str componentsSeparatedByString:@"<Jid>"];
        NSString *Jid=[array objectAtIndex:1] ? [array objectAtIndex:1]:@"";
        NSRange ranfrom=[Jid rangeOfString:@"</Jid>"];
        userDict = @{ @"Jid" : [Jid substringToIndex:ranfrom.location]};
        //result = [@[userDict]mutableCopy];
        [result addObject:userDict];
        
      /*  NSDictionary *userNameDict;
        NSArray *userNameArr=[str componentsSeparatedByString:@"<Username>"];
        NSString *userName=[userNameArr objectAtIndex:1] ? [userNameArr objectAtIndex:1] : @"";
        NSRange nameRange=[userName rangeOfString:@"</Username>"];
        userNameDict = @{ @"Username" : [userName substringToIndex:nameRange.location]};
        //result = [@[userNameDict]mutableCopy];
        [result addObject:userNameDict];
       */
        
        NSArray *userIdArr=[str componentsSeparatedByString:@"<UserId>"];
        NSString *userId=[userIdArr objectAtIndex:1] ? [userIdArr objectAtIndex:1] : @"";
        NSRange userIdRange=[userId rangeOfString:@"</UserId>"];
        userDict = @{ @"UserId" : [userId substringToIndex:userIdRange.location]};
        //result = [@[userDict]mutableCopy];
        [result addObject:userDict];
        
        NSLog(@"result  is %@",result);
        NSLog(@"final thing sakinaka is %@",[Jid substringToIndex:ranfrom.location]);
        NSLog(@"dictionary of love  is %@",userDict);
        //NSMutableArray *array = [@[ @"1", @"2", @"3" ] mutableCopy];
		NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:[elems count]];
        
  
		for (NSXMLElement *elem in elems) {
            
            NSLog(@"xml is %@",[elem stringValue]);
            NSLog(@"name is %@",[elem name]);
			[arr addObject:elem];
		}
		NSArray *route=[NSArray arrayWithArray:elems];
        for (NSDictionary *dict in route) {
            NSLog(@"dict is %@",dict);
        }
		
	}
	
	return result;
}


@end
