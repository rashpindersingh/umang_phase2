//
//  XMPPMessageUserCoreDataObject.h
//  SimpleChat
//
//  Created by Sanjay Chauhan on 22/12/13.
//  Copyright (c) 2013 Sanjay Chauhan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class XMPPMessageCoreDataObject;

@interface XMPPMessageUserCoreDataObject : NSManagedObject

@property (nonatomic, retain) NSString * displayName;
@property (nonatomic, retain) NSString * jidStr;
@property (nonatomic, retain) NSSet *messages;
@end

@interface XMPPMessageUserCoreDataObject (CoreDataGeneratedAccessors)

- (void)addMessagesObject:(XMPPMessageCoreDataObject *)value;
- (void)removeMessagesObject:(XMPPMessageCoreDataObject *)value;
- (void)addMessages:(NSSet *)values;
- (void)removeMessages:(NSSet *)values;

@end
