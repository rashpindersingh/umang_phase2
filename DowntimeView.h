//
//  DowntimeView.h
//  UMANG
//
//  Created by Deepak Rawat on 03/01/19.
//  Copyright © 2019 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DowntimeView : UIView
{
    
}

@property (strong, nonatomic) IBOutlet UIImageView *logoImage;
@property (strong, nonatomic) IBOutlet UILabel *underConstLabel;
@property (strong, nonatomic) IBOutlet UILabel *backSoonLabel;
@property (strong, nonatomic) IBOutlet UIButton *retryBtn;



-(IBAction)btn_clickRetry:(id)sender;

@end

NS_ASSUME_NONNULL_END
