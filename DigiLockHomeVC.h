//
//  DigiLockHomeVC.h
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigiLockHomeVC : UIViewController
{
    IBOutlet UIImageView *digilocker;
    IBOutlet UIButton *btn_shareDocument;
    IBOutlet UIButton *btn_uploadDocument;
    IBOutlet UIButton *btn_linkdigilocker;
    
    
}
-(IBAction)backbtnAction:(id)sender;
-(IBAction)btn_shareDocument:(id)sender;
-(IBAction)btn_uploadDocument:(id)sender;
-(IBAction)btn_linkdigilocker:(id)sender;
@end
