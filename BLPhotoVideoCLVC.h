//
//  BLPhotoVideoCLVC.h
//  BlendPhotoAndVideo
//
//  Created by Hoang Tran on 12/24/14.
//  Copyright (c) 2014 ILandApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLPhotoVideoCLVC : UICollectionViewCell

@property (nonatomic, assign) IBOutlet UIImageView *imgView;
@property (nonatomic, assign) IBOutlet UIImageView *imgViewVideoPlaySign;
@property (strong, nonatomic) IBOutlet UIView *viewBorder;
@property (strong, nonatomic) IBOutlet UIImageView *imgChecked;

@property BOOL isChoosed;

@end
