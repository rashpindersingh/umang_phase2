//
//  AadharHelpLinkVC.m
//  Umang
//
//  Created by admin on 27/03/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "AadharHelpLinkVC.h"
#import "LinkTableCell.h"
#import "SocialHelpIconCell.h"
#import "HelpViewController.h"
#import "FAQWebVC.h"
#import "SubmitQueryVC.h"
#import "MBProgressHUD.h"
#import "UMAPIManager.h"
#import "AadharRegViaNotLinkModuleVC.h"

@interface AadharHelpLinkVC ()<UITableViewDataSource,UITableViewDelegate>
{
     SharedManager *singleton;
    NSMutableArray *arrAadharData;
    NSMutableArray *arrAadharHelpName;
    NSMutableArray *arrAadharHelpImage;
    NSMutableArray *arrLinkTitle;
    NSMutableArray *arrLinkTitleDescription;
    NSMutableArray *arrLinkImage;
    MBProgressHUD *hud;
    __weak IBOutlet UIButton *btnAadhar;
    __weak IBOutlet UILabel *lblHelp;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tblLinkAadharHelp;

@end

@implementation AadharHelpLinkVC 


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    lblHelp.text = NSLocalizedString(@"help", nil);
    arrAadharHelpName = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"help_faq", nil),NSLocalizedString(@"user_manual", nil), nil];
    
    arrAadharHelpImage = [[NSMutableArray alloc]initWithObjects:@"more_help_support",@"faq_Updated",@"_SocialHelp", nil];
    
    arrLinkTitle = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"re_synk", nil),NSLocalizedString(@"unlink_aadhaar", nil), nil];
      arrLinkTitleDescription = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"resync_aadhaar_help_txt", nil),NSLocalizedString(@"unlink_aadhaar_help_txt", nil), nil];
    arrLinkImage = [[NSMutableArray alloc]initWithObjects:@"re_sync_HelpLink.png",@"unlink_HelpLink.png", nil];
    
    
    [btnAadhar setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];


    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        // [btnBack sizeToFit];
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnAadhar.titleLabel.font.pointSize]}];
        
        //or whatever font you're using
        CGRect framebtn=CGRectMake(btnAadhar.frame.origin.x, btnAadhar.frame.origin.y, btnAadhar.frame.size.width, btnAadhar.frame.size.height);
        
        
        [btnAadhar setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnAadhar.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    
    singleton=[SharedManager sharedSingleton];
    [super viewDidLoad];
    _tblLinkAadharHelp.delegate = self;
    _tblLinkAadharHelp.dataSource = self;
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnAadhar.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHelp.font = [AppFont semiBoldFont:17.0];
    
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (IBAction)btnAadharClicked:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];

    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section

{
    
    if (section == 0) {
        return 0;
    }
    else
    {
    return  44;
    }
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    //check header height is valid
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,50)];
    headerView.backgroundColor=[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(15,5,300,15);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:0.298 green:0.337 blue:0.424 alpha:1.0];
    
    
    CGRect labelFrame =  label.frame;
    labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
    label.frame = labelFrame;
    
    label.font = [UIFont boldSystemFontOfSize:14.0];
    
    label.text = NSLocalizedString(@"more_help_lbl", nil);
    
    
    [headerView addSubview:label];
    
    return headerView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        static NSString *simpleTableIdentifier = @"LinkTableCell";
        
        LinkTableCell *linkCell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        linkCell.accessoryType = UITableViewCellAccessoryNone;
        if (linkCell == nil)
        {
            linkCell = [[LinkTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        linkCell.lblTitle.text = [arrLinkTitle objectAtIndex:indexPath.row];
        linkCell.lblTitleDescrition.text =  [arrLinkTitleDescription objectAtIndex:indexPath.row];
        linkCell.imgLinkCell.image = [UIImage imageNamed:[arrLinkImage objectAtIndex:indexPath.row]];
        linkCell.backgroundColor = [UIColor whiteColor];
        linkCell.lblTitle.font = [AppFont regularFont:14];
        linkCell.lblTitleDescrition.font = [AppFont regularFont:13.0];
        
        return linkCell;
    }
    
    else
    {
        
    static NSString *simpleTableIdentifier = @"HelpCell";
    
    SocialHelpIconCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (cell == nil)
    {
        cell = [[SocialHelpIconCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [arrAadharHelpName objectAtIndex:indexPath.row];
    cell.textLabel.font = [AppFont regularFont:17.0];
    cell.imageView.image = [UIImage imageNamed:[arrAadharHelpImage objectAtIndex:indexPath.row]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.backgroundColor = [UIColor whiteColor];
   

    return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 70.0;
    }
    else
    {
    return 50.0;
    }
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
         return 2;
    }
    else
    {
    return 3;
    }
    
}

-(void)resyncMethod
{
    NSLog(@"Inside resync Method");
    singleton = [SharedManager sharedSingleton];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    AadharRegViaNotLinkModuleVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharRegViaNotLinkModuleVC"];
    vc.str_aadhaar_Value=singleton.objUserProfile.objAadhar.aadhar_number;
    vc.tagFrom=@"RESYNCAADHAARCARD";
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0)
    {
        if (indexPath.row==0)
        {
         // [self refreshview];
            
        }
         if (indexPath.row==1)
         {
              //  [self resyncMethod];
         }
        
    }
   
    else
    {
    
    if (indexPath.row==0)//tab picker
    {
        
        [self myHelp_Action];
        
    }
    
    if (indexPath.row==1)//language picker
    {
        
        
        [self openFAQWebVC];
        
    }
    
    if (indexPath.row==2)
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
        if ([singleton.arr_initResponse  count]>0) {
            vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"usrman"];
            
        }
        
        vc.titleOpen= NSLocalizedString(@"user_manual", nil);
        // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self.navigationController pushViewController:vc animated:YES];
     }
        
  }
    
}

-(void)refreshview
{
    [self hitFetchProfileAPI];
    
    
    
}
-(void)getAadhaarData
{
    
    arrAadharData = [[NSMutableArray alloc]init];
    
    _tblLinkAadharHelp.dataSource = self;
    _tblLinkAadharHelp.delegate = self;
    
   // [self prepareTempDataForAadharCard];
    
   // _tblLinkAadharHelp.tableHeaderView = [self designAadharProfileView];
}

-(void)hitFetchProfileAPI
{
    singleton = [SharedManager sharedSingleton];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
    
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                
                
                NSLog(@"Inside refresh method");
                
                
                if (singleton.objUserProfile.objAadhar.aadhar_number.length)
                {
                    NSLog(@"data found");
                   [self getAadhaarData];
                    
                }
                else
                {
                    
                    //[self NotLinkedAadharVC];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    
                }
                singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                
                NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                
                if ([abbreviation length]==0) {
                    abbreviation=@"";
                }
                
                NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                emblemString = emblemString.length == 0 ? @"":emblemString;
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
                
                //-------- Add later----------
                singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                //-------- Add later----------
                
                
                if ([singleton.user_id length]==0) {
                    singleton.user_id=@"";
                }
                //------------------------- Encrypt Value------------------------
                // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                //[defaults setObject:singleton.user_id forKey:@"USER_ID"];
                
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                
                
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}





-(void)myHelp_Action
{
    NSLog(@"My Help Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)openFAQWebVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    if ([singleton.arr_initResponse  count]>0) {
        vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"faq"];
        
    }
    vc.titleOpen= NSLocalizedString(@"help_faq", nil);
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
