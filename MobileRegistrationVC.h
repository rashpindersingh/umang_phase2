//
//  MobileRegistrationVC.h
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MobileRegistrationVC : UIViewController<UIScrollViewDelegate>
{
    SharedManager*  singleton ;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *lbl_agreeterm;
    IBOutlet UIButton *btn_Eula;
    IBOutlet UIButton *btn_AcceptBox;


}
-(IBAction)btnAcceptBox:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topConstraintScrollView;
-(IBAction)btnOpenEula:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topConstraintScroll;

@property (weak, nonatomic) IBOutlet UIView *vwTextBG;
//----- added for passing rty and tout-----
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
//----- added for passing rty and tout-----
@property(weak,nonatomic)NSString *commingFrom;

@end
