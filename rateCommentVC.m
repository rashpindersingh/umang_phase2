//
//  rateCommentVC.m
//  Umang
//
//  Created by admin on 09/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "rateCommentVC.h"
#import "UIImageView+WebCache.h"
#import "AMRatingControl.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
//static const NSInteger kStarWidthAndHeight = 25;

#define MAX_LENGTH 1
#define kOFFSET_FOR_KEYBOARD 80.0

#import "UIView+Toast.h"



@interface rateCommentVC ()<UITextFieldDelegate>
{
    SharedManager *singleton;
    MBProgressHUD *hud ;
    int rate;
}

@end

@implementation rateCommentVC
@synthesize comment,ratedBy;
@synthesize dictionary;


-(IBAction)btnSubmitAction:(id)sender
{
    NSLog(@"ratedBy=%@",ratedBy);
    
    NSLog(@"rate=%d",rate);
    
    
    
    if ([ratedBy isEqualToString:@"0"]||[ratedBy length]==0||ratedBy==nil)
    {
        
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"alert", nil) message:@"Please select a rating first!" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alertview show];
        
        
        
    }
    /* else if ([txt_addcomment.text isEqualToString:@""]||[txt_addcomment.text length]==0||txt_addcomment.text==nil)
     {
     UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Enter Your Comment!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
     [alertview show];
     }*/
    else
    {
        
        [self hitRatingserviceAPI:ratedBy];
        
    }
    
}


-(void)hitRatingserviceAPI:(NSString*)rating
{
    
    NSString *service_id=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"SERVICE_ID"]];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:rating forKey:@"rating"];//Enter mobile number of user
    [dictBody setObject:service_id forKey:@"sid"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"st"]; //tkn number
    [dictBody setObject:txt_addcomment.text forKey:@"comt"]; //tkn number
    
    // [dictBody setObject:@"en" forKey:@"lcle"]; //tkn number
    
    
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_RATINGS withBody:dictBody andTag:TAG_REQUEST_API_RATINGS completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            // NSString *rd=[response valueForKey:@"rd"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                [self startNotifierRateReload];
                [self showToast:NSLocalizedString(@"service_rated_successful", nil)];
                [self btn_close_Action:self];
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}

-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}

- (void) startNotifierRateReload
{
    // All instances of TestClass will be notified
    self.ratingUpdate(NSLocalizedString(@"service_rated_successful", nil));
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADRATING" object:nil];
    
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (fDeviceHeight<=568) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
        
    }
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{    if (fDeviceHeight<=568) {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [txt_addcomment resignFirstResponder];
            
        }
    }
}



-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}


- (IBAction)btn_close_Action:(id)sender {
    
    
    [UIView animateWithDuration:0.4 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    self.ratingUpdate(@"0");
}



- (void)viewDidLoad
{
    
    singleton=[SharedManager sharedSingleton];
    
    
    NSLog(@"value _comment=%@",comment);
    NSLog(@"value _rating=%@",ratedBy);
    NSLog(@"value dictionary=%@",dictionary);
    
    lbl_serviceName.text=[dictionary valueForKey:@"SERVICE_NAME"];
    
    
    txt_addcomment.text=[NSString stringWithFormat:@"%@",comment];
    txt_addcomment.delegate=self;
    
    NSURL *url=[NSURL URLWithString:[dictionary objectForKey:@"SERVICE_IMAGE"]];
    [serviceImg sd_setImageWithURL:url
                  placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
    
    
    
    
    
    UIImage *dot, *star;
    dot = [UIImage imageNamed:@"star_border"];
    star = [UIImage imageNamed:@"star_filled"];
    //AMRatingControl *imagesRatingControl = [[AMRatingControl alloc] initWithLocation:CGPointMake(fDeviceWidth/2+5,fDeviceHeight/3+fDeviceHeight/16)emptyImage:dot solidImage:star andMaxRating:5];
    CGRect ratinFrame = CGRectMake(0,0,(5 * 25),25);
    ratinFrame.origin.x = ((vw_popup.frame.size.width  / 2) - 12.5) - (ratinFrame.size.width / 2) ;
    AMRatingControl *imagesRatingControl = [[AMRatingControl alloc] initWithLocation:CGPointMake(ratinFrame.origin.x,vw_popup.frame.size.height/2.5)emptyImage:dot solidImage:star andMaxRating:5];
    
    
    rate=[ratedBy intValue];
    
    [imagesRatingControl setRating:rate];
    
    
    
    NSString *imageSmily=[NSString stringWithFormat:@"%d_star",rate];
    img_smily.image=[UIImage imageNamed:imageSmily] ;
    
    
    imagesRatingControl.editingDidEndBlock = ^(NSUInteger rating)
    {
        NSLog(@"editingDidEndBlock %@",[NSString stringWithFormat:@"%lu_star", (unsigned long)rating]);
        ratedBy=[NSString stringWithFormat:@"%lu",(unsigned long)rating];
        
        
        if (rating <=5 && rating>0) {
            
            NSString* setimg=  [NSString stringWithFormat:@"%lu_star", (unsigned long)rating];
            // ratedBy=[NSString stringWithFormat:@"%lu",(unsigned long)rating];
            
            img_smily.image=[UIImage imageNamed:setimg] ;
        }
        else
        {
            img_smily.image=[UIImage imageNamed:@"1_star"] ;
            
        }
        
    };
    
    
    // Add the control(s) as a subview of your view
    [vw_popup addSubview:imagesRatingControl];
    
    
    
    
    
    [super viewDidLoad];
    
    txt_addcomment.placeholder = NSLocalizedString(@"feedback", nil);
    [btn_submit setTitle:NSLocalizedString(@"submit", nil) forState:UIControlStateNormal];
    
    
    
    self.view.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.3];
    
    [self addShadowToTheView:self.view];
    
    [self addShadowToTheView:vw_popup];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btn_submit.titleLabel setFont:[AppFont mediumFont:18.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lbl_serviceName.font = [AppFont mediumFont:14.0];
    txt_addcomment.font = [AppFont mediumFont:14.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

