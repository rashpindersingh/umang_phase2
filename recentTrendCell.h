//
//  recentTrendCell.h
//  Umang
//
//  Created by admin on 05/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface recentTrendCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UILabel *lbl_text;
@property(nonatomic,retain)IBOutlet UIImageView *img_icon;
@property(nonatomic,retain)IBOutlet UIImageView *img_right_icon;

@end
