//
//  BLSelectAlbumCell.h
//  BlendPhotoAndVideo
//
//  Created by Anhtd on 1/23/15.
//  Copyright (c) 2015 ILandApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLSelectAlbumCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbAlbumName;
@property (strong, nonatomic) IBOutlet UILabel *lbNumberOfAlbum;
@property (strong, nonatomic) IBOutlet UIImageView *imgAlbum;


@end
