//
//  LoginAppVC.m
//  Umang
//
//  Created by deepak singh rawat on 06/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "LoginAppVC.h"
#import "NewLanguageSelectVC.h"
#import "APRoundedButton.h"
#import "TabBarVC.h"
#import "ChooseRegistrationTypeVC.h"
#import "LoginWithOTPVC.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "ForgotMPINVC.h"
#import "SocialAuthentication.h"
// For social login
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>

#import "UserEditVC.h"
#import "EnterMobileOTPVC.h"

#import "UIImageView+WebCache.h"
#import "UMANG-Swift.h"

#import <LocalAuthentication/LocalAuthentication.h>

#import "MobileRegistrationVC.h"
#import "UpdMpinVC.h"

#import "CZPickerView.h"
#import "LanguageBO.h"
#import "NSBundle+Language.h"

#import "FAQWebVC.h"
#import "StateList.h"
#import "TabBarVC_Flag.h"
#import "UMANG-Swift.h"

#define kOFFSET_FOR_KEYBOARD 80.0
static int isGoogleLogin = 0;
static int isfacebookLogin = 0;

@interface LoginAppVC ()<UITextFieldDelegate,GIDSignInUIDelegate,UIActionSheetDelegate,CZPickerViewDelegate,CZPickerViewDataSource>
{
    
    
    MBProgressHUD *hud ;
    __weak IBOutlet UIButton *btnTwitter;
    
    NSString *val_mpin_aadhar;
    NSString *val_aadhar;
    BOOL isLoginAuthInProgress;
    
    NSString *val_mpin_mb;
    NSString *val_mb;
    SharedManager *singleton;
    
    //MobileRegistrationVC *mobileReg;
    MobileRegister_FlagVC *mobileReg;
    
    IBOutlet UIButton *changeLanguageButton;
    
    NSMutableArray *arrLanguageNames;
    LanguageBO *selectedLanguage;
    // BOOL isGoogleLogin;
    // BOOL isFacebookLogin;
    
    __weak IBOutlet UIView *under_View;
    UIView *menuView;
    
    
    BOOL mpinSelected;
    BOOL otpSelected;
}
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UITextField *txt_enterId;
@property (weak, nonatomic) IBOutlet UIImageView *img_id;

@property (weak, nonatomic) IBOutlet UIButton *btn_mobile;
@property (weak, nonatomic) IBOutlet UIButton *btn_Adhar;

- (IBAction)mobileAction:(id)sender;
- (IBAction)aadharAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIImageView *img_mpin;
@property ( nonatomic) BOOL  isLangChange;
@property (strong, nonatomic) IBOutlet UIButton *radioBtnMpin;
@property (strong, nonatomic) IBOutlet UIButton *radioBtnOTP;


@end


@implementation LoginAppVC
@synthesize btnLogin,btnNewUser;
@synthesize tout,rtry;

- (IBAction)menuButtonPressed:(UIButton *)sender
{
    [[SharedManager sharedSingleton] traceEvents:@"Menu Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];

    [self btnOpenSheet];
}
- (IBAction)backBtnAction:(UIButton *)sender
{
    if ([self.sender isEqualToString:@"information"] && self.isLangChange == false) {
        [self dismissViewControllerAnimated:false
                                 completion:nil];
    }
     else  if (self.isLangChange == false )
     {
        [self.navigationController popViewControllerAnimated:YES];
    }
     else if (self.isLangChange ) {
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        SharedManager.sharedSingleton.isLoginViewShow = @"NO";
        [delegate updateAppLanguage:@"NO"];
        return;
    }
    
    
    
    
    
    
}

- (void)btnOpenSheet

{
    NSString *information=NSLocalizedString(@"change_language", nil);
    NSString *viewMap=NSLocalizedString(@"help_faq", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [[SharedManager sharedSingleton] traceEvents:@"Change Language Option" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
            [self showLanguageOptions];
        }
            break;
        case 1:
        {
            
              [[SharedManager sharedSingleton] traceEvents:@"FAQ Option" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
            vc.isfrom    = @"WEBVIEWHANDLE";
            
            if ([singleton.arr_initResponse  count]>0) {
                vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"faq"];
                
            }
            vc.titleOpen= NSLocalizedString(@"help_faq", nil);
            
            [[self topMostController] presentViewController:vc animated:YES completion:nil];
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

#pragma mark - Login With OTP Handling

-(void)apiForLoginWithOTP
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:_txt_enterId.text forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"sms" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile contact //not supported iphone
    // [dictBody setObject:@"login" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    [dictBody setObject:@"loginmob" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    
    
    singleton.mobileNumber=_txt_enterId.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LOGIN_WITH_OTP withBody:dictBody andTag:TAG_REQUEST_LOGIN_WITH_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
            //  NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
            //  NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            
            
            
            //------ Sharding Logic parsing---------------
            
            
            tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
            rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
            
            
            
            
            
            //   NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rd=[response valueForKey:@"rd"];
            //  NSString *rs=[response valueForKey:@"rs"];
            
            
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                singleton.mobileNumber=_txt_enterId.text;
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                EnterMobileOTPVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"EnterMobileOTPVC"];
                vc.lblScreenTitleName.text = @"Mobile Number Verification";
                vc.TYPE_LOGIN_CHOOSEN = ISFROMLOGINWITHOTP;
                vc.tout=tout;
                vc.rtry=rtry;
                //vc.TYPE_RESEND_OTP_FROM=ISFROMLOGINWITHOTPVC;
                [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [self presentViewController:vc animated:YES completion:nil];
                
                
            }
            
            
        }
        else{
            if ([[response valueForKey:@"rc"] isEqualToString:@"API0096"] || ([[response valueForKey:@"rc"] isEqualToString:@"PRF"] && [[response valueForKey:@"rs"] isEqualToString:@"F"]))
            {
                UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil)message:error.localizedDescription delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"cancel", nil),NSLocalizedString(@"ok", nil), nil];
                    
                alertview.tag=2010;
                [alertview show];
                
                
            }else {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                message:error.localizedDescription
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil];
                [alert show];
            }
           
            
        }
        
    }];
    
}

#pragma mark - Radio Button Action

- (IBAction)radioBtnMPINClicked:(UIButton *)sender
{
    if (otpSelected)
    {
        [sender setImage:[UIImage imageNamed:@"icon_radio_button_select"] forState:UIControlStateNormal];
        mpinSelected = YES;
        self.txtMPin.userInteractionEnabled = YES;
    }
    /*else
    {
        [sender setImage:[UIImage imageNamed:@"icon_radio_button"] forState:UIControlStateNormal];
        mpinSelected = NO;
        self.txtMPin.userInteractionEnabled = NO;
    }*/
    
    
    if (otpSelected)
    {
        [self.radioBtnOTP setImage:[UIImage imageNamed:@"icon_radio_button"] forState:UIControlStateNormal];
        otpSelected = NO;
        [btnLogin setTitle:NSLocalizedString(@"login_caps", @"")  forState:UIControlStateNormal];
        self.txtMPin.userInteractionEnabled = YES;
    
    }
    
}
- (IBAction)radioBtnOTPClicked:(UIButton *)sender
{
    if (mpinSelected)
    {
        [sender setImage:[UIImage imageNamed:@"icon_radio_button_select"] forState:UIControlStateNormal];
        [btnLogin setTitle:NSLocalizedString(@"btn_continue", @"")  forState:UIControlStateNormal];
        otpSelected = YES;
        
        self.txtMPin.userInteractionEnabled = NO;
    }
    /*else
    {
        [sender setImage:[UIImage imageNamed:@"icon_radio_button"] forState:UIControlStateNormal];
        [btnLogin setTitle:NSLocalizedString(@"login_caps", @"")  forState:UIControlStateNormal];
        otpSelected = NO;
        
        self.txtMPin.userInteractionEnabled = YES;
    }*/
    
    if (mpinSelected)
    {
        [self.radioBtnMpin setImage:[UIImage imageNamed:@"icon_radio_button"] forState:UIControlStateNormal];
        self.txtMPin.userInteractionEnabled = NO;
        self.txtMPin.text = @"";
        mpinSelected = NO;
    }
    
    if (sender == nil)
    {
        [self.radioBtnOTP setImage:[UIImage imageNamed:@"icon_radio_button_select"] forState:UIControlStateNormal];
    }
}



#pragma mark - Language Action
- (IBAction)changeLanguageAction:(UIButton *)sender
{
    [self showLanguageOptions];
}

#pragma mark - FAQ Action
- (IBAction)faqButtonPressed:(UIButton *)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.isfrom    = @"WEBVIEWHANDLE";
    
    if ([singleton.arr_initResponse  count]>0) {
        vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"faq"];
        
    }
    vc.titleOpen= NSLocalizedString(@"help_faq", nil);
    
    [[self topMostController] presentViewController:vc animated:YES completion:nil];
}

#pragma mark- Language Picker

-(void)prepareTempDataForLanguageType
{
    
    arrLanguageNames = [NSMutableArray new];
    
    LanguageBO *objService = [[LanguageBO alloc] init];
    objService.languageName = @"English";
    objService.localeName = @"en";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"हिंदी";
    objService.localeName = @"hi-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"অসমীয়া";
    objService.localeName = @"as-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"বাংলা";
    objService.localeName = @"bn-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"ગુજરાતી";
    objService.localeName = @"gu-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"ಕನ್ನಡ";
    objService.localeName = @"kn-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"മലയാളം";
    objService.localeName = @"ml-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"मराठी";
    objService.localeName = @"mr-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"ଓଡ଼ିଆ";
    objService.localeName = @"or-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"ਪੰਜਾਬੀ";
    objService.localeName = @"pa-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"தமிழ்";
    objService.localeName = @"ta-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"తెలుగు";
    objService.localeName = @"te-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"اردو";
    objService.localeName = @"ur-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    
}

- (void)czpickerView:(CZPickerView *)pickerView
didConfirmWithItemAtRow:(NSInteger)row
{
    if (selectedLanguage != nil)
    {
        selectedLanguage.isLanguageSelected = NO;
    }
    
    selectedLanguage = arrLanguageNames [row];
      [[SharedManager sharedSingleton] traceEvents:@"Language Selected" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    
    NSString *changeString = [NSString stringWithFormat:@" [%@]",[NSLocalizedString(@"change", nil) uppercaseString]];
    
    NSAttributedString *lang = [[NSAttributedString alloc] initWithString:[singleton.languageSelected uppercaseString] attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14] , NSForegroundColorAttributeName: [UIColor whiteColor]}];
    NSAttributedString *change = [[NSAttributedString alloc] initWithString:changeString attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14],NSForegroundColorAttributeName: [UIColor whiteColor]}];
    NSMutableAttributedString *strLang = [[NSMutableAttributedString alloc] initWithAttributedString:lang];
    [strLang appendAttributedString:change];
    
    
    
    //NSString *combinedString = [NSString stringWithFormat:@"%@ [%@]",singleton.languageSelected,[NSLocalizedString(@"change", nil) uppercaseString]];
    
    //[changeLanguageButton setAttributedTitle:strLang forState:UIControlStateNormal];
    selectedLanguage.isLanguageSelected = YES;
    
    
    // Update Langugae Bundle
    [[NSUserDefaults standardUserDefaults] setObject:selectedLanguage.localeName forKey:KEY_PREFERED_LOCALE];
    [NSBundle setLanguage:selectedLanguage.localeName];
    
    [self checkLanguageSelected];
    
    //[self loadView];
    
    [self viewDidLoad];
    
   // [self hitInitAPI];
}

-(void)hitInitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    singleton = [SharedManager sharedSingleton];
    
    //hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    //hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
       // [hud hideAnimated:YES];
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            singleton.arr_initResponse=[[NSMutableDictionary alloc]init];
            singleton.arr_initResponse=[response valueForKey:@"pd"];
            NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
            
            [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
            
            
            NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
            NSString*  emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
            NSLog(@"value of abbr=%@",abbr);
            
            if ([abbr length]==0)
            {
                abbr=@"";
            }
            
            emblemString = emblemString.length == 0? @"": emblemString;
            
            [[NSUserDefaults standardUserDefaults] setObject:[abbr capitalizedString] forKey:@"ABBR_KEY"];
            NSString*  infoTab=[singleton.arr_initResponse valueForKey:@"infotab"];
[[NSUserDefaults standardUserDefaults] setObject:[infoTab capitalizedString] forKey:@"infotab"];
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //---clear data and last fetch date so Home API can load data
            
            //---clear data and last fetch date so Home API can load data
            
            //------------------------- Encrypt Value------------------------
            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
            // Encrypt
            [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
            
            NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSHTTPCookie *cookie;
            for (cookie in [storage cookies]) {
                
                [storage deleteCookie:cookie];
                
            }
            NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
            [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            //------------------------- Encrypt Value------------------------
            
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                // get the data here
                [singleton.dbManager deleteBannerHomeData];
                [singleton.dbManager  deleteAllServices];
                [singleton.dbManager  deleteSectionData];
                
                
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   //-------------check condition temp-----
                                   //                                   [[NSUserDefaults standardUserDefaults] setObject:singleton.tabSelected  forKey:@"SELECTED_TAB"];
                                   
                                   [[NSUserDefaults standardUserDefaults]synchronize];
                                   
                                   singleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
                                   
                                   
                                   
                                   //                                   NSLog(@"selected tab is: %@",selectedTab);
                                   //                                   NSLog(@"selected tab home_small: %@",NSLocalizedString(@"home_small", nil));
                                   //                                   NSLog(@"selected tab favourites_small: %@",NSLocalizedString(@"favourites_small", nil));
                                   //                                   NSLog(@"selected tab all_services_small: %@",NSLocalizedString(@"all_services_small", nil));
                                   //                                   if ([selectedTab isEqualToString:NSLocalizedString(@"home_small", nil)]) {
                                   //                                       singleton.tabSelected=NSLocalizedString(@"home_small", nil);
                                   //
                                   //                                   }
                                   //                                   else if ([selectedTab isEqualToString:NSLocalizedString(@"favourites_small", nil)]) {
                                   //                                       singleton.tabSelected=NSLocalizedString(@"favourites_small", nil);
                                   //
                                   //                                   }
                                   //                                   else if ([selectedTab isEqualToString:NSLocalizedString(@"all_services_small", nil)]) {
                                   //                                       singleton.tabSelected=NSLocalizedString(@"all_services_small", nil);
                                   //
                                   //
                                   //                                   }
                                   //                                   else{
                                   //                                       singleton.tabSelected=NSLocalizedString(@"home_small", nil);
                                   //
                                   //
                                   //                                   }
                                   //
                                   
                                   
                                   
                                   
                                   //[self performSelector:@selector(homeviewJump) withObject:nil afterDelay:0.2];
                                   
                               });
            });
            
            
            
            
            // jump to home view  tab
            
            /* facebooklink
             faq
             forceupdate
             googlepluslink
             opensource
             privacypolicy
             splashScreen
             tabordering
             termsandcondition
             twitterlink
             ver
             vermsg
             */
            
            //------ save value in nsuserdefault for relanch app
            // [[NSUserDefaults standardUserDefaults] setObject:response forKey:@"TOUR_Key"];
            //[[NSUserDefaults standardUserDefaults] synchronize];
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"network_error_txt",nil)  delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok",nil) , nil];
            alert.tag=989;
            [alert show];
            
            //NSString *toast=[NSString stringWithFormat:@"%@",error.localizedDescription];
            
            //[self showToast:toast];
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
        }
        
    }];
    
}

-(void)homeviewJump
{
    
    AppDelegate *appD = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [appD updateAppLanguage:@"NO"];
    
    
}

-(void)checkLanguageSelected
{
    singleton=[SharedManager sharedSingleton];
    
    singleton.languageSelected=selectedLanguage.localeName;
    
    
    
    // Check existing language if set
    NSString *existingLanguageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    
    if (existingLanguageSaved == nil)
    { // There is no language selected. Set English by default
        [NSBundle setLanguage:TXT_LANGUAGE_ENGLISH];
        [[NSUserDefaults standardUserDefaults] setObject:TXT_LANGUAGE_ENGLISH forKey:KEY_PREFERED_LOCALE];
        
        singleton.languageSelected=NSLocalizedString(@"english", nil);
        
    }
    else // Set already saved language
    {
        [NSBundle setLanguage:existingLanguageSaved];
        if ([existingLanguageSaved containsString:@"en"]) {
            
            singleton.languageSelected= NSLocalizedString(@"english", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"hi"])
        {
            
            singleton.languageSelected=NSLocalizedString(@"hindi", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"as"])
        {
            
            singleton.languageSelected=NSLocalizedString(@"assamese", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"bn"]) {
            
            singleton.languageSelected=NSLocalizedString(@"bengali", nil);
            
        }
        if ([existingLanguageSaved containsString:@"gu"]) {
            
            singleton.languageSelected=NSLocalizedString(@"gujarati", nil);
            
        }
        if ([existingLanguageSaved containsString:@"kn"]) {
            
            singleton.languageSelected=NSLocalizedString(@"kannada", nil);
            
        }
        if ([existingLanguageSaved containsString:@"ml"]) {
            
            singleton.languageSelected=NSLocalizedString(@"malayalam", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"mr"]) {
            
            singleton.languageSelected=NSLocalizedString(@"marathi", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"or"]) {
            
            singleton.languageSelected=NSLocalizedString(@"oriya", nil);
            
        }
        
        
        
        if ([existingLanguageSaved containsString:@"pa"]) {
            
            singleton.languageSelected=NSLocalizedString(@"punjabi", nil);
            
        }
        
        
        if ([existingLanguageSaved containsString:@"ta"]) {
            
            singleton.languageSelected=NSLocalizedString(@"tamil", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"te"]) {
            
            singleton.languageSelected=NSLocalizedString(@"telugu", nil);
            
        }
        
        
        if ([existingLanguageSaved containsString:@"ur"]) {
            
            singleton.languageSelected=NSLocalizedString(@"urdu", nil);
            
        }
        
        
        singleton.tabSelected=NSLocalizedString(@"home_small", nil);
        
    }
    
}

-(void)showLanguageOptions
{
    UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    NewLanguageSelectVC *langVC = [storyboard instantiateViewControllerWithIdentifier:@"NewLanguageSelectVC"];
    __weak __typeof(self) weakSelf = self;

    langVC.LanguageSelect = ^(LanguageModel *model) {
       // [weakSelf hitInitAPI];
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [weakSelf changeLangaugeAction];
        }];
    };
    langVC.sender = @"LoginAppVC";
    [langVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:langVC animated:true completion:nil];
    
    /*CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"choose_language", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"Cancel", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = NO;
    picker.allowRadioButtons = NO;
    picker.needFooterView = NO;
    picker.headerTitleColor = [UIColor colorWithRed:12.0/255.0 green:97.0/255.0 blue:161.0/255.0 alpha:1.0];
    picker.isClearOptionRequired = NO;
    picker.isLanguageScreenSelected = YES;
    [picker show];*/

}

-(void)changeLangaugeAction{
    
    //[delegate updateAppLanguage:@"NO"];
    // === Start Delete flag info detail while language change
    self.isLangChange = true;
    [singleton.dbManager deleteFlagBannerData];
    [singleton.dbManager deleteInfoFlagServicesData];
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFlagScreenTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row
{
    NSAttributedString *att;
    
    LanguageBO *objService = arrLanguageNames [row];
    att = [[NSAttributedString alloc]
           initWithString:NSLocalizedString(objService.languageName, nil)
           attributes:@{
                        NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                        }];
    
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row
{
    
    return arrLanguageNames[row];
    
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return arrLanguageNames.count;
}


- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView
{
      [[SharedManager sharedSingleton] traceEvents:@"Cancel Language Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    NSLog(@"Canceled.");
}

#pragma mark -
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField == _txtMPin) {
        [_radioBtnMpin setImage:[UIImage imageNamed:@"icon_radio_button_select"] forState:UIControlStateNormal];
        mpinSelected = YES;
        if (otpSelected)
        {
            [self.radioBtnOTP setImage:[UIImage imageNamed:@"icon_radio_button"] forState:UIControlStateNormal];
            otpSelected = NO;
            [btnLogin setTitle:NSLocalizedString(@"login_caps", @"")  forState:UIControlStateNormal];
            self.txtMPin.userInteractionEnabled = YES;
            
        }
    }
   
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+150)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}





- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568)
    {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [self hideKeyboard];
            
        }
    }
    
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}



- (IBAction)aadharAction:(id)sender
{
    
    [btnLogin setTitle:NSLocalizedString(@"btn_continue", @"") forState:UIControlStateNormal];
    [_txtAadhar setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.btn_Adhar setSelected:YES];
    [self.btn_mobile setSelected:NO];
    self.img_id.hidden=TRUE;
    self.img_mpin.hidden=TRUE;
    self.txt_enterId.hidden=TRUE;
    self.txtMPin.hidden=TRUE;
    self.btnLoginWithOTP.hidden=TRUE;
    lblPrefixMobileNum.hidden  = TRUE;
    self.btnForgotPin.hidden=TRUE;
    lbl_line1.hidden=TRUE;
    lbl_line2.hidden=TRUE;
    self.lbl_line3.hidden=false;
    //self.img_adhar.hidden=false;
    self.img_adhar.hidden = true;
    
    self.txtAadhar.hidden=false;
    self.img_adhar.image=[UIImage imageNamed:@"icon_newadhar"];
    self.txtAadhar.placeholder=[NSLocalizedString(@"aadhaar_no", nil) uppercaseString];
    flg_LgType=FALSE;
    [_txtMPin resignFirstResponder];
    [_txt_enterId resignFirstResponder];
    
    
    under_View.frame = CGRectMake(_btn_Adhar.frame.origin.x, _btn_Adhar.frame.origin.y + _btn_Adhar.frame.size.height, _btn_Adhar.frame.size.width, 3);
}



- (IBAction)mobileAction:(id)sender
{
     [[SharedManager sharedSingleton] traceEvents:@"Mobile Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    [btnLogin setTitle:NSLocalizedString(@"login_caps", @"") forState:UIControlStateNormal];
    [_txtAadhar setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    //    self.img_id.hidden=false;
    //    self.img_mpin.hidden=false;
    
    self.img_id.hidden   = true;
    self.img_mpin.hidden = true;
    
    self.txt_enterId.hidden=false;
    self.txtMPin.hidden=false;
    self.btnLoginWithOTP.hidden=false;
    lblPrefixMobileNum.hidden = false;
    self.btnForgotPin.hidden=false;
    
    lbl_line1.hidden=false;
    lbl_line2.hidden=false;
    
    
    self.lbl_line3.hidden=TRUE;
    self.img_adhar.hidden=TRUE;
    self.txtAadhar.hidden=TRUE;
    
    
    
    self.img_id.image=[UIImage imageNamed:@"mobile_no"];
    self.txt_enterId.text=val_mb;
    self.txtMPin.text=val_mpin_mb;
    
    [self.btn_Adhar setSelected:NO];
    [self.btn_mobile setSelected:YES];
    
    
    
    self.txt_enterId.placeholder=[NSLocalizedString(@"new_mob_num", nil) uppercaseString];
    
    flg_LgType=TRUE;
    
    [_txtMPin resignFirstResponder];
    [_txt_enterId resignFirstResponder];
    
    under_View.frame = CGRectMake(_btn_mobile.frame.origin.x, _btn_mobile.frame.origin.y + _btn_mobile.frame.size.height, _btn_mobile.frame.size.width, 3);
}



-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

// Since user is on login screen. means no social account should be logged in previously

-(void)logoutFromSocialAccounts
{
    // Logout from social frameworks as well.
    SocialAuthentication *objSocial = [[SocialAuthentication alloc] init];
    [objSocial logoutFromAllSocialFramewors];
    objSocial = nil;
    
}



/*-(void)viewWillDisappear:(BOOL)animated
 {
 
 // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FETCHHOMEDATA" object:nil];
 [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GOOGLE_LOGIN" object:nil];
 [super viewWillDisappear:YES];
 }
 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isLoginAuthInProgress = NO;
    self.isLangChange = false;
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"languageScreenShown"];
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: LOGIN_APP_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    singleton = [SharedManager sharedSingleton];
    [singleton setStateId:@""];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GOOGLE_LOGIN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(googleLoginCompleted:) name:@"GOOGLE_LOGIN" object:nil];
    
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]]];
    //self.view.layer.contents = (id)[UIImage imageNamed:@"login_bg"].CGImage;
    
    //    self.img_id.hidden=false;
    //    self.img_mpin.hidden=false;
    self.img_id.hidden   = true;
    self.img_mpin.hidden = true;
    
    self.txt_enterId.hidden=false;
    self.txtMPin.hidden=false;
    self.btnLoginWithOTP.hidden=false;
    self.btnForgotPin.hidden=false;
    lblPrefixMobileNum.hidden = false;
    lbl_line1.hidden=false;
    lbl_line2.hidden=false;
    self.lbl_line3.hidden=TRUE;
    self.img_adhar.hidden=TRUE;
    self.txtAadhar.hidden=TRUE;
    
    
    if ([[UIScreen mainScreen]bounds].size.height >= 1024)
    {
        _btn_mobile.titleLabel.font = [UIFont systemFontOfSize:24.0];
        _btn_Adhar.titleLabel.font = [UIFont systemFontOfSize:24.0];
        _btn_Adhar.titleLabel.font = [UIFont systemFontOfSize:24.0];
        _btnLoginWithOTP.titleLabel.font = [UIFont systemFontOfSize:24.0];
        _btnForgotPin.titleLabel.font = [UIFont systemFontOfSize:24.0];
        btnLogin.titleLabel.font = [UIFont systemFontOfSize:26.0];
        btnNewUser.titleLabel.font = [UIFont systemFontOfSize:26.0];
        _txt_enterId.font = [UIFont systemFontOfSize:22.0];
        lblPrefixMobileNum.font = [UIFont systemFontOfSize:22.0];
        _txtMPin.font = [UIFont systemFontOfSize:22.0];
        _txtAadhar.font = [UIFont systemFontOfSize:22.0];
        _lblDontHaveAccount.font = [UIFont systemFontOfSize:24.0];
        _orLoginWith.font = [UIFont systemFontOfSize:22.0];
        
    }
    
    
    [_btn_mobile setTitle:NSLocalizedString(@"mobile_text", nil).uppercaseString forState:UIControlStateNormal];
    [_btn_Adhar setTitle:NSLocalizedString(@"aadhaar", nil).uppercaseString forState:UIControlStateNormal];
    [_btnLoginWithOTP setTitle:NSLocalizedString(@"initiate_login_with_otp", nil) forState:UIControlStateNormal];
    
    NSString*btn_forget=[NSString stringWithFormat:@"%@",NSLocalizedString(@"unable_access_account", nil)];
    [_btnForgotPin setTitle:btn_forget forState:UIControlStateNormal];
    
    under_View.frame = CGRectMake(_btn_mobile.frame.origin.x, _btn_mobile.frame.origin.y + _btn_mobile.frame.size.height, _btn_mobile.frame.size.width, 3);
    
    
    /*[self.btn_mobile setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:190.0/255.0 green:211.0/255.0 blue:228.0/255.0 alpha:1.0]] forState:UIControlStateNormal ];
     
     [self.btn_mobile setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:219.0/255.0 green:232.0/255.0 blue:239.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
     
     [self.btn_mobile setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:219.0/255.0 green:232.0/255.0 blue:239.0/255.0 alpha:1.0]] forState:UIControlStateSelected];
     
     [self.btn_Adhar setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:190.0/255.0 green:211.0/255.0 blue:228.0/255.0 alpha:1.0]] forState:UIControlStateNormal ];
     
     [self.btn_Adhar setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:219.0/255.0 green:232.0/255.0 blue:239.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
     
     [self.btn_Adhar setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:219.0/255.0 green:232.0/255.0 blue:239.0/255.0 alpha:1.0]] forState:UIControlStateSelected];*/
    
    
    flg_LgType=TRUE;
    self.img_id.image=[UIImage imageNamed:@"mobile_no"];
    //self.txt_enterId.text=@"MOBILE NUMBER";
    [self.btn_Adhar setSelected:NO];
    [self.btn_mobile setSelected:YES];
    
   
    
    
    // vw_login.layer.borderColor = [[UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0] CGColor];
    // vw_login.layer.borderWidth = 0.7;
    vw_login.layer.cornerRadius = 10;
    vw_login.layer.masksToBounds = true;
    
    //btnLogin.backgroundColor = [UIColor colorWithRed:33.0/255.0 green:81.0/255.0 blue:112.0/255.0 alpha:0.5];
    //btnNewUser.backgroundColor = [UIColor colorWithRed:33.0/255.0 green:81.0/255.0 blue:112.0/255.0 alpha:0.5];
    
    // btnLogin.layer.borderColor = [UIColor colorWithRed:139.0/255.0 green:186.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
    //btnLogin.layer.borderWidth = 1.0;
    btnLogin.layer.cornerRadius = 4.0;
    
    //btnNewUser.layer.borderColor = [UIColor colorWithRed:139.0/255.0 green:186.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
    //btnNewUser.layer.borderWidth = 1.0;
    //btnNewUser.layer.cornerRadius = 4.0;
    
    
   
    
    
    /*NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[NSLocalizedString(@"click_here_to_change_state", nil) dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
     
     [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.0/255.0f green:89.0/255.0f blue:157.0/255.0f alpha:1.0] range:NSMakeRange(0, attrStr.length)];*/
    

    

    
    _txtMPin.delegate=self;
    _txt_enterId.delegate=self;
    _txtAadhar.delegate=self;
    
    [_txtMPin setValue:[UIColor colorWithRed:170.0/255.0f green:170.0/255.0f blue:170.0/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    [_txt_enterId setValue:[UIColor colorWithRed:170.0/255.0f green:170.0/255.0f blue:170.0/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    [_txtAadhar setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    [self.btnLoginWithOTP addTarget:self action:@selector(btnLoginWithOTPClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnForgotPin addTarget:self action:@selector(btnForgotPINClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    
    
    // Do any additional setup after loading the view.
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    scrollView.delegate=self;
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollView.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height;
                       scrollView.contentSize = contentRect.size;
                   });
    
    
//    NSString *changeString = [NSString stringWithFormat:@" [%@]",[NSLocalizedString(@"change", nil) uppercaseString]];
//
//    NSAttributedString *lang = [[NSAttributedString alloc] initWithString:[singleton.languageSelected uppercaseString]attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14],NSForegroundColorAttributeName: [UIColor whiteColor]}];
//    NSAttributedString *change = [[NSAttributedString alloc] initWithString:changeString attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14],NSForegroundColorAttributeName: [UIColor whiteColor]}];
//    NSMutableAttributedString *strLang = [[NSMutableAttributedString alloc] initWithAttributedString:lang];
//    [strLang appendAttributedString:change];
//
    
    
    //NSString *combinedString = [NSString stringWithFormat:@"%@ [%@]",singleton.languageSelected,[NSLocalizedString(@"change", nil) uppercaseString]];
    //[changeLanguageButton setAttributedTitle:strLang forState:UIControlStateNormal];
    
    [self setLocalizedValue];
    
    [self.radioBtnMpin setImage:[UIImage imageNamed:@"icon_radio_button_select"] forState:UIControlStateNormal];
    [self.radioBtnOTP setImage:[UIImage imageNamed:@"icon_radio_button"] forState:UIControlStateNormal];
    mpinSelected = YES;
    otpSelected = NO;
    
    btnNewUser.layer.cornerRadius = 18.0f;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        btnNewUser.layer.cornerRadius = 25.0f;
    }
    
    
    btnNewUser.layer.borderWidth  = 1.0f;
    btnNewUser.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:166.0/255.0f blue:86.0/255.0f alpha:1.0].CGColor;
    btnNewUser.clipsToBounds = YES;
    
}

-(void)setLocalizedValue {
    
    [_txtMPin setValue:[UIColor colorWithRed:170.0/255.0f green:170.0/255.0f blue:170.0/255.0f alpha:1.0f] forKeyPath:@"_placeholderLabel.textColor"];
    [_txt_enterId setValue:[UIColor colorWithRed:170.0/255.0f green:170.0/255.0f blue:170.0/255.0f alpha:1.0f] forKeyPath:@"_placeholderLabel.textColor"];
    [_txtAadhar setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.txt_enterId.placeholder=[NSLocalizedString(@"new_mob_num", nil) uppercaseString];
    [_btn_mobile setTitle:NSLocalizedString(@"mobile_text", nil).uppercaseString forState:UIControlStateNormal];
    [_btn_Adhar setTitle:NSLocalizedString(@"aadhaar", nil).uppercaseString forState:UIControlStateNormal];
    [_btnLoginWithOTP setTitle:NSLocalizedString(@"initiate_login_with_otp", nil) forState:UIControlStateNormal];
    
    NSString*btn_forget=[NSString stringWithFormat:@"%@",NSLocalizedString(@"forgot_mpin", nil)];
    
    [_btnForgotPin setTitle:btn_forget forState:UIControlStateNormal];
    self.txt_enterId.placeholder=[NSLocalizedString(@"new_mob_num", nil) uppercaseString];
    
    self.txtAadhar.placeholder= [NSLocalizedString(@"aadhaar_no_small", nil) uppercaseString];
    //@"AADHAR NUMBER";
    
    _txtMPin.placeholder=NSLocalizedString(@"mpin", nil);
    
    if(mpinSelected)
    {
        [btnLogin setTitle:NSLocalizedString(@"login_caps", @"")  forState:UIControlStateNormal];
    }
    else
    {
        [btnLogin setTitle:NSLocalizedString(@"btn_continue", @"")  forState:UIControlStateNormal];
    }
    
    _lblDontHaveAccount.text = NSLocalizedString(@"no_account", @"");
    
    _orLoginWith.text = [NSLocalizedString(@"login_with_text", @"") uppercaseString];
    NSMutableAttributedString *newuser;
    if (self.view.frame.size.height >= 1024)
    {
        
        newuser = [[NSMutableAttributedString alloc] initWithData:[NSLocalizedString(@"new_user", @"") dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        
        [newuser addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, newuser.length)];
        [newuser addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:22 weight:UIFontWeightMedium] range:NSMakeRange(0, newuser.length)];
        
    }
    else
    {
        newuser = [[NSMutableAttributedString alloc] initWithData:[NSLocalizedString(@"new_user", @"") dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        [newuser addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.0/255.0f green:166.0/255.0f blue:86.0/255.0f alpha:1.0] range:NSMakeRange(0, newuser.length)];
        [newuser addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17 weight:UIFontWeightMedium] range:NSMakeRange(0, newuser.length)];
    }
    
    
    
    NSString *registerStr = NSLocalizedString(@"register_caps", nil);
    [btnNewUser setTitleColor:[UIColor colorWithRed:0.0/255.0f green:166.0/255.0f blue:86.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    //[btnNewUser setAttributedTitle:newuser  forState:UIControlStateNormal];
    [btnNewUser setTitle:registerStr forState:UIControlStateNormal];
}




-(void)hideKeyboard
{
    [scrollView setContentOffset:
     CGPointMake(0, -scrollView.contentInset.top) animated:YES];
    
    
    if ([_txt_enterId.text length]!=0) {
        singleton.temp_reg_mobile=_txt_enterId.text;
    }
    [self.view endEditing:YES];
    
    // [self.txt_mobileNo resignFirstResponder];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setLocalizedValue];
    [self hitInitAPI];
    
    [self setFontforView:self.view andSubViews:YES];
    
    [[SDImageCache sharedImageCache]clearMemory];
    [[SDImageCache sharedImageCache]clearDisk];
    
    NSMutableDictionary *tempDic=[NSMutableDictionary new];
    [tempDic setValue:@"" forKey:@"test"];
    [[NSUserDefaults standardUserDefaults] setValue:tempDic forKey:@"LABLE_DIC"];
    [[NSUserDefaults standardUserDefaults] setValue:tempDic forKey:@"LOCALVALUE_DIC"];
    
    
    
    //------- Added to clear value while jump in login screen-------
    [[NSUserDefaults standardUserDefaults] setObject:0 forKey:@"BadgeValue"];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0]; // this one
    
    
    if ([[UIScreen mainScreen]bounds].size.width == 480||[[UIScreen mainScreen]bounds].size.width == 320||[[UIScreen mainScreen]bounds].size.width == 568||[[UIScreen mainScreen]bounds].size.width == 667||[[UIScreen mainScreen]bounds].size.width == 736)
    {
        AppDelegate *appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        appD.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
        //appDelegate.shouldRotate = NO;
        //[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }
    //-----------------------------------------------------------
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //——————— Add to handle portrait mode only———
    //  [[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    //   AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    //  appDelegate.shouldRotate = NO;
    //  [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    [self logoutFromSocialAccounts];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    
    /*UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
     CAGradientLayer *gradient = [CAGradientLayer layer];
     
     gradient.frame = view.bounds;
     gradient.startPoint = CGPointMake(0.5, 0.0);
     gradient.endPoint   = CGPointMake(0.5, 1.0);
     gradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor colorWithRed:0.0/255.0f green:89.0/255.0f blue:157.0/255.0f alpha:1.0].CGColor];
     
     [view.layer insertSublayer:gradient atIndex:0];
     [self.view addSubview:view];
     [self.view bringSubviewToFront:view];*/
    
    //}
    
    
    //updMPinVC.dic_info=user_dic;
    [self setViewFont];

//    [self showHint];

    
}
-(void)checkHintScreen {
    NSString* showOTP_Hint = [[NSUserDefaults standardUserDefaults] valueForKey:@"showLoginOTP_Hint"];
    
    
    if ([[showOTP_Hint uppercaseString] isEqualToString:@"YES"])
    {
        
        showOTP_Hint = @"NO";
        
        [[NSUserDefaults standardUserDefaults] setValue:showOTP_Hint forKey:@"showLoginOTP_Hint"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
       [self showHint];
    }

}
-(void)showHint
{
    
    //    CGRect tabBarRect = self.tabBarController.tabBar.frame;
    //    NSInteger buttonCount = self.tabBarController.tabBar.items.count;
    //    CGFloat containingWidth = tabBarRect.size.width/buttonCount;
    //    CGFloat originX = containingWidth * (buttonCount-1) ;
    //    CGRect containingRect = CGRectMake( (originX+containingWidth/2) -self.tabBarController.tabBar.frame.size.height/2, fDeviceHeight-self.tabBarController.tabBar.frame.size.height, self.tabBarController.tabBar.frame.size.height , self.tabBarController.tabBar.frame.size.height );
    [_btnLoginWithOTP setTitleColor: [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:0.9] forState:UIControlStateNormal];
    //_btnLoginWithOTP.titleLabel
    MaterialShowcase *showcase = [[MaterialShowcase alloc] init];
    showcase.backgroundPromptColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:0.4];
    showcase.tag = 6660;
    showcase.isLogin = @"yes";
    //showcase.backgroundPromptColorAlpha = 0.96
  
   
    [showcase setTargetViewWithView:_btnLoginWithOTP.titleLabel];
    showcase.primaryText =NSLocalizedString(@"login_with_otp", nil);; //
    //showcase.targetTintColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:1];
    showcase.TARGET_HOLDER_RADIUS = 240;
    [showcase setDelegate:nil];
    showcase.secondaryText = NSLocalizedString(@"login_hint_text", nil);
    [showcase showWithCompletion:^{
         [_btnLoginWithOTP setTitleColor: [UIColor colorWithRed:170.0/255.0f green:170.0/255.0f blue:170.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    }];    // 0 59 187
    
    
}
-(void)setViewFont
{
    [_btn_mobile.titleLabel setFont:[AppFont regularFont:15.0]];
    [_btn_Adhar.titleLabel setFont:[AppFont mediumFont:15.0]];
    
    _txt_enterId.font = [AppFont regularFont:17.0];
    _txtAadhar.font = [AppFont mediumFont:17.0];
    _txtMPin.font = [AppFont regularFont:17.0];
    [btnLogin.titleLabel setFont:[AppFont semiBoldFont:17.0]];
    [_btnLoginWithOTP.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btnForgotPin.titleLabel setFont:[AppFont regularFont:15.0]];
    lblPrefixMobileNum.font = [AppFont regularFont:17.0];
    _orLoginWith.font = [AppFont regularFont:17.0];
    _lblDontHaveAccount.font = [AppFont mediumFont:15.0];
    [btnNewUser.titleLabel setFont:[AppFont mediumFont:17.0]];
    
    
    if (self.view.frame.size.width == 320)
    {
        _txt_enterId.font = [AppFont regularFont:15.0];
        _txtMPin.font = [AppFont regularFont:15.0];
        [btnLogin.titleLabel setFont:[AppFont semiBoldFont:15.0]];
        [_btnLoginWithOTP.titleLabel setFont:[AppFont regularFont:14.0]];
        [_btnForgotPin.titleLabel setFont:[AppFont regularFont:13.0]];
        lblPrefixMobileNum.font = [AppFont regularFont:15.0];
        _orLoginWith.font = [AppFont regularFont:15.0];
        _lblDontHaveAccount.font = [AppFont mediumFont:13.0];
        [btnNewUser.titleLabel setFont:[AppFont mediumFont:15.0]];
    }
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [self prepareTempDataForLanguageType];
    [self checkHintScreen];
    //[self showHint];
}


-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}


-(void)btnForgotPINClicked
{
      [[SharedManager sharedSingleton] traceEvents:@"Forgot MPIN Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Flagship" bundle:nil];
    AccountRecoveryVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AccountRecoveryVC"];
    vc.hidesBottomBarWhenPushed = YES;
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:true];
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    ForgotMPINVC *vc;
//    if ([[UIScreen mainScreen]bounds].size.height == 1024)
//    {
//        vc = [[ForgotMPINVC alloc] initWithNibName:@"ForgotMPINVC_iPad" bundle:nil];
//        vc.strMobileNumber =    val_mb;
//        //loadNibNamed:@"MobileRegistrationVC_ipad" owner:self options:nil];
//        // cell = (MobileRegistrationVC_ipad *)[nib objectAtIndex:0];
//        [self presentViewController:vc animated:YES completion:nil];
//
//    }
//
//
//    else
//    {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"ForgotMPINVC"];
//        vc.strMobileNumber =    val_mb;
//        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//        [self presentViewController:vc animated:YES completion:nil];
//
//    }
    
}



-(void)btnLoginWithOTPClicked
{
    
    [self radioBtnOTPClicked:nil];
    
    
    /*[[SharedManager sharedSingleton] traceEvents:@"Login With OTP Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    LoginWithOTPVC *vc;
    if ([[UIScreen mainScreen]bounds].size.height >= 1024)
    {
        vc = [[LoginWithOTPVC alloc] initWithNibName:@"LoginWithOTPVC_iPad" bundle:nil];
        vc.strMobileNumber =  val_mb;
        //loadNibNamed:@"MobileRegistrationVC_ipad" owner:self options:nil];
        // cell = (MobileRegistrationVC_ipad *)[nib objectAtIndex:0];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:vc animated:YES completion:nil];
        
        
    }
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginWithOTPVC"];
    vc.strMobileNumber =  val_mb;
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:YES completion:nil];*/
    
    
    /*
     UserEditVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserEditVC"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     [self presentViewController:vc animated:YES completion:nil];
     
     */
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
    NSString *phoneRegex =@"[6789][0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==_txt_enterId)
    {
        if (flg_LgType==TRUE)
        {
            NSString * currentTxtStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
            
            NSLog(@"value =%lu  range.length=%lu",_txt_enterId.text.length,(unsigned long)range.length);
            
            NSLog(@"currentTxtStr txt =%@ ",currentTxtStr);
            currentTxtStr = [currentTxtStr stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            //mobile
            //if (_txt_enterId.text.length >= 10 && range.length == 0)
            if (currentTxtStr.length >= 10 && range.length == 0)
                
            {
                
                _txt_enterId.text=[currentTxtStr substringToIndex:10];;
                [textField resignFirstResponder];
                
                if (mpinSelected)
                {
                    [_txtMPin becomeFirstResponder];
                }
                
                return NO; // return NO to not change text
                
            }
            
            
        }
        else
        {
            NSString * currentTxtStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
            currentTxtStr = [currentTxtStr stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            NSLog(@"currentTxtStr txt =%@ ",currentTxtStr);
            
            //aadhaar card
            [textField becomeFirstResponder];
            //if (_txtAadhar.text.length >= 12 && range.length == 0)
            if (currentTxtStr.length >= 12 && range.length == 0)
            {
                _txtAadhar.text=[currentTxtStr substringToIndex:12];
                
                [textField resignFirstResponder];
                return NO; // return NO to not change text
            }
            
        }
        
        
    }
    else if (textField==_txtMPin)
    {
        //mobile
        NSString * currentTxtStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
        currentTxtStr = [currentTxtStr stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
        
        NSLog(@"currentTxtStr txt =%@ ",currentTxtStr);
        
        //  if (_txtMPin.text.length >= 4 && range.length == 0)
        if (currentTxtStr.length >= 4 && range.length == 0)
        {
            _txtMPin.text=[currentTxtStr substringToIndex:4];;
            
            [textField resignFirstResponder];
            return NO; // return NO to not change text
        }
    }
    
    
    else if (textField == _txtAadhar)
    {
        NSString * currentTxtStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSLog(@"currentTxtStr txt =%@ ",currentTxtStr);
        currentTxtStr = [currentTxtStr stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
        
        //aadhaar card
        [textField becomeFirstResponder];
        // if (_txtAadhar.text.length >= 12 && range.length == 0)
        if (currentTxtStr.length >= 12 && range.length == 0)
        {
            _txtAadhar.text=[currentTxtStr substringToIndex:12];
            
            
            [textField resignFirstResponder];
            return NO; // return NO to not change text
        }
        
    }
    
    return YES;
    
    
    
    
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    
    if (flg_LgType==TRUE)
    {
        val_mb=_txt_enterId.text;
        
        val_mpin_mb=_txtMPin.text;
        
        
        
    }
    else                 //Aadhaar Card
        
    {
        val_aadhar=_txt_enterId.text;
        
        val_mpin_aadhar=_txtMPin.text;
    }
    
    NSLog(@"val_mpin_aadhar=%@",val_mpin_aadhar);
    NSLog(@"val_mpin_mb=%@",val_mpin_mb);
    
    if (fDeviceHeight<=568)
    {
        
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
        [scrollView setContentOffset:CGPointZero animated:YES];
        [UIView commitAnimations];
    }
    
    [self setFontforView:self.view andSubViews:YES];
    
}








//=========== Login Method=================
-(IBAction)loginPressed:(id)sender
{
      [[SharedManager sharedSingleton] traceEvents:@"Login Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    // For Login Case
    //self.USER_LOGIN_TYPE = LOGIN_OTP;
    
    self.USER_LOGIN_TYPE = LOGIN_MPIN;
    
    
    NSLog(@"Login Pressed");
    
    
    NSString *errorMsg=@"";
    if (flg_LgType==TRUE)
    {
        
        if([_txt_enterId.text isEqualToString:@""])
        {
            
            errorMsg= NSLocalizedString(@"mobile_blank_txt", nil);
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:errorMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
            [alert show];
            
        }
        else  if (_txt_enterId.text.length <10)
        {
            errorMsg=NSLocalizedString(@"please_complete_enter_mobile_num", nil);
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:errorMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
            [alert show];
        }
        else if ([self validatePhone:_txt_enterId.text]!=TRUE) {
            
            errorMsg=NSLocalizedString(@"enter_correct_phone_number", nil);
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:errorMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
            [alert show];
        }
        
        else if([_txtMPin.text isEqualToString:@""] && mpinSelected)
        {
            errorMsg=NSLocalizedString(@"enter_mpin_txt", nil);
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:errorMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
            [alert show];
            
        }
        
        else
        {
            if (mpinSelected)
            {
                [self hitAPI];
            }
            else if (otpSelected)
            {
                //OPEN LOGIN WITH OTP
                [self apiForLoginWithOTP];
            }
            
            
            
        }
        
    }
    
    else if (flg_LgType==FALSE)
    {
        //aadhaar card
        self.USER_LOGIN_TYPE = LOGIN_AADHAR;
        NSLog(@"Aadhar Login Pressed");
        
        //aadhaar card
        if([_txtAadhar.text isEqualToString:@""])
        {
            errorMsg=NSLocalizedString(@"enter_aadhaar_txt", nil);
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:errorMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
            [alert show];
        }
        
        else
        {
            [self hitAPI];
        }
        
    }
    
    
}



//===========GOOGLE METHOD ==============

-(void)hitLoginAPIForGoogleSocialAccounts{
    
    isGoogleLogin=2;
    NSLog(@"isGoogleLogin=%d",isGoogleLogin);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GOOGLE_LOGIN" object:nil];
    
    
    NSString *encryptedInputID = @"";
    NSString *loginType = @"";
    NSString *sType = @"";
    
    NSLog(@"self.socialID=%@",self.socialID);
    
    
    
    encryptedInputID=self.socialID;
    
    loginType = @"soc";
    sType = @"google";
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:loginType forKey:@"type"];
    
    [dictBody setObject:encryptedInputID forKey:@"lid"];
    
    [dictBody setObject:sType forKey:@"stype"];
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_LOGIN withBody:dictBody andTag:TAG_REQUEST_LOGIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        NSLog(@"dictBody is = %@",dictBody);
        
        [hud hideAnimated:YES];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //  NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
            
            NSString *rs=[response valueForKey:@"rs"];
            
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
          

            
            singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
            
            //-------- Add later----------
            singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
            singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
            //-------- Add later----------
            
            
            if ([rs isEqualToString:@"SU"]||[rs isEqualToString:@"S"])
            {
                
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                //---fix lint---
                // NSMutableArray *generalpdList=[[NSMutableArray alloc]init];
                // generalpdList=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                
                NSMutableArray *generalpdList = (NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                
                
                //singleton.user_id=[[response valueForKey:@"pd"]valueForKey:@"uid"];
                singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                
                singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
                
                NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                
                NSString*  emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                
                if ([abbreviation length]==0) {
                    abbreviation=@"";
                }
                
                emblemString = emblemString.length == 0? @"": emblemString;
                
                [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //-------- Add later----------
                singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                //-------- Add later----------
                singleton.user_tkn=tkn;
                
                //-------- Add later For handling mpinflag / mpinmand ----------
                singleton.shared_mpinflag =[[response valueForKey:@"pd"]  valueForKey:@"mpinflag"];
                singleton.shared_mpinmand =[[response valueForKey:@"pd"]  valueForKey:@"mpinmand"];
                
                NSLog(@"mpinflag =%@",singleton.shared_mpinflag);
                NSLog(@"mpinmand =%@",singleton.shared_mpinmand);
                if (singleton.shared_mpinflag == (NSString *)[NSNull null]||[singleton.shared_mpinflag length]==0) {
                    singleton.shared_mpinflag=@"TRUE";
                }
                
                if (singleton.shared_mpinmand == (NSString *)[NSNull null]||[singleton.shared_mpinmand length]==0) {
                    singleton.shared_mpinmand=@"TRUE";
                }
                NSString *mpindial =[[response valueForKey:@"pd"]  valueForKey:@"mpindial"];
                NSLog(@"mpindial =%@",mpindial);
                [[NSUserDefaults standardUserDefaults] setValue:mpindial forKey:@"mpindial"];
                
                
                // recflag save start
                NSString *recflag  = [[response valueForKey:@"pd"]  valueForKey:@"recflag"];
                [[NSUserDefaults standardUserDefaults] setValue:recflag forKey:@"recflag"];

                

                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinflag forKey:@"mpinflag"];
                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinmand forKey:@"mpinmand"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //-------- Add later For handling mpinflag / mpinmand ----------
                
                //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                // [[NSUserDefaults standardUserDefaults]synchronize];
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                
                
                
                
                NSString* str_name=[generalpdList valueForKey:@"nam"];
                NSString* str_mno =[generalpdList valueForKey:@"mno"];
                
                
                NSString* str_amno=[generalpdList valueForKey:@"amno"];
                
                
                NSString* str_city=[generalpdList valueForKey:@"cty"];
                NSString* str_state=[generalpdList valueForKey:@"st"];
                NSString* str_district=[generalpdList valueForKey:@"dist"];
                NSString* str_dob=[generalpdList valueForKey:@"dob"];
                NSString* str_gender=[generalpdList valueForKey:@"gndr"];
                NSString* str_pic=[generalpdList valueForKey:@"pic"];
                
                
                NSString* str_occup=[generalpdList valueForKey:@"occup"];
                NSString* str_qual=[generalpdList valueForKey:@"qual"];
                NSString* str_email=[generalpdList valueForKey:@"email"];
                
                
                
                
                
                
                singleton.notiTypeGenderSelected=@"";
                singleton.profileNameSelected =@"";
                singleton.profilestateSelected=@"";
                singleton.notiTypeCitySelected=@"";
                singleton.notiTypDistricteSelected=@"";
                singleton.profileDOBSelected=@"";
                singleton.altermobileNumber=@"";
                singleton.user_profile_URL=@"";
                
                
                
                singleton.user_Qualification=@"";
                singleton.user_Occupation=@"";
                singleton.profileEmailSelected=@"";
                
                singleton.user_profile_URL=@"";
                
                
                
                
                if ([str_occup length]!=0) {
                    singleton.user_Occupation=str_occup;
                }
                
                if ([str_qual length]!=0) {
                    singleton.user_Qualification=str_qual;
                }
                if ([str_email length]!=0) {
                    singleton.profileEmailSelected=str_email;
                }
                
                
                
                
                if ([str_mno length]!=0) {
                    singleton.mobileNumber=str_mno;
                }
                
                
                if ([str_amno length]!=0) {
                    singleton.altermobileNumber=str_amno;
                }
                
                
                
                if ([str_pic length]!=0) {
                    
                    singleton.user_profile_URL=str_pic;
                    //------------------------- Encrypt Value------------------------
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //------------------------- Encrypt Value------------------------
                    
                    
                }
                
                if ([str_name length]!=0) {
                    str_name = [str_name stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[str_name substringToIndex:1] uppercaseString]];
                    
                    
                }
                if ([singleton.profileNameSelected length]==0) {
                    if ([str_name length]!=0) {
                        singleton.profileNameSelected=str_name;
                        
                    }
                }
                
                
                
                
                if ([singleton.notiTypeGenderSelected length]==0) {
                    if ([str_gender length]!=0) {
                        str_gender=[str_gender uppercaseString];
                        
                        if ([str_gender isEqualToString:@"M"]|| [str_gender isEqualToString:@"MALE"])
                        {
                            singleton.notiTypeGenderSelected=@"Male";
                            
                        }
                        if ([str_gender isEqualToString:@"F"]||[str_gender isEqualToString:@"FEMALE"])
                        {
                            singleton.notiTypeGenderSelected=@"Female";
                            
                        }
                        if ([str_gender isEqualToString:@"T"]) {
                            singleton.notiTypeGenderSelected=@"Other";
                            
                        }
                        
                        
                        
                    }
                }
                
                if ([singleton.profilestateSelected length]==0) {
                    if ([str_state length]!=0) {
                        singleton.profilestateSelected=str_state;
                        //state_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypeCitySelected length]==0) {
                    if ([str_city length]!=0) {
                        singleton.notiTypeCitySelected=str_city;
                        // city_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypDistricteSelected length]==0) {
                    if ([str_district length]!=0) {
                        singleton.notiTypDistricteSelected=str_district;
                        //district_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                
                if ([singleton.profileDOBSelected length]==0) {
                    if ([str_dob length]!=0) {
                        singleton.profileDOBSelected=str_dob;
                        
                    }
                }
                
                // ========= check to open show mpin alert box=====
                dispatch_async(dispatch_get_main_queue(),^{
                    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    //[delegate showHideSetMpinAlertBox ];
                    if(!([[recflag uppercaseString] isEqualToString:@"T"]||[[recflag uppercaseString] isEqualToString:@"TRUE"]))
                    {
                        [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];
                    }
                    else
                    {
                        
                        [delegate showHideSetMpinAlertBox ];
                    }
                    // check every time case App is opening
                    [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];
                });
                
                
                [self alertwithMsg:rd];
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            _txtMPin.text=@"";
        }
        
    }];
}



//=========================


//=======================
//       FACEBOOK
//=======================




-(void)hitLoginAPIForFacebookSocialAccounts
{
    
    
    isfacebookLogin=2;
    NSString *encryptedInputID = @"";
    NSString *loginType = @"";
    NSString *sType = @"";
    
    NSLog(@"self.socialID=%@",self.socialID);
    
    
    encryptedInputID=self.socialID;
    loginType = @"soc";
    sType = @"fb";
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:loginType forKey:@"type"];
    
    [dictBody setObject:encryptedInputID forKey:@"lid"];
    
    [dictBody setObject:sType forKey:@"stype"];
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_LOGIN withBody:dictBody andTag:TAG_REQUEST_LOGIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        NSLog(@"dictBody is = %@",dictBody);
        
        [hud hideAnimated:YES];
        
        isLoginAuthInProgress = NO;
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //  NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
            
            NSString *rs=[response valueForKey:@"rs"];
            
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
          

            // singleton.user_id=[[response valueForKey:@"pd"]valueForKey:@"uid"];
            
            singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
            
            //-------- Add later----------
            singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
            singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
            //-------- Add later----------
            
            
            if ([rs isEqualToString:@"SU"]||[rs isEqualToString:@"S"])
            {
                
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                //---fix lint---
                // NSMutableArray *generalpdList=[[NSMutableArray alloc]init];
                // generalpdList=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                
                NSMutableArray *generalpdList = (NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                
                
                //singleton.user_id=[[response valueForKey:@"pd"]valueForKey:@"uid"];
                singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                
                NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                
                if ([abbreviation length]==0)
                {
                    abbreviation=@"";
                }
                
                emblemString = emblemString.length == 0 ? @"" : emblemString;
                
                [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
                
                //-------- Add later----------
                singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                //-------- Add later----------
                singleton.user_tkn=tkn;
                
                //-------- Add later For handling mpinflag / mpinmand ----------
                singleton.shared_mpinflag =[[response valueForKey:@"pd"]  valueForKey:@"mpinflag"];
                singleton.shared_mpinmand =[[response valueForKey:@"pd"]  valueForKey:@"mpinmand"];
                
                NSLog(@"mpinflag =%@",singleton.shared_mpinflag);
                NSLog(@"mpinmand =%@",singleton.shared_mpinmand);
                if (singleton.shared_mpinflag == (NSString *)[NSNull null]||[singleton.shared_mpinflag length]==0) {
                    singleton.shared_mpinflag=@"TRUE";
                }
                
                if (singleton.shared_mpinmand == (NSString *)[NSNull null]||[singleton.shared_mpinmand length]==0) {
                    singleton.shared_mpinmand=@"TRUE";
                }
                NSString *mpindial =[[response valueForKey:@"pd"]  valueForKey:@"mpindial"];
                NSLog(@"mpindial =%@",mpindial);
                [[NSUserDefaults standardUserDefaults] setValue:mpindial forKey:@"mpindial"];
                
                
                // recflag save start
                NSString *recflag  = [[response valueForKey:@"pd"]  valueForKey:@"recflag"];
                [[NSUserDefaults standardUserDefaults] setValue:recflag forKey:@"recflag"];

                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinflag forKey:@"mpinflag"];
                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinmand forKey:@"mpinmand"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //-------- Add later For handling mpinflag / mpinmand ----------
                
                //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                // [[NSUserDefaults standardUserDefaults]synchronize];
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                
                
                
                
                NSString* str_name=[generalpdList valueForKey:@"nam"];
                NSString* str_mno =[generalpdList valueForKey:@"mno"];
                
                
                NSString* str_amno=[generalpdList valueForKey:@"amno"];
                
                
                NSString* str_city=[generalpdList valueForKey:@"cty"];
                NSString* str_state=[generalpdList valueForKey:@"st"];
                NSString* str_district=[generalpdList valueForKey:@"dist"];
                NSString* str_dob=[generalpdList valueForKey:@"dob"];
                NSString* str_gender=[generalpdList valueForKey:@"gndr"];
                NSString* str_pic=[generalpdList valueForKey:@"pic"];
                
                
                NSString* str_occup=[generalpdList valueForKey:@"occup"];
                NSString* str_qual=[generalpdList valueForKey:@"qual"];
                NSString* str_email=[generalpdList valueForKey:@"email"];
                
                
                
                
                
                
                singleton.notiTypeGenderSelected=@"";
                singleton.profileNameSelected =@"";
                singleton.profilestateSelected=@"";
                singleton.notiTypeCitySelected=@"";
                singleton.notiTypDistricteSelected=@"";
                singleton.profileDOBSelected=@"";
                singleton.altermobileNumber=@"";
                singleton.user_profile_URL=@"";
                
                
                
                singleton.user_Qualification=@"";
                singleton.user_Occupation=@"";
                singleton.profileEmailSelected=@"";
                
                singleton.user_profile_URL=@"";
                
                
                
                
                if ([str_occup length]!=0) {
                    singleton.user_Occupation=str_occup;
                }
                
                if ([str_qual length]!=0) {
                    singleton.user_Qualification=str_qual;
                }
                if ([str_email length]!=0) {
                    singleton.profileEmailSelected=str_email;
                }
                
                
                
                
                if ([str_mno length]!=0) {
                    singleton.mobileNumber=str_mno;
                }
                
                
                if ([str_amno length]!=0) {
                    singleton.altermobileNumber=str_amno;
                }
                
                
                
                if ([str_pic length]!=0) {
                    
                    singleton.user_profile_URL=str_pic;
                    //------------------------- Encrypt Value------------------------
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //------------------------- Encrypt Value------------------------
                    
                    
                }
                
                if ([str_name length]!=0) {
                    str_name = [str_name stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[str_name substringToIndex:1] uppercaseString]];
                    
                    
                }
                if ([singleton.profileNameSelected length]==0) {
                    if ([str_name length]!=0) {
                        singleton.profileNameSelected=str_name;
                        
                    }
                }
                
                
                
                
                if ([singleton.notiTypeGenderSelected length]==0) {
                    if ([str_gender length]!=0) {
                        str_gender=[str_gender uppercaseString];
                        
                        if ([str_gender isEqualToString:@"M"]|| [str_gender isEqualToString:@"MALE"])
                        {
                            singleton.notiTypeGenderSelected=@"Male";
                            
                        }
                        if ([str_gender isEqualToString:@"F"]||[str_gender isEqualToString:@"FEMALE"])
                        {
                            singleton.notiTypeGenderSelected=@"Female";
                            
                        }
                        if ([str_gender isEqualToString:@"T"]) {
                            singleton.notiTypeGenderSelected=@"Other";
                            
                        }
                        
                        
                        
                    }
                }
                
                if ([singleton.profilestateSelected length]==0) {
                    if ([str_state length]!=0) {
                        singleton.profilestateSelected=str_state;
                        //state_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypeCitySelected length]==0) {
                    if ([str_city length]!=0) {
                        singleton.notiTypeCitySelected=str_city;
                        // city_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypDistricteSelected length]==0) {
                    if ([str_district length]!=0) {
                        singleton.notiTypDistricteSelected=str_district;
                        //district_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                
                if ([singleton.profileDOBSelected length]==0) {
                    if ([str_dob length]!=0) {
                        singleton.profileDOBSelected=str_dob;
                        
                    }
                }
                
                // ========= check to open show mpin alert box=====
                dispatch_async(dispatch_get_main_queue(),^{
                    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    //[delegate showHideSetMpinAlertBox ];
                    if(!([[recflag uppercaseString] isEqualToString:@"T"]||[[recflag uppercaseString] isEqualToString:@"TRUE"]))
                    {
                        [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];
                    }
                    else
                    {
                        
                        [delegate showHideSetMpinAlertBox ];
                    }
                    [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];

                });
                
                [self alertwithMsg:rd];
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            _txtMPin.text=@"";
        }
        
    }];
}






//======================
//   TWITTER
//=====================

-(void)hitLoginAPIForTwitterSocialAccounts
{
    
    
    NSString *encryptedInputID = @"";
    NSString *loginType = @"";
    NSString *sType = @"";
    
    NSLog(@"self.socialID=%@",self.socialID);
    
    
    encryptedInputID=self.socialID;
    loginType = @"soc";
    sType = @"twitter";
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:loginType forKey:@"type"];
    
    [dictBody setObject:encryptedInputID forKey:@"lid"];
    
    [dictBody setObject:sType forKey:@"stype"];
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_LOGIN withBody:dictBody andTag:TAG_REQUEST_LOGIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        NSLog(@"dictBody is = %@",dictBody);
        
        [hud hideAnimated:YES];
        
        isLoginAuthInProgress = NO;
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //  NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
            
            NSString *rs=[response valueForKey:@"rs"];
            
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
          

            // singleton.user_id=[[response valueForKey:@"pd"]valueForKey:@"uid"];
            
            singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
            
            //-------- Add later----------
            singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
            singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
            //-------- Add later----------
            
            
            if ([rs isEqualToString:@"SU"]||[rs isEqualToString:@"S"]) {
                
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                //---fix lint---
                // NSMutableArray *generalpdList=[[NSMutableArray alloc]init];
                // generalpdList=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                
                NSMutableArray *generalpdList = (NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                
                
                //singleton.user_id=[[response valueForKey:@"pd"]valueForKey:@"uid"];
                singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                
                NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                
                if ([abbreviation length]==0) {
                    abbreviation=@"";
                }
                
                emblemString = emblemString.length == 0 ? @"":emblemString;
                
                [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
                
                //-------- Add later----------
                singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                //-------- Add later----------
                singleton.user_tkn=tkn;
                
                //-------- Add later For handling mpinflag / mpinmand ----------
                singleton.shared_mpinflag =[[response valueForKey:@"pd"]  valueForKey:@"mpinflag"];
                singleton.shared_mpinmand =[[response valueForKey:@"pd"]  valueForKey:@"mpinmand"];
                
                NSLog(@"mpinflag =%@",singleton.shared_mpinflag);
                NSLog(@"mpinmand =%@",singleton.shared_mpinmand);
                if (singleton.shared_mpinflag == (NSString *)[NSNull null]||[singleton.shared_mpinflag length]==0) {
                    singleton.shared_mpinflag=@"TRUE";
                }
                
                if (singleton.shared_mpinmand == (NSString *)[NSNull null]||[singleton.shared_mpinmand length]==0) {
                    singleton.shared_mpinmand=@"TRUE";
                }
                NSString *mpindial =[[response valueForKey:@"pd"]  valueForKey:@"mpindial"];
                NSLog(@"mpindial =%@",mpindial);
                [[NSUserDefaults standardUserDefaults] setValue:mpindial forKey:@"mpindial"];
                
                // recflag save start
                NSString *recflag  = [[response valueForKey:@"pd"]  valueForKey:@"recflag"];
                [[NSUserDefaults standardUserDefaults] setValue:recflag forKey:@"recflag"];


                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinflag forKey:@"mpinflag"];
                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinmand forKey:@"mpinmand"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //-------- Add later For handling mpinflag / mpinmand ----------
                
                //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                // [[NSUserDefaults standardUserDefaults]synchronize];
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                
                
                
                
                NSString* str_name=[generalpdList valueForKey:@"nam"];
                NSString* str_mno =[generalpdList valueForKey:@"mno"];
                
                
                NSString* str_amno=[generalpdList valueForKey:@"amno"];
                
                
                NSString* str_city=[generalpdList valueForKey:@"cty"];
                NSString* str_state=[generalpdList valueForKey:@"st"];
                NSString* str_district=[generalpdList valueForKey:@"dist"];
                NSString* str_dob=[generalpdList valueForKey:@"dob"];
                NSString* str_gender=[generalpdList valueForKey:@"gndr"];
                NSString* str_pic=[generalpdList valueForKey:@"pic"];
                
                
                NSString* str_occup=[generalpdList valueForKey:@"occup"];
                NSString* str_qual=[generalpdList valueForKey:@"qual"];
                NSString* str_email=[generalpdList valueForKey:@"email"];
                
                
                
                
                
                
                singleton.notiTypeGenderSelected=@"";
                singleton.profileNameSelected =@"";
                singleton.profilestateSelected=@"";
                singleton.notiTypeCitySelected=@"";
                singleton.notiTypDistricteSelected=@"";
                singleton.profileDOBSelected=@"";
                singleton.altermobileNumber=@"";
                singleton.user_profile_URL=@"";
                
                
                
                singleton.user_Qualification=@"";
                singleton.user_Occupation=@"";
                singleton.profileEmailSelected=@"";
                
                singleton.user_profile_URL=@"";
                
                
                
                
                if ([str_occup length]!=0) {
                    singleton.user_Occupation=str_occup;
                }
                
                if ([str_qual length]!=0) {
                    singleton.user_Qualification=str_qual;
                }
                if ([str_email length]!=0) {
                    singleton.profileEmailSelected=str_email;
                }
                
                
                
                
                if ([str_mno length]!=0) {
                    singleton.mobileNumber=str_mno;
                }
                
                
                if ([str_amno length]!=0) {
                    singleton.altermobileNumber=str_amno;
                }
                
                
                
                if ([str_pic length]!=0) {
                    
                    singleton.user_profile_URL=str_pic;
                    //------------------------- Encrypt Value------------------------
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //------------------------- Encrypt Value------------------------
                    
                    
                }
                
                if ([str_name length]!=0) {
                    str_name = [str_name stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[str_name substringToIndex:1] uppercaseString]];
                    
                    
                }
                if ([singleton.profileNameSelected length]==0) {
                    if ([str_name length]!=0) {
                        singleton.profileNameSelected=str_name;
                        
                    }
                }
                
                
                
                
                if ([singleton.notiTypeGenderSelected length]==0) {
                    if ([str_gender length]!=0) {
                        str_gender=[str_gender uppercaseString];
                        
                        if ([str_gender isEqualToString:@"M"]|| [str_gender isEqualToString:@"MALE"])
                        {
                            singleton.notiTypeGenderSelected=@"Male";
                            
                        }
                        if ([str_gender isEqualToString:@"F"]||[str_gender isEqualToString:@"FEMALE"])
                        {
                            singleton.notiTypeGenderSelected=@"Female";
                            
                        }
                        if ([str_gender isEqualToString:@"T"]) {
                            singleton.notiTypeGenderSelected=@"Other";
                            
                        }
                        
                        
                        
                    }
                }
                
                if ([singleton.profilestateSelected length]==0) {
                    if ([str_state length]!=0) {
                        singleton.profilestateSelected=str_state;
                        //state_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypeCitySelected length]==0) {
                    if ([str_city length]!=0) {
                        singleton.notiTypeCitySelected=str_city;
                        // city_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypDistricteSelected length]==0) {
                    if ([str_district length]!=0) {
                        singleton.notiTypDistricteSelected=str_district;
                        //district_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                
                if ([singleton.profileDOBSelected length]==0) {
                    if ([str_dob length]!=0) {
                        singleton.profileDOBSelected=str_dob;
                        
                    }
                }
                
                // ========= check to open show mpin alert box=====
                dispatch_async(dispatch_get_main_queue(),^{
                    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    //[delegate showHideSetMpinAlertBox ];
                    if(!([[recflag uppercaseString] isEqualToString:@"T"]||[[recflag uppercaseString] isEqualToString:@"TRUE"]))
                    {
                        [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];
                    }
                    else
                    {
                        
                        [delegate showHideSetMpinAlertBox ];
                    }
                    [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];

                });
                
                [self alertwithMsg:rd];
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            _txtMPin.text=@"";
        }
        
    }];
}





//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    
    
    NSString *encryptedInputID = @"";
    NSString *loginType = @"";
    NSString *sType = @"";
    
    
    if (self.USER_LOGIN_TYPE == LOGIN_OTP)//login with OTP
    {
        //encryptedInputID=[_txtMPin.text sha256HashFor:_txtMPin.text];
        NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

        NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",_txtMPin.text ,strSaltMPIN];
        encryptedInputID=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
        loginType = @"mobo";
        sType = @"";
    }
    else if (self.USER_LOGIN_TYPE == LOGIN_FACEBOOK) {
        // encryptedInputID=[self.socialID sha256HashFor:self.socialID];
        // NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",self.socialID ,SaltMPIN];
        // encryptedInputID=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
        
        encryptedInputID=self.socialID;
        loginType = @"soc";
        sType = @"fb";
        
    }
    
    
    else if (self.USER_LOGIN_TYPE == LOGIN_TWITTER) {
        // encryptedInputID=[self.socialID sha256HashFor:self.socialID];
        // NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",self.socialID ,SaltMPIN];
        // encryptedInputID=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
        
        encryptedInputID=self.socialID;
        loginType = @"soc";
        sType = @"twitter";
        
    }
    
    else if (self.USER_LOGIN_TYPE == LOGIN_GOOGLE) {
        // encryptedInputID=[self.socialID sha256HashFor:self.socialID];
        // NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",self.socialID ,SaltMPIN];
        // encryptedInputID=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
        encryptedInputID=self.socialID;
        
        loginType = @"soc";
        sType = @"google";
    }
    
    else if (self.USER_LOGIN_TYPE == LOGIN_MPIN)//login with MPIN
    {
        loginType = @"mobm";
        sType = @"";
        NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

        NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",_txtMPin.text,strSaltMPIN];
        encryptedInputID=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
        
    }
    
    else if (self.USER_LOGIN_TYPE == LOGIN_AADHAR)//login with AAdhar
    {
        
        
        
        
        
    }
    
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    if (flg_LgType==TRUE)
    {
        //mobile
        //        [dictBody setObject:@"mobm" forKey:@"type"];
        //for Mobile with Mpin
        //[dictBody setObject:@"mobo" forKey:@"type"]; //for Mobile with OTP
        //[dictBody setObject:@"soc" forKey:@"type"];  //for Mobile with Social
        
        [dictBody setObject:loginType forKey:@"type"];
        
        
        //for Mobile with Mpin
        //[dictBody setObject:@"OTP" forKey:@"lid"];  //for Mobile with OTP
        //[dictBody setObject:@"Social ID" forKey:@"lid"]; //for Mobile with Social ID
        [dictBody setObject:encryptedInputID forKey:@"lid"];
        
        [dictBody setObject:sType forKey:@"stype"];
        [dictBody setObject:_txt_enterId.text forKey:@"mno"];
        
        
        
        
    }
    else  //Aadhar case not working now
    {
        //aadhaar card
        [dictBody setObject:@"aadhar" forKey:@"type"];
        [dictBody setObject:@"" forKey:@"stype"];
        [dictBody setObject:_txtAadhar.text forKey:@"aadhr"];
        
        [dictBody setObject:@"loginadhr" forKey:@"ort"];
        
        //[dictBody setObject:@"mobm" forKey:@"mobo"];
        
        
        [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_GENERATE_AADHAR_OTP withBody:dictBody andTag:TAG_REQUEST_GENERATE_AADHAR_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
            NSLog(@"dictBody is = %@",dictBody);
            
            
            [hud hideAnimated:YES];
            // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
            
            
            if (error == nil) {
                NSLog(@"Server Response = %@",response);
                
                NSString *node=[response valueForKey:@"node"];
                if([node length]>0)
                {
                    [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
                
                
                //----- below value need to be forword to next view according to requirement after checking Android apk-----
                //                NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
                //                NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
                NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
                
                tout=[[[[response valueForKey:@"pd"]valueForKey:@"pd"] valueForKey:@"tout"] intValue];
                rtry=[[[response valueForKey:@"pd"] valueForKey:@"pd"]valueForKey:@"rtry"];
                
                
                
                //                NSString *rc=[response valueForKey:@"rc"];
                //                NSString *rd=[response valueForKey:@"rd"];
                //                NSString *rs=[response valueForKey:@"rs"];
                NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
                
                //----- End value need to be forword to next view according to requirement after checking Android apk-----
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                {
                    
                    singleton.user_tkn=tkn;
                    //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                    //[[NSUserDefaults standardUserDefaults]synchronize];
                    
                    
                    //------------------------- Encrypt Value------------------------
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //------------------------- Encrypt Value------------------------
                    
                    singleton.objUserProfile = nil;
                    singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                    
                    //singleton.mobileNumber=txt.text;
                    
                    
                    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    EnterMobileOTPVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"EnterMobileOTPVC"];
                    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                    vc.strAadharNumber = _txtAadhar.text;
                    vc.tout=tout;
                    vc.rtry=rtry;
                    vc.maskedMobileNumber = wmsg;
                    //vc.TYPE_RESEND_OTP_FROM=ISFROMLOGINAPPVC;
                    vc.lblScreenTitleName.text = @"Aadhaar Number Verification";
                    vc.TYPE_LOGIN_CHOOSEN = IS_FROM_AADHAR_LOGIN;
                    
                    [self presentViewController:vc animated:YES completion:nil];
                    
                }
            }
            else{
                NSLog(@"Error Occured = %@",error.localizedDescription);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                message:error.localizedDescription
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil];
                [alert show];
                
                
                _txtAadhar.text=@"";
                
                
            }
            
        }];
        
        
        
        return;
        
    }
    
    
    
    
    
    
    
    
    //  if (_txt_enterId.text.length) {
    //    [dictBody setObject:_txt_enterId.text forKey:@"mno"];
    //  }
    //  else{
    //    [dictBody setObject:@"" forKey:@"mno"];
    //  }
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_LOGIN withBody:dictBody andTag:TAG_REQUEST_LOGIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        NSLog(@"dictBody is = %@",dictBody);
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            //[self openNextView];
            /*
             
             
             ]  jsonResponse ={
             pd =     {
             rc = "<null>";
             rd = "<null>";
             rs = "<null>";
             tkn = "21ae9a61-6b0e-4862-b98e-ff5f5b44a91a";
             };
             rc = PE0018;
             rd = "Mpin Match & session is created successfully";
             rs = S;
             }
             
             */
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
            
            NSString *rs=[response valueForKey:@"rs"];
            
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
         
            
          
            
            
            // singleton.user_id=[[response valueForKey:@"pd"]valueForKey:@"uid"];
            
            singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
            
            //-------- Add later----------
            singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
            singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
            //-------- Add later----------
            
            
            if ([rs isEqualToString:@"SU"]||[rs isEqualToString:@"S"]) {
                
                if (flg_LgType==TRUE)
                {
                    singleton.mobileNumber=_txt_enterId.text;
                    
                    
                    
                }
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                //---fix lint---
                // NSMutableArray *generalpdList=[[NSMutableArray alloc]init];
                // generalpdList=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                
                NSMutableArray *generalpdList = (NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                
                
                //singleton.user_id=[[response valueForKey:@"pd"]valueForKey:@"uid"];
                singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                
                NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                
                if ([abbreviation length]==0)
                {
                    abbreviation=@"";
                }
                
                NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                emblemString = emblemString.length == 0 ? @"":emblemString;
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                
                
                [[NSUserDefaults standardUserDefaults] setObject:[abbreviation capitalizedString] forKey:@"ABBR_KEY"];
                [singleton setStateId:singleton.user_StateId];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //-------- Add later For handling mpinflag / mpinmand ----------
                singleton.shared_mpinflag =[[response valueForKey:@"pd"]  valueForKey:@"mpinflag"];
                singleton.shared_mpinmand =[[response valueForKey:@"pd"]  valueForKey:@"mpinmand"];
                
                NSLog(@"mpinflag =%@",singleton.shared_mpinflag);
                NSLog(@"mpinmand =%@",singleton.shared_mpinmand);
                if (singleton.shared_mpinflag == (NSString *)[NSNull null]||[singleton.shared_mpinflag length]==0) {
                    singleton.shared_mpinflag=@"TRUE";
                }
                
                if (singleton.shared_mpinmand == (NSString *)[NSNull null]||[singleton.shared_mpinmand length]==0) {
                    singleton.shared_mpinmand=@"TRUE";
                }
                NSString *mpindial =[[response valueForKey:@"pd"]  valueForKey:@"mpindial"];
                NSLog(@"mpindial =%@",mpindial);
                [[NSUserDefaults standardUserDefaults] setValue:mpindial forKey:@"mpindial"];
                
                // recflag save start
                NSString *recflag  = [[response valueForKey:@"pd"]  valueForKey:@"recflag"];
                [[NSUserDefaults standardUserDefaults] setValue:recflag forKey:@"recflag"];


                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinflag forKey:@"mpinflag"];
                [[NSUserDefaults standardUserDefaults] setValue:singleton.shared_mpinmand forKey:@"mpinmand"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //-------- Add later For handling mpinflag / mpinmand ----------

                
                
                //-------- Add later----------
                singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                //-------- Add later----------
                singleton.user_tkn=tkn;
                
                
                
                //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
                // [[NSUserDefaults standardUserDefaults]synchronize];
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value-----------------------
                
                
                NSString* str_name=[generalpdList valueForKey:@"nam"];
                NSString* str_mno =[generalpdList valueForKey:@"mno"];
                NSString* str_amno=[generalpdList valueForKey:@"amno"];
                NSString* str_city=[generalpdList valueForKey:@"cty"];
                NSString* str_state=[generalpdList valueForKey:@"st"];
                NSString* str_district=[generalpdList valueForKey:@"dist"];
                NSString* str_dob=[generalpdList valueForKey:@"dob"];
                NSString* str_gender=[generalpdList valueForKey:@"gndr"];
                NSString* str_pic=[generalpdList valueForKey:@"pic"];
                
                NSString* str_occup=[generalpdList valueForKey:@"occup"];
                NSString* str_qual=[generalpdList valueForKey:@"qual"];
                NSString* str_email=[generalpdList valueForKey:@"email"];
                
                
                
                
                
                
                singleton.notiTypeGenderSelected=@"";
                singleton.profileNameSelected =@"";
                singleton.profilestateSelected=@"";
                singleton.notiTypeCitySelected=@"";
                singleton.notiTypDistricteSelected=@"";
                singleton.profileDOBSelected=@"";
                singleton.altermobileNumber=@"";
                singleton.user_profile_URL=@"";
                
                
                
                singleton.user_Qualification=@"";
                singleton.user_Occupation=@"";
                singleton.profileEmailSelected=@"";
                
                singleton.user_profile_URL=@"";
                
                
                
                
                if ([str_occup length]!=0) {
                    singleton.user_Occupation=str_occup;
                }
                
                if ([str_qual length]!=0) {
                    singleton.user_Qualification=str_qual;
                }
                if ([str_email length]!=0) {
                    singleton.profileEmailSelected=str_email;
                }
                
                
                
                
                if ([str_mno length]!=0) {
                    singleton.mobileNumber=str_mno;
                }
                
                
                if ([str_amno length]!=0) {
                    singleton.altermobileNumber=str_amno;
                }
                
                
                
                if ([str_pic length]!=0) {
                    
                    singleton.user_profile_URL=str_pic;
                    //------------------------- Encrypt Value------------------------
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //------------------------- Encrypt Value------------------------
                    
                    
                }
                
                if ([str_name length]!=0) {
                    str_name = [str_name stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[str_name substringToIndex:1] uppercaseString]];
                    
                    
                }
                if ([singleton.profileNameSelected length]==0) {
                    if ([str_name length]!=0) {
                        singleton.profileNameSelected=str_name;
                        
                    }
                }
                
                
                
                
                if ([singleton.notiTypeGenderSelected length]==0) {
                    if ([str_gender length]!=0) {
                        str_gender=[str_gender uppercaseString];
                        
                        if ([str_gender isEqualToString:@"M"]|| [str_gender isEqualToString:@"MALE"])
                        {
                            singleton.notiTypeGenderSelected=@"Male";
                            
                        }
                        if ([str_gender isEqualToString:@"F"]||[str_gender isEqualToString:@"FEMALE"])
                        {
                            singleton.notiTypeGenderSelected=@"Female";
                            
                        }
                        if ([str_gender isEqualToString:@"T"]) {
                            singleton.notiTypeGenderSelected=@"Other";
                            
                        }
                        
                        
                        
                    }
                }
                
                if ([singleton.profilestateSelected length]==0) {
                    if ([str_state length]!=0) {
                        singleton.profilestateSelected=str_state;
                        //state_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypeCitySelected length]==0) {
                    if ([str_city length]!=0) {
                        singleton.notiTypeCitySelected=str_city;
                        // city_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypDistricteSelected length]==0) {
                    if ([str_district length]!=0) {
                        singleton.notiTypDistricteSelected=str_district;
                        //district_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                
                if ([singleton.profileDOBSelected length]==0) {
                    if ([str_dob length]!=0) {
                        singleton.profileDOBSelected=str_dob;
                        
                    }
                }
                
                
                [[NSUserDefaults standardUserDefaults] setValue:[NSDate new] forKey:RECOVERY_BOX_DATE];
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:RECOVERY_BOX_COUNT];
                
                
                
                
                
                
                
               // ========= check to open show mpin alert box=====
                dispatch_async(dispatch_get_main_queue(),^{
                    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    //[delegate showHideSetMpinAlertBox ];
                    if(!([[recflag uppercaseString] isEqualToString:@"T"]||[[recflag uppercaseString] isEqualToString:@"TRUE"]))
                    {
                        [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];
                    }
                    else
                    {
                        
                        [delegate showHideSetMpinAlertBox ];
                    }
                    [delegate checkRecoveryOptionBOXShownStatus:@"loginOTPorMPIN"];

                });
                
                
                [self alertwithMsg:rd];
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);

            /*
             {
             gcmid = "";
             node = "";
             ntfp = "";
             ntft = "";
             pd =     {
             };
             plang = "";
             rc = PRF;
             rd = "This Mobile number is not registered with UMANG. Please visit New User section for completing registration process.";
             rs = F;
             }

             {
             gcmid = "";
             node = "";
             ntfp = "";
             ntft = "";
             pd =     {
             };
             plang = "";
             rc = API0096;
             rd = "This mobile number is not registered with UMANG. Please visit New User section for completing registration process.";
             rs = F;
             }
             
             */
            
            NSLog(@"RC value=%@",[response valueForKey:@"rc"]);
         
            
            if ([[response valueForKey:@"rc"] isEqualToString:@"API0096"] || ([[response valueForKey:@"rc"] isEqualToString:@"PRF"] && [[response valueForKey:@"rs"] isEqualToString:@"F"]))
            {
                if (self.USER_LOGIN_TYPE == LOGIN_MPIN)//login with MPIN
                {
                   
                    
                    
                    
                    UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil)message:error.localizedDescription delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"cancel", nil),NSLocalizedString(@"ok", nil), nil];
                    
                    
                    
                    alertview.tag=2010;

                    [alertview show];
                }

            }
            else
            {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
                
            }
            _txtMPin.text=@"";
        }
        
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 2010 && buttonIndex == 1)
    {
        NSLog(@"Inside LOGIN_MPIN ");
        [self registerPressed:self];
    }
   else
   {
       NSLog(@"Never Execute ");

   }
}


-(void)alertwithMsg:(NSString*)msg
{
    
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [self openNextView];
    }];
    
    /*
     UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login Successfully!" message:msg preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
     {
     
     [self openNextView];
     
     }];
     [alert addAction:cancelAction];
     [self presentViewController:alert animated:YES completion:nil];*/
    
}

-(void)openNextView
{
    // Default value for Keep Me Login is True
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSInteger currentIndexOfTab=[singleton getSelectedTabIndex];
    TabBarVC *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    tbc.selectedIndex=currentIndexOfTab;
    tbc.comingFrom = @"login";
    //[self presentViewController:tbc animated:NO completion:nil];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
//    UIViewController *topvc=[self topMostController];
//
//    [topvc presentViewController:tbc animated:NO completion:nil];
   
    [delegate.window setRootViewController:tbc];
    [UIView transitionWithView:delegate.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
    
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}



- (IBAction)btnForgotMpinClicked:(id)sender
{
    
    
}




-(IBAction)registerPressed:(id)sender

{
      [[SharedManager sharedSingleton] traceEvents:@"Signup Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    NSLog(@"Register Pressed");
    

    /*if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        mobileReg = [[MobileRegistrationVC alloc] initWithNibName:@"MobileRegistrationVC_iPad" bundle:nil];
        //loadNibNamed:@"MobileRegistrationVC_ipad" owner:self options:nil];
        // cell = (MobileRegistrationVC_ipad *)[nib objectAtIndex:0];
        [self presentViewController:mobileReg animated:YES completion:nil];
        
    }
    
    else
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
        
        mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegistrationVC"];
        [mobileReg setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:mobileReg animated:YES completion:nil];
        
    }*/
    

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Flagship" bundle:nil];
    MobileRegister_FlagVC *mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegister_FlagVC"];
    mobileReg.mobileNumber=_txt_enterId.text;
    [mobileReg setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:mobileReg animated:YES completion:nil];
    
    
}



#pragma mark- Social Login
#pragma mark-

#pragma mark- Social Login
#pragma mark-

-(void)twitterAuthenticationSuccessful:(SocialUserBO*)object
{
    self.USER_LOGIN_TYPE = LOGIN_TWITTER;
    
    
    NSLog(@"SocialUserBO=%@",object);
    
    self.socialID=object.social_id;
    // flg_LgType= true;
    [self hitLoginAPIForTwitterSocialAccounts];
    
    
}

- (IBAction)btnTwitterClicked:(id)sender
{
      [[SharedManager sharedSingleton] traceEvents:@"Twitter Login Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    [self performSelector:@selector(removeLoaderSocial)  withObject:nil afterDelay:1];
    
    
    SocialAuthentication *objAuth = [[SocialAuthentication alloc] init];
    [objAuth loginWithTwitterFromController:self];
    
}

- (IBAction)btnGoogleClicked:(id)sender{
    
      [[SharedManager sharedSingleton] traceEvents:@"Google Login Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    isGoogleLogin=1;
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    [self performSelector:@selector(removeLoaderSocial)  withObject:nil afterDelay:1];
    
    
    self.USER_LOGIN_TYPE = LOGIN_GOOGLE;
    singleton.SOCIAL_GOOGLE_FROM=@"GOOGLE_LOGIN";
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];
}

- (IBAction)btnFacebookClicked:(id)sender {
    
      [[SharedManager sharedSingleton] traceEvents:@"Facebook Login Button" withAction:@"Clicked" withLabel:@"Login Screen" andValue:0];
    isfacebookLogin=1;
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    [self performSelector:@selector(removeLoaderSocial)  withObject:nil afterDelay:1];
    
    
    self.USER_LOGIN_TYPE = LOGIN_FACEBOOK;
    [self fetchFBUserProfileIfLoggedIn];
}





-(void)removeLoaderSocial
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
    
    
    
}




-(void)doLoginWithFacebook
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    BOOL isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]];
    if (isInstalled)
    {
        login.loginBehavior = FBSDKLoginBehaviorNative;
        
    } else {
        login.loginBehavior = FBSDKLoginBehaviorWeb;
        
    }
    
    
    login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error)
        {
            NSLog(@"Unexpected login error: %@", error);
            NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
            NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops";
            [[[UIAlertView alloc] initWithTitle:alertTitle
                                        message:alertMessage
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                              otherButtonTitles:nil] show];
        }
        else if (result.isCancelled)
        {
            NSLog(@"Cancell");
        }
        else
        {
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                [self fetchFBUserProfileIfLoggedIn];
            }
            
        }
    }];
    
}

-(void)fetchFBUserProfileIfLoggedIn
{
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in, do work such as go to next view controller.
        
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"id, name, email" forKey:@"fields"];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                      id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user:%@", result);
                 self.socialID = [result objectForKey:@"id"];
                 
                 
                 NSLog(@"isFacebookLogin :%d", isfacebookLogin);
                 
                 if (isfacebookLogin==1)
                 {
                     isfacebookLogin=2;
                     [self hitLoginAPIForFacebookSocialAccounts];
                 }
             }
         }];
    }
    else{
        [self doLoginWithFacebook];
    }
}

-(void)googleLoginCompleted:(NSNotification*)notification
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"GOOGLE_LOGIN" object:nil];
    
    NSLog(@"isGoogleLogin=======>%d",isGoogleLogin);
    
    
    if (isGoogleLogin==1)
    {
        isGoogleLogin=2;
        
        GIDGoogleUser *user = notification.object;
        self.socialID=user.userID;
        self.USER_LOGIN_TYPE = LOGIN_GOOGLE;
        NSLog(@"INside=======>%d",isGoogleLogin);
        
        [self hitLoginAPIForGoogleSocialAccounts];
    }
    
    
    
    
}


/*
 - (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
*/
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    //    vw_login.frame = CGRectMake(30, vw_login.frame.origin.y, fDeviceWidth-60, vw_login.frame.size.height);
    //    [_btn_mobile setFrame:CGRectMake(0, 0, vw_login.frame.size.width/2, 30)];
    //
    //    [_btn_Adhar setFrame:CGRectMake(vw_login.frame.size.width/2, 0, vw_login.frame.size.width/2, 30)];
    
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


//#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
//#define supportedInterfaceOrientationsReturnType NSUInteger
//#else
//#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
//#endif

//- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscape;
//}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/



@end

