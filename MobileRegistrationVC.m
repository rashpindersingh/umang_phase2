//
//  MobileRegistrationVC.m
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import "MobileRegistrationVC.h"
//#import "RegStep2ViewController.h"
#import "UMAPIManager.h"

#import "MBProgressHUD.h"

#define MAX_LENGTH 10
#define kOFFSET_FOR_KEYBOARD 80.0
#import "EnterMobileOTPVC.h"
#import "EulaScreenVC.h"

#import "LoginAppVC.h"


@interface MobileRegistrationVC ()<UIScrollViewDelegate,EulaChangeDelegate>
{
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *txtMsgLogintheApp;
    MBProgressHUD *hud ;
    IBOutlet UILabel *lblCountryCode;
    BOOL flagAccept;
}

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_msg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_submsg;
@property (weak, nonatomic) IBOutlet UITextField *txt_mobileNo;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;

- (IBAction)btn_nextAction:(id)sender;
@end

@implementation MobileRegistrationVC
@synthesize tout,rtry;
@synthesize commingFrom;
- (id)initWithNibName:(NSString* )nibNameOrNil bundle:(NSBundle* )nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        self =[super initWithNibName:@"MobileRegistrationVC_iPad" bundle:nil];
    }
    return self;
}
/*- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
*/
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    [self hitInitAPI];
    
}

-(void)hitInitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    singleton = [SharedManager sharedSingleton];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            singleton.arr_initResponse=[[NSMutableDictionary alloc]init];
            singleton.arr_initResponse=[response valueForKey:@"pd"];
            NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
            
            [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
            
            
            NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
            NSString*  emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
            NSLog(@"value of abbr=%@",abbr);
            
            if ([abbr length]==0)
            {
                abbr=@"";
            }
            
            emblemString = emblemString.length == 0? @"": emblemString;
            
            [[NSUserDefaults standardUserDefaults] setObject:[abbr capitalizedString] forKey:@"ABBR_KEY"];
            NSString*  infoTab=[singleton.arr_initResponse valueForKey:@"infotab"];
            [[NSUserDefaults standardUserDefaults] setObject:[infoTab capitalizedString] forKey:@"infotab"];
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //---clear data and last fetch date so Home API can load data
            
            //---clear data and last fetch date so Home API can load data
            
            //------------------------- Encrypt Value------------------------
            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
            // Encrypt
            [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
            
            NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSHTTPCookie *cookie;
            for (cookie in [storage cookies]) {
                
                [storage deleteCookie:cookie];
                
            }
            NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
            [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            //------------------------- Encrypt Value------------------------
            
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                // get the data here
                [singleton.dbManager deleteBannerHomeData];
                [singleton.dbManager  deleteAllServices];
                [singleton.dbManager  deleteSectionData];
                
                
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   //-------------check condition temp-----
                                   //                                   [[NSUserDefaults standardUserDefaults] setObject:singleton.tabSelected  forKey:@"SELECTED_TAB"];
                                   
                                   [[NSUserDefaults standardUserDefaults]synchronize];
                                   
                                   singleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
                                   
                                   
                              
                               });
            });
            
            
            
            
            // jump to home view  tab
            
         
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
       
        }
        
    }];
    
}


#pragma mark- add Navigation View to View

-(void)addNavigationView{
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btnBackClicked:btnBack];
    };
    [self.view addSubview:nvView];
    
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
    _lbl_title.font = [AppFont semiBoldFont:24.0];
    _lbl_title_msg.font = [AppFont semiBoldFont:16.0];
    _lbl_title_submsg.font = [AppFont mediumFont:14.0];
    lblCountryCode.font = [AppFont regularFont:22];
    _txt_mobileNo.font = [AppFont regularFont:22];
    txtMsgLogintheApp.font = [AppFont regularFont:12.0];
    
    
}
-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

- (void)viewDidLoad
{
    flagAccept=FALSE;
    [btn_AcceptBox setImage:[UIImage imageNamed:@"checkbox_outline"] forState:UIControlStateNormal];
    btn_Eula.hidden=TRUE;
    // btn_Eula.titleLabel.text= NSLocalizedString(@"eula", nil);
    // lbl_agreeterm.text = NSLocalizedString(@"agree_to_terms_condition", nil);
    
    
    //NSLog(@"lbl_agreeterm=%@",lbl_agreeterm.text);
    
    NSString * htmlString = NSLocalizedString(@"I_agree_to_the_term_EULA", nil);
    
    @try {
        htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx;}</style>",lbl_agreeterm.font.fontName,
                                                          lbl_agreeterm.font.pointSize]];    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    }
    @finally {
    }
    
    
    
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                        documentAttributes:nil
                                                                     error:nil];
    lbl_agreeterm.attributedText = attrStr;
    [lbl_agreeterm setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnOpenEula:)];
    [tapGestureRecognizer setNumberOfTapsRequired:1];
    [lbl_agreeterm addGestureRecognizer:tapGestureRecognizer];
    lbl_agreeterm.lineBreakMode = NSLineBreakByWordWrapping;
    
    
    
    [super viewDidLoad];
    
    [_txt_mobileNo becomeFirstResponder];
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:MOBILE_REGISTRATION_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    singleton = [SharedManager sharedSingleton];
    _txt_mobileNo.clearButtonMode = UITextFieldViewModeWhileEditing;
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    txtMsgLogintheApp.text = NSLocalizedString(@"register_intro_sub_heading_hint", nil);
    _lbl_title.text = NSLocalizedString(@"registration_label", nil);
    _lbl_title_msg.text = NSLocalizedString(@"register_intro_heading", nil);
    _lbl_title_submsg.text =NSLocalizedString(@"otp_on_this_number", nil);
    //  [_btn_next setTitle:NSLocalizedString(@"next", @"") forState:UIControlStateNormal];
    /*  _txt_mobileNo.text=singleton.temp_reg_mobile;
     
     
     if (_txt_mobileNo.text.length==MAX_LENGTH) {
     //self.btn_next.enabled=YES;
     
     self.btn_next.enabled=YES;
     [self enableBtnNext:YES];
     
     }
     else
     {
     //self.btn_next.enabled=NO;
     self.btn_next.enabled=NO;
     [self enableBtnNext:NO];
     
     }
     */
    self.view.userInteractionEnabled = YES;
    
    [_txt_mobileNo addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    //    self.vwTextBG.layer.borderColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0].CGColor;
    //    self.vwTextBG.layer.borderWidth = 2.0;
    //    self.vwTextBG.layer.cornerRadius = 4.0;
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    //--------- Code for handling -------------------
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollView.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height+100;
                       scrollView.contentSize = contentRect.size;
                   });
    
    [self addNavigationView];
}

-(void)hideKeyboard
{
    [scrollView setContentOffset:
     CGPointMake(0, -scrollView.contentInset.top) animated:YES];
    
    [self.txt_mobileNo resignFirstResponder];
}

- (void)textFieldDidChange:(UITextField *)textField
{
    
    
    
    if (textField.text.length >= MAX_LENGTH)
    {
        textField.text = [textField.text substringToIndex:MAX_LENGTH];
        
        //self.btn_next.enabled=YES;
        // [self enableBtnNext:YES];
        
        // NSLog(@"got it");
    }
    else
    {
        // self.btn_next.enabled=NO;
        // [self enableBtnNext:NO];
    }
    [self checkValidation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnOpenEula:(id)sender
{
    [[SharedManager sharedSingleton] traceEvents:@"Open Eula Button" withAction:@"Clicked" withLabel:@"Mobile Register Screen" andValue:0];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];
    EulaScreenVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"EulaScreenVC"];
    vc.EulaDelegate=self;
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:NO completion:nil];
    
    
}




-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        [_txt_mobileNo resignFirstResponder];
        
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
        
    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}
- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
    NSString *phoneRegex =@"[6789][0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
}


-(IBAction)btnAcceptBox:(id)sender
{
    [[SharedManager sharedSingleton] traceEvents:@"Accept Eula Button" withAction:@"Clicked" withLabel:@"Mobile Register Screen" andValue:0];
    if (flagAccept==FALSE)
    {
        [btn_AcceptBox setImage:[UIImage imageNamed:@"checkbox_Tick"] forState:UIControlStateNormal];
        flagAccept=TRUE;
        
    }
    else
    {
        [btn_AcceptBox setImage:[UIImage imageNamed:@"checkbox_outline"] forState:UIControlStateNormal];
        
        flagAccept=FALSE;
        
    }
    
    [self checkValidation];
    
    
    
    
    
    
}

-(void)checkValidation
{
    
    if (([self validatePhone:_txt_mobileNo.text]==TRUE) && flagAccept==TRUE)
    {
        //Do nothing button is disable
        self.btn_next.enabled=YES;
        [self enableBtnNext:YES];
        
    }
    else
    {
        //Do nothing button is disable
        self.btn_next.enabled=NO;
        [self enableBtnNext:NO];
        
    }
}

- (IBAction)btn_nextAction:(id)sender
{
    //jump to next step here
    [[SharedManager sharedSingleton] traceEvents:@"Next Button" withAction:@"Clicked" withLabel:@"Mobile Register Screen" andValue:0];
    if (([self validatePhone:_txt_mobileNo.text]!=TRUE)) {
        
        NSLog(@"Wrong Mobile");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:NSLocalizedString(@"enter_correct_phone_number", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        
        [alert show];
        _txt_mobileNo.text = @"";
    }
    
    else
    {
        [self hitAPI];
    }
}

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:_txt_mobileNo.text forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"sms" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile contact //not supported iphone
    [dictBody setObject:@"rgtmob" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    singleton.mobileNumber=_txt_mobileNo.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_REGISTRATION withBody:dictBody andTag:TAG_REQUEST_INIT_REG completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            
            
            
            //------ Sharding Logic parsing---------------
            
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            //            NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
            //            NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
            //            NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
            
            tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
            rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
            
            
            
            //            NSString *rc=[response valueForKey:@"rc"];
            //            NSString *rd=[response valueForKey:@"rd"];
            //            NSString *rs=[response valueForKey:@"rs"];
            
            
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                [self openNextView];
            }
            
        }
        else
        {
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil)otherButtonTitles:nil];
            
            alert.tag=100;
            
            _txt_mobileNo.text = @"";
            
            [alert show];
            
        }
        
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 100)
    {
        [self openLoginView];
    }
    
}




-(void)openLoginView
{
    //----later add
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------
    
    if (singleton.user_tkn.length == 0)
    {
        if ([commingFrom isEqualToString:@"PreLogin"])
        {
            LoginAppVC *vc;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
            }
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        else
        {
            [self dismissViewControllerAnimated:NO completion:nil];
            
        }
    }
    else
    {
        LoginAppVC *vc;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
        }
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:vc animated:YES completion:nil];
    }
    
    
    
    
    
}




- (void)EulaAcceptChanged:(BOOL)Acceptstatus
{
    NSLog(@"Acceptstatus value of bool is = %@", (Acceptstatus ? @"YES" : @"NO"));
    //flagAccept=Acceptstatus;
    
    if (Acceptstatus==TRUE)
    {
        [btn_AcceptBox setImage:[UIImage imageNamed:@"checkbox_Tick"] forState:UIControlStateNormal];
        flagAccept=TRUE;
        
    }
    else
    {
        [btn_AcceptBox setImage:[UIImage imageNamed:@"checkbox_outline"] forState:UIControlStateNormal];
        
        flagAccept=FALSE;
        
    }
    
    [self checkValidation];
    
    
    
}


- (IBAction)btnBackClicked:(id)sender
{
    if (singleton.user_tkn.length == 0)
    {
        //commingFrom

        if ([commingFrom isEqualToString:@"PreLogin"])
        {
            [self.navigationController popViewControllerAnimated:YES];

        }
        else
        {
            [self dismissViewControllerAnimated:NO completion:nil];

        }
        
     

    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
}


-(void)openNextView
{
    
    if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        EnterMobileOTPVC *vc = [[EnterMobileOTPVC alloc] initWithNibName:@"EnterMobileOTPVC_iPad" bundle:nil];
        vc.tout=tout;
        vc.rtry=rtry;
        vc.TYPE_LOGIN_CHOOSEN=IS_FROM_MOBILE_NUMBER_REGISTRATION;
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:vc animated:YES completion:nil];
        
        
    }
    
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        EnterMobileOTPVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"EnterMobileOTPVC"];
        vc.tout=tout;
        vc.rtry=rtry;
        vc.TYPE_LOGIN_CHOOSEN=IS_FROM_MOBILE_NUMBER_REGISTRATION;
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:vc animated:NO completion:nil];
    }
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+150)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];
    
    [self setFontforView:self.view andSubViews:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568)
    {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [self.txt_mobileNo resignFirstResponder];
        }
    }
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

