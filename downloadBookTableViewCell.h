//
//  downloadBookTableViewCell.h
//  EbookDownloadSpice
//
//  Created by admin on 08/04/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface downloadBookTableViewCell : UITableViewCell


@property(nonatomic,retain)IBOutlet UILabel *lbl_bookName;
@property(nonatomic,retain)IBOutlet UILabel *lbl_bookCount;
@property(nonatomic,retain)IBOutlet UILabel *lbl_bookDownloadPercent;
@property(nonatomic,retain)IBOutlet UIButton *btn_playresume;
@property(nonatomic,retain)IBOutlet UIButton *btn_Status;
@property(nonatomic,retain)IBOutlet UIProgressView *vw_downloadStatusBar;


@end
