//
//  EnterNewMobNumVC.m
//  Umang
//
//  Created by admin on 09/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "EnterNewMobNumVC.h"
//#import "RegStep2ViewController.h"
#import "UMAPIManager.h"

#import "MBProgressHUD.h"

#import "VerifyOTPNewMbVC.h"

#import "SecuritySettingVC.h"


#define MAX_LENGTH 10
#define kOFFSET_FOR_KEYBOARD 80.0


@interface EnterNewMobNumVC()<UIScrollViewDelegate>
{
    __weak IBOutlet UIButton *btnBack;
    MBProgressHUD *hud ;
}

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_msg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_submsg;
@property (weak, nonatomic) IBOutlet UITextField *txt_mobileNo;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property(nonatomic,retain) NSMutableDictionary *dictBody;

- (IBAction)btn_nextAction:(id)sender;

@end

@implementation EnterNewMobNumVC
@synthesize userenterMPIN;

@synthesize tout,rtry;
@synthesize dictBody;
- (void)viewDidLoad
{
 
    [super viewDidLoad];
    
    if (self.view.frame.size.width > 500)
    {
        self.btn_next.frame = CGRectMake(200, self.btn_next.frame.origin.y, 368, 65);
    }
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
    
    singleton = [SharedManager sharedSingleton];
    [_txt_mobileNo becomeFirstResponder];
    _lbl_title.text = NSLocalizedString(@"change_mobile_num", nil);
    _lbl_title_msg.text = NSLocalizedString(@"enter_mobile_num", nil);
     _lbl_title_submsg.text = NSLocalizedString(@"otp_on_this_number", nil);
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    self.btn_next.enabled=NO;
    self.view.userInteractionEnabled = YES;
    
    [_txt_mobileNo addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.vwTextBG.layer.borderColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0].CGColor;
    self.vwTextBG.layer.borderWidth = 2.0;
    self.vwTextBG.layer.cornerRadius = 4.0;
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    //--------- Code for handling -------------------
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollView.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height+100;
                       scrollView.contentSize = contentRect.size;
                   });
    
    [self addNavigationView];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
    _lbl_title.font = [AppFont semiBoldFont:21.0];
    _lbl_title_msg.font = [AppFont semiBoldFont:16.0];
    _lbl_title_submsg.font = [AppFont mediumFont:12.0];
   // lblCountryCode.font = [AppFont mediumFont:21];
    _txt_mobileNo.font = [AppFont mediumFont:21];
    
}

-(void)hideKeyboard
{
    [scrollView setContentOffset:
     CGPointMake(0, -scrollView.contentInset.top) animated:YES];
    
    [self.txt_mobileNo resignFirstResponder];
}


/*
 - (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
*/

- (void)textFieldDidChange:(UITextField *)textField
{
    
    
    
    if (textField.text.length >= MAX_LENGTH)
    {
        textField.text = [textField.text substringToIndex:MAX_LENGTH];
        
        self.btn_next.enabled=YES;
        [self enableBtnNext:YES];
        
        // NSLog(@"got it");
    }
    else
    {
        self.btn_next.enabled=NO;
        [self enableBtnNext:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        [_txt_mobileNo resignFirstResponder];
        
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
        
    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}
- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
    NSString *phoneRegex =@"[6789][0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
}

#pragma mark- add Navigation View to View

-(void)addNavigationView{
    btnBack.hidden = true;
    //.hidden = true;
    //btnHelp.hidden = true;
    //  .hidden = true;
    NavigationView *nvView = [[NavigationView alloc] init];
    __weak typeof(self) weakSelf = self;
    nvView.didTapBackButton = ^(id btnBack) {
        [weakSelf btnBackClicked:btnBack];
    };
    //nvView.lblTitle.text = lblHeadingAccountSeeting.text;
    [self.view addSubview:nvView];
    [self.view layoutIfNeeded];
    if (iPhoneX())
    {
        CGRect tbFrame = scrollView.frame;
        tbFrame.origin.y = kiPhoneXNaviHeight
        tbFrame.size.height = fDeviceHeight - kiPhoneXNaviHeight;
        scrollView.frame = tbFrame;
        [self.view layoutIfNeeded];
    }
}
- (IBAction)btn_nextAction:(id)sender
{
    //jump to next step here
    if (([self validatePhone:_txt_mobileNo.text]!=TRUE)) {
        
        NSLog(@"Wrong Mobile");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:NSLocalizedString(@"enter_correct_phone_number", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [self hitAPI];
    }
}

//----- hitAPI  registration ------
-(void)hitAPI
{
    NSString *encryptedInputID = @"";
    NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",userenterMPIN ,strSaltMPIN];
    encryptedInputID=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    [dictBody setObject:_txt_mobileNo.text forKey:@"nmno"];
    [dictBody setObject:@"updtmob" forKey:@"ort"];
    [dictBody setObject:@"" forKey:@"email"];
    [dictBody setObject:encryptedInputID forKey:@"mpin"];
    [dictBody setObject:@"" forKey:@"dgt"];
    [dictBody setObject:@"" forKey:@"st"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"initiate" forKey:@"type"];

    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);    
    
    //singleton.mobileNumber=_txt_mobileNo.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UMOBILE withBody:dictBody andTag:TAG_REQUEST_UMOBILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
            rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
            
            
            
//            NSString *rc=[response valueForKey:@"rc"];
//            NSString *rd=[response valueForKey:@"rd"];
//            NSString *rs=[response valueForKey:@"rs"];
            
            
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                [self openNextView];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}
- (IBAction)btnBackClicked:(id)sender {
    
   // [self.navigationController popViewControllerAnimated:YES];
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    for (UIViewController *aViewController in allViewControllers)
    {
        if ([aViewController isKindOfClass:[SecuritySettingVC class]])
        {
            // [aViewController removeFromParentViewController];
            [self.navigationController popToViewController:aViewController
                                                  animated:YES];
            //[self closeView];
            
        }
    }

}


-(void)openNextView
{
    //EnterMobileOTPVC
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    VerifyOTPNewMbVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyOTPNewMbVC"];
    
    vc.tout=tout;
    vc.rtry=rtry;
    
    //[dictBody setObject:encryptedInputID forKey:@"mpin"];

    vc.mpinEncrypt=[dictBody valueForKey:@"mpin"];
    vc.newmobile=[dictBody valueForKey:@"nmno"];

  //  [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
   // [self presentViewController:vc animated:NO completion:nil];
    [self.navigationController pushViewController:vc animated:YES];

    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+150)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568)
    {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [self.txt_mobileNo resignFirstResponder];
        }
    }
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
