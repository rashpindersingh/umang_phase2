//
//  AboutServiceCell.h
//  Umang
//
//  Created by admin on 28/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutServiceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *aboutServiceDescription;

@end
