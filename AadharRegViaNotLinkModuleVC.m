//
//  AadharRegViaNotLinkModuleVC.m
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import "AadharRegViaNotLinkModuleVC.h"
#import "MyTextField.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "UserEditVC.h"
#define kOFFSET_FOR_KEYBOARD 80.0
#import "AadharOTPViaNotLinkVC.h"



@interface AadharRegViaNotLinkModuleVC()<MyTextFieldDelegate,UIScrollViewDelegate>

{
    BOOL flag_Accept;
    MBProgressHUD *hud;
    NSString *aadharStr;
    
    __weak IBOutlet UILabel *lblTip;
    SharedManager *singleton;
    __weak IBOutlet UIButton *btnBack;
    IBOutlet UIScrollView *scrollview;
    int tagAPI;
    
    IBOutlet UIScrollView *scrollView;
    
    __weak IBOutlet UILabel *lblHeader;
    
    __weak IBOutlet UILabel *lblRegisterAadhar;
    
    __weak IBOutlet UILabel *lblVerificationOTP;
    
    __weak IBOutlet UILabel *lblAgreeTerms;
    
    
}
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet MyTextField *txt_aadhar1;
@property (weak, nonatomic) IBOutlet MyTextField *txt_aadhar2;
@property (weak, nonatomic) IBOutlet MyTextField *txt_aadhar3;

@end

@implementation AadharRegViaNotLinkModuleVC
@synthesize txt_aadhar1,txt_aadhar2,txt_aadhar3;
@synthesize str_aadhaar_Value;



@synthesize tout,rtry;

- (id)initWithNibName:(NSString* )nibNameOrNil bundle:(NSBundle* )nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        self =[super initWithNibName:@"AadharRegViaNotLinkModuleVC_iPad" bundle:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
    NSString *str = NSLocalizedString(@"tip", nil);
    lblTip.text = [NSString stringWithFormat:@"%@%@",str,NSLocalizedString(@"aadhaar_mob_tip", nil)];
    
    
    [txt_aadhar1 becomeFirstResponder];
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:AADHAR_REGISTRATION_VIA_NOT_LINKED];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    self.btn_next.enabled=NO;
    flag_Accept=TRUE;
    
    
    
    
    
    [lblTip setHidden:YES];
    
    if ([self.tagFrom isEqualToString:@"RESYNCAADHAARCARD"]) {
        txt_aadhar1.enabled = NO;
        txt_aadhar2.enabled = NO;
        txt_aadhar3.enabled = NO;
        
        
        self.btn_acceptTerm.selected=TRUE;
        flag_Accept=FALSE;
        
        self.btn_next.enabled=YES;
        
        
        [self enableBtnNext:YES];
        
        
        if ([str_aadhaar_Value length]>=12)
        {
            //str_aadhaar_Value
            
            
            @try {
                // our starting point will begin at 0
                NSInteger startingPoint = 0;
                
                // each substring will be 2 characters long
                NSInteger substringLength = 4;
                
                // create our empty mutable array
                NSMutableArray *substringArray = [NSMutableArray array];
                // loop through the array as many times as the substring length fits into the test
                // string
                for (NSInteger i = 0; i < str_aadhaar_Value.length / substringLength; i++) {
                    // get the substring of the test string starting at the starting point, with the intended substring length
                    NSString *substring = [str_aadhaar_Value substringWithRange:NSMakeRange(startingPoint, substringLength)];
                    
                    // add it to the array
                    [substringArray addObject:substring];
                    
                    // increment the starting point by the amount of the substring length, so next iteration we
                    // will start after the last character we left off at
                    startingPoint += substringLength;
                }
                
                if( [substringArray count]>=3)
                {
                    txt_aadhar1.text=[substringArray objectAtIndex:0];
                    txt_aadhar2.text=[substringArray objectAtIndex:1];
                    txt_aadhar3.text=[substringArray objectAtIndex:2];
                    
                    
                }
                
                [self checkValidation];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
        }
    }
    else
    {
        txt_aadhar1.enabled = YES;
        txt_aadhar2.enabled = YES;
        txt_aadhar3.enabled = YES;
    }
    
    
    singleton=[SharedManager sharedSingleton];
    //----- Setting delegate for Custom textfield so back space operation work smooth
    
    
    lblHeader.text = NSLocalizedString(@"adhaar_details", nil);
    lblRegisterAadhar.text = NSLocalizedString(@"aadhar_intro", nil);
    
    lblAgreeTerms.text = NSLocalizedString(@"aadhaar_consent_txt", nil);
    lblAgreeTerms.adjustsFontSizeToFitWidth = YES;
    lblVerificationOTP.text = NSLocalizedString(@"aadhar_sub_intro", nil);
    
    
    
    txt_aadhar1.myDelegate = self;
    txt_aadhar2.myDelegate = self;
    txt_aadhar3.myDelegate = self;
    
    [txt_aadhar1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [txt_aadhar2 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [txt_aadhar3 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _btnSkip.hidden = YES;
    
    self.view.userInteractionEnabled = YES;
    
    
    [self.btn_acceptTerm setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
    [self.btn_acceptTerm setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateSelected];
    
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollView.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height+100;
                       scrollView.contentSize = contentRect.size;
                   });
    
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeader.font = [AppFont semiBoldFont:22.0];
    lblVerificationOTP.font = [AppFont mediumFont:14];
    lblRegisterAadhar.font = [AppFont semiBoldFont:16.0];
    txt_aadhar1.font = [AppFont regularFont:21.0];
    txt_aadhar2.font = [AppFont regularFont:21.0];
    txt_aadhar3.font = [AppFont regularFont:21.0];
    lblAgreeTerms.font = [AppFont mediumFont:13];
    lblTip.font = [AppFont lightFont:13];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (IBAction)btnSkipClicked:(id)sender
{
    
    
}



- (IBAction)btnAccept_termsClicked:(UIButton*)sender
{
    sender.selected = !(sender.selected);
    
    
    if (((sender.selected == YES))&&(aadharStr.length ==  12))
    {
        [self enableBtnNext:YES];
        self.btn_next.enabled=YES;
        
    }
    else
    {
        [self enableBtnNext:NO];
        self.btn_next.enabled=NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)hideKeyboard
{
    //[self.view endEditing:YES];
    [scrollView setContentOffset:
     CGPointMake(0, -scrollView.contentInset.top) animated:YES];
    
    
    [txt_aadhar1 resignFirstResponder];
    [txt_aadhar2 resignFirstResponder];
    [txt_aadhar3 resignFirstResponder];
    
    
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void)textFieldDidDelete:(UITextField *)textField
{
    
    if (textField.text.length == 0)
    {
        
        if ([textField isEqual:txt_aadhar3])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [txt_aadhar2 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar2])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [txt_aadhar1 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar1])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [textField resignFirstResponder];
        }
        
    }
    
}


//---Code for hangle jump from one textfield to another while filling values
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.text.length >= 4)
    {
        
        if ([textField isEqual:txt_aadhar1])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [txt_aadhar2 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar2])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [txt_aadhar3 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar3])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [textField resignFirstResponder];
        }
        
        
        [self checkValidation];
        
        // NSLog(@"got it");
    }
    else
    {
        flag_Accept=FALSE;
        [self btnAcceptTerm:self];
        
    }
    
    
}


-(void)checkValidation
{
    aadharStr=[NSString stringWithFormat:@"%@%@%@",txt_aadhar1.text, txt_aadhar2.text, txt_aadhar3.text];
    
    if (aadharStr.length <12)
    {
        //Enable button and jump to next view
        
        [self enableBtnNext:NO];
        self.btn_next.enabled=NO;
        
        
    }
    else
    {
        
        //        self.btn_next.enabled=YES;
        //
        //        [self enableBtnNext:YES];
        
        
    }
    
    
}


-(void)enableBtnNext:(BOOL)status
{
    if (status == YES)
    {
        [self hideKeyboard];
        
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}


- (IBAction)btnNextClicked:(id)sender
{
    
    [self hitAPIforLinkProfile];
    
    
    
    
}


- (IBAction)btnBackClicked:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)hitAPIforLinkProfile
{
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:aadharStr forKey:@"aadhr"];
    [dictBody setObject:@"aadhar" forKey:@"type"];
    if ([self.tagFrom isEqualToString:@"RESYNCAADHAARCARD"])
    {
        [dictBody setObject:@"relink" forKey:@"chnl"];
        
    }
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_REGISTRATION withBody:dictBody andTag:TAG_REQUEST_INIT_REG completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         
         if (error == nil) {
             NSLog(@"Server Response = %@",response);
             //------ Sharding Logic parsing---------------
             NSString *node=[response valueForKey:@"node"];
             if([node length]>0)
             {
                 [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
             }
             
             
             //------ Sharding Logic parsing---------------
             
             
             //----- below value need to be forword to next view according to requirement after checking Android apk-----
             //   NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
             
             
             NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"maskmno"];
             
             //     NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
             
             
             tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
             rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
             
             
             
             
             //   NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
             
             //             NSString *rc=[response valueForKey:@"rc"];
             //             NSString *rd=[response valueForKey:@"rd"];
             //             NSString *rs=[response valueForKey:@"rs"];
             
             
             //----- End value need to be forword to next view according to requirement after checking Android apk-----
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
             {
                 
                 //    singleton.mobileNumber=txtMobileNumber.text;
                 
                 AadharOTPViaNotLinkVC *vc;
                 
                 if ([[UIScreen mainScreen]bounds].size.height == 1024)
                 {
                     vc = [[AadharOTPViaNotLinkVC alloc] initWithNibName:@"AadharOTPViaNotLinkVC_iPad" bundle:nil];
                     if ([self.tagFrom isEqualToString:@"RESYNCAADHAARCARD"])
                     {
                         vc.TAGFROM=@"RESYNCAADHAARCARD";
                         
                     }
                     
                     // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                     vc.strAadharNumber = aadharStr;
                     vc.maskedMobileNumber = wmsg;
                     
                     
                     
                     vc.lblScreenTitleName.text = @"Mobile Number Verification";
                     
                     
                     vc.tout=tout;
                     vc.rtry=rtry;
                     
                     
                     
                     [self.navigationController pushViewController:vc animated:YES];
                     
                 }
                 
                 else
                 {
                     
                     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                     
                     vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharOTPViaNotLinkVC"];
                     
                     if ([self.tagFrom isEqualToString:@"RESYNCAADHAARCARD"])
                     {
                         vc.TAGFROM=@"RESYNCAADHAARCARD";
                         
                     }
                     
                     // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                     vc.strAadharNumber = aadharStr;
                     vc.maskedMobileNumber = wmsg;
                     
                     
                     
                     vc.lblScreenTitleName.text = @"Mobile Number Verification";
                     
                     
                     vc.tout=tout;
                     vc.rtry=rtry;
                     
                     
                     
                     [self.navigationController pushViewController:vc animated:YES];
                     
                     
                     
                     
                 }
             }
             //             682647029382
         }
         else{
             NSLog(@"Error Occured = %@",error.localizedDescription);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                             message:error.localizedDescription
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                   otherButtonTitles:nil];
             [alert show];
             
             txt_aadhar1.text=@"";
             txt_aadhar2.text=@"";
             txt_aadhar3.text=@"";
             
             [self enableBtnNext:NO];
             self.btn_next.enabled=NO;
             
             self.btn_acceptTerm.selected=FALSE;
             flag_Accept=FALSE;
             [self checkValidation];
             
         }
         
     }];
    
    
    
}



- (IBAction)btnAcceptTerm:(id)sender
{
    
    if (flag_Accept==TRUE) {
        self.btn_acceptTerm.selected=TRUE;
        flag_Accept=FALSE;
        
        
    }
    else
    {
        self.btn_acceptTerm.selected=FALSE;
        flag_Accept=TRUE;
        
        
        
    }
    [self checkValidation];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+150)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [self.txt_aadhar1 resignFirstResponder];
            [self.txt_aadhar2 resignFirstResponder];
            [self.txt_aadhar3 resignFirstResponder];
            
        }
    }
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
